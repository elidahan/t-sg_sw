DESCRIPTION = "TSG BSP"
SECTION = "bsp"
#RDEPENDS_${PN} = "glib-2.0"
DEPENDS = "glib-2.0"
DEPENDS += "readline"
DEPENDS += "curl"
LICENSE = "CLOSED"
LIC_FILES_CHKSUM = ""

inherit externalsrc
EXTERNALSRC = "${THISDIR}/../../../../t-sg_sw/sources"
EXTERNALSRC_BUILD = "${WORKDIR}/build"

inherit pkgconfig cmake
EXTRA_OECMAKE += " -DMACHINE=KONTRON-COME-BBD7 -DBOARD=COME"

SOLIBS = ".so"
FILES_SOLIBSDEV = ""

addtask add_env after do_preconfigure before do_compile
do_add_env () {
    export MACHINE="kontron-come-bbd7"
}

do_install() {

    # add the /usr/lib and /usr/bin folders to the sysroot for this recipe, to be
    # added to the final rootfs
    install -d ${D}${libdir}
    install -d ${D}${bindir}
   
# install the prebuilt library in /usr/lib with default permissions
    install ${B}/common/*.so ${D}${libdir}
    chrpath -d ${D}${libdir}/libtsg_common.so
    install ${B}/http_util/*.so ${D}${libdir}
    chrpath -d ${D}${libdir}/libtsg_http_util.so
    install ${B}/devman/spibus/*.so ${D}${libdir}
    chrpath -d ${D}${libdir}/libtsg_spibus.so
    install ${B}/devman/i2cbus/*.so ${D}${libdir}
    chrpath -d ${D}${libdir}/libtsg_i2cbus.so
    install ${B}/devman/i2ccore/*.so ${D}${libdir}
    chrpath -d ${D}${libdir}/libtsg_i2ccore.so
    install ${B}/devman/intc/*.so ${D}${libdir}
    chrpath -d ${D}${libdir}/libtsg_intc.so
    install ${B}/devman/axispi/*.so ${D}${libdir}
    chrpath -d ${D}${libdir}/libtsg_axispi.so
    install ${B}/devman/uart/*.so ${D}${libdir}
    chrpath -d ${D}${libdir}/libtsg_axiuart.so
    install ${B}/devman/rtc/*.so ${D}${libdir}
    chrpath -d ${D}${libdir}/libtsg_rtc.so
    install ${B}/devman/eth/*.so ${D}${libdir}
    chrpath -d ${D}${libdir}/libtsg_eth.so
    install ${B}/devman/mailbox/*.so ${D}${libdir}
    chrpath -d ${D}${libdir}/libtsg_mbox.so
    install ${B}/devman/gpio/*.so ${D}${libdir}
    chrpath -d ${D}${libdir}/libtsg_gpio.so
    install ${B}/devman/fan/*.so ${D}${libdir}
    chrpath -d ${D}${libdir}/libtsg_fan.so
    install ${B}/devman/spidev/*.so ${D}${libdir}
    chrpath -d ${D}${libdir}/libtsg_spidev.so
    install ${B}/devman/pmbus/*.so ${D}${libdir}
    chrpath -d ${D}${libdir}/libtsg_pmbus.so
    install ${B}/devman/eeprom/*.so ${D}${libdir}
    chrpath -d ${D}${libdir}/libtsg_eeprom.so
    install ${B}/devman/lcd/*.so ${D}${libdir}
    chrpath -d ${D}${libdir}/libtsg_lcd.so
    install ${B}/devman/currmon/*.so ${D}${libdir}
    chrpath -d ${D}${libdir}/libtsg_currmon.so   
    install ${B}/devman/xadc/*.so ${D}${libdir}
    chrpath -d ${D}${libdir}/libtsg_xadc.so
    install ${B}/devman/sysmon/*.so ${D}${libdir}
    chrpath -d ${D}${libdir}/libtsg_sysmon.so
    install ${B}/devman/trans/*.so ${D}${libdir}
    chrpath -d ${D}${libdir}/libtsg_trans.so   
    install ${B}/devman/portmap/*.so ${D}${libdir}
    chrpath -d ${D}${libdir}/libtsg_portmap.so   
    install ${B}/devman/smartcard/*.so ${D}${libdir}
    chrpath -d ${D}${libdir}/libtsg_smartcard.so   
    install ${B}/devman/i2cio/*.so ${D}${libdir}
    chrpath -d ${D}${libdir}/libtsg_i2cio.so   
    install ${B}/devman/cpu/*.so ${D}${libdir}
    chrpath -d ${D}${libdir}/libtsg_cpu.so   
    install ${B}/devman/dramtester/*.so ${D}${libdir}
    chrpath -d ${D}${libdir}/libtsg_dramtester.so   
    install ${B}/devman/src/*.so ${D}${libdir}
    chrpath -d ${D}${libdir}/libtsg_devman.so
    install ${B}/devman/board/*.so ${D}${libdir}
    chrpath -d ${D}${libdir}/libtsg_board.so
    install ${B}/devman/devmansm/tsg_devmansm ${D}${bindir}
    chrpath -d ${D}${bindir}/tsg_devmansm
}
