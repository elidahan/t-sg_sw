DESCRIPTION = "Kernel module for COMx  BSP"
SECTION = "bsp"
LICENSE = "CLOSED"
LIC_FILES_CHKSUM = ""

SRC_URI = "file://devman_core.c \
           file://come_impl.c\
           file://board/come_mod.c\
           file://spi_flash/spi_flash_mod.c\
           file://devman_mod.h\
           file://come_mod.h\
           file://spi_mod.h\
           file://Makefile \
"
inherit module
S = "${WORKDIR}"

do_install() {
	install -d ${D}/lib/modules/${KERNEL_VERSION}/drivers/come/
	install -m 0644 *.ko ${D}/lib/modules/${KERNEL_VERSION}/drivers/come/
}



