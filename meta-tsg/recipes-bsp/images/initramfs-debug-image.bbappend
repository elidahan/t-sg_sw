
DEPENDS += "u-boot-mfgtool"
IMAGE_FSTYPES += "cpio.gz.u-boot"

PACKAGE_INSTALL += "util-linux net-tools iputils e2fsprogs dosfstools \
                                          tar emmc-deploy"
