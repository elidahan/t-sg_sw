DESCRIPTION = "TSG BSP"
SECTION = "bsp"

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

LICENSE = "CLOSED"

SRC_URI += "file://deploy.sh" 

do_install() {
    install -d ${D}${bindir}
    install -m 0755 ${WORKDIR}/deploy.sh ${D}${bindir}
}
