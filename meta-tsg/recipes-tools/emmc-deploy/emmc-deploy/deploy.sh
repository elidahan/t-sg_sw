#!/bin/sh

tftp -g -b 65000 -r zImage $1
tftp -g -b 65000 -r imx7d-tsg.dtb $1
tftp -g -b 65000 -r initramfs-debug-image-imx7dsmarc.cpio.gz.u-boot $1
tftp -g -b 65000 -r uEnv.txt $1
tftp -g -b 65000 -r fsl-image-validation-imx-imx7dsmarc.tar.bz2 $1
echo "export DISK=/dev/mmcblk2"
export DISK=/dev/mmcblk2
echo "umount" ${DISK}p1
umount ${DISK}p1
echo "umount" ${DISK}p2
umount ${DISK}p2
echo "umount" ${DISK}p3
umount ${DISK}p3
echo "dd if=/dev/zero of=${DISK} bs=1M count=4096"
dd if=/dev/zero of=${DISK} bs=1M count=4096
echo "sfdisk" ${DISK} "<<-__EOF__
1M,16M,0x83
,48M,0x83,*
,,,-
__EOF__
"
sfdisk ${DISK} <<-__EOF__
1M,16M,0x83
,48M,0x83,*
,,,-
__EOF__
echo "mkfs.vfat -F 16" ${DISK}p1 "-n initramfs"
mkfs.vfat -F 16 ${DISK}p1 -n initramfs
echo "mkfs.vfat -F 16" ${DISK}p1 "-n boot"
mkfs.vfat -F 16 ${DISK}p2 -n boot
echo "mkfs.ext4" ${DISK}p3 "-L rootfs"
mkfs.ext4 ${DISK}p3 -L rootfs
echo "mkdir -p /media/initramfs/"
mkdir -p /media/initramfs/
echo "mkdir -p /media/boot/"
mkdir -p /media/boot/
echo "mkdir -p /media/rootfs/"
mkdir -p /media/rootfs/
echo "mount" ${DISK}p1 "/media/initramfs/"
mount ${DISK}p1 /media/initramfs/
echo "mount" ${DISK}p2 "/media/boot/"
mount ${DISK}p2 /media/boot/
echo "mount" ${DISK}p3 "/media/rootfs/"
mount ${DISK}p3 /media/rootfs/
echo "cp -v zImage /media/initramfs/"
cp -v zImage /media/initramfs/
echo "cp -v imx7d-tsg.dtb  /media/initramfs/"
cp -v imx7d-tsg.dtb /media/initramfs/
echo "cp -v initramfs-debug-image-imx7dsmarc.cpio.gz.u-boot /media/initramfs/"
cp -v initramfs-debug-image-imx7dsmarc.cpio.gz.u-boot /media/initramfs/
echo "cp -v uEnv.txt  /media/initramfs/"
cp -v uEnv.txt /media/initramfs/
echo "cp -v zImage /media/boot/"
cp -v zImage /media/boot/
echo "mkdir -p /media/boot/dtbs"
mkdir -p /media/boot/dtbs
echo "cp -v imx7d-tsg.dtb /media/boot/dtbs/"
cp -v imx7d-tsg.dtb /media/boot/dtbs/
echo "tar jvxf /fsl-image-validation-imx-imx7dsmarc.tar.bz2 -C /media/rootfs/"
tar jvxf /fsl-image-validation-imx-imx7dsmarc.tar.bz2 -C /media/rootfs/
echo "sync"
sync
echo "umount /media/initramfs"
umount /media/initramfs
echo "umount /media/boot"
umount /media/boot
echo "umount /media/rootfs"
umount /media/rootfs
