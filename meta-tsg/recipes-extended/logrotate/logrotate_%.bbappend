FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI += "file://logrotate.timer \
            file://logrotate.conf"

do_install_append () {
    install -d ${D}${systemd_system_unitdir}
    install -d ${D}${sysconfdir}
    install -d ${D}${sysconfdir}/logrotate.d
    install -m 0644 ${WORKDIR}/logrotate.timer ${D}${systemd_system_unitdir}/logrotate.timer
    install -m 0644 ${WORKDIR}/logrotate.conf ${D}${sysconfdir}/logrotate.conf
}
