FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI += "file://imx7d-tsg.dts \
            file://imx7dtsg_defconfig \
            " 

addtask copy_defconfig1 after do_preconfigure before do_compile
do_copy_defconfig1 () {
	cp ${WORKDIR}/imx7dtsg_defconfig ${B}/.config
	cp ${WORKDIR}/imx7dtsg_defconfig ${B}/../defconfig
}

do_compile_prepend() {
	cp ${WORKDIR}/imx7d-tsg.dts ${S}/arch/${ARCH}/boot/dts
}
