DESCRIPTION = "Kernel module for TSG  BSP"
SECTION = "bsp"
LICENSE = "CLOSED"
LIC_FILES_CHKSUM = ""

TARGET_CFLAGS += " -D TSG"

SRC_URI = "file://devman_core.c \
           file://spi_fpga/spi_fpga_mod.c \
           file://spi_flash/spi_flash_mod.c \
           file://gpio/gpio_mod.c \
           file://board/tsg_mod.c\
           file://devman_mod.h\
           file://spi_mod.h\
           file://gpio_mod.h\
           file://Makefile \
"
inherit module
S = "${WORKDIR}"

do_install() {
	install -d ${D}/lib/modules/${KERNEL_VERSION}/drivers/tsg/
	install -m 0644 *.ko ${D}/lib/modules/${KERNEL_VERSION}/drivers/tsg/
}



