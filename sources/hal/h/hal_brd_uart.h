/********************************************************************************/
/**
 * @file hal_brd_uart.h
 * @brief HAL UART interface
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#ifndef _HAL_BRD_UART_H
#define _HAL_BRD_UART_H

#include <ccl_types.h>

typedef enum {
    HAL_UART_CLI_PORT,
    HAL_UART_PORT_MAX
} hal_uart_port_t;

/** UART properties enumeration */
typedef enum {
    HAL_UART_BAUD_RATE = 1,
    HAL_UART_PARITY,
    HAL_UART_STOP_BIT,
    HAL_UART_DATA_LEN
} hal_uart_property_t;


/**
 * @brief Set UART property 
 * @param[in] port - serial port
 * @param[in] property - serial port property
 * @param[in] val - property value
 * @return CCL_OK on success, error code on failure 
 * @note currently only CLI port is supported 
 */
ccl_err_t hal_brd_uart_property_set(hal_uart_port_t port, hal_uart_property_t property, b_u32 val);

/**
 * @brief Get UART property
 * @param[in] port - serial port
 * @param[in] property - uart property
 * @param[out] pval - property value
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t hal_brd_uart_property_get(hal_uart_port_t port, hal_uart_property_t property, b_u32 *pval);

#endif /*_HAL_BRD_UART_H*/
