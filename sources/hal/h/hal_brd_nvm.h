/********************************************************************************/
/**
 * @file hal_brd_nvm.h
 * @brief HAL NVM (Non Volatile Memory) interface
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#ifndef _HAL_BRD_NVM_H
#define _HAL_BRD_NVM_H

/** EEPROMs enumerator */
typedef enum {
    HAL_NVM_MAIN_BRD,
    HAL_NVM_ATP_CARD,
    HAL_NVM_APP_CPU_1,
    HAL_NVM_APP_CPU_2,
    HAL_NVM_APP_CPU_3,
    HAL_NVM_APP_CPU_4,
    HAL_NVM_PS_1,
    HAL_NVM_PS_2,
    HAL_NVM_MAX_NUM
} hal_nvm_dev_t;

/** NVM parameters names */
typedef enum {
	HAL_NVM_NAME_BOARD_SN,         /**< Board Serial Number */
	HAL_NVM_NAME_ASSEMBLY_NUM,     /**< Assembly Number */
	HAL_NVM_NAME_HW_REV,           /**< Hardware Revision */
	HAL_NVM_NAME_HW_SUB_REV,       /**< Hardware Subrevision */
	HAL_NVM_NAME_PART_NUM,         /**< Part Number */
	HAL_NVM_NAME_CLEI,             /**< CLEI */
	HAL_NVM_NAME_MFG_DATE,         /**< Manufacturing Date */
    HAL_NVM_NAME_LICENSE,          /**< License value */
    HAL_NVM_NAME_DEV_TYPE_ID,      /**< Device type ID */
	HAL_NVM_NAME_LAST              /**< Should remain last */
} hal_nvm_param_names_t;

/** NVM parameters types */
typedef enum {
	HAL_NVM_TYPE_BYTE,
    HAL_NVM_TYPE_CHAR,
	HAL_NVM_TYPE_I16,
    HAL_NVM_TYPE_U16,
	HAL_NVM_TYPE_I32,
    HAL_NVM_TYPE_U32,
    HAL_NVM_TYPE_I64,
    HAL_NVM_TYPE_U64,
	HAL_NVM_TYPE_STRING,
	HAL_NVM_TYPE_BIN,
	HAL_NVM_TYPE_LAST,/* Should remain last */
} hal_nvm_param_types_t;

/**
 * @brief Get number of NVM parameters 
 * @param[in]  nvm - NVM device index 
 * @param[out]  pnum - number of parameters
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t hal_brd_nvm_num_of_fields_get(hal_nvm_dev_t nvm,  b_u32 *pnum);

/**
 * @brief Get storage type of NVM field
 * @param[in]  nvm - reference NVM module index 
 * @param[in] field - NVM field index 
 * @param[out]  ptype - type of referenced field
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t hal_brd_nvm_field_type_get(hal_nvm_dev_t nvm,  hal_nvm_param_names_t field,  hal_nvm_param_types_t *ptype);

/**
 * @brief Get maximum length of NVM field
 * @param[in]  nvm - NVM device index 
 * @param[in] field - NVM field index 
 * @param[out]  plen - maximum length of NVM field
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t hal_brd_nvm_field_size_get(hal_nvm_dev_t nvm,  hal_nvm_param_names_t field,  b_u32 *plen);

/**
 * @brief Get contents of NVM field
 * @param[in]  nvm - reference NVM module index 
 * @param[in] field - NVM field index 
 * @param[out]  pbuf - buffer for returned value
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t hal_brd_nvm_field_get(hal_nvm_dev_t nvm,  hal_nvm_param_names_t field,  void *pbuf);

/**
 * @brief Set contents of NVM field
 * @param[in]  nvm - reference NVM module index 
 * @param[in] field - NVM field index 
 * @param[in]  pbuf - buffer for written value
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t hal_brd_nvm_field_set(hal_nvm_dev_t nvm,  hal_nvm_param_names_t field,  void *pbuf);


#endif /*_HAL_BRD_NVM_H*/
