/********************************************************************************/
/**
 * @file hal_brd_power_seq.h
 * @brief HAL power sequencer interface
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#ifndef _HAL_BRD_POWER_SEQ_H
#define _HAL_BRD_POWER_SEQ_H

#include <ccl_types.h>

#define HAL_PSEQ_VOLTAGES 8

/** structure representing power sequencer monitored voltages */
typedef struct {
    b_u8 vmon[HAL_PSEQ_VOLTAGES]
} hal_pseq_voltages_t;


/**
 * @brief Get monitored voltages of power sequencer
 * @param[out] pv - monitored voltages
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t hal_brd_pseq_voltages_read(hal_pseq_voltages_t *pv);

#endif /*_HAL_BRD_POWER_SEQ_H*/
