/********************************************************************************/
/**
 * @file hal_brd_clear_switch.h
 * @brief HAL clear switch interface
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#ifndef _HAL_BRD_CLEAR_SWITCH_H
#define _HAL_BRD_CLEAR_SWITCH_H

#include <ccl_types.h>


/** clear contact switches enumeration */
typedef enum {
    HAL_CLEAR_SWITCH_EMERGENCY = 1,
    HAL_CLEAR_SWITCH_CLR,
    HAL_CLEAR_SWITCH_INVALID
} hal_clear_switch_t;


/**
 * @brief Read value of input clear switch
 * @param[in] cswitch - clear contact switch 
 * @param[out] pval - retrieved value 
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t hal_brd_clear_switch_get(hal_clear_switch_t cswitch, b_u32 *pval);

/**
 * @brief Set led of input clear switch
 * @param[in] cswitch - clear contact switch 
 * @param[out] ledstate - led's value (on/off) 
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t hal_brd_clear_switch_led_set(hal_clear_switch_t cswitch, b_bool ledstate);

#endif /*_HAL_BRD_CLEAR_SWITCH_H*/
