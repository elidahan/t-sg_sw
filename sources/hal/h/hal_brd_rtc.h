/********************************************************************************/
/**
 * @file hal_brd_rtc.h
 * @brief HAL RTC interface
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#ifndef _HAL_BRD_RTC_H
#define _HAL_BRD_RTC_H

#include <ccl_types.h>

/** RTC devices enumeration */
typedef enum {
    HAL_RTC_MAIN_BRD,
    HAL_RTC_ATP_CARD_1,
    HAL_RTC_ATP_CARD_2,
    HAL_RTC_MAX_NUM
} hal_rtc_device_t;

/** RTC battery statuses enumeration */
typedef enum {
    HAL_RTC_BATTERY_OK,
    HAL_RTC_BATTERY_LOW
} hal_rtc_battery_status_t;

/**< RTC time management structure*/
typedef struct {
    b_u16 year;
    b_u8 month;
    b_u8 date;
    b_u8 hours;
    b_u8 minutes;
    b_u8 seconds;
    b_u8 tens_of_secs;
    b_u8 hundreds_of_secs;
} hal_rtc_time_t;

/**
 * @brief  Set time for specific RTC device
 * @param[in] dev - RTC device 
 * @param[in] time - written time
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t hal_brd_rtc_clock_set(hal_rtc_device_t dev, hal_rtc_time_t time);

/**
 * @brief  Get time from specific RTC device
 * @param[in] dev - RTC device 
 * @param[out] ptime - read time 
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t hal_brd_rtc_clock_get(hal_rtc_device_t dev, hal_rtc_time_t *ptime);

/**
 * @brief  Write n bytes sequentially starting from specific 
 *         address in user RAM
 * @param[in] dev - RTC device 
 * @param[in] addr - starting address 
 * @param[in] buf - written data 
 * @param[in] len - the lenght of written data 
 *  
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t hal_brd_rtc_mem_write(hal_rtc_device_t dev, b_u8 addr, b_u8 *buf, b_u16 len);

/**
 * @brief  Read n bytes sequentially starting from specific 
 *         address in user RAM
 * @param[in] dev - RTC device 
 * @param[in] addr - starting address 
 * @param[out] buf - read data 
 * @param[in] len - the lenght of read data 
 *  
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t hal_brd_rtc_mem_read(hal_rtc_device_t dev, b_u8 addr, b_u8 *buf, b_u16 len);

/**
 * @brief  Clear user RAM memory
 * @param[in] dev - RTC device 
 *  
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t hal_brd_rtc_mem_clear(hal_rtc_device_t dev);

/**
 * @brief  Get battery status of RTC device
 * @param[in] dev - RTC device 
 * @param[out] pstatus - battery status 
 *  
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t hal_brd_rtc_battery_status_get(hal_rtc_device_t dev, hal_rtc_battery_status_t *pstatus);

#endif /*_HAL_BRD_RTC_H*/
