/********************************************************************************/
/**
 * @file hal_brd_power_supply.h
 * @brief HAL power supply interface
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#ifndef _HAL_BRD_POWER_SUPPLY_H
#define _HAL_BRD_POWER_SUPPLY_H

#include <ccl_types.h>

#define HAL_PS_FANS_NUM 2

/** power supply types enumeration */
typedef enum {
    HAL_PS_TYPE_UNKNOWN,
    HAL_PS_TYPE_AC,
    HAL_PS_TYPE_DC
} hal_ps_type_t;

/** available modules inside power supply enumeration */
typedef enum {
    HAL_PS_STATUS_AVAIL_POWER = 1,
    HAL_PS_STATUS_AVAIL_FAN
} hal_ps_avail_module_t;

/** power supply status enumeration */
typedef enum {
    HAL_PS_STATUS_OK,
    HAL_PS_STATUS_FAIL,
    HAL_PS_STATUS_ABSENT
} hal_ps_status_e;

/** power supply output voltage status enumeration */
typedef enum {
    HAL_PS_VOUT_OVLT_FAULT = 1,   /**< Vout over voltage fault */
    HAL_PS_VOUT_OVLT_WARN,        /**< Vout over voltage warning */
    HAL_PS_VOUT_UVLT_FAULT,       /**< Vout under voltage fault */
    HAL_PS_VOUT_UVLT_WARN         /**< Vout under voltage warning */
} hal_ps_vout_status_t;

/** power supply output current status enumeration */
typedef enum {
    HAL_PS_IOUT_OCRNT_FAULT = 1,  /**< Iout over current fault */
    HAL_PS_IOUT_OCRNT_WARN        /**< Iout over current warning */
} hal_ps_iout_status_t;

/** power supply output power status enumeration */
typedef enum {
    HAL_PS_POUT_OPWR_FAULT = 1,   /**< Pout over power fault */
    HAL_PS_POUT_OPWR_WARN         /**< Pout over power warning */
} hal_ps_pout_status_t;

/** power supply input power status enumeration */
typedef enum {
    HAL_PS_PIN_OPWR_WARN = 1,    /**< Pin over power warning */
} hal_ps_pin_status_t;

/** power supply temperature status enumeration */
typedef enum {
    HAL_PS_TEMP_OVER_FAULT = 1,   /**< Over temperature fault */
    HAL_PS_TEMP_OVER_WARN,        /**< Over temperature warning */
    HAL_PS_TEMP_UNDER_FAULT,      /**< Under temperature fault */
    HAL_PS_TEMP_UNDER_WARN        /**< Under temperature warning */
} hal_ps_temp_status_t;

/** power supply fans status enumeration */
typedef enum {
    HAL_PS_FAN_FAULT = 1,         /**< Fan fault */
    HAL_PS_FAN_WARN               /**< Fan warning */
} hal_ps_fan_status_t;

/** power supply status structure */
typedef struct {
    hal_ps_type_t           type;                        /**< Type of power supply (AC/DC) */ 
    hal_ps_status_e         ps_status;                   /**< Power Supply overall status (Logical OR of statuses of its entities) */
    hal_ps_vout_status_t    vout_status;                 /**< Indicate status of output voltage of power supply */
    hal_ps_iout_status_t    iout_status;                 /**< Indicate status of output current of power supply */
    hal_ps_pout_status_t    pout_status;                 /**< Indicate status of output power of power supply */
    hal_ps_pin_status_t     pin_status;                  /**< Indicate status of input power of power supply */
    hal_ps_temp_status_t    temp_status;                 /**< Indicate status of temperature of power supply */
    hal_ps_fan_status_t     fan_status[HAL_PS_FANS_NUM]; /**< Indicate status of fans of power supply */
} hal_ps_status_t;

/**
 * @brief Get number of PS units
 * @param[out] pnum - number of PS units
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t hal_brd_ps_number_of_psu_get(int *pnum);

/**
 * @brief Get PS's data: type, list of available state 
 *        parameters and main operative status of PS
 * @param[in] psu - PS type 
 * @param[out] pstatus - PS status 
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t hal_brd_ps_status_get(hal_ps_type_t psu,  hal_ps_status_t *pstatus);


/**
 * @brief Get actual measured output voltage in volts
 * @param[in] psu - PS type 
 * @param[out] vout - measured output voltage 
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t hal_brd_ps_vout_get(hal_ps_type_t psu, b_i32 *vout);

/**
 * @brief Get actual measured output current in amperes
 * @param[in] psu - PS type 
 * @param[out] iout - measured output current 
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t hal_brd_ps_iout_get(hal_ps_type_t psu, b_i32 *iout);

/**
 * @brief Get output power in watts
 * @param[in] psu - PS type 
 * @param[out] pout - output power 
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t hal_brd_ps_pout_get(hal_ps_type_t psu, b_i32 *pout);

/**
 * @brief Get input power in watts
 * @param[in] psu - PS type 
 * @param[out] pin - input power 
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t hal_brd_ps_pin_get(hal_ps_type_t psu, b_i32 *pin);

/**
 * @brief Get temperature in degree Celsius
 * @param[in] psu - PS type 
 * @param[out] temp - temperature 
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t hal_brd_ps_temperature_get(hal_ps_type_t psu, b_i32 *temp);

/**
 * @brief Get speed of the fan
 * @param[in] psu - PS type 
 * @param[in] fan - fan number 
 * @param[out] speed - fan speed 
 * @return CCL_OK on success, error code on failure 
 * @note 0 <= fan < HAL_PS_FANS_NUM  
 */
ccl_err_t hal_brd_ps_fan_speed_get(hal_ps_type_t psu, b_i32 fan, b_i32 *speed);

#endif /*_HAL_BRD_POWER_SUPPLY_H*/
