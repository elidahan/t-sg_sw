/********************************************************************************/
/**
 * @file hal_brd_port.h
 * @brief HAL ports interface
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#ifndef _HAL_BRD_PORT_H
#define _HAL_BRD_PORT_H

/**
 * @brief Initialize the port given by index (logical port) 
 * @param[in] logport - port index
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t hal_brd_port_init(b_u32 logport);

/**
 * @brief Get number of the ports in the system 
 * @param[out]  pports - number of ports
 * @return CCL_OK on success, error code on failure 
 * @note number of the ports includes both front panel and 
 *       inband ports. Ports between 2 devices are counted as
 *       separate ports
 */
ccl_err_t hal_brd_port_number_of_ports_get(b_u32 *pports);

/**
 * @brief Get specific counter of the port of the specific 
 *        device
 * @param[in] port - port index 
 * @param[in] dcounter - type of the counter 
 * @param[out] pcounter - counter's value
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t hal_brd_port_counter_get(b_u32 logport, devman_if_counter_t dcounter, 
                                   b_u64 *pcounter);

/**
 * @brief Clear specific counter of the port of the specific 
 *        device
 * @param[in] port - port index 
 * @param[in] dcounter - type of the counter 
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t hal_brd_port_counter_clear(b_u32 logport, devman_if_counter_t dcounter);

/**
 * @brief Set Tx enable/disable state of the port of the 
 *        specific device
 * @param[in] port - port index 
 * @param[in] state - Tx state 
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t hal_brd_port_tx_state_set(b_u32 logport, b_bool state);

/**
 * @brief Get Tx enable/disable state of the port of the 
 *        specific device
 * @param[in] port - port index 
 * @param[out] pstate - Tx state 
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t hal_brd_port_tx_state_get(b_u32 logport, b_bool *pstate);

/**
 * @brief Get port's link status
 * @param[in] port - port index 
 * @param[out] pstatus - link status
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t hal_brd_port_link_status_get(b_u32 logport, 
                                       devman_if_link_status_t *pstatus);

/**
 * @brief Control port's led (on/off)
 * @param[in] port - port index 
 * @param[in] state - led state
 * @return CCL_OK on success, error code on failure 
 * @note Only front panel ports are relevant 
 */
ccl_err_t hal_brd_port_led_state_set(b_u32 logport,  b_bool state);


/**
 * @brief Retrieve eeprom data from the transceiver connected to
 *        the specific port
 * @param[in] port - port index 
 * @param[in] pmedia_param - pointer to media param structure
 * @return CCL_OK on success, error code on failure 
 * @note Only front panel ports are relevant 
 */
ccl_err_t hal_brd_port_media_params_get(b_u32 logport, 
                                        devman_media_param_t *pmedia_param);

/**
 * @brief Retrieve eeprom data from the transceiver connected to
 *        the specific port and serialize it in a buffer as key
 *        - value pairs
 * @param[in] port - port index 
 * @param[in] pbuf - pointer to a buffer holding serialized data 
 * @param[in] bufsize - buffer size 
 * @return CCL_OK on success, error code on failure 
 * @note Only front panel ports are relevant. Buffer size should 
 *       be 1KB
 */
ccl_err_t hal_brd_port_media_details_get(b_u32 logport, b_u8 *pbuf, 
                                         b_u32 bufsize);

/**
 * @brief Set specific property of the port of the specific 
 *        device
 * Properties are complied with RFC8343 - A YANG Data Model for 
 * Interface Management 
 * @param[in] dev - device the port belongs to 
 * @param[in] port - port index 
 * @param[in] dprop - property to be configured
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t hal_brd_port_property_set(b_u32 logport, devman_if_property_t dprop, 
                                    void *pdata, b_u32 size);

/**
 * @brief Get specific property of the port of the specific 
 *        device
 * Properties are complied with RFC8343 - A YANG Data Model for 
 * Interface Management 
 * @param[in] port - port index 
 * @param[out] dprop - the value of the property
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t hal_brd_port_property_get(b_u32 logport, devman_if_property_t dprop, 
                                    void *pdata, b_u32 size);

#endif /*_HAL_BRD_PORT_H*/
