/********************************************************************************/
/**
 * @file hal_brd_spi_mux.h
 * @brief HAL SPI MUX interface
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#ifndef _HAL_BRD_SPI_MUX_H
#define _HAL_BRD_SPI_MUX_H

#include <ccl_types.h>


/** SPI MUX serial flashes enumeration */
typedef enum {
    HAL_SPI_MUX_MAIN_CPU = 1,
    HAL_SPI_MUX_CTRL_CPU,
    HAL_SPI_MUX_APP_CPU_1,
    HAL_SPI_MUX_APP_CPU_2,
    HAL_SPI_MUX_APP_CPU_3,
    HAL_SPI_MUX_APP_CPU_4,
    HAL_SPI_MUX_CPLD_FLASH_1,
    HAL_SPI_MUX_CPLD_FLASH_2,
} hal_spi_mux_dest_t;


/**
 * @brief Set SPI MUX destination
 * @param[in] dest - spi mux destination
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t hal_brd_spi_mux_dest_set(hal_spi_mux_dest_t dest);

#endif /*_HAL_BRD_SPI_MUX_H*/
