/********************************************************************************/
/**
 * @file hal_brd_trans.h
 * @brief HAL Transceivers interface
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#ifndef _HAL_BRD_TRANS_H
#define _HAL_BRD_TRANS_H

typedef enum { 
    HAL_TRANS_TYPE_NOT_INSTALLED             = 1,
    HAL_TRANS_TYPE_UNKNOWN                   = 2,
    HAL_TRANS_TYPE_1000BASE_CX_MM_SFP        = 3,
    HAL_TRANS_TYPE_1000BASE_CX_SM_SFP        = 4,
    HAL_TRANS_TYPE_1000BASE_T_SFP_COPPER_MR  = 5,
    HAL_TRANS_TYPE_1000BASE_T_SFP_COPPER_SR  = 6,
    HAL_TRANS_TYPE_1000BASE_T_SFP_COPPER_SR_WO_PHY  = 7,
    HAL_TRANS_TYPE_1000BASE_T_SFP_COPPER_DISABLED    = 8,
    HAL_TRANS_TYPE_1000BASE_BX_SM_SFP                = 9,
    HAL_TRANS_TYPE_1000BASE_BX_MM_SFP                = 10,
    HAL_TRANS_TYPE_1000BASE_PX_SM_SFP                = 11,
    HAL_TRANS_TYPE_1000BASE_PX_MM_SFP                = 12,
    HAL_TRANS_TYPE_1000BASE_DWDM_SM_SFP              = 13,
    HAL_TRANS_TYPE_1000BASE_DWDM_MM_SFP              = 14,
    HAL_TRANS_TYPE_1000BASE_SX_MM_SFP                = 15,
    HAL_TRANS_TYPE_1000BASE_SX_SM_SFP                = 16,
    HAL_TRANS_TYPE_1000BASE_LX_SM_SFP                = 17,
    HAL_TRANS_TYPE_1000BASE_LX_MM_SFP                = 18,
    HAL_TRANS_TYPE_1000BASE_XD_SM_SFP                = 19,     
    HAL_TRANS_TYPE_1000BASE_XD_MM_SFP                = 20,
    HAL_TRANS_TYPE_1000BASE_ZX_SM_SFP                = 21,
    HAL_TRANS_TYPE_1000BASE_ZX_MM_SFP                = 22,
    HAL_TRANS_TYPE_1000BASE_EX_SM_SFP                = 23,
    HAL_TRANS_TYPE_1000BASE_EX_MM_SFP                = 24,
    HAL_TRANS_TYPE_1000BASE_XWDM_SM_SFP              = 25,
    HAL_TRANS_TYPE_1000BASE_XWDM_MM_SFP              = 26,
    HAL_TRANS_TYPE_100BASE_BX_SM_SFP                 = 27,
    HAL_TRANS_TYPE_100BASE_BX_MM_SFP                 = 28,
    HAL_TRANS_TYPE_100BASE_PX_SM_SFP                 = 29,
    HAL_TRANS_TYPE_100BASE_PX_MM_SFP                 = 30,
    HAL_TRANS_TYPE_100BASE_FX_SM_SFP                 = 31,
    HAL_TRANS_TYPE_100BASE_FX_MM_SFP                 = 32,
    HAL_TRANS_TYPE_100BASE_SX_SM_SFP                 = 33,
    HAL_TRANS_TYPE_100BASE_SX_MM_SFP                 = 34,
    HAL_TRANS_TYPE_100BASE_LX_SM_SFP                 = 35,
    HAL_TRANS_TYPE_100BASE_LX_MM_SFP                 = 36,
    HAL_TRANS_TYPE_100BASE_XD_SM_SFP                 = 37,     
    HAL_TRANS_TYPE_100BASE_XD_MM_SFP                 = 38,
    HAL_TRANS_TYPE_100BASE_ZX_SM_SFP                 = 39,
    HAL_TRANS_TYPE_100BASE_ZX_MM_SFP                 = 40,
    HAL_TRANS_TYPE_100BASE_EX_SM_SFP                 = 41,
    HAL_TRANS_TYPE_100BASE_EX_MM_SFP                 = 42,
    HAL_TRANS_TYPE_100BASE_XWDM_SM_SFP               = 43,
    HAL_TRANS_TYPE_100BASE_XWDM_MM_SFP               = 44,
    HAL_TRANS_TYPE_10G_BASE_SR_MM_XFP                = 45,
    HAL_TRANS_TYPE_10G_BASE_LR_MM_XFP                = 46,
    HAL_TRANS_TYPE_10G_BASE_LRM_MM_XFP               = 47,
    HAL_TRANS_TYPE_10G_BASE_LR_SM_XFP                = 48,
    HAL_TRANS_TYPE_10G_BASE_SR_SM_XFP                = 49,
    HAL_TRANS_TYPE_10G_BASE_LRM_SM_XFP               = 50,
    HAL_TRANS_TYPE_10G_BASE_SX_MM_XFP                = 51,
    HAL_TRANS_TYPE_10G_BASE_SX_SM_XFP                = 52,
    HAL_TRANS_TYPE_10G_BASE_LX_MM_XFP                = 53,
    HAL_TRANS_TYPE_10G_BASE_LX_SM_XFP                = 54,
    HAL_TRANS_TYPE_10G_BASE_XD_SM_XFP                = 55,     
    HAL_TRANS_TYPE_10G_BASE_XD_MM_XFP                = 56,
    HAL_TRANS_TYPE_10G_BASE_ZX_SM_XFP                = 57,
    HAL_TRANS_TYPE_10G_BASE_ZX_MM_XFP                = 58,
    HAL_TRANS_TYPE_10G_BASE_EX_SM_XFP                = 59,
    HAL_TRANS_TYPE_10G_BASE_EX_MM_XFP                = 60,
    HAL_TRANS_TYPE_10G_BASE_XWDM_SM_XFP              = 61,
    HAL_TRANS_TYPE_10G_BASE_XWDM_MM_XFP              = 62,
    HAL_TRANS_TYPE_10G_BASE_SX_SM_SFP_PLUS           = 63,
    HAL_TRANS_TYPE_10G_BASE_SX_MM_SFP_PLUS           = 64,
    HAL_TRANS_TYPE_40G_BASE_QSFP_SPI                 = 65,
    HAL_TRANS_TYPE_RJ45                              = 66,
    HAL_TRANS_TYPE_10G_BASE_ZX_SM_SFP_PLUS           = 67,
    HAL_TRANS_TYPE_10G_BASE_ZX_MM_SFP_PLUS           = 68,
    HAL_TRANS_TYPE_10G_BASE_XD_SM_SFP_PLUS           = 69,     
    HAL_TRANS_TYPE_10G_BASE_XD_MM_SFP_PLUS           = 70,
    HAL_TRANS_TYPE_10G_BASE_LX_SM_SFP_PLUS           = 71,     
    HAL_TRANS_TYPE_10G_BASE_LX_MM_SFP_PLUS           = 72,
    HAL_TRANS_TYPE_10G_BASE_DWDM_SM_SFP_PLUS         = 73,
    HAL_TRANS_TYPE_10G_BASE_DWDM_MM_SFP_PLUS         = 74,
	HAL_TRANS_TYPE_MAX                        
} hal_trans_link_type_t;

/** transceiver revision structure */
typedef struct {
	b_u32	                rev_id;
	b_i8					rev_name_short[PORT_REV_NAME_SHORT];
	b_i8					rev_name_long[PORT_REV_NAME_LONG];
} hal_trans_revision_t;

/** transceiver types enumeration   */
typedef enum {
    HAL_TRANS_UNKNOWN,
    HAL_TRANS_GBIC,
    HAL_TRANS_SOLDERED,
    HAL_TRANS_SFP,
    HAL_TRANS_XBI,
    HAL_TRANS_XENPAK,
    HAL_TRANS_XFP,
    HAL_TRANS_XFF,
    HAL_TRANS_XFP_E,
    HAL_TRANS_XPAK,
    HAL_TRANS_X2,
    HAL_TRANS_SFP_PLUS,
    HAL_TRANS_DWDM_SFP,
    HAL_TRANS_QSFP
} hal_trans_transeiver_type_t;

typedef enum {
    HAL_TRANS_EX_UNKNOWN,
    HAL_TRANS_EX_MOD_DEF_1,
    HAL_TRANS_EX_MOD_DEF_2,
    HAL_TRANS_EX_MOD_DEF_3,
    HAL_TRANS_EX_MOD_TWO_WIRE_ID,
    HAL_TRANS_EX_MOD_DEF_5,
    HAL_TRANS_EX_MOD_DEF_6,
    HAL_TRANS_EX_MOD_DEF_7
} hal_trans_extended_type_t;

typedef enum {
    HAL_TRANS_CONNECTOR_UNKNOWN,
    HAL_TRANS_CONNECTOR_SC,
    HAL_TRANS_CONNECTOR_FCS1_COPPER,
    HAL_TRANS_CONNECTOR_FCS2_COPPER,
    HAL_TRANS_CONNECTOR_BNC_TNC,
    HAL_TRANS_CONNECTOR_FC_COAX,
    HAL_TRANS_CONNECTOR_FIBER_JACK,
    HAL_TRANS_CONNECTOR_LC,
    HAL_TRANS_CONNECTOR_MT_RJ,
    HAL_TRANS_CONNECTOR_MU,
    HAL_TRANS_CONNECTOR_SG,
    HAL_TRANS_CONNECTOR_OPTICAL_PIGTAIL,
    HAL_TRANS_CONNECTOR_MPO_PARALLEL_OPTIC,
    HAL_TRANS_CONNECTOR_HSSDC,
    HAL_TRANS_CONNECTOR_COPPER_PIGTAIL,
    HAL_TRANS_CONNECTOR_RJ45,
    HAL_TRANS_CONNECTOR_LAST
} hal_trans_connector_type_t;

typedef enum {
    HAL_TRANS_10G_UNKNOWN,
    HAL_TRANS_10G_SR,
    HAL_TRANS_10G_LR,
    HAL_TRANS_10G_LRM
} hal_trans_10g_compliance_t;

typedef enum {
    HAL_TRANS_INFINIBAND_UNKNOWN,
    HAL_TRANS_INFINIBAND_1X_COPPER_PASSIVE,
    HAL_TRANS_INFINIBAND_1X_COPPER_ACTIVE,
    HAL_TRANS_INFINIBAND_1X_LX,
    HAL_TRANS_INFINIBAND_1X_SX
} hal_trans_infiniband_compliance_t;

typedef enum {
    HAL_TRANS_ESCON_UNKNOWN,
    HAL_TRANS_ESCON_SMF,
    HAL_TRANS_ESCON_MMF
} hal_trans_escon_compliance_t;

typedef enum {
    HAL_TRANS_SONET_UNKNOWN,
    HAL_TRANS_SONET_OC3_SHORT,
    HAL_TRANS_SONET_OC3_SM_INTER,
    HAL_TRANS_SONET_OC3_SM_LONG,
    HAL_TRANS_SONET_OC12_SHORT,
    HAL_TRANS_SONET_OC12_SM_INTER,
    HAL_TRANS_SONET_OC12_SM_LONG,
    HAL_TRANS_SONET_OC48_SHORT,
    HAL_TRANS_SONET_OC48_SM_INTER,
    HAL_TRANS_SONET_OC48_SM_LONG,
    HAL_TRANS_SONET_SPEC_BIT1,
    HAL_TRANS_SONET_SPEC_BIT2,
    HAL_TRANS_SONET_192_SHORT,
    HAL_TRANS_SONET_OC3_INTER_SR1_IR1_LR1
} hal_trans_sonet_compliance_t;

typedef enum {
    HAL_TRANS_ETH_UNKNOWN,
    HAL_TRANS_ETH_1000SX,
    HAL_TRANS_ETH_1000LX,
    HAL_TRANS_ETH_1000CX,
    HAL_TRANS_ETH_1000T,
    HAL_TRANS_ETH_100LX,
    HAL_TRANS_ETH_100FX,
    HAL_TRANS_ETH_BX10,
    HAL_TRANS_ETH_PX
} hal_trans_ethernet_compliance_t;

typedef enum {
    HAL_TRANS_FCL_UNKNOWN,
    HAL_TRANS_FCL_MEDIUM,
    HAL_TRANS_FCL_LONG,
    HAL_TRANS_FCL_INTERMEDIATE,
    HAL_TRANS_FCL_SHORT,
    HAL_TRANS_FCL_VERY_LONG
} hal_trans_fibre_chnl_length_t;

typedef enum {
    HAL_TRANS_FCT_UNKNOWN,
    HAL_TRANS_FCT_COPPER_FC_BASE_T,
    HAL_TRANS_FCT_COPPER_PASSIVE,
    HAL_TRANS_FCT_COPPER_ACTIVE,
    HAL_TRANS_FCT_LONGWAVE_LL,
    HAL_TRANS_FCT_SHORTWAVE_W_OFC_SL,
    HAL_TRANS_FCT_SHORTWAVE_WO_OFC_SN,
    HAL_TRANS_FCT_ELECTRIC_INTRA_ENCL_EL,
    HAL_TRANS_FCT_ELECTRIC_INTER_ENCL_EL,
    HAL_TRANS_FCT_LONGWAVE_LC,
    HAL_TRANS_FCT_SHORTWAVE_LINEAR_RX_SA
} hal_trans_fibre_chnl_technology_t;

typedef enum {
    HAL_TRANS_FCM_UNKNOWN,
    HAL_TRANS_FCM_SINGLE_SM,
    HAL_TRANS_FCM_MULTI_50UM_M5,
    HAL_TRANS_FCM_MULTI_62_5UM_M6,
    HAL_TRANS_FCM_VIDEO_COAX_TV,
    HAL_TRANS_FCM_MINI_COAX_MI,
    HAL_TRANS_FCM_TWIST_PAIR_TP,
    HAL_TRANS_FCM_TWIN_AX_PAIR_TW
} hal_trans_fibre_chnl_media_t;

typedef enum {
    HAL_TRANS_FCS_UNKNOWN,
    HAL_TRANS_FCS_100,
    HAL_TRANS_FCS_200,
    HAL_TRANS_FCS_400,
    HAL_TRANS_FCS_800,
    HAL_TRANS_FCS_1200,
    HAL_TRANS_FCS_1600
} hal_trans_fibre_chnl_speed_t;

typedef struct {
    hal_trans_10g_compliance_t        g10_compliance;  /* 3-3 B4-7 10GBASE-SR, 10GBASE-LR, 10GBASE-LRM*/
    hal_trans_infiniband_compliance_t infiniband_compliance;  /* 3-3 B0-B3 */
    hal_trans_escon_compliance_t      escon_compliance;  /* 4-4 B6-B7 */
    hal_trans_sonet_compliance_t      sonet_compliance;  /* 4(B0-B5)-5 */
    hal_trans_ethernet_compliance_t   eth_compliance;     /* 6-6 - 1000BASE-T, 1000BASE-CX, etc. */
    hal_trans_fibre_chnl_length_t     fibre_chn_lenfth;   /* 7(B3-B7) */
    hal_trans_fibre_chnl_technology_t fibre_chn_technlgy; /* 7(B0-B2)-8 */
    hal_trans_fibre_chnl_media_t      fibre_chn_media  ; /* 9 */
    hal_trans_fibre_chnl_speed_t      fibre_chn_speed;   /* 10-10 */
} hal_trans_tranceiver_t;

typedef struct {
    b_i32     Year;
    b_i32     Month;
    b_i32     Day;
    b_i32     Lot;
} hal_trans_date_t;

typedef enum {
    HAL_TRANS_ENCODING_UNKNOWN,
    HAL_TRANS_ENCODING_8B_10B,
    HAL_TRANS_ENCODING_4B_5B,
    HAL_TRANS_ENCODING_NRZ,
    HAL_TRANS_ENCODING_MANCHESTER,
    HAL_TRANS_ENCODING_SONET_SCRAMBLED,
    HAL_TRANS_ENCODING_64B_66B
} hal_trans_encoding_t;

typedef enum {
    HAL_TRANS_RATE_ID_UNKNOWN,
    HAL_TRANS_RATE_ID_4_2_1G__AS0_AS1,
    HAL_TRANS_RATE_ID_8_4_2G_RX,
    HAL_TRANS_RATE_ID_8_4_2G_RX_TX,
    HAL_TRANS_RATE_ID_8_4_2G_TX,
} hal_trans_rate_id_t;

typedef struct {
    b_bool      relevance;
    b_bool      status;
} hal_trans_diag_param_type_t;

typedef struct {
    hal_trans_diag_param_type_t      ddm_supported;       /* B6 on 92*/
    hal_trans_diag_param_type_t      intern_calibr;       /* B5 on 92*/
    hal_trans_diag_param_type_t      extern_calibr;       /* B4 on 92*/
    hal_trans_diag_param_type_t      avg_power_measure;    /* B3 on 92*/
    hal_trans_diag_param_type_t      adr_chng_required;    /* B2 on 92*/
    hal_trans_diag_param_type_t      ber_support;         /* B4 on 92*/
} hal_trans_diag_type_t;

/** vendor data describing media parameters */
typedef struct {
    hal_trans_transeiver_type_t  transeiver_type;     /*0-0 - Unknown, GIBIC, etc.*/
    hal_trans_extended_type_t    extended_type;       /*1-1 - MOD_DEF N*/
	hal_trans_connector_type_t   connector_type;	    /* 2-2 - FiberJack, LC, MT-RJ, etc */
    hal_trans_tranceiver_t       tranceiver;         /* 3-10 - Elecric or optical compability */
    hal_trans_encoding_t         encoding;           /* 11-11 */
    hal_trans_rate_id_t          rate_id;             /* 13-13 */
    b_i32                        bit_rate_nom;         /* 12-12 - Nominal signalling rate */
    b_i32                        bitrate_max;         /* 12 & 66-66 - Maximal signalling rate */
    b_i32                        bit_rate_min;         /* 12 & 67-67 - Minimal signalling rate */
    b_i32                        length_smf;          /* 14-15 Length for single mode fiber */
    b_i32                        length50um;         /* 16-16 Length for 50 */
    b_i32                        length62_5um;       /* 17-17 Length for 62.5 um */
    b_i32                        length_copper;       /* 18-18 Length for copper */
    b_i32                        length_om3;          /* 19-19 Length for 50 um OM3 fiber */
	b_i8                         vendor_name[17];	    /* 20-35 SFP vendor name (ASCII) */
	b_i8                         vendor_oui[4];		/* 37-39 SFP vendor IEEE company ID */
	b_i8                         vendor_pn[17];		/* 40-55 Part number provided by SFP vendor (ASCII) */
	b_i8                         vendor_rev[5];		/* 56-59 Revision level for part number provided by vendor (ASCII) */
	b_i8                         vendor_sn[17];		/* 68-83 Serial number provided by vendor (ASCII) */
    hal_trans_date_t             mfg_date;            /* 84-91 Vendor's manufacturing date */
	hal_trans_diag_type_t        diagnostic;		    /* 92-92 Supporting diagnostic parameters */
} hal_trans_description_t;

/** Main struct of medial parameters */
typedef struct {
	hal_trans_link_type_t		link_type;		/* specific transceiver in slot */
	hal_trans_revision_t	    revision; 		/* transmission type ID */
	hal_trans_description_t		description;    /* vendor data describing media parameters */
} hal_trans_param_t;

/** Parameters of treshold */
typedef struct {
    union {
        b_u8	msb;
        b_i8	modulo;
    };
    union {
        b_u8	lsb;
	    b_u8	fraction;
    };
} hal_trans_diag_tresh_value_t;

typedef struct {
    b_bool                          is_applicable;
	hal_trans_diag_tresh_value_t	 hi_alarm;
	hal_trans_diag_tresh_value_t	lo_alarm;
	hal_trans_diag_tresh_value_t	hi_warn;
	hal_trans_diag_tresh_value_t	lo_warn;
} hal_trans_diag_tresh_unit_t;


/** vendor-defined tresholds */
typedef struct {
	hal_trans_diag_tresh_unit_t	    temperature;	/* Temperature treshold */
	hal_trans_diag_tresh_unit_t	    voltage;	    /* Voltage treshold */
	hal_trans_diag_tresh_unit_t  	bias;	        /* Bias treshold */
    hal_trans_diag_tresh_unit_t     tx_power;		/* Tx power treshold */
	hal_trans_diag_tresh_unit_t     rx_power;		/* Rx power treshold */
} hal_trans_diag_tresh_t;


typedef struct {
    hal_trans_diag_tresh_value_t     slope;
    hal_trans_diag_tresh_value_t     offset;
} hal_trans_diag_calib_item_t;


typedef struct {
    b_bool                         is_applicable;
    b_float32                      rx_pwr[5];
    hal_trans_diag_calib_item_t    txi;
    hal_trans_diag_calib_item_t    tx_pwr;
    hal_trans_diag_calib_item_t    temperature;
    hal_trans_diag_calib_item_t    voltage;
} hal_trans_diag_calib_t;


/* Main structure of media digital diagnostic */
typedef struct {
	hal_trans_diag_tresh_t	    threshold;			/* vendor-defined tresholds */
    hal_trans_diag_calib_t      calibration;        /* DDM calibration parameters */
} hal_trans_diag_param_t;


/** Main structure of media digital measured status */
typedef struct {
    hal_trans_diag_tresh_value_t    temperature;
    hal_trans_diag_tresh_value_t    bcc;
    hal_trans_diag_tresh_value_t    tx_bias;
    hal_trans_diag_tresh_value_t    tx_power;
    hal_trans_diag_tresh_value_t    rx_power;
    hal_trans_diag_tresh_value_t    aux1;
    hal_trans_diag_tresh_value_t    aux2;
    b_bool                          tx_disable;
    b_bool                          soft_tx_disable;
    b_bool                          rate_select;
    b_bool                          soft_rate_select;
    b_bool                          tx_fault;
    b_bool                          los;
    b_bool                          data_ready_bar;
} hal_trans_diag_status_t;

/** IDs of media internal PHY's registers*/
typedef enum {
    HAL_TRANS_REG_ID_CONTROL,
    HAL_TRANS_REG_ID_STATUS,
    HAL_TRANS_REG_ID_PHY_IDENTIFIER1,
    HAL_TRANS_REG_ID_PHY_IDENTIFIER2,
    HAL_TRANS_REG_ID_AUTONEG_ADVERTISE,
    HAL_TRANS_REG_ID_LINK_PARTNER_ABILITY,
    HAL_TRANS_REG_ID_AUTONEG_EXPANSION,
    HAL_TRANS_REG_ID_NEXT_PAGE_TX,
    HAL_TRANS_REG_ID_LINK_PARTNER_NEXT_PAGE,
    HAL_TRANS_REG_ID_1000B_T_CONTROL,
    HAL_TRANS_REG_ID_1000B_T_STATUS,
    HAL_TRANS_REG_ID_EXT_STATUS,
    HAL_TRANS_REG_ID_PHY_SPECIFIC_CONTROL,
    HAL_TRANS_REG_ID_PHY_SPECIFIC_STATUS,
    HAL_TRANS_REG_ID_INTERRUPT_ENABLE,
    HAL_TRANS_REG_ID_INTERRUPT_STATUS,
    HAL_TRANS_REG_ID_EXT_PHY_SPECIFIC_CONTROL,
    HAL_TRANS_REG_ID_RX_ERROR_COUNTER,
    HAL_TRANS_REG_ID_EXT_ADDR_CABLE_DIAGNOSTIC,
    HAL_TRANS_REG_ID_LED_CONTROL,
    HAL_TRANS_REG_ID_MANUAL_LED_OVERRIDE,
    HAL_TRANS_REG_ID_EXT_PHY_SPECIFIC_CONTROL2,
    HAL_TRANS_REG_ID_EXT_PHY_SPECIFIC_STATUS,
    HAL_TRANS_REG_ID_CABLE_DIAGNOSTIC,
    HAL_TRANS_REG_ID_LAST_REGISTER  /* This must be the last */
} hal_trans_reg_id_t;


/**
 * @brief Check if transceiver is present for specific port of 
 *        specific device
 * @param[in] dev - device the port belongs to 
 * @param[in] port - port index 
 * @return CCL_TRUE transceiver is present, CCL_FALSE - 
 *         otherwise
 */
b_bool hal_brd_trans_is_present(devman_dev_t dev, int port);

/**
 * @brief Retrieve transceiver parameters for specific port of
 *        specific device
 * @param[in] dev - device the port belongs to 
 * @param[in] port - port index 
 * @param[out] pparam - transceiver parameters
 * @return CCL_TRUE transceiver is present, CCL_FALSE - 
 *         otherwise
 */
ccl_err_t hal_brd_trans_data_get(devman_dev_t dev, int port, hal_trans_param_t *pparam);

/**
 * @brief Set transceiver parameters for specific port of
 *        specific device
 * @param[in] dev - device the port belongs to 
 * @param[in] port - port index 
 * @param[in] pparam - transceiver parameters
 * @return CCL_TRUE transceiver is present, CCL_FALSE - 
 *         otherwise
 * @note Currently supported dwdm wave length settings, dmm 
 *       thresholds settings
 */
ccl_err_t hal_brd_trans_data_set(devman_dev_t dev, int port, hal_trans_param_t *pparam);

#endif /*_HAL_BRD_TRANS_H*/
