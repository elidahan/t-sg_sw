/********************************************************************************/
/**
 * @file hal_brd_dry_contact.h
 * @brief HAL dry contact interface
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#ifndef _HAL_BRD_DRY_CONTACT_H
#define _HAL_BRD_DRY_CONTACT_H

#include <ccl_types.h>


/** dry contact values enumeration */
typedef enum {
    HAL_DRY_CONTACT_RESET = 1,
    HAL_DRY_CONTACT_CLEAR,
    HAL_DRY_CONTACT_FACTORY_DEFAULT,
    HAL_DRY_CONTACT_INVALID
} hal_dry_contact_val_t;


/**
 * @brief Read value of input dry contacts
 * @param[out] pdcontact - dry contact value
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t hal_brd_dry_contact_get(hal_dry_contact_val_t *pdcontact);

#endif /*_HAL_BRD_DRY_CONTACT_H*/
