/********************************************************************************/
/**
 * @file hal_brd_sensors.h
 * @brief HAL sensors interface
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#ifndef _HAL_BRD_SENSOR_H
#define _HAL_BRD_SENSOR_H

#include <ccl_types.h>

/** sensor types enumeration */
typedef enum {
    HAL_SNSR_TYPE_VOLTAGE,
    HAL_SNSR_TYPE_TEMPERATURE,
    HAL_SNSR_MAX_TYPE
} hal_sensor_type_t;

/** sensor status enumeration */
typedef enum {
    HAL_SNSR_OK,
    HAL_SNSR_FAIL
}hal_sensor_status_t;

/** sensor operational state */
typedef enum {
    HAL_SNSR_ON,
    HAL_SNSR_OFF
}hal_sensor_op_state_t;

/** temperature sensors types enumeration */
typedef enum {
    HAL_SNSR_TEMP_CPU = 1,
    HAL_SNSR_TEMP_PS,
    HAL_SNSR_TEMP_FAN,
    HAL_SNSR_TEMP_MAX_TYPE,
} hal_temp_sensor_t;


/**
 * @brief Get number of sensors of specified type
 * @param[in]  type - specified type 
 * @param[out] pnum - number of sensors
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t hal_brd_sens_number_of_sensors_get(hal_sensor_type_t type,  int *pnum);

/**
 * @brief Get specified onboard voltage status
 * @param[in]  sensor - sensor index
 * @param[out] pstatus - voltage status
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t hal_brd_sens_voltage_status_get(int sensor,  hal_sensor_status_t pstatus);

/**
 * @brief Get temperature from specified sensor (value)
 * @param[in]  sensor - sensor index
 * @param[out] ptemp - temperature
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t hal_brd_sens_temperature_get(int sensor,  b_i32 *ptemp);

/**
 * @brief Get power from specified sensor (value)
 * @param[in]  sensor - sensor index
 * @param[out] ppower - power
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t hal_brd_sens_power_get(int sensor,  b_i32 *ppower);

/**
 * @brief Get operational state of the sensor 
 * @param[in] sensor - sensor index 
 * @param[out] pstate - operational state
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t hal_brd_sens_op_state_get(int sensor, hal_sensor_op_state_t *pstate);


#endif /*_HAL_BRD_SENSOR_H*/
