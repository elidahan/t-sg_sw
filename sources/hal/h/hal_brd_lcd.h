/********************************************************************************/
/**
 * @file hal_brd_lcd.h
 * @brief HAL LCD interface
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#ifndef _HAL_BRD_LCD_H
#define _HAL_BRD_LCD_H

#include <ccl_types.h>

#define HAL_LCD_MAX_STRING_LEN 80
#define HAL_LCD_MAX_ROWS 4
#define HAL_LCD_MAX_COLUMNS 20

/** LCD components enumeration */
typedef enum {
    HAL_LCD_DISPLAY,
    HAL_LCD_KEYPAD
} hal_lcd_component_t;

/** backlight settings enumeration */
typedef enum {
    HAL_LCD_BACKLIGHT_OFF,
    HAL_LCD_BACKLIGHT_ON,
    HAL_LCD_BACKLIGHT_AUTO
} hal_lcd_backlight_t;

/** cursor direction enumeration */
typedef enum {
    HAL_LCD_CURSOR_UP,
    HAL_LCD_CURSOR_DOWN,
    HAL_LCD_CURSOR_LEFT,
    HAL_LCD_CURSOR_RIGHT
} hal_lcd_cursor_direction_t;

/** available leds enumeration */
typedef enum {
    HAL_LCD_LED_1 = 1,
    HAL_LCD_LED_2,
    HAL_LCD_LED_3,
} hal_lcd_led_t;

/** available led colors enumeration */
typedef enum {
    HAL_LCD_LED_GREEN,
    HAL_LCD_LED_RED
} hal_lcd_led_color_t;

/**
 * @brief Clear LCD screen
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t hal_brd_lcd_screen_clear(void);

/**
 * @brief Display the string on the screen starting from current
 *        cursor position
 * @param[in] str - string to display
 * @return CCL_OK on success, error code on failure 
 * @note maximum supported string length is 
 *       HAL_LCD_MAX_STRING_LEN
 */
ccl_err_t hal_brd_lcd_string_display(char *str);

/**
 * @brief Move the cursor one position to specific direction
 * @param[in] dir - direction to move
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t hal_brd_lcd_cursor_move(hal_lcd_cursor_direction_t dir);

/**
 * @brief Set the cursor to a specific cursor position where the
 *        next character is printed
 * @param[in] row - destination row 
 * @param[in] column - destination column
 * @return CCL_OK on success, error code on failure 
 * @note maximum supported row is HAL_LCD_MAX_ROWS, maximum 
 *       supported column is HAL_LCD_MAX_COLUMNS
 */
ccl_err_t hal_brd_lcd_cursor_set(b_u32 row, b_u32 column);

/**
 * @brief Display the startup message on the screen starting 
 *        from upper-left position
 * @param[in] msg - message to display
 * @return CCL_OK on success, error code on failure 
 * @note maximum supported message length is 
 *       HAL_LCD_MAX_STRING_LEN
 */
ccl_err_t hal_brd_lcd_startup_message_display(char *msg);

/**
 * @brief Set the colors for specific led
 * @param[in] led - led to set the color for
 * @param[in] color1 - first color 
 * @param[in] state1 - first color on/off 
 * @param[in] color2 - second color 
 * @param[in] state2 - second color on/off 
 * @return CCL_OK on success, error code on failure 
 * @note generated led's color is comprised from 2 separate 
 *       colors
 */
ccl_err_t hal_brd_lcd_led_color_set(hal_lcd_led_color_t led,
                                    hal_lcd_led_color_t color1, b_bool state1, 
                                    hal_lcd_led_color_t color2, b_bool state2);

/**
 * @brief Control backlight of LCD component
 * @param[in] comp - LCD component
 * @param[in] mode - backlight mode of the component
 * @return CCL_OK on success, error code on failure 
 * @note generated led's color is comprised from 2 separate 
 *       colors
 */
ccl_err_t hal_brd_lcd_backlight_set(hal_lcd_component_t comp, hal_lcd_backlight_t mode);

/**
 * @brief Control brightness of LCD component
 * @param[in] comp - LCD component
 * @param[in] brightness - brightness of the component
 * @return CCL_OK on success, error code on failure 
 * @note brightness level from 0(Dim) to 255(Bright) 
 */
ccl_err_t hal_brd_lcd_brightness_set(hal_lcd_component_t comp, b_u8 brightness);

/**
 * @brief Check if the key is pressed and if yes, provide
 *        pressed key value
 * @param[out] pkey - key value
 * @return CCL_TRUE if the key is pressed, CCL_FALSE otherwise
 */
b_bool hal_brd_lcd_key_is_pressed(b_u8 *key);

#endif /*_HAL_BRD_LCD_H*/
