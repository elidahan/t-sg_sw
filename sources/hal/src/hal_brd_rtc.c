/********************************************************************************/
/**
 * @file hal_brd_rtc.c
 * @brief HAL RTC interface implementation
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "hal_brd_rtc.h"

ccl_err_t hal_brd_rtc_clock_set(hal_rtc_device_t dev, hal_rtc_time_t time)
{
    return CCL_OK;
}

ccl_err_t hal_brd_rtc_clock_get(hal_rtc_device_t dev, hal_rtc_time_t *ptime)
{
    return CCL_OK;
}

ccl_err_t hal_brd_rtc_mem_write(hal_rtc_device_t dev, b_u8 addr, b_u8 *buf, b_u16 len)
{
    return CCL_OK;
}

ccl_err_t hal_brd_rtc_mem_read(hal_rtc_device_t dev, b_u8 addr, b_u8 *buf, b_u16 len)
{
    return CCL_OK;
}

ccl_err_t hal_brd_rtc_mem_clear(hal_rtc_device_t dev)
{
    return CCL_OK;
}

ccl_err_t hal_brd_rtc_battery_status_get(hal_rtc_device_t dev, hal_rtc_battery_status_t *pstatus)
{
    return CCL_OK;
}





