/********************************************************************************/
/**
 * @file hal_brd_clear_switch.c
 * @brief HAL clear switch interface implementation
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "hal_brd_clear_switch.h"

ccl_err_t hal_brd_clear_switch_get(hal_clear_switch_t cswitch, b_u32 *pval)
{
    return CCL_OK;
}

ccl_err_t hal_brd_clear_switch_led_set(hal_clear_switch_t cswitch, b_bool ledstate)
{
    return CCL_OK;
}





