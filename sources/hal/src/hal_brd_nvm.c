/********************************************************************************/
/**
 * @file hal_brd_nvm.c
 * @brief HAL NVM (Non Volatile Memory) interface implementation
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "hal_brd_nvm.h"

ccl_err_t hal_brd_nvm_num_of_fields_get(hal_nvm_dev_t nvm,  b_u32 *pnum)
{
    return CCL_OK;
}

ccl_err_t hal_brd_nvm_field_type_get(hal_nvm_dev_t nvm,  hal_nvm_param_names_t field,  hal_nvm_param_types_t *ptype)
{
    return CCL_OK;
}

ccl_err_t hal_brd_nvm_field_size_get(hal_nvm_dev_t nvm,  hal_nvm_param_names_t field,  b_u32 *plen)
{
    return CCL_OK;
}

ccl_err_t hal_brd_nvm_field_get(hal_nvm_dev_t nvm,  hal_nvm_param_names_t field,  void *pbuf)
{
    return CCL_OK;
}

ccl_err_t hal_brd_nvm_field_set(hal_nvm_dev_t nvm,  hal_nvm_param_names_t field,  void *pbuf)
{
    return CCL_OK;
}




