/********************************************************************************/
/**
 * @file hal_brd_fan.c
 * @brief HAL fans interface implementation
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/

#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "hal_brd_fan.h"

ccl_err_t hal_brd_fan_number_of_trays_get(int *ptrays)
{
    return CCL_OK;
}

ccl_err_t hal_brd_fan_number_of_fans_in_tray_get(int tray,  int *pfans)
{
    return CCL_OK;
}

ccl_err_t hal_brd_fan_tray_status_get(int tray,  hal_fan_status_t *pstatus)
{
    return CCL_OK;
}

ccl_err_t hal_brd_fan_tray_fan_status_get(int tray,  int fan,  hal_fan_status_t *pstatus)
{
    return CCL_OK;
}

ccl_err_t hal_brd_fan_control_mode_set( hal_fan_control_t mode)
{
    return CCL_OK;
}

ccl_err_t hal_brd_fan_control_mode_get(hal_fan_control_t *pmode)
{
    return CCL_OK;
}

ccl_err_t hal_brd_fan_tray_speed_set(int tray,  int percent)
{
    return CCL_OK;
}

ccl_err_t hal_brd_fan_tray_speed_get(int tray,  int *ppercent)
{
    return CCL_OK;
}



