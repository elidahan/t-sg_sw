/********************************************************************************/
/**
 * @file hal_brd_port.c
 * @brief HAL ports interface implementation
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "devman_brd_gen.h"
#include "devman_brd_port.h"
#include "devman_brd_media.h"

static ccl_err_t _ret;

ccl_err_t hal_brd_port_init(b_u32 logport)
{
    _ret = devman_brd_port_init(logport);
    if (_ret) {
        ccl_syslog_err("Error! hal_brd_port_init() for port %d\n", logport);
        return CCL_FAIL;
    }

    return CCL_OK;
}

ccl_err_t hal_brd_port_number_of_ports_get(b_u32 *pports)
{
    _ret = devman_brd_port_number_of_ports_get(pports);
    if (_ret) {
        ccl_syslog_err("Error! devman_brd_port_number_of_ports_get()\n");
        return CCL_FAIL;
    }

    return CCL_OK;
}

ccl_err_t hal_brd_port_counter_get(b_u32 logport, devman_if_counter_t counter_type, 
                                   b_u64 *pcounter)
{
    _ret = devman_brd_port_counter_get(logport, counter_type, pcounter);
    if (_ret) {
        ccl_syslog_err("Error! devman_brd_port_counter_get(). port: %d, counter_type: %d\n",
                       logport, counter_type);
        return CCL_FAIL;
    }

    return CCL_OK;
}

ccl_err_t hal_brd_port_counter_clear(b_u32 logport, 
                                     devman_if_counter_t counter_type)
{
    return CCL_OK;
}

ccl_err_t hal_brd_port_tx_state_set(b_u32 logport, b_bool state)
{
    _ret = devman_brd_port_tx_state_set(logport, state);
    if (_ret) {
        ccl_syslog_err("Error! hal_brd_port_tx_state_set()\n");
        return CCL_FAIL;
    }

    return CCL_OK;
}

ccl_err_t hal_brd_port_tx_state_get(b_u32 logport, b_bool *pstate)
{
    _ret = devman_brd_port_tx_state_get(logport, pstate);
    if (_ret) {
        ccl_syslog_err("Error! hal_brd_port_tx_state_set()\n");
        return CCL_FAIL;
    }

    return CCL_OK;
}

ccl_err_t hal_brd_port_link_status_get(b_u32 logport, 
                                       devman_if_link_status_t *pstatus)
{
    b_u8 link;

    _ret = devman_brd_port_link_status_get(logport, pstatus);
    if (_ret) {
        ccl_syslog_err("Error! devman_brd_port_link_status_get()\n");
        return CCL_FAIL;
    }

    return CCL_OK;
}

ccl_err_t hal_brd_port_if_type_get(b_u32 logport, devman_if_type_t *piftype)
{
    _ret = devman_brd_port_if_type_get(logport, piftype);
    if (_ret) {
        ccl_syslog_err("Error! hal_brd_port_if_type_get()\n");
        return CCL_FAIL;
    }

    return CCL_OK;
}

ccl_err_t hal_brd_port_led_state_set(b_u32 logport,  b_bool state)
{
    _ret = devman_brd_port_led_state_set(logport, state);
    if (_ret) {
        ccl_syslog_err("Error! devman_brd_port_led_state_set()\n");
        return CCL_FAIL;
    }

    return CCL_OK;
}

ccl_err_t hal_brd_port_media_params_get(b_u32 logport, 
                                        devman_media_param_t *pmedia_param)
{
    if ( !pmedia_param ) {
        ccl_syslog_err("Error! Invalid input parameters\n");
        return CCL_BAD_PARAM_ERR;
    }

    _ret = devman_brd_port_property_get(logport, DEVMAN_IF_MEDIA_PARAMETERS, 
                                        pmedia_param, sizeof(*pmedia_param));
    if (_ret) {
        ccl_syslog_err("get port %d media params failed\n", logport);
        return CCL_FAIL;
    }

    return CCL_OK;
}

ccl_err_t hal_brd_port_media_details_get(b_u32 logport, b_u8 *pbuf, b_u32 bufsize)
{
    if ( !pbuf ) {
        ccl_syslog_err("Error! Invalid input parameters\n");
        return CCL_BAD_PARAM_ERR;
    }

    _ret = devman_brd_port_property_get(logport, DEVMAN_IF_MEDIA_DETAILS, 
                                        pbuf, bufsize);
    if (_ret) {
        ccl_syslog_err("get port %d media params failed\n", logport);
        return CCL_FAIL;
    }

    return CCL_OK;
}

ccl_err_t hal_brd_port_property_set(b_u32 logport, devman_if_property_t prop, 
                                    void *pdata, b_u32 size)
{
    _ret = devman_brd_port_property_set(logport, prop, pdata, size);
    if (_ret) {
        ccl_syslog_err("Error! devman_brd_port_property_set()\n");
        return CCL_FAIL;
    }

    return CCL_OK;
}

ccl_err_t hal_brd_port_property_get(b_u32 logport, devman_if_property_t prop, 
                                    void *pdata, b_u32 size)
{
    _ret = devman_brd_port_property_get(logport, prop, pdata, size);
    if (_ret) {
        ccl_syslog_err("Error! devman_brd_port_property_get()\n");
        return CCL_FAIL;
    }

    return CCL_OK;
}

