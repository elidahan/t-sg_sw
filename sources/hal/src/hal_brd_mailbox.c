/********************************************************************************/
/**
 * @file hal_brd_mailbox.c
 * @brief HAL mailbox interface implementation
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "hal_brd_mailbox.h"

ccl_err_t hal_brd_mailbox_value_set(hal_mailbox_user_t muser, b_u32 val)
{
    return CCL_OK;
}

ccl_err_t hal_brd_mailbox_value_get(hal_mailbox_user_t muser, b_u32 *pval)
{
    return CCL_OK;
}

ccl_err_t hal_brd_mailbox_value_bcast(b_u32 val)
{
    return CCL_OK;
}





