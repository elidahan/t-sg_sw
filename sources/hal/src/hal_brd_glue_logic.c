/********************************************************************************/
/**
 * @file hal_brd_glue_logic.c
 * @brief HAL Glue Logic interface implementation
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "hal_brd_glue_logic.h"

ccl_err_t hal_brd_gl_init(void)
{
    return CCL_OK;
}

ccl_err_t hal_brd_gl_write(int reg,  int val)
{
    return CCL_OK;
}

ccl_err_t hal_brd_gl_read(int reg,  int *pval)
{
    return CCL_OK;
}


