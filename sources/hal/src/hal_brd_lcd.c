/********************************************************************************/
/**
 * @file hal_brd_lcd.c
 * @brief HAL LCD interface implementation
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "hal_brd_lcd.h"

ccl_err_t hal_brd_lcd_screen_clear(void)
{
    return CCL_OK;
}

ccl_err_t hal_brd_lcd_string_display(char *str)
{
    return CCL_OK;
}

ccl_err_t hal_brd_lcd_cursor_move(hal_lcd_cursor_direction_t dir)
{
    return CCL_OK;
}

ccl_err_t hal_brd_lcd_cursor_set(b_u32 row, b_u32 column)
{
    return CCL_OK;
}

ccl_err_t hal_brd_lcd_startup_message_display(char *msg)
{
    return CCL_OK;
}

ccl_err_t hal_brd_lcd_led_color_set(hal_lcd_led_color_t led,
                                    hal_lcd_led_color_t color1, b_bool state1, 
                                    hal_lcd_led_color_t color2, b_bool state2)
{
    return CCL_OK;
}

ccl_err_t hal_brd_lcd_backlight_set(hal_lcd_component_t comp, hal_lcd_backlight_t mode)
{
    return CCL_OK;
}

ccl_err_t hal_brd_lcd_brightness_set(hal_lcd_component_t comp, b_u8 brightness)
{
    return CCL_OK;
}

b_bool hal_brd_lcd_key_is_pressed(b_u8 *key)
{
    return CCL_TRUE;
}


