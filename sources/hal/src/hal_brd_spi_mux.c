/********************************************************************************/
/**
 * @file hal_brd_spi_mux.c
 * @brief HAL SPI MUX interface implementation
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "hal_brd_spi_mux.h"

ccl_err_t hal_brd_spi_mux_dest_set(hal_spi_mux_dest_t dest)
{
    return CCL_OK;
}



