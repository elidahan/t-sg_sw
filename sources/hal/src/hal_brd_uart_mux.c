/********************************************************************************/
/**
 * @file hal_brd_uart_mux.c
 * @brief HAL UART MUX interface implementation
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "hal_brd_uart_mux.h"

ccl_err_t hal_brd_uart_mux_dest_set(hal_uart_mux_dest_t dest)
{
    return CCL_OK;
}



