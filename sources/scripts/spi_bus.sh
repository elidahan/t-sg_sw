#!/bin/bash

source common.sh

usage() { echo "Usage: $0 [-ac] [-d duration]" 1>&2; exit 1; }

artix=0
cpu=0

while getopts ":d:ac" o; do
    case "${o}" in
        d)
            duration=${OPTARG}
            ;;
        a)
            artix=1
            ;;
        c)
            cpu=1
            ;;
        *)
            usage
            ;;
    esac
done

shift $((OPTIND-1))

if [ -z "${duration}" ]; then
    usage
fi

for i in {1..3..2}
do
  echo "x axispi/$i reset"
  tsg_devmansm -c "x axispi/$i reset" 2>&1 | logmsg
  echo "x axispi/$i start"
  tsg_devmansm -c "x axispi/$i start" 2>&1 | logmsg
done

#I2CCORE #0 (ATP)
if [ "$artix" -eq 1 ]
then
  #Set Flash Mux to enable access from Artix
  echo "x atp fpga_writew 0x2a000 0xf0"
  tsg_devmansm -c "x atp fpga_writew 0x2a000 0xf0" 2>&1 | logmsg
  start=$(date +%s)
  end=$(date +%s)
  seconds=$(echo "$end - $start" | bc)
  while [ $seconds -lt $duration ]
  do
    #2 SMARC flashes, 4 Xeon flashes
    for i in {0..5}
    do
      echo "x atp axispi_flashrd $i 0x9f 4"
      tsg_devmansm -c "x atp axispi_flashrd $i 0x9f 4"  2>&1 | logmsg   
      if [ $? -eq 1 ] 
      then
        echo "Error: x atp axispi_flashrd $i 0x9f 4"
        exit 1
      fi
      sleep 0.1
    done    
    #2 Generic Flashes
    for i in {10..11}
    do
      echo "x atp axispi_flashrd $i 0x9f 4"
      tsg_devmansm -c "x atp axispi_flashrd $i 0x9f 4"  2>&1 | logmsg   
      if [ $? -eq 1 ] 
      then
        echo "Error: x atp axispi_flashrd $i 0x9f 4"
        exit 1
      fi
      sleep 0.1
    done    
    end=$(date +%s)
    seconds=$(echo "$end - $start" | bc)
  done
fi


exit 0
