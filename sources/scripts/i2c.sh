#!/bin/bash

source common.sh

usage() { echo "Usage: $0 [-perci] [-d duration]" 1>&2; exit 1; }

pmbus=0
eeprom=0
rtc=0
currmon=0
i2cio=0

while getopts ":d:perci" o; do
    case "${o}" in
        d)
            duration=${OPTARG}
            ;;
        p)
            pmbus=1
            ;;
        e)
            eeprom=1
            ;;
        r)
            rtc=1
            ;;
        c)
           currmon=1
            ;;
        i)
           i2cio=1
            ;;
        *)
            usage
            ;;
    esac
done

shift $((OPTIND-1))

if [ -z "${duration}" ]; then
    usage
fi

for i in {0..1}
do
  echo "x i2ccore/$i reset"
  tsg_devmansm -c "x i2ccore/$i reset" 2>&1 | logmsg
  echo "x i2ccore/$i start"
  tsg_devmansm -c "x i2ccore/$i start" 2>&1 | logmsg
done

#PMBUS
if [ "$pmbus" -eq 1 ]
then
  for i in {0..10}
  do
    start=$(date +%s)
    end=$(date +%s)
    seconds=$(echo "$end - $start" | bc)
    while [ $seconds -lt $duration ]
    do
      echo "show pmbus/$i read_vout"
      tsg_devmansm -c "show pmbus/$i read_vout"  2>&1 | logmsg
      if [ $? -eq 1 ] 
      then
        echo "Error: show pmbus/$i read_vout"
        exit 1
      fi
      sleep 0.1
      end=$(date +%s)
      seconds=$(echo "$end - $start" | bc)
    done
  done
fi

#EEPROM
if [ "$eeprom" -eq 1 ]
then
  for i in {1..7}
  do
      echo "x atp eepromwr $i 0x20 aacc55dd"
      tsg_devmansm -c "x atp eepromwr $i 0x20 aacc55dd"  2>&1 | logmsg   
      if [ $? -eq 1 ] 
      then
        echo "Error: x atp eepromwr $i 0x20 aacc55dd"
        exit 1
      fi
    start=$(date +%s)
    end=$(date +%s)
    seconds=$(echo "$end - $start" | bc)
    while [ $seconds -lt $duration ]
    do
      echo "x atp eepromrd $i 0x20 4"
      tsg_devmansm -c "x atp eepromrd $i 0x20 4"  2>&1 | logmsg   
      if [ $? -eq 1 ] 
      then
        echo "Error: x atp eepromrd $i 0x20 4"
        exit 1
      fi
      sleep 0.1
      end=$(date +%s)
      seconds=$(echo "$end - $start" | bc)
    done
  done
fi

#RTC
if [ "$rtc" -eq 1 ]
then
  for i in {0..0} #for i in {0..2}
  do
    start=$(date +%s)
    end=$(date +%s)
    seconds=$(echo "$end - $start" | bc)
    while [ $seconds -lt $duration ]
    do
      echo "c rtc/$i day c5"
      tsg_devmansm -c "c rtc/$i day c5"  2>&1 | logmsg   
      if [ $? -eq 1 ] 
      then
        echo "Error: c rtc/$i day c5"
        exit 1
      fi
      sleep 0.1
      echo "s rtc/$i day"
      tsg_devmansm -c "s rtc/$i day"  2>&1 | logmsg   
      if [ $? -eq 1 ] 
      then
        echo "Error: s rtc/$i day"
        exit 1
      fi
      end=$(date +%s)
      seconds=$(echo "$end - $start" | bc)
    done
  done
fi

#Current Monitor
if [ "$currmon" -eq 1 ]
then
  for i in {0..5} 
  do
    start=$(date +%s)
    end=$(date +%s)
    seconds=$(echo "$end - $start" | bc)
    while [ $seconds -lt $duration ]
    do
      echo "show currmon/$i busv"
      tsg_devmansm -c "s currmon/$i busv"  2>&1 | logmsg   
      if [ $? -eq 1 ] 
      then
        echo "Error: s currmon/$i busv"
        exit 1
      fi
      sleep 0.1
      end=$(date +%s)
      seconds=$(echo "$end - $start" | bc)
    done
  done
fi

#I2CIO
if [ "$i2cio" -eq 1 ]
then
  for i in {0..0} 
  do
    start=$(date +%s)
    end=$(date +%s)
    seconds=$(echo "$end - $start" | bc)
    while [ $seconds -lt $duration ]
    do
      echo "s i2cio/0 iport7_0"
      tsg_devmansm -c "s i2cio/0 iport7_0"  2>&1 | logmsg   
      if [ $? -eq 1 ] 
      then
        echo "Error: s i2cio/0 iport7_0"
        exit 1
      fi
      sleep 0.1
      end=$(date +%s)
      seconds=$(echo "$end - $start" | bc)
    done
  done
fi


exit 0
