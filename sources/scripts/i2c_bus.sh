#!/bin/bash

source $(dirname "$0")/common.sh

usage() { echo "Usage: $0 [-psatd] [-d duration]" 1>&2; exit 1; }

pmbus=0
sysbus=0
atp=0
trans=0
dimm=0

while getopts ":d:psatd" o; do
    case "${o}" in
        d)
            duration=${OPTARG}
            ;;
        p)
            pmbus=1
            ;;
        s)
            sysbus=1
            ;;
        a)
            atp=1
            ;;
        t)
            trans=1
            ;;
        d)
            dimm=1
            ;;
        *)
            usage
            ;;
    esac
done

shift $((OPTIND-1))

if [ -z "${duration}" ]; then
    usage
fi

for i in {0..0}
do
  echo "x i2ccore/$i reset"
  tsg_devmansm -c "x i2ccore/$i reset" 2>&1 | logmsg
  echo "x i2ccore/$i start"
  tsg_devmansm -c "x i2ccore/$i start" 2>&1 | logmsg
done

#I2CCORE #0 (ATP)
if [ "$atp" -eq 1 ]
then
  start=$(date +%s)
  end=$(date +%s)
  seconds=$(echo "$end - $start" | bc)
  while [ $seconds -lt $duration ]
  do
    #EEPROM
    for i in {6..7}
    do
        run_test eeprom $i aacc55dd          
    done
    #RTC
    for i in {1..2}
    do
        run_test rtc $i c5
    done
    #I2CIO
    for i in {8..9} 
    do
        run_test i2cio $i
    done
    end=$(date +%s)
    seconds=$(echo "$end - $start" | bc)
  done
fi

#I2CCORE #1 (PMBUS, SYSTEM BUS, RED PMBUS, I2CIO)
#PMBUS
if [ "$pmbus" -eq 1 ]
then
  start=$(date +%s)
  end=$(date +%s)
  seconds=$(echo "$end - $start" | bc)
  while [ $seconds -lt $duration ]
  do
    #
    for i in {0..10}
    do
      echo "show pmbus/$i read_vout"
      tsg_devmansm -c "show pmbus/$i read_vout"  2>&1 | logmsg
      if [ $? -eq 1 ] 
      then
        echo "Error: show pmbus/$i read_vout"
        exit 1
      fi
      sleep 0.1
    done
    #FAN Controller
    for i in {0..0}
    do
      echo "s fan/$i version"
      tsg_devmansm -c "s fan/$i version"  2>&1 | logmsg
      if [ $? -eq 1 ] 
      then
        echo "Error: s fan/$i version"
        exit 1
      fi
      sleep 0.1
    done
    end=$(date +%s)
    seconds=$(echo "$end - $start" | bc)
  done
fi

#SYSTEM BUS
if [ "$sysbus" -eq 1 ]
then
  for i in {1..5}
  do
    echo "x atp eepromwr $i 0x20 aacc55dd"
    tsg_devmansm -c "x atp eepromwr $i 0x20 aacc55dd"  2>&1 | logmsg   
    if [ $? -eq 1 ] 
    then
      echo "Error: x atp eepromwr $i 0x20 aacc55dd"
      exit 1
    fi
    sleep 0.1
  done    
  for i in {0..0}
  do
    echo "c rtc/$i day c5"
    tsg_devmansm -c "c rtc/$i day c5"  2>&1 | logmsg   
    if [ $? -eq 1 ] 
    then
      echo "Error: c rtc/$i day c5"
      exit 1
    fi
    sleep 0.1
  done
  for i in {0..0}
  do
    echo "c lcd/$i set_brightness 10"
    tsg_devmansm -c "c lcd/$i set_brightness 10"  2>&1 | logmsg   
    if [ $? -eq 1 ] 
    then
      echo "Error: c lcd/$i set_brightness 10"
      exit 1
    fi
    sleep 0.1
  done
  start=$(date +%s)
  end=$(date +%s)
  seconds=$(echo "$end - $start" | bc)
  while [ $seconds -lt $duration ]
  do
    #EEPROM
    for i in {1..5}
    do
      echo "x atp eepromrd $i 0x20 4"
      tsg_devmansm -c "x atp eepromrd $i 0x20 4"  2>&1 | logmsg   
      if [ $? -eq 1 ] 
      then
        echo "Error: x atp eepromrd $i 0x20 4"
        exit 1
      fi
      sleep 0.1
    done
    #RTC
    for i in {0..0}
    do
      echo "s rtc/$i day"
      tsg_devmansm -c "s rtc/$i day"  2>&1 | logmsg   
      if [ $? -eq 1 ] 
      then
        echo "Error: s rtc/$i day"
        exit 1
      fi
      sleep 0.1    
    done
    #Current Monitor
    for i in {0..5} 
    do
      echo "show currmon/$i busv"
      tsg_devmansm -c "s currmon/$i busv"  2>&1 | logmsg   
      if [ $? -eq 1 ] 
      then
        echo "Error: s currmon/$i busv"
        exit 1
      fi  
      sleep 0.1    
    done
    end=$(date +%s)
    seconds=$(echo "$end - $start" | bc)
  done  
fi

#I2CCORE #2,3 (SFP, QSFP)
if [ "$trans" -eq 1 ]
then
  #Unreset QSFPs
  echo "x atp fpga_writew 0x40000000 0x30000000"
  tsg_devmansm -c "x atp fpga_writew 0x40000000 0x30000000" 2>&1 | logmsg
  echo "x atp fpga_writew 0x40010000 0x30000000"
  tsg_devmansm -c "x atp fpga_writew 0x40010000 0x30000000" 2>&1 | logmsg

  start=$(date +%s)
  end=$(date +%s)
  seconds=$(echo "$end - $start" | bc)
  while [ $seconds -lt $duration ]
  do
    #SFP (0-11, 14-25)
    #QSFP (12-13, 26-27)
    for i in {0..27}
    do
      echo "x atp trans_data $i msa_raw"
      tsg_devmansm -c "x atp trans_data $i msa_raw"  2>&1 | logmsg
      if [ $? -eq 1 ] 
      then
        echo "Error: x atp trans_data $i msa_raw"
        exit 1
      fi
      sleep 0.1
    done
    end=$(date +%s)
    seconds=$(echo "$end - $start" | bc)
  done
fi

#I2CCORE #4 (SODIMMs)
if [ "$trans" -eq 1 ]
then
  start=$(date +%s)
  end=$(date +%s)
  seconds=$(echo "$end - $start" | bc)
  while [ $seconds -lt $duration ]
  do
    for i in {8..10}
    do
      echo "x atp eepromrd $i 0x0 16"
      tsg_devmansm -c "x atp eepromrd $i 0x0 16"  2>&1 | logmsg
      if [ $? -eq 1 ] 
      then
        echo "Error: x atp eepromrd $i 0x0 16"
        exit 1
      fi
      sleep 0.1
    done
    end=$(date +%s)
    seconds=$(echo "$end - $start" | bc)
  done
fi

exit 0
