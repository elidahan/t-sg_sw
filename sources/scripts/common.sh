#!/bin/bash

logmsg()
{
    read IN # This reads a string from stdin and stores it in a variable called IN
    DateTime=`date "+%Y/%m/%d %H:%M:%S"`
    echo $DateTime $IN
    echo $IN | fgrep -i "error"
    if [ $? -eq 0 ]
    then
        echo "command has failed"
        return 1
    else
        echo "command success" 
    fi
}

run_test_eeprom()
{
    i=$1
    data=$2
    echo "x atp eepromwr $i 0x20 $data"
    tsg_devmansm -c "x atp eepromwr $i 0x20 $data"  2>&1 | logmsg   
    if [ $? -eq 1 ] 
    then
      echo "Error: x atp eepromwr $i 0x20 $data"
      exit 1
    fi
    sleep 0.1
    echo "x atp eepromrd $i 0x20 4"
    tsg_devmansm -c "x atp eepromrd $i 0x20 4"  2>&1 | logmsg   
    if [ $? -eq 1 ] 
    then
        echo "Error: x atp eepromrd $i 0x20 4"
        exit 1
    fi
    sleep 0.1
}

run_test_rtc()
{
    i=$1
    data=$2
    echo "c rtc/$i day $data"
    tsg_devmansm -c "c rtc/$i day $data"  2>&1 | logmsg   
    if [ $? -eq 1 ] 
    then
      echo "Error: c rtc/$i day $data"
      exit 1
    fi
    sleep 0.1
    echo "s rtc/$i day"
    tsg_devmansm -c "s rtc/$i day"  2>&1 | logmsg   
    if [ $? -eq 1 ] 
    then
    echo "Error: s rtc/$i day"
    exit 1
    fi
    sleep 0.1    
}

run_test_i2cio()
{
    i=$1
    echo "s i2cio/$i iport7_0"
    tsg_devmansm -c "s i2cio/$i iport7_0"  2>&1 | logmsg   
    if [ $? -eq 1 ] 
    then
    echo "Error: s i2cio/$i iport7_0"
    exit 1
    fi
    sleep 0.1   
}

run_test()
{   
    case "$1" in
        pmbus|eeprom|rtc|i2cio|fan|lcd| \
        currmon|trans|sodimm|spiflash|uart)
            run_test_${1} $2 $3
            ;;
    *)
        echo "$0 Error: wrong argument $1"
        exit 1
        ;; 
    esac      
}

