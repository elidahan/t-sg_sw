/********************************************************************************/
/**
 * @file ccl_logger.c
 * @brief common logger implementation.
 * @date Jun 16, 2022
 * @version 1.0
 * @copyright 2016 CELARE. All rights reserved.
 *
 * Prints log messages to the file and/or syslog according to the current log level.
 * Currently log levels are:
 * - INFO -- information messages
 * - ERROR -- errors
 * - DEBUG -- debug messages
 *
 * Log file format is the following:
 * \<timestamp\> [pid=\<pid\>] [\<log level\>] \<message\>
 *
 **/
/********************************************************************************/

#include "ccl_types.h"
#include "ccl_logger.h"
#include "ccl_util.h"

#include <time.h>
#include <stdarg.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#include <syslog.h>
#include <errno.h>
#include <sys/syscall.h>
#include <glib.h>


/**
 * @brief Prepares log message for printing to the log.
 * Sets pid, log tag, timestamp to log message and prints to the file.
 * @param[in]  logger
 * @param[in]  tag - log level
 * @param[in]  message - message to log
 * @param[in]  args - variable length printf arguments
 * @return void
 */
static void log_format(ccl_logger_t *logger, const char* tag, const char* message, va_list args) {
    time_t now;
    time(&now);
    char date[26];
    ctime_r(&now, date);
    date[strlen(date) - 1] = '\0';

    char *fmt_msg = NULL;

    asprintf(&fmt_msg, "%s [pid=%d] [tid=%ld] [%s] %s \n", date, getpid(), syscall(__NR_gettid), tag, message);

    vfprintf(logger->log_file, fmt_msg, args);
    fflush(logger->log_file);
    free(fmt_msg);
}

typedef struct {
    char *log_msg_id;
    time_t last_pr_time;
} log_ratelimit_info_t;

static  log_ratelimit_info_t *alloc_log_ratelimit_info(const char *log_msg_id) {
    log_ratelimit_info_t *ratelimit_info = calloc(1, sizeof(*ratelimit_info));
    assert(ratelimit_info);

    ratelimit_info->log_msg_id = strdup(log_msg_id);

    return ratelimit_info;
}

static void destroy_log_ratelimit_info(log_ratelimit_info_t *ratelimit_info) {
    free(ratelimit_info->log_msg_id);
    free(ratelimit_info);
}

#define LOG_BASIC_BODY(LEVEL, logger, message)           \
    va_list args;                                        \
    if (logger->log_lvl >= LOG_LVL_##LEVEL) {            \
        va_start(args, message);                         \
        log_format(logger, STRINGIFY_VALUE_OF(LEVEL), message, args); \
        va_end(args);                                    \
    }                                                    \
    if (logger->log_syslog_lvl >= LOG_LVL_##LEVEL) {     \
        va_start(args, message);                         \
        vsyslog(LOG_LVL_##LEVEL, message, args);         \
        va_end(args);                                    \
    }                                                    \


/**
 * @brief Prints debug log message to the file and syslog.
 * Prints debug log message to the file.
 * @param[in]  logger
 * @param[in]  message
 * @return void
 */
#define LOG_BASIC_RATELIMITED(fn_name, LEVEL) \
void ccl_log_##fn_name##_ratelimited(ccl_logger_t *logger, const char *message, ...) { \
\
    if (logger->log_lvl < LOG_LVL_##LEVEL && logger->log_syslog_lvl < LOG_LVL_##LEVEL) { \
        return; \
    } \
\
    int res = pthread_mutex_lock(&logger->ratelimit_table_lock); \
    assert(!res); \
\
    log_ratelimit_info_t *ratelimit_info = g_hash_table_lookup(logger->ratelimit_table, message); \
    if (!ratelimit_info) { \
        ratelimit_info = alloc_log_ratelimit_info(message); \
        res = g_hash_table_insert(logger->ratelimit_table, ratelimit_info->log_msg_id, ratelimit_info); \
        assert(res); \
    } \
    int pr_log = 0; \
    time_t curr_time = time(NULL); \
    if (curr_time > ratelimit_info->last_pr_time + logger->log_msg_pr_interval) { \
        ratelimit_info->last_pr_time = curr_time; \
        pr_log = 1; \
    } \
\
    res = pthread_mutex_unlock(&logger->ratelimit_table_lock); \
    assert(!res); \
\
    if (pr_log) { \
       LOG_BASIC_BODY(LEVEL, logger, message);\
    }\
\
}\

LOG_BASIC_RATELIMITED(debug, DEBUG)

LOG_BASIC_RATELIMITED(info, INFO)

LOG_BASIC_RATELIMITED(notice, NOTICE)

LOG_BASIC_RATELIMITED(warn, WARNING)

LOG_BASIC_RATELIMITED(error, ERROR)

LOG_BASIC_RATELIMITED(alert, ALERT)

LOG_BASIC_RATELIMITED(crit, CRITICAL)

LOG_BASIC_RATELIMITED(emerg, EMERGENCY)

#define LOG_BASIC(fn_name, LEVEL) \
void ccl_log_##fn_name (ccl_logger_t *logger, const char* message, ...) \
{                                                               \
    LOG_BASIC_BODY(LEVEL, logger, message)                      \
}                                                               \

/**
 * @brief Prints debug log message to the file and syslog.
 * Prints debug log message to the file.
 * @param[in]  logger
 * @param[in]  message
 * @return void
 */
LOG_BASIC(debug, DEBUG)

/**
 * @brief Prints info log message to the file.
 * Prints info log message to the file.
 * @param[in]  logger
 * @param[in]  message
 * @return void
 */
LOG_BASIC(info, INFO)

/**
 * @brief Prints notice log message to the file.
 * Prints notice log message to the file.
 * @param[in]  logger
 * @param[in]  message
 * @return void
 */
LOG_BASIC(notice, NOTICE)


/**
 * @brief Prints warning log message to the file.
 * Prints warning log message to the file.
 * @param[in]  logger
 * @param[in]  message
 * @return void
 */
LOG_BASIC(warn, WARNING)

/**
 * @brief Prints error log message to the file.
 * Prints error log message to the file.
 * @param[in]  logger
 * @param[in]  message
 * @return void
 */
LOG_BASIC(error, ERROR)

/**
 * @brief Prints alert log message to the file.
 * Prints alert log message to the file.
 * @param[in]  logger
 * @param[in]  message
 * @return void
 */
LOG_BASIC(alert, ALERT)

/**
 * @brief Prints critical log message to the file.
 * Prints critical log message to the file.
 * @param[in]  logger
 * @param[in]  message
 * @return void
 */
LOG_BASIC(crit, CRITICAL)

/**
 * @brief Prints emergency log message to the file.
 * Prints emergency log message to the file.
 * @param[in]  logger
 * @param[in]  message
 * @return void
 */
LOG_BASIC(emerg, EMERGENCY)

/**
 * @brief Prints critical error log message to the file and dies with core dump.
 * Prints critical error log message to the file and dies.
 * Also prints a message to syslog.
 * @param[in]  logger
 * @param[in]  message
 * @return void
 */
void ccl_log_panic(ccl_logger_t *logger, const char* message, ...) {
    va_list args;

    va_start(args, message);
    log_format(logger, "EMERGENCY", message, args);
    va_end(args);

    va_start(args, message);
    vsyslog(LOG_LVL_EMERGENCY, message, args);
    va_end(args);
    fclose(logger->log_file);
    abort();
}

#define DEFAULT_RATELIMITED_MSG_PR_INTERVAL 5

ccl_logger_t ccl_create_logger(const ccl_logger_conf_t *cfg) {
    FILE *f = fopen(cfg->log_file, "a");
    if (!f) {
        ccl_syslog_err("failed to open log file %s, error - %s", cfg->log_file, strerror(errno));
        exit(1);
    }
    ccl_logger_t logger;
    logger.log_file = f;
    logger.log_lvl = cfg->log_lvl;
    logger.log_syslog_lvl = cfg->log_syslog_lvl;
    logger.module_name = cfg->module_name;
    pthread_mutex_init(&logger.ratelimit_table_lock, NULL);
    logger.ratelimit_table = g_hash_table_new_full(g_str_hash, g_str_equal, NULL, (GDestroyNotify)destroy_log_ratelimit_info);
    logger.log_msg_pr_interval = DEFAULT_RATELIMITED_MSG_PR_INTERVAL;

    ccl_log_info(&logger, "Logger was successfuly created, log file is: %s", cfg->log_file);
    return logger;
}

void ccl_register_logger_cb(ccl_logger_t *logger, logger_callback_fn_t logger_cb)
{
    logger->f_cb = logger_cb;
}

void ccl_destroy_logger(ccl_logger_t *logger) {
    ccl_log_info(logger, "Destroying logger");
    g_hash_table_destroy(logger->ratelimit_table);
    pthread_mutex_destroy(&logger->ratelimit_table_lock);
    fclose(logger->log_file);
}

/**
 * @brief Changes log level.
 * Changes log level
 * @param[in]  logger
 * @param[in]  new_log_lvl - new log level
 * @return void
 */
void ccl_set_log_level(ccl_logger_t *logger, enum log_level new_log_lvl) {
    logger->log_lvl = new_log_lvl;
}

/**
 * @brief Changes log syslog level.
 * Changes log syslog level
 * @param[in]  logger
 * @param[in]  new_log_lvl - new syslog level
 * @return void
 */
void ccl_set_log_syslog_level(ccl_logger_t *logger, enum log_level new_log_lvl) {
    logger->log_syslog_lvl = new_log_lvl;
}

/**
 * @brief Prints error log message to syslog.
 * Prints error log message to syslog.
 * @param[in]  message
 * @return void
 */
void ccl_syslog_err(const char* message, ...) {
    va_list args;
    va_start(args, message);
    vsyslog(LOG_ERR, message, args);
    va_end(args);
}

void ccl_syslog_warning(const char* message, ...) {
    va_list args;
    va_start(args, message);
    vsyslog(LOG_WARNING, message, args);
    va_end(args);
}

void ccl_destroy_logger_conf(ccl_logger_conf_t *conf) {
    free((char*)conf->log_file);
}

ccl_err_t ccl_log_file_chown(ccl_logger_t *logger, uid_t uid, gid_t gid) {
    int fd = fileno(logger->log_file);
    return !fchown(fd, uid, gid) ? CCL_OK : CCL_ERRNO;
}
