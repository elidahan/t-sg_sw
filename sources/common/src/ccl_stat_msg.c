/********************************************************************************/
/**
 * @file stat_msg.c
 * @brief Simple wrapper around cJSON to work with statistics messages
 * @date March 26, 2018
 * @version 1.25
 * @copyright 2018 CELARE. All rights reserved.
 *
 * Note: by default ccl_stat_msg_t includes index and type fields, so it is not needed
 * to add them manually.
 *
 **/
/********************************************************************************/

#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_stat_msg.h"

#define INDEX_STRING_TEMPLATE "tsg-%u-monitor"

void ccl_init_stat_msg(ccl_stat_msg_t *stat_msg, unsigned const id, const char *stat_type)
{
    char index_string[sizeof(INDEX_STRING_TEMPLATE) + MAX_U64_INT_STR_LEN];
    sprintf(index_string, INDEX_STRING_TEMPLATE, id);
    stat_msg->msg = cJSON_CreateObject();
    cJSON_AddItemToObject(stat_msg->msg, "index", cJSON_CreateString(index_string));

    cJSON_AddItemToObject(stat_msg->msg, "type", cJSON_CreateString(stat_type));
}

void ccl_stat_msg_add_numeric_field(ccl_stat_msg_t *stat_msg, const char *prefix, 
                                    const char *field_name, double value) 
{
    if (!prefix) 
        cJSON_AddItemToObject(stat_msg->msg, field_name, cJSON_CreateNumber(value));
    else {
        char concat_fname[CCL_BUFSIZ];
        snprintf(concat_fname, CCL_BUFSIZ, "%s_%s", prefix, field_name);
        cJSON_AddItemToObject(stat_msg->msg, concat_fname, cJSON_CreateNumber(value));
    }
}

void ccl_stat_msg_add_string_field(ccl_stat_msg_t *stat_msg, const char *prefix, 
                                   const char *field_name, const char *value) 
{
    if (!prefix) 
        cJSON_AddItemToObject(stat_msg->msg, field_name, cJSON_CreateString(value));
    else {
        char concat_fname[CCL_BUFSIZ];
        snprintf(concat_fname, CCL_BUFSIZ, "%s_%s", prefix, field_name);
        cJSON_AddItemToObject(stat_msg->msg, concat_fname, cJSON_CreateString(value));
    }    
}

void ccl_stat_msg_add_custom_json_field(ccl_stat_msg_t *stat_msg, const char *prefix, 
                                        const char *field_name, cJSON *obj) 
{
    if (!prefix) 
        cJSON_AddItemToObject(stat_msg->msg, field_name, obj);
    else {
        char concat_fname[CCL_BUFSIZ];
        snprintf(concat_fname, CCL_BUFSIZ, "%s_%s", prefix, field_name);
        cJSON_AddItemToObject(stat_msg->msg, concat_fname, obj);
    }    
}

char *ccl_stat_msg_to_string_unformatted(ccl_stat_msg_t *stat_msg) 
{
    return cJSON_PrintUnformatted(stat_msg->msg);
}

char *ccl_stat_msg_to_string(ccl_stat_msg_t *stat_msg) 
{
    return cJSON_Print(stat_msg->msg);
}

void ccl_deinit_stat_msg(ccl_stat_msg_t *stat_msg) 
{
    cJSON_Delete(stat_msg->msg);
}
