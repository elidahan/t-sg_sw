/********************************************************************************/
/**
 * @file stat_sender.c
 * @brief API to implement generic statistics sender class
 * @copyright 2020 CELARE. All rights reserved.
 *
 * Statitics sender is used to send periodic modules statistics.
 **/
/********************************************************************************/
#include <stdlib.h>

#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "ccl_stat_msg.h"
#include "ccl_stat_sender.h"

/**
 * @brief Implementation of timer expiration callback
 * Creates and sends statistics message to mq. Creates statistics message
 * with common fields such as timestamp, system resourse usage, etc.
 * Calls prepare_custom_stat() function pointer to add module specific fields.
 * @param[in] sender
 * @return void
 */
static void _send_stat_impl(ccl_stat_sender_t *sender) {

    time_t curr_time = time(NULL);
    b_u32 i;

    ccl_init_stat_msg(&sender->msg, sender->sender_id, sender->sender_type);

    ccl_stat_msg_add_numeric_field(&sender->msg, NULL, "timestamp", curr_time);
    ccl_stat_msg_add_numeric_field(&sender->msg, NULL, "delta_time", 
                                   difftime(curr_time, sender->prev_send_time));

    ccl_get_sys_res_usage(&sender->sys_rusage_stat);

    ccl_stat_msg_add_numeric_field(&sender->msg, NULL, "cpu_num", 
                                   sender->sys_rusage_stat.cpu_num);
    ccl_stat_msg_add_numeric_field(&sender->msg, NULL, "cpu_usage", 
                                   sender->sys_rusage_stat.cpu_usage);
    ccl_stat_msg_add_numeric_field(&sender->msg, NULL, "load_average", 
                                   sender->sys_rusage_stat.load_avg);
    ccl_stat_msg_add_numeric_field(&sender->msg, NULL, "total_memory", 
                                   sender->sys_rusage_stat.total_mem);
    ccl_stat_msg_add_numeric_field(&sender->msg, NULL, "free_memory", 
                                   sender->sys_rusage_stat.free_mem);
    ccl_stat_msg_add_numeric_field(&sender->msg, NULL, "used_memory", 
                                   sender->sys_rusage_stat.used_mem);
    ccl_stat_msg_add_numeric_field(&sender->msg, NULL, "shared_memory", 
                                   sender->sys_rusage_stat.sharedram);
    ccl_stat_msg_add_numeric_field(&sender->msg, NULL, "buffered_memory", 
                                   sender->sys_rusage_stat.bufferram);
    ccl_stat_msg_add_numeric_field(&sender->msg, NULL, "available_memory", 
                                   sender->sys_rusage_stat.avail_mem);

    for (i = 0; i < sender->sys_rusage_stat.sys_sensors.voltage_sensors_num; i++) 
        ccl_stat_msg_add_numeric_field(&sender->msg, NULL, 
                                       sender->sys_rusage_stat.sys_sensors.voltage_sensors[i].name, 
                                       sender->sys_rusage_stat.sys_sensors.voltage_sensors[i].value);
    for (i = 0; i < sender->sys_rusage_stat.sys_sensors.temp_sensors_num; i++) 
        ccl_stat_msg_add_numeric_field(&sender->msg, NULL, 
                                       sender->sys_rusage_stat.sys_sensors.temp_sensors[i].name, 
                                       sender->sys_rusage_stat.sys_sensors.temp_sensors[i].value);

    sender->prepare_custom_stat(&sender->msg, sender->priv);

    sender->prev_send_time = curr_time;

    ccl_deinit_stat_msg(&sender->msg);
}

/**
 * @brief Stat sending timer expiration callback
 *
 * @param[in] sigval
 * @return void
 */
static void _stat_send_timer_cb(union sigval sigval) {
    _send_stat_impl((ccl_stat_sender_t*)sigval.sival_ptr);
}

ccl_err_t ccl_init_stat_sender(ccl_stat_sender_t *sender,
                               unsigned sender_id,
                               const char *sender_type,
                               time_t stat_send_period,
                               prepare_custom_stat_fn_t prepare_custom_stat_fn,
                               sys_sensors_t *sensors,
                               void *priv,
                               ccl_logger_t *logger)
{
    memset(sender, 0, sizeof(*sender));

    sender->sender_id = sender_id;
    sender->sender_type = sender_type;
    sender->priv = priv;
    sender->prepare_custom_stat = prepare_custom_stat_fn;
    sender->prev_send_time = time(NULL);

    if (sensors) 
        memcpy(&sender->sys_rusage_stat.sys_sensors, sensors, sizeof(*sensors));
    int i;
    for(i=0;i<sender->sys_rusage_stat.sys_sensors.voltage_sensors_num; i++) 
        ccl_syslog_err("%s\n", sender->sys_rusage_stat.sys_sensors.voltage_sensors[i].sysfs_path);
    for(i=0;i<sender->sys_rusage_stat.sys_sensors.temp_sensors_num; i++) 
        ccl_syslog_err("%s\n", sender->sys_rusage_stat.sys_sensors.temp_sensors[i].sysfs_path);

    struct sigevent sev;
    memset(&sev, 0, sizeof(sev));
    sev.sigev_notify = SIGEV_THREAD;
    sev.sigev_notify_function = _stat_send_timer_cb;
    sev.sigev_value.sival_ptr = sender;

    SYSCALL_PANIC_FAILURE_CHECK(
                timer_create(CLOCK_REALTIME, &sev, &sender->timer),
                0, logger,
                "timer_create");

    struct itimerspec itspec;
    memset(&itspec, 0, sizeof(itspec));

    itspec.it_interval.tv_sec = itspec.it_value.tv_sec = stat_send_period;

    SYSCALL_PANIC_FAILURE_CHECK(
                timer_settime(sender->timer, 0, &itspec, NULL),
                0, logger,
                "timer_settime");

    sender->started = CCL_TRUE;

    return CCL_OK;
}

ccl_err_t ccl_update_stat_sender(ccl_stat_sender_t *sender,
                                 time_t stat_send_period,
                                 ccl_logger_t *logger)
{
    struct itimerspec itspec;
    memset(&itspec, 0, sizeof(itspec));

    if (!sender->started) 
        return CCL_OK;

    itspec.it_interval.tv_sec = itspec.it_value.tv_sec = stat_send_period;

    SYSCALL_PANIC_FAILURE_CHECK(
                timer_settime(sender->timer, 0, &itspec, NULL),
                0, logger,
                "timer_settime");

    return CCL_OK;
}

void ccl_destroy_stat_sender(ccl_stat_sender_t *sender)
{
    SYSCALL_PANIC_FAILURE_CHECK(
        timer_delete(sender->timer),
        0, sender->logger,
        "timer_delete");
}
