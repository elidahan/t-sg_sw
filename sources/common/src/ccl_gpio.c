/********************************************************************************/
/**
 * @file ccl_gpio.c
 * @brief GPIO access implementation
 * @copyright 2020 CELARE. All rights reserved.
 */
/********************************************************************************/
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/resource.h>
#include <unistd.h>
#include <ctype.h>
#include <signal.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <assert.h>
#include <pwd.h>
#include <grp.h>
#include <sys/mman.h>
#include <dirent.h>

#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_gpio.h"
#include "ccl_logger.h"

extern ccl_logger_t logger;
static volatile long *gpioaddr;

ccl_err_t ccl_gpio_setup(size_t gpio_block_size, 
                         unsigned long gpio_offset, 
                         unsigned long **gpio_addr)
{
    b_i32  m_mfd;
    if ((m_mfd = open ("/dev/mem", O_RDWR | O_SYNC) ) < 0) {
        ccl_log_error(&logger, "%s(): open /dev/mem error\n", __func__);
        fprintf(stderr,"Error! %s[%d], Unable to open /dev/mem: %s\n", 
                __FUNCTION__, __LINE__, strerror(errno));
        return CCL_FAIL;
    }

    gpioaddr = (unsigned long*)mmap(NULL, gpio_block_size, PROT_READ|PROT_WRITE, 
                                    MAP_SHARED, m_mfd, gpio_offset);
    close(m_mfd);

    if (*gpioaddr == MAP_FAILED) {
        ccl_log_error(&logger, "%s(): mmap error\n", __func__);
        fprintf(stderr,"Error! %s[%d], mmap error %s\n", __FUNCTION__, __LINE__, strerror(errno));
        return CCL_FAIL;
    }

    return CCL_OK;
}

ccl_err_t ccl_gpio_release(unsigned long *gpio_addr, size_t gpio_block_size)
{
    if(munmap((void *)gpioaddr, gpio_block_size) < 0) {
        fprintf(stderr,"munmap failed: %s\n", strerror(errno));
        return CCL_FAIL;
    }
    return CCL_OK;
}


void ccl_gpio_set_direction(unsigned long *gpio_addr, b_i32 gpio_bank, 
                            b_i32 gpio_bank_size, b_i32 gpio, b_i32 direction)
{
    unsigned long value = *(b_u32 *)((b_u8 *)gpioaddr + gpio_bank_size * gpio_bank + 4);

    fprintf(stderr,"%s[%d] - gpioaddr=0x%08x, addr=0x%08x, value=0x%08x, gpio=%d\n", 
            __FUNCTION__, __LINE__, 
            gpioaddr,
            (b_u8 *)gpioaddr + gpio_bank_size * (gpio_bank-1) + 4,
            value, gpio);

    if (direction == 1) /* output */
        value |= (1 << gpio);
    else
        value &= ~(1 << gpio);

    fprintf(stderr,"%s[%d] - value=0x%x, gpio=%d\n", 
            __FUNCTION__, __LINE__, value, gpio);

    *(b_u32 *)((b_u8 *)gpioaddr + gpio_bank_size * (gpio_bank-1) + 4) = value;
}

void ccl_gpio_set(unsigned long *gpio_addr, b_i32 gpio_bank, b_i32 gpio_bank_size, b_i32 gpio)
{
    unsigned long value = *(b_u32 *)((b_u8 *)gpioaddr + gpio_bank_size * (gpio_bank-1));
    fprintf(stderr,"%s[%d] - value=0x%08x, gpio=%d\n", 
            __FUNCTION__, __LINE__, value, gpio);
    value |= (1 << gpio);
    fprintf(stderr,"%s[%d] - value=0x%08x, gpio=%d\n", 
            __FUNCTION__, __LINE__, value, gpio);
    *(b_u32 *)((b_u8 *)gpioaddr + gpio_bank_size * (gpio_bank-1)) = value;
}

void ccl_gpio_clear(unsigned long *gpio_addr, b_i32 gpio_bank, b_i32 gpio_bank_size, b_i32 gpio)
{
    unsigned long value = *(b_u32 *)((b_u8 *)gpioaddr + gpio_bank_size * (gpio_bank-1));
    fprintf(stderr,"%s[%d] - value=0x%08x, gpio=%d\n", 
            __FUNCTION__, __LINE__, value, gpio);
    value &= ~(1 << gpio);
    fprintf(stderr,"%s[%d] - value=0x%08x, gpio=%d\n", 
            __FUNCTION__, __LINE__, value, gpio);
    *(b_u32 *)((b_u8 *)gpioaddr + gpio_bank_size * (gpio_bank-1)) = value;
}

b_i32 ccl_gpio_read(unsigned long *gpio_addr, b_i32 gpio_bank, b_i32 gpio_bank_size, b_i32 gpio)
{
    unsigned long value = *(b_u32 *)((b_u8 *)gpioaddr + gpio_bank_size * (gpio_bank-1));
    fprintf(stderr,"%s[%d] - value=0x%08x, gpio=%d\n", 
            __FUNCTION__, __LINE__, value, gpio);
    return !!(value & (1 << gpio));
}


