/********************************************************************************/
/**
 * @file ccl_util.c
 * @brief Helpers implementation
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/resource.h>
#include <unistd.h>
#include <ctype.h>
#include <signal.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/statvfs.h>
#include <assert.h>
#include <pwd.h>
#include <grp.h>
#include <sys/mman.h>
#include <dirent.h>

#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"

extern ccl_logger_t logger;

ccl_err_t ccl_parse_sysfs_value(const char *filename, unsigned long *val)
{
	FILE *f;
	char buf[BUFSIZ];
	char *end = NULL;

	if ((f = fopen(filename, "r")) == NULL) {
		ccl_syslog_err("%s(): cannot open sysfs value %s\n",
			__func__, filename);
		return CCL_FAIL;
	}

	if (fgets(buf, sizeof(buf), f) == NULL) {
		ccl_syslog_err("%s(): cannot read sysfs value %s\n",
			__func__, filename);
		fclose(f);
		return CCL_FAIL;
	}
	*val = strtoul(buf, &end, 0);
	if ((buf[0] == '\0') || (end == NULL) || (*end != '\n')) {
		ccl_syslog_err("%s(): cannot parse sysfs value %s\n",
				__func__, filename);
		fclose(f);
		return CCL_FAIL;
	}
	fclose(f);
	return CCL_OK;
}

int ccl_strsplit(char *string, int stringlen, 
                 char **tokens, int maxtokens, 
                 char delim)
{
	int i, tok = 0;
	int tokstart = 1; /* first token is right at start of string */

	if (string == NULL || tokens == NULL)
		goto einval_error;

	for (i = 0; i < stringlen; i++) {
		if (string[i] == '\0' || tok >= maxtokens)
			break;
		if (tokstart) {
			tokstart = 0;
			tokens[tok++] = &string[i];
		}
		if (string[i] == delim) {
			string[i] = '\0';
			tokstart = 1;
		}
	}
	return tok;

einval_error:
	errno = EINVAL;
	return -1;
}

int ccl_2s_compl_to_decimal(b_u32 value, b_u8 bits)
{
    b_u32 bitmask = 0;
    b_i32 i;

    for (i = 0; i < bits; i++) 
        bitmask |= (1 << i);

    value &= bitmask;

    if (!(value >> (bits-1) & 1)) 
        return value;

    return (value - (1 << bits));
}
#if 0
void _get_network_if_stats(char *ifname) 
{
    ccl_err_t _ret;
    b_u32 i, j;
    char tx_pkts[100];
    char tx_bytes[100];
    char tx_dropped[100];
    char tx_errors[100]; 
    char rx_pkts[100];
    char rx_bytes[100];
    char rx_dropped[100];
    char rx_errors[100];
    char link_speed[100];
    char link_duplex[100];
    char link_status[100];
    unsigned long val;

    _ret = ccl_parse_sysfs_value("/sys/class/net/%s/statistics/tx_packets", &val);
    /* per HW port statistics */
    for (i = 0; i < probe_config->io_cores_num; i++) {
        lp = &app_linux.instance_params[i];
        for (j = 0; j < lp->io.n_intfs; j++) {
            b_u8 port_id = lp->io.intfs[j].port_num;
            if (ports[port_id].device && lp->io.intfs[j].os_name) {
                struct if_device *device = (struct if_device *)ports[port_id].device;
                sprintf(rx_pkts, "/sys/class/net/%s/statistics/rx_packets", lp->io.intfs[j].os_name);
                sprintf(rx_bytes, "/sys/class/net/%s/statistics/rx_bytes", lp->io.intfs[j].os_name);
                sprintf(rx_dropped, "/sys/class/net/%s/statistics/rx_dropped", lp->io.intfs[j].os_name);

                ptr_file = fopen(rx_pkts, "r");
                if (!ptr_file) {
                    log_error(&logger, "couldn't open file: %s\n", rx_pkts);
                    return;
                }
                if (fgets(buf,200, ptr_file) != NULL) {
                    stats.rx_pkts = strtoull(buf, (char **)NULL, 10);
                }
                fclose(ptr_file);

                ptr_file = fopen(rx_bytes, "r");
                if (!ptr_file) {
                    log_error(&logger, "couldn't open file: %s\n", rx_bytes);
                    return;
                }
                if (fgets(buf,200, ptr_file) != NULL) {
                    stats.rx_bytes = strtoull(buf, (char **)NULL, 10);
                }
                fclose(ptr_file);

                ptr_file = fopen(rx_dropped, "r");
                if (!ptr_file) {
                    log_error(&logger, "couldn't open file: %s\n", rx_dropped);
                    return;
                }
                if (fgets(buf,200, ptr_file) != NULL) {
                    stats.dropped_pkts = strtoull(buf, (char **)NULL, 10);
                }
                fclose(ptr_file);

                perf_mon.if_stats[port_id].rx_pkts = stats.rx_pkts - device->stats.rx_pkts;
                perf_mon.if_stats[port_id].rx_bytes = stats.rx_bytes - device->stats.rx_bytes;
                perf_mon.if_stats[port_id].dropped_pkts = stats.dropped_pkts - device->stats.dropped_pkts;

                log_debug(&logger, "port %d, pkts: %llu bytes: %llu dropped: %llu",
                          i, 
                          perf_mon.if_stats[port_id].rx_pkts,
                          perf_mon.if_stats[port_id].rx_bytes,
                          perf_mon.if_stats[port_id].dropped_pkts);

                device->stats.rx_pkts = stats.rx_pkts;
                device->stats.rx_bytes = stats.rx_bytes;
                device->stats.dropped_pkts = stats.dropped_pkts;

                if (lp->io.intfs[j].name) {
                    strncpy(perf_mon.if_status[port_id].if_name, lp->io.intfs[j].name, sizeof(perf_mon.if_status[port_id].if_name)-1);
                }
                sprintf(link_speed, "/sys/class/net/%s/speed", lp->io.intfs[j].os_name);
                sprintf(link_duplex, "/sys/class/net/%s/duplex", lp->io.intfs[j].os_name);
                sprintf(link_status, "/sys/class/net/%s/operstate", lp->io.intfs[j].os_name);

                ptr_file = fopen(link_speed, "r");
                if (!ptr_file) {
                    log_error(&logger, "couldn't open file: %s\n", link_speed);
                    return;
                }
                if (fgets(buf,200, ptr_file) != NULL) {
                    perf_mon.if_status[port_id].link_speed = strtoull(buf, (char **)NULL, 10);
                }
                fclose(ptr_file);

                ptr_file = fopen(link_duplex, "r");
                if (!ptr_file) {
                    log_error(&logger, "couldn't open file: %s\n", link_duplex);
                    return;
                }
                fgets(perf_mon.if_status[port_id].link_duplex, sizeof(perf_mon.if_status[port_id].link_duplex), ptr_file);
                if (perf_mon.if_status[port_id].link_state[strlen(perf_mon.if_status[port_id].link_duplex)-1] == '\n') {
                    perf_mon.if_status[port_id].link_state[strlen(perf_mon.if_status[port_id].link_duplex)-1] = '\0';
                }
                fclose(ptr_file);

                ptr_file = fopen(link_status, "r");
                if (!ptr_file) {
                    log_error(&logger, "couldn't open file: %s\n", link_status);
                    return;
                }
                fgets(perf_mon.if_status[port_id].link_state, sizeof(perf_mon.if_status[port_id].link_state), ptr_file);
                if (perf_mon.if_status[port_id].link_state[strlen(perf_mon.if_status[port_id].link_state)-1] == '\n') {
                    perf_mon.if_status[port_id].link_state[strlen(perf_mon.if_status[port_id].link_state)-1] = '\0';
                }
                fclose(ptr_file);

                if (lp->io.intfs[j].name) {
                    b_i32 sock;
                    struct ifreq ifr;
                    struct ethtool_cmd ecmd;
                    b_i32 ret;

                    sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_IP);
                    if (sock < 0) {
                        log_error(&logger, "%s[%d] - socket error: %s", __func__, __LINE__, strerror(errno));
                        return;
                    }

                    strncpy(ifr.ifr_name, lp->io.intfs[j].os_name, sizeof(ifr.ifr_name)-1);
                    ifr.ifr_data = &ecmd;

                    ecmd.cmd = ETHTOOL_GSET;

                    ret = ioctl(sock, SIOCETHTOOL, &ifr);
                    if (ret < 0) {
                        close(sock);
                        log_error(&logger, "%s[%d] - ioctl error: %s", __func__, __LINE__, strerror(errno));
                        return;
                    }
                    strncpy(perf_mon.if_status[port_id].link_autoneg, ecmd.autoneg ? "on" : "off", sizeof(perf_mon.if_status[port_id].link_autoneg)-1);
                    close(sock);
                }
            }
        }
    }
}
#endif
/**
 * @brief Gets system running status 
 *  
 * 1. Disk usage 
 * 2. RAM memory usage 
 * 3. 3 most busy processes in terms of RAM memory usage 
 * 4. Per core CPU usage in percentages 
 * 5. RAM memory usage of sensor application 
 * 
 * @return void
 */
static void _get_sysinfo(sys_resources_usage_stat_t *stat)
{
    FILE *ptr_file;
    char procfs_load_avg[] = "/proc/loadavg";
    char procfs_stat[] = "/proc/stat";
    char buf[200];
    char *value, *last;
    b_u32 i = 0;
    double cpu_usage[10];
    char cpu_name[10];
    double overall_ticks = 0;
    b_u64 overall_ticks_interval, idle_ticks_interval;

    char *saveptr;

    stat->cpu_num = sysconf(_SC_NPROCESSORS_ONLN);

    ptr_file = popen("free -k | grep -i mem", "r");
    if (!ptr_file) {
        ccl_syslog_err("%s[%d] - popen() error: %s", __func__, __LINE__, strerror(errno));
        return;
    }
    if (fgets(buf,100, ptr_file) != NULL) {
        /* skip first column */
        value = strtok_r(buf, " ", &saveptr);
        /* total memory */
        value = strtok_r(NULL, " ", &saveptr);
        stat->total_mem = strtoull(value, (char **)NULL, 10);
        /* used memory (total - free - buffers - cache)*/
        value = strtok_r(NULL, " ", &saveptr);
        stat->used_mem = strtoull(value, (char **)NULL, 10);
        /* free memory */
        value = strtok_r(NULL, " ", &saveptr);
        stat->free_mem = strtoull(value, (char **)NULL, 10);
        /* shared memory */
        value = strtok_r(NULL, " ", &saveptr);
        stat->sharedram = strtoull(value, (char **)NULL, 10);
        /* buffered/cached memory */
        value = strtok_r(NULL, " ", &saveptr);
        stat->bufferram = strtoull(value, (char **)NULL, 10);
        stat->avail_mem = stat->total_mem - stat->used_mem;
    }
    fclose(ptr_file);

    ccl_log_debug(&logger, "total memory: %llu, free memory: %llu, shared memory: %llu, buffered memory: %llu, used memory: %llu",
              stat->total_mem,
              stat->free_mem,
              stat->sharedram,
              stat->bufferram,
              stat->used_mem);

    ptr_file = fopen(procfs_load_avg, "r");
    if (!ptr_file) {
        ccl_syslog_err("couldn't open file: %s\n", procfs_load_avg);
        return;
    }
    if (fgets(buf,100, ptr_file) != NULL) {
        value = strtok_r(buf, " ", &saveptr);
        stat->load_avg = atof(value);
        ccl_log_debug(&logger, "load average: %f\n", stat->load_avg);
    }

    fclose(ptr_file);

    ptr_file = fopen(procfs_stat, "r");
    if (!ptr_file) {
        ccl_syslog_err("couldn't open file: %s\n", procfs_stat);
        return;
    }
    while (fgets(buf,200, ptr_file) != NULL) {
        value = strtok_r(buf, " ", &saveptr);
        if (strstr(value, "cpu") != NULL) {
            overall_ticks = 0;
            strcpy(cpu_name, value);
            for (i = 0; i < 10; i++) {
                value = strtok_r(NULL, " ", &saveptr);
                if (value != NULL) {
                    cpu_usage[i] = atof(value);
                    overall_ticks += cpu_usage[i];
                }
            }

            if (!strcmp(cpu_name, "cpu")) {
                overall_ticks_interval = overall_ticks - stat->overall_ticks_prev;
                idle_ticks_interval = cpu_usage[IDLE_TICKS_INDEX] - stat->idle_ticks_prev;
                stat->cpu_usage = (double)(overall_ticks_interval - idle_ticks_interval) * 100 / overall_ticks_interval;

                stat->overall_ticks_prev = overall_ticks;
                stat->idle_ticks_prev = cpu_usage[IDLE_TICKS_INDEX]; 
                break;
            }
        }
    }
    fclose(ptr_file);
}

static void _get_sensors_info(sys_resources_usage_stat_t *stat)
{
    ccl_err_t _ret;
    b_u32 i;
    b_u32 val;

    for (i = 0; i < stat->sys_sensors.voltage_sensors_num; i++) 
        stat->sys_sensors.voltage_sensors[i].value = 0; 

    for (i = 0; i < stat->sys_sensors.temp_sensors_num; i++) 
        stat->sys_sensors.temp_sensors[i].value = 0; 

    for (i = 0; i < stat->sys_sensors.voltage_sensors_num; i++) {
        _ret = ccl_parse_sysfs_value(stat->sys_sensors.voltage_sensors[i].sysfs_path, &val);
        if (_ret) 
            return;
        stat->sys_sensors.voltage_sensors[i].value = (b_double64)val / 1000; 

    }

    for (i = 0; i < stat->sys_sensors.temp_sensors_num; i++) {
        _ret = ccl_parse_sysfs_value(stat->sys_sensors.temp_sensors[i].sysfs_path, &val);
        if (_ret) 
            return;
        stat->sys_sensors.temp_sensors[i].value = (b_double64)val / 1000; 
    }

    return;
}

static void _get_sys_mem_usage(size_t *vmem_size, size_t *rss_size, unsigned granularity) 
{
    size_t page_size = sysconf(_SC_PAGESIZE);

    FILE *fp = NULL;
    if ( (fp = fopen("/proc/self/statm", "r") ) != NULL )
    {
        if(fscanf(fp, "%zd%zd", vmem_size, rss_size) == 2)
        {
            *vmem_size = (*vmem_size * page_size) / granularity;
            *rss_size  =  (*rss_size * page_size) / granularity;
        }
        fclose( fp );
    } else {
        ccl_syslog_err("%s: failed to open /proc/self/statm, error - %s", 
                      __func__, strerror(errno));
        *vmem_size = 0;
        *rss_size  = 0;
    }
}

void ccl_get_sys_mem_usage(size_t *vmem_size, size_t *rss_size, unsigned granularity) 
{
    _get_sys_mem_usage(vmem_size, rss_size, granularity);
}

void ccl_get_sys_res_usage(sys_resources_usage_stat_t *stat) 
{
    _get_sysinfo(stat);
    _get_sensors_info(stat);
}


