/********************************************************************************/
/**
 * @file ccl_intr.h
 * @brief Defines celare interrupt handler
 * @date 2019
 * @version 1.0
 * @copyright 2019 CELARE. All rights reserved.
 *
 **/
/********************************************************************************/
#ifndef __CCL_INTR_H__
#define __CCL_INTR_H__

/**
 * This data type defines an interrupt handler for a device.
 * The argument points to the instance of the component
 */
typedef void (*ccl_interrupt_handler) (void *instance);

#endif /* __CCL_INTR_H__ */
