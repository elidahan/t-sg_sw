/********************************************************************************/
/**
 * @file ccl_logger.h
 * @brief Logger definition and interface declaration.
 * @copyright 2016 CELARE. All rights reserved.
 */
/********************************************************************************/

#ifndef __LOGGER_H__
#define __LOGGER_H__

#include <stdio.h>
#include <pthread.h>
#include <time.h>
#include <unistd.h>

#define EXIT_STATUS_PANIC 3
#define EXIT_STATUS_WRONG_CFG CCL_WRONG_CONFIG

typedef void *(*logger_callback_fn_t)(void *);

/**
  * @brief Logger log levels.
  *
  * Logger log levels.
  */
enum log_level {
    LOG_LVL_EMERGENCY = 0, /**< emergency messages */
    LOG_LVL_ALERT,         /**< alert messages */
    LOG_LVL_CRITICAL,      /**< critical messages */
    LOG_LVL_ERROR,         /**< error messages */
    LOG_LVL_WARNING,       /**< warning messages */
    LOG_LVL_NOTICE,        /**< notice messages */
    LOG_LVL_INFO,          /**< info messages */
    LOG_LVL_DEBUG,         /**< debug messages */
};

/**
  * @brief Logger configuration.
  *
  * Logger configuration
  */
typedef struct {
    const char *log_file;            /**< log file */
    const char *module_name;         /**< module name */
    enum log_level log_lvl;          /**< log level */
    enum log_level log_syslog_lvl;   /**< log syslog level */
} ccl_logger_conf_t;

/**
  * @brief Logger structure.
  *
  * Logger structure.
  */
typedef struct {
    FILE *log_file;                        /**< Log file for log owner */ 
    enum log_level log_lvl;                /**< Log level for log owner */ 
    enum log_level log_syslog_lvl;         /**< Log level for system syslog */
    const char *module_name;               /**< Owner */
    logger_callback_fn_t f_cb;             /**< Callback function that will be called upon a log event */
    pthread_mutex_t ratelimit_table_lock; 
    void *ratelimit_table;                 /**< log messages storage */
    time_t log_msg_pr_interval;            /**< log message printing interval */
} ccl_logger_t;

/**
 * @brief Creates logger.
 * Creates logger from ccl_logger_conf_t struct.
 * @param[in]  cfg - logger config struct
 * @return ccl_logger_t
 * @note logger should be destroyed by destroy_logger() function
 */
ccl_logger_t ccl_create_logger(const ccl_logger_conf_t *cfg);

/**
 * @brief Registers logger callback that will be called upon a 
 * log event
 * @param[in] logger logger object
 * @param[in] logger_cb callback function 
 * @return void
 */
void ccl_register_logger_cb(ccl_logger_t *logger, logger_callback_fn_t logger_cb);

/**
 * @brief Destroys logger.
 * Destroys logger.
 * @param[in]  logger
 * @return void
 */
void ccl_destroy_logger(ccl_logger_t *logger);

#define LOG_BASIC_RATELIMITED(level) void log_##level##_ratelimited(ccl_logger_t *logger, const char *message, ...)

LOG_BASIC_RATELIMITED(debug);
LOG_BASIC_RATELIMITED(info);
LOG_BASIC_RATELIMITED(notice);
LOG_BASIC_RATELIMITED(warn);
LOG_BASIC_RATELIMITED(error);
LOG_BASIC_RATELIMITED(alert);
LOG_BASIC_RATELIMITED(crit);
LOG_BASIC_RATELIMITED(emerg);
LOG_BASIC_RATELIMITED(panic);

#undef LOG_BASIC_RATELIMITED

void ccl_log_debug(ccl_logger_t *logger, const char* message, ...);
void ccl_log_info(ccl_logger_t *logger, const char* message, ...);
void ccl_log_notice(ccl_logger_t *logger, const char* message, ...);
void ccl_log_warn(ccl_logger_t *logger, const char* message, ...);
void ccl_log_error(ccl_logger_t *logger, const char* message, ...);
void ccl_log_alert(ccl_logger_t *logger, const char* message, ...);
void ccl_log_crit(ccl_logger_t *logger, const char* message, ...);
void ccl_log_emerg(ccl_logger_t *logger, const char* message, ...);
void ccl_log_panic(ccl_logger_t *logger, const char* message, ...);

void ccl_set_log_level(ccl_logger_t *logger, enum log_level new_log_lvl);
void ccl_set_log_syslog_level(ccl_logger_t *logger, enum log_level new_log_lvl);

void ccl_syslog_err(const char* message, ...);

/**
 * @brief Prints warning log message to syslog.
 * This function is used in order to print log messages before logger object is being created
 * @param[in]  message
 * @return void
 */
void ccl_syslog_warning(const char* message, ...);

/**
 * @brief Change owner of log file, requires CAP_CHOWN capability
 *
 * @param[in] logger - logger object
 * @param[in] uid - user id 
 * @param[in] gid - group id
 * @return void
 */
ccl_err_t ccl_log_file_chown(ccl_logger_t *logger, uid_t uid, gid_t gid);

/**
 * @brief Cleanup logger conf.
 *
 * @param[in]  conf
 * @return void
 */
void ccl_destroy_logger_conf(ccl_logger_conf_t *conf);

#include <string.h>
#include <errno.h>

#define MEMORY_ALLOCATION_FAILURE_CHECK(value, logger_ptr, alloc_fn) \
    if (!value) ccl_log_panic(logger_ptr, "%s: "alloc_fn" failed, error - %s", __func__, strerror(errno));
#define SYSCALL_PANIC_FAILURE_CHECK(expr, expected, logger_ptr, syscall) \
    if ((expr) != expected) ccl_log_panic(logger_ptr, "%s: "syscall" failed, error - %s", __func__, strerror(errno));

#define PTHREAD_FAILURE_PANIC_CHECK(logger_ptr, pthread_fn, args...) \
{ \
    int ret = pthread_fn(args);\
    if (ret != 0) {\
        ccl_log_panic(logger_ptr, "%s: "#pthread_fn" failed, error - %s", __func__, strerror(ret)); \
    }\
}

#endif /* __LOGGER_H__ */
