/********************************************************************************/
/**
 * @file ccl_types.h
 * @brief Defines tsense data types
 * @date June 26, 2016
 * @version 1.0
 * @copyright 2016 CELARE. All rights reserved.
 *
 **/
/********************************************************************************/

#ifndef __CCL_TYPES_H__
#define __CCL_TYPES_H__

#define MIN_INT8        (-128)		/**< minimum value for b_i8 type */
#define MAX_INT8        (127)		/**< maximum value for b_i8 type */
#define MIN_INT16       (-32768)	/**< minimum value for b_i16 type */
#define MAX_INT16       (32767)		/**< maximum value for b_i16 type */
#define MIN_INT32       (-2147483648)	/**< minimum value for b_i32 type */
#define MAX_INT32       (2147483647)	/**< maximum value for b_i32 type */
#define MIN_INT64       (-9223372036854775808ll)	/**< minimum value for b_i64 type */
#define MAX_INT64       (9223372036854775807ll)	/**< maximum value for b_i64 type */
#define MIN_UINT8       (0x00)		/**< minimum value for b_u8 type */
#define MAX_UINT8       (0xFF)		/**< maximum value for b_u8 type */
#define MIN_UINT16      (0x0000)	/**< minimum value for b_u16 type */	
#define MAX_UINT16      (0xFFFF)	/**< maximum value for b_u16 type */
#define MIN_UINT32      (0x00000000)	/**< minimum value for b_u32 type */
#define MAX_UINT32      (0xFFFFFFFF)	/**< maximum value for b_u32 type */
#define MIN_UINT32LL    (0x00000000LL)	/**< minimum value for b_u32 type, LL notation */
#define MAX_UINT32LL    (0xFFFFFFFFLL)	/**< maximum value for b_u32 type, LL notation */
#define MIN_UINT64      (0x0000000000000000LL)	/**< minimum value for b_u64 type */
#define MAX_UINT64      (0xFFFFFFFFFFFFFFFFLL)	/**< maximum value for b_u64 type */
#define MIN_UCHAR       (0x00)
#define MAX_UCHAR       (0xFF)
#define MIN_WCHAR       (0)
#define MAX_WCHAR       MAX_UINT16
#define MIN_ERRCODE     MIN_INT32

#ifndef CCL_BUFSIZ
#define CCL_BUFSIZ 1024
#endif

#define DEFAULT_NAME_LEN 32
#define DEFAULT_PATH_LEN 128

#define BILLION 1000000000L

#undef  IN
#define IN      /**< nothing - used for function parameter documentation */
#undef  OUT
#define OUT     /**< nothing - used for function parameter documentation */
#undef  INOUT
#define INOUT   /**< nothing - used for function parameter documentation */

typedef     signed int          b_i32; 	/**< 32 bit signed type */
typedef     unsigned int        b_u32;	/**< 32 bit unsigned type */

typedef     signed char         b_i8;	/**< 8 bit signed type */
typedef     unsigned char       b_u8;	/**< 8 bit unsigned type */

typedef     signed short        b_i16;	/**< 16 bit signed type */
typedef     unsigned short      b_u16;	/**< 16 bit unsigned type */

typedef     signed long long    b_i64;	/**< 64 bit signed type */
typedef     unsigned long long  b_u64;	/**< 64 bit unsigned type */

typedef     unsigned char       b_Boolean;	/**< Boolean type: CCL_TRUE, CCL_FALSE */
typedef     unsigned char       b_bool;	/**< Boolean type: CCL_TRUE, CCL_FALSE */

typedef     b_u32               b_size_t;/**< size type, 32 bit unsigned */

typedef float                   b_float32;  /**< 32 bit single-precision floating-point type (IEEE 754) */ 
typedef double                  b_double64; /**< 64 bit double-precision floating-point type (IEEE 754) */

#define MAX_U64_INT_STR_LEN 20

#undef NULL
#define NULL 0

#define	CCL_FALSE	((b_bool)0)		/**< boolen TRUE value */
#define	CCL_TRUE	(!CCL_FALSE)	/**< boolen FALSE value */

#define	CCL_MIN(a,b)	((a)<(b)?(a):(b))	/**< Macro for getting min value of two variables */
#define	CCL_MAX(a,b)	((a)>(b)?(a):(b)) /**< Macro for getting max value of two variables */

/** TSense return codes 
*/
typedef enum {
    CCL_OK               = 0,  /**< Operation was completed successfully.*/
    CCL_FAIL             = 1,  /**< Operation was not performed.*/
    CCL_WRONG_CONFIG     = 2,  /**< "Module configuration is wrong"*/
    CCL_NO_MEM_ERR       = 3,  /**< Not enough memory to perform the operation.*/
    CCL_NOT_FOUND        = 4,  /**< Key not found by search*/
    CCL_NOT_MORE         = 5,  /**< There are no more elements in list*/
    CCL_ALREADY_EXISTS   = 6,  /**< There is already exists element in list*/
    CCL_NOT_DEFINED      = 7,  /**< value undefined*/
    CCL_NOT_INIT_ERR     = 8,  /**< Module is not initialized */ 
    CCL_ERRNO            = 9,  /**< The real error code can be obtain via errno*/
    CCL_CONTINUE_WALK    = 10, /**< Continue to iterate */
    CCL_NOT_SUPPORT      = 11, /**< Not supported */
    CCL_INTR_ERR         = 12, /**< Internal error means an assert in release version */
    CCL_TIMEOUT          = 13, /**< Timeout */	
    CCL_NO_PERMISSION    = 14, /**< No permission */
    CCL_NO_TRAFFIC       = 15, /**< No traffic */
    CCL_IN_PROGRESS      = 16, /**< Operation is in progress */
    CCL_CONNECT_FAILED   = 17, /**< Connection to the member failed */
    CCL_DECOMPRESS_FAIL  = 18, /**< Failed to decompress data*/
    CCL_AUTH_FAILED      = 19, /**< Failed to authenticate*/
    CCL_BAD_PARAM_ERR    = 20, /**< Input parameters are not correct or out of range.*/
    CCL_BUSY             = 21, /**< Module is busy - operation can not be completed */       
	CCL_ERROR_MAX              /**< MAX error ID */
} ccl_err_t;

#define CCL_ERROR_RETURN(R) if(CCL_OK != (R)) return (R); /**< Return if code is not CCL_OK */

#define MAC_ADDR_BYTES_SIZE 6
#define MAC_ADDR_VENDOR_ID_SIZE 3

typedef struct __attribute__((packed)) {
    unsigned char mac[MAC_ADDR_BYTES_SIZE];
} mac_t;

#endif /* __CCL_TYPES_H__ */

