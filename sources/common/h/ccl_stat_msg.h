/********************************************************************************/
/**
 * @file stat_msg.h
 * @brief Simple wrapper around cJSON to work with statistics messages
 * @copyright 2020 CELARE. All rights reserved.
 */
/********************************************************************************/

#ifndef _CCL_STAT_MSG_H_
#define _CCL_STAT_MSG_H_

#include "cJSON.h"

typedef struct {
    cJSON *msg;
} ccl_stat_msg_t;

/**
 * @brief Initialize statistics message object
 * Creates new cJSON object with index and type fields
 * @param[out] stat_msg
 * @param[in]  id - sender id
 * @param[in]  stat_type - stat type
 * @return void
 * @note should be deinitialized with deinit_stat_msg()
 */
void ccl_init_stat_msg(ccl_stat_msg_t *stat_msg,
                       const unsigned id,
                       const char *stat_type);

/**
 * @brief Add numeric field to stat msg
 * Wrapper around cJSON_AddItemToObject and cJSON_CreateNumber
 * @param[in]  stat_msg
 * @param[in]  prefix 
 * @param[in]  field_name
 * @param[in]  value
 * @return void
 */
void ccl_stat_msg_add_numeric_field(ccl_stat_msg_t *stat_msg,
                                    const char *prefix,
                                    const char *field_name,
                                    double value);

/**
 * @brief Add string field to stat msg
 * Wrapper around cJSON_AddItemToObject and cJSON_CreateString
 * @param[in]  stat_msg 
 * @param[in]  prefix 
 * @param[in]  field_name
 * @param[in]  value
 * @return void
 */
void ccl_stat_msg_add_string_field(ccl_stat_msg_t *stat_msg,
                                   const char *prefix,
                                   const char *field_name,
                                   const char *value);

/**
 * @brief Add cJSON object field to stat msg
 * Wrapper around cJSON_AddItemToObject
 * @param[in]  stat_msg
 * @param[in]  prefix 
 * @param[in]  field_name
 * @param[in]  value
 * @return void
 */
void ccl_stat_msg_add_custom_json_field(ccl_stat_msg_t *stat_msg,
                                        const char *prefix,
                                        const char *field_name,
                                        cJSON *obj);

/**
 * @brief Print unformatted json stat
 *
 * @param[in]  stat_msg
 * @return string which should be deallocated by free()
 */
char *ccl_stat_msg_to_string_unformatted(ccl_stat_msg_t *stat_msg);

/**
 * @brief Print formatted json stat
 *
 * @param[in]  stat_msg
 * @return string which should be deallocated by free()
 */
char *ccl_stat_msg_to_string(ccl_stat_msg_t *stat_msg);

/**
 * @brief ccl_stat_msg_t destructor
 *
 * @param[in]  stat_msg
 * @return void
 */
void ccl_deinit_stat_msg(ccl_stat_msg_t *stat_msg);

#endif /*_CCL_STAT_MSG_H_ */
