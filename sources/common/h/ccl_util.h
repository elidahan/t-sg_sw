/********************************************************************************/
/**
 * @file ccl_util.h
 * @brief helpers definition
 * @copyright 2019 CELARE. All rights reserved.
 */
/********************************************************************************/
#ifndef __CCL_UTIL_H__
#define __CCL_UTIL_H__

#include <stdio.h>
#include <sys/time.h>

#define STRINGIFY(x) #x
#define STRINGIFY_VALUE_OF(x) STRINGIFY(x)

#define PROFILE_START                  \
struct timespec start;                 \
struct timespec end;                   \
clock_gettime(CLOCK_MONOTONIC, &start);

#define PROFILE_END(logger_ptr, what)                      \
        clock_gettime(CLOCK_MONOTONIC, &end);              \
        b_double64 diff = timedifference_nsec(end, start); \
        log_debug(logger_ptr, "%s: Profile: %f seconds spent to %s", __func__, diff / 10e9, what);

#define DUMPBUF(buf, sz, cl)              \
        for (int i = 0; i < (sz); i++) {  \
            PRINTL("%02x ", buf[i]);      \
            if (!((i+1) % cl))            \
                PRINTL("\n");             \
        }                                 \
        PRINTL("\n");

#define GET_STATIC_ARRAY_SIZE(array) (sizeof(array) / sizeof(array[0]))

typedef struct {
    b_double64    cpu_usage;
    b_double64    kernel_cpu_usage;
    b_double64    user_cpu_usage;

    b_u64          prev_cpu_tu;      
    b_u64          prev_cpu_user_u;  
    b_u64          prev_cpu_kernel_u;
    struct timeval prev_snapshot_ts;
    size_t         rss;
    size_t         vmem;
} sys_res_usages_stat_t;

#define PROC_NUM 3
/* per cpu idle ticks index in /proc/stat file */
#define IDLE_TICKS_INDEX 3

#define SENSORS_MAXNUM 10

/**
 * Structure used for tracking most memory consuming processes
 */
typedef struct {
    char        name[DEFAULT_NAME_LEN];     /**< process name */
    b_u64       abs_mem;  /**< memory used in KBytes */
    b_double64 rel_mem;    /**< memory used in percentages */
} proc_stats_t;

typedef struct {
    char        name[DEFAULT_NAME_LEN];
    char        sysfs_path[DEFAULT_PATH_LEN];
    b_double64  value;
} sensor_t;

typedef struct {
    sensor_t voltage_sensors[SENSORS_MAXNUM];
    b_u32 voltage_sensors_num;
    sensor_t temp_sensors[SENSORS_MAXNUM];
    b_u32 temp_sensors_num;
} sys_sensors_t;
/**
 * Structure used to collect
 * system monitored data 
 */
typedef struct {
    b_double64 load_avg;                       /**< system load average */                                                                   
    b_double64 cpu_usage;                      /**< system cpu usage */                                                                     
    b_u64 overall_ticks_prev;                  /**< overall number of cpu clock ticks (jiffies) as counted in previous monitored interval */
    b_u64 idle_ticks_prev;                     /**< number of jiffies for idle cycles as counted in previous monitored interval */          
    b_u32 cpu_num;                             /**< number of logical cpu cores in system */                                                
    b_double64 used_size_perc;                 /**< ramdisk used percentage */                                                              
    b_u64 total_mem;                           /**< RAM total memory in system (KBytes) */                                                  
    b_u64 free_mem;                            /**< RAM free memory (KBytes) */                                                             
    b_u64 used_mem;                            /**< RAM used memory (KBytes) */                                                             
    b_u64 sharedram;                           /**< RAM shared memory (KBytes) */                                                           
    b_u64 bufferram;                           /**< RAM buffered memory (KBytes) */                                                         
    b_u64 avail_mem;                           /**< RAM total - used (KBytes)*/                                                             
    proc_stats_t pstat[PROC_NUM];              /**< structure used for tracking most memory consuming processes */                          
    sys_sensors_t sys_sensors;
} sys_resources_usage_stat_t;


/** @brief Read a single numeric value from a file
 * on the filesystem. Used to read information from files on 
 * /sys 
 * @param[in] filename file to read from 
 * @param[out] val read value 
 * @return CCL_OK on success, CCL_FAIL on failure  
 */ 
ccl_err_t ccl_parse_sysfs_value(const char *filename, unsigned long *val);

/**
 * @brief Takes string "string" parameter and splits it at 
 * character "delim" up to maxtokens-1 times - to give 
 * "maxtokens" resulting tokens. Like strtok or strsep 
 * functions, this modifies its input string, by replacing 
 * instances of "delim" with '\\0'. All resultant tokens are 
 * returned in the "tokens" array which must have enough entries 
 * to hold "maxtokens". 
 *
 * @param[inout] string input string to be split into tokens 
 *
 * @param[in] stringlen max length of the input buffer 
 *
 * @param[out] tokens array to hold the pointers to the tokens 
 *            in the string
 *
 * @param[in] maxtokens number of elements in the tokens array. 
 *            At most, maxtokens-1 splits of the string will be
 *            done.
 *
 * @param[in] delim character on which the split of the data
 *            will be done
 *
 * @return The number of tokens in the tokens array. 
 */
int ccl_strsplit(char *string, int stringlen, 
                 char **tokens, int maxtokens, 
                 char delim);


/** 
 * @brief Converts 2's complement number to decimal 
 *        representation
 * @param[in] value number to convert 
 * @param[in] bits number of bits 
 *  
 * @return converted number
*/
int ccl_2s_compl_to_decimal(b_u32 value, b_u8 bits);

/**
 * @brief Retrieves sys memory usage statistics.
 * Memory usage is being retrieved from /proc/self/statm
 * @param[out] vmem_size
 * @param[out] rss_size
 * @param[in]  granularity
 * @return void
 */
void ccl_get_sys_mem_usage(size_t *vmem_size, size_t *rss_size, unsigned granularity);

/**
 * @brief Retrieves sys resources usage statistics.
 * Retrieves sys resources usage statistics such as cpu usage, 
 * rss and virtual memory size. 
 * @param[out]  stat statistics structure
 * @return void
 */
void ccl_get_sys_res_usage(sys_resources_usage_stat_t *stat);


#endif /* __CCL_UTIL_H__ */

