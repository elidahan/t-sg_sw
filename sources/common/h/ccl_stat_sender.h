/********************************************************************************/
/**
 * @file stat_sender.h
 * @brief API to implement generic statistics sender class
 * @copyright 2020 CELARE. All rights reserved.
 */
/********************************************************************************/

#ifndef _CCL_STAT_SENDER_H_
#define _CCL_STAT_SENDER_H_

#include <time.h>
#include <signal.h>

typedef void (*prepare_custom_stat_fn_t)(ccl_stat_msg_t *msg, void *priv);

typedef struct {
    timer_t timer;

    prepare_custom_stat_fn_t prepare_custom_stat;
    void *priv;

    ccl_stat_msg_t msg;

    unsigned sender_id;
    const char *sender_type;
    ccl_logger_t *logger;
    time_t prev_send_time;
    b_bool started;
    sys_resources_usage_stat_t sys_rusage_stat;
} ccl_stat_sender_t;

/**
 * @brief Initialize statistics sender object
 *
 * @param[out] sender
 * @param[in]  sender_id
 * @param[in]  sender_type - used as "type"
 * @param[in]  stat_send_period
 * @param[in]  prepare_custom_stat_fn - function to add custom fields to stat, not NULL 
 * @param[in]  system_sensors - used by prepare_custom_stat_fn 
 * @param[in]  priv - function argument 
 * @param[in]  logger logger object
 * @return TS_OK on success, other code on error
 * @note should be deinitialized with destroy_stat_sender()
 */
ccl_err_t ccl_init_stat_sender(ccl_stat_sender_t *sender,
                               unsigned sender_id,
                               const char *sender_type,
                               time_t stat_send_period,
                               prepare_custom_stat_fn_t prepare_custom_stat_fn,
                               sys_sensors_t *sensors,
                               void *priv,
                               ccl_logger_t *logger);


/**
 * @brief Update statistics send period
 *
 * @param[in] sender
 * @param[in]  stat_send_period
 * @param[in]  logger logger object
 * @return TS_OK on success, other code on error
 * @note should be deinitialized with destroy_stat_sender()
 */
ccl_err_t ccl_update_stat_sender(ccl_stat_sender_t *sender,
                                 time_t stat_send_period,
                                 ccl_logger_t *logger);

/**
 * @brief Destroy statistics sender object
 *
 * @param[void] sender
 * @return void
 */
void ccl_destroy_stat_sender(ccl_stat_sender_t *sender);
 
#endif /* _CCL_STAT_SENDER_H_ */
