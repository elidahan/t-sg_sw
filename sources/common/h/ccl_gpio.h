/********************************************************************************/
/**
 * @file ccl_gpio.h
 * @brief GPIO access implementation
 * @copyright 2020 CELARE. All rights reserved.
 */
/********************************************************************************/

#ifndef __CCL_GPIO_H__
#define __CCL_GPIO_H__

ccl_err_t ccl_gpio_setup(size_t gpio_block_size, 
                         unsigned long gpio_offset, 
                         unsigned long **gpio_addr);
ccl_err_t ccl_gpio_release(unsigned long *gpio_addr, 
                           size_t gpio_block_size);
void ccl_gpio_set_direction(unsigned long *gpio_addr, 
                            b_i32 gpio_bank,                        
                            b_i32 gpio_bank_size, 
                            b_i32 gpio, 
                            b_i32 direction);
void ccl_gpio_set(unsigned long *gpio_addr, 
                  b_i32 gpio_bank,                   
                  b_i32 gpio_bank_size, 
                  b_i32 gpio);
void ccl_gpio_clear(unsigned long *gpio_addr, 
                    b_i32 gpio_bank, 
                    b_i32 gpio_bank_size, 
                    b_i32 gpio);
b_i32 ccl_gpio_read(unsigned long *gpio_addr, 
                    b_i32 gpio_bank, 
                    b_i32 gpio_bank_size, 
                    b_i32 gpio);

#endif /*__CCL_GPIO_H__*/
