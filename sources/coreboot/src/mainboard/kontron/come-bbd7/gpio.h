/*
 * This file is part of the coreboot project.
 *
 * Copyright (C) 2019 Kontron Modular Computers SAS
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#ifndef BBD7_GPIO_H_
#define BBD7_GPIO_H_

#include <soc/gpio.h>

static const struct gpio_config bbd7_gpio_config[] = {
		PCH_GPIO_INPUT(0),
		PCH_GPIO_OUT_LOW(1),
		PCH_GPIO_OUT_LOW(2),
		PCH_GPIO_INPUT_INVERT(3),
		PCH_GPIO_OUT_LOW(4), /* KDP #25624: DMI link x4 */
		PCH_GPIO_INPUT(5),
		PCH_GPIO_INPUT_INVERT(6),
                PCH_GPIO_INPUT_INVERT_NMI(7),
		PCH_GPIO_INPUT_INVERT(8),
		PCH_GPIO_NATIVE(9),
		PCH_GPIO_NATIVE(10),
		PCH_GPIO_INPUT_INVERT(11),
		PCH_GPIO_OUT_LOW(12),
		PCH_GPIO_NATIVE(14),
		PCH_GPIO_OUT_HIGH(15),
		PCH_GPIO_OUT_LOW(16),
		PCH_GPIO_OUT_LOW(17),
		PCH_GPIO_INPUT(18),
		PCH_GPIO_INPUT(19),
		PCH_GPIO_NATIVE(20),
		PCH_GPIO_OUT_LOW(21),
		PCH_GPIO_INPUT(22),
		PCH_GPIO_INPUT(23),
		PCH_GPIO_OUT_HIGH(24),
		PCH_GPIO_OUT_LOW(25),
		PCH_GPIO_OUT_LOW(26),
		PCH_GPIO_INPUT(27),
		PCH_GPIO_OUT_LOW(28),
		PCH_GPIO_OUT_LOW(29),
		PCH_GPIO_NATIVE(30),
		PCH_GPIO_INPUT(31),
		PCH_GPIO_OUT_LOW(32),
		PCH_GPIO_OUT_LOW(33),
		PCH_GPIO_NATIVE(35),
		PCH_GPIO_OUT_LOW(36),
		PCH_GPIO_INPUT(37),
		PCH_GPIO_INPUT(38),
		PCH_GPIO_OUT_LOW(39),
		PCH_GPIO_NATIVE(40),
		PCH_GPIO_NATIVE(41),
		PCH_GPIO_NATIVE(42),
		PCH_GPIO_NATIVE(43),
		PCH_GPIO_OUT_LOW(44),
		PCH_GPIO_OUT_LOW(45),
		PCH_GPIO_OUT_LOW(46),
		PCH_GPIO_OUT_LOW(48),
		PCH_GPIO_OUT_LOW(49),
		PCH_GPIO_OUT_LOW(50),
		PCH_GPIO_INPUT(51),
		PCH_GPIO_OUT_LOW(52),
		PCH_GPIO_INPUT(53),
		PCH_GPIO_OUT_LOW(54),
		PCH_GPIO_OUT_LOW(55),
		PCH_GPIO_INPUT(57),
		PCH_GPIO_NATIVE(58),
		PCH_GPIO_NATIVE(59),
		PCH_GPIO_INPUT(60),
		PCH_GPIO_NATIVE(61),
		PCH_GPIO_NATIVE(62),
		PCH_GPIO_OUT_LOW(65),
		PCH_GPIO_OUT_LOW(66),
		PCH_GPIO_OUT_LOW(67),
		PCH_GPIO_OUT_LOW(68),
		PCH_GPIO_OUT_LOW(69),
		PCH_GPIO_OUT_LOW(70),
		PCH_GPIO_OUT_LOW(71),
                PCH_GPIO_INPUT(72),
                PCH_GPIO_NATIVE(74),
                PCH_GPIO_NATIVE(75),
		PCH_GPIO_END
};

#endif /* BBD7_GPIO_H_ */
