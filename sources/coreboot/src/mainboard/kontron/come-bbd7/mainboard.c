/*
 * This file is part of the coreboot project.
 *
 * Copyright (C) 2007-2009 coresystems GmbH
 * Copyright (C) 2011 Google Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <types.h>
#include <device/device.h>
#include <console/console.h>
#include <device/pci_ids.h>
#include <device/pci_def.h>
#include <device/pci_ops.h>
#if CONFIG(VGA_ROM_RUN)
#include <x86emu/x86emu.h>
#endif
#include <arch/interrupt.h>
#include <boot/coreboot_tables.h>
/* Since SPI controller opcode registers are locked by FSP, they need to be
initialized to a known good state before ReadyToBoot event and after
every SPI flash access (e.g. for MRC cache) has been finished in order
to enable the OS to use SPI controller without constraints.
*/
#include <soc/pci_devs.h>

/*
 * SPI Opcode Menu setup for SPIBAR lock down
 * should support most common flash chips.
 */
#define SPI_OPCODE_TYPE_READ_NO_ADDRESS         0
#define SPI_OPCODE_TYPE_WRITE_NO_ADDRESS        1
#define SPI_OPCODE_TYPE_READ_WITH_ADDRESS       2
#define SPI_OPCODE_TYPE_WRITE_WITH_ADDRESS      3

#define SPI_OPCODE_WRSR         0x01
#define SPI_OPCODE_PAGE_PROGRAM 0x02
#define SPI_OPCODE_READ         0x03
#define SPI_OPCODE_WRDIS        0x04
#define SPI_OPCODE_RDSR         0x05
#define SPI_OPCODE_WREN         0x06
#define SPI_OPCODE_FAST_READ    0x0b
#define SPI_OPCODE_ERASE_SECT   0x20
#define SPI_OPCODE_READ_ID      0x9f
#define SPI_OPCODE_ERASE_BLOCK  0xd8

#define SPI_OPMENU_0 SPI_OPCODE_PAGE_PROGRAM /* BYPR: Byte Program */
#define SPI_OPTYPE_0 SPI_OPCODE_TYPE_WRITE_WITH_ADDRESS /* Write, address required */

#define SPI_OPMENU_1 SPI_OPCODE_READ /* READ: Read Data */
#define SPI_OPTYPE_1 SPI_OPCODE_TYPE_READ_WITH_ADDRESS /* Read, address required */

#define SPI_OPMENU_2 SPI_OPCODE_ERASE_SECT /* SE20: Sector Erase 0x20 */
#define SPI_OPTYPE_2 SPI_OPCODE_TYPE_WRITE_WITH_ADDRESS /* Write, address required */

#define SPI_OPMENU_3 SPI_OPCODE_RDSR /* RDSR: Read Status Register */
#define SPI_OPTYPE_3 SPI_OPCODE_TYPE_READ_NO_ADDRESS /* Read, no address */

#define SPI_OPMENU_4 SPI_OPCODE_READ_ID /* RDID: Read ID */
#define SPI_OPTYPE_4 SPI_OPCODE_TYPE_READ_NO_ADDRESS /* Read, no address */

#define SPI_OPMENU_5 SPI_OPCODE_WRSR /* WRSR: Write Status Register */
#define SPI_OPTYPE_5 SPI_OPCODE_TYPE_WRITE_NO_ADDRESS /* Write, no address */

#define SPI_OPMENU_6 SPI_OPCODE_FAST_READ /* READ FAST: Read Data */
#define SPI_OPTYPE_6 SPI_OPCODE_TYPE_READ_WITH_ADDRESS /* Read, address required */

#define SPI_OPMENU_7 SPI_OPCODE_WRDIS /* Write Disable */
#define SPI_OPTYPE_7 SPI_OPCODE_TYPE_WRITE_NO_ADDRESS /* Write, no address */

#define SPI_OPPREFIX    ((SPI_OPCODE_WREN << 8) | SPI_OPCODE_WREN)

#define SPI_OPTYPE ((SPI_OPTYPE_7 << 14) | (SPI_OPTYPE_6 << 12) | \
                    (SPI_OPTYPE_5 << 10) | (SPI_OPTYPE_4 << 8) | \
                    (SPI_OPTYPE_3 << 6) | (SPI_OPTYPE_2 << 4) | \
                    (SPI_OPTYPE_1 << 2) | (SPI_OPTYPE_0))

#define SPI_OPMENU_UPPER ((SPI_OPMENU_7 << 24) | (SPI_OPMENU_6 << 16) | \
                          (SPI_OPMENU_5 << 8) | SPI_OPMENU_4)
#define SPI_OPMENU_LOWER ((SPI_OPMENU_3 << 24) | (SPI_OPMENU_2 << 16) | \
                          (SPI_OPMENU_1 << 8) | SPI_OPMENU_0)

#define SPIBAR_OFFSET           0x3800
#define SPI_REG_PREOP           0x94
#define SPI_REG_OPTYPE          0x96
#define SPI_REG_OPMENU_L        0x98
#define SPI_REG_OPMENU_H        0x9c

/* USB */
#define LPTH_USB_MAX_PHYSICAL_PORTS         14
#define LPTH_XHCI_MAX_USB3_PORTS            6

#define R_PCH_XHCI_U2OCM1                   0xC0

#define R_PCH_XHCI_U2OCM2                   0xC4

#define R_PCH_XHCI_U3OCM1                   0xC8

#define R_PCH_XHCI_U3OCM2                   0xCC

#define R_PCH_XHCI_XHCC2                    0x44




#define R_PCH_RCRB_IOBPD		0x2334
#define R_PCH_RCRB_IOBPS		0x2338
#define BIT0				0x00000001
#define B_PCH_RCRB_IOBPS_BUSY		0x00000001
#define B_PCH_RCRB_IOBPS		0x00000006
#define R_PCH_RCRB_IOBPIRI		0x2330
#define B_PCH_RCRB_IOBPS_IOBPIA		0xFF00
#define V_PCH_RCRB_IOBPS_IOBPIA_R       0x0600
#define V_PCH_RCRB_IOBPS_IOBPIA_W	0x0700

typedef enum {
	PchUsbOverCurrentPin0 = 0,
	PchUsbOverCurrentPin1,
	PchUsbOverCurrentPin2,
	PchUsbOverCurrentPin3,
	PchUsbOverCurrentPin4,
	PchUsbOverCurrentPin5,
	PchUsbOverCurrentPin6,
	PchUsbOverCurrentPin7,
	PchUsbOverCurrentPinSkip,
	PchUsbOverCurrentPinMax
} PCH_USB_OVERCURRENT_PIN;

typedef struct _IOBP_MMIO_TABLE_STRUCT {
	uint32_t Address;
	uint32_t AndMask;
	uint32_t OrMask;
} IOBP_MMIO_TABLE_STRUCT;

void ReadIobp (
	uint32_t RootComplexBar,
	uint32_t Address,
	uint32_t *Data
);

void ReadIobp (
	uint32_t RootComplexBar,
	uint32_t Address,
	uint32_t *Data
)
{
	uint8_t reg8;
	uint16_t reg16;

        *Data = 0;
        ///
        /// Step 1 Poll RCBA + 2338[0] = 0b
        ///
        while (read8((void*)(RootComplexBar + R_PCH_RCRB_IOBPS)) & B_PCH_RCRB_IOBPS_BUSY);
        ///
        /// Step 2
        /// Write RCBA + Offset 2330h[31:0] with IOBP register address
        ///
        write32 ((void*)(RootComplexBar + R_PCH_RCRB_IOBPIRI), Address);
        ///
        /// Step 3
        /// Set RCBA + 2338h[15:8] = 00000110b
        ///
        reg16 = read16((void *)(RootComplexBar + R_PCH_RCRB_IOBPS));
        reg16 &= (~B_PCH_RCRB_IOBPS_IOBPIA);
        reg16 |= V_PCH_RCRB_IOBPS_IOBPIA_R;
        write16((void*)(RootComplexBar + R_PCH_RCRB_IOBPS), reg16);
        ///
        /// Step 4
        /// Set RCBA + 233Ah[15:0] = F000h
        ///
        write16((void*)(RootComplexBar + 0x233A), 0xF000);
        ///
        /// Step 5
        /// Set RCBA + 2338h[0] = 1b
        ///
        reg16 = read16((void *)(RootComplexBar + R_PCH_RCRB_IOBPS));
        reg16 |= BIT0;
        write16((void*)(RootComplexBar + R_PCH_RCRB_IOBPS), reg16);
   
        ///
        /// Step 6
        /// Poll RCBA + Offset 2338h[0] = 0b, Polling for Busy bit
        ///
        while (read8((void*)(RootComplexBar + R_PCH_RCRB_IOBPS)) & B_PCH_RCRB_IOBPS_BUSY);
   
        ///
        /// Step 7
        /// Check if RCBA + 2338h[2:1] = 00b for successful transaction
        ///
        reg8 = read8((void *)(RootComplexBar + R_PCH_RCRB_IOBPS)) & B_PCH_RCRB_IOBPS;
        if (reg8 == 0) {
          ///
          /// Step 8
          /// Read RCBA + 2334h[31:0] for IOBP data
          ///
          *Data = read32((void*)(RootComplexBar + R_PCH_RCRB_IOBPD));
        }
}

uint8_t ProgramIobp (
        uint32_t RootComplexBar,
        uint32_t Address,
        uint32_t AndMask,
        uint32_t OrMask
);

uint8_t ProgramIobp (
	uint32_t RootComplexBar,
	uint32_t Address,
	uint32_t AndMask,
	uint32_t OrMask
)
{
	uint32_t Data32;
	uint16_t reg16;
	uint8_t reg8;

        ///
        /// PCH BIOS Spec Rev 0.5.0, Section 7.1.4 IOSF SBI Programming
        /// Step 1 to Step 8
        ///
        ReadIobp (RootComplexBar, Address, &Data32);
        ///
        /// Step 9
        /// Update the SBI data accordingly
        ///
        Data32 &= AndMask;
        Data32 |= OrMask;
        ///
        /// Step 10
        /// Set RCBA + 2338h[15:8] = 00000111b
        ///
        reg16 = read16((void*)(RootComplexBar + R_PCH_RCRB_IOBPS));
        reg16 &= ~B_PCH_RCRB_IOBPS_IOBPIA;
        reg16 |=  V_PCH_RCRB_IOBPS_IOBPIA_W;
        write16((void*)(RootComplexBar + R_PCH_RCRB_IOBPS), reg16);
        ///
        /// Step 11
        /// Write RCBA + 2334h[31:0] with updated SBI data
        ///
        write32 ((void*)(RootComplexBar + R_PCH_RCRB_IOBPD), Data32);
        ///
        /// Step 12
        /// Set RCBA + 233Ah[15:0] = F000h
        ///
        write16 ((void*)(RootComplexBar + 0x233A), 0xF000);
        ///
        /// Step 13
        /// Set RCBA + 2338h[0] = 1b
        ///
        reg16 = read16((void*)(RootComplexBar + R_PCH_RCRB_IOBPS));
        reg16 |= BIT0;
        write16((void*)(RootComplexBar + R_PCH_RCRB_IOBPS), reg16);
        ///
        /// Step 14
        /// Poll RCBA + Offset 2338h[0] = 0b
        ///
        while (read8((void*)(RootComplexBar + R_PCH_RCRB_IOBPS)) & B_PCH_RCRB_IOBPS_BUSY);
        ///
        /// Step 15
        /// Check if RCBA + 2338h[2:1] = 00b for successful transaction
        ///
        reg8 = read8((void*)(RootComplexBar + R_PCH_RCRB_IOBPS)) & B_PCH_RCRB_IOBPS;

        return reg8;
}



/*
 * mainboard_enable is executed as first thing after enumerate_buses().
 * This is the earliest point to add customization.
 */
static void mainboard_enable(struct device *dev)
{
}

static void sata_init(void)
{
	uint32_t rcba = 0;
	uint16_t size, i;
	uint8_t ret;
        struct device *dev = pcidev_on_root(LPC_DEV, LPC_FUNC);


	IOBP_MMIO_TABLE_STRUCT *PchSataSharedHsio;

	IOBP_MMIO_TABLE_STRUCT PchSataSharedHsioLptH_C0[] = {
        { 0xEA002008, (uint32_t)~(0xFFFC6108), 0xEA6C6108 },
        { 0xEA002208, (uint32_t)~(0xFFFC6108), 0xEA6C6108 },
        { 0xEA002038, (uint32_t)~(0x0000000F), 0x0000000D },
        { 0xEA002238, (uint32_t)~(0x0000000F), 0x0000000D },
        { 0xEA00202C, (uint32_t)~(0x00020F00), 0x00020100 },
        { 0xEA00222C, (uint32_t)~(0x00020F00), 0x00020100 },
        { 0xEA002040, (uint32_t)~(0x1F000000), 0x01000000 },
        { 0xEA002240, (uint32_t)~(0x1F000000), 0x01000000 },
        { 0xEA002010, (uint32_t)~(0xFFFF0000), 0x0D510000 },
        { 0xEA002210, (uint32_t)~(0xFFFF0000), 0x0D510000 },
        { 0xEA002018, (uint32_t)~(0xFFFF0300), 0x38250100 },
        { 0xEA002218, (uint32_t)~(0xFFFF0300), 0x38250100 },
        { 0xEA002000, (uint32_t)~(0xCF030000), 0xCF030000 },
        { 0xEA002200, (uint32_t)~(0xCF030000), 0xCF030000 },
        { 0xEA002028, (uint32_t)~(0xFF1F0000), 0x580E0000 },
        { 0xEA002228, (uint32_t)~(0xFF1F0000), 0x580E0000 },
        { 0xEA00201C, (uint32_t)~(0x00007C00), 0x00002400 },
        { 0xEA00221C, (uint32_t)~(0x00007C00), 0x00002400 },
        { 0xEA002088, (uint32_t)~(0x00FFFF00), 0x00809800 },
        { 0xEA002288, (uint32_t)~(0x0000FF00), 0x00008000 },
        { 0xEA00208C, (uint32_t)~(0x00FF0000), 0x00370000 },
        { 0xEA00228C, (uint32_t)~(0x00FF0000), 0x00800000 },
        { 0xEA0020A4, (uint32_t)~(0x0030FF00), 0x00308300 },
        { 0xEA0022A4, (uint32_t)~(0x0030FF00), 0x00308300 },
        { 0xEA0020AC, (uint32_t)~(0x00000030), 0x00000020 },
        { 0xEA0022AC, (uint32_t)~(0x00000030), 0x00000020 },
        { 0xEA002140, (uint32_t)~(0x00FFFFFF), 0x00140718 },
        { 0xEA002340, (uint32_t)~(0x00FFFFFF), 0x00140718 },
        { 0xEA002144, (uint32_t)~(0x00FFFFFF), 0x00140998 },
        { 0xEA002344, (uint32_t)~(0x00FFFFFF), 0x00140998 },
        { 0xEA002148, (uint32_t)~(0x00FFFFFF), 0x00140998 },
        { 0xEA002348, (uint32_t)~(0x00FFFFFF), 0x00140998 },
        { 0xEA00217C, (uint32_t)~(0x03000000), 0x03000000 },
        { 0xEA00237C, (uint32_t)~(0x03000000), 0x03000000 },
        { 0xEA002178, (uint32_t)~(0x00001F00), 0x00001800 },
        { 0xEA002378, (uint32_t)~(0x00001F00), 0x00001800 },
        { 0xEA00210C, (uint32_t)~(0x0038000F), 0x00000005 },
        { 0xEA00230C, (uint32_t)~(0x0038000F), 0x00000005 }
	};

	IOBP_MMIO_TABLE_STRUCT *PchSataHsio;

	IOBP_MMIO_TABLE_STRUCT PchSataHsioLptH_C0[] = {
        { 0xEA008008, (uint32_t)~(0xFF000000), 0x1C000000 },
        { 0xEA002408, (uint32_t)~(0xFFFC6108), 0xEA6C6108 },
        { 0xEA002608, (uint32_t)~(0xFFFC6108), 0xEA6C6108 },
        { 0xEA000808, (uint32_t)~(0xFFFC6108), 0xEA6C6108 },
        { 0xEA000A08, (uint32_t)~(0xFFFC6108), 0xEA6C6108 },
        { 0xEA002438, (uint32_t)~(0x0000000F), 0x0000000D },
        { 0xEA002638, (uint32_t)~(0x0000000F), 0x0000000D },
        { 0xEA000838, (uint32_t)~(0x0000000F), 0x0000000D },
        { 0xEA000A38, (uint32_t)~(0x0000000F), 0x0000000D },
        { 0xEA002440, (uint32_t)~(0x1F000000), 0x01000000 },
        { 0xEA002640, (uint32_t)~(0x1F000000), 0x01000000 },
        { 0xEA000840, (uint32_t)~(0x1F000000), 0x01000000 },
        { 0xEA000A40, (uint32_t)~(0x1F000000), 0x01000000 },
        { 0xEA002410, (uint32_t)~(0xFFFF0000), 0x0D510000 },
        { 0xEA002610, (uint32_t)~(0xFFFF0000), 0x0D510000 },
        { 0xEA000810, (uint32_t)~(0xFFFF0000), 0x0D510000 },
        { 0xEA000A10, (uint32_t)~(0xFFFF0000), 0x0D510000 },
        { 0xEA00242C, (uint32_t)~(0x00020800), 0x00020000 },
        { 0xEA00262C, (uint32_t)~(0x00020800), 0x00020000 },
        { 0xEA00082C, (uint32_t)~(0x00020800), 0x00020000 },
        { 0xEA000A2C, (uint32_t)~(0x00020800), 0x00020000 },
        { 0xEA002418, (uint32_t)~(0xFFFF0300), 0x38250100 },
        { 0xEA002618, (uint32_t)~(0xFFFF0300), 0x38250100 },
        { 0xEA000818, (uint32_t)~(0xFFFF0300), 0x38250100 },
        { 0xEA000A18, (uint32_t)~(0xFFFF0300), 0x38250100 },
        { 0xEA002400, (uint32_t)~(0xCF030000), 0xCF030000 },
        { 0xEA002600, (uint32_t)~(0xCF030000), 0xCF030000 },
        { 0xEA000800, (uint32_t)~(0xCF030000), 0xCF030000 },
        { 0xEA000A00, (uint32_t)~(0xCF030000), 0xCF030000 },
        { 0xEA002428, (uint32_t)~(0xFF1F0000), 0x580E0000 },
        { 0xEA002628, (uint32_t)~(0xFF1F0000), 0x580E0000 },
        { 0xEA000828, (uint32_t)~(0xFF1F0000), 0x580E0000 },
        { 0xEA000A28, (uint32_t)~(0xFF1F0000), 0x580E0000 },
        { 0xEA00241C, (uint32_t)~(0x00007C00), 0x00002400 },
        { 0xEA00261C, (uint32_t)~(0x00007C00), 0x00002400 },
        { 0xEA00081C, (uint32_t)~(0x00007C00), 0x00002400 },
        { 0xEA000A1C, (uint32_t)~(0x00007C00), 0x00002400 },
	{ 0xEA002488, (uint32_t)~(0x00FFFF00), 0x00739800 },
	{ 0xEA002688, (uint32_t)~(0x00FFFF00), 0x00739800 },
	{ 0xEA000888, (uint32_t)~(0x00FFFF00), 0x00739800 },
        { 0xEA000A88, (uint32_t)~(0x00FFFF00), 0x00739800 },
        { 0xEA00248C, (uint32_t)~(0x00FF0000), 0x00450000 },
        { 0xEA00268C, (uint32_t)~(0x00FF0000), 0x00450000 },
        { 0xEA00088C, (uint32_t)~(0x00FF0000), 0x00450000 },
        { 0xEA000A8C, (uint32_t)~(0x00FF0000), 0x00450000 },
        { 0xEA0024A4, (uint32_t)~(0x0030FF00), 0x00308300 },
        { 0xEA0026A4, (uint32_t)~(0x0030FF00), 0x00308300 },
        { 0xEA0008A4, (uint32_t)~(0x0030FF00), 0x00308300 },
        { 0xEA000AA4, (uint32_t)~(0x0030FF00), 0x00308300 },
        { 0xEA0024AC, (uint32_t)~(0x00000030), 0x00000020 },
        { 0xEA0026AC, (uint32_t)~(0x00000030), 0x00000020 },
        { 0xEA0008AC, (uint32_t)~(0x00000030), 0x00000020 },
        { 0xEA000AAC, (uint32_t)~(0x00000030), 0x00000020 },
        { 0xEA002540, (uint32_t)~(0x00FFFFFF), 0x00140718 },
        { 0xEA002740, (uint32_t)~(0x00FFFFFF), 0x00140718 },
        { 0xEA000940, (uint32_t)~(0x00FFFFFF), 0x00140718 },
        { 0xEA000B40, (uint32_t)~(0x00FFFFFF), 0x00140718 },
        { 0xEA002544, (uint32_t)~(0x00FFFFFF), 0x00140998 },
        { 0xEA002744, (uint32_t)~(0x00FFFFFF), 0x00140998 },
        { 0xEA000944, (uint32_t)~(0x00FFFFFF), 0x00140998 },
        { 0xEA000B44, (uint32_t)~(0x00FFFFFF), 0x00140998 },
        { 0xEA002548, (uint32_t)~(0x00FFFFFF), 0x00140998 },
        { 0xEA002748, (uint32_t)~(0x00FFFFFF), 0x00140998 },
        { 0xEA000948, (uint32_t)~(0x00FFFFFF), 0x00140998 },
        { 0xEA000B48, (uint32_t)~(0x00FFFFFF), 0x00140998 },
        { 0xEA00257C, (uint32_t)~(0x03000000), 0x03000000 },
        { 0xEA00277C, (uint32_t)~(0x03000000), 0x03000000 },
        { 0xEA00097C, (uint32_t)~(0x03000000), 0x03000000 },
        { 0xEA000B7C, (uint32_t)~(0x03000000), 0x03000000 },
        { 0xEA002578, (uint32_t)~(0x00001F00), 0x00001800 },
        { 0xEA002778, (uint32_t)~(0x00001F00), 0x00001800 },
        { 0xEA000978, (uint32_t)~(0x00001F00), 0x00001800 },
        { 0xEA000B78, (uint32_t)~(0x00001F00), 0x00001800 },
        { 0xEA00250C, (uint32_t)~(0x0038000F), 0x00000005 },
        { 0xEA00270C, (uint32_t)~(0x0038000F), 0x00000005 },
        { 0xEA00090C, (uint32_t)~(0x0038000F), 0x00000005 },
        { 0xEA000B0C, (uint32_t)~(0x0038000F), 0x00000005 }
	};

        /* Get Root Complex Base Address */
        rcba = pci_read_config32(dev, 0xf0);
	/* Bits 31-14 are the base address, 13-1 are reserved, 0 is enable */
        rcba &= 0xffffc000;

        if (!rcba)
                return;

	size = (sizeof (PchSataSharedHsioLptH_C0) / sizeof (IOBP_MMIO_TABLE_STRUCT));
        PchSataSharedHsio = &PchSataSharedHsioLptH_C0[0];
	for (i = 0; i < size; i++) {
		ret = ProgramIobp (
			rcba,
			PchSataSharedHsio[i].Address,
			PchSataSharedHsio[i].AndMask,
			PchSataSharedHsio[i].OrMask
                );
	}

        size = (sizeof (PchSataHsioLptH_C0) / sizeof (IOBP_MMIO_TABLE_STRUCT));
        PchSataHsio = &PchSataHsioLptH_C0[0];
	for (i = 0; i < size; i++) {
		ret = ProgramIobp (
			rcba,
			PchSataHsio[i].Address,
			PchSataHsio[i].AndMask,
			PchSataHsio[i].OrMask
                );
	}
}

static void usb_init(void)
{
	PCH_USB_OVERCURRENT_PIN *Usb20OverCurrentMappings;
	PCH_USB_OVERCURRENT_PIN *Usb30OverCurrentMappings;
	struct device *dev = pcidev_on_root(XHCI_DEV, XHCI_FUNC);
	uint32_t XhciHsOcm1;
	uint32_t XhciHsOcm2;
	uint32_t XhciSsOcm1;
	uint32_t XhciSsOcm2;
	uint32_t Index;
	uint32_t CurrentIndex;
	uint32_t reg32;

	/* ------------------------- PCH Customization -----------------------*/
	/* Add USB OVERCURRENT customization */
	PCH_USB_OVERCURRENT_PIN COMe_bBD7Usb20OverCurrentMappings[LPTH_USB_MAX_PHYSICAL_PORTS] = {   
		PchUsbOverCurrentPin0,          //Port00: USB0,OC0#
		PchUsbOverCurrentPin0,          //Port01: USB1,OC0#
                PchUsbOverCurrentPin2,          //Port02: USB2,OC2#
                PchUsbOverCurrentPin2,          //Port03: USB3,OC2#
                PchUsbOverCurrentPinSkip,       //Port04: Not used,no OCn#
                PchUsbOverCurrentPinSkip,       //Port05: Not used,no OCn#
                PchUsbOverCurrentPinSkip,       //Port06: Not used,no OCn#
                PchUsbOverCurrentPinSkip,       //Port07: Not used,no OCn#
                PchUsbOverCurrentPinSkip,       //Port08: Not used,no OCn#
                PchUsbOverCurrentPinSkip,       //Port09:Not used,no OCn#
                PchUsbOverCurrentPinSkip,       //Port10:Not used,no OCn#
                PchUsbOverCurrentPinSkip,       //Port11:Not used,no OCn#
                PchUsbOverCurrentPinSkip,       //Port12:Not used,no OCn#
                PchUsbOverCurrentPinSkip        //Port13:Not used,no OCn#
	};

	PCH_USB_OVERCURRENT_PIN COMe_bBD7Usb30OverCurrentMappings[LPTH_XHCI_MAX_USB3_PORTS] = { 
                PchUsbOverCurrentPin0,          //Port00: USB0,OC0#
                PchUsbOverCurrentPin0,          //Port01: USB1,OC0#
                PchUsbOverCurrentPin2,          //Port02: USB2,OC2#
                PchUsbOverCurrentPin2,          //Port03: USB3,OC2#
                PchUsbOverCurrentPinSkip,       //Port04: Not used,no OCn#
                PchUsbOverCurrentPinSkip        //Port05: Not used,no OCn#
	};

	Usb20OverCurrentMappings = &COMe_bBD7Usb20OverCurrentMappings[0];
	Usb30OverCurrentMappings = &COMe_bBD7Usb30OverCurrentMappings[0];

	XhciHsOcm1  = 0;
	XhciHsOcm2  = 0;
	XhciSsOcm1  = 0;
	XhciSsOcm2  = 0;

	for (Index = 0; Index < LPTH_USB_MAX_PHYSICAL_PORTS; Index++) {
		if (Usb20OverCurrentMappings[Index] == PchUsbOverCurrentPinSkip) {
		/*
		* No OC pin assigned, skip this port
		*/
		} else {
			if (Index < 8) {
			/*
			* Port 0-7: OC0 - OC3
			*/
			if (Usb20OverCurrentMappings[Index] > PchUsbOverCurrentPin3) {
				continue;
			}
			CurrentIndex = Usb20OverCurrentMappings[Index] * 8 + Index;
			XhciHsOcm1 |= (uint32_t) (1 << CurrentIndex);
			} else {
				/*
				* Port 8-13: OC4 - OC7
				*/
				if ((Usb20OverCurrentMappings[Index] < PchUsbOverCurrentPin4) ||
					(Usb20OverCurrentMappings[Index] > PchUsbOverCurrentPin7)) {
					continue;
				}
				CurrentIndex = (Usb20OverCurrentMappings[Index] - 4) * 8 + (Index - 8);
				XhciHsOcm2 |= (uint32_t) (1 << CurrentIndex);
			}
		}
	}

	for (Index = 0; Index < LPTH_XHCI_MAX_USB3_PORTS; Index++) {
		if (Usb30OverCurrentMappings[Index] == PchUsbOverCurrentPinSkip) {
			/*
			* No OC pin assigned, skip this port
			*/
		} else {
			/*
			* Port 0-5: OC0 - OC7
			*/
			if (Usb30OverCurrentMappings[Index] < PchUsbOverCurrentPin4) {
				CurrentIndex = Usb30OverCurrentMappings[Index] * 8 + Index;
				XhciSsOcm1 |= (uint32_t) (1 << CurrentIndex);
			} else {
				CurrentIndex = (Usb30OverCurrentMappings[Index] - 4) * 8 + Index;
				XhciSsOcm2 |= (uint32_t) (1 << CurrentIndex);
			}
		}
	}

        pci_write_config32(dev, R_PCH_XHCI_U2OCM1, XhciHsOcm1);
        pci_write_config32(dev, R_PCH_XHCI_U2OCM2, XhciHsOcm2);
        pci_write_config32(dev, R_PCH_XHCI_U3OCM1, XhciSsOcm1);
        pci_write_config32(dev, R_PCH_XHCI_U3OCM2, XhciSsOcm2);

        /* lock overcurrent map */
        reg32 = pci_read_config32(dev, R_PCH_XHCI_XHCC2);
        reg32 |= 0x80000000;
        pci_write_config32(dev, R_PCH_XHCI_XHCC2, reg32);
}

static void spi_init(void)
{
        void *spi_base = NULL;
        uint32_t rcba = 0;
        struct device *dev = pcidev_on_root(LPC_DEV, LPC_FUNC);

        /* Get address of SPI controller. */
        rcba = (pci_read_config32(dev, 0xf0) & 0xffffc000);
	/* Bits 31-14 are the base address, 13-1 are reserved, 0 is enable */
        rcba = rcba & 0xffffc000;

        if (!rcba)
                return;
        spi_base = (void *)(rcba + SPIBAR_OFFSET);

        /* Setup OPCODE menu */
        write16((spi_base + SPI_REG_PREOP), SPI_OPPREFIX);
        write16((spi_base + SPI_REG_OPTYPE), SPI_OPTYPE);
        write32((spi_base + SPI_REG_OPMENU_L), SPI_OPMENU_LOWER);
        write32((spi_base + SPI_REG_OPMENU_H), SPI_OPMENU_UPPER);

/* Suppress Debug messages */
#if 0
	/* Debug Flash Protection */
	/* FLOCKDN */
	printk(BIOS_INFO,"HSFS = %x\n", read16(spi_base + 0x4));
	/* PR */
	printk(BIOS_INFO,"PR0 = %x\n", read32(spi_base + 0x74));
	printk(BIOS_INFO,"PR1 = %x\n", read32(spi_base + 0x78));
	printk(BIOS_INFO,"PR2 = %x\n", read32(spi_base + 0x7C));
	printk(BIOS_INFO,"PR3 = %x\n", read32(spi_base + 0x80));
	printk(BIOS_INFO,"PR4 = %x\n", read32(spi_base + 0x84));
#endif
}

#define PCH_LCP_SIRQ_CNTL	0x64
#define SIRQ_CONTINUOUS_MODE	0x40

static void lpc_init(void)
{
	/* SIRQ in CONTINUOUS mode */
	struct device *dev = pcidev_on_root(LPC_DEV, LPC_FUNC);
	uint8_t reg8;

	reg8 = pci_read_config8(dev, PCH_LCP_SIRQ_CNTL);
	reg8 |= SIRQ_CONTINUOUS_MODE;
	pci_write_config8(dev, PCH_LCP_SIRQ_CNTL, reg8);
}


static void mainboard_final(void *chip_info)
{
	spi_init();
	usb_init();
	lpc_init();
	sata_init();
}

struct chip_operations mainboard_ops = {
	.enable_dev = mainboard_enable,
	.final = mainboard_final
};
