/*
 * This file is part of the coreboot project.
 *
 * Copyright (C) 2019 Kontron Modular Computers SAS
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <stddef.h>
#include <soc/romstage.h>
#include <drivers/intel/fsp1_0/fsp_util.h>
#include <device/pci_ops.h>
#include <device/pci_def.h>
#include <soc/lpc.h>
#include <soc/pci_devs.h>
#include "uart.h"
#include "gpio.h"

//
// COM1 Port I/O address
//
#define COM1_PORT 0x3f8
#define COM2_PORT 0x2f8

//
// Prototype functions
//
void serial_port_init(void);
int serial_port_write(char *buffer, int number_of_bytes);
int serial_port_read(char *buffer, int number_of_bytes);
int serial_output(char *string);
void print_message(void);
void cpld_init(void);
void uart_basic_init(void);

//
// Functions definition
//
void uart_basic_init(void)
{
  short com_port;
  /* switch to baud rate initialization */
  if (CONFIG_UART_FOR_CONSOLE == 0)
    com_port = COM1_PORT;
  else if (CONFIG_UART_FOR_CONSOLE == 1)
    com_port = COM2_PORT;
  else
    com_port = 0x3f8;
  /* Set divisor/data=8/parity=odd/stop=0/breakset=0 */
  outb(0x83,com_port + 3);
  /* Set speed = 115200 */
  outb(0x1,com_port);
  /* return to normal mode */
  outb(0x03,com_port+3);
  outb(0x01,com_port+2);
}


void serial_port_init(void)
{
  int baudrate = 38400;
  int basebaudrate = 153600;
  int divisor = basebaudrate / baudrate;
  short com_port = COM1_PORT;
  char data = UART_DATA - 5;
  char parity = UART_PARITY;
  char breakset = UART_BREAK_SET;
  char stopbit = UART_STOP;
  char output_data;

  //
  // Set communications format
  //
  output_data = ((DLAB << 7) | ((breakset << 6) | ((parity << 3) |
                ((stopbit << 2) | data))));
  outb(com_port + LCR_OFFSET, output_data);
  //
  // Configure baud rate
  //
  outb(com_port + BAUD_HIGH_OFFSET, (char)(divisor >> 8));
  outb(com_port + BAUD_LOW_OFFSET, (char) (divisor & 0xff));
  //
  // Switch back to bank 0
  //
  output_data = (char) ((~DLAB << 7) | ((breakset << 6) | ((parity << 3) |
                ((stopbit << 2) | data))));
  outb(com_port + LCR_OFFSET, output_data);
}

int serial_port_write (
  char *buffer,
  int  number_of_bytes
)
{
  int result;
  char data;
  short com_port = COM1_PORT;

  if (!buffer) {
    return 0;
  }

  result = number_of_bytes;

  while (number_of_bytes--) {
    //
    // Wait for the serail port to be ready.
    //
    do {
      data = inb (com_port + LSR_OFFSET);
    } while ((data & LSR_TXRDY) == 0);
    outb(com_port, *buffer++);
  }

  return result;
}

int serial_port_read (
  char *buffer,
  int number_of_bytes
)
{
  short com_port = COM1_PORT;
  char data;
  int result;

  if (!buffer) {
    return 0;
  }

  result = number_of_bytes;

  while (number_of_bytes--) {
    //
    // Wait for the serial port to be ready.
    //
    do {
      data = inb(com_port + LSR_OFFSET);
    } while ((data & LSR_RXDA) == 0);

    *buffer++ = inb(com_port);
  }

  return result;
}

int serial_output(
  char *string
)
{
  char *buffer;
  int count = 0;
  int result;

  if (!string)
    return 0;

  buffer = string;

  // Lets count the size of the string
  while (*(buffer + count)) {
    if (*(buffer + count) == '\r' && *(buffer + count + 1) == '\n')
      count+=2;
    else if (*(buffer + count) == '\n') {
    // we need to add \r before \n so if we found \n send all that before it 
      if (count)  
        result = serial_port_write(buffer, count); 
        // replace "\n" with "\r\n" and send 2 symbols
      result = serial_port_write((char*)"\r\n", 2);
      buffer = buffer + count + 1;  // init buffer with next
                                    // position after \n
      count = 0;                    // and reset counter
    }
    else 
      count++;
  }
  if (count)  
    result = serial_port_write(buffer, count); 

  return result;
}

#define CPLD_INDEX_ADDRESS 0x0A80
#define CPLD_DATA_ADDRESS  0x0A81

void cpld_init(void)
{
  /* Disable both internal UARTs in SOC */
  /* ================================== */
  /* NOTE:
   *	Must be done earlier before entering FSP binary.
   *	For now, it is disabled directly in FSP binary.
   */
  //pci_write_config32(PCI_DEV(0, LPC_DEV, 0), 0x80, 0x0);

  /* Active both CPLD uart interfaces */
  outb(0x30,CPLD_INDEX_ADDRESS);
  outb(0x84,CPLD_DATA_ADDRESS); /* COM1: 3F8/IRQ 4 */
  outb(0x31,CPLD_INDEX_ADDRESS);
  outb(0x93,CPLD_DATA_ADDRESS); /* COM2: 2F8/IRQ 3 */
  /* Release the Mutex */
  outb(0x80,CPLD_INDEX_ADDRESS);
}


/**
 * /brief mainboard call for setup that needs to be done before fsp init
 *
 */
void early_mainboard_romstage_entry(void)
{
  /*---------------------------------------------------------------------------------*/
  /* Init GPIOs */
  /*---------------------------------------------------------------------------------*/
  init_gpios(bbd7_gpio_config);

  /*---------------------------------------------------------------------------------*/
  /* Init Serial from FPD UART */
  /*---------------------------------------------------------------------------------*/

  /* Set I/O decode ranges for COM1(3F8h)/COM2(2F8h) */
  pci_write_config16(PCI_DEV(0x0, LPC_DEV, LPC_FUNC), LPC_IO_DEC, 0x0010);

  /* Enable COM1/COM2/KBD/LPT/SuperIO1+2 */
  pci_write_config16(PCI_DEV(0, LPC_DEV, LPC_FUNC), LPC_EN, CNF2_LPC_EN
                  | CNF1_LPC_EN | KBC_LPC_EN | COMA_LPC_EN
                  | COMB_LPC_EN | LPT_LPC_EN);

  /* Enable I/O ranges decoding for CPLD */
  pci_write_config32(PCI_DEV(0, LPC_DEV, LPC_FUNC), LPC_GEN1_DEC, 0x000C0A81);

  /* Serial Port Initialization */
  serial_port_init();

  /* CPLD initialization */
  cpld_init();

  /* Uart basic init */
  uart_basic_init();
}

/**
 * /brief mainboard call for setup that needs to be done after fsp init
 *
 */
void late_mainboard_romstage_entry(void)
{
}

/**
 * /brief customize fsp parameters here if needed
 */
void romstage_fsp_rt_buffer_callback(FSP_INIT_RT_BUFFER *FspRtBuffer)
{
}
