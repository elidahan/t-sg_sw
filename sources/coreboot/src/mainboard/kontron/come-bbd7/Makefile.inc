##
## This file is part of the coreboot project.
##
## Copyright (C) 2012 Google Inc.
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; version 2 of the License.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##

ramstage-y += irqroute.c

################################################################################
# Kontron specific tools
################################################################################
BUILDFLASH:=util/kontron/buildflash

################################################################################
# COREBOOT + UBOOT final image filename
################################################################################
COREBOOT_UBOOT_FINAL_IMAGE:=$(obj)/coreboot_uboot_$(GDATE).bin

################################################################################
# COLORS
################################################################################
COLOR_SUCCESS=\033[0;32m
COLOR_NORMAL=\033[0m
COLOR_ERROR=\033[0;31m
COLOR_YELLOW=\033[0;33m
COLOR_PURPLE=\033[0;35m

ifeq ($(CONFIG_PAYLOAD_UBOOT),y)

$(COREBOOT_UBOOT_FINAL_IMAGE): $(BUILDFLASH) $(obj)/coreboot.rom
	@echo
	@echo -ne '$(COLOR_YELLOW)#### create KONTRON final image'
	@echo -ne '$(COLOR_PURPLE) $(COREBOOT_UBOOT_FINAL_IMAGE) $(COLOR_YELLOW)'
	@echo -e '####$(COLOR_NORMAL)\n'
	$(BUILDFLASH) $@

files_added:: $(COREBOOT_UBOOT_FINAL_IMAGE)

endif

