/*
 * This file is part of the coreboot project.
 *
 * Copyright (C) 2012 Google Inc.
 * Copyright (C) 2015-2016 Intel Corp.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef IRQROUTE_H
#define IRQROUTE_H

#include <soc/irq.h>
#include <soc/pci_devs.h>

#define NB_PCIE_PORT1_DEV			1
#define NB_PCIE_PORT1_FUNC			0
#define NB_PCIE_PORT1_DEVID			0x6f02
#define NB_PCIE_PORT1_DEV_FUNC			PCI_DEVFN(NB_PCIE_PORT1_DEV,\
							NB_PCIE_PORT1_FUNC)

#define NB_PCIE_PORT2_DEV			2
#define NB_PCIE_PORT2A_DEV			NB_PCIE_PORT2_DEV
#define NB_PCIE_PORT2A_FUNC			0
#define NB_PCIE_PORT2A_DEVID			0x6f04
#define NB_PCIE_PORT2A_DEV_FUNC			PCI_DEVFN(NB_PCIE_PORT2A_DEV,\
							NB_PCIE_PORT2A_FUNC)

#define NB_PCIE_PORT2B_DEV			NB_PCIE_PORT2_DEV
#define NB_PCIE_PORT2B_FUNC			2
#define NB_PCIE_PORT2B_DEVID			0x6f06
#define NB_PCIE_PORT2B_DEV_FUNC			PCI_DEVFN(NB_PCIE_PORT2B_DEV,\
							NB_PCIE_PORT2B_FUNC)

#define NB_PCIE_PORT3_DEV			3
#define NB_PCIE_PORT3_FUNC			0
#define NB_PCIE_PORT3_DEVID			0x6f08
#define NB_PCIE_PORT3_DEV_FUNC			PCI_DEVFN(NB_PCIE_PORT3_DEV,\
							NB_PCIE_PORT3_FUNC)

#define IIO_HP_DEV				VTD_DEV
#define IIO_RAS_DEV				VTD_DEV
#define IO_APIC_DEV				VTD_DEV
#define MEI1_DEV				ME_DEV
#define MEI2_DEV				ME_DEV

#define PCI_DEV_PIRQ_ROUTES \
	PCI_DEV_PIRQ_ROUTE(SOC_DEV,		A, B, C, D), \
	PCI_DEV_PIRQ_ROUTE(NB_PCIE_PORT1_DEV,	A, B, C, D), \
	PCI_DEV_PIRQ_ROUTE(NB_PCIE_PORT2A_DEV,	A, B, C, D), \
	PCI_DEV_PIRQ_ROUTE(NB_PCIE_PORT2B_DEV,	A, B, C, D), \
	PCI_DEV_PIRQ_ROUTE(NB_PCIE_PORT3_DEV,	A, B, C, D), \
	PCI_DEV_PIRQ_ROUTE(VTD_DEV,		A, B, C, D), \
	PCI_DEV_PIRQ_ROUTE(IIO_HP_DEV,		A, B, C, D), \
	PCI_DEV_PIRQ_ROUTE(IIO_RAS_DEV,		A, B, C, D), \
	PCI_DEV_PIRQ_ROUTE(IO_APIC_DEV,		A, B, C, D), \
	PCI_DEV_PIRQ_ROUTE(XHCI_DEV,		A, B, C, D), \
	PCI_DEV_PIRQ_ROUTE(EHCI1_DEV,		A, B, C, D), \
	PCI_DEV_PIRQ_ROUTE(EHCI2_DEV,		A, B, C, D), \
	PCI_DEV_PIRQ_ROUTE(MEI1_DEV,		A, B, C, D), \
	PCI_DEV_PIRQ_ROUTE(MEI2_DEV,		A, B, C, D), \
        PCI_DEV_PIRQ_ROUTE(PCIE_PORT1_DEV,	A, B, C, D), \
        PCI_DEV_PIRQ_ROUTE(PCIE_PORT2_DEV,	A, B, C, D), \
        PCI_DEV_PIRQ_ROUTE(PCIE_PORT3_DEV,	A, B, C, D), \
        PCI_DEV_PIRQ_ROUTE(PCIE_PORT4_DEV,	A, B, C, D), \
        PCI_DEV_PIRQ_ROUTE(PCIE_PORT5_DEV,	A, B, C, D), \
        PCI_DEV_PIRQ_ROUTE(PCIE_PORT6_DEV,	A, B, C, D), \
        PCI_DEV_PIRQ_ROUTE(PCIE_PORT7_DEV,	A, B, C, D), \
        PCI_DEV_PIRQ_ROUTE(PCIE_PORT8_DEV,	A, B, C, D), \
	PCI_DEV_PIRQ_ROUTE(LPC_DEV,		A, B, C, D), \
	PCI_DEV_PIRQ_ROUTE(SATA_DEV,		A, B, C, D), \
	PCI_DEV_PIRQ_ROUTE(SMBUS_DEV,		A, B, C, D), \
	PCI_DEV_PIRQ_ROUTE(SATA2_DEV,		A, B, C, D)

/*
 * Route each PIRQ[A-H] to a PIC IRQ[0-15]
 * Reserved: 0, 1, 2, 8, 13
 * ACPI/SCI: 9
 */
#define PIRQ_PIC_ROUTES \
	PIRQ_PIC(A, 11), \
	PIRQ_PIC(B, 10), \
	PIRQ_PIC(C,  5), \
	PIRQ_PIC(D, 11), \
	PIRQ_PIC(E, DISABLE), \
	PIRQ_PIC(F, DISABLE), \
	PIRQ_PIC(G, DISABLE), \
	PIRQ_PIC(H, DISABLE)

#endif /* IRQROUTE_H */
