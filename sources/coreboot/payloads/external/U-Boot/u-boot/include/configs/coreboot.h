/* SPDX-License-Identifier: GPL-2.0+ */
/*
 * Copyright (C) 2018, Bin Meng <bmeng.cn@gmail.com>
 */

/*
 * board/config.h - configuration options, board specific
 */

#ifndef __CONFIG_H
#define __CONFIG_H

#include <configs/x86-common.h>

/* Kontron - Start */
/*****************************************************************************/
/*                                                                           */
/*                       SPI Flash Environment Variables                     */
/*                                                                           */
/* Defined at SPI flash offset = 0xA0000 (Not used by coreboot)              */
/* Size = 4KB (1 sector) already defined in x86-common.h                     */
/*****************************************************************************/
#define CONFIG_ENV_OFFSET     0xA0000
#define CONFIG_ENV_SECT_SIZE  0x1000 
/* Kontron - End */


#define CONFIG_SYS_MONITOR_LEN		(1 << 20)

#define CONFIG_STD_DEVICES_SETTINGS	"stdin=serial,i8042-kbd,usbkbd\0" \
					"stdout=serial,vidconsole\0" \
					"stderr=serial,vidconsole\0"

/* ATA/IDE support */
#define CONFIG_SYS_IDE_MAXBUS		2
#define CONFIG_SYS_IDE_MAXDEVICE	4
#define CONFIG_SYS_ATA_BASE_ADDR	0
#define CONFIG_SYS_ATA_DATA_OFFSET	0
#define CONFIG_SYS_ATA_REG_OFFSET	0
#define CONFIG_SYS_ATA_ALT_OFFSET	0
#define CONFIG_SYS_ATA_IDE0_OFFSET	0x1f0
#define CONFIG_SYS_ATA_IDE1_OFFSET	0x170
#define CONFIG_ATAPI

#endif	/* __CONFIG_H */
