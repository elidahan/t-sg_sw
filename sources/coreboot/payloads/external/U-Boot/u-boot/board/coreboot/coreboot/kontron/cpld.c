/*
 * Copyright (C) 2019 Kontron Modular Computers SAS
 *
 * Author: Christian ROY <christian.roy@kontron.com>
 *
 * SPDX-License-Identifier:	GPL-2.0+
 *
 * Description: board-specific CPLD support
 *
 */

#include <common.h>
#include <command.h>
#include <asm/io.h>
#include "cpld.h"

u8 cpld_read(u8 reg)
{
 *(volatile u8 *)(CPLD_INDEX_ADDRESS) = reg;
 return *(volatile u8 *)(CPLD_DATA_ADDRESS);
}

void cpld_write(u8 reg, u8 value)
{
  *(volatile u8 *)(CPLD_INDEX_ADDRESS) = reg;
  *(volatile u8 *)(CPLD_DATA_ADDRESS) = value;
}

