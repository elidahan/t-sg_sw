/*
 * Copyright (C) 2019 Kontron Modular Computers SAS
 *
 * Author: Christian ROY <christian.roy@kontron.com>
 *
 * SPDX-License-Identifier:     GPL-2.0+
 *
 * Description: board-specific CPLD support
 *
 */

/*
 * CPLD I/O ADDRESS DECODING 
 */
#define CPLD_INDEX_ADDRESS 0x0A80
#define CPLD_DATA_ADDRESS  0x0A81

/*
 * CPLD register set
 */
#define CPLD_VERSION_LSB  0x00 /* R  CPLD Version LSB */
#define CPLD_VERSION_MSB  0x01 /* R  CPLD Version MSB */
#define CPLD_BUILD_LSB    0x02 /* R  Build number LSB */
#define CPLD_BUILD_MSB    0x03 /* R  Build number MSB */
#define CPLD_FEATURE_LSB  0x04 /* R  CPLD Features LSB */
#define CPLD_FEATURE_MSB  0x05 /* R  CPLD Features MSB */
#define CPLD_SPEC         0x06 /* R  Version of the CPLD Specification */
                               /* 0x07-0x0A  Reserved  0xFF */
#define I2C_PRERlo        0x0B /* R/W  I2C Clock prescaler LSB */
#define I2C_PRERhi        0x0C /* R/W  I2C Clock prescaler MSB */
#define I2C_CTR           0x0D /* R/W  I2C Control Register */
#define I2C_TXR           0x0E /* R/W  Receive/Transmit register */
#define I2C_CR            0x0F /*  W  Command register */
#define I2C_SR            0x0F /* R  Status register */
                               /* 0x10.0x14  Reserved  0xFF */
#define I2C_MUX           0x15 /* R/W  Bus Multiplexer */
#define WD_KICK           0x16 /*   W  Write anything to kick the WD */
#define WD_CFG            0x17 /* R/W  Global Status / Config */
#define WD_S1CFG          0x18 /* Stage 1 Configuration register */
#define WD_S2CFG          0x19 /* Stage 2 Config register */
#define WD_S3CFG          0x1A /* Stage 3 Config register */
#define WD_S1B1           0x1B /* R/W  Stage 1 timeout byte 1 (LSB) */
#define WD_S1B2           0x1C /* R/W  Stage 1 timeout byte 2 */
#define WD_S1B3           0x1D /* R/W  Stage 1 timeout byte 3 */
#define WD_S1B4           0x1E /* R/W  Stage 1 timeout byte 4 (MSB) */
#define WD_S2B1           0x1F /* R/W  Stage 2 timeout byte 1 (LSB) */
#define WD_S2B2           0x20 /* R/W  Stage 2 timeout byte 2 */
#define WD_S2B3           0x21 /* R/W  Stage 2 timeout byte 3 */
#define WD_S2B4           0x22 /* R/W  Stage 2 timeout byte 4 (MSB) */
#define WD_S3B1           0x23 /* R/W  Stage 3 timeout byte 1 (LSB) */
#define WD_S3B2           0x24 /* R/W  Stage 3 timeout byte 2 */
#define WD_S3B3           0x25 /* R/W  Stage 3 timeout byte 3 */
#define WD_S3B4           0x26 /* R/W  Stage 3 timeout byte 4 (MSB) */
                               /* 0x27-0x26  Reserved  0xFF */
#define UART1_CFG         0x30 /* R/W  UART1 Configuration register */
#define UART2_CFG         0x31 /* R/W  UART2 Configuration register */
#define UART3_CFG         0x32 /* R/W  UART3 Configuration register */
#define UART4_CFG         0x33 /* R/W  UART4 Configuration register */
#define CFG_GPIO          0x35 /* R/W  GPIO Configuration register */
#define CFG_I2C           0x36 /* R/W  I2C Configuration register */
#define CFG_CPLD          0x37 /* R/W  Global Configuration Register */ 
                               /* 0x38-0x3F  Reserved  0xFF */
#define IO_DIR_0          0x40 /* R/W  GPIO[7..0] direction setting */
#define IO_DIR_1          0x41 /* R/W  GPIO[15..8] direction setting */
#define IO_LVL_0          0x42 /* R/W  GPIO[7..0] level control */
#define IO_LVL_1          0x43 /* R/W  GPIO[15..8] level control */
#define IO_STS_0          0x44 /* R/W  GPIO[7..0] event status */
#define IO_STS_1          0x45 /* R/W  GPIO[15..8] event status */
#define IO_EVT_LVL_EDGE_0 0x46 /* R/W  GPIO[7..0] Level (0) or Edge (1) event */
#define IO_EVT_LVL_EDGE_1 0x47 /* R/W  GPIO[15..8] Level(0) or Edge (1) event */
#define IO_EVT_LOW_HIGH_0 0x48 /* R/W  GPIO[7..0]  Low  Level/Falling  Edge (0)
                               or  High Level / Rising Edge (1) event */
#define IO_EVT_LOW_HIGH_1 0x49 /* R/W  GPIO[15..8] Low Level/Falling Edge (0)
                               or High Level / Rising Edge (1) event */ 
#define IO_IEN_0          0x4A /* R/W  GPIO[7..0] Interrupt Mask Register */
#define IO_IEN_1          0x4B /* R/W  GPIO[15..8] Interrupt Mask Register*/ 
#define IO_NMIEN_0        0x4C /* R/W  GPIO[7..0] NMI Mask Register */ 
#define IO_NMIEN_1        0x4D /* R/W  GPIO[15..8] NMI Mask Register */ 
                               /* 0x4E-0x4F  Reserved  0xFF */
                               /* 0x50-0x5F  Reserved  0xFF */
#define SLEEP_SMI_CONTROL 0x60 /* W  Writing .1. to bit 0 will set SMI_n output
                               to '1'. Bit 0 will return to '0' after 1 LPC CLK
                               tick automatically. */
                              /* 0x61-0xFF  Reserved  0xFF */


/* 
 * CPLD access functions
 */

u8 cpld_read(u8 reg);
void cpld_write(u8 reg, u8 value);

#define CPLD_READ(reg)         cpld_read(reg))
#define CPLD_WRITE(reg, value) cpld_write(reg, value)

