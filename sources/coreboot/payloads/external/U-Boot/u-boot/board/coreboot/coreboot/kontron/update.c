/*
 * Copyright (C) 2019 Kontron Modular Computers SAS
 *
 * Author: Christian ROY <christian.roy@kontron.com>
 *
 * SPDX-License-Identifier:     GPL-2.0+
 *
 * Description: SPI Flash update
 *
 */

/* This module depends of following MACRO definitions in .config:
    - CONFIG_SPI
    - CONFIG_DM_SPI
    - CONFIG_DM_SPI_FLASH

   See doc/driver-model/spi-howto.txt and include/spi.h for more information.
 */

#include <common.h>
#include <command.h>
#include <i2c.h>
#include <u-boot/md5.h>
#include <cli.h>
#include <spi.h>
#include <spi_flash.h>
#include <dm/device-internal.h>
#include <asm/io.h>

/***************** U-BOOT Update ****************/

extern char version_string[];
extern flash_info_t flash_info[];

/* 0 if OK; 1 if not */
/* FIX ME: return always TRUE, need to be adapted for Coreboot */
int check_uboot_image(void) {
  return 0;
}

DECLARE_GLOBAL_DATA_PTR;

#define CONFIG_FIRM_SPI_FLASH_START 0
#define CONFIG_FIRM_SPI_FLASH_SIZE  0x1000000

static const char *spi_flash_update_block(struct spi_flash *flash, u32 offset,
		size_t len, const char *buf, char *cmp_buf, size_t *skipped)
{
        char *ptr = (char *)buf;

        debug("offset=%#x, sector_size=%#x, len=%#zx\n",
              offset, flash->sector_size, len);
        /* Read the entire sector so to allow for rewriting */
        if (spi_flash_read(flash, offset, flash->sector_size, cmp_buf))
                return "read";
        /* Compare only what is meaningful (len) */
        if (memcmp(cmp_buf, buf, len) == 0) {
                debug("Skip region %x size %zx: no change\n",
                      offset, len);
                *skipped += len;
                return NULL;
        }
        /* Erase the entire sector */
        if (spi_flash_erase(flash, offset, flash->sector_size))
                return "erase";
        /* If it's a partial sector, copy the data into the temp-buffer */
        if (len != flash->sector_size) {
                memcpy(cmp_buf, buf, len);
                ptr = cmp_buf;
        }
        /* Write one complete sector */
        if (spi_flash_write(flash, offset, flash->sector_size, ptr))
                return "write";

        return NULL;
}

static int spi_flash_update(struct spi_flash *flash, u32 offset, size_t len,
		const char *buf)
{
        const char *err_oper = NULL;
        char *cmp_buf;
        const char *end = buf + len;
        size_t todo;            /* number of bytes to do in this pass */
        size_t skipped = 0;     /* statistics */
        const ulong start_time = get_timer(0);
        size_t scale = 1;
        ulong delta;

        if (end - buf >= 200)
                scale = (end - buf) / 100;
        cmp_buf = memalign(ARCH_DMA_MINALIGN, flash->sector_size);
        if (cmp_buf) {
                ulong last_update = get_timer(0);

                for (; buf < end && !err_oper; buf += todo, offset += todo) {
                        todo = min_t(size_t, end - buf, flash->sector_size);
                        if (get_timer(last_update) > 100) {
                                printf("   \rUpdating, %zu%% ",
                                       100 - (end - buf) / scale);
                                last_update = get_timer(0);
                        }
                        err_oper = spi_flash_update_block(flash, offset, todo,
                                        buf, cmp_buf, &skipped);
                }
        } else {
                err_oper = "malloc";
        }
        free(cmp_buf);
        putc('\r');
        if (err_oper) {
                printf("SPI flash failed in %s step\n", err_oper);
                return 1;
        }

        delta = get_timer(start_time);
        printf("%zu bytes written, %zu bytes skipped", len - skipped,
               skipped);
        printf(" in %ld.%lds\n", delta / 1000, delta % 1000);

        return 0;
}


int do_update (cmd_tbl_t * cmdtp, int flag, int argc, char * const argv[])
{
	u32	addr;
	struct  udevice *new;
	struct  spi_flash *qflash;
	int     spibus,spics,ret;
	char	response[10];
	int	force=0;
	uint16_t n_sector;
	/*struct mtd_info *mtd = &qflash->mtd;*/
	void *buf;
	u32 offset;
	size_t len;

	/* Check out if force is present and the end of the command */
	if (strcmp(argv[(argc-1)],"force") == 0)
		force=1;
	
	/* Check the number of argument / need to be three */
	if (argc == 3 || ((argc == 4) && force)) {
		/* If the second parameter is main */
		if (strcmp(argv[1],"flash") == 0) {
			/* Get the address */
			addr = simple_strtoul(argv[2], NULL, 16);

			if (!check_uboot_image()) { 
				/* main (user) flash currently mapped to CS0 */
				spibus=0;
				spics=0;

				ret = spi_flash_probe_bus_cs(spibus, spics, 0, 0, &new);
				if (ret) {
					printf("FAILED: spi flash not detected...\n");
					return 1;
				}
				
				qflash = dev_get_uclass_priv(new);				

				if (!force) {
					memset(response,0,sizeof(response));
					cli_readline_into_buffer("Do you really want to flash firmware image ? (yes/no) : ",response,0);
					if (strcmp(response,"yes")) {
						printf("U-boot copy aborted by user !\n");
						return -1;
					}
				}
				
				/* unprotect sectors */
				for (n_sector = 0; n_sector <= 4095; n_sector++)
				  spi_flash_protect(qflash, n_sector*4096, 4096, 0);
				printf("Erase SPI Flash...");
#if 0
				/* erase sectors     */
				for (n_sector = 0; n_sector <= 4095; n_sector++) {
					spi_flash_erase(qflash, n_sector*4096, 4096);
					mdelay(10); // wait for 10 ms between each sector
				}
				printf("[ OK ]\n");
				/* write flash       */
				printf("Write SPI Flash...");
				for (n_sector = 0; n_sector <= 4095; n_sector++)
				  spi_flash_write(qflash, n_sector*4096, 4096,(const void *)(size_t)(addr+n_sector*4096));
				/* protect sectors */
				printf("[ OK ]\n");
#else
				buf = map_physmem(addr, len, MAP_WRBACK);
			        if (!buf && addr) {
			                puts("Failed to map physical memory\n");
			                return 1;
			        }
				offset = CONFIG_FIRM_SPI_FLASH_START;
				len = CONFIG_FIRM_SPI_FLASH_SIZE;
				spi_flash_update(qflash, offset, len, buf);
#endif
				for (n_sector = 0; n_sector <= 4095; n_sector++)
				  spi_flash_protect(qflash, n_sector*4096, 4096, 1);
				/* checkout the image in spi flash */
				if (!check_uboot_image()) {
					printf("Image in SPI flash is safe\n");
				}
				else {
					printf("Image in SPI flash is not safe !!!\n");
				}
			} else {
				return -1;
			}
		} else if (strcmp(argv[1],"rescue") == 0) {
			printf("Rescue mode is not supported on this board !!!\n");
			return -1;
		} else {
			printf("Type 'help update' under the prompt to known how to use this command\n");
			return -1;
		}
	} else if (argc == 2 || ((argc == 3) && force)) {
		if (strcmp(argv[1],"restore") == 0) {
			printf("Rescue mode is not supported on this board !!!\n");
			return -1;
		} else {
			printf("Type 'help update' under the prompt to known how to use this command\n");
			return -1;
		}
	} else {
		printf("Type 'help update' under the prompt to known how to use this command\n");
		return -1;
	}	

	return 0;
}

U_BOOT_CMD(
	update,	4,	1,	do_update,
	"Updates coreboot/U-boot firmware",
	"flash addr [force]\n"
	" update coreboot/U-boot firmware Flash from address in memory\n"
#ifdef RESCUE
	"update rescue addr [force]\n"
	" update the rescue U-boot firmware from address in memory; in rescue mode only\n"
	"update restore [force]\n"
	" restore the main U-boot firmware from the rescue one; in rescue mode only\n"
        "\n"
        "Note that [force] parameter is an option. This setting allow the user to flash the binary\n"
        "after the md5sum checking, without asking whether or not the user would really like to\n"
        "burn the flash.\n"   
#endif
);
