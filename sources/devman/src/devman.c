/********************************************************************************/
/**
 * @file devman.c
 * @brief Device Manager framework implementation 
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <semaphore.h>
#include <linux/limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>

#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "devman_mod.h"
#include "adt7490.h"       
#include "temac.h"      
#include "axigpio.h"       
#include "axispi.h"        
#include "axiuart16550.h"  
#include "spibus.h"      
#include "hses10g.h"       
#include "i2cbus.h"        
#include "i2ccore.h"       
#include "lk2047t1u.h"     
#include "m41st87w.h"      
#include "mbox.h"          
#include "usplus100g.h"    
#include "xadc.h"          
#include "sysmon.h"
#include "trans.h" 
#include "ina220.h"
#include "intc.h"
#include "pmbus.h"
#include "tca6416a.h"
#include "spidev.h"
#include "eeprom.h"
#include "portmap.h"
#include "dramtester.h"
#include "eswitch.h"

extern ccl_logger_t logger;

//#define DEVMAN_DEBUG
/* printing/error-returning macros */
#ifdef DEVMAN_DEBUG
    #define PDEBUG(fmt, args...) \
        ccl_syslog_err("DEVMAN: %s[%d]: " fmt, __FUNCTION__, __LINE__, ## args)
#else
    #define PDEBUG(fmt, args...)
#endif
#define DEVMAN_ERROR_RETURN(err) do { \
   ccl_syslog_err("Error! %s[%d], error=%i\n", __FUNCTION__, __LINE__, (err)); \
   return err; \
} while (0)

#define DEVMAN_CDEV_NAME    "/dev/devman"

/** 
 *  device attribute types/names
*/
static struct tag_atype_names {
    attr_type_e type;
    char        name[32];
} _atype_names[] = { 
    { eDEVMAN_ATTR_BOOL    , "bool"},
    { eDEVMAN_ATTR_BYTE    , "byte"},
    { eDEVMAN_ATTR_HALF    , "half"},
    { eDEVMAN_ATTR_WORD    , "word"},
    { eDEVMAN_ATTR_STRING  , "string"},
    { eDEVMAN_ATTR_BUFF    , "buffer"},
    { eDEVMAN_ATTR_ETHADDR , "ethaddr"},
};

#define DEVMAN_NUM_TO_BOOL_S(n) ( (n)? "yes": "no" )
#define DEVMAN_BOOL_S_TO_NUM(s) ( (((s)[0]=='y')&&((s)[1]=='e')&&((s)[2]=='s'))? 1: 0 )

#define DEVMAN_DISCRIMINATOR_UNSPECIFIED    -1
#define DEVMAN_ATTRIDX_UNSPECIFIED          -1

static ccl_err_t _rc;

static device_driver_t _drivers[DEVMAN_DRIVERS_MAXNUM]; 
static int _registered_drivers;

static devman_device_t _devices[DEVMAN_DEVICES_MAXNUM];
static int _created_devices;

static inline ccl_err_t _fdevman_attr_read(char *dev_name, void *p_priv, 
                                           b_u32 id, b_u32 offset, 
                                           b_u32 offset_sz, b_u32 idx, 
                                           b_u8 *p_value, b_u32 length)
{
    PDEBUG("dev_name: %s, id: %d, offset: %d, offset_sz: %d, idx: %d, p_value: %p, len: %d\n\n",
           dev_name, id, offset, offset_sz, idx,p_value, length);
    if (!strncmp(dev_name, "i2ccore", DEVMAN_DEVNAME_LEN-1)) 
        return i2ccore_attr_read(p_priv, id, offset, offset_sz, idx,p_value, length);
    else if (!strncmp(dev_name, "intc", DEVMAN_DEVNAME_LEN-1)) 
        return intc_attr_read(p_priv, id, offset, offset_sz, idx,p_value, length);
    else if (!strncmp(dev_name, "axispi", DEVMAN_DEVNAME_LEN-1)) 
        return axispi_attr_read(p_priv, id, offset, offset_sz, idx,p_value, length);
    else if (!strncmp(dev_name, "axiuart", DEVMAN_DEVNAME_LEN-1)) 
        return axiuart_attr_read(p_priv, id, offset, offset_sz, idx,p_value, length);
    else if (!strncmp(dev_name, "spidev", DEVMAN_DEVNAME_LEN-1)) 
        return spidev_attr_read(p_priv, id, offset, offset_sz, idx,p_value, length);
    else if (!strncmp(dev_name, "eeprom", DEVMAN_DEVNAME_LEN-1)) 
        return eeprom_attr_read(p_priv, id, offset, offset_sz, idx,p_value, length);
    else if (!strncmp(dev_name, "rtc", DEVMAN_DEVNAME_LEN-1)) 
        return rtc_attr_read(p_priv, id, offset, offset_sz, idx,p_value, length);
    else if (!strncmp(dev_name, "temac", DEVMAN_DEVNAME_LEN-1)) 
        return temac_attr_read(p_priv, id, offset, offset_sz, idx,p_value, length);
    else if (!strncmp(dev_name, "hses10g", DEVMAN_DEVNAME_LEN-1)) 
        return hses10g_attr_read(p_priv, id, offset, offset_sz, idx,p_value, length);
    else if (!strncmp(dev_name, "usplus100g", DEVMAN_DEVNAME_LEN-1)) 
        return usplus100g_attr_read(p_priv, id, offset, offset_sz, idx,p_value, length);
    else if (!strncmp(dev_name, "axigpio", DEVMAN_DEVNAME_LEN-1)) 
        return axigpio_attr_read(p_priv, id, offset, offset_sz, idx,p_value, length);
    else if (!strncmp(dev_name, "fan", DEVMAN_DEVNAME_LEN-1)) 
        return adt7490_attr_read(p_priv, id, offset, offset_sz, idx,p_value, length);
    else if (!strncmp(dev_name, "pmbus", DEVMAN_DEVNAME_LEN-1)) 
        return pmbus_attr_read(p_priv, id, offset, offset_sz, idx,p_value, length);
    else if (!strncmp(dev_name, "lcd", DEVMAN_DEVNAME_LEN-1)) 
        return lk2047t1u_attr_read(p_priv, id, offset, offset_sz, idx,p_value, length);
    else if (!strncmp(dev_name, "mbox", DEVMAN_DEVNAME_LEN-1)) 
        return mbox_attr_read(p_priv, id, offset, offset_sz, idx,p_value, length);
    else if (!strncmp(dev_name, "trans", DEVMAN_DEVNAME_LEN-1)) 
        return trans_attr_read(p_priv, id, offset, offset_sz, idx,p_value, length);
    else if (!strncmp(dev_name, "i2cio", DEVMAN_DEVNAME_LEN-1)) 
        return tca6416a_attr_read(p_priv, id, offset, offset_sz, idx,p_value, length);
    else if (!strncmp(dev_name, "currmon", DEVMAN_DEVNAME_LEN-1)) 
        return ina220_attr_read(p_priv, id, offset, offset_sz, idx,p_value, length);
    else if (!strncmp(dev_name, "xadc", DEVMAN_DEVNAME_LEN-1)) 
        return xadc_attr_read(p_priv, id, offset, offset_sz, idx,p_value, length);
    else if (!strncmp(dev_name, "sysmon", DEVMAN_DEVNAME_LEN-1)) 
        return sysmon_attr_read(p_priv, id, offset, offset_sz, idx,p_value, length);
    else if (!strncmp(dev_name, "portmap", DEVMAN_DEVNAME_LEN-1)) 
        return portmap_attr_read(p_priv, id, offset, offset_sz, idx,p_value, length);
    else if (!strncmp(dev_name, "eswitch", DEVMAN_DEVNAME_LEN-1)) 
        return eswitch_attr_read(p_priv, id, offset, offset_sz, idx,p_value, length);
    else {
        ccl_syslog_err("dev_name: %s wasn't found\n",
               dev_name);
        return CCL_FAIL;
    }
}

static inline ccl_err_t _fdevman_attr_write(char *dev_name, void *p_priv, 
                                            b_u32 id, b_u32 offset, 
                                            b_u32 offset_sz, b_u32 idx, 
                                            b_u8 *p_value, b_u32 length)
{
    PDEBUG("dev_name: %s, id: %d, offset: %d, offset_sz: %d, idx: %d, p_value: %p, len: %d\n\n",
           dev_name, id, offset, offset_sz, idx,p_value, length);
    if (!strncmp(dev_name, "i2ccore", DEVMAN_DEVNAME_LEN-1)) 
        return i2ccore_attr_write(p_priv, id, offset, offset_sz, idx,p_value, length);
    else if (!strncmp(dev_name, "intc", DEVMAN_DEVNAME_LEN-1)) 
        return intc_attr_write(p_priv, id, offset, offset_sz, idx,p_value, length);
    else if (!strncmp(dev_name, "axispi", DEVMAN_DEVNAME_LEN-1)) 
        return axispi_attr_write(p_priv, id, offset, offset_sz, idx,p_value, length);
    else if (!strncmp(dev_name, "axiuart", DEVMAN_DEVNAME_LEN-1)) 
        return axiuart_attr_write(p_priv, id, offset, offset_sz, idx,p_value, length);
    else if (!strncmp(dev_name, "spidev", DEVMAN_DEVNAME_LEN-1)) 
        return spidev_attr_write(p_priv, id, offset, offset_sz, idx,p_value, length);
    else if (!strncmp(dev_name, "eeprom", DEVMAN_DEVNAME_LEN-1)) 
        return eeprom_attr_write(p_priv, id, offset, offset_sz, idx,p_value, length);
    else if (!strncmp(dev_name, "rtc", DEVMAN_DEVNAME_LEN-1)) 
        return rtc_attr_write(p_priv, id, offset, offset_sz, idx,p_value, length);
    else if (!strncmp(dev_name, "temac", DEVMAN_DEVNAME_LEN-1)) 
        return temac_attr_write(p_priv, id, offset, offset_sz, idx,p_value, length);
    else if (!strncmp(dev_name, "hses10g", DEVMAN_DEVNAME_LEN-1)) 
        return hses10g_attr_write(p_priv, id, offset, offset_sz, idx,p_value, length);
    else if (!strncmp(dev_name, "usplus100g", DEVMAN_DEVNAME_LEN-1)) 
        return usplus100g_attr_write(p_priv, id, offset, offset_sz, idx,p_value, length);
    else if (!strncmp(dev_name, "axigpio", DEVMAN_DEVNAME_LEN-1)) 
        return axigpio_attr_write(p_priv, id, offset, offset_sz, idx,p_value, length);
    else if (!strncmp(dev_name, "fan", DEVMAN_DEVNAME_LEN-1)) 
        return adt7490_attr_write(p_priv, id, offset, offset_sz, idx,p_value, length);
    else if (!strncmp(dev_name, "pmbus", DEVMAN_DEVNAME_LEN-1)) 
        return pmbus_attr_write(p_priv, id, offset, offset_sz, idx,p_value, length);
    else if (!strncmp(dev_name, "lcd", DEVMAN_DEVNAME_LEN-1)) 
        return lk2047t1u_attr_write(p_priv, id, offset, offset_sz, idx,p_value, length);
    else if (!strncmp(dev_name, "mbox", DEVMAN_DEVNAME_LEN-1)) 
        return mbox_attr_write(p_priv, id, offset, offset_sz, idx,p_value, length);
    else if (!strncmp(dev_name, "trans", DEVMAN_DEVNAME_LEN-1)) 
        return trans_attr_write(p_priv, id, offset, offset_sz, idx,p_value, length);
    else if (!strncmp(dev_name, "i2cio", DEVMAN_DEVNAME_LEN-1)) 
        return tca6416a_attr_write(p_priv, id, offset, offset_sz, idx,p_value, length);
    else if (!strncmp(dev_name, "currmon", DEVMAN_DEVNAME_LEN-1)) 
        return ina220_attr_write(p_priv, id, offset, offset_sz, idx,p_value, length);
    else if (!strncmp(dev_name, "xadc", DEVMAN_DEVNAME_LEN-1)) 
        return xadc_attr_write(p_priv, id, offset, offset_sz, idx,p_value, length);
    else if (!strncmp(dev_name, "sysmon", DEVMAN_DEVNAME_LEN-1)) 
        return sysmon_attr_write(p_priv, id, offset, offset_sz, idx,p_value, length);
    else if (!strncmp(dev_name, "portmap", DEVMAN_DEVNAME_LEN-1)) 
        return portmap_attr_write(p_priv, id, offset, offset_sz, idx,p_value, length);
    else if (!strncmp(dev_name, "eswitch", DEVMAN_DEVNAME_LEN-1)) 
        return eswitch_attr_write(p_priv, id, offset, offset_sz, idx,p_value, length);
    else if (!strncmp(dev_name, "dramtester", DEVMAN_DEVNAME_LEN-1)) 
        return dramtester_attr_write(p_priv, id, offset, offset_sz, idx,p_value, length);
    else
        ccl_syslog_err("Error! Invalid input parameters: dev name: %s\n", dev_name);
        return CCL_FAIL;
}

/** 
 *  open cdev
 */
static ccl_err_t _fdevman_open(char *dev_name, b_i32 *p_fd)
{
    b_i32   i, retry = 5, delay = 500; /* in msec */

    if ( !dev_name || !p_fd )
        return CCL_BAD_PARAM_ERR;

    for ( i=0; i < retry; ++i ) {
        *p_fd = open((char*)dev_name, O_RDWR);
        if ( *p_fd == -1 && EBUSY == errno ) {
            ccl_syslog_err("Warning! Can't open %s device! retry %d", dev_name, retry);
            usleep(delay * 1000);
        } else
            break;
    }
    if ( *p_fd == -1 )
        return CCL_FAIL;

    return CCL_OK;
}

/** 
 *  close cdev
 */
static ccl_err_t _fdevman_close(b_i32 fd)
{
    if ( fd != -1 )
        close(fd);
    return CCL_OK;
}

/** 
 *  Find next attribute descriptor by name substring
 */
static ccl_err_t _fdevman_attr_descr_find_next(device_driver_t *p_drv, 
                                               IN char *name, 
                                               OUT device_attr_t **pp_attr)
{    
    char    aname[DEVMAN_ATTRS_NAME_LEN+1]; 
    b_u32     i;

    /* check input parameters */
    if ( !p_drv || !name ) {
        ccl_syslog_err("Error! Invalid input parameters\n");
        DEVMAN_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* cut the index if any */
    for ( i = 0; i < strnlen(name, DEVMAN_ATTRS_NAME_LEN); i++ ) {
        if ( name[i] != '[' )
            aname[i] = name[i];
        else
            break;
    }
    aname[i] = 0;

    PDEBUGG("123$=> aname=%s, dev_type=%s\n", aname, p_drv->name);

    /* find attribute by name substring */
    for ( i = 0; i < p_drv->attrs_num; i++ ) {
        /* run to the the prev attr occurence */
        if ( *pp_attr ) {
            if ( *pp_attr == &p_drv->attrs[i] )
                *pp_attr = 0;
            continue;
        }
        /* match the pattern */
        if ( strstr(p_drv->attrs[i].name, aname) ) {
            *pp_attr = &p_drv->attrs[i];
            PDEBUGG("123$=> found attr=%s, dev=%s\n",
                    p_drv->attrs[i].name, p_drv->name);
            break;
        }
    }
    if ( i >= p_drv->attrs_num ) 
        *pp_attr = 0;

    return CCL_OK;
}

/** String to buffer conversion, the string must be pure
 *  hexdigit pattern
 *  Returns: how many bytes were processed from the input string
 */
static int _fdevman_hexstr_to_buff(char *buffer, int room_size, char *src_str)
{
    int     src_len = strnlen(src_str, DEVMAN_CMD_PARM_LEN);
    int     i, j, dst_len, cn, shift = 0;

    if ( !buffer || !room_size || !src_len || (src_len > room_size*2) )
        return 0;

    dst_len = src_len/2 + src_len%2;
    memset(buffer, 0, dst_len);
    i = src_len;
    j = dst_len-1;
    do {
        cn = src_str[--i];

        if ( (cn >='0') && (cn<='9') )
            cn = cn - '0';
        else if ( (cn>='a') && (cn<='f') )
            cn = 0xa + cn - 'a';
        else if ( (cn>='A') && (cn<='F') )
            cn = 0xA + cn - 'A';
        else
            return 0;

        buffer[j] |= (char)(cn<<shift);

        PDEBUGG("123$=> cn=%x, buffer[j]=%x\n", cn, buffer[j]);

        j -= shift>>2;
        shift ^= 4;
    } 
    while ( i );

    return dst_len;
}

#define DEVMAN_IS_HEXDIG(c)  ( ((c)>='0'&&(c)<='9')|| \
                               ((c)>='a'&&(c)<='f')|| \
                               ((c)>='A'&&(c)<='F') )

/** String to buffer conversion, the string can contain
 *  non-hexdigit symbols
 *  Returns: how many bytes were processed from the input string
 */
static ccl_err_t _fdevman_str_to_buff(char *buffer, int room_size, char *string)
{
    char    tmp_str[128] = {0};
    int     is_hexin = 0, is_delimitred = 0;
    int     str_len, bcount = 0, i, j;

    /* chipas */
    if ( !buffer || !room_size || !string || 
         !(str_len = strnlen(string, DEVMAN_CMD_PARM_LEN)) ) {
        ccl_syslog_err("Error! Invalid input parameters\n"
               "buffer=%p, room_size=%d, string=%p, str_len=%d\n",
               buffer, room_size, string, str_len);
        DEVMAN_ERROR_RETURN(0);
    }
    /* what kind of the input string here */
    for ( i = 0; i < str_len; i++ ) {
        is_hexin = is_hexin? is_hexin: DEVMAN_IS_HEXDIG(string[i]);
        is_delimitred = is_delimitred? is_delimitred: !DEVMAN_IS_HEXDIG(string[i]);
    }
    /* chipas */
    if ( !is_hexin ) {
        ccl_syslog_err("Error! There is not any digit in the input string\n");
        DEVMAN_ERROR_RETURN(0);
    }

    /* whole hexstring without separators */
    if ( !is_delimitred ) {
        if ( !(bcount = _fdevman_hexstr_to_buff(buffer, room_size, string)) ) {
            ccl_syslog_err("Error! Can't convert string to buffer\n"
                   "buffer=%p, room_size=%d, string=%s, str_len=%d\n",
                   buffer, room_size, string, str_len);
            DEVMAN_ERROR_RETURN(0);
        }
    }
    /* hexpatterns are separated in the string */
    else {
        memset(tmp_str, 0, sizeof(tmp_str));
        /* for each hexpattern */
        for ( i = 0, j = 0; i <= str_len; i++ ) {
            /* collect the hexpattern */
            if ( i < str_len && DEVMAN_IS_HEXDIG(string[i]) ) {
                tmp_str[j++] = string[i];
                tmp_str[j] = 0; 
            } else if ( j ) {
                j = 0;
                /* enforce 'byte:byte..' patterns */
                if ( !(_rc = _fdevman_hexstr_to_buff(&buffer[bcount++], 1, tmp_str)) ) {
                    ccl_syslog_err("Error! Can't convert string to buffer\n");
                    DEVMAN_ERROR_RETURN(0);
                }
            }
        }
    }
    return bcount;
}

/*** Devices types/instances/attributes visualisation APIs */

/** 
 *  Get type irregular attributes number(hidden, readonly)
 */
static int _fdevman_type_irregular_attrs_num(device_driver_t *p_drv, 
                                             int *p_hid, int *p_ro)
{
    b_u32 i;
    if ( !p_drv || !p_hid || !p_ro ) {
        ccl_syslog_err("Error! Invalid input parameters: p_drv=%p,p_hid=%p,p_ro=%p\n", 
               p_drv, p_hid, p_ro);
        DEVMAN_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    *p_hid = *p_ro = 0;   
    for ( i=0; i < p_drv->attrs_num; i++  ) {
        if ( p_drv->attrs[i].flags & eDEVMAN_ATTRFLAG_HIDDEN )
            (*p_hid)++;
        if ( p_drv->attrs[i].flags & eDEVMAN_ATTRFLAG_RO )
            (*p_ro)++;
    }
    return 0;
}

/** 
 *  Show device type attribute info
 */
static void _fdevman_attr_help(device_attr_t *p_descr)
{
    char                 helps[64+DEVMAN_ATTRS_HELP_STRLEN];
    char                 offs[20];
    char                 spaces[40];
    if ( !p_descr )
        return;
    memset(helps, 0, sizeof(helps));
    memset(offs, 0, sizeof(offs));

    snprintf(offs, sizeof(offs), "0x%x", p_descr->offset);
    snprintf(helps, sizeof(helps), "[%6s]: ", offs);
    snprintf(helps+strnlen(helps, sizeof(helps)), sizeof(helps) - 
             strnlen(helps, sizeof(helps)), "%s", p_descr->name);
    if ( p_descr->maxnum > 1 )
        snprintf(helps+strnlen(helps, sizeof(helps)), sizeof(helps) - 
                 strnlen(helps, sizeof(helps)), "[%d]", p_descr->maxnum);
    snprintf(spaces, sizeof(spaces), "%*s", sizeof(spaces) - strnlen(helps, sizeof(helps)), " ");
    snprintf(helps+strnlen(helps, sizeof(helps)), sizeof(helps) - 
             strnlen(helps, sizeof(helps)), "%s", spaces);
    snprintf(helps+strnlen(helps, sizeof(helps)), sizeof(helps) - 
             strnlen(helps, sizeof(helps)), "- ");
    snprintf(helps+strnlen(helps, sizeof(helps)), sizeof(helps) - 
             strnlen(helps, sizeof(helps)), "%s", _atype_names[p_descr->type].name);
    if ( (p_descr->type == eDEVMAN_ATTR_STRING) || 
         (p_descr->type == eDEVMAN_ATTR_BUFF) ||
         (p_descr->type == eDEVMAN_ATTR_ETHADDR) )
        snprintf(helps+strnlen(helps, sizeof(helps)), sizeof(helps) - 
                 strnlen(helps, sizeof(helps)), "/%d", p_descr->size);
    if ( !p_descr->flags )
        snprintf(helps+strnlen(helps, sizeof(helps)), sizeof(helps) - 
                 strnlen(helps, sizeof(helps)), "; ");
    else {
        snprintf(helps+strnlen(helps, sizeof(helps)), sizeof(helps) - 
                 strnlen(helps, sizeof(helps)), ",");
        char hid[8]="", ro[8]=""; 
        if ( p_descr->flags & eDEVMAN_ATTRFLAG_HIDDEN )
            snprintf(hid, 5, "HID|");
        if ( p_descr->flags & eDEVMAN_ATTRFLAG_RO )
            snprintf(ro, 4, "RO|");
        /*...123$*/
        snprintf(helps+strnlen(helps, sizeof(helps)), sizeof(helps) - 
                 strnlen(helps, sizeof(helps)), "%s%s\b; ", hid, ro);

    }
    snprintf(helps+strnlen(helps, sizeof(helps)), sizeof(helps) - 
             strnlen(helps, sizeof(helps)), "%s", p_descr->help);
    PRINTL("%s\n", helps);
    return;
}    

/** 
 *  Show device attribute, if array element - from the
 *  <start_idx>, <print_elems_num> elements
 */
static void _fdevman_attr_show(devman_device_t *p_dev, 
                               device_attr_t *p_descr, 
                               b_u32 start_idx, 
                               int print_elems_num)
{
    char                 offs_n[20], offs_s[32];
    char                 aname[DEVMAN_ATTRS_NAME_LEN+1];
    char                 aval[20];
    const char           *form;
    b_u32                j, k, offset; 
    int                  n, printed_cnt=0;
    
    PDEBUG("pdev=%p, p_descr=%p, start_idx=%d, p_descr->maxnum=%d\n", 
           p_dev, p_descr, start_idx, p_descr->maxnum);
    if ( !p_dev || !p_descr || start_idx >= p_descr->maxnum ) {
        PRINTL("%s[%d] - pdev=%p, p_descr=%p, start_idx=%d, p_descr->maxnum=%d\n", 
               __FUNCTION__, __LINE__, p_dev, p_descr, start_idx, p_descr->maxnum);
        return;
    }
    if (p_descr->flags == eDEVMAN_ATTRFLAG_WO ) 
        return;

    memset(offs_n, 0, sizeof(offs_n));
    memset(offs_s, 0, sizeof(offs_s));
    memset(aname, 0, sizeof(aname));
    memset(aval, 0, sizeof(aval));
    /* force truncation of overprinting */
    if ( (start_idx + print_elems_num) > p_descr->maxnum )
        print_elems_num = p_descr->maxnum - start_idx;
    //snprintf(aname, sizeof(aname), "%s", p_descr->name);
    switch ( p_descr->type ) {
    case eDEVMAN_ATTR_BOOL:
        {
            for ( j=start_idx; j < start_idx + print_elems_num; j++ ) {
                b_bool b;
                _rc = devman_attr_array_get_bool(p_dev, p_descr->name, j, &b);
                if ( _rc ) {
                    PRINTL("%s[%d] - devman_attr_array_get_bool error: name: %s, index: %d \n", 
                           __FUNCTION__, __LINE__, p_descr->name, j);
                    continue;
                }
                offset = p_descr->offset+j*sizeof(b_bool);
                snprintf(offs_n, sizeof(offs_n), "0x%x", offset);
                snprintf(offs_s, sizeof(offs_s), "[%7s]: ", offs_n);
                if (p_descr->maxnum > 1) {
                    //PRINTL("%s%24s[%d] = %s\n", offs_s, aname, j, DEVMAN_NUM_TO_BOOL_S(b));
                    n = snprintf(aname, DEVMAN_ATTRS_NAME_LEN+1, "%s[%d]", p_descr->name, j);
                    if (n < 0)
                        return;
                } else {
                    n = snprintf(aname, DEVMAN_ATTRS_NAME_LEN+1, "%s", p_descr->name);
                    if (n < 0)
                        return;
                }    
                PRINTL("%s%27s = %s\n", offs_s, aname, DEVMAN_NUM_TO_BOOL_S(b));
            }
            break;
        }
    case eDEVMAN_ATTR_BYTE:
        {
            form = p_descr->format;
            form = form? form: "0x%02x";
            for ( j=start_idx; j < start_idx + print_elems_num; j++ ) {
                b_u8 b;
                _rc = devman_attr_array_get_byte(p_dev, p_descr->name, j, &b);
                if ( _rc ) {
                    PRINTL("%s[%d] - devman_attr_array_get_byte error: name: %s, index: %d \n", 
                           __FUNCTION__, __LINE__, p_descr->name, j);
                    continue;
                }
                offset = p_descr->offset+j*sizeof(b_u8);
                snprintf(offs_n, sizeof(offs_n), "0x%x", offset);
                snprintf(offs_s, sizeof(offs_s), "[%7s]: ", offs_n);
                snprintf(aval,sizeof(aval), form,b);
                if (p_descr->maxnum > 1) {
                    //PRINTL("%s%24s[%d] = %s\n", offs_s, aname, j, aval);
                    n = snprintf(aname, DEVMAN_ATTRS_NAME_LEN+1, "%s[%d]", p_descr->name, j);
                    if (n < 0)
                        return;
                } else {
                    //PRINTL("%s%27s = %s\n", offs_s, aname, aval);
                    n = snprintf(aname, DEVMAN_ATTRS_NAME_LEN+1, "%s", p_descr->name);
                    if (n < 0)
                        return;
                }
                PRINTL("%s%27s = %s\n", offs_s, aname, aval);
            }
            break;
        }
    case eDEVMAN_ATTR_HALF:
        {
            form = p_descr->format;
            form = form? form: "0x%04x";
            for ( j=start_idx; j < start_idx + print_elems_num; j++ ) {
                b_u16 h;
                _rc = devman_attr_array_get_half(p_dev, p_descr->name, j, &h);
                if ( _rc ) {
                    PRINTL("%s[%d] - devman_attr_array_get_half error: name: %s, index: %d \n", 
                           __FUNCTION__, __LINE__, p_descr->name, j);
                    continue;
                }
                offset = p_descr->offset+j*sizeof(b_u16);
                snprintf(offs_n, sizeof(offs_n), "0x%x", offset);
                snprintf(offs_s, sizeof(offs_s), "[%7s]: ", offs_n);
                snprintf(aval,sizeof(aval),form,h);
                if (p_descr->maxnum > 1) {
                    //PRINTL("%s%24s[%d] = %s\n", offs_s, aname, j, aval);
                    n = snprintf(aname, DEVMAN_ATTRS_NAME_LEN+1, "%s[%d]", p_descr->name, j);
                    if (n < 0)
                        return;
                } else {
                    //PRINTL("%s%27s = %s\n", offs_s, aname, aval);
                    n = snprintf(aname, DEVMAN_ATTRS_NAME_LEN+1, "%s", p_descr->name);
                    if (n < 0)
                        return;
                }
                PRINTL("%s%27s = %s\n", offs_s, aname, aval);    
            }
            break;
        }
    case eDEVMAN_ATTR_WORD:
        {
            form = p_descr->format;
            form = form? form: "0x%08x";
            for ( j=start_idx; j < start_idx + print_elems_num; j++ ) {
                b_u32 w;
                _rc = devman_attr_array_get_word(p_dev, p_descr->name, j, &w);
                if ( _rc ) {
                    PRINTL("%s[%d] - devman_attr_array_get_word error: name: %s, index: %d \n", 
                           __FUNCTION__, __LINE__, p_descr->name, j);
                    continue;
                }
                offset = p_descr->offset+j*sizeof(b_u32);
                snprintf(offs_n, sizeof(offs_n), "0x%x", offset);
                snprintf(offs_s, sizeof(offs_s), "[%7s]: ", offs_n);
                snprintf(aval,sizeof(aval),form,w);
                if (p_descr->maxnum > 1) {
                    //PRINTL("%s%24s[%d] = %s\n", offs_s, aname, j, aval);
                    n = snprintf(aname, DEVMAN_ATTRS_NAME_LEN+1, "%s[%d]", p_descr->name, j);
                    if (n < 0)
                        return;                
                } else {
                    //PRINTL("%s%27s = %s\n", offs_s, aname, aval);
                    n = snprintf(aname, DEVMAN_ATTRS_NAME_LEN+1, "%s", p_descr->name);
                    if (n < 0)
                        return;                                    
                }
                PRINTL("%s%27s = %s\n", offs_s, aname, aval);    
            }
            break;
        }
    case  eDEVMAN_ATTR_BUFF:
        {
            int is_mac = 0;
            if ( p_descr->format )
                is_mac = strstr(p_descr->format,"mac")? 1: 0;
            for ( j=start_idx; j < start_idx + print_elems_num; j++ ) {
                b_u8    buff[DEVMAN_ATTRS_VISIBLE_DEPTH];
                _rc = devman_attr_array_get_buff(p_dev, p_descr->name, j, 
                                                 buff, DEVMAN_ATTRS_VISIBLE_DEPTH);
                if ( _rc ) {
                    PRINTL("%s[%d] - devman_attr_array_get_buff error: name: %s, index: %d \n", 
                           __FUNCTION__, __LINE__, p_descr->name, j);
                    continue;
                }
                offset = p_descr->offset+j*p_descr->size;
                snprintf(offs_n, sizeof(offs_n), "0x%x", offset);
                snprintf(offs_s, sizeof(offs_s), "[%7s]: ", offs_n);
                if ( is_mac ) {
                    if (p_descr->maxnum > 1) {
                        //PRINTL("%s%24s[%d] = ", offs_s, aname, j);
                        n = snprintf(aname, DEVMAN_ATTRS_NAME_LEN+1, 
                                     "%s[%d]", p_descr->name, j);
                        if (n < 0)
                            return;                                    
                    } else {
                        n = snprintf(aname, DEVMAN_ATTRS_NAME_LEN+1, 
                                     "%s", p_descr->name);
                        if (n < 0)
                            return;                                    
                    }
                    PRINTL("%s%27s = ", offs_s, aname);
                    for ( k = 0; k < p_descr->size && k < DEVMAN_ATTRS_PRINT_MAXBYTES; k++ )
                        PRINTL("%02x:", buff[k]);
                    PRINTL("\b \n");
                } else {
                    if (p_descr->maxnum > 1) {
                        //PRINTL("%s%24s[%d] = 0x[", offs_s, aname, j);
                        n = snprintf(aname, DEVMAN_ATTRS_NAME_LEN+1, 
                                     "%s[%d]", p_descr->name, j);
                        if (n < 0)
                            return;                                    
                    } else {
                        //PRINTL("%s%27s = 0x[", offs_s, aname);
                        n = snprintf(aname, DEVMAN_ATTRS_NAME_LEN+1, 
                                     "%s", p_descr->name);
                        if (n < 0)
                            return;                                    
                    }
                    PRINTL("%s%27s = 0x[", offs_s, aname);    
                    for ( k = 0; k < p_descr->size && k < DEVMAN_ATTRS_PRINT_MAXBYTES; k++ )
                        PRINTL("%02x,", buff[k]);
                    if ( k < p_descr->size )
                        PRINTL("...");
                    PRINTL("\b]\n");
                }
            }
            break;
        }
    case  eDEVMAN_ATTR_STRING:
        {
            for ( j=start_idx; j < start_idx + print_elems_num; j++ ) {
                b_u8  str[DEVMAN_ATTRS_VISIBLE_DEPTH];
                _rc = devman_attr_array_get_string(p_dev, p_descr->name, j, str);
                if ( _rc ) {
                    PRINTL("%s[%d] - devman_attr_array_get_string error: name: %s, index: %d \n", 
                           __FUNCTION__, __LINE__, p_descr->name, j);
                    continue;
                }
                offset = p_descr->offset+j*p_descr->size;
                snprintf(offs_n, sizeof(offs_n), "0x%x", offset);
                snprintf(offs_s, sizeof(offs_s), "[%7s]: ", offs_n);
                if (p_descr->maxnum > 1) {
                    //PRINTL("%s%24s[%d] = \"%s\"\n", offs_s, aname, j, str);
                    n = snprintf(aname, DEVMAN_ATTRS_NAME_LEN+1, 
                                 "%s[%d]", p_descr->name, j);
                    if (n < 0)
                        return;                                                       
                } else {
                    n = snprintf(aname, DEVMAN_ATTRS_NAME_LEN+1, 
                                 "%s", p_descr->name);
                    if (n < 0)
                        return;                                                       
                }
                PRINTL("%s%27s = \"%s\"\n", offs_s, aname, str);    
            }
            break;
        }
    default:
        {
            break;
        }
    }
    return;
}    

/** 
 *  Show command
 */
static void _fdevman_cmd_show(device_cmd_t *p_cmd)
{
    char    str[DEVMAN_DEVICE_CMD_HELP_STRLEN];
    char    lbracket, rbracket;
    b_u32   i; 
    int     len;

    len = snprintf(str, sizeof(str), "%s ", p_cmd->name);
    for ( i = 0; i < p_cmd->parms_num; i++ ) {
        lbracket = '<'; rbracket = '>';
        if ( p_cmd->parms[i].flags & eDEVMAN_ATTRFLAG_OPTIONAL ) {
            lbracket = '['; rbracket = ']';
        }
        len += snprintf(str+len, sizeof(str)-len, "%c%s%c ", 
                        lbracket, p_cmd->parms[i].name, rbracket);
    }
    if ( len < 31 ) {
        for ( i = len; i < 26; i++ )
            len += snprintf(str+len, sizeof(str)-len, " ");
    }
    len += snprintf(str+len, sizeof(str)-len, " - %s", p_cmd->help);
    PRINTL("%s\n", str);
    return;    
}

/** 
 *  Show device commands
 */
static void _fdevman_device_cmds_show(devman_device_t *p_dev, char *name)
{
    int     is_match = 0; 
    b_u32   i;

    if ( !p_dev || !p_dev->p_drv )
        return;
    /* check if some commands are defined for the device */
    if ( !p_dev->p_drv->cmds_num ) {
        PRINTL("There is no commands defined for the device %s\n", p_dev->name);
        return;
    }
    /* try show similar to pattern only if any */
    if ( name ) {
        for ( i = 0; i <  p_dev->p_drv->cmds_num; i++ ) {
            if ( strstr(p_dev->p_drv->cmds[i].name, name) ) {
                if ( !is_match )
                    PRINTL("-----------------------\n%s commands similar to %s:\n-----------------------\n", 
                           p_dev->name, name);
                _fdevman_cmd_show(&p_dev->p_drv->cmds[i]);
                is_match = 1;
            }
        }
    }
    /* there isn't pattern or we haven't any match, show all commands */
    if ( !is_match ) {
        PRINTL("-----------------------\n%s commands list:\n-----------------------\n", 
               p_dev->name);
        for ( i = 0; i <  p_dev->p_drv->cmds_num; i++ )
            _fdevman_cmd_show(&p_dev->p_drv->cmds[i]);
    }
    PRINTL("\n");
    return;
}

/** 
 *  Show device instance with attributes/values
 */
static void _fdevman_device_show(devman_device_t *p_dev, int show_all)
{
    int     hid=0, ro=0; 
    b_u32   i; 

    if ( !p_dev || !p_dev->p_drv )
        return;
    if ( _fdevman_type_irregular_attrs_num(p_dev->p_drv, &hid, &ro) ) {
        PRINTL("Can't get device %s irregular attrs number\n", p_dev->name);
        return;
    }
    PRINTL("===================================================\n");
    PRINTL("Device %s/%d: signature = 0x%08x\n", 
           p_dev->name, (p_dev->sig & BRDDEV_MINOR_MASK), p_dev->sig); 
    PDEBUGG("p_drv=0x%x,p_priv=0x%x\n", (int)p_dev->p_drv, (int)p_dev->p_priv);
    PRINTL("---------------------------------------------------\n");
    PRINTL("attrs_num=%d(hidden=%d,readonly=%d), commands=%d\n", 
           p_dev->p_drv->attrs_num, hid, ro, p_dev->p_drv->cmds_num);
    PRINTL("---------------------------------------------------\n");
    PDEBUGG("driver <%s>: p_drv->f_add=0x%x,p_drv->f_cut=0x%x\n", 
            p_dev->p_drv->name, (int)p_dev->p_drv->f_add, (int)p_dev->p_drv->f_cut);
    for ( i = 0; i <  p_dev->p_drv->attrs_num; i++ ) {
        if ( show_all || !(p_dev->p_drv->attrs[i].flags & eDEVMAN_ATTRFLAG_HIDDEN) )
            _fdevman_attr_show(p_dev, &p_dev->p_drv->attrs[i], 
                               0, DEVMAN_ATTRS_PRINT_ELEMS_DSHOW);
    }
    if ( show_all ) {
        _fdevman_device_cmds_show(p_dev, 0/*no pattern, show all commands*/);
        return;
    }
    PRINTL("\n"); 
}

/** 
 *  
 */
static ccl_err_t _fdevman_device_parse_type_discr(IN char *dev_str, 
                                                  OUT char *type, 
                                                  OUT int *p_discr)
{
    char    discr_str[20], *p_e;
    int     discr;
    b_u32   i; 

    /* check input parameters */
    if ( !dev_str || !dev_str || !p_discr ) {
        ccl_syslog_err("Error! Invalid input parameters\n");
        DEVMAN_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* separate the attribute name and index if any */
    for ( i = 0; i < strnlen(dev_str, DEVMAN_CMD_PARM_LEN); i++ ) {
        if ( dev_str[i] != '/' )
            type[i] = dev_str[i];
        else {
            memcpy(discr_str, &dev_str[i+1], 
                   (strnlen(dev_str, DEVMAN_CMD_PARM_LEN) - i));
            break;
        }
    }
    type[i] = 0;
    discr_str[strnlen(dev_str, DEVMAN_CMD_PARM_LEN)-i] = 0;
    PDEBUGG("123$=> type=%s,discr_str=%s\n", type, discr_str);
    discr = strtol(discr_str, &p_e, 10);
    if ( p_e != discr_str )
        *p_discr = discr;
    else
        *p_discr = DEVMAN_DISCRIMINATOR_UNSPECIFIED;
    return CCL_OK;
}

/** 
 *  
 */
static ccl_err_t _fdevman_attr_parse_name_idx(IN char *attr_str, 
                                              OUT char *name, 
                                              OUT int *p_idx)
{
    char    idx_str[20] = {0}; 
    char    *p_e = NULL;
    int     idx; 
    b_u32   i;

    /* check input parameters */
    if ( !attr_str || !name || !p_idx ) {
        ccl_syslog_err("Error! Invalid input parameters\n");
        DEVMAN_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* separate the attribute name and index if any */
    for ( i = 0; i < strnlen(attr_str, DEVMAN_ATTRS_NAME_LEN); i++ ) {
        if ( attr_str[i] != '[' )
            name[i] = attr_str[i];
        else {
            memcpy(idx_str, &attr_str[i+1], 
                   (strnlen(attr_str, DEVMAN_ATTRS_NAME_LEN) - i -1));
            break;
        }
    }
    name[i] = 0;
    idx_str[strnlen(attr_str, DEVMAN_ATTRS_NAME_LEN)-i-1] = 0;
    idx = strtol(idx_str, &p_e, 10);
    if ( p_e != idx_str )
        *p_idx = idx;
    else {
        *p_idx = DEVMAN_ATTRIDX_UNSPECIFIED;
    }

    return CCL_OK;
}

ccl_err_t devman_ioctl(b_i32 opcode, void *v_parm)
{
    devman_ioctl_parm_t   *p_parm = (devman_ioctl_parm_t *)v_parm;
    b_i32                 fd;

    /* chips */
    if ( !p_parm ) {
        ccl_syslog_err("Error! Invalid input parameters, p_parm=%p\n", p_parm);
        DEVMAN_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    _rc = _fdevman_open(DEVMAN_CDEV_NAME, &fd);
    if ( _rc ) {
        ccl_syslog_err("Error! Can't open %s device!\n", DEVMAN_CDEV_NAME);
        DEVMAN_ERROR_RETURN(_rc);
    }

    PDEBUGG("123$=> opcode=0x%08x,p_parm=%p\n"
           "name=%s, page=0x%08x, offset=0x%08x, value=0x%08x, p=0x%08x, length=0x%08x\n", 
           opcode, p_parm,
           p_parm->name, p_parm->page, p_parm->offset, 
           p_parm->value, p_parm->p, p_parm->length);

    _rc = ioctl(fd, opcode, p_parm);
    _fdevman_close(fd);

    if ( _rc ) {
        ccl_syslog_err("Error! Can't ioctl the %s device! _rc=%d\n", 
               DEVMAN_CDEV_NAME, _rc);
        DEVMAN_ERROR_RETURN(_rc);
    }

    return CCL_OK;
}

ccl_err_t devman_attr_descr_find(IN devo_t devo, 
                                 IN char *name, 
                                 OUT device_attr_t **pp_attr)
{    
    devman_device_t  *p_dev;
    device_driver_t  *p_drv;
    char             aname[DEVMAN_ATTRS_NAME_LEN+1]; 
    b_u32            i;

    /* check input parameters */
    if ( !devo || !name ) {
        ccl_syslog_err("Error! Invalid input parameters\n");
        DEVMAN_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get device/driver handle */
    p_dev = (devman_device_t *)devo;
    p_drv = p_dev->p_drv;

    /* cut the index if any */
    for ( i = 0; i < strnlen(name, DEVMAN_ATTRS_NAME_LEN); i++ ) {
        if ( name[i] != '[' )
            aname[i] = name[i];
        else
            break;
    }
    aname[i] = 0;

    PDEBUGG("123$=> aname=%s, dev_type=%s\n", aname, p_drv->name);

    /* find attribute by name */
    *pp_attr = 0;
    for ( i = 0; i < p_drv->attrs_num; i++ ) {
        if ( !strncmp(p_drv->attrs[i].name, aname, DEVMAN_ATTRS_NAME_LEN-1) ) {
            *pp_attr = &p_drv->attrs[i];
            PDEBUGG("123$=> found attr=%s, dev=%s\n",
                    p_drv->attrs[i].name, p_drv->name);
            break;
        }
    }   
    return CCL_OK;
}

ccl_err_t devman_attr_array_set_bool(IN devo_t devo, IN char *name, 
                                     IN b_u32 idx, IN b_bool value)
{
    return devman_attr_array_set_byte(devo, name, idx, (b_u8)value);
}

ccl_err_t devman_attr_array_get_bool(IN devo_t devo, IN char *name, 
                                     IN b_u32 idx, OUT b_bool *p_value)
{
    return devman_attr_array_get_byte(devo, name, idx, (b_u8 *)p_value);
}

ccl_err_t devman_attr_array_set_byte(IN devo_t devo, IN char *name, 
                                     IN b_u32 idx, IN b_u8 value)
{
    devman_device_t  *p_dev;
    device_attr_t    *p_attr;
    b_u8             *p_priv_field;             

    /* check input parameters */
    if ( !devo || !name ) {
        ccl_syslog_err("Error! Invalid input parameters\n");
        DEVMAN_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get device handle */
    p_dev = (devman_device_t *)devo;

    /* try to find the attribute */
    _rc = devman_attr_descr_find(devo, name, &p_attr);
    if ( _rc || !p_attr ) {
        ccl_syslog_err("Error! Can't find %s attr on device %s\n", 
               name, p_dev->name);
        DEVMAN_ERROR_RETURN(CCL_FAIL);
    }
    /* do nothing if attribute is read-only */
    if ( p_attr->flags & eDEVMAN_ATTRFLAG_RO ) {
        ccl_syslog_err("Warning! Try to set read-only attribute %s attr on device %s\n", 
               name, p_dev->name);
        return 0;
    }

    /* check attribute type */
    if ( p_attr->type != eDEVMAN_ATTR_BYTE && 
         p_attr->type != eDEVMAN_ATTR_BOOL/*123$*/) {
        ccl_syslog_err("Error! Can't set byte for attr=%s, type=%d, idx=%d, on device %s\n", 
               name, p_attr->type, idx, p_dev->name);
        DEVMAN_ERROR_RETURN(CCL_FAIL);
    }
    /* check if there is the write accessor */
    if ( p_attr->offset_sz ) {
        _rc = _fdevman_attr_write(p_dev->name, p_dev->p_priv, p_attr->id, 
                                p_attr->offset, p_attr->offset_sz, 
                                idx, &value, sizeof(b_u8));
        if ( _rc ) {
            ccl_syslog_err("Error! Can't write by accessor %s attr on device %s\n", 
                   name, p_dev->name);
            DEVMAN_ERROR_RETURN(CCL_FAIL);
        }
    }
    /* if there isn't, try to write half by attr offset */
    else if ( p_dev->p_priv ) {
        int offset = p_attr->offset + ((p_attr->maxnum-1)? idx*sizeof(b_u8): 0);
        p_priv_field = (b_u8 *)((char *)p_dev->p_priv + offset);
        *p_priv_field = value;
    } else {
        ccl_syslog_err("Error! Can't find howto write attr=%s on device %s\n", 
               name, p_dev->name);
        DEVMAN_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}

ccl_err_t devman_attr_array_get_byte(IN devo_t devo, IN char *name, 
                                     IN b_u32 idx, OUT b_u8 *p_value)
{
    devman_device_t  *p_dev;
    device_attr_t    *p_attr;
    b_u8             *p_priv_field;             

    /* check input parameters */
    if ( !devo || !name || !p_value ) {
        ccl_syslog_err("Error! Invalid input parameters\n");
        DEVMAN_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get device handle */
    p_dev = (devman_device_t *)devo;

    /* try to find the attribute */
    _rc = devman_attr_descr_find(devo, name, &p_attr);
    if ( _rc || !p_attr ) {
        ccl_syslog_err("Error! Can't find %s attr on device %s\n", name, p_dev->name);
        DEVMAN_ERROR_RETURN(CCL_FAIL);
    }
    /* check attribute type */
    if ( p_attr->type != eDEVMAN_ATTR_BYTE && 
         p_attr->type != eDEVMAN_ATTR_BOOL ) {
        ccl_syslog_err("Error! Can't get byte for attr=%s, type=%d, idx=%d, on device %s\n", 
               name, p_attr->type, idx, p_dev->name);
        DEVMAN_ERROR_RETURN(CCL_FAIL);
    }

    /* check if there is the read accessor */
    if ( p_attr->offset_sz ) {
        _rc = _fdevman_attr_read(p_dev->name, p_dev->p_priv, p_attr->id, 
                               p_attr->offset, p_attr->offset_sz, 
                               idx, p_value, sizeof(b_u8));
        if ( _rc ) {
            ccl_syslog_err("Error! Can't read by accessor %s attr on device %s\n", 
                   name, p_dev->name);
            DEVMAN_ERROR_RETURN(CCL_FAIL);
        }
    }
    /* if there isn't, try to read half by attr offset */
    else if ( p_dev->p_priv ) {
        int offset = p_attr->offset + ((p_attr->maxnum-1)? idx*sizeof(b_u8): 0);
        p_priv_field = (b_u8 *)((char *)p_dev->p_priv + offset);
        *p_value = *p_priv_field;
    } else {
        ccl_syslog_err("Error! Can't find howto read %s attr on device %s\n", 
               name, p_dev->name);
        DEVMAN_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}

ccl_err_t devman_attr_array_set_half(IN devo_t devo, IN char *name, 
                                     IN b_u32 idx, IN b_u16 value)
{
    devman_device_t  *p_dev;
    device_attr_t    *p_attr;
    b_u16            *p_priv_field;             

    /* check input parameters */
    if ( !devo || !name ) {
        ccl_syslog_err("Error! Invalid input parameters\n");
        DEVMAN_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get device handle */
    p_dev = (devman_device_t *)devo;

    /* try to find the attribute */
    _rc = devman_attr_descr_find(devo, name, &p_attr);
    if ( _rc || !p_attr ) {
        ccl_syslog_err("Error! Can't find %s attr on device %s\n", name, p_dev->name);
        DEVMAN_ERROR_RETURN(CCL_FAIL);
    }
    /* do nothing if attribute is read-only */
    if ( p_attr->flags & eDEVMAN_ATTRFLAG_RO ) {
        ccl_syslog_err("Warning! Try to set read-only attribute %s attr on device %s\n", 
               name, p_dev->name);
        return 0;
    }
    /* check attribute type */
    if ( p_attr->type != eDEVMAN_ATTR_HALF ) {
        ccl_syslog_err("Error! Can't set half for attr=%s, type=%d, idx=%d, on device %s\n", 
               name, p_attr->type, idx, p_dev->name);
        DEVMAN_ERROR_RETURN(CCL_FAIL);
    }
    /* check if there is the write accessor */
    if ( p_attr->offset_sz ) {
        _rc = _fdevman_attr_write(p_dev->name, p_dev->p_priv, p_attr->id, 
                                p_attr->offset, p_attr->offset_sz, 
                                idx, (b_u8 *)&value, sizeof(b_u16));
        if ( _rc ) {
            ccl_syslog_err("Error! Can't write by accessor %s attr on device %s\n", 
                   name, p_dev->name);
            DEVMAN_ERROR_RETURN(CCL_FAIL);
        }
    }
    /* if there isn't, try to write half by attr offset */
    else if ( p_dev->p_priv ) {
        int offset = p_attr->offset + ((p_attr->maxnum-1)? idx*sizeof(b_u16): 0);
        p_priv_field = (b_u16 *)((char *)p_dev->p_priv + offset);
        *p_priv_field = value;
    } else {
        ccl_syslog_err("Error! Can't find howto write attr=%s on device %s\n", 
               name, p_dev->name);
        DEVMAN_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}

ccl_err_t devman_attr_array_get_half(IN devo_t devo, IN char *name, 
                                     IN b_u32 idx, OUT b_u16 *p_value)
{
    devman_device_t  *p_dev;
    device_attr_t    *p_attr;
    b_u16            *p_priv_field;             

    /* check input parameters */
    if ( !devo || !name || !p_value ) {
        ccl_syslog_err("Error! Invalid input parameters\n");
        DEVMAN_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get device handle */
    p_dev = (devman_device_t *)devo;

    /* try to find the attribute */
    _rc = devman_attr_descr_find(devo, name, &p_attr);
    if ( _rc || !p_attr ) {
        ccl_syslog_err("Error! Can't find %s attr on device %s\n", name, p_dev->name);
        DEVMAN_ERROR_RETURN(CCL_FAIL);
    }
    /* check attribute type */
    if ( p_attr->type != eDEVMAN_ATTR_HALF ) {
        ccl_syslog_err("Error! Can't get half for attr=%s, type=%d, idx=%d, on device %s\n", 
               name, p_attr->type, idx, p_dev->name);
        DEVMAN_ERROR_RETURN(CCL_FAIL);
    }
    /* check if there is the read accessor */
    if ( p_attr->offset_sz ) {
        _rc = _fdevman_attr_read(p_dev->name, p_dev->p_priv, p_attr->id, 
                               p_attr->offset, p_attr->offset_sz, 
                               idx, (b_u8 *)p_value, sizeof(b_u16));
        if ( _rc ) {
            ccl_syslog_err("Error! Can't read by accessor %s attr on device %s\n", 
                   name, p_dev->name);
            DEVMAN_ERROR_RETURN(CCL_FAIL);
        }
    } 
    /* if there isn't, try to read half by attr offset */
    else if ( p_dev->p_priv ) {
        int offset = p_attr->offset + ((p_attr->maxnum-1)? idx*sizeof(b_u16): 0);
        p_priv_field = (b_u16 *)((char *)p_dev->p_priv + offset);
        *p_value = *p_priv_field;
    } else {
        ccl_syslog_err("Error! Can't find howto read %s attr on device %s\n", 
               name, p_dev->name);
        DEVMAN_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}

ccl_err_t devman_attr_array_set_word(IN devo_t devo, IN char *name, 
                                     IN b_u32 idx, IN b_u32 value)
{
    devman_device_t  *p_dev;
    device_attr_t    *p_attr;
    b_u32            *p_priv_field;             

    /* check input parameters */
    if ( !devo || !name ) {
        ccl_syslog_err("Error! Invalid input parameters\n");
        DEVMAN_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get device handle */
    p_dev = (devman_device_t *)devo;

    /* try to find the attribute */
    _rc = devman_attr_descr_find(devo, name, &p_attr);
    if ( _rc || !p_attr ) {
        ccl_syslog_err("Error! Can't find %s attr on device %s\n", name, p_dev->name);
        DEVMAN_ERROR_RETURN(CCL_FAIL);
    }
    /* do nothing if attribute is read-only */
    if ( p_attr->flags & eDEVMAN_ATTRFLAG_RO ) {
        ccl_syslog_err("Warning! Try to set read-only attribute %s attr on device %s\n", 
               name, p_dev->name);
        return 0;
    }
    /* check attribute type */
    if ( p_attr->type != eDEVMAN_ATTR_WORD ) {
        ccl_syslog_err("Error! Can't set word for attr=%s, type=%d, idx=%d, on device %s\n", 
               name, p_attr->type, idx, p_dev->name);
        DEVMAN_ERROR_RETURN(CCL_FAIL);
    }
    /* check if there is the write accessor */
    if ( p_attr->offset_sz ) {
        _rc = _fdevman_attr_write(p_dev->name, p_dev->p_priv, p_attr->id, 
                                p_attr->offset, p_attr->offset_sz, 
                                idx, (b_u8 *)&value, sizeof(b_u32));
        if ( _rc ) {
            ccl_syslog_err("Error! Can't write by accessor %s attr on device %s\n", 
                   name, p_dev->name);
            DEVMAN_ERROR_RETURN(CCL_FAIL);
        }
    }
    /* if there isn't, try to write word by attr offset */
    else if ( p_dev->p_priv ) {
        int offset = p_attr->offset + ((p_attr->maxnum-1)? idx*sizeof(b_u32): 0);
        p_priv_field = (b_u32 *)((char *)p_dev->p_priv + offset);
        *p_priv_field = value;
    } else {
        ccl_syslog_err("Error! Can't find howto write attr=%s on device %s\n", 
               name, p_dev->name);
        DEVMAN_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}

ccl_err_t devman_attr_array_get_word(IN devo_t devo, IN char *name, 
                                     IN b_u32 idx, OUT b_u32 *p_value)
{
    devman_device_t  *p_dev;
    device_attr_t    *p_attr;
    b_u32            *p_priv_field;             

    /* check input parameters */
    if ( !devo || !name || !p_value ) {
        ccl_syslog_err("Error! Invalid input parameters\n");
        DEVMAN_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get device handle */
    p_dev = (devman_device_t *)devo;

    /* try to find the attribute */
    _rc = devman_attr_descr_find(devo, name, &p_attr);
    if ( _rc || !p_attr ) {
        ccl_syslog_err("Error! Can't find %s attr on device %s\n", name, p_dev->name);
        DEVMAN_ERROR_RETURN(CCL_FAIL);
    }
    /* check attribute type */
    if ( p_attr->type != eDEVMAN_ATTR_WORD ) {
        ccl_syslog_err("Error! Can't get word for attr=%s, type=%d, idx=%d, on device %s\n", 
               name, p_attr->type, idx, p_dev->name);
        DEVMAN_ERROR_RETURN(CCL_FAIL);
    }
    /* check if there is the read accessor */
    if ( p_attr->offset_sz ) {
        _rc = _fdevman_attr_read(p_dev->name, p_dev->p_priv, p_attr->id, 
                               p_attr->offset, p_attr->offset_sz, 
                               idx, (b_u8 *)p_value, sizeof(b_u32));
        if ( _rc ) {
            ccl_syslog_err("Error! Can't read by accessor %s attr on device %s\n", 
                   name, p_dev->name);
            DEVMAN_ERROR_RETURN(CCL_FAIL);
        }
    }
    /* if there isn't, try to read word by attr offset */
    else if ( p_dev->p_priv ) {
        int offset = p_attr->offset + ((p_attr->maxnum-1)? idx*sizeof(b_u32): 0);
        p_priv_field = (b_u32 *)((char *)p_dev->p_priv + offset);
        *p_value = *p_priv_field;
    } else {
        ccl_syslog_err("Error! Can't find howto read %s attr on device %s\n", name, p_dev->name);
        DEVMAN_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}

ccl_err_t devman_attr_array_set_double(IN devo_t devo, IN char *name, 
                                       IN b_u32 idx, IN b_u64 value)
{
    devman_device_t  *p_dev;
    device_attr_t    *p_attr;
    b_u64            *p_priv_field;             

    /* check input parameters */
    if ( !devo || !name ) {
        ccl_syslog_err("Error! Invalid input parameters\n");
        DEVMAN_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get device handle */
    p_dev = (devman_device_t *)devo;

    /* try to find the attribute */
    _rc = devman_attr_descr_find(devo, name, &p_attr);
    if ( _rc || !p_attr ) {
        ccl_syslog_err("Error! Can't find %s attr on device %s\n", name, p_dev->name);
        DEVMAN_ERROR_RETURN(CCL_FAIL);
    }
    /* do nothing if attribute is read-only */
    if ( p_attr->flags & eDEVMAN_ATTRFLAG_RO ) {
        ccl_syslog_err("Warning! Try to set read-only attribute %s attr on device %s\n", 
               name, p_dev->name);
        return 0;
    }
    /* check attribute type */
    if ( p_attr->type != eDEVMAN_ATTR_DOUBLE ) {
        ccl_syslog_err("Error! Can't set double for attr=%s, type=%d, idx=%d, on device %s\n", 
               name, p_attr->type, idx, p_dev->name);
        DEVMAN_ERROR_RETURN(CCL_FAIL);
    }
    /* check if there is the write accessor */
    if ( p_attr->offset_sz ) {
        _rc = _fdevman_attr_write(p_dev->name, p_dev->p_priv, p_attr->id, 
                                p_attr->offset, p_attr->offset_sz, 
                                idx, (b_u8 *)&value, sizeof(b_u64));
        if ( _rc ) {
            ccl_syslog_err("Error! Can't write by accessor %s attr on device %s\n", 
                   name, p_dev->name);
            DEVMAN_ERROR_RETURN(CCL_FAIL);
        }
    }
    /* if there isn't, try to write word by attr offset */
    else if ( p_dev->p_priv ) {
        int offset = p_attr->offset + ((p_attr->maxnum-1)? idx*sizeof(b_u64): 0);
        p_priv_field = (b_u64 *)((char *)p_dev->p_priv + offset);
        *p_priv_field = value;
    } else {
        ccl_syslog_err("Error! Can't find howto write attr=%s on device %s\n", 
               name, p_dev->name);
        DEVMAN_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}

ccl_err_t devman_attr_array_get_double(IN devo_t devo, IN char *name, 
                                       IN b_u32 idx, OUT b_u64 *p_value)
{
    devman_device_t  *p_dev;
    device_attr_t    *p_attr;
    b_u64            *p_priv_field;             

    /* check input parameters */
    if ( !devo || !name || !p_value ) {
        ccl_syslog_err("Error! Invalid input parameters\n");
        DEVMAN_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get device handle */
    p_dev = (devman_device_t *)devo;

    /* try to find the attribute */
    _rc = devman_attr_descr_find(devo, name, &p_attr);
    if ( _rc || !p_attr ) {
        ccl_syslog_err("Error! Can't find %s attr on device %s\n", name, p_dev->name);
        DEVMAN_ERROR_RETURN(CCL_FAIL);
    }
    /* check attribute type */
    if ( p_attr->type != eDEVMAN_ATTR_DOUBLE ) {
        ccl_syslog_err("Error! Can't get double for attr=%s, type=%d, idx=%d, on device %s\n", 
               name, p_attr->type, idx, p_dev->name);
        DEVMAN_ERROR_RETURN(CCL_FAIL);
    }
    /* check if there is the read accessor */
    if ( p_attr->offset_sz ) {
        _rc = _fdevman_attr_read(p_dev->name, p_dev->p_priv, p_attr->id, 
                               p_attr->offset, p_attr->offset_sz, 
                               idx, (b_u8 *)p_value, sizeof(b_u64));
        if ( _rc ) {
            ccl_syslog_err("Error! Can't read by accessor %s attr on device %s\n", 
                   name, p_dev->name);
            DEVMAN_ERROR_RETURN(CCL_FAIL);
        }
    }
    /* if there isn't, try to read word by attr offset */
    else if ( p_dev->p_priv ) {
        int offset = p_attr->offset + ((p_attr->maxnum-1)? idx*sizeof(b_u64): 0);
        p_priv_field = (b_u64 *)((char *)p_dev->p_priv + offset);
        *p_value = *p_priv_field;
    } else {
        ccl_syslog_err("Error! Can't find howto read %s attr on device %s\n", name, p_dev->name);
        DEVMAN_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}

ccl_err_t devman_attr_array_set_buff(IN devo_t devo, IN char *name, IN b_u32 idx, 
                                     IN b_u8 *p_value, IN b_u32 length)
{
    devman_device_t  *p_dev;
    device_attr_t    *p_attr;
    char             *p_priv_field;             

    /* check input parameters */
    if ( !devo || !name || !p_value || !length ) {
        ccl_syslog_err("Error! Invalid input parameters\n");
        DEVMAN_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get device handle */
    p_dev = (devman_device_t *)devo;

    /* try to find the attribute */
    _rc = devman_attr_descr_find(devo, name, &p_attr);
    if ( _rc || !p_attr ) {
        ccl_syslog_err("Error! Can't find %s attr on device %s\n", name, p_dev->name);
        DEVMAN_ERROR_RETURN(CCL_FAIL);
    }
    /* do nothing if attribute is read-only */
    if ( p_attr->flags & eDEVMAN_ATTRFLAG_RO ) {
        ccl_syslog_err("Warning! Try to set read-only attribute %s attr on device %s\n", 
               name, p_dev->name);
        return CCL_OK;
    }
    /* check attribute type */
    if ( p_attr->type != eDEVMAN_ATTR_BUFF && p_attr->type != eDEVMAN_ATTR_STRING ) {
        ccl_syslog_err("Error! Can't set buff for attr=%s, type=%d, idx=%d, on device %s\n", 
               name, p_attr->type, idx, p_dev->name);
        DEVMAN_ERROR_RETURN(CCL_FAIL);
    }
    /* don't overlap */
    if ( p_attr->size && length > p_attr->size )
        length = p_attr->size;
    /* check if there is the write accessor */
    if ( p_attr->offset_sz ) {
        _rc = _fdevman_attr_write(p_dev->name, p_dev->p_priv, p_attr->id, 
                                p_attr->offset, p_attr->offset_sz, 
                                idx, (b_u8 *)p_value, length);
        if ( _rc ) {
            ccl_syslog_err("Error! Can't write by accessor %s attr on device %s\n", 
                   name, p_dev->name);
            DEVMAN_ERROR_RETURN(CCL_FAIL);
        }
    }
    /* if there isn't, try to write buffer by attr offset */
    else if ( p_dev->p_priv ) {
        int offset = p_attr->offset + ((p_attr->maxnum-1)? idx*length: 0);
        p_priv_field = ((char *)p_dev->p_priv + offset);
        memcpy(p_priv_field, p_value, length);
    } else {
        ccl_syslog_err("Error! Can't find howto write attr=%s on device %s\n", 
               name, p_dev->name);
        DEVMAN_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}

ccl_err_t devman_attr_array_get_buff(IN devo_t devo, IN char *name, 
                                     IN b_u32 idx, OUT b_u8 *p_value, 
                                     IN b_u32 length)
{
    devman_device_t  *p_dev;
    device_attr_t    *p_attr;
    char             *p_priv_field;             

    /* check input parameters */
    if ( !devo || !name || !p_value || !length ) {
        ccl_syslog_err("Error! Invalid input parameters\n");
        DEVMAN_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get device handle */
    p_dev = (devman_device_t *)devo;

    /* try to find the attribute */
    _rc = devman_attr_descr_find(devo, name, &p_attr);
    if ( _rc || !p_attr ) {
        ccl_syslog_err("Error! Can't find %s attr on device %s\n", name, p_dev->name);
        DEVMAN_ERROR_RETURN(CCL_FAIL);
    }
    /* check attribute type */
    if ( p_attr->type != eDEVMAN_ATTR_BUFF && p_attr->type != eDEVMAN_ATTR_STRING ) {
        ccl_syslog_err("Error! Can't get buff for attr=%s, type=%d, idx=%d, on device %s\n", 
               name, p_attr->type, idx, p_dev->name);
        DEVMAN_ERROR_RETURN(CCL_FAIL);
    }
    /* don't overlap */
    if ( p_attr->size && length > p_attr->size )
        length = p_attr->size;
    /* check if there is the read accessor */
    if ( p_attr->offset_sz ) {
        _rc = _fdevman_attr_read(p_dev->name, p_dev->p_priv, p_attr->id, 
                               p_attr->offset, p_attr->offset_sz, 
                               idx, (b_u8 *)p_value, length);
        if ( _rc ) {
            ccl_syslog_err("Error! Can't read by accessor %s attr on device %s\n", 
                   name, p_dev->name);
            DEVMAN_ERROR_RETURN(CCL_FAIL);
        }
    }
    /* if there isn't, try to read buff by attr offset */
    else if ( p_dev->p_priv ) {
        int offset = p_attr->offset + ((p_attr->maxnum-1)? idx*length: 0);
        ccl_syslog_err("p_attr->offset: %d, new offset: %d, idx: %d, length: %d\n",
                       p_attr->offset, offset, idx, length);
        p_priv_field = ((char *)p_dev->p_priv + offset);
        memcpy(p_value, p_priv_field, length);
    } else {
        ccl_syslog_err("Error! Can't find howto read %s attr on device %s\n", name, p_dev->name);
        DEVMAN_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}

ccl_err_t devman_attr_array_set_buff_with_offset(IN devo_t devo, IN char *name, 
                                                 IN b_u32 idx, IN b_u8 *p_value, 
                                                 IN b_u32 offset, IN b_u32 offset_sz,
                                                 IN b_u32 length)
{
    devman_device_t  *p_dev;
    device_attr_t    *p_attr;
    char             *p_priv_field;             

    /* check input parameters */
    if ( !devo || !name || !p_value || !length ) {
        ccl_syslog_err("Error! Invalid input parameters\n");
        DEVMAN_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get device handle */
    p_dev = (devman_device_t *)devo;

    /* try to find the attribute */
    _rc = devman_attr_descr_find(devo, name, &p_attr);
    if ( _rc || !p_attr ) {
        ccl_syslog_err("Error! Can't find %s attr on device %s\n", name, p_dev->name);
        DEVMAN_ERROR_RETURN(CCL_FAIL);
    }
    p_attr->offset = offset;
    if (offset_sz) {
        p_attr->offset_sz = offset_sz;
    }
    /* do nothing if attribute is read-only */
    if ( p_attr->flags & eDEVMAN_ATTRFLAG_RO ) {
        ccl_syslog_err("Warning! Try to set read-only attribute %s attr on device %s\n", 
               name, p_dev->name);
        return 0;
    }
    /* check attribute type */
    if ( p_attr->type != eDEVMAN_ATTR_BUFF && p_attr->type != eDEVMAN_ATTR_STRING ) {
        ccl_syslog_err("Error! Can't set buff for attr=%s, type=%d, idx=%d, on device %s\n", 
               name, p_attr->type, idx, p_dev->name);
        DEVMAN_ERROR_RETURN(CCL_FAIL);
    }
    /* don't overlap */
    if ( p_attr->size && length > p_attr->size )
        length = p_attr->size;
    /* check if there is the write accessor */
    if ( p_attr->offset_sz ) {
        _rc = _fdevman_attr_write(p_dev->name, p_dev->p_priv, p_attr->id, 
                                p_attr->offset, p_attr->offset_sz, 
                                idx, (b_u8 *)p_value, length);
        if ( _rc ) {
            ccl_syslog_err("Error! Can't write by accessor %s attr on device %s\n", 
                   name, p_dev->name);
            DEVMAN_ERROR_RETURN(CCL_FAIL);
        }
    }
    /* if there isn't, try to write buffer by attr offset */
    else if ( p_dev->p_priv ) {
        int offset = p_attr->offset + ((p_attr->maxnum-1)? idx*length: 0);
        p_priv_field = ((char *)p_dev->p_priv + offset);
        memcpy(p_priv_field, p_value, length);
    } else {
        ccl_syslog_err("Error! Can't find howto write attr=%s on device %s\n", 
               name, p_dev->name);
        DEVMAN_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}

ccl_err_t devman_attr_array_get_buff_with_offset(IN devo_t devo, IN char *name, 
                                                 IN b_u32 idx, OUT b_u8 *p_value, 
                                                 IN b_u32 offset, IN b_u32 offset_sz,
                                                 IN b_u32 length)
{
    devman_device_t  *p_dev;
    device_attr_t    *p_attr;
    char             *p_priv_field;             

    /* check input parameters */
    if ( !devo || !name || !p_value || !length ) {
        ccl_syslog_err("Error! Invalid input parameters\n");
        DEVMAN_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get device handle */
    p_dev = (devman_device_t *)devo;

    /* try to find the attribute */
    _rc = devman_attr_descr_find(devo, name, &p_attr);
    if ( _rc || !p_attr ) {
        ccl_syslog_err("Error! Can't find %s attr on device %s\n", name, p_dev->name);
        DEVMAN_ERROR_RETURN(CCL_FAIL);
    }
    p_attr->offset = offset;
    if (offset_sz) {
        p_attr->offset_sz = offset_sz;
    }
    /* check attribute type */
    if ( p_attr->type != eDEVMAN_ATTR_BUFF && p_attr->type != eDEVMAN_ATTR_STRING ) {
        ccl_syslog_err("Error! Can't get buff for attr=%s, type=%d, idx=%d, on device %s\n", 
               name, p_attr->type, idx, p_dev->name);
        DEVMAN_ERROR_RETURN(CCL_FAIL);
    }
    /* don't overlap */
    if ( p_attr->size && length > p_attr->size )
        length = p_attr->size;
    /* check if there is the read accessor */
    if ( p_attr->offset_sz ) {
        _rc = _fdevman_attr_read(p_dev->name, p_dev->p_priv, p_attr->id, 
                               p_attr->offset, p_attr->offset_sz, 
                               idx, (b_u8 *)p_value, length);
        if ( _rc ) {
            ccl_syslog_err("Error! Can't read by accessor %s attr on device %s\n", 
                   name, p_dev->name);
            DEVMAN_ERROR_RETURN(CCL_FAIL);
        }
    }
    /* if there isn't, try to read buff by attr offset */
    else if ( p_dev->p_priv ) {
        int offset = p_attr->offset + ((p_attr->maxnum-1)? idx*length: 0);
        p_priv_field = ((char *)p_dev->p_priv + offset);
        memcpy(p_value, p_priv_field, length);
    } else {
        ccl_syslog_err("Error! Can't find howto read %s attr on device %s\n", name, p_dev->name);
        DEVMAN_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}

ccl_err_t devman_attr_array_set_cmd(IN devo_t devo, IN char *name, IN b_u32 idx)
{
    devman_device_t  *p_dev;
    device_attr_t    *p_attr;
    b_u8             *p_priv_field;             

    /* check input parameters */
    if ( !devo || !name ) {
        ccl_syslog_err("Error! Invalid input parameters\n");
        DEVMAN_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get device handle */
    p_dev = (devman_device_t *)devo;

    /* try to find the attribute */
    _rc = devman_attr_descr_find(devo, name, &p_attr);
    if ( _rc || !p_attr ) {
        ccl_syslog_err("Error! Can't find %s attr on device %s\n", 
               name, p_dev->name);
        DEVMAN_ERROR_RETURN(CCL_FAIL);
    }
    /* do nothing if attribute is read-only */
    if ( p_attr->flags & eDEVMAN_ATTRFLAG_RO ) {
        ccl_syslog_err("Warning! Try to set read-only attribute %s attr on device %s\n", 
               name, p_dev->name);
        return 0;
    }

    /* check attribute type */
    if ( p_attr->type != eDEVMAN_ATTR_CMD) {
        ccl_syslog_err("Error! Can't set byte for attr=%s, type=%d, idx=%d, on device %s\n", 
               name, p_attr->type, idx, p_dev->name);
        DEVMAN_ERROR_RETURN(CCL_FAIL);
    }
    /* check if there is the write accessor */
    if ( p_attr->offset_sz ) {
        _rc = _fdevman_attr_write(p_dev->name, p_dev->p_priv, p_attr->id, 
                                p_attr->offset, p_attr->offset_sz, 
                                idx, NULL, 0);
        if ( _rc ) {
            ccl_syslog_err("Error! Can't write by accessor %s attr on device %s\n", 
                   name, p_dev->name);
            DEVMAN_ERROR_RETURN(CCL_FAIL);
        }
    } else {
        ccl_syslog_err("Error! Can't find howto write attr=%s on device %s\n", 
               name, p_dev->name);
        DEVMAN_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

ccl_err_t devman_attr_array_set_string(IN devo_t devo, IN char *name, 
                                       IN b_u32 idx, IN b_u8 *p_value)
{
    devman_device_t  *p_dev;
    device_attr_t    *p_attr;
    b_u32            length;             

    /* check input parameters */
    if ( !devo || !name || !p_value ) {
        ccl_syslog_err("Error! Invalid input parameters\n");
        DEVMAN_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get device handle */
    p_dev = (devman_device_t *)devo;
    /* check if string */
    for ( length = 0; length < DEVMAN_ATTRS_VISIBLE_DEPTH; length++ )
        if ( !p_value[length] )
            break;
    if ( length >= DEVMAN_ATTRS_VISIBLE_DEPTH ) {
        ccl_syslog_err("Error! Invalid input parameters\n");
        DEVMAN_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* try to find the attribute */
    _rc = devman_attr_descr_find(devo, name, &p_attr);
    if ( _rc || !p_attr ) {
        ccl_syslog_err("Error! Can't find %s attr on device %s\n", name, p_dev->name);
        DEVMAN_ERROR_RETURN(CCL_FAIL);
    }
    /* do nothing if attribute is read-only */
    if ( p_attr->flags & eDEVMAN_ATTRFLAG_RO ) {
        ccl_syslog_err("Warning! Try to set read-only attribute %s attr on device %s\n", 
               name, p_dev->name);
        return 0;
    }
    /* check attribute type */
    if ( p_attr->type != eDEVMAN_ATTR_BUFF && p_attr->type != eDEVMAN_ATTR_STRING ) {
        ccl_syslog_err("Error! Can't get buff for attr=%s, type=%d, idx=%d, on device %s\n", 
               name, p_attr->type, idx, p_dev->name);
        DEVMAN_ERROR_RETURN(CCL_FAIL);
    }
    /* check length */
    if ( length > p_attr->size ) {
        ccl_syslog_err("Error! Can't set too long string %s on device %s\n", 
               p_value, p_dev->name);
        DEVMAN_ERROR_RETURN(CCL_FAIL);
    }

    return devman_attr_array_set_buff(devo, name, idx, p_value, length+1);       
}

ccl_err_t devman_attr_array_get_string(IN devo_t devo, IN char *name, 
                                       IN b_u32 idx, OUT b_u8 *p_value)
{   
    devman_device_t  *p_dev;
    device_attr_t    *p_attr;

    /* check input parameters */
    if ( !devo || !name || !p_value ) {
        ccl_syslog_err("Error! Invalid input parameters\n");
        DEVMAN_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get device handle */
    p_dev = (devman_device_t *)devo;    
    /* try to find the attribute */
    _rc = devman_attr_descr_find(devo, name, &p_attr);
    if ( _rc || !p_attr ) {
        ccl_syslog_err("Error! Can't find %s attr on device %s\n", name, p_dev->name);
        DEVMAN_ERROR_RETURN(CCL_FAIL);
    }
    /* check attribute type */
    if ( p_attr->type != eDEVMAN_ATTR_BUFF && p_attr->type != eDEVMAN_ATTR_STRING ) {
        ccl_syslog_err("Error! Can't get buff for attr=%s, type=%d, idx=%d, on device %s\n", 
               name, p_attr->type, idx, p_dev->name);
        DEVMAN_ERROR_RETURN(CCL_FAIL);
    }

    /* try to get the buffer */
    _rc = devman_attr_array_get_buff(devo, name, idx, p_value, p_attr->size);
    if ( _rc ) {
        ccl_syslog_err("Error! Can't get buffer\n");
        DEVMAN_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* cut off the string */
    p_value[p_attr->size] = 0;
    return CCL_OK;
}

ccl_err_t devman_attr_set_bool(IN devo_t devo, IN char *name, IN b_bool value)
{
    char aname[32];
    int  idx = 0;
    _rc = _fdevman_attr_parse_name_idx(name, aname, &idx);
    idx = (idx==DEVMAN_ATTRIDX_UNSPECIFIED)? 0: idx;
    return _rc? _rc: devman_attr_array_set_bool(devo, aname, idx, value);
}

ccl_err_t devman_attr_get_bool(IN devo_t devo, IN char *name, OUT b_bool *p_value)
{
    char aname[32];
    int  idx = 0;
    _rc = _fdevman_attr_parse_name_idx(name, aname, &idx);
    idx = (idx==DEVMAN_ATTRIDX_UNSPECIFIED)? 0: idx;
    return _rc? _rc: devman_attr_array_get_bool(devo, aname, idx, p_value);
}

ccl_err_t devman_attr_set_byte(IN devo_t devo, IN char *name, IN b_u8 value)
{
    char aname[32];
    int  idx = 0;
    _rc = _fdevman_attr_parse_name_idx(name, aname, &idx);
    idx = (idx==DEVMAN_ATTRIDX_UNSPECIFIED)? 0: idx;
    return _rc? _rc: devman_attr_array_set_byte(devo, aname, idx, value);
}

ccl_err_t devman_attr_get_byte(IN devo_t devo, IN char *name, OUT b_u8 *p_value)
{
    char aname[32];
    int  idx = 0;
    _rc = _fdevman_attr_parse_name_idx(name, aname, &idx);
    idx = (idx==DEVMAN_ATTRIDX_UNSPECIFIED)? 0: idx;
    return _rc? _rc: devman_attr_array_get_byte(devo, aname, idx, p_value);
}

ccl_err_t devman_attr_set_half(IN devo_t devo, IN char *name, IN b_u16 value)
{
    char aname[32];
    int  idx = 0;
    _rc = _fdevman_attr_parse_name_idx(name, aname, &idx);
    idx = (idx==DEVMAN_ATTRIDX_UNSPECIFIED)? 0: idx;
    return _rc? _rc: devman_attr_array_set_half(devo, aname, idx, value);
}

ccl_err_t devman_attr_get_half(IN devo_t devo, IN char *name, OUT b_u16 *p_value)
{
    char aname[32];
    int  idx = 0;
    _rc = _fdevman_attr_parse_name_idx(name, aname, &idx);
    idx = (idx==DEVMAN_ATTRIDX_UNSPECIFIED)? 0: idx;
    return _rc? _rc: devman_attr_array_get_half(devo, aname, idx, p_value);
}

ccl_err_t devman_attr_set_word(IN devo_t devo, IN char *name, IN b_u32 value)
{
    char aname[32];
    int  idx = 0;
    _rc = _fdevman_attr_parse_name_idx(name, aname, &idx);
    idx = (idx==DEVMAN_ATTRIDX_UNSPECIFIED)? 0: idx;
    return _rc? _rc: devman_attr_array_set_word(devo, aname, idx, value);
}

ccl_err_t devman_attr_get_word(IN devo_t devo, IN char *name, OUT b_u32 *p_value)
{
    char aname[32];
    int  idx = 0;
    _rc = _fdevman_attr_parse_name_idx(name, aname, &idx);
    idx = (idx==DEVMAN_ATTRIDX_UNSPECIFIED)? 0: idx;
    return _rc? _rc: devman_attr_array_get_word(devo, aname, idx, p_value);
}

ccl_err_t devman_attr_set_double(IN devo_t devo, IN char *name, IN b_u64 value)
{
    char aname[32];
    int  idx = 0;
    _rc = _fdevman_attr_parse_name_idx(name, aname, &idx);
    idx = (idx==DEVMAN_ATTRIDX_UNSPECIFIED)? 0: idx;
    return _rc? _rc: devman_attr_array_set_double(devo, aname, idx, value);
}

ccl_err_t devman_attr_get_double(IN devo_t devo, IN char *name, OUT b_u64 *p_value)
{
    char aname[32];
    int  idx = 0;
    _rc = _fdevman_attr_parse_name_idx(name, aname, &idx);
    idx = (idx==DEVMAN_ATTRIDX_UNSPECIFIED)? 0: idx;
    return _rc? _rc: devman_attr_array_get_double(devo, aname, idx, p_value);
}

ccl_err_t devman_attr_set_buff(IN devo_t devo, IN char *name, 
                               IN b_u8 *p_value, IN b_u32 length)
{
    char aname[32];
    int  idx = 0;
    _rc = _fdevman_attr_parse_name_idx(name, aname, &idx);
    idx = (idx==DEVMAN_ATTRIDX_UNSPECIFIED)? 0: idx;
    return _rc? _rc: devman_attr_array_set_buff(devo, aname, idx, p_value, length);
}

ccl_err_t devman_attr_get_buff(IN devo_t devo, IN char *name, 
                               OUT b_u8 *p_value, IN b_u32 length)
{
    char aname[32];
    int  idx = 0;
    _rc = _fdevman_attr_parse_name_idx(name, aname, &idx);
    idx = (idx==DEVMAN_ATTRIDX_UNSPECIFIED)? 0: idx;
    return _rc? _rc: devman_attr_array_get_buff(devo, aname, idx, p_value, length);
}

ccl_err_t devman_attr_set_string(IN devo_t devo, IN char *name, IN b_u8 *p_value)
{
    char aname[32];
    int  idx = 0;
    _rc = _fdevman_attr_parse_name_idx(name, aname, &idx);
    idx = (idx==DEVMAN_ATTRIDX_UNSPECIFIED)? 0: idx;
    return _rc? _rc: devman_attr_array_set_string(devo, aname, idx, p_value);
}

ccl_err_t devman_attr_get_string(IN devo_t devo, IN char *name, OUT b_u8 *p_value)
{
    char aname[32];
    int  idx = 0;
    _rc = _fdevman_attr_parse_name_idx(name, aname, &idx);
    idx = (idx==DEVMAN_ATTRIDX_UNSPECIFIED)? 0: idx;
    return _rc? _rc: devman_attr_array_get_string(devo, aname, idx, p_value);
}

ccl_err_t devman_attr_set(IN devo_t devo, 
                          IN device_attr_t *p_attr, 
                          IN b_u32 idx, IN b_u8 *p_value)
{
    /* check input parameters */
    if ( !devo || !p_attr || !p_value ) {
        ccl_syslog_err("Error! Invalid input parameters\n");
        DEVMAN_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* call appropriate helper */
    if ( p_attr->type == eDEVMAN_ATTR_BOOL )
        _rc = devman_attr_array_set_bool(devo, p_attr->name, idx, *(b_bool *)p_value);
    else if ( p_attr->type == eDEVMAN_ATTR_BYTE )
        _rc = devman_attr_array_set_byte(devo, p_attr->name, idx, *(b_u8 *)p_value);
    else if ( p_attr->type == eDEVMAN_ATTR_HALF )
        _rc = devman_attr_array_set_half(devo, p_attr->name, idx, *(b_u16 *)p_value);
    else if ( p_attr->type == eDEVMAN_ATTR_WORD )
        _rc = devman_attr_array_set_word(devo, p_attr->name, idx, *(b_u32 *)p_value);
    else if ( p_attr->type == eDEVMAN_ATTR_STRING )
        _rc = devman_attr_array_set_string(devo, p_attr->name, idx, p_value);
    else if ( p_attr->type == eDEVMAN_ATTR_CMD )
        _rc = devman_attr_array_set_cmd(devo, p_attr->name, idx);
    return _rc;
}

ccl_err_t devman_attr_get(IN devo_t devo, 
                          IN device_attr_t *p_attr, 
                          IN b_u32 idx, OUT b_u8 *p_value)
{
    /* check input parameters */
    if ( !devo || !p_attr || !p_value ) {
        ccl_syslog_err("Error! Invalid input parameters\n");
        DEVMAN_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* call appropriate helper */
    if ( p_attr->type == eDEVMAN_ATTR_BOOL )
        _rc = devman_attr_array_get_bool(devo, p_attr->name, idx, (b_bool *)p_value);
    else if ( p_attr->type == eDEVMAN_ATTR_BYTE )
        _rc = devman_attr_array_get_byte(devo, p_attr->name, idx, (b_u8 *)p_value);
    else if ( p_attr->type == eDEVMAN_ATTR_HALF )
        _rc = devman_attr_array_get_half(devo, p_attr->name, idx, (b_u16 *)p_value);
    else if ( p_attr->type == eDEVMAN_ATTR_WORD )
        _rc = devman_attr_array_get_word(devo, p_attr->name, idx, (b_u32 *)p_value);
    else if ( p_attr->type == eDEVMAN_ATTR_STRING )
        _rc = devman_attr_array_get_string(devo, p_attr->name, idx, p_value);
    return _rc;
}

ccl_err_t devman_attr_set_s(IN devo_t devo, 
                            IN device_attr_t *p_attr, 
                            IN b_u32 idx, IN char *string)
{
    char    *p_end;
    int      bs = 16;
    /* check input parameters */
    if ( !devo || !p_attr || !string ) {
        ccl_syslog_err("Error! Invalid input parameters\n");
        DEVMAN_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* call appropriate helper */
    if ( p_attr->format )
        bs = (strchr(p_attr->format,'x')? 16: 10);
    if ( p_attr->type == eDEVMAN_ATTR_BOOL ) {
        b_bool b = DEVMAN_BOOL_S_TO_NUM(string);
        _rc = devman_attr_array_set_bool(devo, p_attr->name, idx, b);
    } else if ( p_attr->type == eDEVMAN_ATTR_BYTE ) {
        b_u8 b;
        b = (b_u8)strtoul(string, &p_end, bs);
        _rc = devman_attr_array_set_byte(devo, p_attr->name, idx, b);
    } else if ( p_attr->type == eDEVMAN_ATTR_HALF ) {
        b_u16 h = (b_u16)strtoul(string, &p_end, bs);
        _rc = devman_attr_array_set_half(devo, p_attr->name, idx, h);
    } else if ( p_attr->type == eDEVMAN_ATTR_WORD ) {
        b_u32 w = (b_u32)strtoul(string, &p_end, bs);
        _rc = devman_attr_array_set_word(devo, p_attr->name, idx, w);
    } else if ( p_attr->type == eDEVMAN_ATTR_STRING ) {
        _rc = devman_attr_array_set_string(devo, p_attr->name, idx, (b_u8 *)string);
    } else if ( p_attr->type == eDEVMAN_ATTR_BUFF ) {
        char    buff[DEVMAN_ATTRS_VISIBLE_DEPTH];
        int     bcount = _fdevman_str_to_buff(buff, p_attr->size, string);
        _rc = !bcount? CCL_FAIL: devman_attr_array_set_buff(devo, p_attr->name, 
                                                            idx, (b_u8 *)buff, bcount);
    } else if (p_attr->type == eDEVMAN_ATTR_CMD) {
        _rc = devman_attr_array_set_cmd(devo, p_attr->name, idx);
    }
    return _rc;
}

ccl_err_t devman_attr_get_s(IN devo_t devo, 
                            IN device_attr_t *p_attr, 
                            IN b_u32 idx, IN size_t size, 
                            OUT char *string)
{
    /* check input parameters */
    if ( !devo || !p_attr || !string ) {
        ccl_syslog_err("Error! Invalid input parameters\n");
        DEVMAN_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* call appropriate helper */
    if ( p_attr->type == eDEVMAN_ATTR_BOOL ) {
        b_bool b;
        _rc = devman_attr_array_get_bool(devo, p_attr->name, idx, &b);
        _rc = _rc? _rc: 0& snprintf(string, size, "%s", DEVMAN_NUM_TO_BOOL_S(b));
    } else if ( p_attr->type == eDEVMAN_ATTR_BYTE ) {
        b_u8 b;
        _rc = devman_attr_array_get_byte(devo, p_attr->name, idx, &b);
        _rc = _rc? _rc: 0& snprintf(string, size, p_attr->format, b);
    } else if ( p_attr->type == eDEVMAN_ATTR_HALF ) {
        b_u16 h;
        _rc = devman_attr_array_get_half(devo, p_attr->name, idx, &h);
        _rc = _rc? _rc: 0& snprintf(string, size, p_attr->format, h);
    } else if ( p_attr->type == eDEVMAN_ATTR_WORD ) {
        b_u32 w;
        _rc = devman_attr_array_get_word(devo, p_attr->name, idx, &w);
        _rc = _rc? _rc: 0& snprintf(string, size, p_attr->format, w);
    } else if ( p_attr->type == eDEVMAN_ATTR_STRING ) {
        _rc = devman_attr_array_get_string(devo, p_attr->name, idx, (b_u8 *)string);
    }
    return _rc;
}

ccl_err_t devman_attr_cmp_s(IN devo_t devo, 
                            IN device_attr_t *p_attr, 
                            IN b_u32 idx, IN char *string, 
                            b_u32 *p_is_diff)
{
    char    *p_end;
    int      bs = 16;
    /* check input parameters */
    if ( !devo || !p_attr || !string || !p_is_diff ) {
        ccl_syslog_err("Error! Invalid input parameters\n");
        DEVMAN_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    *p_is_diff = 1;
    if ( p_attr->format )
        bs = (strchr(p_attr->format,'x')? 16: 10);
    /* call appropriate helper */
    if ( p_attr->type == eDEVMAN_ATTR_BOOL ) {
        b_bool attr_val, b = DEVMAN_BOOL_S_TO_NUM(string);
        _rc = devman_attr_array_get_bool(devo, p_attr->name, idx, &attr_val);
        *p_is_diff = (attr_val != b)? 1: 0;            
    } else if ( p_attr->type == eDEVMAN_ATTR_BYTE ) {
        b_u8 attr_val, b = (b_u8)strtoul(string, &p_end, bs);
        _rc = devman_attr_array_get_byte(devo, p_attr->name, idx, &attr_val);
        *p_is_diff = (attr_val != b)? 1: 0;            
    } else if ( p_attr->type == eDEVMAN_ATTR_HALF ) {
        b_u16 attr_val, h = (b_u16)strtoul(string, &p_end, bs);
        _rc = devman_attr_array_get_half(devo, p_attr->name, idx, &attr_val);
        *p_is_diff = (attr_val != h)? 1: 0;            
    } else if ( p_attr->type == eDEVMAN_ATTR_WORD ) {
        b_u32 attr_val, w = (b_u32)strtoul(string, &p_end, bs);
        _rc = devman_attr_array_get_word(devo, p_attr->name, idx, &attr_val);
        *p_is_diff = (attr_val != w)? 1: 0;            
    } else if ( p_attr->type == eDEVMAN_ATTR_STRING ) {
        char attr_val[DEVMAN_ATTRS_SIZE_MAX];
        memset(attr_val, 0, sizeof(attr_val));
        _rc = devman_attr_array_get_string(devo, p_attr->name, idx, (b_u8 *)attr_val);
        *p_is_diff = strncmp(attr_val, string, DEVMAN_ATTRS_SIZE_MAX-1);
    } else if ( p_attr->type == eDEVMAN_ATTR_BUFF ) {
        char    attr_val[DEVMAN_ATTRS_VISIBLE_DEPTH], buff[DEVMAN_ATTRS_VISIBLE_DEPTH];
        int     bcount = _fdevman_str_to_buff(buff, p_attr->size, string);
        _rc = !bcount? CCL_FAIL: devman_attr_array_get_buff(devo, p_attr->name, 
                                                            idx, (b_u8 *)attr_val, bcount);
        *p_is_diff = memcmp(attr_val, buff, bcount);
    }
    return _rc;
}

ccl_err_t devman_device_cmd_exec(devo_t devo, char *cmd_name, 
                                 char *parm_vals_str[], b_u32 parms_num)
{    
    devman_device_t     *p_dev = NULL;
    device_driver_t     *p_drv;
    device_cmd_t        *p_cmd;
    char                *p_end; 
    char                 buff[DEVMAN_ATTRS_VISIBLE_DEPTH];
    int                  bs = 0;
    b_u32                i;

    /* check input parameters */
    if ( !devo || !cmd_name || (parms_num && !parm_vals_str) ) {
        ccl_syslog_err("Error! Invalid input parameters\n");
        DEVMAN_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get device/driver handle */
    p_dev = (devman_device_t *)devo;
    p_drv = p_dev->p_drv;

    if ( !p_drv->cmds_num ) {
        ccl_syslog_err("Error! This device hasn't any defined commands\n");
        DEVMAN_ERROR_RETURN(CCL_NOT_DEFINED);
    }

    PDEBUGG("123$=> dev_type=%s, cmd_name=%s, parms_num=%d\n", 
            p_drv->name, cmd_name, parms_num);

    /* find cmd by name */
    for ( i = 0; i < p_drv->cmds_num; i++ ) {
        if ( !strncmp(p_drv->cmds[i].name, cmd_name, 
                      DEVMAN_DEVICE_CMD_NAME_LEN-1) ) {
            p_cmd = &p_drv->cmds[i];
            PDEBUGG("123$=> found cmd=%s, dev=%s\n",p_drv->cmds[i].name, p_drv->name);
            break;
        }
    }
    /* check if cmd is found */
    if ( i >= p_drv->cmds_num ) {
        ccl_syslog_err("Error! Command %s not found\n", cmd_name);
        DEVMAN_ERROR_RETURN(CCL_NOT_DEFINED);
    }
    /* check parameters number */
    {
        int optionals = 0;
        for ( i = 0; i < p_cmd->parms_num; i++ ) {
            if ( p_cmd->parms[i].flags & eDEVMAN_ATTRFLAG_OPTIONAL )
                optionals++;
        }
        if ( (parms_num > p_cmd->parms_num) || 
             (parms_num < (p_cmd->parms_num - optionals)) ) {
            ccl_syslog_err("Error! Command %s has %d parms(%d optionals), not %d\n", 
                   cmd_name, p_cmd->parms_num, optionals, parms_num);
            DEVMAN_ERROR_RETURN(CCL_NOT_DEFINED);
        }
    }

    /* prepare cmd parameters */
    for ( i = 0; i < parms_num; i++ ) {
        if ( p_cmd->parms[i].format )
            bs = (strchr(p_cmd->parms[i].format,'x')? 16: 10);

        if ( p_cmd->parms[i].type == eDEVMAN_ATTR_BOOL ) {
            p_cmd->parms[i].value = DEVMAN_BOOL_S_TO_NUM(parm_vals_str[i]);
        } else if ( p_cmd->parms[i].type == eDEVMAN_ATTR_BYTE ) {
            p_cmd->parms[i].value = (b_u8)strtoul(parm_vals_str[i], &p_end, bs);
        } else if ( p_cmd->parms[i].type == eDEVMAN_ATTR_HALF ) {
            p_cmd->parms[i].value = (b_u16)strtoul(parm_vals_str[i], &p_end, bs);
        } else if ( p_cmd->parms[i].type == eDEVMAN_ATTR_WORD ) {
            p_cmd->parms[i].value = (b_u32)strtoul(parm_vals_str[i], &p_end, bs);
        } else if ( p_cmd->parms[i].type == eDEVMAN_ATTR_STRING ) {
            p_cmd->parms[i].value = (uintptr_t)parm_vals_str[i];
            strncpy(p_cmd->parms[i].buf, parm_vals_str[i], DEVMAN_CMD_PARM_LEN);
        } else if ( p_cmd->parms[i].type == eDEVMAN_ATTR_BUFF ) {
            int  bcount = _fdevman_str_to_buff(buff, p_cmd->parms[i].size, parm_vals_str[i]);
            p_cmd->parms[i].value = (uintptr_t)buff;
            strncpy(p_cmd->parms[i].buf, parm_vals_str[i], DEVMAN_CMD_PARM_LEN);
            p_cmd->parms[i].rsize = bcount;
            PDEBUG("size=%d, value=%s, buf=%p, bcount=%d\n", 
                   p_cmd->parms[i].size, parm_vals_str[i], (uintptr_t)buff, bcount);        
        }
        PDEBUGG("123$=> parm_vals_str[%d]=%s, value[%d]=%d\n", 
                i, parm_vals_str[i], i, p_cmd->parms[i].value);        
    }

    /* and launch the command target */    
    if ( p_cmd->f_cmd )
        return p_cmd->f_cmd(devo, p_cmd->parms, parms_num);

    return CCL_OK;
}

/*** Devices types/instances maintanance APIs */

ccl_err_t devman_driver_register(device_driver_t *p_drv)
{
    int  i;

    /* check input parameters */
    if ( !p_drv ) {
        ccl_syslog_err("Error! Invalid input parameters\n");
        DEVMAN_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* look for first free place in the database  */
    for ( i = 0; i < DEVMAN_DRIVERS_MAXNUM; i++ ) {
        if ( !_drivers[i].name[0] ) {
            memcpy(&_drivers[i], p_drv, sizeof(device_driver_t));
            _registered_drivers++;
            break;
        }
    }

    return CCL_OK;
}

ccl_err_t devman_driver_unregister(device_driver_t *p_drv)
{
    devman_device_t     *p_dev;
    int                  i;

    /* check input parameters */
    if ( !p_drv ) {
        ccl_syslog_err("Error! Invalid input parameters\n");
        DEVMAN_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* remove the appropriate device instances */
    for ( i=0; i < DEVMAN_DEVICES_MAXNUM; i++ ) {
        if ( !strncmp(p_drv->name, _devices[i].name, DEVMAN_DEVNAME_LEN-1) ) {
            p_dev = &_devices[i];
            _rc = devman_device_delete(p_dev);
            if ( _rc ) {
                ccl_syslog_err("Warning! Can't delete device %s, sig = 0x%08x\n", 
                       p_dev->name, p_dev->sig);
            }
        }
    }         
    /* look for the driver in the database, cleanup the place  */
    for ( i=0; i < DEVMAN_DRIVERS_MAXNUM; i++ ) {
        if ( !strncmp(&_drivers[i].name[0], p_drv->name, DEVMAN_DEVNAME_LEN-1) ) {
            memset(&_drivers[i], 0, sizeof(device_driver_t));
            /*123$; database processing */
            _registered_drivers--;
            break;
        }
    }

    return CCL_OK;
}

ccl_err_t devman_device_type(char *name, device_driver_t **pp_drv)
{
    int    i;

    /* check input parameters */
    if ( !name || !pp_drv ) {
        ccl_syslog_err("Error! Invalid input parameters: name=0x%x, pp_drv=0x%x\n", 
               name, pp_drv);
        DEVMAN_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* look for the registered device driver in the database  */
    *pp_drv = 0;
    for ( i = 0; i < _registered_drivers; i++ ) {
        if ( !strncmp(_drivers[i].name, name, DEVMAN_DEVNAME_LEN-1) ) {
            *pp_drv = &_drivers[i];
            break;
        }
    }
    return CCL_OK;
}

ccl_err_t devman_device_create(char *type_name, b_u32 sig, 
                               void *p_brdspec, devo_t *p_devo)
{
    devman_device_t     *p_dev = NULL;
    device_driver_t     *p_drv = 0;
    int                  i;

    /* check input parameters */
    if ( !type_name || !type_name[0] || !p_brdspec || !p_devo ) {
        ccl_syslog_err("Error! Invalid input parameters\n");
        DEVMAN_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* look for first free place in the database  */
    for ( i = 0; i < DEVMAN_DEVICES_MAXNUM; i++ ) {
        if ( !_devices[i].name[0] ) {
            p_dev = &_devices[i];
            break;
        }
    }
    if (!p_dev) {
        ccl_syslog_err("Error! Can't find free entry in database for device %s\n", 
               type_name);
        DEVMAN_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* try to bind the appropriate driver to the created device */
    for ( i=0; i < DEVMAN_DRIVERS_MAXNUM; i++ ) {
        if ( !strncmp(type_name, _drivers[i].name, DEVMAN_DEVNAME_LEN-1) ) {
            p_drv = &_drivers[i];
            break;
        }
    }
    if ( !p_drv  ) {
        ccl_syslog_err("Error! Can't find driver for device %s\n", type_name);
        DEVMAN_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* allocate and clean the device private context */
    p_dev->p_priv = malloc(p_drv->priv_size);
    if ( !p_dev->p_priv ) {
        ccl_syslog_err("Error! Can't allocate memory for the device %s private info, size=%d bytes\n", 
               type_name, p_drv->priv_size);
        DEVMAN_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    memset(p_dev->p_priv, 0, p_drv->priv_size);

    /* initialize the device specific stuff */
    if ( p_drv->f_add ) {
        _rc = p_drv->f_add((void *)p_dev, p_brdspec);
        if ( _rc ) {
            ccl_syslog_err("Error! Can't create device instance\n");
            free(p_dev->p_priv);
            p_dev->p_priv = 0;
            DEVMAN_ERROR_RETURN(CCL_BAD_PARAM_ERR);
        }
    }

    /* copy the name/signature */
    strncpy(p_dev->name, type_name, DEVMAN_DEVNAME_LEN);
    p_dev->sig = sig;
    p_dev->p_drv = p_drv;

    /*123$; database processing*/
    _created_devices++;
    p_drv->instances++;

    /* the device object handle is returned here */
    *p_devo = p_dev;   

    return CCL_OK;
}

ccl_err_t devman_device_delete(devo_t devo)
{
    devman_device_t  *p_dev;
    device_driver_t  *p_drv;

    /* check input parameters */
    if ( !devo ) {
        ccl_syslog_err("Error! Invalid input parameters: devo=0x%x\n", devo);
        DEVMAN_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get the device context */
    p_dev = (devman_device_t *)devo;

    /* get the driver handle */
    p_drv = p_dev->p_drv;
    if ( !p_drv  ) {
        ccl_syslog_err("Error! Can't find driver for device %s\n", p_dev->name);
        DEVMAN_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* deinitialize the device specific stuff */
    if ( p_drv->f_cut ) {
        _rc = p_drv->f_cut(p_dev);
        if ( _rc ) {
            ccl_syslog_err("Error! Can't delete device instance\n");
        }
    }
    /* release the private memory */
    free(p_dev->p_priv);

    /*123$; database processing*/
    memset(p_dev, 0, sizeof(devman_device_t));
    _created_devices--;
    p_drv->instances--;

    return CCL_OK;
}

ccl_err_t devman_device_object(char *name, devo_t *p_devo)
{
    //devman_device_t  *p_dev;
    char              type[20] = {0}, discr[20] = {0}, *p_end;
    b_u32             minor, i;

    /* check input parameters */
    if ( !name || !p_devo || !strnlen(name, DEVMAN_DEVNAME_LEN) ) {
        ccl_syslog_err("Error! Invalid input parameters: name=0x%x, p_devo=0x%x\n", 
               name, p_devo);
        DEVMAN_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    PDEBUG("123$=> strnlen(name)=%d\n", strnlen(name, DEVMAN_DEVNAME_LEN));

    /* separate the device type and discriminator if any */
    for ( i = 0; i < strnlen(name, DEVMAN_DEVNAME_LEN); i++ ) {
        if ( name[i] != '/' )
            type[i] = name[i];
        else {
            memcpy(discr, &name[i+1], (strnlen(name, DEVMAN_DEVNAME_LEN) - i));
            break;
        }
    }
    type[i] = 0;
    PDEBUG("123$=> type=%s,discr=%s\n", type, discr);

    minor = discr[0]? strtol(discr, &p_end, 10): 0;

    PDEBUG("123$=> type=%s,discr=%s,minor=%d\n", type, discr, minor);

    /* look for the device in the database  */
    *p_devo = 0;
    for ( i = 0; i < DEVMAN_DEVICES_MAXNUM; i++ ) {
        if ( !strncmp(_devices[i].name, type, DEVMAN_DEVNAME_LEN-1) && 
             (_devices[i].sig & BRDDEV_MINOR_MASK) == minor ) {
            *p_devo = &_devices[i];
            break;
        }
    }
    return CCL_OK;
}

ccl_err_t devman_device_object_identify(char *type, int discr, devo_t *p_devo)
{
    //devman_device_t  *p_dev;
    int              i;

    /* check input parameters */
    if ( !type || !p_devo || !strnlen(type, DEVMAN_DEVNAME_LEN) ) {
        ccl_syslog_err("Error! Invalid input parameters: name=0x%x, p_devo=0x%x\n", 
               type, p_devo);
        DEVMAN_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* look for the device in the database  */
    *p_devo = 0;
    for ( i = 0; i < DEVMAN_DEVICES_MAXNUM; i++ ) {
        if ( !strncmp(_devices[i].name, type, DEVMAN_DEVNAME_LEN-1) && 
             (_devices[i].sig & BRDDEV_MINOR_MASK) == (b_u32)discr ) {
            *p_devo = &_devices[i];
            break;
        }
    }
    return CCL_OK;
}

void *devman_device_private(devo_t devo)
{
    devman_device_t  *p_dev;
    if ( !devo ) {
        ccl_syslog_err("Error! Invalid input parameters: devo=0x%x\n", devo);
        DEVMAN_ERROR_RETURN(NULL);
    }
    /* get the device context */
    p_dev = (devman_device_t *)devo;
    return p_dev->p_priv;
}

void devman_type_help(char *name)
{
    device_driver_t    *p_drv=0;
    int                 hid=0, ro=0, i;

    for ( i=0; i < _registered_drivers; i++ ) {
        if ( !strncmp(_drivers[i].name, name, DEVMAN_DEVNAME_LEN-1) )
            p_drv = &_drivers[i];
    }
    if ( !p_drv ) {
        ccl_syslog_err("There is no registered type: %s\n", name);
        return;
    }
    if ( _fdevman_type_irregular_attrs_num(p_drv, &hid, &ro) ) {
        ccl_syslog_err("Can't get type %s irregular attrs number\n", name);
        return;
    }
    PRINTL("-------------------------------------------\n");
    PRINTL("type_name = %s\n", p_drv->name);
    PRINTL("instances = %d\n", p_drv->instances);
    PRINTL("attrs_num = %d(hidden=%d,readonly=%d)\n", p_drv->attrs_num, hid, ro);
    PRINTL("-------------------------------------------\n");
    for ( i=0; i < (int)p_drv->attrs_num; i++  ) {
        _fdevman_attr_help(&p_drv->attrs[i]);
    }

    PRINTL("\n");        
}

void devman_type_devices_show(char *type_name)
{
    int i, show_all = 0;
    if ( !type_name )
        return;
    PRINTL("========= instances of %s device ==========\n", type_name);
    for ( i=0; i < _created_devices; i++ ) {
        if ( !strncmp(_devices[i].name, type_name, DEVMAN_DEVNAME_LEN-1)  )
            _fdevman_device_show(&_devices[i], show_all);
    }         
}

void devman_types_show(void)
{
    int i;
    PRINTL("=========== registered types=%d ===============\n",
           _registered_drivers);
    for ( i=0; i < _registered_drivers/*123$*/; i++ ) {
        PRINTL("type_name = %s\n", _drivers[i].name);
        PRINTL("instances = %d\n", _drivers[i].instances);
        PRINTL("attrs_num = %d, commands = %d\n", 
               _drivers[i].attrs_num, _drivers[i].cmds_num);
        PRINTL("-------------------------------------------\n");
    } 
    PRINTL("\n");        
}

void devman_devices_show_all(void)
{
    int i, show_all = 0;
    devman_types_show();
    PRINTL("=========== created instances=%d ===============\n",
           _created_devices);
    for ( i=0; i < _created_devices/*123$*/; i++ ) {
        _fdevman_device_show(&_devices[i], show_all);
    }         
}

ccl_err_t devman_command_handle(devman_cmd_t *p_devman_cmd)
{
    char               type[20];
    int                discr;
    char               aname[DEVMAN_ATTRS_NAME_LEN+1];
    int                idx;
    int                i = 0;
    devman_device_t   *p_dev;
    device_driver_t   *p_drv;
    device_attr_t     *p_attr;

    /* chipas */
    if ( !p_devman_cmd ) {
        ccl_syslog_err("Error! Invalid input parameters\n");
        DEVMAN_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    if ( p_devman_cmd->type == eDEVMAN_CMD_SHOW ) {
        /* check if there is the parameters list */
        if ( !p_devman_cmd->parms_num ) {
            DEVMAN_ERROR_RETURN(CCL_NOT_DEFINED);
        }

        /* next argument: 'types', 'all' or device name/discriminator */
        if ( !strcasecmp(p_devman_cmd->parm[0], "types") ) {
            devman_types_show();
            return CCL_OK;
        }

        if ( !strcasecmp(p_devman_cmd->parm[0], "all") ) {
            devman_devices_show_all();
            return CCL_OK;
        }

        _rc = _fdevman_device_parse_type_discr(p_devman_cmd->parm[0], type, &discr);
        if ( _rc ) {
            ccl_syslog_err("Error! Can't get device type, discr for the %s\n", 
                   p_devman_cmd->parm[0]);
            DEVMAN_ERROR_RETURN(CCL_FAIL);
        }
        _rc = devman_device_type(type, &p_drv);
        if ( _rc || !p_drv ) {
            ccl_syslog_err("Error! Can't identify %s type\n", type);
            DEVMAN_ERROR_RETURN(CCL_FAIL);
        }
        if ( discr == DEVMAN_DISCRIMINATOR_UNSPECIFIED && 
             p_devman_cmd->parms_num <= 1) {
                devman_type_devices_show(type);
                return CCL_OK;
        }

        device_iterator:
        if ( discr == DEVMAN_DISCRIMINATOR_UNSPECIFIED) {
            if (i == DEVMAN_DEVICES_MAXNUM) 
                return CCL_OK;
                _rc = devman_device_object_identify(type, i++, (devo_t *)&p_dev);
                if ( !p_dev ) 
                    goto device_iterator;
        } else {
            _rc = devman_device_object_identify(type, discr, (devo_t *)&p_dev);
            if ( _rc || !p_dev ) {
                ccl_syslog_err("Error! Can't identify %s name, %d discr object\n", 
                       p_devman_cmd->parm[0], discr);
                DEVMAN_ERROR_RETURN(CCL_FAIL);
            }
        }

        /* show all device if there isn't specified attribute */
        if ( p_devman_cmd->parms_num <= 1 ) {
            /* show the device, don't display hidden */            
            _fdevman_device_show(p_dev, 0);
            return CCL_OK;
        } else if ( !strcasecmp(p_devman_cmd->parm[1], "all") ) {
            /* show the device, display all */            
            _fdevman_device_show(p_dev, 1);
            return CCL_OK;
        }
        /* next argument: attribute name[index]; try to identify the attribute */
        _rc = _fdevman_attr_parse_name_idx(p_devman_cmd->parm[1], aname, &idx);
        if ( _rc ) {
            ccl_syslog_err("Error! Can't parse to name/idx attr %s\n", 
                   p_devman_cmd->parm[1]);
            DEVMAN_ERROR_RETURN(CCL_FAIL);
        }
        p_attr = 0;
        do {
            b_u32 print_elems_num;
            _rc = _fdevman_attr_descr_find_next(p_drv, aname, &p_attr);
            if ( _rc || !p_attr ) {
                ccl_syslog_err("Error! Couldn't find attribute consisting %s\n", aname);
                break;
            }
            /* show all array elements if index isn't specified */
            if ( idx == DEVMAN_ATTRIDX_UNSPECIFIED ) {
                print_elems_num = p_attr->maxnum;
                /* force attr array show max number limitation */
                if ( print_elems_num > DEVMAN_ATTRS_PRINT_ELEMS_ASHOW )
                    print_elems_num = DEVMAN_ATTRS_PRINT_ELEMS_ASHOW;
                _fdevman_attr_show(p_dev, p_attr, 0, print_elems_num);
                if ( print_elems_num != p_attr->maxnum ) {
                    ccl_syslog_err("...\nTruncate the output; the attributes array %s[] contains of %d elements\n" 
                           "Use indexing, for example: \"%s[%d]\" \n"
                           "                       or: \"%s[%d] <elements_num>\"\n",
                           p_attr->name, p_attr->maxnum, 
                           p_attr->name, p_attr->maxnum/3,
                           p_attr->name, p_attr->maxnum/2+1);
                }
                if ( discr == DEVMAN_DISCRIMINATOR_UNSPECIFIED)
                    goto device_iterator;
                else 
                    break;
            } else {
                print_elems_num = 1;
                if ( p_devman_cmd->parms_num > 2 ) {
                    char    *p_end;
                    int      bs = (strchr(p_devman_cmd->parm[3],'x')? 16: 10);
                    int      val;                        
                    print_elems_num = DEVMAN_ATTRS_PRINT_ELEMS_ASHOW;
                    val = strtoul(p_devman_cmd->parm[2], &p_end, bs);
                    if ( val > 0 )
                        print_elems_num = ((b_u32)(idx + val) <= p_attr->maxnum) ? 
                                          (b_u32)val : (p_attr->maxnum - idx);
                }
                _fdevman_attr_show(p_dev, p_attr, idx, print_elems_num);
                if ( discr == DEVMAN_DISCRIMINATOR_UNSPECIFIED)
                    goto device_iterator;
                else 
                    break;
            }
        }
        while ( p_attr );
    } else if ( p_devman_cmd->type == eDEVMAN_CMD_CONF ) {
        /* check if there is the necessary parameters list */
        if ( p_devman_cmd->parms_num < 3 ) {
            ccl_syslog_err("Error! p_devman_cmd->parms_num=%d\n", 
                   p_devman_cmd->parms_num);
            DEVMAN_ERROR_RETURN(CCL_NOT_DEFINED);
        }

        /* next argument: device type/discriminator; let's
           identify the device instance by type/discriminator */
        _rc = _fdevman_device_parse_type_discr(p_devman_cmd->parm[0], type, &discr);
        if ( _rc ) {
            ccl_syslog_err("Error! Can't get device type, discr for the %s\n", 
                   p_devman_cmd->parm[0]);
            DEVMAN_ERROR_RETURN(CCL_FAIL);
        }
        if ( discr == DEVMAN_DISCRIMINATOR_UNSPECIFIED ) {
            discr = 0;
            /* get the device instance handle */
            _rc = devman_device_object_identify(type, discr, (devo_t *)&p_dev);
            if ( _rc || !p_dev ) {
                ccl_syslog_err("Error! Can't identify %s type, %d discr object\n", type, discr);
                DEVMAN_ERROR_RETURN(CCL_FAIL);
            }
            /* we must exactly know what device we're configuring now */
            if ( !p_dev->p_drv || p_dev->p_drv->instances > 1 )
                DEVMAN_ERROR_RETURN(CCL_FAIL);
        } else {
            /* get the device instance handle */
            _rc = devman_device_object_identify(type, discr, (devo_t *)&p_dev);
            if ( _rc || !p_dev ) {
                ccl_syslog_err("Error! Can't identify %s type, %d discr object\n", type, discr);
                DEVMAN_ERROR_RETURN(CCL_FAIL);
            }
        }

        /* next argument: attribute name[index]; try to identify the attribute */    
        _rc = _fdevman_attr_parse_name_idx(p_devman_cmd->parm[1], aname, &idx);
        if ( _rc ) {
            ccl_syslog_err("Error! Can't parse to name/idx attr %s\n", p_devman_cmd->parm[1]);
            DEVMAN_ERROR_RETURN(CCL_FAIL);
        }
        /* get attribute descriptor */
        _rc = devman_attr_descr_find((devo_t *)p_dev, aname, &p_attr);
        if ( _rc || !p_attr ) {
            ccl_syslog_err("Error! Can't find attr %s on device %s/%d\n", 
                   aname, p_dev->name, p_dev->sig & BRDDEV_MINOR_MASK);
            DEVMAN_ERROR_RETURN(CCL_FAIL);
        }

        /* next argument: attribute value; check if index is specified for array element */
        if ( idx == DEVMAN_ATTRIDX_UNSPECIFIED ) {
            /* set target value for all attribute array elements */
            for ( idx = 0; idx < (int)p_attr->maxnum; idx++ ) {
                _rc = devman_attr_set_s((devo_t *)p_dev, p_attr, idx, p_devman_cmd->parm[2]);
                if ( _rc ) {
                    ccl_syslog_err("Error! Can't set attr %s, value %s, device %s/%d\n", 
                           p_attr->name, p_devman_cmd->parm[2], p_dev->name, 
                           p_dev->sig & BRDDEV_MINOR_MASK);
                    DEVMAN_ERROR_RETURN(CCL_FAIL);
                }
                /* lets the attribute to be set */
                usleep(20*1000); 

                /* read back, force attr array show max number limitation */                      
                if ( idx < DEVMAN_ATTRS_PRINT_ELEMS_ASHOW )
                    _fdevman_attr_show(p_dev, p_attr, idx, 1);
                else if (idx == DEVMAN_ATTRS_PRINT_ELEMS_ASHOW ) {
                    ccl_syslog_err("...\nTruncate the output; the attributes array %s[] contains of %d elements\n" 
                           "Use indexing, for example: \"%s[%d]\" \n"
                           "                       or: \"%s[%d] <elements_num>\"\n",
                           p_attr->name, p_attr->maxnum, 
                           p_attr->name, p_attr->maxnum/3,
                           p_attr->name, p_attr->maxnum/2+1);
                }
            }
        } else {
            idx = (p_attr->maxnum == 1) ? 0: idx;
            /* set attribute using the input string */
            _rc = devman_attr_set_s((devo_t *)p_dev, p_attr, idx, p_devman_cmd->parm[2]);
            if ( _rc ) {
                ccl_syslog_err("Error! Can't set attr %s, value %s, device %s/%d\n", 
                       p_attr->name, p_devman_cmd->parm[2], p_dev->name, 
                       p_dev->sig & BRDDEV_MINOR_MASK);
                DEVMAN_ERROR_RETURN(CCL_FAIL);
            }
            /* lets the attribute to be set */
            usleep(20*1000);                       
            /* read back */                      
            _fdevman_attr_show(p_dev, p_attr, idx, 1);
        }        
    } else if ( p_devman_cmd->type == eDEVMAN_CMD_TEST ) {
        int is_diff;    
        /* check if there is the necessary parameters list */
        if ( p_devman_cmd->parms_num < 3 ) {
            ccl_syslog_err("Error! p_devman_cmd->parms_num=%d\n", 
                   p_devman_cmd->parms_num);
            DEVMAN_ERROR_RETURN(CCL_NOT_DEFINED);
        }

        /* next argument: device type/discriminator; let's identify
           the device instance by type/discriminator */
        _rc = _fdevman_device_parse_type_discr(p_devman_cmd->parm[0], type, &discr);
        if ( _rc ) {
            ccl_syslog_err("Error! Can't get device type, discr for the %s\n", 
                   p_devman_cmd->parm[0]);
            return CCL_FAIL;
        }
        if ( discr == DEVMAN_DISCRIMINATOR_UNSPECIFIED ) {
            discr = 0;
            /* get the device instance handle */
            _rc = devman_device_object_identify(type, discr, (devo_t *)&p_dev);
            if ( _rc || !p_dev ) {
                ccl_syslog_err("Error! Can't identify %s type, %d discr object\n", type, discr);
                DEVMAN_ERROR_RETURN(CCL_NOT_DEFINED);
            }
            /* we must exactly know what device we're configuring now */
            if ( !p_dev->p_drv || p_dev->p_drv->instances > 1 )
                DEVMAN_ERROR_RETURN(CCL_FAIL);
        } else {
            /* get the device instance handle */
            _rc = devman_device_object_identify(type, discr, (devo_t *)&p_dev);
            if ( _rc || !p_dev ) {
                ccl_syslog_err("Error! Can't identify %s type, %d discr object\n", type, discr);
                DEVMAN_ERROR_RETURN(CCL_FAIL);
            }
        }

        /* next argument: attribute name[index]; try to identify the attribute */    
        _rc = _fdevman_attr_parse_name_idx(p_devman_cmd->parm[1], aname, &idx);
        if ( _rc ) {
            ccl_syslog_err("Error! Can't parse to name/idx attr %s\n", p_devman_cmd->parm[1]);
            DEVMAN_ERROR_RETURN(CCL_FAIL);
        }
        /* get attribute descriptor */
        _rc = devman_attr_descr_find((devo_t *)p_dev, aname, &p_attr);
        if ( _rc || !p_attr ) {
            ccl_syslog_err("Error! Can't find attr %s on device %s/%d\n", 
                   aname, p_dev->name, p_dev->sig & BRDDEV_MINOR_MASK);
            DEVMAN_ERROR_RETURN(CCL_FAIL);
        }

        /* next argument: attribute value; check if index is specified for array element */
        if ( idx == DEVMAN_ATTRIDX_UNSPECIFIED ) {
            /* compare given value to all attribute array elements */
            for ( idx = 0; idx < (int)p_attr->maxnum; idx++ ) {
                _rc = devman_attr_cmp_s((devo_t *)p_dev, p_attr, idx, 
                                        p_devman_cmd->parm[2], (b_u32 *)&is_diff);
                if ( _rc ) {
                    ccl_syslog_err("Error! Can't compare attr %s to value %s, device %s/%d\n", 
                           p_attr->name, p_devman_cmd->parm[2], 
                           p_dev->name, p_dev->sig & BRDDEV_MINOR_MASK);
                    DEVMAN_ERROR_RETURN(CCL_FAIL);
                }
                if ( is_diff ) {
                    ccl_syslog_err("\nERROR! The difference is observed, attribute actual value:\n");
                    _fdevman_attr_show(p_dev, p_attr, idx, 1);
                    ccl_syslog_err("Testing value = %s\n\n", p_devman_cmd->parm[2]);
                    DEVMAN_ERROR_RETURN(CCL_FAIL);
                }
                ccl_syslog_err("OK\n");
            }
        } else {
            idx = (p_attr->maxnum == 1) ? 0: idx;
            /* compare attribute using the input string */
            _rc = devman_attr_cmp_s((devo_t *)p_dev, p_attr, idx, 
                                    p_devman_cmd->parm[2], (b_u32 *)&is_diff);
            if ( _rc ) {
                ccl_syslog_err("Error! Can't compare attr %s to value %s, device %s/%d\n", 
                       p_attr->name, p_devman_cmd->parm[2], 
                       p_dev->name, p_dev->sig & BRDDEV_MINOR_MASK);
                DEVMAN_ERROR_RETURN(CCL_FAIL);
            }
            if ( is_diff ) {
                ccl_syslog_err("\nERROR! The difference is observed, attribute actual value:\n");
                _fdevman_attr_show(p_dev, p_attr, idx, 1);
                ccl_syslog_err("Testing value = %s\n\n", p_devman_cmd->parm[2]);
                DEVMAN_ERROR_RETURN(CCL_FAIL);
            }
            ccl_syslog_err("OK\n");
        }        
    } else if ( p_devman_cmd->type == eDEVMAN_CMD_EXEC ) {
        char *cmd_name;
        char *parms[DEVMAN_DEVICE_CMD_PARMS_MAXNUM];
        int   parms_num = 0;

        /* check if there is the necessary parameters list */
        if ( !p_devman_cmd->parms_num ) {
            DEVMAN_ERROR_RETURN(CCL_NOT_DEFINED);
        }

        /* next argument: device type/discriminator; let's identify
           the device instance by type/discriminator */
        _rc = _fdevman_device_parse_type_discr(p_devman_cmd->parm[0], type, &discr);
        if ( _rc ) {
            ccl_syslog_err("Error! Can't get device type, discr for the %s\n", 
                   p_devman_cmd->parm[0]);
            DEVMAN_ERROR_RETURN(CCL_FAIL);
        }
        if ( discr == DEVMAN_DISCRIMINATOR_UNSPECIFIED ) {
            discr = 0;
            /* get the device instance handle */
            _rc = devman_device_object_identify(type, discr, (devo_t *)&p_dev);
            if ( _rc || !p_dev ) {
                ccl_syslog_err("Error! Can't identify %s type, %d discr object\n", type, discr);
                DEVMAN_ERROR_RETURN(CCL_FAIL);
            }
            /* we must exactly know what device we're operating now */
            if ( !p_dev->p_drv || p_dev->p_drv->instances > 1 )
                DEVMAN_ERROR_RETURN(CCL_FAIL);
        } else {
            /* get the device instance handle */
            _rc = devman_device_object_identify(type, discr, (devo_t *)&p_dev);
            if ( _rc || !p_dev ) {
                ccl_syslog_err("Error! Can't identify %s type, %d discr object\n", type, discr);
                DEVMAN_ERROR_RETURN(CCL_FAIL);
            }
        }

        /* at least 'devname' and 'cmdname' must be given */
        if ( p_devman_cmd->parms_num < 2 ) {
            _fdevman_device_cmds_show(p_dev, 0);
            DEVMAN_ERROR_RETURN(CCL_FAIL);
        }

        cmd_name = p_devman_cmd->parm[1];

        /* next argument: command name string; extract
           the next arguments: command parameters strings */
        for ( idx = 0; idx < DEVMAN_DEVICE_CMD_PARMS_MAXNUM && 
              idx+2 < p_devman_cmd->parms_num; idx++  ) {
            parms[idx] = p_devman_cmd->parm[idx+2];
            if ( !parms[idx] )
                break;
        }
        parms_num = idx;
        /* launch the command */
        _rc = devman_device_cmd_exec(p_dev, cmd_name, parms, parms_num);
        if ( _rc ) {
            ccl_syslog_err("Error! Can't launch %s cmd, on device %s\n", cmd_name, type);
            if ( _rc == CCL_NOT_DEFINED )
                _fdevman_device_cmds_show(p_dev, cmd_name);
            DEVMAN_ERROR_RETURN(CCL_FAIL);
        }
    }
    if ( p_devman_cmd->type == eDEVMAN_CMD_HELP ) {
        /* check if there is the parameters list */
        if ( !p_devman_cmd->parms_num ) {
            DEVMAN_ERROR_RETURN(CCL_NOT_DEFINED);
        }
        /* separate type name and discriminator if any */
        _rc = _fdevman_device_parse_type_discr(p_devman_cmd->parm[0], type, &discr);
        if ( _rc ) {
            ccl_syslog_err("Error! Can't get device type, discr for the %s\n", 
                   p_devman_cmd->parm[0]);
            DEVMAN_ERROR_RETURN(CCL_FAIL);
        }
        /* help all type if there isn't specified attribute */
        if ( p_devman_cmd->parms_num <= 1 ) {
            devman_type_help(type);
            return CCL_OK;
        }
        /* next argument: attribute name[index]; try to identify the attribute */
        _rc = _fdevman_attr_parse_name_idx(p_devman_cmd->parm[1], aname, &idx);
        if ( _rc ) {
            ccl_syslog_err("Error! Can't parse to name/idx attr %s\n", p_devman_cmd->parm[1]);
            DEVMAN_ERROR_RETURN(CCL_FAIL);
        }
        /* get type descriptor(driver) by the name of type */
        _rc = devman_device_type(type, &p_drv);
        if ( _rc || !p_drv ) {
            ccl_syslog_err("Error! Can't identify %s type\n", type);
            return CCL_FAIL;
        }
        /* help all 'similar' attributes */
        p_attr = 0;        
        do {
            _rc = _fdevman_attr_descr_find_next(p_drv, aname, &p_attr);
            if ( _rc || !p_attr )
                break;
            _fdevman_attr_help(p_attr);
        }
        while ( p_attr );

    }
    return CCL_OK;
}    

void devman_command_show(devman_cmd_t *p_cmd)
{    
    int i;
    if ( !p_cmd )
        return;
    printf("p_cmd->type=%d\n", p_cmd->type);
    printf("p_cmd->parms_num=%d\n", p_cmd->parms_num);
    for ( i = 0; i < p_cmd->parms_num; i++ ) {
        printf("parm[%d]=%s\n", i, p_cmd->parm[i]);
    }
    return;
}

ccl_err_t devman_byte_reorder(b_u8 *p_buf, b_u32 size)
{
    b_u32 i;
    b_u8 tmp;

    if (!p_buf || !size) {
        ccl_syslog_err("Invalid input: p_buf=%p, size=%d\n",
               p_buf, size);
        return CCL_FAIL;
    }

    for (i = 0; i < size/2; i++) {
        tmp = p_buf[i];
        p_buf[i] = p_buf[size-1-i];
        p_buf[size-1-i] = tmp; 
    }

    return CCL_OK;
}

ccl_err_t devman_init_crc_table(b_u8 *p_buf, b_u32 size)
{
    b_u8  bit, remainder;
    b_u32  i;
    const b_u8 crc7_poly = 0x107;

    if (!p_buf || size != DEVBRD_PMBUS_CRC_VAL_NUM) {
        ccl_syslog_err("Error! Invalid input parameters: p_buf=%p, size=%d\n",
               p_buf, size);
        return CCL_FAIL;
    }
    /*
     * Compute the remainder of each possible i.
     */
    for (i = 0; i < DEVBRD_PMBUS_CRC_VAL_NUM; ++i) {
        /*
         * Start with the i followed by zeros.
         */
        remainder = i;
        /*
         * Perform modulo-2 division, a bit at a time.
         */
        for (bit = 0; bit < 8; bit++) 
            remainder = (remainder << 1) ^ ((remainder & 0x80) ? crc7_poly : 0);
        /*
         * Store the result into the table.
         */
        p_buf[i] = remainder;
    }
    return CCL_OK;
}

b_u8 devman_get_crc(b_u8 message[], b_u32 length, b_u8 crc_table[])
{
  b_u32 i;
  b_u8  crc = 0;
 
  for (i = 0; i < length; i++)
    crc = crc_table[crc ^ message[i]];

  return crc;
}

char cmd_batm_devman_usage[] = 
"\n"
" devman [-s,--show] types                                      - show registered device types\n"
" devman [-s,--show] all                                        - show all devices,instances\n"
" devman [-s,--show] <type>                                     - show all instances of the device type\n"
" devman [-s,--show] <type>/<discrim>                           - show device instance\n"
" devman [-s,--show] <type>/<discrim> <attrname>                - show device instance attribute value\n"
" devman [-s,--show] <type>/<discrim> <attrname><[idx]>         - show attributes array element value\n"
" devman [-s,--show] <type>/<discrim> <attrname><[idx]> <num>   - show attributes array <num> elements from the <idx>\n"
" devman [-c,--conf] <type>/<discrim> <attrname> <value>        - configure device instance attribute value\n"
" devman [-c,--conf] <type>/<discrim> <attrname><[idx]> <value> - configure attributes array element value\n"
" devman [-t,--test] <type>/<discrim> <attrname> <value>        - test device instance attribute value\n"
" devman [-t,--test] <type>/<discrim> <attrname><[idx]> <value> - test attributes array element value\n"
" devman [-x,--exec] <type>/<discrim>                           - show commands defined for the device\n"
" devman [-x,--exec] <type>/<discrim> <command> [parm1 parm2..] - launch device instance command\n"
" devman [-h,--help] <type>                                     - display registered type info\n"
" devman [-h,--help] <type> <attrname>                          - display registered type attribute info\n"
;


