/********************************************************************************/
/**
 * @file i2ccore.c
 * @brief Xilinx I2C Bus Interface device manager driver
 * @copyright 2019 CELARE. All rights reserved.
 */
/********************************************************************************/
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <semaphore.h>
#include <linux/limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>

#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "spibus.h"
#include "i2ccore.h"

//#define I2CCORE_DEBUG
/* printing/error-returning macros */
#ifdef I2CCORE_DEBUG
    #define PDEBUG(fmt, args...) \
            ccl_syslog_err("I2CCORE: %s[%d]: " fmt, __FUNCTION__, __LINE__, ## args)
#else
    #define PDEBUG(fmt, args...)
#endif
#define I2CCORE_ERROR_RETURN(err) do { \
   ccl_syslog_err("Error! %s[%d], error=%i\n", __FUNCTION__, __LINE__, (err)); \
   return err; \
} while (0)

#define I2CCORE_REG_READ(context, offset, offset_sz, idx, pvalue, size) do { \
    PDEBUGG("\n");\
    _ret = _i2ccore_reg_read((context), \
                             (offset), \
                             (offset_sz), (idx), \
                             (pvalue), (size)); \
    if ( _ret ) { \
        PDEBUG("_i2ccore_reg_read error\n"); \
        I2CCORE_ERROR_RETURN(_ret); \
    } \
} while (0)

#define I2CCORE_REG_WRITE(context, offset, offset_sz, idx, pvalue, size) do { \
    PDEBUGG("\n");\
    _ret = _i2ccore_reg_write((context), \
                              (offset), \
                              (offset_sz), (idx), \
                              (pvalue), (size)); \
    if ( _ret ) { \
        PDEBUG("_i2ccore_reg_write error\n"); \
        I2CCORE_ERROR_RETURN(_ret); \
    } \
} while (0)

/**
 * The following constants are used to specify whether to do
 * Read or a Write operation on IIC bus.
 */
#define I2CCORE_READ_OPERATION	1 /**< Read operation on the IIC bus */
#define I2CCORE_WRITE_OPERATION	0 /**< Write operation on the IIC bus */

/**
 * The following constants are used with Transmit Function 
 * (_devi2ccore_raw_write_data) to specify whether to STOP after 
 * the current transfer of data or own the bus with a Repeated 
 * start. 
 */
#define I2CCORE_STOP		    0x00 /**< Send a stop on the IIC bus after the current data transfer */
#define I2CCORE_REPEATED_START	0x01 /**< Donot Send a stop on the IIC bus after the current data transfer */

#define I2CCORE_RW_BLOCK_MAX    (256 + sizeof(b_u32))	

/** 
 *  i2ccore registers offsets
 */
#define I2CCORE_GIE             0x1C  /**< Global Interrupt Enable register */
#define I2CCORE_ISR             0x20  /**< Interrupt Status register */          
#define I2CCORE_IER             0x28  /**< Interrupt Enable register */
#define I2CCORE_SOFTR           0x40  /**< Soft Reset register */
#define I2CCORE_CR              0x100 /**< Control register */
#define I2CCORE_SR              0x104 /**< Status register */
#define I2CCORE_TX_FIFO         0x108 /**< Transmit FIFO register */
#define I2CCORE_RX_FIFO         0x10C /**< Receive FIFO register */
#define I2CCORE_ADR             0x110 /**< Slave Address register */
#define I2CCORE_TX_FIFO_OCY     0x114 /**< Transmit FIFO Occupancy register */
#define I2CCORE_RX_FIFO_OCY     0x118 /**< Receive FIFO Occupancy register*/
#define I2CCORE_TEN_ADR         0x11C /**< Slave Ten Bit Address register */
#define I2CCORE_RX_FIFO_PIRQ    0x120 /**< Receive FIFO Programmable Depth Interrupt register */
#define I2CCORE_GPO             0x124 /**< General Purpose Output register */

#define I2CCORE_MAX_OFFSET      I2CCORE_GPO

/** 
 *  Reset register mask
 */
#define I2CCORE_RESET_MASK   	          0x0000000A /**< RESET Mask  */                          

/** 
 *  Control register (CR) masks
 */
#define I2CCORE_CR_ENABLE_DEVICE_MASK	  0x00000001 /**< Device enable = 1 */                    
#define I2CCORE_CR_TX_FIFO_RESET_MASK	  0x00000002 /**< Transmit FIFO reset=1 */                
#define I2CCORE_CR_MSMS_MASK		      0x00000004 /**< Master starts Txing=1 */                
#define I2CCORE_CR_DIR_IS_TX_MASK		  0x00000008 /**< Dir of Tx. Txing=1 */                   
#define I2CCORE_CR_NO_ACK_MASK		      0x00000010 /**< Tx Ack. NO ack = 1 */                   
#define I2CCORE_CR_REPEATED_START_MASK	  0x00000020 /**< Repeated start = 1 */                   
#define I2CCORE_CR_GENERAL_CALL_MASK	  0x00000040 /**< Gen Call enabled = 1 */                 

/** 
 *  Status register (SR) masks
 */
#define I2CCORE_SR_GEN_CALL_MASK		  0x00000001 /**< 1 = A Master issued a GC */             
#define I2CCORE_SR_ADDR_AS_SLAVE_MASK	  0x00000002 /**< 1 = When addressed as slave */          
#define I2CCORE_SR_BUS_BUSY_MASK		  0x00000004 /**< 1 = Bus is busy */                      
#define I2CCORE_SR_MSTR_RDING_SLAVE_MASK  0x00000008 /**< 1 = Dir: Master <--- slave */          
#define I2CCORE_SR_TX_FIFO_FULL_MASK	  0x00000010 /**< 1 = Tx FIFO full */                     
#define I2CCORE_SR_RX_FIFO_FULL_MASK	  0x00000020 /**< 1 = Rx FIFO full */                     
#define I2CCORE_SR_RX_FIFO_EMPTY_MASK	  0x00000040 /**< 1 = Rx FIFO empty */                    
#define I2CCORE_SR_TX_FIFO_EMPTY_MASK	  0x00000080 /**< 1 = Tx FIFO empty */                    

/** 
 *  Interrupt Status (ISR)/Enable (IER) registers masks
 */
#define I2CCORE_INTR_ARB_LOST_MASK	      0x00000001 /**< 1 = Arbitration lost */                 
#define I2CCORE_INTR_TX_ERROR_MASK	      0x00000002 /**< 1 = Tx error/msg complete */            
#define I2CCORE_INTR_TX_EMPTY_MASK	      0x00000004 /**< 1 = Tx FIFO/reg empty */                
#define I2CCORE_INTR_RX_FULL_MASK	      0x00000008 /**< 1 = Rx FIFO/reg=OCY level */            
#define I2CCORE_INTR_BNB_MASK	          0x00000010 /**< 1 = Bus not busy */                     
#define I2CCORE_INTR_AAS_MASK	          0x00000020 /**< 1 = When addr as slave */               
#define I2CCORE_INTR_NAAS_MASK	          0x00000040 /**< 1 = Not addr as slave */                
#define I2CCORE_INTR_TX_HALF_MASK	      0x00000080 /**< 1 = Tx FIFO half empty */               

/** 
 *  Global Interrupt Enable (GIE) mask
 */
#define I2CCORE_GINTR_ENABLE_MASK	      0x80000000 /**< Global Interrupt Enable Mask */

/**
 * TX_FIFO Register (DTR) masks 
 */
#define I2CCORE_TX_DYN_START_MASK		  0x00000100 /**< 1 = Set dynamic start */
#define I2CCORE_TX_DYN_STOP_MASK		  0x00000200 /**< 1 = Set dynamic stop */

/**
 * All Tx interrupts commonly used.
 */
#define I2CCORE_TX_INTERRUPTS	          (I2CCORE_INTR_TX_ERROR_MASK | \
				                           I2CCORE_INTR_TX_EMPTY_MASK | \
                  				           I2CCORE_INTR_TX_HALF_MASK)    

/**
 * All interrupts commonly used
 */
#define I2CCORE_TX_RX_INTERRUPTS	      (I2CCORE_INTR_RX_FULL_MASK | \
                                           I2CCORE_TX_INTERRUPTS)

#define I2CCORE_GPO_MAX_WIDTH             32         /** Maximum width of GPO register */

#define I2CCORE_TX_FIFO_DEPTH             16
#define I2CCORE_RX_FIFO_DEPTH             16
#define I2CCORE_RX_FIFO_PIRQ_DEPTH        I2CCORE_RX_FIFO_DEPTH - 1

/** @struct i2ccore_ctx_t
 *  i2ccore context structure
 */
typedef struct i2ccore_ctx_t {
    void                *devo;
    i2ccore_brdspec_t   brdspec;
} i2ccore_ctx_t;

static ccl_err_t  _ret;
static int        _rc;

/** 
 *  i2c cores references
 */
static i2ccore_ctx_t *_p_i2ccore[eDEVBRD_I2C_BUSES_NUM];

static ccl_err_t _i2ccore_reg_read(void *p_priv, b_u32 offset, 
                                   b_u32 offset_sz, b_u32 idx, 
                                   b_u8 *p_value, b_u32 size);
static ccl_err_t _i2ccore_reg_write(void *p_priv, b_u32 offset, 
                                    b_u32 offset_sz, b_u32 idx, 
                                    b_u8 *p_value, b_u32 size);
static ccl_err_t _devi2ccore_raw_write_data(i2c_route_t *p_i2c_point, 
                                            b_u8 *p_data, 
                                            b_u16 data_len, 
                                            b_u8 option,
                                            b_u32 *bytes_count);
static ccl_err_t _devi2ccore_raw_write_data_fifo(i2c_route_t *p_i2c_point, 
                                                 b_u8 *p_data, 
                                                 b_u16 data_len, 
                                                 b_u32 *bytes_count);
static ccl_err_t _devi2ccore_raw_dyn_write_data_fifo(i2c_route_t *p_i2c_point, 
                                                     b_u8 *p_data, 
                                                     b_u16 data_len, 
                                                     b_u32 *bytes_count);
static ccl_err_t _devi2ccore_raw_read_data(i2c_route_t *p_i2c_point, 
                                           b_u32 offset, b_u32 offset_sz,
                                           b_u8 *p_data, b_u32 data_len, 
                                           b_u32 *bytes_count);
static ccl_err_t _devi2ccore_raw_read_data_fifo(i2c_route_t *p_i2c_point, 
                                                b_u32 offset, b_u32 offset_sz,
                                                b_u8 *p_data, b_u32 data_len, 
                                                b_u32 *bytes_count);
static ccl_err_t _devi2ccore_raw_dyn_read_data_fifo(i2c_route_t *p_i2c_point, 
                                                    b_u32 offset, b_u32 offset_sz,
                                                    b_u8 *p_data, b_u32 data_len, 
                                                    b_u32 *bytes_count);
static ccl_err_t _i2ccore_set_offset(i2c_route_t *p_i2c_point, 
                                     b_u32 offset, 
                                     b_u32 offset_sz);

/**
 * @brief Check if i2c route is valid
 * @param[in] p_route i2c route
 * @return CCL_TRUE if the route is valid, CCL_FALSE otherwise 
 */
static b_bool _fi2ccore_route_is_valid(i2c_route_t *p_route, b_u8 mux_pin)
{

    int i, j = 0;

    if ( !p_route || !p_route->dev_i2ca || !_p_i2ccore[p_route->bus_id] ) {
        PDEBUG("Error! Invalid input parameters\n");
        I2CCORE_ERROR_RETURN(CCL_FALSE);
    }

    /* check muxes */
    while ( p_route->mux_i2ca[j] ) {
        for ( i = 0; i < _p_i2ccore[p_route->bus_id]->brdspec.slaves_num[mux_pin]; i++ ) {
            if ( p_route->mux_i2ca[j] == 
                 _p_i2ccore[p_route->bus_id]->brdspec.slave_i2ca[mux_pin][i] )
                break;
        }
        if ( i >= _p_i2ccore[p_route->bus_id]->brdspec.slaves_num[mux_pin] )
            goto _fi2ccore_route_is_invalid;
        j++;
    }
    /* check i2ca */
    for ( i = 0; i < _p_i2ccore[p_route->bus_id]->brdspec.slaves_num[mux_pin]; i++ ) {
        if ( p_route->dev_i2ca == 
             _p_i2ccore[p_route->bus_id]->brdspec.slave_i2ca[mux_pin][i] )
            return CCL_TRUE;
    }

    _fi2ccore_route_is_invalid:
    PDEBUG("Error! I2C route is invalid: bus=%d,i2ca=0x%x,"
           "mux[0]=0x%x,mux[1]=0x%x, mux_pin=%d\n", 
           p_route->bus_id, p_route->dev_i2ca, p_route->mux_i2ca[0], 
           p_route->mux_i2ca[1], mux_pin);    

    I2CCORE_ERROR_RETURN(CCL_FALSE);
}

ccl_err_t devi2ccore_write_buff(i2c_route_t *p_i2c_point, 
                                b_u8 mux_pin,
                                b_u32 offset, b_u32 offset_sz, 
                                b_u8 *p_value, b_u32 size)
{
    b_u8            write_buff[I2CCORE_RW_BLOCK_MAX];
    b_u32           bytes_count;
    b_u32           regval;
    b_u32           i, j;

    if ( !p_i2c_point ||  
         mux_pin > I2CCORE_GPO_MAX_WIDTH - 1 || 
         (size + offset_sz) > (int)I2CCORE_RW_BLOCK_MAX ) {
        PDEBUG("Error! Invalid input parameters\n");
        I2CCORE_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    PDEBUGG("offset=0x%03x, value=0x%08x\n", offset, *p_value);
#if 0
    /* check input parameters */
    if ( !_fi2ccore_route_is_valid(p_i2c_point, mux_pin)) {
        PDEBUG("Error! Invalid input parameters: p_i2c_point=0x%x "
               "bus_id=%d, dev_i2ca=0x%x, mux_i2ca=0x%x, mux_pin=%d\n", 
               p_i2c_point, p_i2c_point->bus_id,p_i2c_point->dev_i2ca, 
               p_i2c_point->mux_i2ca[0], mux_pin);
        I2CCORE_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
#endif
    /* prepare write buffer's offset prefix */    
    for (i = 0; i < offset_sz; i++) {
        j = (offset_sz - i - 1) * 8;
        write_buff[i] = (offset >> j) & 0xff;
    }
    /* setup the target value */ 
    memcpy(&write_buff[offset_sz], p_value, size); 

    PDEBUG("bus=%d, mux=%d, i2ca=0x%0x,offset=%d,offset_sz=%d,length=%d\n", 
           p_i2c_point->bus_id, mux_pin, p_i2c_point->dev_i2ca, offset, offset_sz, size);

    /* set mux via GPO */
    regval = mux_pin;
    I2CCORE_REG_WRITE(_p_i2ccore[p_i2c_point->bus_id], I2CCORE_GPO, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));
#if 0
    /* write buffer down */
    _ret = _devi2ccore_raw_dyn_write_data_fifo(p_i2c_point, 
                                               write_buff,
                                               offset_sz+size, 
                                               &bytes_count);
#else
    /* write buffer down */
    _ret = _devi2ccore_raw_write_data(p_i2c_point, write_buff, 
                                      offset_sz+size, I2CCORE_STOP, 
                                      &bytes_count);
#endif
    if ( _ret ) {
        PDEBUG("Error! Can't write word\n");
        I2CCORE_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

ccl_err_t devi2ccore_read_buff(i2c_route_t *p_i2c_point, 
                               b_u8 mux_pin, 
                               b_u32 offset, b_u32 offset_sz, 
                               b_u8 *p_value, b_u32 size)
{
    b_u32           bytes_count;
    b_u32           regval;
    b_u32           i, j;

    if ( !p_i2c_point || !offset_sz || 
         offset_sz > (int)sizeof(b_u32) || !p_value || 
         !size || mux_pin > I2CCORE_GPO_MAX_WIDTH - 1 || 
         (size + offset_sz) > (int)I2CCORE_RW_BLOCK_MAX ) {
        PDEBUG("Error! Invalid input parameters\n");
        I2CCORE_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    PDEBUG("bus=%d, mux=%d, i2ca=0x%0x,offset=%d,offset_sz=%d,length=%d\n", 
           p_i2c_point->bus_id, mux_pin, p_i2c_point->dev_i2ca, offset, offset_sz, size);
#if 0
    /* check input parameters */
    if ( !_fi2ccore_route_is_valid(p_i2c_point, mux_pin)) {
        PDEBUG("Error! Invalid input parameters: p_i2c_point=0x%x "
               "bus_id=%d, dev_i2ca=0x%x, mux_i2ca=0x%x, mux_pin=%d\n", 
               p_i2c_point, p_i2c_point->bus_id,p_i2c_point->dev_i2ca, 
               p_i2c_point->mux_i2ca[0], mux_pin);
        I2CCORE_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
#endif

    /* set mux via GPO */
    regval = mux_pin;
    I2CCORE_REG_WRITE(_p_i2ccore[p_i2c_point->bus_id], I2CCORE_GPO, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));


    double delta_usec;
    struct timespec started_tm;
    struct timespec ended_tm;

    clock_gettime(CLOCK_MONOTONIC, &started_tm);
#if 1
    /* read transaction */
    _ret = _devi2ccore_raw_read_data_fifo(p_i2c_point, 
                                          offset, offset_sz, 
                                          p_value, size, 
                                          &bytes_count);
#else
    /* read transaction */
    _ret = _devi2ccore_raw_dyn_read_data_fifo(p_i2c_point, 
                                              offset, offset_sz, 
                                              p_value, size, 
                                              &bytes_count);
#endif
    clock_gettime(CLOCK_MONOTONIC, &ended_tm);
    delta_usec = (double)(BILLION * (ended_tm.tv_sec - started_tm.tv_sec) + 
                          ended_tm.tv_nsec - started_tm.tv_nsec) / 1000;
    PDEBUG("%s - i2c read: %.3f usec\n", __FUNCTION__, delta_usec);
    if ( _ret ) {
        PDEBUG("Error! Can't read byte\n");
        I2CCORE_ERROR_RETURN(_ret);
    }

    return _ret;
}

/* i2ccore register read */
static ccl_err_t _i2ccore_reg_read(void *p_priv, b_u32 offset, 
                                   __attribute__((unused)) b_u32 offset_sz, 
                                   b_u32 idx, b_u8 *p_value, b_u32 size)
{
    i2ccore_ctx_t   *p_i2ccore;

    if ( !p_priv || offset > I2CCORE_MAX_OFFSET || 
         !p_value || idx || size != sizeof(b_u32) ) {
        PDEBUG("Error! Invalid input parameters\n");
        I2CCORE_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    PDEBUGG("123$=> offset=0x%03x, size=%d\n", offset, size);

    p_i2ccore = (i2ccore_ctx_t *)p_priv;     
    offset += p_i2ccore->brdspec.offset;
    _ret = devspibus_read_buff(p_i2ccore->brdspec.devname, offset, idx, p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to read over spi bus\n");
        I2CCORE_ERROR_RETURN(_ret);
    }
    PDEBUG("p_i2ccore->brdspec.offset=0x%x, offset=0x%03x, value=0x%08x\n", 
           p_i2ccore->brdspec.offset, offset, *(b_u32 *)p_value);

    return _ret;
}

/* i2ccore register write */
static ccl_err_t _i2ccore_reg_write(void *p_priv, b_u32 offset, 
                                    b_u32 __attribute__((unused)) offset_sz, 
                                    b_u32 idx, b_u8 *p_value, b_u32 size)
{
    i2ccore_ctx_t   *p_i2ccore;
    if ( !p_priv || offset > I2CCORE_MAX_OFFSET || 
         !p_value || idx || size != sizeof(b_u32) ) {
        PDEBUG("Error! Invalid input parameters\n");
        I2CCORE_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_i2ccore = (i2ccore_ctx_t *)p_priv;     
    offset += p_i2ccore->brdspec.offset;
    PDEBUG("p_i2ccore->brdspec.offset=0x%x, offset=0x%03x, value=0x%08x\n", 
           p_i2ccore->brdspec.offset, offset, *(b_u32 *)p_value);

    _ret = devspibus_write_buff(p_i2ccore->brdspec.devname, offset, idx, p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to write over spi bus\n");
        I2CCORE_ERROR_RETURN(_ret);
    }

    return _ret;
}

static ccl_err_t _i2ccore_set_offset(i2c_route_t *p_i2c_point, 
                                     b_u32 offset, b_u32 offset_sz)
{
    i2ccore_ctx_t *p_i2ccore;
    b_u32 regval = 0;
    b_u32 received_byte_count = 0;
    b_u8 write_buff[sizeof(b_u32)];
    b_u32 i, j;
    b_u32 cnt = 0;

    /* get the current smbus context */
    p_i2ccore = _p_i2ccore[p_i2c_point->bus_id];

    /* prepare internal dev offset as preamble write buffer */    
    for (i = 0; i < offset_sz; i++) {
        j = (offset_sz - i - 1) * 8;
        write_buff[i] = (offset >> j) & 0xff;
    }

    for (i = 0; i < offset_sz; i++) 
        PDEBUG("write_buff[%d]=0x%02x\n", i, write_buff[i]);
    PDEBUG("\n");   
    /*
     * Set the address register to the specified address by writing
     * the address to the device, this must be tried until it succeeds
     * because a previous write to the device could be pending and it
     * will not ack until that write is complete.
     */
    cnt = 0;
    do {
        if (cnt++ > 10000) {
            I2CCORE_ERROR_RETURN(CCL_FAIL);
        }
        I2CCORE_REG_READ(p_i2ccore,I2CCORE_SR, 
                         sizeof(b_u32), 0, 
                         (b_u8 *)&regval, sizeof(b_u32));
        if (!(regval & I2CCORE_SR_BUS_BUSY_MASK)) {
            _ret = _devi2ccore_raw_write_data(p_i2c_point, write_buff, 
                                              offset_sz, I2CCORE_REPEATED_START, 
                                              &received_byte_count);
            if (_ret) {
                PDEBUG("_devi2ccore_raw_write_data error\n");
                I2CCORE_ERROR_RETURN(_ret);
            }

            if (received_byte_count != offset_sz) {
                /* Send is aborted so reset Tx FIFO */
                I2CCORE_REG_READ(p_i2ccore,I2CCORE_CR, 
                                 sizeof(b_u32), 0, 
                                 (b_u8 *)&regval, sizeof(b_u32));
                regval |= I2CCORE_CR_TX_FIFO_RESET_MASK;
                I2CCORE_REG_WRITE(p_i2ccore,I2CCORE_CR, 
                                  sizeof(b_u32), 0, 
                                  (b_u8 *)&regval, sizeof(b_u32));                            
                regval = I2CCORE_CR_ENABLE_DEVICE_MASK;
                I2CCORE_REG_WRITE(p_i2ccore,I2CCORE_CR, 
                                  sizeof(b_u32), 0, 
                                  (b_u8 *)&regval, sizeof(b_u32));
            }
        }

    } while (received_byte_count != offset_sz);

    return CCL_OK;
}

static ccl_err_t _i2ccore_ier_write(i2ccore_ctx_t *p_i2ccore, b_u32 value)
{
    b_u32 regval= value;

    if ( !p_i2ccore ) {
        PDEBUG("Error! Invalid input parameters\n");
        I2CCORE_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_IER, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/**
* @brief Disable the specified interrupt in the 
* Interrupt enable register.  It is non-destructive in that the 
* register is read and only the interrupt specified is cleared. 
* Clearing an interrupt acknowledges it. 
*
* @param[in] p_i2ccore is a pointer to the i2ccore_ctx_t 
*       instance to be worked on
* @param[in] mask contains the interrupts to be disabled
*
* @return	None.
*/
static ccl_err_t _i2ccore_disable_intr(i2ccore_ctx_t *p_i2ccore, b_u32 mask)
{
    b_u32 regval;

    if ( !p_i2ccore ) {
        PDEBUG("Error! Invalid input parameters\n");
        I2CCORE_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    I2CCORE_REG_READ(p_i2ccore,I2CCORE_IER, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));
    regval &= ~mask;
    I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_IER, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/**
* @brief Enable the specified interrupt in the 
* Interrupt enable register.  It is non-destructive in that the 
* register is read and only the interrupt specified is cleared. 
* Clearing an interrupt acknowledges it. 
*
* @param[in] p_i2ccore is a pointer to the i2ccore_ctx_t 
*       instance to be worked on
* @param[in] mask contains the interrupts to be disabled
*
* @return	None.
*/
static ccl_err_t _i2ccore_enable_intr(i2ccore_ctx_t *p_i2ccore, b_u32 mask)
{
    b_u32 regval;

    if ( !p_i2ccore ) {
        PDEBUG("Error! Invalid input parameters\n");
        I2CCORE_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    I2CCORE_REG_READ(p_i2ccore,I2CCORE_IER, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));
    regval |= mask;
    I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_IER, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/**
* @brief Clear the specified interrupt in the 
* Interrupt status register.  It is non-destructive in that the 
* register is read and only the interrupt specified is cleared. 
* Clearing an interrupt acknowledges it. 
*
* @param[in] p_i2ccore is a pointer to the i2ccore_ctx_t 
*       instance to be worked on
* @param[in] mask contains the interrupts to be disabled
*
* @return	None.
*/
static ccl_err_t _i2ccore_clear_isr(i2ccore_ctx_t *p_i2ccore, b_u32 mask)
{
    b_u32 regval;

    if ( !p_i2ccore ) {
        PDEBUG("Error! Invalid input parameters\n");
        I2CCORE_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    I2CCORE_REG_READ(p_i2ccore,I2CCORE_ISR, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));
    regval &= mask;
    I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_ISR, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/**
* @brief Clear and enable the specified interrupt in the 
* Interrupt status and enable registers.  It is non-destructive 
* in that the registers are read and only the interrupt 
* specified is modified. Clearing an interrupt acknowledges it. 
*
* @param[in] p_i2ccore is a pointer to the i2ccore_ctx_t 
*       instance to be worked on
* @param[in] mask contains the interrupts to be disabled
*
* @return	None.
*/
static ccl_err_t _i2ccore_clear_enable_intr(i2ccore_ctx_t *p_i2ccore, b_u32 mask)
{
    b_u32 regval;

    if ( !p_i2ccore ) {
        PDEBUG("Error! Invalid input parameters\n");
        I2CCORE_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    I2CCORE_REG_READ(p_i2ccore,I2CCORE_ISR, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));
    PDEBUG("regval=0x%x, mask=0x%x\n",
           regval, mask);
    regval &= mask;
    I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_ISR, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));
    I2CCORE_REG_READ(p_i2ccore,I2CCORE_IER, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));
    regval |= mask;
    I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_IER, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/**
* @brief Start the IIC device and driver by enabling the proper
* interrupts such that data may be sent and received on the IIC bus.
* This function must be called before the functions to send and receive data.
*
* Before _i2ccore_start() is called, the interrupt control must 
* connect the ISR routine to the interrupt handler. This is done 
* by the user, and not _i2ccore_start() to allow the user to use 
* an interrupt controller of their choice. 
*
* Start enables:
*  - IIC device
*  - Interrupts:
*	 - Addressed as slave to allow messages from another master
*	 - Arbitration Lost to detect Tx arbitration errors
*	 - Global IIC interrupt
*
* @param[in] p_i2ccore is a pointer to the i2ccore_ctx_t 
*       instance to be worked on
*
* @return CCL_OK on success, error code on failure
*/
static ccl_err_t _i2ccore_start(void *p_priv)
{
    i2ccore_ctx_t *p_i2ccore;
    b_u32         regval; 

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        I2CCORE_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_i2ccore = (i2ccore_ctx_t *)p_priv;

    /* Mask off all interrupts, each is enabled when needed */
    _ret = _i2ccore_ier_write(p_i2ccore, 0);
    if (_ret) {
        PDEBUG("_i2ccore_ier_write error\n");
        I2CCORE_ERROR_RETURN(_ret);
    }

    /*
     * Clear all interrupts by reading and rewriting exact value back.
     * Only those bits set will get written as 1 (writing 1 clears intr).
     */
    _ret = _i2ccore_clear_isr(p_i2ccore, 0xFFFFFFFF);
    if (_ret) {
        PDEBUG("_i2ccore_clear_isr error\n");
        I2CCORE_ERROR_RETURN(_ret);
    }

    /* Enable the device */
    regval = I2CCORE_CR_ENABLE_DEVICE_MASK;
    I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_CR, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));

    /* Set Rx FIFO Occupancy depth to throttle at first byte(after reset = 0) */
    regval = 0;
    I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_RX_FIFO_PIRQ, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));

    /* Clear and enable the interrupts needed */
    _ret = _i2ccore_clear_enable_intr(p_i2ccore, 
                                      I2CCORE_INTR_AAS_MASK | 
                                      I2CCORE_INTR_ARB_LOST_MASK | 
                                      I2CCORE_TX_RX_INTERRUPTS);
    if (_ret) {
        PDEBUG("_i2ccore_clear_enable_intr error\n");
        I2CCORE_ERROR_RETURN(_ret);
    }

    if (p_i2ccore->brdspec.op_mode == eDEVBRD_OP_MODE_INTR) {
        /* Enable the Global Interrupt Enable */
        regval = I2CCORE_GINTR_ENABLE_MASK;
        I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_GIE, 
                          sizeof(b_u32), 0, 
                          (b_u8 *)&regval, sizeof(b_u32));
    }

    return CCL_OK;
}

/**
 * @brief Resets the IIC device.
 * @param[in] p_priv is a pointer to the i2ccore_ctx_t instance
 *       to be worked on.
 *
 * @return	None.
 *
 * @note     The complete IIC core is Reset on giving a software reset to
 *           the IIC core. Some previous versions of the core only reset
 *           the Interrupt Logic/Registers, please refer to the HW specification
 *           for futher details about this.
 */
static ccl_err_t _i2ccore_reset(void *p_priv)
{
    i2ccore_ctx_t *p_i2ccore;
    b_u32         regval = I2CCORE_RESET_MASK;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        I2CCORE_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_i2ccore = (i2ccore_ctx_t *)p_priv;

    /* reset the device */
    I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_SOFTR, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

static ccl_err_t _i2ccore_init(void *p_priv)
{
    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        I2CCORE_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* reset the device */
    _ret = _i2ccore_reset(p_priv);
    if ( _ret ) {
        PDEBUG("_i2ccore_start error\n");
        I2CCORE_ERROR_RETURN(_ret);
    }

    /* start the device */
    _ret = _i2ccore_start(p_priv);
    if ( _ret ) {
        PDEBUG("_i2ccore_start error\n");
        I2CCORE_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

/**
* @brief Stop the IIC device and driver such that data is no 
* longer sent or received on the IIC bus. This function stops 
* the device by disabling interrupts. This function only 
* disables interrupts within the device such that the caller is 
* responsible for disconnecting the interrupt handler of the 
* device from the interrupt source and disabling interrupts at 
* other levels. 
*
* Due to bus throttling that could hold the bus between messages when using
* repeated start option, stop will not occur when the device is actively
* sending or receiving data from the IIC bus or the bus is being throttled
* by this device, but instead return CCL_BUSY.
*
* @param[in] p_i2ccore is a pointer to the i2ccore_ctx_t 
*       instance to be worked on
*
* @return
*		- CCL_OK indicates all IIC interrupts are disabled.
*       No messages can be received or transmitted until
*       i2ccore_start() is called.
*		- CCL_BUSY indicates this device is currently engaged
*       in message traffic and cannot be stopped.
*       - CCL_BAD_PARAM_ERR otherwise
*
* @note		None.
*
****************************************************************************/
static ccl_err_t _i2ccore_stop(void *p_priv)
{
    i2ccore_ctx_t *p_i2ccore;
    b_u32         regval; 
    b_u32         status;
    b_u32         cntl;


    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        I2CCORE_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_i2ccore = (i2ccore_ctx_t *)p_priv;

    /* Disable all interrupts globally */
    regval= 0;
    I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_GIE, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));
    I2CCORE_REG_READ(p_i2ccore, I2CCORE_CR, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&cntl, sizeof(b_u32));
    I2CCORE_REG_READ(p_i2ccore, I2CCORE_SR, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&status, sizeof(b_u32));

    if ((cntl & I2CCORE_CR_MSMS_MASK) ||
        (status & I2CCORE_SR_MSTR_RDING_SLAVE_MASK)) {
        /*
         * When this device is using the bus
         * - re-enable interrupts to finish current messaging
         * - return bus busy
         */
        regval= I2CCORE_GINTR_ENABLE_MASK;
        I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_GIE, 
                          sizeof(b_u32), 0, 
                          (b_u8 *)&regval, sizeof(b_u32));

        I2CCORE_ERROR_RETURN(CCL_BUSY);
    }

    return CCL_OK;
}

/**
* @brief Tell whether the I2C bus is busy or free.
*
* @param[in] p_i2ccore is a pointer to the i2ccore_ctx_t 
*       instance to be worked on
* @param[out] pbusy - pointer to boolean value indicating 
*       whether the bus is busy
* @return error code
*/
static ccl_err_t _i2ccore_check_is_bus_busy(i2ccore_ctx_t *p_i2ccore, b_bool *pbusy)
{
    b_u32 status = 0;

    if ( !p_i2ccore || !pbusy ) {
        PDEBUG("Error! Invalid input parameters\n");
        I2CCORE_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    I2CCORE_REG_READ(p_i2ccore, I2CCORE_SR, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&status, sizeof(b_u32));
    if (status & I2CCORE_SR_BUS_BUSY_MASK)
        *pbusy = CCL_TRUE;
    else
        *pbusy = CCL_FALSE;

    return CCL_OK;
}

/**
* @brief Wait until the I2C bus is free or timeout.
*
* @param[in] p_i2ccore is a pointer to the i2ccore_ctx_t 
*       instance to be worked on
*  
* @return
*		- CCL_OK if the I2C bus was freed before the timeout.
*		- CCL_FAIL otherwise.
*
* @note		None.
*
*******************************************************************************/
static ccl_err_t _i2ccore_wait_bus_free(i2ccore_ctx_t *p_i2ccore)
{
    b_u32 cnt = 0;
    b_bool busy;

    if ( !p_i2ccore ) {
        PDEBUG("Error! Invalid input parameters\n");
        I2CCORE_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    _ret = _i2ccore_check_is_bus_busy(p_i2ccore, &busy);
    if (_ret) {
        PDEBUG("_i2ccore_check_is_bus_busy error\n");
        I2CCORE_ERROR_RETURN(CCL_FAIL);
    }
    while (busy) {
        if (cnt++ > 10000) {
            I2CCORE_ERROR_RETURN(CCL_FAIL);
        }
        usleep(100);
        _ret = _i2ccore_check_is_bus_busy(p_i2ccore, &busy);
        if (_ret) {
            PDEBUG("_i2ccore_check_is_bus_busy error\n");
            I2CCORE_ERROR_RETURN(CCL_FAIL);
        }
    }

    return CCL_OK;
}

/**
*
* @brief Send the address for a 7 bit address during both read 
* and write operations. It takes care of the details to format 
* the address correctly. This macro is designed to be called 
* internally to the drivers. 
*
* @param[in] p_i2ccore is a pointer to the i2ccore_ctx_t 
*       instance to be worked on
* @param[in] p_i2c_point is the address of the i2c device to 
*       send to
* @param[in] operation indicates I2CCORE_READ_OPERATION or 
*       I2CCORE_WRITE_OPERATION
* @param[in] mask indicates used dynamic start/stop masks
*
* @return	error code
*/
static inline ccl_err_t _i2ccore_send_7bit_address(i2ccore_ctx_t *p_i2ccore, 
                                                   i2c_route_t *p_i2c_point, 
                                                   b_u32 operation,
                                                   b_u32 mask)
{                                   
    b_u32 local_addr = (b_u8)(p_i2c_point->dev_i2ca << 1);
    local_addr = (local_addr & 0xFE) | 
                  operation | 
                  mask;

    I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_TX_FIFO, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&local_addr, sizeof(b_u32));
    return CCL_OK;
}

/******************************************************************************
*
* This function fills the FIFO using the occupancy register to determine the
* available space to be filled. When the repeated start option is on, the last
* byte is withheld to allow the control register to be properly set on the last
* byte.
* 
* @param[in] p_i2ccore is a pointer to the i2ccore_ctx_t 
*       instance to be worked on
* @param[in] p_data points to the data to be sent.
* @param[in] data_len is the number of bytes to be sent.
* @param[in] is_dynamic indicates whether dynamic mode is used
*       for data transmission 
* @param[out] bytes_count is the pointer to hold the number of 
*       bytes sent
* 
* @return	error code
*/
ccl_err_t _i2ccore_tx_fifo_fill(i2ccore_ctx_t *p_i2ccore, 
                                b_u8 *p_data, 
                                b_u16 data_len, 
                                b_bool is_dynamic,
                                b_bool set_stop_bit,
                                b_u32 *bytes_count)
{
	b_u32 avail_bytes;
    b_u32 regval = 0;
	int i;
	int bytes_to_send;
    b_bool space_avail = CCL_FALSE;

	/*
	 * Determine number of bytes to write to FIFO. Number of bytes that
	 * can be put into the FIFO is (FIFO depth) - (current occupancy + 1)
	 * When more room in FIFO than msg bytes put all of message in the FIFO.
	 */
    I2CCORE_REG_READ(p_i2ccore, I2CCORE_TX_FIFO_OCY, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));
	avail_bytes = I2CCORE_TX_FIFO_DEPTH - (regval + 1);
    PDEBUG("avail_bytes=%d\n", avail_bytes);
	if (data_len > avail_bytes) {
		bytes_to_send = avail_bytes;
	} else {
        /*
         * More space in FIFO than bytes in message.
         */
        if (is_dynamic) {
            bytes_to_send = data_len;
            space_avail = CCL_TRUE;
        }
        else
            bytes_to_send = data_len - 1;
	}
    PDEBUG("bytes_to_send=%d\n", bytes_to_send);

	/*
	 * Fill FIFO with amount determined above.
	 */
	for (i = 0; i < bytes_to_send; i++) {
        regval = *(b_u8 *)p_data;
        /* if this is the last byte in the message
           append the mask to it */
        if (is_dynamic && space_avail &&
            set_stop_bit && 
            (i == (bytes_to_send - 1))) {
            regval |= I2CCORE_TX_DYN_STOP_MASK;
        }
        I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_TX_FIFO, 
                  sizeof(b_u32), 0, 
                  (b_u8 *)&regval, sizeof(b_u32));
        p_data++;
	}

    *bytes_count = bytes_to_send;
    PDEBUG("bytes_count=%d\n", *bytes_count);

    return CCL_OK;
}

/**
* @brief Send the specified buffer to the device that has been 
* previously addressed on the IIC bus.  This function assumes 
* that the 7 bit address has been sent and it should wait for 
* the transmit of the address to complete. 
*
* @param[in] p_i2ccore is a pointer to the i2ccore_ctx_t 
*       instance to be worked on
* @param[in] p_data points to the data to be sent.
* @param[in] data_len is the number of bytes to be sent.
* @param[in] option indicates whether to hold or free the bus 
*       after transmitting the data.
* @param[out] bytes_count is the pointer to the number of bytes 
*       remaining to be sent
*
* @return	error code
*/
static ccl_err_t _i2ccore_send_data(i2ccore_ctx_t *p_i2ccore, b_u8 *p_data, 
                                    b_u16 data_len, b_u32 option, b_u32 *bytes_count)
{
    b_u32 regval = 0;
    b_u32 cnt = 0;

    if ( !p_i2ccore || !p_data || !data_len || !bytes_count ) {
        PDEBUG("Error! Invalid input parameters: p_i2ccore=0x%x, p_data=0x%p, "
               "data_len=%d, bytes_count=0x%p\n", 
               p_i2ccore, p_data, data_len, bytes_count);
        I2CCORE_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Send the specified number of bytes in the specified buffer by polling
     * the device registers and blocking until complete
     */
    while (data_len > 0) {
        PDEBUG("data_len=%d, p_data=0x%02x\n", data_len, *p_data);
        /*
         * Wait for the transmit to be empty before sending any more
         * data by polling the interrupt status register
         */
        cnt = 0;
        while (1) {
            if (cnt++ > 10000) {
                I2CCORE_ERROR_RETURN(CCL_FAIL);
            }
            I2CCORE_REG_READ(p_i2ccore, I2CCORE_ISR, 
                             sizeof(b_u32), 0, 
                             (b_u8 *)&regval, sizeof(b_u32));

            if (regval & (I2CCORE_INTR_TX_ERROR_MASK |
                         I2CCORE_INTR_ARB_LOST_MASK |
                         I2CCORE_INTR_BNB_MASK)) {
                *bytes_count = data_len;
                return CCL_OK;
            }
            if (regval & I2CCORE_INTR_TX_EMPTY_MASK)
                break;
        }
        /* If there is more than one byte to send then put the
         * next byte to send into the transmit FIFO
         */
        if (data_len > 1) {
            regval = *(b_u8 *)p_data;
            I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_TX_FIFO, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));
            p_data++;
        } else {
            if (option == I2CCORE_STOP) {
                /*
                 * If the Option is to release the bus after
                 * the last data byte, Set the stop Option
                 * before sending the last byte of data so
                 * that the stop Option will be generated
                 * immediately following the data. This is
                 * done by clearing the MSMS bit in the
                 * control register.
                 */
                regval= I2CCORE_CR_ENABLE_DEVICE_MASK | 
                        I2CCORE_CR_DIR_IS_TX_MASK;
                I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_CR, 
                                  sizeof(b_u32), 0, 
                                  (b_u8 *)&regval, sizeof(b_u32));
            }

            /*
             * Put the last byte to send in the transmit FIFO
             */
            regval = *(b_u8 *)p_data;
            I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_TX_FIFO, 
                              sizeof(b_u32), 0, 
                              (b_u8 *)&regval, sizeof(b_u32));
            p_data++;
            if (option == I2CCORE_REPEATED_START) {
                _ret = _i2ccore_clear_isr(p_i2ccore, I2CCORE_INTR_TX_EMPTY_MASK);
                if ( _ret ) {
                    PDEBUG("_i2ccore_clear_isr error\n");
                    I2CCORE_ERROR_RETURN(CCL_BAD_PARAM_ERR);
                }
                /*
                 * Wait for the transmit to be empty before
                 * setting RSTA bit.
                 */
                cnt = 0;
                while (1) {
                    if (cnt++ > 10000) {
                        I2CCORE_ERROR_RETURN(CCL_FAIL);
                    }
                    I2CCORE_REG_READ(p_i2ccore, I2CCORE_ISR, 
                                     sizeof(b_u32), 0, 
                                     (b_u8 *)&regval, sizeof(b_u32));
                    if (regval& I2CCORE_INTR_TX_EMPTY_MASK) {
                        /*
                         * RSTA bit should be set only
                         * when the FIFO is completely
                         * Empty.
                         */
                        regval= I2CCORE_CR_REPEATED_START_MASK | 
                                I2CCORE_CR_ENABLE_DEVICE_MASK | 
                                I2CCORE_CR_DIR_IS_TX_MASK | 
                                I2CCORE_CR_MSMS_MASK;
                        I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_CR, 
                                          sizeof(b_u32), 0, 
                                          (b_u8 *)&regval, sizeof(b_u32));
                        break;
                    }
                }
            }
        }

        /*
         * Clear the latched interrupt status register and this must be
         * done after the transmit FIFO has been written to or it won't
         * clear
         */
        _ret = _i2ccore_clear_isr(p_i2ccore, I2CCORE_INTR_TX_EMPTY_MASK);
        if ( _ret ) {
            PDEBUG("_i2ccore_clear_isr error\n");
            I2CCORE_ERROR_RETURN(CCL_BAD_PARAM_ERR);
        }

        /*
         * Update the byte count to reflect the byte sent and clear
         * the latched interrupt status so it will be updated for the
         * new state
         */
        data_len--;
    }

    if (option == I2CCORE_STOP) {
        /*
         * If the Option is to release the bus after transmission of
         * data, Wait for the bus to transition to not busy before
         * returning, the IIC device cannot be disabled until this
         * occurs. Note that this is different from a receive operation
         * because the stop Option causes the bus to go not busy.
         */
        cnt = 0;
        while (1) {
            if (cnt++ > 10000) {
                I2CCORE_ERROR_RETURN(CCL_FAIL);
            }
            I2CCORE_REG_READ(p_i2ccore, I2CCORE_ISR, 
                             sizeof(b_u32), 0, 
                             (b_u8 *)&regval, sizeof(b_u32));
            if (regval& I2CCORE_INTR_BNB_MASK) {
                break;
            }
        }
    }

    *bytes_count = data_len;

    return CCL_OK;
}

/**
* @brief Send data as a master on the IIC bus.  This function 
* sends the data using polled I/O and blocks until the data has 
* been sent. It only supports 7 bit addressing mode of 
* operation.  
*
* @param[in] p_i2c_point is the address of the i2c device to 
*       send to
* @param[in] p_data points to the data to be sent.
* @param[in] data_len is the number of bytes to be sent. 
* @param[in] option can be "stop" or "repeated start" 
* @param[out] bytes_count is the pointer to hold the number of 
*       bytes sent
*
* @return	error code
*/
static ccl_err_t _devi2ccore_raw_write_data(i2c_route_t *p_i2c_point, 
                                            b_u8 *p_data, 
                                            b_u16 data_len, 
                                            b_u8 option, 
                                            b_u32 *bytes_count)
{
    i2ccore_ctx_t *p_i2ccore;
    b_u32 remaining_byte_count;
    b_u32 regval = 0;
    b_u32 ctrl = 0;
    b_u32 status = 0;
    b_u32 cnt = 0;

    if ( !p_i2c_point || !p_data || !data_len || !bytes_count ) {
        PDEBUG("Error! Invalid input parameters: p_i2ccore=0x%x, p_data=0x%p, "
               "data_len=%d, bytes_count=0x%p\n", 
               p_i2c_point, p_data, data_len, bytes_count);
        I2CCORE_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    *bytes_count = 0;
    /* get the current smbus context */
    p_i2ccore = _p_i2ccore[p_i2c_point->bus_id];
    /* Wait until I2C bus is freed, exit if timed out. */
    if (_i2ccore_wait_bus_free(p_i2ccore) != CCL_OK) {
        *bytes_count = 0; 
        I2CCORE_ERROR_RETURN(CCL_FAIL);
    }
    /* Check to see if already Master on the Bus.
     * If Repeated Start bit is not set send Start bit by setting
     * MSMS bit else Send the address.
     */
    I2CCORE_REG_READ(p_i2ccore, I2CCORE_CR, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&ctrl, sizeof(b_u32));
    if ((ctrl & I2CCORE_CR_REPEATED_START_MASK) == 0) {
        /*
         * Put the address into the FIFO to be sent and indicate
         * that the operation to be performed on the bus is a
         * write operation
         */
        _ret = _i2ccore_send_7bit_address(p_i2ccore, 
                                          p_i2c_point, 
                                          I2CCORE_WRITE_OPERATION,
                                          0);
        if ( _ret ) {
            PDEBUG("_i2ccore_send_7bit_address error\n");
            I2CCORE_ERROR_RETURN(CCL_BAD_PARAM_ERR);
        }
        /* Clear the latched interrupt status so that it will
         * be updated with the new state when it changes, this
         * must be done after the address is put in the FIFO
         */
        _ret = _i2ccore_clear_isr(p_i2ccore, 
                                  I2CCORE_INTR_TX_EMPTY_MASK |
                                  I2CCORE_INTR_TX_ERROR_MASK |
                                  I2CCORE_INTR_ARB_LOST_MASK);
        if ( _ret ) {
            PDEBUG("_i2ccore_clear_isr error\n");
            I2CCORE_ERROR_RETURN(CCL_BAD_PARAM_ERR);
        }
    PDEBUG("\n");   
        /*
         * MSMS must be set after putting data into transmit FIFO,
         * indicate the direction is transmit, this device is master
         * and enable the IIC device
         */
        regval= I2CCORE_CR_MSMS_MASK | 
                I2CCORE_CR_DIR_IS_TX_MASK | 
                I2CCORE_CR_ENABLE_DEVICE_MASK;
        I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_CR, 
                          sizeof(b_u32), 0, 
                          (b_u8 *)&regval, sizeof(b_u32));

        /*
         * Clear the latched interrupt
         * status for the bus not busy bit which must be done while
         * the bus is busy
         */
        I2CCORE_REG_READ(p_i2ccore, I2CCORE_SR, 
                         sizeof(b_u32), 0, 
                         (b_u8 *)&status, sizeof(b_u32));
        cnt = 0;
        while ((status & I2CCORE_SR_BUS_BUSY_MASK) == 0) {
            if (cnt++ > 10000) {
                I2CCORE_ERROR_RETURN(CCL_FAIL);
            }
            I2CCORE_REG_READ(p_i2ccore, I2CCORE_SR, 
                             sizeof(b_u32), 0, 
                             (b_u8 *)&status, sizeof(b_u32));
        }
        _ret = _i2ccore_clear_isr(p_i2ccore, I2CCORE_INTR_BNB_MASK);
        if ( _ret ) {
            PDEBUG("_i2ccore_clear_isr error\n");
            I2CCORE_ERROR_RETURN(CCL_BAD_PARAM_ERR);
        }
    } else {
        /*
         * Already owns the Bus indicating that its a Repeated Start
         * call. 7 bit slave address, send the address for a write
         * operation and set the state to indicate the address has
         * been sent.
         */
        _ret = _i2ccore_send_7bit_address(p_i2ccore, 
                                          p_i2c_point, 
                                          I2CCORE_WRITE_OPERATION,
                                          0);
        if ( _ret ) {
            PDEBUG("_i2ccore_send_7bit_address error\n");
            I2CCORE_ERROR_RETURN(CCL_BAD_PARAM_ERR);
        }
    }

    /* Send the specified data to the device on the IIC bus specified by the
     * the address
     */
    _ret = _i2ccore_send_data(p_i2ccore, p_data, 
                              data_len, option, 
                              &remaining_byte_count);
    if ( _ret ) {
        PDEBUG("_i2ccore_send_data error\n");
        I2CCORE_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }


    I2CCORE_REG_READ(p_i2ccore, I2CCORE_CR, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&ctrl, sizeof(b_u32));
    if ((ctrl & I2CCORE_CR_REPEATED_START_MASK) == 0) {
        /*
         * The Transmission is completed, disable the IIC device if
         * the Option is to release the Bus after transmission of data
         * and return the number of bytes that was received. Only wait
         * if master, if addressed as slave just reset to release
         * the bus.
         */
        if ((ctrl & I2CCORE_CR_MSMS_MASK) != 0) {
            regval= ctrl & ~I2CCORE_CR_MSMS_MASK;
            I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_CR, 
                              sizeof(b_u32), 0, 
                              (b_u8 *)&regval, sizeof(b_u32));
        }

        I2CCORE_REG_READ(p_i2ccore, I2CCORE_SR, 
                         sizeof(b_u32), 0, 
                         (b_u8 *)&status, sizeof(b_u32));
        if ((status & I2CCORE_SR_ADDR_AS_SLAVE_MASK) != 0) {
            regval= 0;
            I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_CR, 
                              sizeof(b_u32), 0, 
                              (b_u8 *)&regval, sizeof(b_u32));
        } else {
            I2CCORE_REG_READ(p_i2ccore, I2CCORE_SR, 
                             sizeof(b_u32), 0, 
                             (b_u8 *)&status, sizeof(b_u32));
            cnt = 0;
            while ((status & I2CCORE_SR_BUS_BUSY_MASK) != 0) {
                if (cnt++ > 10000) {
                    I2CCORE_ERROR_RETURN(CCL_FAIL);
                }
                I2CCORE_REG_READ(p_i2ccore, I2CCORE_SR, 
                                 sizeof(b_u32), 0, 
                                 (b_u8 *)&status, sizeof(b_u32));
            }
        }
    }

    *bytes_count = data_len - remaining_byte_count;

    return CCL_OK;
}

/**
* This function sends data as a master on the IIC bus. If the bus is busy, it
* will indicate so and then enable an interrupt such that the status handler
* will be called when the bus is no longer busy.  The slave address which has
* been set with the XIic_SetAddress() function is the address to which the
* specific data is sent.  Sending data on the bus performs a write operation.
*
* @param[in] p_i2c_point is the address of the i2c device to 
*       send to
* @param[in] p_data points to the data to be sent.
* @param[in] data_len is the number of bytes to be sent. 
* @param[out] bytes_count is the pointer to hold the number of 
*       bytes sent
*
* @return	error code
*/
static ccl_err_t _devi2ccore_raw_write_data_fifo(i2c_route_t *p_i2c_point, b_u8 *p_data, 
                                                 b_u16 data_len, b_u32 *bytes_count)
{
    i2ccore_ctx_t *p_i2ccore;
    b_u32 remaining_byte_count;
    b_u32 regval = 0;
    b_u32 ctrl = 0;
    b_u32 status = 0;


    if ( !p_i2c_point || !p_data || !data_len || !bytes_count ) {
        PDEBUG("Error! Invalid input parameters: p_i2ccore=0x%x, p_data=0x%p, "
               "data_len=%d, bytes_count=0x%p\n", 
               p_i2c_point, p_data, data_len, bytes_count);
        I2CCORE_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    *bytes_count = 0;
    /* get the current smbus context */
    p_i2ccore = _p_i2ccore[p_i2c_point->bus_id];
    if (p_i2ccore->brdspec.op_mode == eDEVBRD_OP_MODE_INTR) {
        /* Disable the Global Interrupt Enable */
        regval = 0;
        I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_GIE, 
                          sizeof(b_u32), 0, 
                          (b_u8 *)&regval, sizeof(b_u32));
    }

    /* Wait until I2C bus is freed, exit if timed out. */
    if (_i2ccore_wait_bus_free(p_i2ccore) != CCL_OK) {
        *bytes_count = 0; 
        if (p_i2ccore->brdspec.op_mode == eDEVBRD_OP_MODE_INTR) {
            /* Enable the Global Interrupt Enable */
            regval = I2CCORE_GINTR_ENABLE_MASK;
            I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_GIE, 
                              sizeof(b_u32), 0, 
                              (b_u8 *)&regval, sizeof(b_u32));
        }
        I2CCORE_ERROR_RETURN(CCL_FAIL);
    }

	/*
	 * If it is already a master on the bus (repeated start), the direction
	 * was set to Tx which is throttling bus. The control register needs to
	 * be set before putting data into the FIFO.
	 */
    I2CCORE_REG_READ(p_i2ccore, I2CCORE_CR, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&ctrl, sizeof(b_u32));
	if (ctrl & I2CCORE_CR_MSMS_MASK) {
		ctrl &= ~I2CCORE_CR_NO_ACK_MASK;
		ctrl |= (I2CCORE_CR_DIR_IS_TX_MASK |
				I2CCORE_CR_REPEATED_START_MASK);
        I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_CR, 
                          sizeof(b_u32), 0, 
                          (b_u8 *)&ctrl, sizeof(b_u32));
	}

    /*
     * Put the address into the FIFO to be sent and indicate
     * that the operation to be performed on the bus is a
     * write operation
     */
    _ret = _i2ccore_send_7bit_address(p_i2ccore, 
                                      p_i2c_point, 
                                      I2CCORE_WRITE_OPERATION,
                                      0);
    if ( _ret ) {
        PDEBUG("_i2ccore_send_7bit_address error\n");
        I2CCORE_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

	/*
	 * Fill remaining available FIFO with message data.
	 */
	if (data_len > 1) {
		_ret = _i2ccore_tx_fifo_fill(p_i2ccore, p_data, 
                                     data_len, CCL_FALSE,
                                     CCL_FALSE, bytes_count);
        if ( _ret ) {
            PDEBUG("_i2ccore_tx_fifo_fill error\n");
            I2CCORE_ERROR_RETURN(CCL_BAD_PARAM_ERR);
        }
	}
    remaining_byte_count = data_len - *bytes_count;
    PDEBUG("p_data=%p, remaining_byte_count=%d\n", p_data, remaining_byte_count);

	/*
	 * After filling fifo, if data yet to send > 1, enable Tx half empty
	 * interrupt.
	 */
	if (remaining_byte_count > 1) {
        _ret = _i2ccore_clear_enable_intr(p_i2ccore, 
                                          I2CCORE_INTR_TX_HALF_MASK);
        if (_ret) {
            PDEBUG("_i2ccore_clear_enable_intr error\n");
            I2CCORE_ERROR_RETURN(_ret);
        }
	}

	/*
	 * Clear any pending Tx empty, Tx Error and then enable them.
	 */
    _ret = _i2ccore_clear_enable_intr(p_i2ccore, 
                                      I2CCORE_INTR_TX_ERROR_MASK |
                                      I2CCORE_INTR_TX_EMPTY_MASK);
    if (_ret) {
        PDEBUG("_i2ccore_clear_enable_intr error\n");
        I2CCORE_ERROR_RETURN(_ret);
    }
	/*
	 * When repeated start not used, MSMS must be set after putting data
	 * into transmit FIFO, start the transmitter.
	 */
    I2CCORE_REG_READ(p_i2ccore, I2CCORE_CR, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&ctrl, sizeof(b_u32));
	if ((ctrl & I2CCORE_CR_MSMS_MASK) == 0) {
		ctrl &= ~I2CCORE_CR_NO_ACK_MASK;
		ctrl |= I2CCORE_CR_MSMS_MASK | I2CCORE_CR_DIR_IS_TX_MASK;
        I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_CR, 
                          sizeof(b_u32), 0, 
                          (b_u8 *)&ctrl, sizeof(b_u32));
	}

complete_tx:
    while (1) {
        I2CCORE_REG_READ(p_i2ccore, I2CCORE_ISR, 
                         sizeof(b_u32), 0, 
                         (b_u8 *)&regval, sizeof(b_u32));
        if (regval & (I2CCORE_INTR_TX_EMPTY_MASK |
                     I2CCORE_INTR_TX_HALF_MASK )) {
            PDEBUG("p_data=%p, remaining_byte_count=%d\n", p_data, remaining_byte_count);
            if (remaining_byte_count > 1) {
                p_data += *bytes_count;
                data_len = remaining_byte_count;
                PDEBUG("*p_data=0x%02x, data_len=%d\n", *p_data, data_len);
                _ret = _i2ccore_tx_fifo_fill(p_i2ccore, p_data, 
                                             data_len, CCL_FALSE, 
                                             CCL_FALSE, bytes_count);
                if ( _ret ) {
                    PDEBUG("_i2ccore_tx_fifo_fill error\n");
                    I2CCORE_ERROR_RETURN(CCL_BAD_PARAM_ERR);
                }
                remaining_byte_count = data_len - *bytes_count;
                PDEBUG("remaining_byte_count=%d, bytes_sent=%d\n", 
                       remaining_byte_count, *bytes_count);
                if (remaining_byte_count < 2) {
                    _ret = _i2ccore_disable_intr(p_i2ccore, 
                                                 I2CCORE_INTR_TX_HALF_MASK);
                    if (_ret) {
                        PDEBUG("_i2ccore_disable_intr error\n");
                        I2CCORE_ERROR_RETURN(_ret);
                    }
                }
                goto complete_tx;
            } else if (remaining_byte_count == 1) {
                if (regval & I2CCORE_INTR_TX_EMPTY_MASK) {
                    /*
                     * Set the stop condition before sending the last byte
                     * of data so that the stop condition will be generated
                     * immediately following the data another transmit
                     * interrupt is not expected so the message is done.
                     */
                    I2CCORE_REG_READ(p_i2ccore, I2CCORE_CR, 
                                     sizeof(b_u32), 0, 
                                     (b_u8 *)&ctrl, sizeof(b_u32));
                    ctrl &= ~I2CCORE_CR_MSMS_MASK;
                    I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_CR, 
                                      sizeof(b_u32), 0, 
                                      (b_u8 *)&ctrl, sizeof(b_u32));
                    p_data += *bytes_count;
                    regval = *(b_u8 *)p_data;
                    PDEBUG("p_data=%p, data=0x%02x\n", p_data, regval);
                    I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_TX_FIFO, 
                              sizeof(b_u32), 0, 
                              (b_u8 *)&regval, sizeof(b_u32));
                    _ret = _i2ccore_disable_intr(p_i2ccore, 
                                                 I2CCORE_TX_INTERRUPTS);
                    if (_ret) {
                        PDEBUG("_i2ccore_disable_intr error\n");
                        I2CCORE_ERROR_RETURN(_ret);
                    }
                    _ret = _i2ccore_enable_intr(p_i2ccore, 
                                                I2CCORE_INTR_BNB_MASK);
                    if (_ret) {
                        PDEBUG("_i2ccore_disable_intr error\n");
                        I2CCORE_ERROR_RETURN(_ret);
                    }
                    break;
                }
            }
		}
     }

    if (p_i2ccore->brdspec.op_mode == eDEVBRD_OP_MODE_INTR) {
        /* Enable the Global Interrupt Enable */
        regval = I2CCORE_GINTR_ENABLE_MASK;
        I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_GIE, 
                          sizeof(b_u32), 0, 
                          (b_u8 *)&regval, sizeof(b_u32));
    }

    return CCL_OK;
}

/**
* This function sends data as a master on the IIC bus. If the bus is busy, it
* will indicate so and then enable an interrupt such that the status handler
* will be called when the bus is no longer busy.  The slave address which has
* been set with the XIic_SetAddress() function is the address to which the
* specific data is sent.  Sending data on the bus performs a write operation.
*
* @param[in] p_i2c_point is the address of the i2c device to 
*       send to
* @param[in] p_data points to the data to be sent.
* @param[in] data_len is the number of bytes to be sent. 
* @param[out] bytes_count is the pointer to hold the number of 
*       bytes sent
*
* @return	error code
*/
static ccl_err_t _devi2ccore_raw_dyn_write_data_fifo(i2c_route_t *p_i2c_point, 
                                                     b_u8 *p_data, 
                                                     b_u16 data_len, 
                                                     b_u32 *bytes_count)
{
    i2ccore_ctx_t *p_i2ccore;
    b_u32 remaining_byte_count;
    b_u32 regval = 0;
    b_u32 ctrl = 0;
    b_u32 status = 0;
    b_u32 cnt = 0;

    if ( !p_i2c_point || !p_data || !data_len || !bytes_count ) {
        PDEBUG("Error! Invalid input parameters: p_i2ccore=0x%x, p_data=0x%p, "
               "data_len=%d, bytes_count=0x%p\n", 
               p_i2c_point, p_data, data_len, bytes_count);
        I2CCORE_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    *bytes_count = 0;
    /* get the current smbus context */
    p_i2ccore = _p_i2ccore[p_i2c_point->bus_id];
    if (p_i2ccore->brdspec.op_mode == eDEVBRD_OP_MODE_INTR) {
        /* Disable the Global Interrupt Enable */
        regval = 0;
        I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_GIE, 
                          sizeof(b_u32), 0, 
                          (b_u8 *)&regval, sizeof(b_u32));
    }

    /* Wait until I2C bus is freed, exit if timed out. */
    if (_i2ccore_wait_bus_free(p_i2ccore) != CCL_OK) {
        *bytes_count = 0; 
        if (p_i2ccore->brdspec.op_mode == eDEVBRD_OP_MODE_INTR) {
            /* Enable the Global Interrupt Enable */
            regval = I2CCORE_GINTR_ENABLE_MASK;
            I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_GIE, 
                              sizeof(b_u32), 0, 
                              (b_u8 *)&regval, sizeof(b_u32));
        }
        I2CCORE_ERROR_RETURN(CCL_FAIL);
    }

    /*
    *  Reset TX FIFO
    */ 
    ctrl = I2CCORE_CR_ENABLE_DEVICE_MASK | 
           I2CCORE_CR_TX_FIFO_RESET_MASK;
    I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_CR, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&ctrl, sizeof(b_u32));
    ctrl &= ~I2CCORE_CR_TX_FIFO_RESET_MASK;
    I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_CR, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&ctrl, sizeof(b_u32));

    /*
     * Put the address into the FIFO to be sent and indicate
     * that the operation to be performed on the bus is a
     * write operation with dynamic start mask set
     */
    _ret = _i2ccore_send_7bit_address(p_i2ccore, 
                                      p_i2c_point, 
                                      I2CCORE_WRITE_OPERATION,
                                      I2CCORE_TX_DYN_START_MASK);
    if ( _ret ) {
        PDEBUG("_i2ccore_send_7bit_address error\n");
        I2CCORE_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

	/*
	 * Fill remaining available FIFO with message data.
	 */
	if (data_len > 1) {
		_ret = _i2ccore_tx_fifo_fill(p_i2ccore, p_data, 
                                     data_len, CCL_TRUE, 
                                     CCL_TRUE, bytes_count);
        if ( _ret ) {
            PDEBUG("_i2ccore_tx_fifo_fill error\n");
            I2CCORE_ERROR_RETURN(CCL_BAD_PARAM_ERR);
        }
	}
    remaining_byte_count = data_len - *bytes_count;
    PDEBUG("p_data=%p, data_len=%d, bytes_count=%d, remaining_byte_count=%d\n", 
           p_data, data_len, *bytes_count, remaining_byte_count);

    cnt = 0;
    while (remaining_byte_count > 0) {
        if (cnt++ > 10000) {
            I2CCORE_ERROR_RETURN(CCL_FAIL);
        }
        I2CCORE_REG_READ(p_i2ccore, I2CCORE_SR, 
                         sizeof(b_u32), 0, 
                         (b_u8 *)&regval, sizeof(b_u32));
        if (regval & I2CCORE_SR_TX_FIFO_EMPTY_MASK) {
            PDEBUG("p_data=%p, remaining_byte_count=%d\n", p_data, remaining_byte_count);
            p_data += *bytes_count;
            data_len = remaining_byte_count;
            if (p_data) 
                PDEBUG("*p_data=0x%02x, data_len=%d\n", *p_data, data_len);
            _ret = _i2ccore_tx_fifo_fill(p_i2ccore, p_data, 
                                         data_len, CCL_TRUE, 
                                         CCL_TRUE, bytes_count);
            if ( _ret ) {
                PDEBUG("_i2ccore_tx_fifo_fill error\n");
                I2CCORE_ERROR_RETURN(CCL_BAD_PARAM_ERR);
            }
            remaining_byte_count = data_len - *bytes_count;
            PDEBUG("remaining_byte_count=%d, bytes_sent=%d\n", 
                   remaining_byte_count, *bytes_count);

            if (!remaining_byte_count)
                break;
		}
     }

    if (p_i2ccore->brdspec.op_mode == eDEVBRD_OP_MODE_INTR) {
        /* Enable the Global Interrupt Enable */
        regval = I2CCORE_GINTR_ENABLE_MASK;
        I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_GIE, 
                          sizeof(b_u32), 0, 
                          (b_u8 *)&regval, sizeof(b_u32));
    }

    return CCL_OK;
}

/**
* @brief Receive the specified data from the device that has 
* been previously addressed on the IIC bus.  This function 
* assumes that the 7 bit address has been sent and it should 
* wait for the transmit of the address to complete. 
*
* @param[in] p_i2ccore is a pointer to the i2ccore_ctx_t 
*       instance to be worked on
* @param[in] p_data points to the buffer to hold the data that 
*       is received.
* @param[in] data_len is the number of bytes to be received.
* @param[in] option indicates whether to hold or free the bus
*       after reception of data, I2CCORE_STOP = end with STOP
*       condition, I2CCORE_REPEATED_START = don't end with STOP
*       condition.
* @param[out] bytes_count the pointer to hold the number of 
*       bytes remaining to be received
*
* @return	error code
*/
static ccl_err_t _i2ccore_recv_data(i2ccore_ctx_t *p_i2ccore, 
                                    b_u8 *p_data, b_u16 data_len, 
                                    b_u32 option, b_u32 *bytes_count)
{
    b_u32 regval = 0, mask = 0, ctrl = 0;
    b_u32 cnt = 0;

    if ( !p_i2ccore || !p_data || !data_len || !bytes_count) {
        PDEBUG("Error! Invalid input parameters: p_i2ccore=0x%p, p_data=0x%p, "
               "data_len=%d, bytes_count=0x%p\n", 
               p_i2ccore, p_data, data_len, bytes_count);
        I2CCORE_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* Attempt to receive the specified number of bytes on the IIC bus */

    while (data_len > 0) {
        PDEBUG("data_len=%d\n", data_len);
        /* Setup the mask to use for checking errors because when
         * receiving one byte OR the last byte of a multibyte message an
         * error naturally occurs when the no ack is done to tell the
         * slave the last byte
         */
        if (data_len == 1) {
            mask = I2CCORE_INTR_ARB_LOST_MASK | 
                   I2CCORE_INTR_BNB_MASK;
        } else {
            mask = I2CCORE_INTR_ARB_LOST_MASK | 
                   I2CCORE_INTR_TX_ERROR_MASK | 
                   I2CCORE_INTR_BNB_MASK;
        }

        /* Wait for the previous transmit and the 1st receive to
         * complete by checking the interrupt status register of the
         * IPIF
         */
        cnt = 0;
        while (1) {
            if (cnt++ > 10000) {
                I2CCORE_ERROR_RETURN(CCL_FAIL);
            }
            I2CCORE_REG_READ(p_i2ccore, I2CCORE_ISR,
                             sizeof(b_u32), 0, 
                             (b_u8 *)&regval, sizeof(b_u32));
            if (regval & I2CCORE_INTR_RX_FULL_MASK)
                break;
            /* Check the transmit error after the receive full
             * because when sending only one byte transmit error
             * will occur because of the no ack to indicate the end
             * of the data
             */
            if (regval & mask) {
                *bytes_count = data_len;
                return CCL_OK;
            }
        }

        I2CCORE_REG_READ(p_i2ccore, I2CCORE_CR, 
                         sizeof(b_u32), 0, 
                         (b_u8 *)&ctrl, sizeof(b_u32));
        /* Special conditions exist for the last two bytes so check for
         * them. Note that the control register must be setup for these
         * conditions before the data byte which was already received is
         * read from the receive FIFO (while the bus is throttled
         */
        if (data_len == 1) {
            if (option == I2CCORE_STOP) {

                /* If the Option is to release the bus after the
                 * last data byte, it has already been read and
                 * no ack has been done, so clear MSMS while
                 * leaving the device enabled so it can get off
                 * the IIC bus appropriately with a stop
                 */
                regval = I2CCORE_CR_ENABLE_DEVICE_MASK;
                I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_CR, 
                                  sizeof(b_u32), 0, 
                                  (b_u8 *)&regval, sizeof(b_u32));
            }
        }

        /* Before the last byte is received, set NOACK to tell the slave
         * IIC device that it is the end, this must be done before
         * reading the byte from the FIFO
         */
        if (data_len == 2) {
            /* Write control reg with NO ACK allowing last byte to
             * have the No ack set to indicate to slave last byte
             * read
             */
            regval = ctrl | I2CCORE_CR_NO_ACK_MASK;
            I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_CR, 
                              sizeof(b_u32), 0, 
                              (b_u8 *)&regval, sizeof(b_u32));
        }

        /* Read in data from the FIFO and unthrottle the bus such that
         * the next byte is read from the IIC bus
         */
        I2CCORE_REG_READ(p_i2ccore, I2CCORE_RX_FIFO, 
                         sizeof(b_u32), 0, 
                         (b_u8 *)&regval, sizeof(b_u32));
        *p_data++ = (b_u8)regval;

        if ((data_len == 1) && (option == I2CCORE_REPEATED_START)) {

            /* RSTA bit should be set only when the FIFO is
             * completely Empty.
             */
            regval = I2CCORE_CR_ENABLE_DEVICE_MASK | 
                     I2CCORE_CR_MSMS_MASK | 
                     I2CCORE_CR_REPEATED_START_MASK;
            I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_CR, 
                              sizeof(b_u32), 0, 
                              (b_u8 *)&regval, sizeof(b_u32));
        }

        /* Clear the latched interrupt status so that it will be updated
         * with the new state when it changes, this must be done after
         * the receive register is read
         */
        _ret = _i2ccore_clear_isr(p_i2ccore, 
                                  I2CCORE_INTR_RX_FULL_MASK | 
                                  I2CCORE_INTR_TX_ERROR_MASK | 
                                  I2CCORE_INTR_ARB_LOST_MASK);
        if (_ret) {
            PDEBUG("_i2ccore_clear_isr error\n");
            I2CCORE_ERROR_RETURN(_ret);
        }

        data_len--;
    }

    if (option == I2CCORE_STOP) {

        /* If the Option is to release the bus after Reception of data,
         * wait for the bus to transition to not busy before returning,
         * the IIC device cannot be disabled until this occurs. It
         * should transition as the MSMS bit of the control register was
         * cleared before the last byte was read from the FIFO
         */
        cnt = 0;
        while (1) {
            if (cnt++ > 10000) {
                I2CCORE_ERROR_RETURN(CCL_FAIL);
            }
            I2CCORE_REG_READ(p_i2ccore, I2CCORE_ISR, 
                             sizeof(b_u32), 0, 
                             (b_u8 *)&regval, sizeof(b_u32));
            if (regval & I2CCORE_INTR_BNB_MASK)
                break;
        }
    }

    *bytes_count = data_len;

    return CCL_OK;
}

/**
* @brief Receive data as a master on the IIC bus.  This function
* receives the data using polled I/O and blocks until the data 
* has been received. It only supports 7 bit addressing mode of 
* operation. This function returns zero if bus is busy. 
*
* @param[in] p_i2c_point is the address of the i2c device to 
*       send to
* @param[in] p_data points to the buffer to hold the data that 
*       is received.
* @param[in] data_len is the number of bytes to be received. 
* @param[out] bytes_count is the pointer to the number of bytes 
*       received
*
* @return	error code
*/
static ccl_err_t _devi2ccore_raw_read_data(i2c_route_t *p_i2c_point, 
                                           b_u32 offset, b_u32 offset_sz, 
                                           b_u8 *p_data, b_u32 data_len, 
                                           b_u32 *bytes_count)
{
    i2ccore_ctx_t *p_i2ccore;
    b_u32 remaining_byte_count;
    b_u32 regval = 0, ctrl = 0;
    b_u32 status = 0;
    b_u32 cnt = 0;

    if ( !p_i2c_point || !p_data || !data_len || !bytes_count ) {
        PDEBUG("Error! Invalid input parameters: p_i2ccore=0x%x, p_data=0x%p, "
               "data_len=%d, bytes_count=0x%p\n", 
               p_i2c_point, p_data, data_len, bytes_count);
        I2CCORE_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    *bytes_count = 0;
    /* get the current smbus context */
    p_i2ccore = _p_i2ccore[p_i2c_point->bus_id];

    /* write address and offset of i2c device */
    _ret = _i2ccore_set_offset(p_i2c_point, offset, offset_sz);
    if ( _ret ) {
        PDEBUG("Error! Can't set offset\n");
        I2CCORE_ERROR_RETURN(_ret);
    }

    /* Tx error is enabled incase the address (7 or 10) has no device to
     * answer with Ack. When only one byte of data, must set NO ACK before
     * address goes out therefore Tx error must not be enabled as it will go
     * off immediately and the Rx full interrupt will be checked.  If full,
     * then the one byte was received and the Tx error will be disabled
     * without sending an error callback msg
     */
    _ret = _i2ccore_clear_isr(p_i2ccore, 
                              I2CCORE_INTR_RX_FULL_MASK | 
                              I2CCORE_INTR_TX_ERROR_MASK | 
                              I2CCORE_INTR_ARB_LOST_MASK);
    if (_ret) {
        PDEBUG("_i2ccore_clear_isr error\n");
        I2CCORE_ERROR_RETURN(_ret);
    }

    /* Set receive FIFO occupancy depth for 1 byte (zero based) */
    regval = 0;
    I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_RX_FIFO_PIRQ, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));

    /* Check to see if already Master on the Bus.
     * If Repeated Start bit is not set send Start bit by setting MSMS bit
     * else Send the address
     */
    I2CCORE_REG_READ(p_i2ccore, I2CCORE_CR, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&ctrl, sizeof(b_u32));
    if ((ctrl & I2CCORE_CR_REPEATED_START_MASK) == 0) {
        /* 7 bit slave address, send the address for a read operation
         * and set the state to indicate the address has been sent
         */
        _ret = _i2ccore_send_7bit_address(p_i2ccore, 
                                          p_i2c_point, 
                                          I2CCORE_READ_OPERATION,
                                          0);
        if (_ret) {
            PDEBUG("_i2ccore_send_7bit_address error\n");
            I2CCORE_ERROR_RETURN(_ret);
        }


        /* MSMS gets set after putting data in FIFO. Start the master
         * receive operation by setting CR Bits MSMS to Master, if the
         * buffer is only one byte, then it should not be acknowledged
         * to indicate the end of data
         */
        ctrl = I2CCORE_CR_MSMS_MASK | 
               I2CCORE_CR_ENABLE_DEVICE_MASK;
        if (data_len == 1) {
            ctrl |= I2CCORE_CR_NO_ACK_MASK;
        }

        /* Write out the control register to start receiving data and
         * call the function to receive each byte into the buffer
         */
        I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_CR, 
                          sizeof(b_u32), 0, 
                          (b_u8 *)&ctrl, sizeof(b_u32));

        /* Clear the latched interrupt status for the bus not busy bit
         * which must be done while the bus is busy
         */
        I2CCORE_REG_READ(p_i2ccore, I2CCORE_SR, 
                         sizeof(b_u32), 0, 
                         (b_u8 *)&status, sizeof(b_u32));
        cnt = 0;
        while ((status & I2CCORE_SR_BUS_BUSY_MASK) == 0) {
            if (cnt++ > 10000) {
                I2CCORE_ERROR_RETURN(CCL_FAIL);
            }
            I2CCORE_REG_READ(p_i2ccore, I2CCORE_SR, 
                             sizeof(b_u32), 0, 
                             (b_u8 *)&status, sizeof(b_u32));
        }

        _ret = _i2ccore_clear_isr(p_i2ccore, I2CCORE_INTR_BNB_MASK);
        if (_ret) {
            PDEBUG("_i2ccore_clear_isr error\n");
            I2CCORE_ERROR_RETURN(_ret);
        }

    } else {
        /* Before writing 7bit slave address the Direction of Tx bit
     * must be disabled
     */
        ctrl &= ~I2CCORE_CR_DIR_IS_TX_MASK;
        if (data_len == 1)
            ctrl |= I2CCORE_CR_NO_ACK_MASK;

        I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_CR, 
                          sizeof(b_u32), 0, 
                          (b_u8 *)&ctrl, sizeof(b_u32));
        /* Already owns the Bus indicating that its a Repeated Start
         * call. 7 bit slave address, send the address for a read
         * operation and set the state to indicate the address has been
         * sent
         */
        _ret = _i2ccore_send_7bit_address(p_i2ccore, 
                                          p_i2c_point, 
                                          I2CCORE_READ_OPERATION,
                                          0);
        if (_ret) {
            PDEBUG("_i2ccore_send_7bit_address error\n");
            I2CCORE_ERROR_RETURN(_ret);
        }
    }

    /* Try to receive the data from the IIC bus */
    _ret = _i2ccore_recv_data(p_i2ccore, p_data, 
                              data_len, I2CCORE_STOP, 
                              &remaining_byte_count);
    if (_ret) {
        PDEBUG("_i2ccore_recv_data error\n");
        I2CCORE_ERROR_RETURN(_ret);
    }

    I2CCORE_REG_READ(p_i2ccore, I2CCORE_CR, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&ctrl, sizeof(b_u32));
    if ((ctrl & I2CCORE_CR_REPEATED_START_MASK) == 0) {
        /* The receive is complete, disable the IIC device if the Option
         * is to release the Bus after Reception of data and return the
         * number of bytes that was received
         */
        regval = 0;
        I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_CR, 
                          sizeof(b_u32), 0, 
                          (b_u8 *)&ctrl, sizeof(b_u32));
    }
    /* Wait until I2C bus is freed, exit if timed out. */
    if (_i2ccore_wait_bus_free(p_i2ccore) != CCL_OK) {
        *bytes_count = 0;
        return CCL_OK;
    }

    /* Return the number of bytes that was received */
    *bytes_count = data_len - remaining_byte_count;
    PDEBUG("received bytes=%d\n", *bytes_count);

    return CCL_OK;
}

/*****************************************************************************/
/**
* This function receives data as a master from a slave device on the IIC bus.
* If the bus is busy, it will indicate so and then enable an interrupt such
* that the status handler will be called when the bus is no longer busy.  The
* slave address which has been set with the XIic_SetAddress() function is the
* address from which data is received. Receiving data on the bus performs a
* read operation.
*
* @param	InstancePtr is a pointer to the Iic instance to be worked on.
* @param	RxMsgPtr is a pointer to the data to be transmitted
* @param	ByteCount is the number of message bytes to be sent
*
* @return
*		- XST_SUCCESS indicates the message reception processes has
*		been initiated.
*		- XST_IIC_BUS_BUSY indicates the bus was in use and that the
*		BusNotBusy interrupt is enabled which will update the
*		EventStatus when the bus is no longer busy.
*		- XST_IIC_GENERAL_CALL_ADDRESS indicates the slave address
*		is set to the the general call address. This is not allowed
*		for Master receive mode.
*
* @internal
*
* The receive FIFO threshold is a zero based count such that 1 must be
* subtracted from the desired count to get the correct value. When receiving
* data it is also necessary to not receive the last byte with the prior bytes
* because the acknowledge must be setup before the last byte is received.
*
******************************************************************************/
static ccl_err_t _devi2ccore_raw_read_data_fifo(i2c_route_t *p_i2c_point, 
                                                b_u32 offset, b_u32 offset_sz, 
                                                b_u8 *p_data, b_u32 data_len, 
                                                b_u32 *bytes_count)
{
    i2ccore_ctx_t *p_i2ccore;
    b_u32 remaining_byte_count;
    b_u32 bytes_in_fifo;
    b_u32 bytes_to_read;
    b_u32 regval = 0, ctrl = 0;
    b_u32 status = 0;
    b_u32 i;
    b_u32 cnt = 0;

    if ( !p_i2c_point || !p_data || !data_len || !bytes_count ) {
        PDEBUG("Error! Invalid input parameters: p_i2ccore=0x%x, p_data=0x%p, "
               "data_len=%d, bytes_count=0x%p\n", 
               p_i2c_point, p_data, data_len, bytes_count);
        I2CCORE_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    *bytes_count = 0;
    /* get the current smbus context */
    p_i2ccore = _p_i2ccore[p_i2c_point->bus_id];

    /* write address and offset of i2c device */
    _ret = _i2ccore_set_offset(p_i2c_point, offset, offset_sz);
    if ( _ret ) {
        PDEBUG("Error! Can't set offset\n");
        I2CCORE_ERROR_RETURN(_ret);
    }

    if (p_i2ccore->brdspec.op_mode == eDEVBRD_OP_MODE_INTR) {
        /* Disable the Global Interrupt Enable */
        regval = 0;
        I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_GIE, 
                          sizeof(b_u32), 0, 
                          (b_u8 *)&regval, sizeof(b_u32));
    }
#if 0
    /* Wait until I2C bus is freed, exit if timed out. */
    if (_i2ccore_wait_bus_free(p_i2ccore) != CCL_OK) {
        *bytes_count = 0; 
        if (p_i2ccore->brdspec.op_mode == eDEVBRD_OP_MODE_INTR) {
            /* Enable the Global Interrupt Enable */
            regval = I2CCORE_GINTR_ENABLE_MASK;
            I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_GIE, 
                              sizeof(b_u32), 0, 
                              (b_u8 *)&regval, sizeof(b_u32));
        }
        I2CCORE_ERROR_RETURN(CCL_FAIL);
    }
#endif
    _ret = _i2ccore_clear_enable_intr(p_i2ccore, 
                                      I2CCORE_INTR_RX_FULL_MASK);
    if (_ret) {
        PDEBUG("_i2ccore_clear_enable_intr error\n");
        I2CCORE_ERROR_RETURN(_ret);
    }

	/*
	 * If already a master on the bus, the direction was set by Rx Interrupt
	 * routine to Tx which is throttling bus because during Rxing, Tx reg is
	 * empty = throttle. CR needs setting before putting data or the address
	 * written will go out as Tx instead of receive. Start Master Rx by
	 * setting CR Bits MSMS to Master and msg direction.
	 */
    I2CCORE_REG_READ(p_i2ccore, I2CCORE_CR, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&ctrl, sizeof(b_u32));

	if (ctrl & I2CCORE_CR_MSMS_MASK) {
		ctrl |= I2CCORE_CR_REPEATED_START_MASK;
        ctrl &= ~(I2CCORE_CR_NO_ACK_MASK | I2CCORE_CR_DIR_IS_TX_MASK);
		if (data_len == 1) 
            ctrl |= I2CCORE_CR_NO_ACK_MASK;
        I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_CR, 
                          sizeof(b_u32), 0, 
                          (b_u8 *)&ctrl, sizeof(b_u32));
	}

	/*
	 * Set receive FIFO occupancy depth which must be done prior to writing
	 * the address in the FIFO because the transmitter will immediatedly
	 * start when in repeated start mode followed by the receiver such that
	 * the number of  bytes to receive should be set 1st.
	 */
	if (data_len == 1) {
		regval = 0;
	} else {
		if (data_len <= I2CCORE_RX_FIFO_DEPTH) {
			regval = data_len - 2;
		} else {
			regval = I2CCORE_RX_FIFO_DEPTH - 1;
		}
	}
    PDEBUG("rx_fifo_pirq_depth=%d\n",regval);
    I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_RX_FIFO_PIRQ, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));
    
    /*
     * 7 bit slave address, send the address for a read operation
     * and set the state to indicate the address has been sent.
     */
    _ret = _i2ccore_send_7bit_address(p_i2ccore, 
                                      p_i2c_point, 
                                      I2CCORE_READ_OPERATION,
                                      0);
    if (_ret) {
        PDEBUG("_i2ccore_send_7bit_address error\n");
        I2CCORE_ERROR_RETURN(_ret);
    }

	/*
	 * Tx error is enabled incase the address (7 or 10) has no device to
	 * answer with Ack. When only one byte of data, must set NO ACK before
	 * address goes out therefore Tx error must not be enabled as it will
	 * go off immediately and the Rx full interrupt will be checked.
	 * If full, then the one byte was received and the Tx error will be
	 * disabled without sending an error callback msg.
	 */
    _ret = _i2ccore_clear_enable_intr(p_i2ccore, 
                                      I2CCORE_INTR_TX_ERROR_MASK);
    if (_ret) {
        PDEBUG("_i2ccore_clear_enable_intr error\n");
        I2CCORE_ERROR_RETURN(_ret);
    }

	/*
	 * When repeated start not used, MSMS gets set after putting data
	 * in Tx reg. Start Master Rx by setting CR Bits MSMS to Master and
	 * msg direction.
	 */
    I2CCORE_REG_READ(p_i2ccore, I2CCORE_CR, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&ctrl, sizeof(b_u32));
	if ((ctrl & I2CCORE_CR_MSMS_MASK) == 0) {
		ctrl |= I2CCORE_CR_MSMS_MASK;
        if (data_len == 1) 
            ctrl |= I2CCORE_CR_NO_ACK_MASK;
        I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_CR, 
                          sizeof(b_u32), 0, 
                          (b_u8 *)&ctrl, sizeof(b_u32));
	}

complete_rx:
    cnt = 0;
    while (1) {
        if (cnt++ > 10000) {
            I2CCORE_ERROR_RETURN(CCL_FAIL);
        }
        I2CCORE_REG_READ(p_i2ccore, I2CCORE_ISR, 
                         sizeof(b_u32), 0, 
                         (b_u8 *)&regval, sizeof(b_u32));

        if (regval & I2CCORE_INTR_RX_FULL_MASK) {
            /*
             * Device is a master receiving, get the contents of the control
             * register and determine the number of bytes in fifo to be read out.
             */
            I2CCORE_REG_READ(p_i2ccore, I2CCORE_CR, 
                             sizeof(b_u32), 0, 
                             (b_u8 *)&ctrl, sizeof(b_u32));
            I2CCORE_REG_READ(p_i2ccore, I2CCORE_RX_FIFO_OCY, 
                             sizeof(b_u32), 0, 
                             (b_u8 *)&bytes_in_fifo, sizeof(b_u32));
            bytes_in_fifo += 1;
            PDEBUG("data_len=%d, bytes_in_fifo=%d\n", data_len, bytes_in_fifo);
            /*
             * If data in FIFO holds all data to be retrieved - 1, set NOACK and
             * disable the Tx error.
             */
            if (data_len < bytes_in_fifo) {
                ccl_syslog_err("Error! data_len (%u) < bytes_in_fifo (%u)\n",
                               data_len, bytes_in_fifo);
                I2CCORE_ERROR_RETURN(CCL_FAIL);
            }
            if ((data_len - bytes_in_fifo) == 1) {
                /*
                 * Disable Tx error interrupt to prevent interrupt
                 * as this device will cause it when it set NO ACK next.
                 */

                _ret = _i2ccore_disable_intr(p_i2ccore, I2CCORE_INTR_TX_ERROR_MASK);
                if (_ret) {
                    PDEBUG("_i2ccore_disable_intr error\n");
                    I2CCORE_ERROR_RETURN(_ret);
                }
                _ret = _i2ccore_clear_isr(p_i2ccore, I2CCORE_INTR_TX_ERROR_MASK);
                if (_ret) {
                    PDEBUG("_i2ccore_clear_isr error\n");
                    I2CCORE_ERROR_RETURN(_ret);
                }
                /*
                 * Write control reg with NO ACK allowing last byte to
                 * have the No ack set to indicate to slave last byte read.
                 */
                ctrl |= I2CCORE_CR_NO_ACK_MASK;
                I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_CR, 
                                  sizeof(b_u32), 0, 
                                  (b_u8 *)&ctrl, sizeof(b_u32));
                /*
                 * Read one byte to clear a place for the last byte to be read
                 * which will set the NO ACK.
                 */
                I2CCORE_REG_READ(p_i2ccore, I2CCORE_RX_FIFO, 
                                 sizeof(b_u32), 0, 
                                 (b_u8 *)&regval, sizeof(b_u32));
                *p_data++ = (b_u8)regval;
                data_len--;
                *bytes_count = 1;
                goto complete_rx;
            } else if ((data_len - bytes_in_fifo) == 0) {
                /*
                 * If repeated start option is off then the master should stop
                 * using the bus, otherwise hold the bus, setting repeated start
                 * stops the slave from transmitting data when the FIFO is read.
                 */
                ctrl &= ~I2CCORE_CR_MSMS_MASK;
                I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_CR, 
                                  sizeof(b_u32), 0, 
                                  (b_u8 *)&ctrl, sizeof(b_u32));

                /*
                 * Read data from the FIFO then set zero based FIFO read depth
                 * for a byte.
                 */
                for (i = 0; i < bytes_in_fifo; i++) {
                    I2CCORE_REG_READ(p_i2ccore, I2CCORE_RX_FIFO, 
                                     sizeof(b_u32), 0, 
                                     (b_u8 *)&regval, sizeof(b_u32));
                    *p_data++ = (b_u8)regval;
                    data_len--;
                    *bytes_count += 1;
                }
                I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_RX_FIFO_PIRQ, 
                                  sizeof(b_u32), 0, 
                                  (b_u8 *)&ctrl, sizeof(b_u32));
        		/*
        		 * Disable Rx full interrupt and write the control reg with ACK
        		 * allowing next byte sent to be acknowledged automatically.
        		 */
                _ret = _i2ccore_disable_intr(p_i2ccore, I2CCORE_INTR_RX_FULL_MASK);
                if (_ret) {
                    PDEBUG("_i2ccore_disable_intr error\n");
                    I2CCORE_ERROR_RETURN(_ret);
                }
                ctrl &= ~I2CCORE_CR_NO_ACK_MASK;
                I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_CR, 
                                  sizeof(b_u32), 0, 
                                 (b_u8 *)&ctrl, sizeof(b_u32));
                break;
            } else {
                /*
                 * Fifo data not at n-1, read all but the last byte of data
                 * from the slave, if more than a FIFO full yet to receive
                 * read a FIFO full.
                 */
                bytes_to_read = data_len - bytes_in_fifo - 1;
                if (bytes_to_read > I2CCORE_RX_FIFO_DEPTH) 
                    bytes_to_read = I2CCORE_RX_FIFO_DEPTH;

                /*
                 * Read in data from the FIFO.
                 */
                for (i = 0; i < bytes_to_read; i++) {
                    I2CCORE_REG_READ(p_i2ccore, I2CCORE_RX_FIFO, 
                                     sizeof(b_u32), 0, 
                                     (b_u8 *)&regval, sizeof(b_u32));
                    *p_data++ = (b_u8)regval;
                    data_len--;
                    *bytes_count += 1;
                }
            }
        }
    }
    if (p_i2ccore->brdspec.op_mode == eDEVBRD_OP_MODE_INTR) {
        /* Enable the Global Interrupt Enable */
        regval = I2CCORE_GINTR_ENABLE_MASK;
        I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_GIE, 
                          sizeof(b_u32), 0, 
                          (b_u8 *)&regval, sizeof(b_u32));
    }

    return CCL_OK;
}

/*****************************************************************************/
/**
* This function receives data as a master from a slave device on the IIC bus.
* If the bus is busy, it will indicate so and then enable an interrupt such
* that the status handler will be called when the bus is no longer busy.  The
* slave address which has been set with the XIic_SetAddress() function is the
* address from which data is received. Receiving data on the bus performs a
* read operation.
*
* @param	InstancePtr is a pointer to the Iic instance to be worked on.
* @param	RxMsgPtr is a pointer to the data to be transmitted
* @param	ByteCount is the number of message bytes to be sent
*
* @return
*		- XST_SUCCESS indicates the message reception processes has
*		been initiated.
*		- XST_IIC_BUS_BUSY indicates the bus was in use and that the
*		BusNotBusy interrupt is enabled which will update the
*		EventStatus when the bus is no longer busy.
*		- XST_IIC_GENERAL_CALL_ADDRESS indicates the slave address
*		is set to the the general call address. This is not allowed
*		for Master receive mode.
*
* @internal
*
* The receive FIFO threshold is a zero based count such that 1 must be
* subtracted from the desired count to get the correct value. When receiving
* data it is also necessary to not receive the last byte with the prior bytes
* because the acknowledge must be setup before the last byte is received.
*
******************************************************************************/
static ccl_err_t _devi2ccore_raw_dyn_read_data_fifo(i2c_route_t *p_i2c_point, 
                                                    b_u32 offset, b_u32 offset_sz, 
                                                    b_u8 *p_data, b_u32 data_len, 
                                                    b_u32 *bytes_count)
{
    i2ccore_ctx_t *p_i2ccore;
    b_u32 remaining_byte_count;
    b_u32 bytes_in_fifo;
    b_u32 bytes_to_read;
    b_u32 regval = 0, ctrl = 0;
    b_u32 status = 0;
    b_u8 write_buff[sizeof(b_u32)];
    b_u32 i, j;
    b_u32 cnt = 0;

    if ( !p_i2c_point || !p_data || !data_len || !bytes_count ) {
        PDEBUG("Error! Invalid input parameters: p_i2ccore=0x%x, p_data=0x%p, "
               "data_len=%d, bytes_count=0x%p\n", 
               p_i2c_point, p_data, data_len, bytes_count);
        I2CCORE_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    *bytes_count = 0;

    /* get the current smbus context */
    p_i2ccore = _p_i2ccore[p_i2c_point->bus_id];

    if (p_i2ccore->brdspec.op_mode == eDEVBRD_OP_MODE_INTR) {
        /* Disable the Global Interrupt Enable */
        regval = 0;
        I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_GIE, 
                          sizeof(b_u32), 0, 
                          (b_u8 *)&regval, sizeof(b_u32));
    }

    /* Wait until I2C bus is freed, exit if timed out. */
    if (_i2ccore_wait_bus_free(p_i2ccore) != CCL_OK) {
        *bytes_count = 0; 
        if (p_i2ccore->brdspec.op_mode == eDEVBRD_OP_MODE_INTR) {
            /* Enable the Global Interrupt Enable */
            regval = I2CCORE_GINTR_ENABLE_MASK;
            I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_GIE, 
                              sizeof(b_u32), 0, 
                              (b_u8 *)&regval, sizeof(b_u32));
        }
        I2CCORE_ERROR_RETURN(CCL_FAIL);
    }

    /*
     * Reset TX FIFO
     */ 
    ctrl = I2CCORE_CR_ENABLE_DEVICE_MASK | 
           I2CCORE_CR_TX_FIFO_RESET_MASK;
    I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_CR, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&ctrl, sizeof(b_u32));
    ctrl &= ~I2CCORE_CR_TX_FIFO_RESET_MASK;
    I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_CR, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&ctrl, sizeof(b_u32));

    regval = I2CCORE_RX_FIFO_DEPTH - 1;
    I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_RX_FIFO_PIRQ, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));
    PDEBUG("rx_fifo_pirq_depth=%d\n",regval);

    /*
     * Put the address into the FIFO to be sent and indicate
     * that the operation to be performed on the bus is a
     * write operation with dynamic start mask set
     */
    _ret = _i2ccore_send_7bit_address(p_i2ccore, 
                                      p_i2c_point, 
                                      I2CCORE_WRITE_OPERATION,
                                      I2CCORE_TX_DYN_START_MASK);
    if ( _ret ) {
        PDEBUG("_i2ccore_send_7bit_address error\n");
        I2CCORE_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* prepare write buffer's offset prefix */    
    for (i = 0; i < offset_sz; i++) {
        j = (offset_sz - i - 1) * 8;
        write_buff[i] = (offset >> j) & 0xff;
    }
   
    _ret = _i2ccore_tx_fifo_fill(p_i2ccore, write_buff, 
                                 offset_sz, CCL_TRUE, 
                                 CCL_FALSE, bytes_count);
    if ( _ret ) {
        PDEBUG("_i2ccore_tx_fifo_fill error\n");
        I2CCORE_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
       
    /*
     * 7 bit slave address, send the address for a read operation
     * and set the state to indicate the address has been sent.
     */
    _ret = _i2ccore_send_7bit_address(p_i2ccore, 
                                      p_i2c_point, 
                                      I2CCORE_READ_OPERATION,
                                      I2CCORE_TX_DYN_START_MASK);
    if (_ret) {
        PDEBUG("_i2ccore_send_7bit_address error\n");
        I2CCORE_ERROR_RETURN(_ret);
    }

    /* 
     * Set in TX FIFO Stop bit and the number of bytes to be received 
     */
    regval = I2CCORE_TX_DYN_STOP_MASK | data_len;

    I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_TX_FIFO, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));

    cnt = 0;
    while (1) {
        if (cnt++ > 10000) {
            I2CCORE_ERROR_RETURN(CCL_FAIL);
        }
        I2CCORE_REG_READ(p_i2ccore, I2CCORE_SR, 
                         sizeof(b_u32), 0, 
                         (b_u8 *)&regval, sizeof(b_u32));
        b_u32 ctrl;
        I2CCORE_REG_READ(p_i2ccore, I2CCORE_CR, 
                         sizeof(b_u32), 0, 
                         (b_u8 *)&ctrl, sizeof(b_u32));
        if (!(regval & I2CCORE_SR_RX_FIFO_EMPTY_MASK)) {
            /*
             * Read one byte to clear a place for the last byte to be read
             * which will set the NO ACK.
             */
            I2CCORE_REG_READ(p_i2ccore, I2CCORE_RX_FIFO, 
                             sizeof(b_u32), 0, 
                             (b_u8 *)&regval, sizeof(b_u32));
            *p_data++ = (b_u8)regval;
            data_len--;
            *bytes_count += 1;
            if (!data_len) 
                break;
        }
    }

    if (p_i2ccore->brdspec.op_mode == eDEVBRD_OP_MODE_INTR) {
        /* Enable the Global Interrupt Enable */
        regval = I2CCORE_GINTR_ENABLE_MASK;
        I2CCORE_REG_WRITE(p_i2ccore, I2CCORE_GIE, 
                          sizeof(b_u32), 0, 
                          (b_u8 *)&regval, sizeof(b_u32));
    }

    return CCL_OK;
}

static ccl_err_t _i2ccore_configure_test(void *p_priv)
{
    i2ccore_ctx_t *p_i2ccore;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        I2CCORE_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_i2ccore = (i2ccore_ctx_t *)p_priv;

    return CCL_OK;
}

static ccl_err_t _i2ccore_run_test(void *p_priv, 
                                   __attribute__((unused)) b_u8 *buf, 
                                   __attribute__((unused)) b_u32 size)
{
    i2ccore_ctx_t *p_i2ccore;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        I2CCORE_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_i2ccore = (i2ccore_ctx_t *)p_priv;

    return CCL_OK;
}

ccl_err_t i2ccore_attr_read(void *p_priv, b_u32 attrid, 
                            b_u32 offset, b_u32 offset_sz, 
                            b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || offset > I2CCORE_MAX_OFFSET || 
         !p_value || idx || size != sizeof(b_u32) ) {
        PDEBUG("Error! Invalid input parameters\n");
        I2CCORE_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch (attrid) {
    case eI2CCORE_GIE:
    case eI2CCORE_ISR:
    case eI2CCORE_IER:
    case eI2CCORE_SOFTR:
    case eI2CCORE_CR:
    case eI2CCORE_SR:
    case eI2CCORE_TX_FIFO:
    case eI2CCORE_RX_FIFO:
    case eI2CCORE_ADR:
    case eI2CCORE_TX_FIFO_OCY:
    case eI2CCORE_RX_FIFO_OCY:
    case eI2CCORE_TEN_ADR:
    case eI2CCORE_RX_FIFO_PIRQ:
    case eI2CCORE_GPO:
        I2CCORE_REG_READ(p_priv, offset, offset_sz, idx, p_value, size);
        break;
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK)
        PDEBUG("Failure in attribute id: %d\n", attrid);

    return _ret;               

}

ccl_err_t i2ccore_attr_write(void *p_priv, b_u32 attrid, 
                             b_u32 offset, b_u32 offset_sz, 
                             b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || offset > I2CCORE_MAX_OFFSET || 
         !p_value || idx || size != sizeof(b_u32) ) {
        PDEBUG("Error! Invalid input parameters\n");
        I2CCORE_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch (attrid) {
    case eI2CCORE_GIE:
    case eI2CCORE_ISR:
    case eI2CCORE_IER:
    case eI2CCORE_SOFTR:
    case eI2CCORE_CR:
    case eI2CCORE_SR:
    case eI2CCORE_TX_FIFO:
    case eI2CCORE_RX_FIFO:
    case eI2CCORE_ADR:
    case eI2CCORE_TX_FIFO_OCY:
    case eI2CCORE_RX_FIFO_OCY:
    case eI2CCORE_TEN_ADR:
    case eI2CCORE_RX_FIFO_PIRQ:
    case eI2CCORE_GPO:
        I2CCORE_REG_WRITE(p_priv, offset, offset_sz, idx, p_value, size);
        break;
    case eI2CCORE_INIT:
        _ret = _i2ccore_init(p_priv);
        break;
    case eI2CCORE_RESET:
        _ret = _i2ccore_reset(p_priv);
        break;
    case eI2CCORE_CONFIGURE_TEST:
        _ret = _i2ccore_configure_test(p_priv);
        break;
    case eI2CCORE_RUN_TEST:
        _ret = _i2ccore_run_test(p_priv, p_value, size);
        break;
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK)
        PDEBUG("Failure in attribute id: %d\n", attrid);

    return _ret;               

}

/* i2ccore device attributes definition */
static device_attr_t _attrs[] = {
    {
        .name = "name", 
        .type = eDEVMAN_ATTR_STRING, .maxnum = 1, 
        .offset = offsetof(struct i2ccore_ctx_t, brdspec.name),
        .size = DEVMAN_DEVNAME_LEN,
        .format = "%s"
    },
    {
        .name = "offset", 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset = offsetof(struct i2ccore_ctx_t, brdspec.offset),
        .format = "0x%08x"
    },
    {
        .name = "dev_id", 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset = offsetof(struct i2ccore_ctx_t, brdspec.dev_id),
        .format = "%d"
    },
    {
        .name = "slaves_num", 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = DEVBRD_I2CBUSES_MAXNUM, 
        .offset = offsetof(struct i2ccore_ctx_t, brdspec.slaves_num),
        .format = "%d"
    },
    {
        .name = "slave_name_bus0", 
        .type = eDEVMAN_ATTR_STRING, .maxnum = DEVBRD_I2CDEVS_MAXNUM, 
        .offset = offsetof(struct i2ccore_ctx_t, brdspec.slave_name[0]),
        .size = DEVBRD_I2CDEVS_NAMELEN,
        .format = "%s"
    },
    {
        .name = "slave_name_bus1", 
        .type = eDEVMAN_ATTR_STRING, .maxnum = DEVBRD_I2CDEVS_MAXNUM, 
        .offset = offsetof(struct i2ccore_ctx_t, brdspec.slave_name[1]),
        .size = DEVBRD_I2CDEVS_NAMELEN,
        .format = "%s"
    },
    {
        .name = "slave_name_bus2", 
        .type = eDEVMAN_ATTR_STRING, .maxnum = DEVBRD_I2CDEVS_MAXNUM, 
        .offset = offsetof(struct i2ccore_ctx_t, brdspec.slave_name[2]),
        .size = DEVBRD_I2CDEVS_NAMELEN,
        .format = "%s"
    },
    {
        .name = "slave_name_bus3", 
        .type = eDEVMAN_ATTR_STRING, .maxnum = DEVBRD_I2CDEVS_MAXNUM, 
        .offset = offsetof(struct i2ccore_ctx_t, brdspec.slave_name[3]),
        .size = DEVBRD_I2CDEVS_NAMELEN,
        .format = "%s"
    },
    {
        .name = "slave_name_bus4", 
        .type = eDEVMAN_ATTR_STRING, .maxnum = DEVBRD_I2CDEVS_MAXNUM, 
        .offset = offsetof(struct i2ccore_ctx_t, brdspec.slave_name[4]),
        .size = DEVBRD_I2CDEVS_NAMELEN,
        .format = "%s"
    },
    {
        .name = "slave_i2ca_bus0", 
        .type = eDEVMAN_ATTR_HALF, .maxnum = DEVBRD_I2CDEVS_MAXNUM, 
        .offset = offsetof(struct i2ccore_ctx_t, brdspec.slave_i2ca[0]),
        .format = "0x%02x"
    },
    {
        .name = "slave_i2ca_bus1", 
        .type = eDEVMAN_ATTR_HALF, .maxnum = DEVBRD_I2CDEVS_MAXNUM, 
        .offset = offsetof(struct i2ccore_ctx_t, brdspec.slave_i2ca[1]),
        .format = "0x%02x"
    },
    {
        .name = "slave_i2ca_bus2", 
        .type = eDEVMAN_ATTR_HALF, .maxnum = DEVBRD_I2CDEVS_MAXNUM, 
        .offset = offsetof(struct i2ccore_ctx_t, brdspec.slave_i2ca[2]),
        .format = "0x%02x"
    },
    {
        .name = "slave_i2ca_bus3", 
        .type = eDEVMAN_ATTR_HALF, .maxnum = DEVBRD_I2CDEVS_MAXNUM, 
        .offset = offsetof(struct i2ccore_ctx_t, brdspec.slave_i2ca[3]),
        .format = "0x%02x"
    },
    {
        .name = "slave_i2ca_bus4", 
        .type = eDEVMAN_ATTR_HALF, .maxnum = DEVBRD_I2CDEVS_MAXNUM, 
        .offset = offsetof(struct i2ccore_ctx_t, brdspec.slave_i2ca[4]),
        .format = "0x%02x"
    },
    {
        .name = "gie", .id = eI2CCORE_GIE, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = I2CCORE_GIE
    },
    {
        .name = "isr", .id = eI2CCORE_ISR, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = I2CCORE_ISR
    },
    {
        .name = "ier", .id = eI2CCORE_IER, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = I2CCORE_IER
    },
    {
        .name = "softr", .id = eI2CCORE_SOFTR, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = I2CCORE_SOFTR
    },
    {
        .name = "cr", .id = eI2CCORE_CR, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = I2CCORE_CR
    },
    {
        .name = "sr", .id = eI2CCORE_SR, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = I2CCORE_SR
    },
    {
        .name = "tx_fifo", .id = eI2CCORE_TX_FIFO, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = I2CCORE_TX_FIFO
    },
    {
        .name = "rx_fifo", .id = eI2CCORE_RX_FIFO, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = I2CCORE_RX_FIFO
    },
    {
        .name = "adr", .id = eI2CCORE_ADR, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = I2CCORE_ADR
    },
    {
        .name = "tx_fifo_ocy", .id = eI2CCORE_TX_FIFO_OCY, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = I2CCORE_TX_FIFO_OCY
    },
    {
        .name = "rx_fifo_ocy", .id = eI2CCORE_RX_FIFO_OCY, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = I2CCORE_RX_FIFO_OCY
    },
    {
        .name = "ten_adr", .id = eI2CCORE_TEN_ADR, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = I2CCORE_TEN_ADR
    },
    {
        .name = "rx_fifo_pirq", .id = eI2CCORE_RX_FIFO_PIRQ, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = I2CCORE_RX_FIFO_PIRQ
    },
    {
        .name = "gpo", .id = eI2CCORE_GPO, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = I2CCORE_GPO
    },
    {
        .name = "init", .id = eI2CCORE_INIT, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "reset", .id = eI2CCORE_RESET, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "config_test", .id = eI2CCORE_CONFIGURE_TEST, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "run_test", .id = eI2CCORE_RUN_TEST, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, 
        .size = sizeof(board_dev_stats_t), 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
};

static ccl_err_t _fi2ccore_cmd_reset(devo_t devo, 
                                     __attribute__((unused)) device_cmd_parm_t parms[], 
                                     __attribute__((unused)) b_u16 parms_num)
{
    void *p_priv = devman_device_private(devo);

    /* reset the device */
    _ret = _i2ccore_reset(p_priv);
    if ( _ret ) {
        PDEBUG("_i2ccore_reset error\n");
        I2CCORE_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

static ccl_err_t _fi2ccore_cmd_start(devo_t devo, 
                                     __attribute__((unused)) device_cmd_parm_t parms[], 
                                     __attribute__((unused)) b_u16 parms_num)
{
    void *p_priv = devman_device_private(devo);

    /* start the device */
    _ret = _i2ccore_start(p_priv);
    if ( _ret ) {
        PDEBUG("_i2ccore_start error\n");
        I2CCORE_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

static ccl_err_t _fi2ccore_cmd_init(devo_t devo, 
                                    __attribute__((unused)) device_cmd_parm_t parms[], 
                                    __attribute__((unused)) b_u16 parms_num)
{
    void *p_priv = devman_device_private(devo);

    _ret = _i2ccore_init(p_priv);
    if ( _ret ) {
        PDEBUG("_i2ccore_init error\n");
        I2CCORE_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

static ccl_err_t _fi2ccore_cmd_stop(devo_t devo, 
                                    __attribute__((unused)) device_cmd_parm_t parms[], 
                                    __attribute__((unused)) b_u16 parms_num)
{
    void *p_priv = devman_device_private(devo);

    /* start the device */
    _ret = _i2ccore_stop(p_priv);
    if ( _ret ) {
        PDEBUG("_i2ccore_stop error\n");
        I2CCORE_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

/* device commands */
static device_cmd_t _cmds[] = 
{
    { 
        .name = "reset", .f_cmd = _fi2ccore_cmd_reset, .parms_num = 0,
        .help = "Reset Xilinx I2C Bus Interface device" 
    },
    { 
        .name = "start", .f_cmd = _fi2ccore_cmd_start, .parms_num = 0,
        .help = "Start Xilinx I2C Bus Interface device" 
    },
    { 
        .name = "init", .f_cmd = _fi2ccore_cmd_init, .parms_num = 0,
        .help = "Init Xilinx I2C Bus Interface device" 
    },
    { 
        .name = "stop", .f_cmd = _fi2ccore_cmd_stop, .parms_num = 0,
        .help = "Stop Xilinx I2C Bus Interface device" 
    },
};

/* Device instance hw-specific initializations 
 */
static ccl_err_t _fi2ccore_add(void *devo, void *brdspec) {
    i2ccore_ctx_t       *p_i2ccore;
    i2ccore_brdspec_t   *p_brdspec;
    b_i32               i;

    if ( !devo || !brdspec ) {
        PDEBUG("Error! Invalid input parameters\n");
        I2CCORE_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_i2ccore = devman_device_private(devo);
    if ( !p_i2ccore ) {
        PDEBUG("NULL context area\n");
        I2CCORE_ERROR_RETURN(CCL_FAIL);
    }
    p_brdspec = (i2ccore_brdspec_t *)brdspec;
    /* back reference the device manager object handle */    
    p_i2ccore->devo = devo;    
    /* save the board specific info */
    memcpy(&p_i2ccore->brdspec, p_brdspec, sizeof(i2ccore_brdspec_t));

    /* store the context */
    _p_i2ccore[p_brdspec->dev_id] = p_i2ccore;

    return CCL_OK;
}

/* Device instance hw-specific deinitializations 
 */
static ccl_err_t _fi2ccore_cut(void *devo)
{
    i2ccore_ctx_t  *p_i2ccore;
    if ( !devo ) {
        PDEBUG("Error! Invalid input parameters\n");
        I2CCORE_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_i2ccore = devman_device_private(devo);
    if ( !p_i2ccore ) {
        PDEBUG("NULL context area\n");
        I2CCORE_ERROR_RETURN(CCL_FAIL);
    }
    /* stop the device */
    _ret = _i2ccore_stop(p_i2ccore);
    if (_ret) {
        PDEBUG("i2ccore_stop error\n");
        I2CCORE_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

/* device type description structure */
static device_driver_t _i2ccore_driver = {
    .name = "i2ccore",
    .f_add = _fi2ccore_add,
    .f_cut = _fi2ccore_cut,
    .attrs = _attrs,
    .attrs_num = sizeof(_attrs)/sizeof(_attrs[0]),
    .cmds = _cmds,
    .cmds_num = sizeof(_cmds)/sizeof(_cmds[0]),
    .priv_size = sizeof(i2ccore_ctx_t)
}; 

/* Initialize I2CCORE device type
 */
ccl_err_t devi2ccore_init(void)
{
    /* register device type */
    _rc = devman_driver_register(&_i2ccore_driver);
    if ( _rc ) {
        PDEBUG("Error! Failed to register driver\n");
        I2CCORE_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}

/* Deinitialize I2CCORE device type
 */
ccl_err_t devi2ccore_fini(void)
{
    /* unregister device type */
    _rc = devman_driver_unregister(&_i2ccore_driver);
    if ( _rc ) {
        PDEBUG("Error! Failed to unregister driver\n");
        I2CCORE_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}
