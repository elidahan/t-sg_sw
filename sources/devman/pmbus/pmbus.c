/********************************************************************************/
/**
 * @file pmbus.c
 * @brief PMBus device manager driver 
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/

#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <semaphore.h>
#include <linux/limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "i2ccore.h"       
#include "pmbus.h"

//#define PMBUS_DEBUG
/* printing/error-returning macros */
#ifdef PMBUS_DEBUG
    #define PDEBUG(fmt, args...) \
            ccl_syslog_err("PMBUS: %s[%d]: " fmt, __FUNCTION__, __LINE__, ## args)
#else
    #define PDEBUG(fmt, args...)
#endif
#define PMBUS_ERROR_RETURN(err) do { \
   ccl_syslog_err("Error! %s[%d], error=%i\n", __FUNCTION__, __LINE__, (err)); \
   return err; \
} while (0)

/* pmbus commands */
#define PMBUS_CAPABILITY           0x19
#define PMBUS_QUERY                0x1A                          
#define PMBUS_VOUT_MODE            0x20                              
#define PMBUS_COEFFICIENTS         0x30                                 
#define PMBUS_FAN_CONFIG_1_2       0x3A                                   
#define PMBUS_STATUS_WORD          0x79                                
#define PMBUS_STATUS_VOUT          0x7A                                
#define PMBUS_STATUS_IOUT          0x7B                                
#define PMBUS_STATUS_INPUT         0x7C                                 
#define PMBUS_STATUS_TEMPERATURE   0x7D                                       
#define PMBUS_STATUS_FANS_1_2      0x81                                    
#define PMBUS_READ_EIN             0x86                             
#define PMBUS_READ_EOUT            0x87                              
#define PMBUS_READ_VOUT            0x8B                              
#define PMBUS_READ_IOUT            0x8C                              
#define PMBUS_READ_TEMPERATURE_1   0x8D                                       
#define PMBUS_READ_FAN_SPEED_1     0x90                                     
#define PMBUS_READ_FAN_SPEED_2     0x91                                     
#define PMBUS_READ_POUT            0x96                              
#define PMBUS_READ_PIN             0x97                             
#define PMBUS_MFR_ID               0x99                           
#define PMBUS_MFR_MODEL            0x9A                              
#define PMBUS_MFR_IOUT_MAX         0xA6                     

#define PMBUS_MAX_OFFSET PMBUS_MFR_IOUT_MAX

/* the pmbus internal control structure */
typedef struct pmbus_ctx_t {
    void                *devo;
    pmbus_brdspec_t     brdspec;
} pmbus_ctx_t;

static ccl_err_t _ret;                     

static ccl_err_t _pmbus_reg_read(void *p_priv, b_u32 attrid, 
                                 b_u32 cmd, b_u32 cmd_sz, 
                                 b_u32 idx, b_u8 *p_value, 
                                 b_u32 size);
static ccl_err_t _pmbus_reg_write(void *p_priv, b_u32 attrid, 
                                  b_u32 cmd, b_u32 cmd_sz, 
                                  b_u32 idx, b_u8 *p_value, 
                                  b_u32 size);
static ccl_err_t _pmbus_buf_read(void *p_priv, b_u32 attrid, 
                                 b_u32 cmd, b_u32 cmd_sz, 
                                 b_u32 idx, b_u8 *p_value, 
                                 b_u32 size);
static ccl_err_t _pmbus_str_read(void *p_priv, b_u32 attrid, 
                                 b_u32 cmd, b_u32 cmd_sz, 
                                 b_u32 idx, b_u8 *p_value, 
                                 b_u32 size);

static ccl_err_t _pmbus_read_measurement_data(void *priv, 
                                              b_u8 *buf);

static ccl_err_t _pmbus_init(void *p_priv)
{
    pmbus_ctx_t    *p_pmbus;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        PMBUS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_pmbus = (pmbus_ctx_t *)p_priv;

    return CCL_OK;
}

static ccl_err_t _pmbus_configure_test(void *p_priv)
{
    pmbus_ctx_t    *p_pmbus;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        PMBUS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_pmbus = (pmbus_ctx_t *)p_priv;

    return CCL_OK;
}

static ccl_err_t _pmbus_run_test(void *p_priv, b_u8 *buf, b_u32 size)
{
    pmbus_ctx_t    *p_pmbus;
    b_u16          data;
    b_u32          regval = 0;
    b_i32          mantissa;
    b_i32          exponent;
    b_double64     read_vin = -1;
    b_double64     read_iin = -1;
    b_double64     read_vout = -1;
    b_double64     read_iout = -1;
    pmbus_stats_t  *stats;
    b_bool         is_fail = CCL_FALSE;

    if ( !p_priv || !buf ) {
        PDEBUG("Error! Invalid input parameters\n");
        PMBUS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_pmbus = (pmbus_ctx_t *)p_priv;

    if (size == sizeof(pmbus_stats_t)) {
        stats = (pmbus_stats_t *)buf;
        memset(&stats->vin, 0, sizeof(*stats)-DEVMAN_ATTRS_NAME_LEN);
    }

    /* Iout */
    if (p_pmbus->brdspec.cmd[ePMBUS_CMD_READ_IOUT].is_supported) {
        _ret = _pmbus_reg_read(p_pmbus, ePMBUS_CMD_READ_IOUT, ePMBUS_CMD_READ_IOUT, 
                               sizeof(b_u8), 0, (b_u8 *)&regval, 2);
        if ( _ret == CCL_OK ) {
            mantissa = ccl_2s_compl_to_decimal(regval, 11);
            exponent = regval >> 11;
            exponent = ccl_2s_compl_to_decimal(exponent, 5);
            read_iout = mantissa * pow(2, exponent);
            if (size == sizeof(pmbus_stats_t)) {
                if (read_iout > 0 && read_iout < p_pmbus->brdspec.iout_max) 
                    stats->iout = read_iout;
            }
            ccl_syslog_err("read_iout: regval=0x%04x, mantissa=%d, exponent=%d, read_iout=%.03f\n", 
                   regval, mantissa, exponent, read_iout);
        }
        if (_ret) 
            is_fail = CCL_TRUE;
    }
    /* Vout */
    if (p_pmbus->brdspec.cmd[ePMBUS_CMD_READ_VOUT].is_supported) {
        _ret = _pmbus_reg_read(p_pmbus, ePMBUS_CMD_READ_VOUT, ePMBUS_CMD_READ_VOUT, 
                               sizeof(b_u8), 0, (b_u8 *)&regval, 2);
        if ( _ret == CCL_OK ) {
            mantissa = ccl_2s_compl_to_decimal(regval, 16);
            read_vout = mantissa * pow(2, p_pmbus->brdspec.exponent);
            if (size == sizeof(pmbus_stats_t)) {
                if (read_vout > 0 && read_vout < p_pmbus->brdspec.vout_max) 
                    stats->vout = read_vout;
            }
            ccl_syslog_err("read_vout: regval=0x%04x, mantissa=%d, exponent=%d, read_vout=%.03f\n", 
                   regval, mantissa, p_pmbus->brdspec.exponent, read_vout);
        }
        if (_ret) 
            is_fail = CCL_TRUE;
    }
    /* Vin */
    if (p_pmbus->brdspec.cmd[ePMBUS_CMD_READ_VIN].is_supported) {
        _ret = _pmbus_reg_read(p_pmbus, ePMBUS_CMD_READ_VIN, ePMBUS_CMD_READ_VIN, 
                               sizeof(b_u8), 0, (b_u8 *)&regval, 2);
        if ( _ret == CCL_OK ) {
            mantissa = ccl_2s_compl_to_decimal(regval, 11);
            exponent = regval >> 11;
            exponent = ccl_2s_compl_to_decimal(exponent, 5);
            read_vin = mantissa * pow(2, exponent);
            if (size == sizeof(pmbus_stats_t)) {
                if (read_vin > 0 && read_vin < p_pmbus->brdspec.vin_max) 
                    stats->vin = read_vin;
            }

            ccl_syslog_err("read_vin: regval=0x%04x, mantissa=%d, exponent=%d, read_vin=%.03f\n", 
                   regval, mantissa, exponent, read_vin);
        }
        if (_ret) 
            is_fail = CCL_TRUE;
    }

    /* Iin  - TODO find correct formula (currently not supported in all our devices) */


    if (is_fail) 
        PMBUS_ERROR_RETURN(CCL_FAIL);

    ccl_syslog_err("Vin=%.03f, Vout=%.03f, Iout=%.03f\n", 
           read_vin, read_vout, read_iout);

    return CCL_OK;
}

ccl_err_t pmbus_attr_read(void *p_priv, b_u32 attrid, 
                          b_u32 offset, b_u32 offset_sz, 
                          b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || offset > PMBUS_MAX_OFFSET || 
         !p_value || idx || size == DEVMAN_ATTRS_SIZE_MAX ) {
        PDEBUG("Error! Invalid input parameters\n"
               "p_priv=%p, offset=0x%04x, p_value=%p\n"
               "idx=%d, size=%d\n");
        PMBUS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch (attrid) {
    case ePMBUS_CMD_PAGE:
    case ePMBUS_CMD_OPERATION:
    case ePMBUS_CMD_ON_OFF_CONFIG:
    case ePMBUS_CMD_PHASE:                    
    case ePMBUS_CMD_WRITE_PROTECT:            
    case ePMBUS_CMD_CAPABILITY:               
    case ePMBUS_CMD_QUERY:                    
    case ePMBUS_CMD_SMBALERT_MASK:            
    case ePMBUS_CMD_VOUT_MODE:                
    case ePMBUS_CMD_VOUT_COMMAND:             
    case ePMBUS_CMD_VOUT_TRIM:                
    case ePMBUS_CMD_VOUT_CAL_OFFSET:          
    case ePMBUS_CMD_VOUT_MAX:                 
    case ePMBUS_CMD_VOUT_MARGIN_HIGH:         
    case ePMBUS_CMD_VOUT_MARGIN_LOW:          
    case ePMBUS_CMD_VOUT_TRANSITION_RATE:     
    case ePMBUS_CMD_VOUT_DROOP:               
    case ePMBUS_CMD_VOUT_SCALE_LOOP:          
    case ePMBUS_CMD_VOUT_SCALE_MONITOR:       
    case ePMBUS_CMD_POUT_MAX:                 
    case ePMBUS_CMD_MAX_DUTY:                 
    case ePMBUS_CMD_FREQUENCY_SWITCH:         
    case ePMBUS_CMD_VIN_ON:                   
    case ePMBUS_CMD_VIN_OFF:                  
    case ePMBUS_CMD_INTERLEAVE:               
    case ePMBUS_CMD_IOUT_CAL_GAIN:            
    case ePMBUS_CMD_IOUT_CAL_OFFSET:          
    case ePMBUS_CMD_FAN_CONFIG_1_2:           
    case ePMBUS_CMD_FAN_COMMAND_1:            
    case ePMBUS_CMD_FAN_COMMAND_2:            
    case ePMBUS_CMD_FAN_CONFIG_3_4:           
    case ePMBUS_CMD_FAN_COMMAND_3:            
    case ePMBUS_CMD_FAN_COMMAND_4:            
    case ePMBUS_CMD_VOUT_OV_FAULT_LIMIT:      
    case ePMBUS_CMD_VOUT_OV_FAULT_RESPONSE:   
    case ePMBUS_CMD_VOUT_OV_WARN_LIMIT:       
    case ePMBUS_CMD_VOUT_UV_WARN_LIMIT:       
    case ePMBUS_CMD_VOUT_UV_FAULT_LIMIT:      
    case ePMBUS_CMD_VOUT_UV_FAULT_RESPONSE:   
    case ePMBUS_CMD_IOUT_OC_FAULT_LIMIT:      
    case ePMBUS_CMD_IOUT_OC_FAULT_RESPONSE:   
    case ePMBUS_CMD_IOUT_OC_LV_FAULT_LIMIT:   
    case ePMBUS_CMD_IOUT_OC_LV_FAULT_RESPONSE:
    case ePMBUS_CMD_IOUT_OC_WARN_LIMIT:       
    case ePMBUS_CMD_IOUT_UC_FAULT_LIMIT:      
    case ePMBUS_CMD_IOUT_UC_FAULT_RESPONSE:   
    case ePMBUS_CMD_OT_FAULT_LIMIT:           
    case ePMBUS_CMD_OT_FAULT_RESPONSE:        
    case ePMBUS_CMD_OT_WARN_LIMIT:            
    case ePMBUS_CMD_UT_WARN_LIMIT:            
    case ePMBUS_CMD_UT_FAULT_LIMIT:           
    case ePMBUS_CMD_UT_FAULT_RESPONSE:        
    case ePMBUS_CMD_VIN_OV_FAULT_LIMIT:       
    case ePMBUS_CMD_VIN_OV_FAULT_RESPONSE:    
    case ePMBUS_CMD_VIN_OV_WARN_LIMIT:        
    case ePMBUS_CMD_VIN_UV_WARN_LIMIT:        
    case ePMBUS_CMD_VIN_UV_FAULT_LIMIT:       
    case ePMBUS_CMD_VIN_UV_FAULT_RESPONSE:    
    case ePMBUS_CMD_IIN_OC_FAULT_LIMIT:       
    case ePMBUS_CMD_IIN_OC_FAULT_RESPONSE:    
    case ePMBUS_CMD_IIN_OC_WARN_LIMIT:        
    case ePMBUS_CMD_POWER_GOOD_ON:            
    case ePMBUS_CMD_POWER_GOOD_OFF:           
    case ePMBUS_CMD_TON_DELAY:                
    case ePMBUS_CMD_TON_RISE:                 
    case ePMBUS_CMD_TON_MAX_FAULT_LIMIT:      
    case ePMBUS_CMD_TON_MAX_FAULT_RESPONSE:   
    case ePMBUS_CMD_TOFF_DELAY:               
    case ePMBUS_CMD_TOFF_FALL:                
    case ePMBUS_CMD_TOFF_MAX_WARN_LIMIT:      
    case ePMBUS_CMD_POUT_OP_FAULT_LIMIT:      
    case ePMBUS_CMD_POUT_OP_FAULT_RESPONSE:   
    case ePMBUS_CMD_POUT_OP_WARN_LIMIT:       
    case ePMBUS_CMD_PIN_OP_WARN_LIMIT:        
    case ePMBUS_CMD_STATUS_BYTE:              
    case ePMBUS_CMD_STATUS_WORD:              
    case ePMBUS_CMD_STATUS_VOUT:              
    case ePMBUS_CMD_STATUS_IOUT:              
    case ePMBUS_CMD_STATUS_INPUT:             
    case ePMBUS_CMD_STATUS_TEMPERATURE:       
    case ePMBUS_CMD_STATUS_CML:               
    case ePMBUS_CMD_STATUS_OTHER:             
    case ePMBUS_CMD_STATUS_MFR_SPECIFIC:      
    case ePMBUS_CMD_STATUS_FANS_1_2:          
    case ePMBUS_CMD_STATUS_FANS_3_4:          
    case ePMBUS_CMD_READ_VIN:                 
    case ePMBUS_CMD_READ_IIN:                 
    case ePMBUS_CMD_READ_VCAP:                
    case ePMBUS_CMD_READ_VOUT:                
    case ePMBUS_CMD_READ_IOUT:                
    case ePMBUS_CMD_READ_TEMPERATURE_1:       
    case ePMBUS_CMD_READ_TEMPERATURE_2:       
    case ePMBUS_CMD_READ_TEMPERATURE_3:       
    case ePMBUS_CMD_READ_FAN_SPEED_1:         
    case ePMBUS_CMD_READ_FAN_SPEED_2:         
    case ePMBUS_CMD_READ_FAN_SPEED_3:         
    case ePMBUS_CMD_READ_FAN_SPEED_4:         
    case ePMBUS_CMD_READ_DUTY_CYCLE:          
    case ePMBUS_CMD_READ_FREQUENCY:           
    case ePMBUS_CMD_READ_POUT:                
    case ePMBUS_CMD_READ_PIN:                 
    case ePMBUS_CMD_PMBUS_REVISION:           
    case ePMBUS_CMD_MFR_SERIAL:               
    case ePMBUS_CMD_MFR_VIN_MIN:              
    case ePMBUS_CMD_MFR_VIN_MAX:              
    case ePMBUS_CMD_MFR_IIN_MAX:              
    case ePMBUS_CMD_MFR_PIN_MAX:              
    case ePMBUS_CMD_MFR_VOUT_MIN:             
    case ePMBUS_CMD_MFR_VOUT_MAX:             
    case ePMBUS_CMD_MFR_IOUT_MAX:             
    case ePMBUS_CMD_MFR_POUT_MAX:             
    case ePMBUS_CMD_MFR_TAMBIENT_MAX:         
    case ePMBUS_CMD_MFR_TAMBIENT_MIN:         
    case ePMBUS_CMD_MFR_PIN_ACCURACY:         
        _ret = _pmbus_reg_read(p_priv, attrid, offset, 
                               offset_sz, idx, p_value, size);
        break;
    case ePMBUS_CMD_PAGE_PLUS_READ:
    case ePMBUS_CMD_COEFFICIENTS:
    case ePMBUS_CMD_READ_EIN:
    case ePMBUS_CMD_READ_EOUT:
    case ePMBUS_CMD_MFR_ID:
    case ePMBUS_CMD_MFR_REVISION:
    case ePMBUS_CMD_MFR_LOCATION:
    case ePMBUS_CMD_APP_PROFILE_SUPPORT:
    case ePMBUS_CMD_MFR_EFFICIENCY_LL:
    case ePMBUS_CMD_MFR_EFFICIENCY_HL:
    case ePMBUS_CMD_IC_DEVICE_ID:
    case ePMBUS_CMD_IC_DEVICE_REV:
    case ePMBUS_CMD_USER_DATA_00:
    case ePMBUS_CMD_USER_DATA_01:
    case ePMBUS_CMD_USER_DATA_02:
    case ePMBUS_CMD_USER_DATA_03:
    case ePMBUS_CMD_USER_DATA_04:
    case ePMBUS_CMD_USER_DATA_05:
    case ePMBUS_CMD_USER_DATA_06:
    case ePMBUS_CMD_USER_DATA_07:
    case ePMBUS_CMD_USER_DATA_08:
    case ePMBUS_CMD_USER_DATA_09:
    case ePMBUS_CMD_USER_DATA_10:
    case ePMBUS_CMD_USER_DATA_11:
    case ePMBUS_CMD_USER_DATA_12:
    case ePMBUS_CMD_USER_DATA_13:
    case ePMBUS_CMD_USER_DATA_14:
    case ePMBUS_CMD_USER_DATA_15:
    case ePMBUS_CMD_MFR_MAX_TEMP_1:
    case ePMBUS_CMD_MFR_MAX_TEMP_2:
    case ePMBUS_CMD_MFR_MAX_TEMP_3:
    case ePMBUS_CMD_MFR_SPECIFIC_00:
    case ePMBUS_CMD_MFR_SPECIFIC_01:
    case ePMBUS_CMD_MFR_SPECIFIC_02:
    case ePMBUS_CMD_MFR_SPECIFIC_03:
    case ePMBUS_CMD_MFR_SPECIFIC_04:
    case ePMBUS_CMD_MFR_SPECIFIC_05:
    case ePMBUS_CMD_MFR_SPECIFIC_06:
    case ePMBUS_CMD_MFR_SPECIFIC_07:
    case ePMBUS_CMD_MFR_SPECIFIC_08:
    case ePMBUS_CMD_MFR_SPECIFIC_09:
    case ePMBUS_CMD_MFR_SPECIFIC_10:
    case ePMBUS_CMD_MFR_SPECIFIC_11:
    case ePMBUS_CMD_MFR_SPECIFIC_12:
    case ePMBUS_CMD_MFR_SPECIFIC_13:
    case ePMBUS_CMD_MFR_SPECIFIC_14:
    case ePMBUS_CMD_MFR_SPECIFIC_15:
    case ePMBUS_CMD_MFR_SPECIFIC_16:
    case ePMBUS_CMD_MFR_SPECIFIC_17:
    case ePMBUS_CMD_MFR_SPECIFIC_18:
    case ePMBUS_CMD_MFR_SPECIFIC_19:
    case ePMBUS_CMD_MFR_SPECIFIC_20:
    case ePMBUS_CMD_MFR_SPECIFIC_21:
    case ePMBUS_CMD_MFR_SPECIFIC_22:
    case ePMBUS_CMD_MFR_SPECIFIC_23:
    case ePMBUS_CMD_MFR_SPECIFIC_24:
    case ePMBUS_CMD_MFR_SPECIFIC_25:
    case ePMBUS_CMD_MFR_SPECIFIC_26:
    case ePMBUS_CMD_MFR_SPECIFIC_27:
    case ePMBUS_CMD_MFR_SPECIFIC_28:
    case ePMBUS_CMD_MFR_SPECIFIC_29:
    case ePMBUS_CMD_MFR_SPECIFIC_30:
    case ePMBUS_CMD_MFR_SPECIFIC_31:
    case ePMBUS_CMD_MFR_SPECIFIC_32:
    case ePMBUS_CMD_MFR_SPECIFIC_33:
    case ePMBUS_CMD_MFR_SPECIFIC_34:
    case ePMBUS_CMD_MFR_SPECIFIC_35:
    case ePMBUS_CMD_MFR_SPECIFIC_36:
    case ePMBUS_CMD_MFR_SPECIFIC_37:
    case ePMBUS_CMD_MFR_SPECIFIC_38:
    case ePMBUS_CMD_MFR_SPECIFIC_39:
    case ePMBUS_CMD_MFR_SPECIFIC_40:
    case ePMBUS_CMD_MFR_SPECIFIC_41:
    case ePMBUS_CMD_MFR_SPECIFIC_42:
    case ePMBUS_CMD_MFR_SPECIFIC_43:
    case ePMBUS_CMD_MFR_SPECIFIC_44:
    case ePMBUS_CMD_MFR_SPECIFIC_45:
    case ePMBUS_CMD_MFR_SPECIFIC_COMMAND:
    case ePMBUS_CMD_PMBUS_COMMAND_EXT:
        _ret = _pmbus_buf_read(p_priv, attrid, offset, 
                               offset_sz, idx, p_value, size);
        break;
    case ePMBUS_CMD_MFR_MODEL:                
    case ePMBUS_CMD_MFR_DATE:                 
        _ret = _pmbus_str_read(p_priv, attrid, offset, 
                               offset_sz, idx, p_value, size);
        break;
    case ePMBUS_MEASUREMENT_DATA:
        _ret = _pmbus_read_measurement_data(p_priv, p_value);
        break;
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK) {
        PDEBUG("Failure in attribute id: %d\n", attrid);
    }

    return _ret;               
}

ccl_err_t pmbus_attr_write(void *p_priv, b_u32 attrid, 
                           b_u32 offset, b_u32 offset_sz, 
                           b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || offset > PMBUS_MAX_OFFSET || 
         idx || size == DEVMAN_ATTRS_SIZE_MAX ) {
        PDEBUG("Error! Invalid input parameters\n"
               "p_priv=%p, offset=0x%04x, p_value=%p\n"
               "idx=%d, size=%d\n");
        PMBUS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch (attrid) {
    case ePMBUS_CMD_PAGE:
    case ePMBUS_CMD_OPERATION:
    case ePMBUS_CMD_ON_OFF_CONFIG:
    case ePMBUS_CMD_CLEAR_FAULTS:             
    case ePMBUS_CMD_PHASE:                    
    case ePMBUS_CMD_WRITE_PROTECT:            
    case ePMBUS_CMD_STORE_DEFAULT_ALL:        
    case ePMBUS_CMD_RESTORE_DEFAULT_ALL:      
    case ePMBUS_CMD_STORE_DEFAULT_CODE:       
    case ePMBUS_CMD_RESTORE_DEFAULT_CODE:     
    case ePMBUS_CMD_STORE_USER_ALL:           
    case ePMBUS_CMD_RESTORE_USER_ALL:         
    case ePMBUS_CMD_STORE_USER_CODE:          
    case ePMBUS_CMD_RESTORE_USER_CODE:        
    case ePMBUS_CMD_SMBALERT_MASK:            
    case ePMBUS_CMD_VOUT_MODE:                
    case ePMBUS_CMD_VOUT_COMMAND:             
    case ePMBUS_CMD_VOUT_TRIM:                
    case ePMBUS_CMD_VOUT_CAL_OFFSET:          
    case ePMBUS_CMD_VOUT_MAX:                 
    case ePMBUS_CMD_VOUT_MARGIN_HIGH:         
    case ePMBUS_CMD_VOUT_MARGIN_LOW:          
    case ePMBUS_CMD_VOUT_TRANSITION_RATE:     
    case ePMBUS_CMD_VOUT_DROOP:               
    case ePMBUS_CMD_VOUT_SCALE_LOOP:          
    case ePMBUS_CMD_VOUT_SCALE_MONITOR:       
    case ePMBUS_CMD_POUT_MAX:                 
    case ePMBUS_CMD_MAX_DUTY:                 
    case ePMBUS_CMD_FREQUENCY_SWITCH:         
    case ePMBUS_CMD_VIN_ON:                   
    case ePMBUS_CMD_VIN_OFF:                  
    case ePMBUS_CMD_INTERLEAVE:               
    case ePMBUS_CMD_IOUT_CAL_GAIN:            
    case ePMBUS_CMD_IOUT_CAL_OFFSET:          
    case ePMBUS_CMD_FAN_CONFIG_1_2:           
    case ePMBUS_CMD_FAN_COMMAND_1:            
    case ePMBUS_CMD_FAN_COMMAND_2:            
    case ePMBUS_CMD_FAN_CONFIG_3_4:           
    case ePMBUS_CMD_FAN_COMMAND_3:            
    case ePMBUS_CMD_FAN_COMMAND_4:            
    case ePMBUS_CMD_VOUT_OV_FAULT_LIMIT:      
    case ePMBUS_CMD_VOUT_OV_FAULT_RESPONSE:   
    case ePMBUS_CMD_VOUT_OV_WARN_LIMIT:       
    case ePMBUS_CMD_VOUT_UV_WARN_LIMIT:       
    case ePMBUS_CMD_VOUT_UV_FAULT_LIMIT:      
    case ePMBUS_CMD_VOUT_UV_FAULT_RESPONSE:   
    case ePMBUS_CMD_IOUT_OC_FAULT_LIMIT:      
    case ePMBUS_CMD_IOUT_OC_FAULT_RESPONSE:   
    case ePMBUS_CMD_IOUT_OC_LV_FAULT_LIMIT:   
    case ePMBUS_CMD_IOUT_OC_LV_FAULT_RESPONSE:
    case ePMBUS_CMD_IOUT_OC_WARN_LIMIT:       
    case ePMBUS_CMD_IOUT_UC_FAULT_LIMIT:      
    case ePMBUS_CMD_IOUT_UC_FAULT_RESPONSE:   
    case ePMBUS_CMD_OT_FAULT_LIMIT:           
    case ePMBUS_CMD_OT_FAULT_RESPONSE:        
    case ePMBUS_CMD_OT_WARN_LIMIT:            
    case ePMBUS_CMD_UT_WARN_LIMIT:            
    case ePMBUS_CMD_UT_FAULT_LIMIT:           
    case ePMBUS_CMD_UT_FAULT_RESPONSE:        
    case ePMBUS_CMD_VIN_OV_FAULT_LIMIT:       
    case ePMBUS_CMD_VIN_OV_FAULT_RESPONSE:    
    case ePMBUS_CMD_VIN_OV_WARN_LIMIT:        
    case ePMBUS_CMD_VIN_UV_WARN_LIMIT:        
    case ePMBUS_CMD_VIN_UV_FAULT_LIMIT:       
    case ePMBUS_CMD_VIN_UV_FAULT_RESPONSE:    
    case ePMBUS_CMD_IIN_OC_FAULT_LIMIT:       
    case ePMBUS_CMD_IIN_OC_FAULT_RESPONSE:    
    case ePMBUS_CMD_IIN_OC_WARN_LIMIT:        
    case ePMBUS_CMD_POWER_GOOD_ON:            
    case ePMBUS_CMD_POWER_GOOD_OFF:           
    case ePMBUS_CMD_TON_DELAY:                
    case ePMBUS_CMD_TON_RISE:                 
    case ePMBUS_CMD_TON_MAX_FAULT_LIMIT:      
    case ePMBUS_CMD_TON_MAX_FAULT_RESPONSE:   
    case ePMBUS_CMD_TOFF_DELAY:               
    case ePMBUS_CMD_TOFF_FALL:                
    case ePMBUS_CMD_TOFF_MAX_WARN_LIMIT:      
    case ePMBUS_CMD_POUT_OP_FAULT_LIMIT:      
    case ePMBUS_CMD_POUT_OP_FAULT_RESPONSE:   
    case ePMBUS_CMD_POUT_OP_WARN_LIMIT:       
    case ePMBUS_CMD_PIN_OP_WARN_LIMIT:        
    case ePMBUS_CMD_STATUS_BYTE:              
    case ePMBUS_CMD_STATUS_WORD:              
    case ePMBUS_CMD_STATUS_VOUT:              
    case ePMBUS_CMD_STATUS_IOUT:              
    case ePMBUS_CMD_STATUS_INPUT:             
    case ePMBUS_CMD_STATUS_TEMPERATURE:       
    case ePMBUS_CMD_STATUS_CML:               
    case ePMBUS_CMD_STATUS_OTHER:             
    case ePMBUS_CMD_STATUS_MFR_SPECIFIC:      
    case ePMBUS_CMD_STATUS_FANS_1_2:          
    case ePMBUS_CMD_STATUS_FANS_3_4:          
    case ePMBUS_CMD_MFR_SERIAL:               
        _ret = _pmbus_reg_write(p_priv, attrid, offset, 
                                offset_sz, idx, p_value, size);
        break;
    case ePMBUS_CMD_PAGE_PLUS_WRITE:
    case ePMBUS_CMD_MFR_ID:
    case ePMBUS_CMD_MFR_REVISION:
    case ePMBUS_CMD_MFR_LOCATION:
    case ePMBUS_CMD_MFR_EFFICIENCY_LL:
    case ePMBUS_CMD_MFR_EFFICIENCY_HL:
    case ePMBUS_CMD_USER_DATA_00:
    case ePMBUS_CMD_USER_DATA_01:
    case ePMBUS_CMD_USER_DATA_02:
    case ePMBUS_CMD_USER_DATA_03:
    case ePMBUS_CMD_USER_DATA_04:
    case ePMBUS_CMD_USER_DATA_05:
    case ePMBUS_CMD_USER_DATA_06:
    case ePMBUS_CMD_USER_DATA_07:
    case ePMBUS_CMD_USER_DATA_08:
    case ePMBUS_CMD_USER_DATA_09:
    case ePMBUS_CMD_USER_DATA_10:
    case ePMBUS_CMD_USER_DATA_11:
    case ePMBUS_CMD_USER_DATA_12:
    case ePMBUS_CMD_USER_DATA_13:
    case ePMBUS_CMD_USER_DATA_14:
    case ePMBUS_CMD_USER_DATA_15:
    case ePMBUS_CMD_MFR_MAX_TEMP_1:
    case ePMBUS_CMD_MFR_MAX_TEMP_2:
    case ePMBUS_CMD_MFR_MAX_TEMP_3:
    case ePMBUS_CMD_MFR_SPECIFIC_00:
    case ePMBUS_CMD_MFR_SPECIFIC_01:
    case ePMBUS_CMD_MFR_SPECIFIC_02:
    case ePMBUS_CMD_MFR_SPECIFIC_03:
    case ePMBUS_CMD_MFR_SPECIFIC_04:
    case ePMBUS_CMD_MFR_SPECIFIC_05:
    case ePMBUS_CMD_MFR_SPECIFIC_06:
    case ePMBUS_CMD_MFR_SPECIFIC_07:
    case ePMBUS_CMD_MFR_SPECIFIC_08:
    case ePMBUS_CMD_MFR_SPECIFIC_09:
    case ePMBUS_CMD_MFR_SPECIFIC_10:
    case ePMBUS_CMD_MFR_SPECIFIC_11:
    case ePMBUS_CMD_MFR_SPECIFIC_12:
    case ePMBUS_CMD_MFR_SPECIFIC_13:
    case ePMBUS_CMD_MFR_SPECIFIC_14:
    case ePMBUS_CMD_MFR_SPECIFIC_15:
    case ePMBUS_CMD_MFR_SPECIFIC_16:
    case ePMBUS_CMD_MFR_SPECIFIC_17:
    case ePMBUS_CMD_MFR_SPECIFIC_18:
    case ePMBUS_CMD_MFR_SPECIFIC_19:
    case ePMBUS_CMD_MFR_SPECIFIC_20:
    case ePMBUS_CMD_MFR_SPECIFIC_21:
    case ePMBUS_CMD_MFR_SPECIFIC_22:
    case ePMBUS_CMD_MFR_SPECIFIC_23:
    case ePMBUS_CMD_MFR_SPECIFIC_24:
    case ePMBUS_CMD_MFR_SPECIFIC_25:
    case ePMBUS_CMD_MFR_SPECIFIC_26:
    case ePMBUS_CMD_MFR_SPECIFIC_27:
    case ePMBUS_CMD_MFR_SPECIFIC_28:
    case ePMBUS_CMD_MFR_SPECIFIC_29:
    case ePMBUS_CMD_MFR_SPECIFIC_30:
    case ePMBUS_CMD_MFR_SPECIFIC_31:
    case ePMBUS_CMD_MFR_SPECIFIC_32:
    case ePMBUS_CMD_MFR_SPECIFIC_33:
    case ePMBUS_CMD_MFR_SPECIFIC_34:
    case ePMBUS_CMD_MFR_SPECIFIC_35:
    case ePMBUS_CMD_MFR_SPECIFIC_36:
    case ePMBUS_CMD_MFR_SPECIFIC_37:
    case ePMBUS_CMD_MFR_SPECIFIC_38:
    case ePMBUS_CMD_MFR_SPECIFIC_39:
    case ePMBUS_CMD_MFR_SPECIFIC_40:
    case ePMBUS_CMD_MFR_SPECIFIC_41:
    case ePMBUS_CMD_MFR_SPECIFIC_42:
    case ePMBUS_CMD_MFR_SPECIFIC_43:
    case ePMBUS_CMD_MFR_SPECIFIC_44:
    case ePMBUS_CMD_MFR_SPECIFIC_45:
    case ePMBUS_CMD_MFR_SPECIFIC_COMMAND:
    case ePMBUS_CMD_PMBUS_COMMAND_EXT:
        _ret = _pmbus_reg_write(p_priv, attrid, offset, 
                                offset_sz, idx, p_value, size);
        break;
    case ePMBUS_CMD_MFR_MODEL:                
    case ePMBUS_CMD_MFR_DATE:                 
        _ret = _pmbus_reg_write(p_priv, attrid, offset, 
                                offset_sz, idx, p_value, size);
        break;
    case ePMBUS_INIT:
        _ret = _pmbus_init(p_priv);
        break;
    case ePMBUS_CONFIGURE_TEST:
        _ret = _pmbus_configure_test(p_priv);
        break;
    case ePMBUS_RUN_TEST:
        _ret = _pmbus_run_test(p_priv, p_value, size);
        break;
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK) {
        PDEBUG("Failure in attribute id: %d\n", attrid);
    }

    return _ret;               
}

/* pmbus attributes definition */
static device_attr_t _attrs[] = {
    {
        .name = "i2c_bus", 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset = offsetof(struct pmbus_ctx_t, brdspec.i2c.bus_id),
        .format = "%d"
    },
    {
        .name = "i2c_addr", 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset = offsetof(struct pmbus_ctx_t, brdspec.i2c.dev_i2ca),
        .format = "0x%03x"
    },
    {
        .name = "exponent", 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset = offsetof(struct pmbus_ctx_t, brdspec.exponent),
        .format = "%d"
    },
    {
        .name = "name", 
        .type = eDEVMAN_ATTR_STRING, .maxnum = 1, 
        .offset = offsetof(struct pmbus_ctx_t, brdspec.name),
        .size = DEVMAN_DEVNAME_LEN,
        .format = "%s"
    },
    {
        .name = "page", .id = ePMBUS_CMD_PAGE, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_PAGE
    },
    {
        .name = "operation", .id = ePMBUS_CMD_OPERATION, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_OPERATION
    },
    {
        .name = "on_off_config", .id = ePMBUS_CMD_ON_OFF_CONFIG, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_ON_OFF_CONFIG
    },
    {
        .name = "clear_faults", .id = ePMBUS_CMD_CLEAR_FAULTS, 
        .type = eDEVMAN_ATTR_CMD, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_CLEAR_FAULTS,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "phase", .id = ePMBUS_CMD_PHASE, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_PHASE
    },
    {
        .name = "page_plus_wr", .id = ePMBUS_CMD_PAGE_PLUS_WRITE, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_PAGE_PLUS_WRITE,
        .flags = eDEVMAN_ATTRFLAG_WO | eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "page_plus_rd", .id = ePMBUS_CMD_PAGE_PLUS_READ, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_PAGE_PLUS_READ,
        .flags = eDEVMAN_ATTRFLAG_RO | eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "write_protect", .id = ePMBUS_CMD_WRITE_PROTECT, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_WRITE_PROTECT
    },
    {
        .name = "store_def_all", .id = ePMBUS_CMD_STORE_DEFAULT_ALL, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, .size = 0,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_STORE_DEFAULT_ALL,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "restore_def_all", .id = ePMBUS_CMD_RESTORE_DEFAULT_ALL, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, .size = 0,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_RESTORE_DEFAULT_ALL,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "store_def_code", .id = ePMBUS_CMD_STORE_DEFAULT_CODE, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_STORE_DEFAULT_CODE,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "restore_def_code", .id = ePMBUS_CMD_RESTORE_DEFAULT_CODE, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_RESTORE_DEFAULT_CODE,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "store_user_all", .id = ePMBUS_CMD_STORE_USER_ALL, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, .size = 0,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_STORE_USER_ALL,
        .flags = eDEVMAN_ATTRFLAG_WO | eDEVMAN_ATTRFLAG_HIDDEN 
    },
    {
        .name = "restore_user_all", .id = ePMBUS_CMD_RESTORE_USER_ALL, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, .size = 0,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_RESTORE_USER_ALL,
        .flags = eDEVMAN_ATTRFLAG_WO | eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "store_user_code", .id = ePMBUS_CMD_STORE_USER_CODE, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_STORE_USER_CODE,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "restore_user_code", .id = ePMBUS_CMD_RESTORE_USER_CODE, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_RESTORE_USER_CODE,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "cap", .id = ePMBUS_CMD_CAPABILITY, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_CAPABILITY,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "query", .id = ePMBUS_CMD_QUERY, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_QUERY,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "smbalert_mask", .id = ePMBUS_CMD_SMBALERT_MASK, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_SMBALERT_MASK
    },
    {
        .name = "vout_mode", .id = ePMBUS_CMD_VOUT_MODE, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_VOUT_MODE
    },
    {
        .name = "vout_cmd", .id = ePMBUS_CMD_VOUT_COMMAND, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_VOUT_COMMAND
    },
    {
        .name = "vout_trim", .id = ePMBUS_CMD_VOUT_TRIM, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_VOUT_TRIM
    },
    {
        .name = "vout_cal_offset", .id = ePMBUS_CMD_VOUT_CAL_OFFSET, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_VOUT_CAL_OFFSET
    },
    {
        .name = "vout_max", .id = ePMBUS_CMD_VOUT_MAX, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_VOUT_MAX
    },
    {
        .name = "vout_margin_high", .id = ePMBUS_CMD_VOUT_MARGIN_HIGH, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_VOUT_MARGIN_HIGH
    },
    {
        .name = "vout_margin_low", .id = ePMBUS_CMD_VOUT_MARGIN_LOW, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_VOUT_MARGIN_LOW
    },
    {
        .name = "vout_trans_rate", .id = ePMBUS_CMD_VOUT_TRANSITION_RATE, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_VOUT_TRANSITION_RATE
    },
    {
        .name = "vout_droop", .id = ePMBUS_CMD_VOUT_DROOP, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_VOUT_DROOP
    },
    {
        .name = "vout_scale_loop", .id = ePMBUS_CMD_VOUT_SCALE_LOOP, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_VOUT_SCALE_LOOP
    },
    {
        .name = "vout_scale_monitor", .id = ePMBUS_CMD_VOUT_SCALE_MONITOR, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_VOUT_SCALE_MONITOR
    },
    {
        .name = "coeff", .id = ePMBUS_CMD_COEFFICIENTS, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = 5, 
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_COEFFICIENTS,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "pout_max", .id = ePMBUS_CMD_POUT_MAX, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_POUT_MAX
    },
    {
        .name = "max_duty", .id = ePMBUS_CMD_MAX_DUTY, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MAX_DUTY
    },
    {
        .name = "freq_switch", .id = ePMBUS_CMD_FREQUENCY_SWITCH, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_FREQUENCY_SWITCH
    },
    {
        .name = "vin_on", .id = ePMBUS_CMD_VIN_ON, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_VIN_ON
    },
    {
        .name = "vin_off", .id = ePMBUS_CMD_VIN_OFF, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 0,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_VIN_OFF
    },
    {
        .name = "intereleave", .id = ePMBUS_CMD_INTERLEAVE, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_INTERLEAVE
    },
    {
        .name = "iout_cal_gain", .id = ePMBUS_CMD_IOUT_CAL_GAIN, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_IOUT_CAL_GAIN
    },
    {
        .name = "iout_cal_offset", .id = ePMBUS_CMD_IOUT_CAL_OFFSET, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_IOUT_CAL_OFFSET
    },
    {
        .name = "fan_config_1_2", .id = ePMBUS_CMD_FAN_CONFIG_1_2, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_FAN_CONFIG_1_2
    },
    {
        .name = "fan_command_1", .id = ePMBUS_CMD_FAN_COMMAND_1, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_FAN_COMMAND_1
    },
    {
        .name = "fan_command_2", .id = ePMBUS_CMD_FAN_COMMAND_2, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_FAN_COMMAND_2
    },
    {
        .name = "fan_config_3_4", .id = ePMBUS_CMD_FAN_CONFIG_3_4, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_FAN_CONFIG_3_4
    },
    {
        .name = "fan_command_3", .id = ePMBUS_CMD_FAN_COMMAND_3, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_FAN_COMMAND_3
    },
    {
        .name = "fan_command_4", .id = ePMBUS_CMD_FAN_COMMAND_4, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_FAN_COMMAND_4
    },
    {
        .name = "vout_ov_fault_limit", .id = ePMBUS_CMD_VOUT_OV_FAULT_LIMIT, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_VOUT_OV_FAULT_LIMIT
    },
    {
        .name = "vout_ov_fault_resp", .id = ePMBUS_CMD_VOUT_OV_FAULT_RESPONSE, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_VOUT_OV_FAULT_RESPONSE
    },
    {
        .name = "vout_ov_warn_limit", .id = ePMBUS_CMD_VOUT_OV_WARN_LIMIT, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_VOUT_OV_WARN_LIMIT
    },
    {
        .name = "vout_uv_warn_limit", .id = ePMBUS_CMD_VOUT_UV_WARN_LIMIT, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_VOUT_UV_WARN_LIMIT
    },
    {
        .name = "vout_uv_fault_limit", .id = ePMBUS_CMD_VOUT_UV_FAULT_LIMIT, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_VOUT_UV_FAULT_LIMIT
    },
    {
        .name = "vout_uv_fault_resp", .id = ePMBUS_CMD_VOUT_UV_FAULT_RESPONSE, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_VOUT_UV_FAULT_RESPONSE
    },
    {
        .name = "iout_oc_fault_limit", .id = ePMBUS_CMD_IOUT_OC_FAULT_LIMIT, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_IOUT_OC_FAULT_LIMIT
    },
    {
        .name = "iout_oc_fault_resp", .id = ePMBUS_CMD_IOUT_OC_FAULT_RESPONSE, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_IOUT_OC_FAULT_RESPONSE
    },
    {
        .name = "iout_oc_lv_fault_limit", .id = ePMBUS_CMD_IOUT_OC_LV_FAULT_LIMIT, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_IOUT_OC_LV_FAULT_LIMIT
    },
    {
        .name = "iout_oc_lv_fault_resp", .id = ePMBUS_CMD_IOUT_OC_LV_FAULT_RESPONSE, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_IOUT_OC_LV_FAULT_RESPONSE
    },
    {
        .name = "iout_oc_warn_limit", .id = ePMBUS_CMD_IOUT_OC_WARN_LIMIT, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_IOUT_OC_WARN_LIMIT
    },
    {
        .name = "iout_uc_fault_limit", .id = ePMBUS_CMD_IOUT_UC_FAULT_LIMIT, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_IOUT_UC_FAULT_LIMIT
    },
    {
        .name = "iout_uc_fault_resp", .id = ePMBUS_CMD_IOUT_UC_FAULT_RESPONSE, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_IOUT_UC_FAULT_RESPONSE
    },
    {
        .name = "ot_fault_limit", .id = ePMBUS_CMD_OT_FAULT_LIMIT, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_OT_FAULT_LIMIT
    },
    {
        .name = "ot_fault_resp", .id = ePMBUS_CMD_OT_FAULT_RESPONSE, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_OT_FAULT_RESPONSE
    },
    {
        .name = "ot_warn_limit", .id = ePMBUS_CMD_OT_WARN_LIMIT, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_OT_WARN_LIMIT
    },
    {
        .name = "ut_warn_limit", .id = ePMBUS_CMD_UT_WARN_LIMIT, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_UT_WARN_LIMIT
    },
    {
        .name = "ut_fault_limit", .id = ePMBUS_CMD_UT_FAULT_LIMIT, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_UT_FAULT_LIMIT
    },
    {
        .name = "ut_fault_resp", .id = ePMBUS_CMD_UT_FAULT_RESPONSE, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_UT_FAULT_RESPONSE
    },
    {
        .name = "vin_ov_fault_limit", .id = ePMBUS_CMD_VIN_OV_FAULT_LIMIT, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_VIN_OV_FAULT_LIMIT
    },
    {
        .name = "vin_ov_fault_response", .id = ePMBUS_CMD_VIN_OV_FAULT_RESPONSE, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_VIN_OV_FAULT_RESPONSE
    },
    {
        .name = "vin_ov_warn_limit", .id = ePMBUS_CMD_VIN_OV_WARN_LIMIT, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_VIN_OV_WARN_LIMIT
    },
    {
        .name = "vin_uv_warn_limit", .id = ePMBUS_CMD_VIN_UV_WARN_LIMIT, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_VIN_UV_WARN_LIMIT
    },
    {
        .name = "vin_uv_fault_limit", .id = ePMBUS_CMD_VIN_UV_FAULT_LIMIT, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_VIN_UV_FAULT_LIMIT
    },
    {
        .name = "vin_uv_fault_resp", .id = ePMBUS_CMD_VIN_UV_FAULT_RESPONSE, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_VIN_UV_FAULT_RESPONSE
    },
    {
        .name = "iin_oc_fault_limit", .id = ePMBUS_CMD_IIN_OC_FAULT_LIMIT, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_IIN_OC_FAULT_LIMIT
    },
    {
        .name = "iin_oc_fault_resp", .id = ePMBUS_CMD_IIN_OC_FAULT_RESPONSE, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_IIN_OC_FAULT_RESPONSE
    },
    {
        .name = "iin_oc_warn_limit", .id = ePMBUS_CMD_IIN_OC_WARN_LIMIT, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_IIN_OC_WARN_LIMIT
    },
    {
        .name = "power_good_on", .id = ePMBUS_CMD_POWER_GOOD_ON, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_POWER_GOOD_ON
    },
    {
        .name = "power_good_off", .id = ePMBUS_CMD_POWER_GOOD_OFF, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_POWER_GOOD_OFF
    },
    {
        .name = "ton_delay", .id = ePMBUS_CMD_TON_DELAY, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_TON_DELAY
    },
    {
        .name = "ton_rise", .id = ePMBUS_CMD_TON_RISE, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_TON_RISE
    },
    {
        .name = "ton_max_fault_limit", .id = ePMBUS_CMD_TON_MAX_FAULT_LIMIT, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_TON_MAX_FAULT_LIMIT
    },
    {
        .name = "ton_max_fault_resp", .id = ePMBUS_CMD_TON_MAX_FAULT_RESPONSE, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_TON_MAX_FAULT_RESPONSE
    },
    {
        .name = "toff_delay", .id = ePMBUS_CMD_TOFF_DELAY, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_TOFF_DELAY
    },
    {
        .name = "toff_fall", .id = ePMBUS_CMD_TOFF_FALL, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_TOFF_FALL
    },
    {
        .name = "toff_max_warn_limit", .id = ePMBUS_CMD_TOFF_MAX_WARN_LIMIT, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_TOFF_MAX_WARN_LIMIT
    },
    {
        .name = "pout_op_fault_limit", .id = ePMBUS_CMD_POUT_OP_FAULT_LIMIT, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_POUT_OP_FAULT_LIMIT
    },
    {
        .name = "pout_op_fault_resp", .id = ePMBUS_CMD_POUT_OP_FAULT_RESPONSE, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_POUT_OP_FAULT_RESPONSE
    },
    {
        .name = "put_op_warn_limit", .id = ePMBUS_CMD_POUT_OP_WARN_LIMIT, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_POUT_OP_WARN_LIMIT
    },
    {
        .name = "pin_op_warn_limit", .id = ePMBUS_CMD_PIN_OP_WARN_LIMIT, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_PIN_OP_WARN_LIMIT
    },
    {
        .name = "status_byte", .id = ePMBUS_CMD_STATUS_BYTE, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_STATUS_BYTE
    },
    {
        .name = "status_word", .id = ePMBUS_CMD_STATUS_WORD, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_STATUS_WORD
    },
    {
        .name = "status_vout", .id = ePMBUS_CMD_STATUS_VOUT, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_STATUS_VOUT
    },
    {
        .name = "status_iout", .id = ePMBUS_CMD_STATUS_IOUT, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_STATUS_IOUT
    },
    {
        .name = "status_input", .id = ePMBUS_CMD_STATUS_INPUT, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_STATUS_INPUT
    },
    {
        .name = "status_temp", .id = ePMBUS_CMD_STATUS_TEMPERATURE, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_STATUS_TEMPERATURE
    },
    {
        .name = "status_cml", .id = ePMBUS_CMD_STATUS_CML, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_STATUS_CML
    },
    {
        .name = "status_other", .id = ePMBUS_CMD_STATUS_OTHER, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_STATUS_OTHER
    },
    {
        .name = "status_mfr_specific", .id = ePMBUS_CMD_STATUS_MFR_SPECIFIC, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_STATUS_MFR_SPECIFIC
    },
    {
        .name = "status_fans_1_2", .id = ePMBUS_CMD_STATUS_FANS_1_2, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_STATUS_FANS_1_2
    },
    {
        .name = "status_fans_3_4", .id = ePMBUS_CMD_STATUS_FANS_3_4, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_STATUS_FANS_3_4
    },
    {
        .name = "read_ein", .id = ePMBUS_CMD_READ_EIN, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_READ_EIN,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "read_eout", .id = ePMBUS_CMD_READ_EOUT, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_READ_EOUT,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "read_vin", .id = ePMBUS_CMD_READ_VIN, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_READ_VIN,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "read_iin", .id = ePMBUS_CMD_READ_IIN, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_READ_IIN,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "read_vcap", .id = ePMBUS_CMD_READ_VCAP, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_READ_VCAP,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "read_vout", .id = ePMBUS_CMD_READ_VOUT, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_READ_VOUT,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "read_iout", .id = ePMBUS_CMD_READ_IOUT, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_READ_IOUT,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "read_temp_1", .id = ePMBUS_CMD_READ_TEMPERATURE_1, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_READ_TEMPERATURE_1,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "read_temp_2", .id = ePMBUS_CMD_READ_TEMPERATURE_2, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_READ_TEMPERATURE_2,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "read_temp_3", .id = ePMBUS_CMD_READ_TEMPERATURE_3, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_READ_TEMPERATURE_3,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "read_fan_speed_1", .id = ePMBUS_CMD_READ_FAN_SPEED_1, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_READ_FAN_SPEED_1,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "read_fan_speed_2", .id = ePMBUS_CMD_READ_FAN_SPEED_2, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_READ_FAN_SPEED_2,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "read_fan_speed_3", .id = ePMBUS_CMD_READ_FAN_SPEED_3, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_READ_FAN_SPEED_3,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "read_fan_speed_4", .id = ePMBUS_CMD_READ_FAN_SPEED_4, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_READ_FAN_SPEED_4,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "read_duty_cycle", .id = ePMBUS_CMD_READ_DUTY_CYCLE, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_READ_DUTY_CYCLE,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "read_freq", .id = ePMBUS_CMD_READ_FREQUENCY, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_READ_FREQUENCY,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "read_pout", .id = ePMBUS_CMD_READ_POUT, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_READ_POUT,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "read_pin", .id = ePMBUS_CMD_READ_PIN, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_READ_PIN,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "pmbus_rev", .id = ePMBUS_CMD_PMBUS_REVISION, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_PMBUS_REVISION,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "mfr_id", .id = ePMBUS_CMD_MFR_ID, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_ID
    },
    {
        .name = "mfr_model", .id = ePMBUS_CMD_MFR_MODEL, 
        .type = eDEVMAN_ATTR_STRING, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_MODEL
    },
    {
        .name = "mfr_rev", .id = ePMBUS_CMD_MFR_REVISION, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_REVISION
    },
    {
        .name = "mfr_location", .id = ePMBUS_CMD_MFR_LOCATION, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_LOCATION
    },
    {
        .name = "mfr_date", .id = ePMBUS_CMD_MFR_DATE, 
        .type = eDEVMAN_ATTR_STRING, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_DATE
    },
    {
        .name = "app_profile_support", .id = ePMBUS_CMD_APP_PROFILE_SUPPORT, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_APP_PROFILE_SUPPORT,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "mfr_vin_min", .id = ePMBUS_CMD_MFR_VIN_MIN, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_VIN_MIN,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "mfr_vin_max", .id = ePMBUS_CMD_MFR_VIN_MAX, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_VIN_MAX,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "mfr_iin_max", .id = ePMBUS_CMD_MFR_IIN_MAX, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_IIN_MAX,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "mfr_pin_max", .id = ePMBUS_CMD_MFR_PIN_MAX, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_PIN_MAX,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "mfr_vout_min", .id = ePMBUS_CMD_MFR_VOUT_MIN, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_VOUT_MIN,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "mfr_vout_max", .id = ePMBUS_CMD_MFR_VOUT_MAX, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_VOUT_MAX,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "mfr_iout_max", .id = ePMBUS_CMD_MFR_IOUT_MAX, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_IOUT_MAX,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "mfr_pout_max", .id = ePMBUS_CMD_MFR_POUT_MAX, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_POUT_MAX,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "mfr_tambient_max", .id = ePMBUS_CMD_MFR_TAMBIENT_MAX, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_TAMBIENT_MAX,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "mfr_tambient_min", .id = ePMBUS_CMD_MFR_TAMBIENT_MIN, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_TAMBIENT_MIN,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "mfr_efficiency_ll", .id = ePMBUS_CMD_MFR_EFFICIENCY_LL, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = 14,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_EFFICIENCY_LL,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "mfr_efficiency_hl", .id = ePMBUS_CMD_MFR_EFFICIENCY_HL, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = 14,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_EFFICIENCY_HL,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "mfr_pin_accuracy", .id = ePMBUS_CMD_MFR_PIN_ACCURACY, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_PIN_ACCURACY,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "ic_device_id", .id = ePMBUS_CMD_IC_DEVICE_ID, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_IC_DEVICE_ID,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "ic_device_rev", .id = ePMBUS_CMD_IC_DEVICE_REV, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_IC_DEVICE_REV,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "user_data_00", .id = ePMBUS_CMD_USER_DATA_00, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_USER_DATA_00
    },
    {
        .name = "user_data_01", .id = ePMBUS_CMD_USER_DATA_01, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_USER_DATA_01
    },
    {
        .name = "user_data_02", .id = ePMBUS_CMD_USER_DATA_02, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_USER_DATA_02
    },
    {
        .name = "user_data_03", .id = ePMBUS_CMD_USER_DATA_03, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_USER_DATA_03
    },
    {
        .name = "user_data_04", .id = ePMBUS_CMD_USER_DATA_04, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_USER_DATA_04
    },
    {
        .name = "user_data_05", .id = ePMBUS_CMD_USER_DATA_05, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_USER_DATA_05
    },
    {
        .name = "user_data_06", .id = ePMBUS_CMD_USER_DATA_06, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_USER_DATA_06
    },
    {
        .name = "user_data_07", .id = ePMBUS_CMD_USER_DATA_07, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_USER_DATA_07
    },
    {
        .name = "user_data_08", .id = ePMBUS_CMD_USER_DATA_08, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_USER_DATA_08
    },
    {
        .name = "user_data_09", .id = ePMBUS_CMD_USER_DATA_09, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_USER_DATA_09
    },
    {
        .name = "user_data_10", .id = ePMBUS_CMD_USER_DATA_10, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_USER_DATA_10
    },
    {
        .name = "user_data_11", .id = ePMBUS_CMD_USER_DATA_11, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_USER_DATA_11
    },
    {
        .name = "user_data_12", .id = ePMBUS_CMD_USER_DATA_12, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_USER_DATA_12
    },
    {
        .name = "user_data_13", .id = ePMBUS_CMD_USER_DATA_13, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_USER_DATA_13
    },
    {
        .name = "user_data_14", .id = ePMBUS_CMD_USER_DATA_14, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_USER_DATA_14
    },
    {
        .name = "user_data_15", .id = ePMBUS_CMD_USER_DATA_15, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_USER_DATA_15
    },
    {
        .name = "mfr_max_temp_1", .id = ePMBUS_CMD_MFR_MAX_TEMP_1, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_MAX_TEMP_1
    },
    {
        .name = "mfr_max_temp_2", .id = ePMBUS_CMD_MFR_MAX_TEMP_2, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_MAX_TEMP_2
    },
    {
        .name = "mfr_max_temp_3", .id = ePMBUS_CMD_MFR_MAX_TEMP_3, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = 2,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_MAX_TEMP_3
    },
    {
        .name = "mfr_specific_00", .id = ePMBUS_CMD_MFR_SPECIFIC_00, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_SPECIFIC_00,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "mfr_specific_01", .id = ePMBUS_CMD_MFR_SPECIFIC_01, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_SPECIFIC_01,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "mfr_specific_02", .id = ePMBUS_CMD_MFR_SPECIFIC_02, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_SPECIFIC_02,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "mfr_specific_03", .id = ePMBUS_CMD_MFR_SPECIFIC_03, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_SPECIFIC_03,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "mfr_specific_04", .id = ePMBUS_CMD_MFR_SPECIFIC_04, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_SPECIFIC_04,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "mfr_specific_05", .id = ePMBUS_CMD_MFR_SPECIFIC_05, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_SPECIFIC_05,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "mfr_specific_06", .id = ePMBUS_CMD_MFR_SPECIFIC_06, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_SPECIFIC_06,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "mfr_specific_07", .id = ePMBUS_CMD_MFR_SPECIFIC_07, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_SPECIFIC_07,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "mfr_specific_08", .id = ePMBUS_CMD_MFR_SPECIFIC_08, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_SPECIFIC_08,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "mfr_specific_09", .id = ePMBUS_CMD_MFR_SPECIFIC_09, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_SPECIFIC_09,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "mfr_specific_10", .id = ePMBUS_CMD_MFR_SPECIFIC_10, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_SPECIFIC_10,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "mfr_specific_11", .id = ePMBUS_CMD_MFR_SPECIFIC_11, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_SPECIFIC_11,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "mfr_specific_12", .id = ePMBUS_CMD_MFR_SPECIFIC_12, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_SPECIFIC_12,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "mfr_specific_13", .id = ePMBUS_CMD_MFR_SPECIFIC_13, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_SPECIFIC_13,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "mfr_specific_14", .id = ePMBUS_CMD_MFR_SPECIFIC_14, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_SPECIFIC_14,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "mfr_specific_15", .id = ePMBUS_CMD_MFR_SPECIFIC_15, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_SPECIFIC_15,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "mfr_specific_16", .id = ePMBUS_CMD_MFR_SPECIFIC_16, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_SPECIFIC_16,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "mfr_specific_17", .id = ePMBUS_CMD_MFR_SPECIFIC_17, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_SPECIFIC_17,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "mfr_specific_18", .id = ePMBUS_CMD_MFR_SPECIFIC_18, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_SPECIFIC_18,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "mfr_specific_19", .id = ePMBUS_CMD_MFR_SPECIFIC_19, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_SPECIFIC_19,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "mfr_specific_20", .id = ePMBUS_CMD_MFR_SPECIFIC_20, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_SPECIFIC_20,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "mfr_specific_21", .id = ePMBUS_CMD_MFR_SPECIFIC_21, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_SPECIFIC_21,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "mfr_specific_22", .id = ePMBUS_CMD_MFR_SPECIFIC_22, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_SPECIFIC_22,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "mfr_specific_23", .id = ePMBUS_CMD_MFR_SPECIFIC_23, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_SPECIFIC_23,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "mfr_specific_24", .id = ePMBUS_CMD_MFR_SPECIFIC_24, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_SPECIFIC_24,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "mfr_specific_25", .id = ePMBUS_CMD_MFR_SPECIFIC_25, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_SPECIFIC_25,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "mfr_specific_26", .id = ePMBUS_CMD_MFR_SPECIFIC_26, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_SPECIFIC_26,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "mfr_specific_27", .id = ePMBUS_CMD_MFR_SPECIFIC_27, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_SPECIFIC_27,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "mfr_specific_28", .id = ePMBUS_CMD_MFR_SPECIFIC_28, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_SPECIFIC_28,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "mfr_specific_29", .id = ePMBUS_CMD_MFR_SPECIFIC_29, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_SPECIFIC_29,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "mfr_specific_30", .id = ePMBUS_CMD_MFR_SPECIFIC_30, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_SPECIFIC_30,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "mfr_specific_31", .id = ePMBUS_CMD_MFR_SPECIFIC_31, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_SPECIFIC_31,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "mfr_specific_32", .id = ePMBUS_CMD_MFR_SPECIFIC_32, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_SPECIFIC_32,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "mfr_specific_33", .id = ePMBUS_CMD_MFR_SPECIFIC_33, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_SPECIFIC_33,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "mfr_specific_34", .id = ePMBUS_CMD_MFR_SPECIFIC_34, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_SPECIFIC_34,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "mfr_specific_35", .id = ePMBUS_CMD_MFR_SPECIFIC_35, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_SPECIFIC_35,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "mfr_specific_36", .id = ePMBUS_CMD_MFR_SPECIFIC_36, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_SPECIFIC_36,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "mfr_specific_37", .id = ePMBUS_CMD_MFR_SPECIFIC_37, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_SPECIFIC_37,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "mfr_specific_38", .id = ePMBUS_CMD_MFR_SPECIFIC_38, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_SPECIFIC_38,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "mfr_specific_39", .id = ePMBUS_CMD_MFR_SPECIFIC_39, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_SPECIFIC_39,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "mfr_specific_40", .id = ePMBUS_CMD_MFR_SPECIFIC_40, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_SPECIFIC_40,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "mfr_specific_41", .id = ePMBUS_CMD_MFR_SPECIFIC_41, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_SPECIFIC_41,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "mfr_specific_42", .id = ePMBUS_CMD_MFR_SPECIFIC_42, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_SPECIFIC_42,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "mfr_specific_43", .id = ePMBUS_CMD_MFR_SPECIFIC_43, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_SPECIFIC_43,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "mfr_specific_44", .id = ePMBUS_CMD_MFR_SPECIFIC_44, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_SPECIFIC_44,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "mfr_specific_45", .id = ePMBUS_CMD_MFR_SPECIFIC_45, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_SPECIFIC_45,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "mfr_specific_cmd", .id = ePMBUS_CMD_MFR_SPECIFIC_COMMAND, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_MFR_SPECIFIC_COMMAND,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "mfr_pmbus_cmd_ext", .id = ePMBUS_CMD_PMBUS_COMMAND_EXT, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = DEVMAN_ATTRS_SIZE_MAX,
        .offset_sz = sizeof(b_u8), .offset = ePMBUS_CMD_PMBUS_COMMAND_EXT,
        .flags = eDEVMAN_ATTRFLAG_HIDDEN
    },
    {
        .name = "measurement_data", .id = ePMBUS_MEASUREMENT_DATA, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = sizeof(pmbus_stats_t), 
        .offset_sz = sizeof(b_u8),
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "init", .id = ePMBUS_INIT, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "config_test", .id = ePMBUS_CONFIGURE_TEST, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "run_test", .id = ePMBUS_RUN_TEST, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, 
        .size = sizeof(pmbus_stats_t), 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
};

/* pmbus register read */
static ccl_err_t _pmbus_reg_read(void *p_priv, b_u32 attrid, 
                                 b_u32 cmd, b_u32 cmd_sz, 
                                 b_u32 idx, b_u8 *p_value, 
                                 b_u32 size)
{
    pmbus_ctx_t   *p_pmbus;
    i2c_route_t   i2c_route;
    b_u16         extra_i2ca;
    b_u32         i, j;

    if ( !p_priv || cmd > PMBUS_MAX_OFFSET || 
         !p_value || idx || size == DEVMAN_ATTRS_SIZE_MAX ) {
        PDEBUG("Error! Invalid input parameters\n");
        PMBUS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_pmbus = (pmbus_ctx_t *)p_priv;     
    memcpy(&i2c_route, &p_pmbus->brdspec.i2c.bus_id, sizeof(i2c_route_t));
    if (p_pmbus->brdspec.cmd[_attrs[attrid].id].extra_i2ca) 
        i2c_route.dev_i2ca = extra_i2ca;
    _ret = devi2ccore_read_buff(&i2c_route, p_pmbus->brdspec.extra_mux_pin, 
                                cmd, cmd_sz, p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to write over spi bus\n");
        PMBUS_ERROR_RETURN(_ret);
    }

    return _ret;
}

/* pmbus register write */
static ccl_err_t _pmbus_reg_write(void *p_priv, b_u32 attrid, 
                                  b_u32 cmd, b_u32 cmd_sz, 
                                  b_u32 idx, b_u8 *p_value, 
                                  b_u32 size)
{
    pmbus_ctx_t   *p_pmbus;
    i2c_route_t   i2c_route;
    b_u16         extra_i2ca;

    if ( !p_priv || cmd > PMBUS_MAX_OFFSET || 
         idx || size == DEVMAN_ATTRS_SIZE_MAX ) {
        PDEBUG("Error! Invalid input parameters\n");
        PMBUS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_pmbus = (pmbus_ctx_t *)p_priv;     
    memcpy(&i2c_route, &p_pmbus->brdspec.i2c.bus_id, sizeof(i2c_route_t));
    if (p_pmbus->brdspec.cmd[_attrs[attrid].id].extra_i2ca) 
        i2c_route.dev_i2ca = extra_i2ca;
    _ret = devi2ccore_write_buff(&i2c_route, p_pmbus->brdspec.extra_mux_pin, 
                                 cmd, cmd_sz, p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to write over spi bus\n");
        PMBUS_ERROR_RETURN(_ret);
    }

    return _ret;
}

/* pmbus buffer read */
static ccl_err_t _pmbus_buf_read(void *p_priv, b_u32 attrid, 
                                 b_u32 cmd, b_u32 cmd_sz, 
                                 b_u32 idx, b_u8 *p_value, 
                                 b_u32 size)
{
    pmbus_ctx_t   *p_pmbus;
    i2c_route_t   i2c_route;
    b_u16         extra_i2ca;

    if ( !p_priv || cmd > PMBUS_MAX_OFFSET || 
         !p_value || idx || size == DEVMAN_ATTRS_SIZE_MAX ) {
        PDEBUG("Error! Invalid input parameters\n");
        PMBUS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_pmbus = (pmbus_ctx_t *)p_priv;     
    memcpy(&i2c_route, &p_pmbus->brdspec.i2c.bus_id, sizeof(i2c_route_t));
    if (p_pmbus->brdspec.cmd[_attrs[attrid].id].extra_i2ca) 
        i2c_route.dev_i2ca = extra_i2ca;
    _ret = devi2ccore_read_buff(&i2c_route, p_pmbus->brdspec.extra_mux_pin, 
                                cmd, cmd_sz, p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to write over spi bus\n");
        PMBUS_ERROR_RETURN(_ret);
    }

    return _ret;
}

/* pmbus string read */
static ccl_err_t _pmbus_str_read(void *p_priv, b_u32 attrid, 
                                 b_u32 cmd, b_u32 cmd_sz, 
                                 b_u32 idx, b_u8 *p_value, 
                                 b_u32 size)
{
    pmbus_ctx_t   *p_pmbus;
    i2c_route_t   i2c_route;
    b_u16         extra_i2ca;

    if ( !p_priv || cmd > PMBUS_MAX_OFFSET || 
         !p_value || idx || size == DEVMAN_ATTRS_SIZE_MAX ) {
        PDEBUG("Error! Invalid input parameters\n");
        PMBUS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_pmbus = (pmbus_ctx_t *)p_priv;     
    memcpy(&i2c_route, &p_pmbus->brdspec.i2c.bus_id, sizeof(i2c_route_t));
    if (p_pmbus->brdspec.cmd[_attrs[attrid].id].extra_i2ca) 
        i2c_route.dev_i2ca = extra_i2ca;
    _ret = devi2ccore_read_buff(&i2c_route, p_pmbus->brdspec.extra_mux_pin, 
                                cmd, cmd_sz, p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to write over spi bus\n");
        PMBUS_ERROR_RETURN(_ret);
    }

    return _ret;
}

static ccl_err_t _pmbus_read_measurement_data(void *priv, b_u8 *buf)
{
    pmbus_ctx_t    *p_pmbus;
    pmbus_stats_t  *pmbus_stats;

    if ( !priv || !buf ) {
        PDEBUG("Error! Invalid input parameters\n");
        PMBUS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_pmbus = (pmbus_ctx_t *)priv;     
    pmbus_stats = (pmbus_stats_t *)buf;

    return CCL_OK;
}

static ccl_err_t _pmbus_test(pmbus_ctx_t *p_pmbus)
{
    b_u32          regval = 0;
    b_i32          mantissa;
    b_i32          exponent;
    b_double64     read_vin = -1;
    b_double64     read_iin = -1;
    b_double64     read_vout = -1;
    b_double64     read_iout = -1;

    if ( !p_pmbus ) {
        PDEBUG("Error! Invalid input parameters\n");
        PMBUS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* Iout */
    if (p_pmbus->brdspec.cmd[ePMBUS_CMD_READ_IOUT].is_supported) {
        _ret |= _pmbus_reg_read(p_pmbus, ePMBUS_CMD_READ_IOUT, ePMBUS_CMD_READ_IOUT, 
                               sizeof(b_u8), 0, (b_u8 *)&regval, 2);
        if ( _ret == CCL_OK ) {
            mantissa = ccl_2s_compl_to_decimal(regval, 11);
            exponent = regval >> 11;
            exponent = ccl_2s_compl_to_decimal(exponent, 5);
            read_iout = mantissa * pow(2, exponent);
            PRINTL("read_iout: regval=0x%04x, mantissa=%d, exponent=%d, read_iout=%.03f\n", 
                   regval, mantissa, exponent, read_iout);
        }
    }
    /* Vout */
    if (p_pmbus->brdspec.cmd[ePMBUS_CMD_READ_VOUT].is_supported) {
        _ret |= _pmbus_reg_read(p_pmbus, ePMBUS_CMD_READ_VOUT, ePMBUS_CMD_READ_VOUT, 
                               sizeof(b_u8), 0, (b_u8 *)&regval, 2);
        if ( _ret == CCL_OK ) {
            mantissa = ccl_2s_compl_to_decimal(regval, 16);
            read_vout = mantissa * pow(2, p_pmbus->brdspec.exponent);
            PRINTL("read_vout: regval=0x%04x, mantissa=%d, exponent=%d, read_vout=%.03f\n", 
                   regval, mantissa, p_pmbus->brdspec.exponent, read_vout);
        }
    }
    /* Vin */
    if (p_pmbus->brdspec.cmd[ePMBUS_CMD_READ_VIN].is_supported) {
        _ret = _pmbus_reg_read(p_pmbus, ePMBUS_CMD_READ_VIN, ePMBUS_CMD_READ_VIN, 
                               sizeof(b_u8), 0, (b_u8 *)&regval, 2);
        if ( _ret == CCL_OK ) {
            mantissa = ccl_2s_compl_to_decimal(regval, 11);
            exponent = regval >> 11;
            exponent = ccl_2s_compl_to_decimal(exponent, 5);
            read_vin = mantissa * pow(2, exponent);
 
            PRINTL("read_vin: regval=0x%04x, mantissa=%d, exponent=%d, read_vin=%.03f\n", 
                   regval, mantissa, exponent, read_vin);
        }
    }
   
    if (_ret) 
        PMBUS_ERROR_RETURN(CCL_BAD_PARAM_ERR);

    PRINTL("Vin=%.03f, Vout=%.03f, Iout=%.03f\n", 
           read_vin, read_vout, read_iout);

    return CCL_OK;

}

static ccl_err_t _fpmbus_cmd_test(devo_t devo, 
                                  __attribute__((unused)) device_cmd_parm_t parms[], 
                                  __attribute__((unused)) b_u16 parms_num)
{
    pmbus_ctx_t    *p_pmbus;

    p_pmbus = devman_device_private(devo);

    _ret = _pmbus_test(p_pmbus);
    if (_ret) {
        PRINTL("Error! %s[%d], error=%i\n", 
               __FUNCTION__, __LINE__, _ret);
        return _ret;
    }

    return CCL_OK;
}

static device_cmd_t _cmds[] = 
{
    { 
        .name = "test", .f_cmd = _fpmbus_cmd_test, .parms_num = 0,
        .help = "Test pmbus" 
    },
};

/* Device instance hw-specific initializations 
 */
static ccl_err_t _fpmbus_add(void *devo, void *brdspec)
{
    pmbus_ctx_t       *p_pmbus;
    pmbus_brdspec_t   *p_brdspec;
    b_u32 i;

    if ( !devo || !brdspec ) {
        PDEBUG("Error! Invalid input parameters\n");
        PMBUS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_pmbus = devman_device_private(devo);
    if ( !p_pmbus ) {
        PDEBUG("NULL context area\n");
        PMBUS_ERROR_RETURN(CCL_FAIL);
    }
    p_brdspec = (pmbus_brdspec_t *)brdspec;
    _ret = devman_init_crc_table(p_brdspec->crc_tbl, DEVBRD_PMBUS_CRC_VAL_NUM);
    if ( _ret ) {
        PDEBUG("devman_init_crc_table error\n");
        PMBUS_ERROR_RETURN(CCL_FAIL);
    }

    PDEBUG("123$=> p_brdspec->i2c.bus_id=%d, dev_i2ca=0x%x\n", 
           p_brdspec->i2c.bus_id, p_brdspec->i2c.dev_i2ca);

    for (i = 0; i < DEVBRD_PMBUS_CRC_VAL_NUM; i++) {
        PDEBUGG("crctbl[%d]=0x%02x\n", i, p_brdspec->crc_tbl[i]);
    }

    p_pmbus->devo = devo;    
    memcpy(&p_pmbus->brdspec, p_brdspec, sizeof(pmbus_brdspec_t));
    for ( i = 0; i < sizeof(_attrs)/sizeof(_attrs[0]); i++ ) {
        if (_attrs[i].size == DEVMAN_ATTRS_SIZE_MAX) {
            if (p_brdspec->cmd[_attrs[i].id].is_supported) 
                _attrs[i].size = p_brdspec->cmd[_attrs[i].id].size;
        }
    }

    return CCL_OK;
}


/* Device instance hw-specific deinitializations 
 */
static ccl_err_t _fpmbus_cut(void *devo)
{
    if ( !devo ) {
        PDEBUG("Error! Invalid input parameters\n");
        PMBUS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    return CCL_OK;
}

/* device type description structure */
static device_driver_t _pmbus_driver = {
    .name = "pmbus",
    .f_add = _fpmbus_add,
    .f_cut = _fpmbus_cut,
    .attrs = _attrs,
    .attrs_num = sizeof(_attrs)/sizeof(_attrs[0]),
    .cmds = _cmds,
    .cmds_num = sizeof(_cmds)/sizeof(_cmds[0]),
    .priv_size = sizeof(pmbus_ctx_t)
}; 

/* Initialize PMBUS device type
 */
ccl_err_t devpmbus_init(void)
{
    /* register device type */
    _ret = devman_driver_register(&_pmbus_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to register driver\n");
        PMBUS_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

/* Deinitialize PMBUS device type
 */
ccl_err_t devpmbus_fini(void)
{
    /* unregister device type */
    _ret = devman_driver_unregister(&_pmbus_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to unregister driver\n");
        PMBUS_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

