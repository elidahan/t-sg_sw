/********************************************************************************/
/**
 * @file axigpio.c
 * @brief AXI GPIO device manager driver
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/

#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <semaphore.h>
#include <linux/limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "spibus.h"
#include "axigpio.h"

//#define AXIGPIO_DEBUG
/* printing/error-returning macros */
#ifdef AXIGPIO_DEBUG
    #define PDEBUG(fmt, args...) \
         ccl_syslog_err("AXIGPIO_DEBUG: %s[%d]: " fmt, __FUNCTION__, __LINE__, ## args)
#else
    #define PDEBUG(fmt, args...)
#endif
#define AXIGPIO_ERROR_RETURN(err) do { \
   ccl_syslog_err("Error! %s[%d], error=%i\n", __FUNCTION__, __LINE__, (err)); \
   return err; \
} while (0)

#define AXIGPIO_REG_READ(context, offset, offset_sz, idx, pvalue, size) do { \
    _ret = _axigpio_reg_read((context), \
                             (offset), \
                             (offset_sz), (idx), \
                             (pvalue), (size)); \
    if ( _ret ) { \
        PDEBUG("_axigpio_reg_read error\n"); \
        AXIGPIO_ERROR_RETURN(_ret); \
    } \
} while (0)

#define AXIGPIO_REG_WRITE(context, offset, offset_sz, idx, pvalue, size) do { \
    _ret = _axigpio_reg_write((context), \
                              (offset), \
                              (offset_sz), (idx), \
                              (pvalue), (size)); \
    if ( _ret ) { \
        PDEBUG("_axigpio_reg_write error\n"); \
        AXIGPIO_ERROR_RETURN(_ret); \
    } \
} while (0)

/** @name Registers
 *
 * Register offsets for this device.
 * @{
 */
#define AXIGPIO_DATA_OFFSET	 0x0   /**< Data register for 1st channel */
#define AXIGPIO_TRI_OFFSET	 0x4   /**< I/O direction reg for 1st channel */
#define AXIGPIO_DATA2_OFFSET 0x8   /**< Data register for 2nd channel */
#define AXIGPIO_TRI2_OFFSET	 0xC   /**< I/O direction reg for 2nd channel */

#define AXIGPIO_GIE_OFFSET	 0x11C /**< Glogal interrupt enable register */
#define AXIGPIO_ISR_OFFSET	 0x120 /**< Interrupt status register */
#define AXIGPIO_IER_OFFSET	 0x128 /**< Interrupt enable register */

#define AXIGPIO_MAX_OFFSET   AXIGPIO_IER_OFFSET

/* @} */

/* The following constant describes the offset of each channels data and
 * tristate register from the base address.
 */
#define AXIGPIO_CHAN_OFFSET  8

/** @name Interrupt Status and Enable Register bitmaps and masks
 *
 * Bit definitions for the interrupt status register and interrupt enable
 * registers.
 * @{
 */
#define AXIGPIO_IR_MASK		0x3 /**< Mask of all bits */
#define AXIGPIO_IR_CH1_MASK	0x1 /**< Mask for the 1st channel */
#define AXIGPIO_IR_CH2_MASK	0x2 /**< Mask for the 2nd channel */
/*@}*/


/** @name Global Interrupt Enable Register bitmaps and masks
 *
 * Bit definitions for the Global Interrupt  Enable register
 * @{
 */
#define AXIGPIO_GIE_GINTR_ENABLE_MASK	0x80000000
/*@}*/


/** @struct axigpio_ctx_t
 *  axigpio context structure
 */
typedef struct axigpio_ctx_t {
    void                *devo;
    axigpio_brdspec_t   brdspec;
    b_bool              is_dual;
} axigpio_ctx_t;

static ccl_err_t    _ret;

/* axigpio register read */
static ccl_err_t _axigpio_reg_read(__attribute__((unused)) void *p_priv, 
                                   b_u32 offset, 
                                   __attribute__((unused)) b_u32 offset_sz, 
                                   b_u32 idx, b_u8 *p_value, b_u32 size)
{
    axigpio_ctx_t *p_axigpio;

    if ( offset > AXIGPIO_MAX_OFFSET || 
         !p_value || idx || size != sizeof(b_u32) ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIGPIO_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_axigpio = (axigpio_ctx_t *)p_priv;     
    offset += p_axigpio->brdspec.offset;
    _ret = devspibus_read_buff(p_axigpio->brdspec.devname, offset, idx, p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to read over spi bus\n");
        AXIGPIO_ERROR_RETURN(CCL_FAIL);
    }
    PDEBUG("p_axigpio->brdspec.offset=0x%x, offset=0x%03x, value=0x%08x\n", 
           p_axigpio->brdspec.offset, offset, *(b_u32 *)p_value);

    return CCL_OK;
}

/* axigpio register write */
static ccl_err_t _axigpio_reg_write(__attribute__((unused)) void *p_priv, 
                                    b_u32 offset, 
                                    __attribute__((unused)) b_u32 offset_sz, 
                                    b_u32 idx, b_u8 *p_value, b_u32 size)
{
    axigpio_ctx_t *p_axigpio;

    if ( offset > AXIGPIO_MAX_OFFSET || 
         !p_value || idx || size != sizeof(b_u32) ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIGPIO_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_axigpio = (axigpio_ctx_t *)p_priv;     
    offset += p_axigpio->brdspec.offset;
    PDEBUG("p_axigpio->brdspec.offset=0x%x, offset=0x%03x, value=0x%08x\n", 
           p_axigpio->brdspec.offset, offset, *(b_u32 *)p_value);

    _ret = devspibus_write_buff(p_axigpio->brdspec.devname, offset, idx, p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to write over spi bus\n");
        AXIGPIO_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

static ccl_err_t _axigpio_init(void *p_priv)
{
    axigpio_ctx_t   *p_axigpio;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIGPIO_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_axigpio = (axigpio_ctx_t *)p_priv;

    return CCL_OK;
}

static ccl_err_t _axigpio_configure_test(void *p_priv)
{
    axigpio_ctx_t   *p_axigpio;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIGPIO_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_axigpio = (axigpio_ctx_t *)p_priv;

    return CCL_OK;
}

static ccl_err_t _axigpio_run_test(void *p_priv, 
                                   __attribute__((unused)) b_u8 *buf, 
                                   __attribute__((unused)) b_u32 size)
{
    axigpio_ctx_t   *p_axigpio;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIGPIO_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_axigpio = (axigpio_ctx_t *)p_priv;

    return CCL_OK;
}


ccl_err_t axigpio_attr_read(void *p_priv, b_u32 attrid, 
                             b_u32 offset, b_u32 offset_sz, 
                             b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || offset > AXIGPIO_MAX_OFFSET || 
         !p_value || idx || size != sizeof(b_u32) ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIGPIO_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch (attrid) {
    case eAXIGPIO_DATA:
    case eAXIGPIO_TRI:
    case eAXIGPIO_DATA2:
    case eAXIGPIO_TRI2:
    case eAXIGPIO_GIE:
    case eAXIGPIO_ISR:
    case eAXIGPIO_IER:
        AXIGPIO_REG_READ(p_priv, offset, offset_sz, idx, p_value, size);
        break;
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK) {
        PDEBUG("Failure in attribute id: %d\n", attrid);
    }

    return _ret;               
}

ccl_err_t axigpio_attr_write(void *p_priv, b_u32 attrid, 
                             b_u32 offset, b_u32 offset_sz, 
                             b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || offset > AXIGPIO_MAX_OFFSET || 
         !p_value || idx || size != sizeof(b_u32) ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIGPIO_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch (attrid) {
    case eAXIGPIO_DATA:
    case eAXIGPIO_TRI:
    case eAXIGPIO_DATA2:
    case eAXIGPIO_TRI2:
    case eAXIGPIO_GIE:
    case eAXIGPIO_ISR:
    case eAXIGPIO_IER:
        AXIGPIO_REG_WRITE(p_priv, offset, offset_sz, idx, p_value, size);
        break;
    case eAXIGPIO_INIT:
        _ret = _axigpio_init(p_priv);
        break;
    case eAXIGPIO_CONFIGURE_TEST:
        _ret = _axigpio_configure_test(p_priv);
        break;
    case eAXIGPIO_RUN_TEST:
        _ret = _axigpio_run_test(p_priv, p_value, size);
        break;
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK) {
        PDEBUG("Failure in attribute id: %d\n", attrid);
    }

    return _ret;               
}

/* axigpio device attributes definition */
static device_attr_t _attrs[] = {
    {
        .name = "name", 
        .type = eDEVMAN_ATTR_STRING, .maxnum = 1, 
        .offset = offsetof(struct axigpio_ctx_t, brdspec.name),
        .size = DEVMAN_DEVNAME_LEN,
        .format = "%s"
    },
    {
        .name = "offset", 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset = offsetof(struct axigpio_ctx_t, brdspec.offset),
        .format = "0x%08x"
    },
    {
        .name = "data", .id = eAXIGPIO_DATA, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIGPIO_DATA_OFFSET
    },
    {
        .name = "tri", .id = eAXIGPIO_TRI, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIGPIO_TRI_OFFSET
    },
    {
        .name = "data2", .id = eAXIGPIO_DATA2, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIGPIO_DATA2_OFFSET
    },
    {
        .name = "tri2", .id = eAXIGPIO_TRI2, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIGPIO_TRI2_OFFSET
    },
    {
        .name = "gie", .id = eAXIGPIO_GIE, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIGPIO_GIE_OFFSET
    },
    {
        .name = "isr", .id = eAXIGPIO_ISR, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIGPIO_ISR_OFFSET
    },
    {
        .name = "ier", .id = eAXIGPIO_IER, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIGPIO_IER_OFFSET
    },
    {
        .name = "init", .id = eAXIGPIO_INIT, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "config_test", .id = eAXIGPIO_CONFIGURE_TEST, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "run_test", .id = eAXIGPIO_RUN_TEST, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, 
        .size = sizeof(board_dev_stats_t), 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
};

/**
 * Set the input/output direction of all discrete signals for the specified
 * GPIO channel.
 *
 * @param[in] p_axigpio is a pointer to an axigpio_ctx_t 
 *       instance to be worked on.
 * @param[in] channel contains the channel of the GPIO (1 or 2) 
 *       to operate on.
 * @param[in] mask is a bitmask specifying which discretes are 
 *      input and which are output. Bits set to 0 are output and
 *      bits set to 1 are input.
 *
 * @return	error code
 *
 * @note 
 * The hardware must be built for dual channels if this function
 * is used with any channel other than 1
 */
static ccl_err_t _axigpio_set_data_direction(axigpio_ctx_t *p_axigpio, b_u32 channel, b_u32 mask)
{
    b_u32 regval;
    int offset;

    if ( !p_axigpio || ((channel != 1) && 
                        ((channel == 2) && !p_axigpio->is_dual)) ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIGPIO_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    offset = ((channel - 1) * AXIGPIO_CHAN_OFFSET) + AXIGPIO_TRI_OFFSET;
    regval = mask;
    AXIGPIO_REG_WRITE(p_axigpio, offset, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/**
 * Get the input/output direction of all discrete signals for the specified
 * GPIO channel.
 *
 * @param[in] p_axigpio is a pointer to an axigpio_ctx_t 
 *       instance to be worked on.
 * @param[in] channel contains the channel of the GPIO (1 or 2) 
 *       to operate on.
 * @param[out] p_dir is the memory that is updated with bitmask
 *       specifying which discretes are input and which are
 *       output. Bits set to 0 are output and bits set to 1 are
 *       input.
 *
 * @return error code
 *
 * @note
 * The hardware must be built for dual channels if this function is used
 * with any channel other than 1
 */
static ccl_err_t _axigpio_get_data_direction(axigpio_ctx_t *p_axigpio, b_u32 channel, b_u32 *p_dir)
{
    int offset;

    if ( !p_axigpio || !p_dir || ((channel != 1) && 
                                  ((channel == 2) && !p_axigpio->is_dual)) ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIGPIO_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    offset = ((channel - 1) * AXIGPIO_CHAN_OFFSET) + AXIGPIO_TRI_OFFSET;
    AXIGPIO_REG_READ(p_axigpio, offset, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)p_dir, sizeof(b_u32));

    return CCL_OK;
}

/**
 * Read state of data register for the specified GPIO channnel.
 *
 * @param[in] p_axigpio is a pointer to an axigpio_ctx_t
 *        instance to be worked on.
 * @param[in] channel contains the channel of the GPIO (1 or 2)
 *        to operate on.
 * @param[out] p_state is the memory that is updated with
 *        current copy of the data register
 *
 * @return	error code
 *
 * @note 
 * The hardware must be built for dual channels if this function
 * is used with any channel other than 1
 */
static ccl_err_t _axigpio_data_read(axigpio_ctx_t *p_axigpio, b_u32 channel, b_u32 *p_data)
{
    int offset; 

    if ( !p_axigpio || !p_data || ((channel != 1) && 
                                   ((channel == 2) && !p_axigpio->is_dual)) ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIGPIO_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    offset = ((channel - 1) * AXIGPIO_CHAN_OFFSET) + AXIGPIO_DATA_OFFSET;
    AXIGPIO_REG_READ(p_axigpio, offset, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)p_data, sizeof(b_u32));

    return CCL_OK;
}

/**
* Write to discretes register for the specified GPIO channel.
*
* @param[in] p_axigpio is a pointer to an axigpio_ctx_t instance
*       to be worked on.
* @param[in] channel contains the channel of the GPIO (1 or 2)
*       to operate on.
* @param[in] data is the value to be written to the discretes
*       register.
*
* @return	error code
*
* @note 
* The hardware must be built for dual channels if this function 
* is  used with any channel other than 1 
*/
static ccl_err_t _axigpio_data_write(axigpio_ctx_t *p_axigpio, b_u32 channel, b_u32 data)
{
    b_u32 regval;
    int offset; 

    if ( !p_axigpio || ((channel != 1) && 
                        ((channel == 2) && !p_axigpio->is_dual)) ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIGPIO_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    offset = ((channel - 1) * AXIGPIO_CHAN_OFFSET) + AXIGPIO_DATA_OFFSET;
    regval = data;
    AXIGPIO_REG_WRITE(p_axigpio, offset, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/**
 * Set output pins to logic 1 for the specified GPIO channel.
 *
 * @param[in] p_axigpio is a pointer to an axigpio_ctx_t instance
 *       to be worked on.
 * @param[in] channel contains the channel of the GPIO (1 or 2)
 *       to operate on.
 *  @param[in] mask is the set of bits that will be set to 1 in
 *       the data register. All other bits in the data register
 *       are unaffected.
 *
 * @return	error code
 *
 * @note
 * The hardware must be built for dual channels if this function 
 *  is used with any channel other than 1
 * This API can only be used if the GPIO_IO ports in the IP are used for
 * connecting to the external output ports.
 */
static ccl_err_t _axigpio_pin_set(axigpio_ctx_t *p_axigpio, b_u32 channel, b_u32 mask)
{
    b_u32 regval;
    int offset; 

    if ( !p_axigpio || ((channel != 1) && 
                        ((channel == 2) && !p_axigpio->is_dual)) ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIGPIO_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    offset = ((channel - 1) * AXIGPIO_CHAN_OFFSET) + AXIGPIO_DATA_OFFSET;
    AXIGPIO_REG_READ(p_axigpio, offset, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));
    regval |= mask;
    AXIGPIO_REG_WRITE(p_axigpio, offset, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/**
 * Set output pins to logic 0 for the specified GPIO channel.
 *
 * @param[in] p_axigpio is a pointer to an axigpio_ctx_t instance
 *       to be worked on.
 * @param[in] channel contains the channel of the GPIO (1 or 2)
 *       to operate on.
 *  @param[in] mask is the set of bits that will be set to 0 in
 *       the data register. All other bits in the data register
 *       are unaffected.
 *
 * @return	error code
 *
 * @note
 * The hardware must be built for dual channels if this function 
 *  is used with any channel other than 1
 * This API can only be used if the GPIO_IO ports in the IP are used for
 * connecting to the external output ports.
 */
static ccl_err_t _axigpio_pin_clear(axigpio_ctx_t *p_axigpio, b_u32 channel, b_u32 mask)
{
    b_u32 regval;
    int offset; 

    if ( !p_axigpio || ((channel != 1) && 
                        ((channel == 2) && !p_axigpio->is_dual)) ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIGPIO_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    offset = ((channel - 1) * AXIGPIO_CHAN_OFFSET) + AXIGPIO_DATA_OFFSET;
    AXIGPIO_REG_READ(p_axigpio, offset, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));
    regval &= ~mask;
    AXIGPIO_REG_WRITE(p_axigpio, offset, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

static ccl_err_t _faxigpio_cmd_set_data_direction(devo_t devo, 
                                                  device_cmd_parm_t parms[], 
                                                  __attribute__((unused)) b_u16 parms_num)
{
    axigpio_ctx_t *p_axigpio = devman_device_private(devo);

    _ret = _axigpio_set_data_direction(p_axigpio, 
                                       parms[0].value, 
                                       parms[1].value);
    if (_ret) {
        PDEBUG("_axigpio_set_data_direction error\n");
        AXIGPIO_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

static device_cmd_t _cmds[] = 
{
    { 
        .name = "set_data_direction", .f_cmd = _faxigpio_cmd_set_data_direction, .parms_num = 2,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "channel", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x", .flags = eDEVMAN_ATTRFLAG_OPTIONAL }, 
            { .name = "mask", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%d", .flags = eDEVMAN_ATTRFLAG_OPTIONAL }, 
        }, 
        .help = "Set data direction" 
    },
};

/* Device instance hw-specific initializations 
 */
static ccl_err_t _faxigpio_add(void *devo, void *brdspec) 
{
    axigpio_ctx_t       *p_axigpio;
    axigpio_brdspec_t   *p_brdspec;
    b_u32               mask = 0; 
    b_u32               channel = 1;
    b_u32               i, j;

    if ( !devo || !brdspec ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIGPIO_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_axigpio = devman_device_private(devo);
    if ( !p_axigpio ) {
        PDEBUG("NULL context area\n");
        AXIGPIO_ERROR_RETURN(CCL_FAIL);
    }
    p_brdspec = (axigpio_brdspec_t *)brdspec;
    /* back reference the device manager object handle */    
    p_axigpio->devo = devo;    
    /* save the board specific info */
    memcpy(&p_axigpio->brdspec, p_brdspec, sizeof(axigpio_brdspec_t));
    for (i = 0; i < p_brdspec->pins_num; i++) {
        if (p_brdspec->direction[i] == eDEVBRD_AXIGPIO_DIR_INPUT) {
            j = i;
            if (i >= DEVBRD_GPIO_PIN_PER_CHANNEL_MAXNUM) {
                channel = 2;
                j = i - DEVBRD_GPIO_PIN_PER_CHANNEL_MAXNUM;
            }
            mask |= (1 << j);
        }
    }
#if 0
    _ret = _axigpio_set_data_direction(p_axigpio, channel, mask);
    if (_ret) {
        PDEBUG("_axigpio_set_data_direction error\n");
        AXIGPIO_ERROR_RETURN(CCL_FAIL);
    }
#endif

    return CCL_OK;
}

/* Device instance hw-specific deinitializations 
 */
static ccl_err_t _faxigpio_cut(void *devo)
{
    axigpio_ctx_t  *p_axigpio;

    if ( !devo ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIGPIO_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_axigpio = devman_device_private(devo);
    if ( !p_axigpio ) {
        PDEBUG("NULL context area\n");
        AXIGPIO_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

/* device type description structure */
static device_driver_t _axigpio_driver = {
    .name = "axigpio",
    .f_add = _faxigpio_add,
    .f_cut = _faxigpio_cut,
    .attrs = _attrs,
    .attrs_num = sizeof(_attrs)/sizeof(_attrs[0]),
    .cmds = _cmds,
    .cmds_num = sizeof(_cmds)/sizeof(_cmds[0]),
    .priv_size = sizeof(axigpio_ctx_t)
}; 

/* Initialize AXIGPIO device type
 */
ccl_err_t devaxigpio_init(void)
{
    /* register device type */
    _ret = devman_driver_register(&_axigpio_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to register driver\n");
        AXIGPIO_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}

/* Deinitialize AXIGPIO device type
 */
ccl_err_t devaxigpio_fini(void)
{
    /* unregister device type */
    _ret = devman_driver_unregister(&_axigpio_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to unregister driver\n");
        AXIGPIO_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}
