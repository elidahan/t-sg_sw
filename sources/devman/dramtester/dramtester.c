/********************************************************************************/
/**
 * @file dramtester.c
 * @brief DRAMTESTER sensors
 * device manager driver
 * @copyright 2019 CELARE. All rights reserved.
 */
/********************************************************************************/

#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <semaphore.h>
#include <linux/limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "dramtester.h"

#define DRAMTESTER_DEBUG
/* pointing/error-returning macros */
#ifdef DRAMTESTER_DEBUG
 #define PDEBUG(fmt, args...) \
         ccl_syslog_err("DRAMTESTER_DEBUG:  %s[%d]: " fmt, __FUNCTION__, __LINE__, ## args)
#else
 #define PDEBUG(fmt, args...)
#endif
#define DRAMTESTER_ERROR_RETURN(err) do { \
   ccl_syslog_err("Error! %s[%d], error=%i\n", __FUNCTION__, __LINE__, (err)); \
   return err; \
} while (0)

//					 0:		reset Test Generator
//					 1:		start boot mode (0->1 transition)
//					 2:		stop boot mode (0->1 transition)
//					 3:		start custom mode (0->1 transtion)
//					 4:		stop custom mode (0->1 transtion)
//					 5:		start PRBS mode (0->1 transtion)
//					 6:		stop PRBS mode (0->1 transtion)
//					 8-15:	read register select
//					 16-17:	slot select
//					read registers:
//					 00:	status. Contains the following bits:
//							 0	DDR ready
//							 1:	boot mode active
//							 2:	boot mode done
//							 3:	custom mode active
//							 4:	custom mode done
//							 5:	PRBS mode active
//							 6:	PRBS mode done
//							 7:	mismatch error
//							 8:	wr resp error
//							 9: rd resp error
//							 12-13: error status (00 - none, 01 - write, 10 - read)
//					 01:	error address
//					 16-31: expected data
//					 32-47:	actual data

#define DRAMTESTER_RESET_TEST_GEN_MASK                 0x00000001 
#define DRAMTESTER_START_BOOT_MODE_MASK                0x00000002 
#define DRAMTESTER_STOP_BOOT_MODE_MASK                 0x00000004 
#define DRAMTESTER_START_CUSTOM_MODE_MASK              0x00000008 
#define DRAMTESTER_STOP_CUSTOM_MODE_MASK               0x00000010 
#define DRAMTESTER_START_PRBS_MODE_MASK                0x00000020 
#define DRAMTESTER_STOP_PRBS_MODE_MASK                 0x00000040 
                                                                  
#define DRAMTESTER_READ_REG_SELECT_MASK                0x00000F00 
#define DRAMTESTER_SLOT_SELECT_MASK                    0x00030000 
#define DRAMTESTER_SLOT_SHIFT                          16         
                                                                  
#define DRAMTESTER_STATUS_OFFSET                       0x00       
#define DRAMTESTER_ERROR_ADDRESS_OFFSET                0x01       

#define DRAMTESTER_STATUS_DDR_READY_MASK               0x00000001 
#define DRAMTESTER_STATUS_BOOT_MODE_ACTIVE_MASK        0x00000002 
#define DRAMTESTER_STATUS_BOOT_MODE_DONE_MASK          0x00000004 
#define DRAMTESTER_STATUS_CUSTOM_MODE_ACTIVE_MASK      0x00000008 
#define DRAMTESTER_STATUS_CUSTOM_MODE_DONE_MASK        0x00000010 
#define DRAMTESTER_STATUS_PRBS_MODE_ACTIVE_MASK        0x00000020 
#define DRAMTESTER_STATUS_PRBS_MODE_DONE_MASK          0x00000040 
#define DRAMTESTER_STATUS_MISMATCH_ERROR_MASK          0x00000080 
#define DRAMTESTER_STATUS_WRITE_RESP_ERROR_MASK        0x00000100 
#define DRAMTESTER_STATUS_READ_RESP_ERROR_MASK         0x00000200 
#define DRAMTESTER_STATUS_WRITE_ERROR_MASK             0x00001000
#define DRAMTESTER_STATUS_READ_ERROR_MASK              0x00002000

#define DRAMTESTER_STATUS_ERROR_MASK                   (DRAMTESTER_STATUS_MISMATCH_ERROR_MASK   | \
                                                        DRAMTESTER_STATUS_WRITE_RESP_ERROR_MASK | \
                                                        DRAMTESTER_STATUS_READ_RESP_ERROR_MASK  | \
                                                        DRAMTESTER_STATUS_WRITE_ERROR_MASK      | \
                                                        DRAMTESTER_STATUS_READ_ERROR_MASK)
/** @struct dramtester_ctx_t
 *  dramtester context structure
 */
typedef struct dramtester_ctx_t {
    void                     *devo;
    dramtester_brdspec_t     brdspec;
} dramtester_ctx_t;

static ccl_err_t  _ret;

static ccl_err_t _dramtester_run_test(void *p_priv, b_u8 *buf, b_u32 size)
{
    dramtester_ctx_t     *p_dramtester;
    b_u32                regval;
    b_u32                offset;
    dram_stats_t         *stats;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        DRAMTESTER_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_dramtester = (dramtester_ctx_t *)p_priv; 

    if (size == sizeof(dram_stats_t)) {
        stats = (dram_stats_t *)buf;
    }

    strncpy(stats->boot_test_status, "active", DEVMAN_DEVNAME_LEN);
    strncpy(stats->custom_test_status, "active", DEVMAN_DEVNAME_LEN);
    strncpy(stats->prbs_test_status, "active", DEVMAN_DEVNAME_LEN);

    offset = p_dramtester->brdspec.offset; 

    /* Write zeros */
    regval = p_dramtester->brdspec.slot << DRAMTESTER_SLOT_SHIFT;
    PDEBUG("write offset=0x%x, value=0x%08x\n", offset, regval);
    _ret |= devspibus_write_buff(p_dramtester->brdspec.devname, offset, 
                                 0, (b_u8 *)&regval, sizeof(b_u32));
    /* Read status */
    _ret |= devspibus_read_buff(p_dramtester->brdspec.devname, offset+8, 
                                0, (b_u8 *)&regval, sizeof(b_u32));
    PDEBUG("read offset=0x%x, value=0x%08x\n", offset+8, regval);

    if (regval & DRAMTESTER_STATUS_PRBS_MODE_ACTIVE_MASK) {
        /* Stop prbs mode test */
        regval = p_dramtester->brdspec.slot << DRAMTESTER_SLOT_SHIFT |
                 DRAMTESTER_STOP_PRBS_MODE_MASK;
        PDEBUG("write offset=0x%x, value=0x%08x\n", offset, regval);
        _ret |= devspibus_write_buff(p_dramtester->brdspec.devname, offset, 
                                     0, (b_u8 *)&regval, sizeof(b_u32));
        /* Write zeros */
        regval = p_dramtester->brdspec.slot << DRAMTESTER_SLOT_SHIFT;
        PDEBUG("write offset=0x%x, value=0x%08x\n", offset, regval);
        _ret |= devspibus_write_buff(p_dramtester->brdspec.devname, offset, 
                                     0, (b_u8 *)&regval, sizeof(b_u32));
        /* Read status */
        _ret |= devspibus_read_buff(p_dramtester->brdspec.devname, offset+8, 
                                    0, (b_u8 *)&regval, sizeof(b_u32));
        PDEBUG("read offset=0x%x, value=0x%08x\n", offset+8, regval);

        if (regval & DRAMTESTER_STATUS_PRBS_MODE_DONE_MASK) {
            if (!(regval & DRAMTESTER_STATUS_ERROR_MASK)) 
                strncpy(stats->prbs_test_status, "passed", DEVMAN_DEVNAME_LEN);
            else 
                strncpy(stats->prbs_test_status, "failed", DEVMAN_DEVNAME_LEN);    
        }
    }

    /* Reset Test Generator */
    regval = p_dramtester->brdspec.slot << DRAMTESTER_SLOT_SHIFT | 
             DRAMTESTER_RESET_TEST_GEN_MASK;

    PDEBUG("write offset=0x%x, value=0x%08x\n", offset, regval);
    _ret = devspibus_write_buff(p_dramtester->brdspec.devname, offset, 
                                0, (b_u8 *)&regval, sizeof(b_u32));
    /* Run boot mode test */
    regval = p_dramtester->brdspec.slot << DRAMTESTER_SLOT_SHIFT |
             DRAMTESTER_START_BOOT_MODE_MASK;
    PDEBUG("write offset=0x%x, value=0x%08x\n", offset, regval);
    _ret |= devspibus_write_buff(p_dramtester->brdspec.devname, offset, 
                                 0, (b_u8 *)&regval, sizeof(b_u32));
    /* Write zeros */
    regval = p_dramtester->brdspec.slot << DRAMTESTER_SLOT_SHIFT;
    PDEBUG("write offset=0x%x, value=0x%08x\n", offset, regval);
    _ret |= devspibus_write_buff(p_dramtester->brdspec.devname, offset, 
                                 0, (b_u8 *)&regval, sizeof(b_u32));
    /* Read status */
    _ret |= devspibus_read_buff(p_dramtester->brdspec.devname, offset+8, 
                                0, (b_u8 *)&regval, sizeof(b_u32));
    PDEBUG("read offset=0x%x, value=0x%08x\n", offset+8, regval);

    if (regval & DRAMTESTER_STATUS_BOOT_MODE_DONE_MASK) {
        if (!(regval & DRAMTESTER_STATUS_ERROR_MASK))  
            strncpy(stats->boot_test_status, "passed", DEVMAN_DEVNAME_LEN);
        else 
            strncpy(stats->boot_test_status, "failed", DEVMAN_DEVNAME_LEN);    
    }

    /* Run custom mode test */
    regval = p_dramtester->brdspec.slot << DRAMTESTER_SLOT_SHIFT |
             DRAMTESTER_START_CUSTOM_MODE_MASK;
    PDEBUG("write offset=0x%x, value=0x%08x\n", offset, regval);
    _ret |= devspibus_write_buff(p_dramtester->brdspec.devname, offset, 
                                 0, (b_u8 *)&regval, sizeof(b_u32));
    /* Write zeros */
    regval = p_dramtester->brdspec.slot << DRAMTESTER_SLOT_SHIFT;
    PDEBUG("write offset=0x%x, value=0x%08x\n", offset, regval);
    _ret |= devspibus_write_buff(p_dramtester->brdspec.devname, offset, 
                                 0, (b_u8 *)&regval, sizeof(b_u32));
    /* Read status */
    _ret |= devspibus_read_buff(p_dramtester->brdspec.devname, offset+8, 
                                0, (b_u8 *)&regval, sizeof(b_u32));
    PDEBUG("read offset=0x%x, value=0x%08x\n", offset+8, regval);

    if (regval & DRAMTESTER_STATUS_CUSTOM_MODE_DONE_MASK) {
        if (!(regval & DRAMTESTER_STATUS_ERROR_MASK))  
            strncpy(stats->custom_test_status, "passed", DEVMAN_DEVNAME_LEN);
        else 
            strncpy(stats->custom_test_status, "failed", DEVMAN_DEVNAME_LEN);    
    }

    /* Run prbs mode test */
    regval = p_dramtester->brdspec.slot << DRAMTESTER_SLOT_SHIFT |
             DRAMTESTER_START_PRBS_MODE_MASK;
    PDEBUG("write offset=0x%x, value=0x%08x\n", offset, regval);
    _ret |= devspibus_write_buff(p_dramtester->brdspec.devname, offset, 
                                 0, (b_u8 *)&regval, sizeof(b_u32));
    /* Write zeros */
    regval = p_dramtester->brdspec.slot << DRAMTESTER_SLOT_SHIFT;
    PDEBUG("write offset=0x%x, value=0x%08x\n", offset, regval);
    _ret |= devspibus_write_buff(p_dramtester->brdspec.devname, offset, 
                                 0, (b_u8 *)&regval, sizeof(b_u32));
    /* Read status */
    _ret |= devspibus_read_buff(p_dramtester->brdspec.devname, offset+8, 
                                0, (b_u8 *)&regval, sizeof(b_u32));
    PDEBUG("read offset=0x%x, value=0x%08x\n", offset+8, regval);

    if ( _ret ) 
        DRAMTESTER_ERROR_RETURN(_ret);


    return CCL_OK;
}

ccl_err_t dramtester_attr_write(void *p_priv, b_u32 attrid, 
                                b_u32 offset, b_u32 offset_sz, 
                                b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || !p_value || idx ) {
        PDEBUG("Error! Invalid input parameters\n");
        DRAMTESTER_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch ( attrid ) {
    case eDRAMTESTER_RUN_TEST:
        _ret = _dramtester_run_test(p_priv, p_value, size);
        break;
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK) {
        PDEBUG("Failure in attribute id: %d\n", attrid);
    }

    return _ret;
}


/* dramtester device attributes definition */
static device_attr_t _attrs[] = {
    {
        .name = "run_test", .id = eDRAMTESTER_RUN_TEST, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, 
        .size = sizeof(board_dev_stats_t), 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
};

/* Device instance hw-specific initializations 
 */
static ccl_err_t _fdramtester_add(void *devo, void *brdspec) {
    dramtester_ctx_t       *p_dramtester;
    dramtester_brdspec_t   *p_brdspec;

    if ( !devo || !brdspec ) {
        PDEBUG("Error! Invalid input parameters\n");
        DRAMTESTER_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_dramtester = devman_device_private(devo);
    if ( !p_dramtester ) {
        PDEBUG("NULL context area\n");
        DRAMTESTER_ERROR_RETURN(CCL_FAIL);
    }
    p_brdspec = (dramtester_brdspec_t *)brdspec;
    /* back reference the device manager object handle */    
    p_dramtester->devo = devo;    
    /* save the board specific info */
    memcpy(&p_dramtester->brdspec, p_brdspec, sizeof(dramtester_brdspec_t));

    return CCL_OK;
}

/* Device instance hw-specific deinitializations 
 */
static ccl_err_t _fdramtester_cut(void *devo)
{
    dramtester_ctx_t  *p_dramtester;

    if ( !devo ) {
        PDEBUG("Error! Invalid input parameters\n");
        DRAMTESTER_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_dramtester = devman_device_private(devo);
    if ( !p_dramtester ) {
        PDEBUG("NULL context area\n");
        DRAMTESTER_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

/* device type description structure */
static device_driver_t _dramtester_driver = {
    .name = "dramtester",
    .f_add = _fdramtester_add,
    .f_cut = _fdramtester_cut,
    .attrs = _attrs,
    .attrs_num = sizeof(_attrs)/sizeof(_attrs[0]),
    .priv_size = sizeof(dramtester_ctx_t)
}; 

/* Initialize DRAMTESTER device type
 */
ccl_err_t devdramtester_init(void)
{
    /* register device type */
    _ret = devman_driver_register(&_dramtester_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to register driver\n");
        DRAMTESTER_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}

/* Deinitialize DRAMTESTER device type
 */
ccl_err_t devdramtester_fini(void)
{
    /* unregister device type */
    _ret = devman_driver_unregister(&_dramtester_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to unregister driver\n");
        DRAMTESTER_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}
