/********************************************************************************/
/**
 * @file hses10g.c
 * @brief 10G/25G High Speed Ethernet Subsystem (HSES10G) device manager driver
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/

#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <semaphore.h>
#include <linux/limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devman_port.h"
#include "devman_brd_gen.h"
#include "devboard.h"
#include "spibus.h"
#include "hses10g.h"


#define HSES10G_DEBUG
/* printing/error-returning macros */
#ifdef HSES10G_DEBUG
 #define PDEBUG(fmt, args...) \
         ccl_syslog_err("HSES10G_DEBUG: %s[%d]: " fmt, __FUNCTION__, __LINE__, ## args)
#else
 #define PDEBUG(fmt, args...)
#endif
#define HSES10G_ERROR_RETURN(err) do { \
   ccl_syslog_err("Error! %s[%d], error=%i\n", __FUNCTION__, __LINE__, (err)); \
   return err; \
} while (0)
#define HSES10G_CHECK_ERROR_RETURN(err) do { \
    if (err != CCL_OK) { \
        ccl_syslog_err("Error! %s[%d], error=%i\n", __FUNCTION__, __LINE__, (err)); \
        return err; \
    } \
} while (0)

#define HSES10G_REG_READ(context, offset, offset_sz, idx, pvalue, size) do { \
    _ret = _hses10g_reg_read((context), \
                             (offset), \
                             (offset_sz), (idx), \
                             (pvalue), (size)); \
    if ( _ret ) { \
        PDEBUG("_hses10g_reg_read error\n"); \
        HSES10G_ERROR_RETURN(_ret); \
    } \
} while (0)

#define HSES10G_REG_WRITE(context, offset, offset_sz, idx, pvalue, size) do { \
    _ret = _hses10g_reg_write((context), \
                              (offset), \
                              (offset_sz), (idx), \
                              (pvalue), (size)); \
    if ( _ret ) { \
        PDEBUG("_hses10g_reg_write error\n"); \
        HSES10G_ERROR_RETURN(_ret); \
    } \
} while (0)

/* HSES10G registers offsets */

/* Configuration Register Map */
#define HSES10G_GT_RESET_REG                                    0x0000 /**< Reset for the serial transceiver (GT) */
#define HSES10G_RESET_REG                                       0x0004 /**< Reset for the RX/TX circuits */
#define HSES10G_MODE_REG                                        0x0008 /**< Local loopback and the mode to read statistics counters configuration */
#define HSES10G_CONFIGURATION_TX_REG1                           0x000C /**< TX block configuration */
#define HSES10G_CONFIGURATION_RX_REG1                           0x0014 /**< RX block configuration */
#define HSES10G_CONFIGURATION_RX_MTU                            0x0018 /**< RX MTU configuration*/
#define HSES10G_CONFIGURATION_VL_LENGTH_REG                     0x001C /**< Normally set to 20479. The normal value is equivalent to (16383*5-4)=81916*/
#define HSES10G_TICK_REG                                        0x0020 /**< Used to read the statistics counters */                                         
#define HSES10G_CONFIGURATION_REVISION_REG                      0x0024 /**< Describes configuration revision */                                             
#define HSES10G_CONFIGURATION_TX_TEST_PAT_SEED_A_LSB            0x0028 /**< Corresponds to MDIO registers 3.34 through 3.37 as defined in Clause 45 LSB */  
#define HSES10G_CONFIGURATION_TX_TEST_PAT_SEED_A_MSB            0x002C /**< Corresponds to MDIO registers 3.34 through 3.37 as defined in Clause 45 MSB */  
#define HSES10G_CONFIGURATION_TX_TEST_PAT_SEED_B_LSB            0x0030 /**< Corresponds to MDIO registers 3.38 through 3.41 as defined in Clause 45 LSB */  
#define HSES10G_CONFIGURATION_TX_TEST_PAT_SEED_B_MSB            0x0034 /**< Corresponds to MDIO registers 3.38 through 3.41 as defined in Clause 45 MSB */   
#define HSES10G_CONFIGURATION_1588_REG                          0x0038 /**< PTP configuration */
#define HSES10G_CONFIGURATION_TX_FLOW_CONTROL_REG1              0x0040 /**< TX pause enable signal for the corresponding priority */
#define HSES10G_CONFIGURATION_TX_FLOW_CONTROL_REFRESH_REG1      0x0044 /**< Configuration of retransmission time of pause packets for priorities 0-1 */
#define HSES10G_CONFIGURATION_TX_FLOW_CONTROL_REFRESH_REG2      0x0048 /**< Configuration of retransmission time of pause packets for priorities 2-3 */
#define HSES10G_CONFIGURATION_TX_FLOW_CONTROL_REFRESH_REG3      0x004C /**< Configuration of retransmission time of pause packets for priorities 4-5 */
#define HSES10G_CONFIGURATION_TX_FLOW_CONTROL_REFRESH_REG4      0x0050 /**< Configuration of retransmission time of pause packets for priorities 6-7 in priority based pause operation */
#define HSES10G_CONFIGURATION_TX_FLOW_CONTROL_REFRESH_REG5      0x0054 /**< Configuration of retransmission time of pause packets in the global pause operation */
#define HSES10G_CONFIGURATION_TX_FLOW_CONTROL_QUANTA_REG1       0x0058 /**< Quanta to be transmitted for priorities 0-1 in priority based pause operation */
#define HSES10G_CONFIGURATION_TX_FLOW_CONTROL_QUANTA_REG2       0x005C /**< Quanta to be transmitted for priorities 2-3 in priority based pause operation */
#define HSES10G_CONFIGURATION_TX_FLOW_CONTROL_QUANTA_REG3       0x0060 /**< Quanta to be transmitted for priorities 4-5 in priority based pause operation */
#define HSES10G_CONFIGURATION_TX_FLOW_CONTROL_QUANTA_REG4       0x0064 /**< Quanta to be transmitted for priorities 6-7 in priority based pause operation */
#define HSES10G_CONFIGURATION_TX_FLOW_CONTROL_QUANTA_REG5       0x0068 /**< Quanta to be transmitted in global pause operation */
#define HSES10G_CONFIGURATION_TX_FLOW_CONTROL_PPP_ETYPE_OP_REG  0x006C /**< Ethertype and Opcode for transmitting priority pause packets */
#define HSES10G_CONFIGURATION_TX_FLOW_CONTROL_GPP_ETYPE_OP_REG  0x0070 /**< Ethertype and Opcode for transmitting global pause packets */
#define HSES10G_CONFIGURATION_TX_FLOW_CONTROL_GPP_DA_REG_LSB    0x0074 /**< Destination address for transmitting global pause packets LSB */
#define HSES10G_CONFIGURATION_TX_FLOW_CONTROL_GPP_DA_REG_MSB    0x0078 /**< Destination address for transmitting global pause packets MSB */
#define HSES10G_CONFIGURATION_TX_FLOW_CONTROL_GPP_SA_REG_LSB    0x007C /**< Source address for transmitting global pause packets LSB */
#define HSES10G_CONFIGURATION_TX_FLOW_CONTROL_GPP_SA_REG_MSB    0x0080 /**< Source address for transmitting global pause packets MSB */
#define HSES10G_CONFIGURATION_TX_FLOW_CONTROL_PPP_DA_REG_LSB    0x0084 /**< Destination address for transmitting priority based pause packets LSB */   
#define HSES10G_CONFIGURATION_TX_FLOW_CONTROL_PPP_DA_REG_MSB    0x0088 /**< Destination address for transmitting priority based pause packets MSB */   
#define HSES10G_CONFIGURATION_TX_FLOW_CONTROL_PPP_SA_REG_LSB    0x008C /**< Source address for transmitting priority based pause packets LSB */        
#define HSES10G_CONFIGURATION_TX_FLOW_CONTROL_PPP_SA_REG_MSB    0x0090 /**< Source address for transmitting priority based pause packets MSB */        
#define HSES10G_CONFIGURATION_RX_FLOW_CONTROL_REG1              0x0094 /**< RX Flow Control configuration */
#define HSES10G_CONFIGURATION_RX_FLOW_CONTROL_REG2              0x0098 /**< RX Flow Control configuration */
#define HSES10G_CONFIGURATION_RX_FLOW_CONTROL_PPP_ETYPE_OP_REG  0x009C /**< Ethertype and Opcode for processing priority pause packets */
#define HSES10G_CONFIGURATION_RX_FLOW_CONTROL_GPP_ETYPE_OP_REG  0x00A0 /**< Ethertype and Opcode for processing global pause packets */
#define HSES10G_CONFIGURATION_RX_FLOW_CONTROL_GCP_PCP_TYPE_REG  0x00A4 /**< Ethertype for global and priority control processing */
#define HSES10G_CONFIGURATION_RX_FLOW_CONTROL_PCP_OP_REG        0x00A8 /**< Minimum and Maximum priority control opcode values */
#define HSES10G_CONFIGURATION_RX_FLOW_CONTROL_GCP_OP_REG        0x00AC /**< Minimum and Maximum global control opcode values */
#define HSES10G_CONFIGURATION_RX_FLOW_CONTROL_DA_REG1_LSB       0x00B0 /**< Unicast destination address for pause processing LSB */
#define HSES10G_CONFIGURATION_RX_FLOW_CONTROL_DA_REG1_MSB       0x00B4 /**< Unicast destination address for pause processing MSB */
#define HSES10G_CONFIGURATION_RX_FLOW_CONTROL_DA_REG2_LSB       0x00B8 /**< Multicast destination address for pause processing LSB */
#define HSES10G_CONFIGURATION_RX_FLOW_CONTROL_DA_REG2_MSB       0x00BC /**< Multicast destination address for pause processing MSB */
#define HSES10G_CONFIGURATION_RX_FLOW_CONTROL_SA_REG1_LSB       0x00C0 /**< Source address for pause processing LSB */
#define HSES10G_CONFIGURATION_RX_FLOW_CONTROL_SA_REG1_MSB       0x00C4 /**< Source address for pause processing MSB */
#define HSES10G_CONFIGURATION_RSFEC_REG                         0x00D0 /**< RS-FEC configuration */
#define HSES10G_CONFIGURATION_FEC_REG                           0x00D4 /**< Configuration of the clause 74 FEC encoding */
#define HSES10G_CONFIGURATION_AN_CONTROL_REG1                   0x00E0 /**< Configuration of Autonegotiation block */
#define HSES10G_CONFIGURATION_AN_CONTROL_REG2                   0x00E4 /**< Configuration of Autonegotiation block */
#define HSES10G_CONFIGURATION_AN_ABILITY                        0x00F8 /**< Configuration of Autonegotiation ability */
#define HSES10G_CONFIGURATION_LT_CONTROL_REG1                   0x0100 /**< Configuration of Link Training block */
#define HSES10G_CONFIGURATION_LT_TRAINED_REG                    0x0104 /**< Indicates that the receiver portion of training is complete */
#define HSES10G_CONFIGURATION_LT_PRESET_REG                     0x0108 /**< Sets the value of the preset bit that is transmitted to the link partner in the control block of the training frame */
#define HSES10G_CONFIGURATION_LT_INIT_REG                       0x010C /**< Sets the value of the initialize bit that is transmitted to the link partner in the control block of the training frame */
#define HSES10G_CONFIGURATION_LT_SEED_REG0                      0x0110 /**< 11-bit signal that seeds the training pattern generator */
#define HSES10G_CONFIGURATION_LT_COEFFICIENT_REG0               0x0130 /**< Configuration of coefficients that is transmitted to the link partner in the control block of the training frame */ 
#define HSES10G_USER_REG_0                                      0x0134 /**< User-defined signal */
#define HSES10G_SWITCH_CORE_SPEED_REG                           0x0138 /**< Switches the line rate between 10G and 25G */ 
#define HSES10G_CONFIGURATION_1588_32BIT_REG                    0x013C /**< PTP configuration */
#define HSES10G_TX_CONFIGURATION_1588_REG                       0x0140 /**< TX PTP configuration */
#define HSES10G_RX_CONFIGURATION_1588_REG                       0x0144 /**< RX PTP configuration */
#define HSES10G_CONFIGURATION_TSN_REG                           0x019C /**< Configuration of Time Sensitive Networking (TSN) feature */

/* Status Register Map */
#define HSES10G_STAT_TX_STATUS_REG1                             0x0400 /**< TX statuses */
#define HSES10G_STAT_RX_STATUS_REG1                             0x0404 /**< RX statuses */  
#define HSES10G_STAT_STATUS_REG1                                0x0408 /**< TX/RX fifo statuses */
#define HSES10G_STAT_RX_BLOCK_LOCK_REG                          0x040C /**< Block lock status for each PCS lane */
#define HSES10G_STAT_RX_RSFEC_STATUS_REG                        0x043C /**< RX RS-FEC block statuses */
#define HSES10G_STAT_RX_FEC_STATUS_REG                          0x0448 /**< FEC decoder statuses  */
#define HSES10G_STAT_TX_RSFEC_STATUS_REG                        0x044C /**< TX RS-FEC block lane alignment status */
#define HSES10G_STAT_TX_FLOW_CONTROL_REG1                       0x0450 /**< TX flow control statuses */
#define HSES10G_STAT_RX_FLOW_CONTROL_REG1                       0x0454 /**< RX flow control statuses */
#define HSES10G_STAT_AN_STATUS                                  0x0458 /**< Auto-Negotiation block statuses */
#define HSES10G_STAT_AN_ABILITY                                 0x045C /**< Auto-Negotiation block abilities */
#define HSES10G_STAT_AN_LINK_CTL                                0x0460 /**< Auto-Negotiation block statuses */
#define HSES10G_STAT_LT_STATUS_REG1                             0x0464 /**< Link-Training block statuses */
#define HSES10G_STAT_LT_STATUS_REG2                             0x0468 /**< Link-Training block statuses */
#define HSES10G_STAT_LT_STATUS_REG3                             0x046C /**< Link-Training block statuses */
#define HSES10G_STAT_LT_STATUS_REG4                             0x0470 /**< Link-Training block statuses */
#define HSES10G_STAT_LT_COEFFICIENT0_REG                        0x0474 /**< Link-Training block statuses */
#define HSES10G_STAT_RX_VALID_CTRL_CODE                         0x0494 /**< Indicates that a PCS block with a valid control code was received */
#define HSES10G_STAT_CORE_SPEED_REG                             0x0498 /**< @todo Add description */
#define HSES10G_STAT_TSN_REG                                    0x049C /**< Indicates verification status */

/* Statistics Counters */
#define HSES10G_STATUS_CYCLE_COUNT_LSB                          0x0500 /**< Number of RX core clock cycles between TICK_REG writes LSB */         
#define HSES10G_STATUS_CYCLE_COUNT_MSB                          0x0504 /**< Number of RX core clock cycles between TICK_REG writes MSB */                  
#define HSES10G_STAT_RX_FRAMING_ERR_LSB                         0x0648 /**< RX number of bad sync header bits detected LSB */          
#define HSES10G_STAT_RX_FRAMING_ERR_MSB                         0x064C /**< RX number of bad sync header bits detected MSB */                   
#define HSES10G_STAT_RX_BAD_CODE_LSB                            0x0660 /**< RX 64B/66B code violation LSB */         
#define HSES10G_STAT_RX_BAD_CODE_MSB                            0x0664 /**< RX 64B/66B code violation MSB */         
#define HSES10G_STAT_RX_ERROR_LSB                               0x0668 /**< RX test pattern mismatch LSB */         
#define HSES10G_STAT_RX_ERROR_MSB                               0x066C /**< RX test pattern mismatch MSB */                  
#define HSES10G_STAT_RX_RSFEC_CORRECTED_CW_INC_LSB              0x0670 /**< RX corrected errors LSB */         
#define HSES10G_STAT_RX_RSFEC_CORRECTED_CW_INC_MSB              0x0674 /**< RX corrected errors MSB */                  
#define HSES10G_STAT_RX_RSFEC_UNCORRECTED_CW_INC_LSB            0x0678 /**< RX uncorrected errors LSB */         
#define HSES10G_STAT_RX_RSFEC_UNCORRECTED_CW_INC_MSB            0x067C /**< RX uncorrected errors MSB */                  
#define HSES10G_STAT_RX_RSFEC_ERR_COUNT0_INC_LSB                0x0680 /**< RX detected errors LSB */                 
#define HSES10G_STAT_RX_RSFEC_ERR_COUNT0_INC_MSB                0x0684 /**< RX detected errors MSB */                          
#define HSES10G_STAT_TX_FRAME_ERROR_LSB                         0x06A0 /**< TX End Of Packet (EOP) abort LSB */         
#define HSES10G_STAT_TX_FRAME_ERROR_MSB                         0x06A4 /**< TX End Of Packet (EOP) abort MSB */                  
#define HSES10G_STAT_TX_TOTAL_PACKETS_LSB                       0x0700 /**< TX number of packets LSB */         
#define HSES10G_STAT_TX_TOTAL_PACKETS_MSB                       0x0704 /**< TX number of packets MSB */                  
#define HSES10G_STAT_TX_TOTAL_GOOD_PACKETS_LSB                  0x0708 /**< TX number of good packets packets LSB */         
#define HSES10G_STAT_TX_TOTAL_GOOD_PACKETS_MSB                  0x070C /**< TX number of good packets packets MSB */         
#define HSES10G_STAT_TX_TOTAL_BYTES_LSB                         0x0710 /**< TX number of bytes LSB */                  
#define HSES10G_STAT_TX_TOTAL_BYTES_MSB                         0x0714 /**< TX number of bytes MSB */                           
#define HSES10G_STAT_TX_TOTAL_GOOD_BYTES_LSB                    0x0718 /**< TX number of good bytes LSB */                  
#define HSES10G_STAT_TX_TOTAL_GOOD_BYTES_MSB                    0x071C /**< TX number of good bytes MSB */                           
#define HSES10G_STAT_TX_PACKET_64_BYTES_LSB                     0x0720 /**< TX number of good and bad packets that contain 64 bytes LSB */         
#define HSES10G_STAT_TX_PACKET_64_BYTES_MSB                     0x0724 /**< TX number of good and bad packets that contain 64 bytes MSB */                  
#define HSES10G_STAT_TX_PACKET_65_127_BYTES_LSB                 0x0728 /**< TX number of good and bad packets that contain between 65 and 127 bytes LSB */                  
#define HSES10G_STAT_TX_PACKET_65_127_BYTES_MSB                 0x072C /**< TX number of good and bad packets that contain between 65 and 127 bytes MSB */                           
#define HSES10G_STAT_TX_PACKET_128_255_BYTES_LSB                0x0730 /**< TX number of good and bad packets that contain between 128 and 255 bytes LSB */                           
#define HSES10G_STAT_TX_PACKET_128_255_BYTES_MSB                0x0734 /**< TX number of good and bad packets that contain between 128 and 255 bytes MSB */                                    
#define HSES10G_STAT_TX_PACKET_256_511_BYTES_LSB                0x0738 /**< TX number of good and bad packets that contain between 256 and 511 bytes LSB */                                    
#define HSES10G_STAT_TX_PACKET_256_511_BYTES_MSB                0x073C /**< TX number of good and bad packets that contain between 256 and 511 bytes MSB */                                             
#define HSES10G_STAT_TX_PACKET_512_1023_BYTES_LSB               0x0740 /**< TX number of good and bad packets that contain between 512 and 1023 bytes LSB */                                             
#define HSES10G_STAT_TX_PACKET_512_1023_BYTES_MSB               0x0744 /**< TX number of good and bad packets that contain between 512 and 1023 bytes MSB */                                                      
#define HSES10G_STAT_TX_PACKET_1024_1518_BYTES_LSB              0x0748 /**< TX number of good and bad packets that contain between 1024 and 1518 bytes LSB */                                                      
#define HSES10G_STAT_TX_PACKET_1024_1518_BYTES_MSB              0x074C /**< TX number of good and bad packets that contain between 1024 and 1518 bytes MSB */
#define HSES10G_STAT_TX_PACKET_1519_1522_BYTES_LSB              0x0750 /**< TX number of good and bad packets that contain between 1519 and 1522 bytes LSB */         
#define HSES10G_STAT_TX_PACKET_1519_1522_BYTES_MSB              0x0754 /**< TX number of good and bad packets that contain between 1519 and 1522 bytes MSB */                  
#define HSES10G_STAT_TX_PACKET_1523_1548_BYTES_LSB              0x0758 /**< TX number of good and bad packets that contain between 1523 and 1548 bytes LSB */                  
#define HSES10G_STAT_TX_PACKET_1523_1548_BYTES_MSB              0x075C /**< TX number of good and bad packets that contain between 1523 and 1548 bytes MSB */         
#define HSES10G_STAT_TX_PACKET_1549_2047_BYTES_LSB              0x0760 /**< TX number of good and bad packets that contain between 1549 and 2047 bytes LSB */         
#define HSES10G_STAT_TX_PACKET_1549_2047_BYTES_MSB              0x0764 /**< TX number of good and bad packets that contain between 1549 and 2047 bytes MSB */         
#define HSES10G_STAT_TX_PACKET_2048_4095_BYTES_LSB              0x0768 /**< TX number of good and bad packets that contain between 2048 and 4095 bytes LSB */         
#define HSES10G_STAT_TX_PACKET_2048_4095_BYTES_MSB              0x076C /**< TX number of good and bad packets that contain between 2048 and 4095 bytes MSB */         
#define HSES10G_STAT_TX_PACKET_4096_8191_BYTES_LSB              0x0770 /**< TX number of good and bad packets that contain between 4096 and 8191 bytes LSB */         
#define HSES10G_STAT_TX_PACKET_4096_8191_BYTES_MSB              0x0774 /**< TX number of good and bad packets that contain between 4096 and 8191 bytes MSB */         
#define HSES10G_STAT_TX_PACKET_8192_9215_BYTES_LSB              0x0778 /**< TX number of good and bad packets that contain between 8192 and 9215 bytes LSB */         
#define HSES10G_STAT_TX_PACKET_8192_9215_BYTES_MSB              0x077C /**< TX number of good and bad packets that contain between 8192 and 9215 bytes MSB */         
#define HSES10G_STAT_TX_PACKET_LARGE_LSB                        0x0780 /**< TX number of all packets that are more than 9215 bytes long LSB */         
#define HSES10G_STAT_TX_PACKET_LARGE_MSB                        0x0784 /**< TX number of all packets that are more than 9215 bytes long MSB */         
#define HSES10G_STAT_TX_PACKET_SMALL_LSB                        0x0788 /**< TX number of all packets that are less than 64 bytes long LSB */          
#define HSES10G_STAT_TX_PACKET_SMALL_MSB                        0x078C /**< TX number of all packets that are less than 64 bytes long MSB */         
#define HSES10G_STAT_TX_BAD_FCS_LSB                             0x07B8 /**< TX number of packets greater than 64 bytes that have FCS errors LSB */         
#define HSES10G_STAT_TX_BAD_FCS_MSB                             0x07BC /**< TX number of packets greater than 64 bytes that have FCS errors MSB */         
#define HSES10G_STAT_TX_UNICAST_LSB                             0x07D0 /**< TX number of good unicast packets LSB */         
#define HSES10G_STAT_TX_UNICAST_MSB                             0x07D4 /**< TX number of good unicast packets MSB */         
#define HSES10G_STAT_TX_MULTICAST_LSB                           0x07D8 /**< TX number of good multicast packets LSB */         
#define HSES10G_STAT_TX_MULTICAST_MSB                           0x07DC /**< TX number of good multicast packets MSB */         
#define HSES10G_STAT_TX_BROADCAST_LSB                           0x07E0 /**< TX number of good broadcast packets LSB */         
#define HSES10G_STAT_TX_BROADCAST_MSB                           0x07E4 /**< TX number of good broadcast packets MSB */         
#define HSES10G_STAT_TX_VLAN_LSB                                0x07E8 /**< TX number of good 802.1Q tagged VLAN packets LSB */         
#define HSES10G_STAT_TX_VLAN_MSB                                0x07EC /**< TX number of good 802.1Q tagged VLAN packets MSB */         
#define HSES10G_STAT_TX_PAUSE_LSB                               0x07F0 /**< TX number of 802.3x MAC Pause Packet with good FCS LSB */         
#define HSES10G_STAT_TX_PAUSE_MSB                               0x07F4 /**< TX number of 802.3x MAC Pause Packet with good FCS MSB */                  
#define HSES10G_STAT_TX_USER_PAUSE_LSB                          0x07F8 /**< TX number of priority based pause packets with good FCS LSB */         
#define HSES10G_STAT_TX_USER_PAUSE_MSB                          0x07FC /**< TX number of priority based pause packets with good FCS MSB */         
#define HSES10G_STAT_RX_TOTAL_PACKETS_LSB                       0x0808 /**< RX number of bytes LSB */         
#define HSES10G_STAT_RX_TOTAL_PACKETS_MSB                       0x080C /**< RX number of bytes MSB */                  
#define HSES10G_STAT_RX_TOTAL_GOOD_PACKETS_LSB                  0x0810 /**< RX number of good packets packets LSB */         
#define HSES10G_STAT_RX_TOTAL_GOOD_PACKETS_MSB                  0x0814 /**< RX number of good packets packets MSB */         
#define HSES10G_STAT_RX_TOTAL_BYTES_LSB                         0x0818 /**< RX number of bytes LSB */                                                                
#define HSES10G_STAT_RX_TOTAL_BYTES_MSB                         0x081C /**< RX number of bytes MSB */                                                                
#define HSES10G_STAT_RX_TOTAL_GOOD_BYTES_LSB                    0x0820 /**< RX number of good bytes LSB */                                                           
#define HSES10G_STAT_RX_TOTAL_GOOD_BYTES_MSB                    0x0824 /**< RX number of good bytes MSB */                                                           
#define HSES10G_STAT_RX_PACKET_64_BYTES_LSB                     0x0828 /**< RX number of good and bad packets that contain 64 bytes LSB */                           
#define HSES10G_STAT_RX_PACKET_64_BYTES_MSB                     0x082C /**< RX number of good and bad packets that contain 64 bytes MSB */                           
#define HSES10G_STAT_RX_PACKET_65_127_BYTES_LSB                 0x0830 /**< RX number of good and bad packets that contain between 65 and 127 bytes LSB */           
#define HSES10G_STAT_RX_PACKET_65_127_BYTES_MSB                 0x0834 /**< RX number of good and bad packets that contain between 65 and 127 bytes MSB */           
#define HSES10G_STAT_RX_PACKET_128_255_BYTES_LSB                0x0838 /**< RX number of good and bad packets that contain between 128 and 255 bytes LSB */          
#define HSES10G_STAT_RX_PACKET_128_255_BYTES_MSB                0x083C /**< RX number of good and bad packets that contain between 128 and 255 bytes MSB */          
#define HSES10G_STAT_RX_PACKET_256_511_BYTES_LSB                0x0840 /**< RX number of good and bad packets that contain between 256 and 511 bytes LSB */          
#define HSES10G_STAT_RX_PACKET_256_511_BYTES_MSB                0x0844 /**< RX number of good and bad packets that contain between 256 and 511 bytes MSB */          
#define HSES10G_STAT_RX_PACKET_512_1023_BYTES_LSB               0x0848 /**< RX number of good and bad packets that contain between 512 and 1023 bytes LSB */         
#define HSES10G_STAT_RX_PACKET_512_1023_BYTES_MSB               0x084C /**< RX number of good and bad packets that contain between 512 and 1023 bytes MSB */         
#define HSES10G_STAT_RX_PACKET_1024_1518_BYTES_LSB              0x0850 /**< RX number of good and bad packets that contain between 1024 and 1518 bytes LSB */         
#define HSES10G_STAT_RX_PACKET_1024_1518_BYTES_MSB              0x0854 /**< RX number of good and bad packets that contain between 1024 and 1518 bytes MSB */         
#define HSES10G_STAT_RX_PACKET_1519_1522_BYTES_LSB              0x0858 /**< RX number of good and bad packets that contain between 1519 and 1522 bytes LSB */         
#define HSES10G_STAT_RX_PACKET_1519_1522_BYTES_MSB              0x085C /**< RX number of good and bad packets that contain between 1519 and 1522 bytes MSB */         
#define HSES10G_STAT_RX_PACKET_1523_1548_BYTES_LSB              0x0860 /**< RX number of good and bad packets that contain between 1523 and 1548 bytes LSB */         
#define HSES10G_STAT_RX_PACKET_1523_1548_BYTES_MSB              0x0864 /**< RX number of good and bad packets that contain between 1523 and 1548 bytes MSB */         
#define HSES10G_STAT_RX_PACKET_1549_2047_BYTES_LSB              0x0868 /**< RX number of good and bad packets that contain between 1549 and 2047 bytes LSB */         
#define HSES10G_STAT_RX_PACKET_1549_2047_BYTES_MSB              0x086C /**< RX number of good and bad packets that contain between 1549 and 2047 bytes MSB */         
#define HSES10G_STAT_RX_PACKET_2048_4095_BYTES_LSB              0x0870 /**< RX number of good and bad packets that contain between 2048 and 4095 bytes LSB */         
#define HSES10G_STAT_RX_PACKET_2048_4095_BYTES_MSB              0x0874 /**< RX number of good and bad packets that contain between 2048 and 4095 bytes MSB */         
#define HSES10G_STAT_RX_PACKET_4096_8191_BYTES_LSB              0x0878 /**< RX number of good and bad packets that contain between 4096 and 8191 bytes LSB */         
#define HSES10G_STAT_RX_PACKET_4096_8191_BYTES_MSB              0x087C /**< RX number of good and bad packets that contain between 4096 and 8191 bytes MSB */         
#define HSES10G_STAT_RX_PACKET_8192_9215_BYTES_LSB              0x0880 /**< RX number of good and bad packets that contain between 8192 and 9215 bytes LSB */         
#define HSES10G_STAT_RX_PACKET_8192_9215_BYTES_MSB              0x0884 /**< RX number of good and bad packets that contain between 8192 and 9215 bytes MSB */         
#define HSES10G_STAT_RX_PACKET_LARGE_LSB                        0x0888 /**< RX number of all packets that are more than 9215 bytes long LSB */                       
#define HSES10G_STAT_RX_PACKET_LARGE_MSB                        0x088C /**< RX number of all packets that are more than 9215 bytes long MSB */                       
#define HSES10G_STAT_RX_PACKET_SMALL_LSB                        0x0890 /**< RX number of all packets that are less than 64 bytes long LSB */                         
#define HSES10G_STAT_RX_PACKET_SMALL_MSB                        0x0894 /**< RX number of all packets that are less than 64 bytes long MSB */                         
#define HSES10G_STAT_RX_UNDERSIZE_LSB                           0x0898 /**< RX number of packets shorter than ctl_rx_min_packet_len with good FCS LSB */                   
#define HSES10G_STAT_RX_UNDERSIZE_MSB                           0x089C /**< RX number of packets shorter than ctl_rx_min_packet_len with good FCS MSB */                   
#define HSES10G_STAT_RX_FRAGMENT_LSB                            0x08A0 /**< RX number of packets shorter than ctl_rx_min_packet_len with bad FCS LSB */                                                                    
#define HSES10G_STAT_RX_FRAGMENT_MSB                            0x08A4 /**< RX number of packets shorter than ctl_rx_min_packet_len with bad FCS MSB */                                             
#define HSES10G_STAT_RX_OVERSIZE_LSB                            0x08A8 /**< RX number of packets longer than ctl_rx_max_packet_len with good FCS LSB */
#define HSES10G_STAT_RX_OVERSIZE_MSB                            0x08AC /**< RX number of packets longer than ctl_rx_max_packet_len with good FCS MSB */              
#define HSES10G_STAT_RX_TOOLONG_LSB                             0x08B0 /**< RX number of packets longer than ctl_rx_max_packet_len with good and bad FCS LSB */                   
#define HSES10G_STAT_RX_TOOLONG_MSB                             0x08B4 /**< RX number of packets longer than ctl_rx_max_packet_len with good and bad FCS LSB */                
#define HSES10G_STAT_RX_JABBER_LSB                              0x08B8 /**< RX number of packets longer than ctl_rx_max_packet_len with bad FCS LSB */                   
#define HSES10G_STAT_RX_JABBER_MSB                              0x08BC /**< RX number of packets longer than ctl_rx_max_packet_len with bad FCS MSB */                                      
#define HSES10G_STAT_RX_BAD_FCS_LSB                             0x08C0 /**< RX number of packets with bad FCS, but not a stomped FCS LSB */                                
#define HSES10G_STAT_RX_BAD_FCS_MSB                             0x08C4 /**< RX number of packets with bad FCS, but not a stomped FCS MSB */                                
#define HSES10G_STAT_RX_PACKET_BAD_FCS_LSB                      0x08C8 /**< RX number of packets between 64 and ctl_rx_max_packet_len with FCS errors LSB */                                                           
#define HSES10G_STAT_RX_PACKET_BAD_FCS_MSB                      0x08CC /**< RX number of packets between 64 and ctl_rx_max_packet_len with FCS errors MSB */                           
#define HSES10G_STAT_RX_STOMPED_FCS_LSB                         0x08D0 /**< RX number of packets with a stomped FCS LSB */         
#define HSES10G_STAT_RX_STOMPED_FCS_MSB                         0x08D4 /**< RX number of packets with a stomped FCS MSB */         
#define HSES10G_STAT_RX_UNICAST_LSB                             0x08D8 /**< RX number of good unicast packets LSB */                                   
#define HSES10G_STAT_RX_UNICAST_MSB                             0x08DC /**< RX number of good unicast packets MSB */                                   
#define HSES10G_STAT_RX_MULTICAST_LSB                           0x08E0 /**< RX number of good multicast packets LSB */                                 
#define HSES10G_STAT_RX_MULTICAST_MSB                           0x08E4 /**< RX number of good multicast packets MSB */                                 
#define HSES10G_STAT_RX_BROADCAST_LSB                           0x08E8 /**< RX number of good broadcast packets LSB */                                 
#define HSES10G_STAT_RX_BROADCAST_MSB                           0x08EC /**< RX number of good broadcast packets MSB */                                 
#define HSES10G_STAT_RX_VLAN_LSB                                0x08F0 /**< RX number of good 802.1Q tagged VLAN packets LSB */                        
#define HSES10G_STAT_RX_VLAN_MSB                                0x08F4 /**< RX number of good 802.1Q tagged VLAN packets MSB */                        
#define HSES10G_STAT_RX_PAUSE_LSB                               0x08F8 /**< RX number of 802.3x MAC Pause Packet with good FCS LSB */                  
#define HSES10G_STAT_RX_PAUSE_MSB                               0x08FC /**< RX number of 802.3x MAC Pause Packet with good FCS MSB */                  
#define HSES10G_STAT_RX_USER_PAUSE_LSB                          0x0900 /**< RX number of priority based pause packets with good FCS LSB */             
#define HSES10G_STAT_RX_USER_PAUSE_MSB                          0x0904 /**< RX number of priority based pause packets with good FCS MSB */             
#define HSES10G_STAT_RX_INRANGEERR_LSB                          0x0908 /**< RX number of packets with Length field error but with good FCS LSB */         
#define HSES10G_STAT_RX_INRANGEERR_MSB                          0x090C /**< RX number of packets with Length field error but with good FCS MSB */         
#define HSES10G_STAT_RX_TRUNCATED_LSB                           0x0910 /**< RX number of truncated packets due to the length exceeding ctl_rx_max_packet_len[14:0] LSB */         
#define HSES10G_STAT_RX_TRUNCATED_MSB                           0x0914 /**< RX number of truncated packets due to the length exceeding ctl_rx_max_packet_len[14:0] MSB */         
#define HSES10G_STAT_RX_TEST_PATTERN_MISMATCH_LSB               0x0918 /**< RX core number of mismatched occured for the test pattern LSB */         
#define HSES10G_STAT_RX_TEST_PATTERN_MISMATCH_MSB               0x091C /**< RX core number of mismatched occured for the test pattern MSB */         
#define HSES10G_STAT_FEC_INC_CORRECT_COUNT_LSB                  0x0920 /**< Number of asserts by ctl_rx_fec_enable when FEC decoder detected and corrected bit errors in correspondent frame LSB */         
#define HSES10G_STAT_FEC_INC_CORRECT_COUNT_MSB                  0x0924 /**< Number of asserts by ctl_rx_fec_enable when FEC decoder detected and corrected bit errors in correspondent frame MSB */         
#define HSES10G_STAT_FEC_INC_CANT_CORRECT_COUNT_LSB             0x0928 /**< Number of asserts by ctl_rx_fec_enable when FEC decoder detected but couldn't correct bit errors in correspondent frame LSB */         
#define HSES10G_STAT_FEC_INC_CANT_CORRECT_COUNT_MSB             0x092C /**< Number of asserts by ctl_rx_fec_enable when FEC decoder detected but couldn't correct bit errors in correspondent frame MSB */         
#define HSES10G_STAT_TX_MM_STATUS_LSB                           0x0980 /**< Number of preemtable packets transmitted LSB */         
#define HSES10G_STAT_TX_MM_STATUS_MSB                           0x0984 /**< Number of preemtable packets transmitted MSB */                  
#define HSES10G_STAT_TX_MM_FRAGMENT_LSB                         0x0988 /**< Number of continuation fragments of an preemtable packet transmitted LSB */                  
#define HSES10G_STAT_TX_MM_FRAGMENT_MSB                         0x098C /**< Number of continuation fragments of an preemtable packet transmitted MSB */                           
#define HSES10G_STAT_TX_MM_HOLD_LSB                             0x0990 /**< Number of ctl_hold_request transitions from 1'b0 to 1'b1 LSB */                  
#define HSES10G_STAT_TX_MM_HOLD_MSB                             0x0994 /**< Number of ctl_hold_request transitions from 1'b0 to 1'b1 MSB */                           
#define HSES10G_STAT_RX_MM_ASSEMBLY_ERROR_LSB                   0x0998 /**< Number of errors detected during fragment assembly LSB */         
#define HSES10G_STAT_RX_MM_ASSEMBLY_ERROR_MSB                   0x099C /**< Number of errors detected during fragment assembly MSB */                  
#define HSES10G_STAT_RX_MM_FRAME_SMD_ERROR_LSB                  0x09A0 /**< Number of rejected frames due to an incorrect SMD value LSB */         
#define HSES10G_STAT_RX_MM_FRAME_SMD_ERROR_MSB                  0x09A4 /**< Number of rejected frames due to an incorrect SMD value MSB */                  
#define HSES10G_STAT_RX_MM_FRAME_ASSEMBLY_OK_LSB                0x09A8 /**< Increments when all the preemptable frame fragments have been assembled and presented LSB */         
#define HSES10G_STAT_RX_MM_FRAME_ASSEMBLY_OK_MSB                0x09AC /**< Increments when all the preemptable frame fragments have been assembled and presented MSB */                  
#define HSES10G_STAT_RX_MM_FRAGMENT_LSB                         0x09B0 /**< Increments when a fragment frame is received LSB */         
#define HSES10G_STAT_RX_MM_FRAGMENT_MSB                         0x09B4 /**< Increments when a fragment frame is received MSB */                  

#define HSES10G_MAX_OFFSET             HSES10G_STAT_RX_MM_FRAGMENT_MSB
#define HSES10G_STAT_INVALID_OFFSET    0xFFFF

/** @name MODE register masks
 * @{
 */
#define HSES10G_MODE_TICK_REG_MODE_MASK	0x40000000
#define HSES10G_MODE_LCLLPBK_MASK	    0x80000000


/** @name TXCFG register masks
 * @{
 */
#define HSES10G_TXCFG_TX_MASK	0x00000001
#define HSES10G_TXCFG_FCS_MASK	0x00000002

/** @name RXCFG register masks
 * @{
 */
#define HSES10G_RXCFG_RX_MASK	0x00000001
#define HSES10G_RXCFG_DEL_FCS_MASK	0x00000002
#define HSES10G_RXCFG_IGN_FCS_MASK	0x00000004

/** @name USXGMII Auto negotiation register masks
 * @{
 */
#define HSES10G_USXGMII_ANBYPASS_MASK	0x00000001
#define HSES10G_USXGMII_ANENABLE_MASK	0x00000020
#define HSES10G_USXGMII_ANMAINRESET_MASK	0x00000040
#define HSES10G_USXGMII_ANRESTART_MASK	0x00000080
#define HSES10G_USXGMII_RATE_MASK		0x00000700
#define HSES10G_USXGMII_ANA_MASK		0x00010000
#define HSES10G_USXGMII_ANA_SPEED_MASK	0x0E000000
#define HSES10G_USXGMII_ANA_FD_MASK		0x10000000
#define HSES10G_USXGMII_ANACK_MASK		0x40000000
#define HSES10G_USXGMII_LINK_STS_MASK	0x80000000

#define HSES10G_USXGMII_RATE_10M_MASK	0x0
#define HSES10G_USXGMII_RATE_100M_MASK	0x1
#define HSES10G_USXGMII_RATE_1G_MASK	0x2
#define HSES10G_USXGMII_RATE_10G_MASK	0x3
#define HSES10G_USXGMII_RATE_2G5_MASK	0x4
#define HSES10G_USXGMII_RATE_SHIFT		8
#define HSES10G_USXGMII_SPEED_SHIFT		25

/** @name RXMTU register masks
 * @{
 */
#define HSES10G_RXMTU_MIN_JUM_MASK	0x000000FF
#define HSES10G_RXMTU_MAX_JUM_MASK	0x7FFF0000
#define HSES10G_RXMTU_MAX_JUM_SHIFT 16

/** @name RXBLSR register masks
 * @{
 */
#define HSES10G_RXBLKLCK_MASK	0x00000001

/** @name TICK register masks
 * @{
 */
#define HSES10G_TICK_STATEN_MASK	0x00000001

/** @name AN status register masks
 * @{
 */
#define HSES10G_AN_COMP_MASK	0x00000004
#define HSES10G_USXGMII_AN_COMP_MASK	0x00010000

/** @name AN ability register masks
 * @{
 */
#define HSES10G_ANA_10GKR_MASK	0x00000004


/** @name Reset and Address Filter (RAF) Register bit definitions.
 *  These bits are associated with the XAE_RAF_OFFSET register.
 * @{
 */
#define HSES10G_RAF_STATSRST_MASK  	0x00002000 	   /**< Statistics Counter
						    *   Reset
						    */
#define HSES10G_RAF_RXBADFRMEN_MASK     	0x00004000 /**< Receive Bad Frame
						    *   Enable
						    */
/*@}*/

/** @name Transmit Inter-Frame Gap Adjustement Register (TFGP) bit definitions
 *  @{
 */
#define HSES10G_TFGP_IFGP_MASK		0x0000007F /**< Transmit inter-frame
					            *   gap adjustment value
					            */
/*@}*/

#define HSES10G_STS_TX_ERROR_MASK		0x1
#define HSES10G_STS_RX_ERROR_MASK		0xFF0


/** @name Other Constant definitions used in the driver
 * @{
 */

#define HSES10G_SPEED_10_GBPS		10	/**< Speed of 10 Gbps */

#define HSES10G_LOOPS_TO_COME_OUT_OF_RST	10000	/**< Number of loops in the driver API 
                                                     * to wait for before returning a failure case. 
                                                     */

#define HSES10G_RST_DELAY_LOOPCNT_VAL	10000	/**< Timeout in ticks used
						                         *  while checking if the core
						                         *  had come out of reset. The
						                         *  exact tick time is defined
						                         *  in each case/loop where it
						                         *  will be used
						                         */

/** @struct hses10g_ctx_t
 *  hses10g context structure
 */
typedef struct hses10g_ctx_t {
    void            *devo;
    eth_brdspec_t   brdspec;
    eth_counter_t   *counters;
    b_u32           counters_num;
} hses10g_ctx_t;

static ccl_err_t    _ret;

/* hses10g device attributes definition */
static device_attr_t _attrs[] = {
    {
        .name = "name", 
        .type = eDEVMAN_ATTR_STRING, .maxnum = 1, 
        .offset = offsetof(struct hses10g_ctx_t, brdspec.name),
        .size = DEVMAN_DEVNAME_LEN,
        .format = "%s"
    },
    {
        .name = "offset", 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset = offsetof(struct hses10g_ctx_t, brdspec.offset),
        .format = "0x%08x"
    },
    {
        .name = "gt_reset", .id = eHSES10G_GT_RESET_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_GT_RESET_REG
    },
    {
        .name = "reset", .id = eHSES10G_RESET_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_RESET_REG
    },
    {
        .name = "mode", .id = eHSES10G_MODE_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_MODE_REG
    },
    {
        .name = "config_tx_reg1", .id = eHSES10G_CONFIGURATION_TX_REG1, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_TX_REG1
    },
    {
        .name = "config_rx_reg1", .id = eHSES10G_CONFIGURATION_RX_REG1, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_RX_REG1
    },
    {
        .name = "config_rx_mtu", .id = eHSES10G_CONFIGURATION_RX_MTU, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_RX_MTU
    },
    {
        .name = "config_vl_len", .id = eHSES10G_CONFIGURATION_VL_LENGTH_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_VL_LENGTH_REG
    },
    {
        .name = "tick", .id = eHSES10G_TICK_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_TICK_REG,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "config_rev", .id = eHSES10G_CONFIGURATION_REVISION_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_REVISION_REG,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "config_tx_low_test_pat_seed_a", .id = eHSES10G_CONFIGURATION_TX_TEST_PAT_SEED_A_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_TX_TEST_PAT_SEED_A_LSB
    },
    {
        .name = "config_tx_high_test_pat_seed_a", .id = eHSES10G_CONFIGURATION_TX_TEST_PAT_SEED_A_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_TX_TEST_PAT_SEED_A_MSB
    },
    {
        .name = "config_tx_low_test_pat_seed_b", .id = eHSES10G_CONFIGURATION_TX_TEST_PAT_SEED_B_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_TX_TEST_PAT_SEED_B_LSB
    },
    {
        .name = "config_tx_high_test_pat_seed_b", .id = eHSES10G_CONFIGURATION_TX_TEST_PAT_SEED_B_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_TX_TEST_PAT_SEED_B_MSB
    },
    {
        .name = "config_1588", .id = eHSES10G_CONFIGURATION_1588_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_1588_REG
    },
    {
        .name = "config_tx_flow_control", .id = eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_REG1, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_TX_FLOW_CONTROL_REG1
    },
    {
        .name = "config_tx_flow_control_refresh1", .id = eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_REFRESH_REG1, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_TX_FLOW_CONTROL_REFRESH_REG1
    },
    {
        .name = "config_tx_flow_control_refresh2", .id = eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_REFRESH_REG2, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_TX_FLOW_CONTROL_REFRESH_REG2
    },
    {
        .name = "config_tx_flow_control_refresh3", .id = eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_REFRESH_REG3, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_TX_FLOW_CONTROL_REFRESH_REG3
    },
    {
        .name = "config_tx_flow_control_refresh4", .id = eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_REFRESH_REG4, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_TX_FLOW_CONTROL_REFRESH_REG4
    },
    {
        .name = "config_tx_flow_control_refresh5", .id = eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_REFRESH_REG5, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_TX_FLOW_CONTROL_REFRESH_REG5
    },
    {
        .name = "config_tx_flow_control_quanta1", .id = eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_QUANTA_REG1, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_TX_FLOW_CONTROL_QUANTA_REG1
    },
    {
        .name = "config_tx_flow_control_quanta2", .id = eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_QUANTA_REG2, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_TX_FLOW_CONTROL_QUANTA_REG2
    },
    {
        .name = "config_tx_flow_control_quanta3", .id = eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_QUANTA_REG3, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_TX_FLOW_CONTROL_QUANTA_REG3
    },
    {
        .name = "config_tx_flow_control_quanta4", .id = eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_QUANTA_REG4, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_TX_FLOW_CONTROL_QUANTA_REG4
    },
    {
        .name = "config_tx_flow_control_quanta5", .id = eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_QUANTA_REG5, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_TX_FLOW_CONTROL_QUANTA_REG5
    },
    {
        .name = "config_tx_flow_control_ppp_etype_op", .id = eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_PPP_ETYPE_OP_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_TX_FLOW_CONTROL_PPP_ETYPE_OP_REG
    },
    {
        .name = "config_tx_flow_control_gpp_etype_op", .id = eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_GPP_ETYPE_OP_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_TX_FLOW_CONTROL_GPP_ETYPE_OP_REG
    },
    {
        .name = "config_tx_low_flow_control_gpp_da", .id = eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_GPP_DA_REG_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_TX_FLOW_CONTROL_GPP_DA_REG_LSB
    },
    {
        .name = "config_tx_high_flow_control_gpp_da", .id = eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_GPP_DA_REG_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_TX_FLOW_CONTROL_GPP_DA_REG_MSB
    },
    {
        .name = "config_tx_low_flow_control_gpp_sa", .id = eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_GPP_SA_REG_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_TX_FLOW_CONTROL_GPP_SA_REG_LSB
    },
    {
        .name = "config_tx_high_flow_control_gpp_sa", .id = eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_GPP_SA_REG_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_TX_FLOW_CONTROL_GPP_SA_REG_MSB
    },
    {
        .name = "config_tx_low_flow_control_ppp_da", .id = eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_PPP_DA_REG_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_TX_FLOW_CONTROL_PPP_DA_REG_LSB
    },
    {
        .name = "config_tx_high_flow_control_ppp_da", .id = eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_PPP_DA_REG_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_TX_FLOW_CONTROL_PPP_DA_REG_MSB
    },
    {
        .name = "config_tx_low_flow_control_ppp_sa", .id = eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_PPP_SA_REG_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_TX_FLOW_CONTROL_PPP_SA_REG_LSB
    },
    {
        .name = "config_tx_high_flow_control_ppp_sa", .id = eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_PPP_SA_REG_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_TX_FLOW_CONTROL_PPP_SA_REG_MSB
    },
    {
        .name = "config_rx_flow_control1", .id = eHSES10G_CONFIGURATION_RX_FLOW_CONTROL_REG1, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_RX_FLOW_CONTROL_REG1
    },
    {
        .name = "config_rx_flow_control2", .id = eHSES10G_CONFIGURATION_RX_FLOW_CONTROL_REG2, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_RX_FLOW_CONTROL_REG2
    },
    {
        .name = "config_rx_flow_control_ppp_etype_op", .id = eHSES10G_CONFIGURATION_RX_FLOW_CONTROL_PPP_ETYPE_OP_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_RX_FLOW_CONTROL_PPP_ETYPE_OP_REG
    },
    {
        .name = "config_rx_flow_control_gpp_etype_op", .id = eHSES10G_CONFIGURATION_RX_FLOW_CONTROL_GPP_ETYPE_OP_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_RX_FLOW_CONTROL_GPP_ETYPE_OP_REG
    },
    {
        .name = "config_rx_flow_control_gcp_etype_op", .id = eHSES10G_CONFIGURATION_RX_FLOW_CONTROL_GCP_PCP_TYPE_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_RX_FLOW_CONTROL_GCP_PCP_TYPE_REG
    },
    {
        .name = "config_rx_flow_control_pcp_op", .id = eHSES10G_CONFIGURATION_RX_FLOW_CONTROL_PCP_OP_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_RX_FLOW_CONTROL_PCP_OP_REG
    },
    {
        .name = "config_rx_flow_control_gcp_op", .id = eHSES10G_CONFIGURATION_RX_FLOW_CONTROL_GCP_OP_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_RX_FLOW_CONTROL_GCP_OP_REG
    },
    {
        .name = "config_rx_low_flow_control_da1", .id = eHSES10G_CONFIGURATION_RX_FLOW_CONTROL_DA_REG1_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_RX_FLOW_CONTROL_DA_REG1_LSB
    },
    {
        .name = "config_rx_high_flow_control_da1", .id = eHSES10G_CONFIGURATION_RX_FLOW_CONTROL_DA_REG1_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_RX_FLOW_CONTROL_DA_REG1_MSB
    },
    {
        .name = "config_rx_low_flow_control_da2", .id = eHSES10G_CONFIGURATION_RX_FLOW_CONTROL_DA_REG2_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_RX_FLOW_CONTROL_DA_REG2_LSB
    },
    {
        .name = "config_rx_high_flow_control_da2", .id = eHSES10G_CONFIGURATION_RX_FLOW_CONTROL_DA_REG2_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_RX_FLOW_CONTROL_DA_REG2_MSB
    },
    {
        .name = "config_rx_low_flow_control_sa1", .id = eHSES10G_CONFIGURATION_RX_FLOW_CONTROL_SA_REG1_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_RX_FLOW_CONTROL_SA_REG1_LSB
    },
    {
        .name = "config_rx_high_flow_control_sa1", .id = eHSES10G_CONFIGURATION_RX_FLOW_CONTROL_SA_REG1_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_RX_FLOW_CONTROL_SA_REG1_MSB
    },
    {
        .name = "config_rsfec", .id = eHSES10G_CONFIGURATION_RSFEC_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_RSFEC_REG
    },
    {
        .name = "config_fec", .id = eHSES10G_CONFIGURATION_FEC_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_FEC_REG
    },
    {
        .name = "config_an_ctl1", .id = eHSES10G_CONFIGURATION_AN_CONTROL_REG1, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_AN_CONTROL_REG1
    },
    {
        .name = "config_an_ctl2", .id = eHSES10G_CONFIGURATION_AN_CONTROL_REG2, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_AN_CONTROL_REG2
    },
    {
        .name = "config_an_ability", .id = eHSES10G_CONFIGURATION_AN_ABILITY, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_AN_ABILITY
    },
    {
        .name = "config_lt_ctl1", .id = eHSES10G_CONFIGURATION_LT_CONTROL_REG1, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_LT_CONTROL_REG1
    },
    {
        .name = "config_lt_trained", .id = eHSES10G_CONFIGURATION_LT_TRAINED_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_LT_TRAINED_REG
    },
    {
        .name = "config_lt_preset", .id = eHSES10G_CONFIGURATION_LT_PRESET_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_LT_PRESET_REG
    },
    {
        .name = "config_lt_init", .id = eHSES10G_CONFIGURATION_LT_INIT_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_LT_INIT_REG
    },
    {
        .name = "config_lt_seed0", .id = eHSES10G_CONFIGURATION_LT_SEED_REG0, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_LT_SEED_REG0
    },
    {
        .name = "config_lt_coef0", .id = eHSES10G_CONFIGURATION_LT_COEFFICIENT_REG0, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_LT_COEFFICIENT_REG0
    },
    {
        .name = "user0", .id = eHSES10G_USER_REG_0, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_USER_REG_0
    },
    {
        .name = "switch_core_speed", .id = eHSES10G_SWITCH_CORE_SPEED_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_SWITCH_CORE_SPEED_REG
    },
    {
        .name = "config_1588_32bit", .id = eHSES10G_CONFIGURATION_1588_32BIT_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_1588_32BIT_REG
    },
    {
        .name = "config_tx_1588", .id = eHSES10G_TX_CONFIGURATION_1588_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_TX_CONFIGURATION_1588_REG
    },
    {
        .name = "config_rx_1588", .id = eHSES10G_RX_CONFIGURATION_1588_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_RX_CONFIGURATION_1588_REG
    },
    {
        .name = "config_tsn", .id = eHSES10G_CONFIGURATION_TSN_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_CONFIGURATION_TSN_REG
    },
    {
        .name = "tx_status1", .id = eHSES10G_STAT_TX_STATUS_REG1, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_STATUS_REG1,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_status1", .id = eHSES10G_STAT_RX_STATUS_REG1, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_STATUS_REG1,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "status1", .id = eHSES10G_STAT_STATUS_REG1, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_STATUS_REG1,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "status_rx_block_lock", .id = eHSES10G_STAT_RX_BLOCK_LOCK_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_BLOCK_LOCK_REG,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "status_rx_rsfec", .id = eHSES10G_STAT_RX_RSFEC_STATUS_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_RSFEC_STATUS_REG,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "status_rx_fec", .id = eHSES10G_STAT_RX_FEC_STATUS_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_FEC_STATUS_REG,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "status_tx_rsfec", .id = eHSES10G_STAT_TX_RSFEC_STATUS_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_RSFEC_STATUS_REG,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "status_tx_flow_control1", .id = eHSES10G_STAT_TX_FLOW_CONTROL_REG1, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_FLOW_CONTROL_REG1,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "status_rx_flow_control1", .id = eHSES10G_STAT_RX_FLOW_CONTROL_REG1, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_FLOW_CONTROL_REG1,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "an_status", .id = eHSES10G_STAT_AN_STATUS, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_AN_STATUS,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "an_ability", .id = eHSES10G_STAT_AN_ABILITY, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_AN_ABILITY,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "status_an_link", .id = eHSES10G_STAT_AN_LINK_CTL, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_AN_LINK_CTL,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "lt_status1", .id = eHSES10G_STAT_LT_STATUS_REG1, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_LT_STATUS_REG1,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "lt_status2", .id = eHSES10G_STAT_LT_STATUS_REG2, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_LT_STATUS_REG2,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "lt_status3", .id = eHSES10G_STAT_LT_STATUS_REG3, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_LT_STATUS_REG3,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "lt_status4", .id = eHSES10G_STAT_LT_STATUS_REG4, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_LT_STATUS_REG4,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "status_lt_coef0", .id = eHSES10G_STAT_LT_COEFFICIENT0_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_LT_COEFFICIENT0_REG,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_valid_ctl_status", .id = eHSES10G_STAT_RX_VALID_CTRL_CODE, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_VALID_CTRL_CODE,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "core_speed_status", .id = eHSES10G_STAT_CORE_SPEED_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_CORE_SPEED_REG,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tsn_status", .id = eHSES10G_STAT_TSN_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TSN_REG,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "cycle_low_count", .id = eHSES10G_STATUS_CYCLE_COUNT_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STATUS_CYCLE_COUNT_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "cycle_high_count", .id = eHSES10G_STATUS_CYCLE_COUNT_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STATUS_CYCLE_COUNT_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_framing_err_low_cnt", .id = eHSES10G_STAT_RX_FRAMING_ERR_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_FRAMING_ERR_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_framing_err_high_cnt", .id = eHSES10G_STAT_RX_FRAMING_ERR_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_FRAMING_ERR_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_bad_code_low_cnt", .id = eHSES10G_STAT_RX_BAD_CODE_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_BAD_CODE_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_bad_code_high_cnt", .id = eHSES10G_STAT_RX_BAD_CODE_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_BAD_CODE_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_error_low_cnt", .id = eHSES10G_STAT_RX_ERROR_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_ERROR_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_error_high_cnt", .id = eHSES10G_STAT_RX_ERROR_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_ERROR_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_rsfec_corrected_cw_inc_low_cnt", .id = eHSES10G_STAT_RX_RSFEC_CORRECTED_CW_INC_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_RSFEC_CORRECTED_CW_INC_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_rsfec_corrected_cw_inc_high_cnt", .id = eHSES10G_STAT_RX_RSFEC_CORRECTED_CW_INC_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_RSFEC_CORRECTED_CW_INC_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_rsfec_uncorrected_cw_inc_low_cnt", .id = eHSES10G_STAT_RX_RSFEC_UNCORRECTED_CW_INC_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_RSFEC_UNCORRECTED_CW_INC_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_rsfec_uncorrected_cw_inc_high_cnt", .id = eHSES10G_STAT_RX_RSFEC_UNCORRECTED_CW_INC_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_RSFEC_UNCORRECTED_CW_INC_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_rsfec_err_count0_inc_low_cnt", .id = eHSES10G_STAT_RX_RSFEC_ERR_COUNT0_INC_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_RSFEC_ERR_COUNT0_INC_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_rsfec_err_count0_inc_high_cnt", .id = eHSES10G_STAT_RX_RSFEC_ERR_COUNT0_INC_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_RSFEC_ERR_COUNT0_INC_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_frame_error_low_cnt", .id = eHSES10G_STAT_TX_FRAME_ERROR_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_FRAME_ERROR_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_frame_error_high_cnt", .id = eHSES10G_STAT_TX_FRAME_ERROR_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_FRAME_ERROR_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_total_packets_low_cnt", .id = eHSES10G_STAT_TX_TOTAL_PACKETS_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_TOTAL_PACKETS_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_total_packets_high_cnt", .id = eHSES10G_STAT_TX_TOTAL_PACKETS_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_TOTAL_PACKETS_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_good_packets_low_cnt", .id = eHSES10G_STAT_TX_TOTAL_GOOD_PACKETS_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_TOTAL_GOOD_PACKETS_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_good_packets_high_cnt", .id = eHSES10G_STAT_TX_TOTAL_GOOD_PACKETS_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_TOTAL_GOOD_PACKETS_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_total_bytes_low_cnt", .id = eHSES10G_STAT_TX_TOTAL_BYTES_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_TOTAL_BYTES_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_total_bytes_high_cnt", .id = eHSES10G_STAT_TX_TOTAL_BYTES_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_TOTAL_BYTES_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_total_good_bytes_low_cnt", .id = eHSES10G_STAT_TX_TOTAL_GOOD_BYTES_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_TOTAL_GOOD_BYTES_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_total_good_bytes_high_cnt", .id = eHSES10G_STAT_TX_TOTAL_GOOD_BYTES_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_TOTAL_GOOD_BYTES_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_64_bytes_low_cnt", .id = eHSES10G_STAT_TX_PACKET_64_BYTES_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_PACKET_64_BYTES_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_64_bytes_high_cnt", .id = eHSES10G_STAT_TX_PACKET_64_BYTES_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_PACKET_64_BYTES_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_65_127_low_cnt", .id = eHSES10G_STAT_TX_PACKET_65_127_BYTES_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_PACKET_65_127_BYTES_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_65_127_high_cnt", .id = eHSES10G_STAT_TX_PACKET_65_127_BYTES_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_PACKET_65_127_BYTES_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_128_255_low_cnt", .id = eHSES10G_STAT_TX_PACKET_128_255_BYTES_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_PACKET_128_255_BYTES_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_128_255_high_cnt", .id = eHSES10G_STAT_TX_PACKET_128_255_BYTES_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_PACKET_128_255_BYTES_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_256_511_low_cnt", .id = eHSES10G_STAT_TX_PACKET_256_511_BYTES_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_PACKET_256_511_BYTES_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_256_511_high_cnt", .id = eHSES10G_STAT_TX_PACKET_256_511_BYTES_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_PACKET_256_511_BYTES_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_512_1023_low_cnt", .id = eHSES10G_STAT_TX_PACKET_512_1023_BYTES_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_PACKET_512_1023_BYTES_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_512_1023_high_cnt", .id = eHSES10G_STAT_TX_PACKET_512_1023_BYTES_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_PACKET_512_1023_BYTES_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_1024_1518_low_cnt", .id = eHSES10G_STAT_TX_PACKET_1024_1518_BYTES_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_PACKET_1024_1518_BYTES_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_1024_1518_high_cnt", .id = eHSES10G_STAT_TX_PACKET_1024_1518_BYTES_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_PACKET_1024_1518_BYTES_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_1519_1522_low_cnt", .id = eHSES10G_STAT_TX_PACKET_1519_1522_BYTES_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_PACKET_1519_1522_BYTES_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_1519_1522_high_cnt", .id = eHSES10G_STAT_TX_PACKET_1519_1522_BYTES_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_PACKET_1519_1522_BYTES_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_1523_1548_low_cnt", .id = eHSES10G_STAT_TX_PACKET_1523_1548_BYTES_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_PACKET_1523_1548_BYTES_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_1523_1548_high_cnt", .id = eHSES10G_STAT_TX_PACKET_1523_1548_BYTES_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_PACKET_1523_1548_BYTES_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_1549_2047_low_cnt", .id = eHSES10G_STAT_TX_PACKET_1549_2047_BYTES_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_PACKET_1549_2047_BYTES_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_1549_2047_high_cnt", .id = eHSES10G_STAT_TX_PACKET_1549_2047_BYTES_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_PACKET_1549_2047_BYTES_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_2048_4095_low_cnt", .id = eHSES10G_STAT_TX_PACKET_2048_4095_BYTES_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_PACKET_2048_4095_BYTES_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_2048_4095_high_cnt", .id = eHSES10G_STAT_TX_PACKET_2048_4095_BYTES_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_PACKET_2048_4095_BYTES_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_4096_8191_low_cnt", .id = eHSES10G_STAT_TX_PACKET_4096_8191_BYTES_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_PACKET_4096_8191_BYTES_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_4096_8191_high_cnt", .id = eHSES10G_STAT_TX_PACKET_4096_8191_BYTES_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_PACKET_4096_8191_BYTES_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_8192_9215_low_cnt", .id = eHSES10G_STAT_TX_PACKET_8192_9215_BYTES_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_PACKET_8192_9215_BYTES_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_8192_9215_high_cnt", .id = eHSES10G_STAT_TX_PACKET_8192_9215_BYTES_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_PACKET_8192_9215_BYTES_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_large_low_cnt", .id = eHSES10G_STAT_TX_PACKET_LARGE_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_PACKET_LARGE_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_large_high_cnt", .id = eHSES10G_STAT_TX_PACKET_LARGE_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_PACKET_LARGE_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_small_low_cnt", .id = eHSES10G_STAT_TX_PACKET_SMALL_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_PACKET_SMALL_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_small_high_cnt", .id = eHSES10G_STAT_TX_PACKET_SMALL_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_PACKET_SMALL_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_bad_fcs_low_cnt", .id = eHSES10G_STAT_TX_BAD_FCS_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_BAD_FCS_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_bad_fcs_high_cnt", .id = eHSES10G_STAT_TX_BAD_FCS_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_BAD_FCS_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_ucast_low_cnt", .id = eHSES10G_STAT_TX_UNICAST_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_UNICAST_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_ucast_high_cnt", .id = eHSES10G_STAT_TX_UNICAST_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_UNICAST_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_mcast_low_cnt", .id = eHSES10G_STAT_TX_MULTICAST_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_MULTICAST_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_mcast_high_cnt", .id = eHSES10G_STAT_TX_MULTICAST_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_MULTICAST_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_bcast_low_cnt", .id = eHSES10G_STAT_TX_BROADCAST_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_BROADCAST_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_bcast_high_cnt", .id = eHSES10G_STAT_TX_BROADCAST_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_BROADCAST_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_vlan_low_cnt", .id = eHSES10G_STAT_TX_VLAN_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_VLAN_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_vlan_high_cnt", .id = eHSES10G_STAT_TX_VLAN_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_VLAN_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_pause_low_cnt", .id = eHSES10G_STAT_TX_PAUSE_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_PAUSE_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_pause_high_cnt", .id = eHSES10G_STAT_TX_PAUSE_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_PAUSE_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_user_pause_low_cnt", .id = eHSES10G_STAT_TX_USER_PAUSE_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_USER_PAUSE_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_user_pause_high_cnt", .id = eHSES10G_STAT_TX_USER_PAUSE_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_USER_PAUSE_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_total_packets_low_cnt", .id = eHSES10G_STAT_RX_TOTAL_PACKETS_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_TOTAL_PACKETS_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_total_packets_high_cnt", .id = eHSES10G_STAT_RX_TOTAL_PACKETS_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_TOTAL_PACKETS_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_total_good_packets_low_cnt", .id = eHSES10G_STAT_RX_TOTAL_GOOD_PACKETS_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_TOTAL_GOOD_PACKETS_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_total_good_packets_high_cnt", .id = eHSES10G_STAT_RX_TOTAL_GOOD_PACKETS_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_TOTAL_GOOD_PACKETS_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_total_bytes_low_cnt", .id = eHSES10G_STAT_RX_TOTAL_BYTES_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_TOTAL_BYTES_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_total_bytes_high_cnt", .id = eHSES10G_STAT_RX_TOTAL_BYTES_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_TOTAL_BYTES_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_total_good_bytes_low_cnt", .id = eHSES10G_STAT_RX_TOTAL_GOOD_BYTES_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_TOTAL_GOOD_BYTES_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_total_good_bytes_high_cnt", .id = eHSES10G_STAT_RX_TOTAL_GOOD_BYTES_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_TOTAL_GOOD_BYTES_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_64_bytes_low_cnt", .id = eHSES10G_STAT_RX_PACKET_64_BYTES_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_PACKET_64_BYTES_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_64_bytes_high_cnt", .id = eHSES10G_STAT_RX_PACKET_64_BYTES_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_PACKET_64_BYTES_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_65_127_bytes_low_cnt", .id = eHSES10G_STAT_RX_PACKET_65_127_BYTES_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_PACKET_65_127_BYTES_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_65_127_bytes_high_cnt", .id = eHSES10G_STAT_RX_PACKET_65_127_BYTES_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_PACKET_65_127_BYTES_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_128_255_bytes_low_cnt", .id = eHSES10G_STAT_RX_PACKET_128_255_BYTES_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_PACKET_128_255_BYTES_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_128_255_bytes_high_cnt", .id = eHSES10G_STAT_RX_PACKET_128_255_BYTES_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_PACKET_128_255_BYTES_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_256_511_bytes_low_cnt", .id = eHSES10G_STAT_RX_PACKET_256_511_BYTES_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_PACKET_256_511_BYTES_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_256_511_bytes_high_cnt", .id = eHSES10G_STAT_RX_PACKET_256_511_BYTES_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_PACKET_256_511_BYTES_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_512_1023_bytes_low_cnt", .id = eHSES10G_STAT_RX_PACKET_512_1023_BYTES_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_PACKET_512_1023_BYTES_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_512_1023_bytes_high_cnt", .id = eHSES10G_STAT_RX_PACKET_512_1023_BYTES_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_PACKET_512_1023_BYTES_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_1024_1518_bytes_low_cnt", .id = eHSES10G_STAT_RX_PACKET_1024_1518_BYTES_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_PACKET_1024_1518_BYTES_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_1024_1518_bytes_high_cnt", .id = eHSES10G_STAT_RX_PACKET_1024_1518_BYTES_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_PACKET_1024_1518_BYTES_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_1519_1522_bytes_low_cnt", .id = eHSES10G_STAT_RX_PACKET_1519_1522_BYTES_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_PACKET_1519_1522_BYTES_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_1519_1522_bytes_high_cnt", .id = eHSES10G_STAT_RX_PACKET_1519_1522_BYTES_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_PACKET_1519_1522_BYTES_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_1523_1548_bytes_low_cnt", .id = eHSES10G_STAT_RX_PACKET_1523_1548_BYTES_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_PACKET_1523_1548_BYTES_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_1523_1548_bytes_high_cnt", .id = eHSES10G_STAT_RX_PACKET_1523_1548_BYTES_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_PACKET_1523_1548_BYTES_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_1549_2047_bytes_low_cnt", .id = eHSES10G_STAT_RX_PACKET_1549_2047_BYTES_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_PACKET_1549_2047_BYTES_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_1549_2047_bytes_high_cnt", .id = eHSES10G_STAT_RX_PACKET_1549_2047_BYTES_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_PACKET_1549_2047_BYTES_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_2048_4095_bytes_low_cnt", .id = eHSES10G_STAT_RX_PACKET_2048_4095_BYTES_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_PACKET_2048_4095_BYTES_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_2048_4095_bytes_high_cnt", .id = eHSES10G_STAT_RX_PACKET_2048_4095_BYTES_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_PACKET_2048_4095_BYTES_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_4096_8191_bytes_low_cnt", .id = eHSES10G_STAT_RX_PACKET_4096_8191_BYTES_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_PACKET_4096_8191_BYTES_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_4096_8191_bytes_high_cnt", .id = eHSES10G_STAT_RX_PACKET_4096_8191_BYTES_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_PACKET_4096_8191_BYTES_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_8192_9215_bytes_low_cnt", .id = eHSES10G_STAT_RX_PACKET_8192_9215_BYTES_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_PACKET_8192_9215_BYTES_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_8192_9215_bytes_high_cnt", .id = eHSES10G_STAT_RX_PACKET_8192_9215_BYTES_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_PACKET_8192_9215_BYTES_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_large_low_cnt", .id = eHSES10G_STAT_RX_PACKET_LARGE_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_PACKET_LARGE_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_large_high_cnt", .id = eHSES10G_STAT_RX_PACKET_LARGE_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_PACKET_LARGE_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_small_low_cnt", .id = eHSES10G_STAT_RX_PACKET_SMALL_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_PACKET_SMALL_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_small_high_cnt", .id = eHSES10G_STAT_RX_PACKET_SMALL_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_PACKET_SMALL_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_undersize_low_cnt", .id = eHSES10G_STAT_RX_UNDERSIZE_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_UNDERSIZE_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_undersize_high_cnt", .id = eHSES10G_STAT_RX_UNDERSIZE_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_UNDERSIZE_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_fragment_low_cnt", .id = eHSES10G_STAT_RX_FRAGMENT_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_FRAGMENT_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_fragment_high_cnt", .id = eHSES10G_STAT_RX_FRAGMENT_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_FRAGMENT_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_oversize_low_cnt", .id = eHSES10G_STAT_RX_OVERSIZE_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_OVERSIZE_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_oversize_high_cnt", .id = eHSES10G_STAT_RX_OVERSIZE_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_OVERSIZE_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_toolong_low_cnt", .id = eHSES10G_STAT_RX_TOOLONG_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_TOOLONG_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_toolong_high_cnt", .id = eHSES10G_STAT_RX_TOOLONG_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_TOOLONG_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_jabber_low_cnt", .id = eHSES10G_STAT_RX_JABBER_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_JABBER_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_jabber_high_cnt", .id = eHSES10G_STAT_RX_JABBER_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_JABBER_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_bad_fcs_low_cnt", .id = eHSES10G_STAT_RX_BAD_FCS_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_BAD_FCS_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_bad_fcs_high_cnt", .id = eHSES10G_STAT_RX_BAD_FCS_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_BAD_FCS_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_bad_fcs_low_cnt", .id = eHSES10G_STAT_RX_PACKET_BAD_FCS_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_PACKET_BAD_FCS_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_bad_fcs_high_cnt", .id = eHSES10G_STAT_RX_PACKET_BAD_FCS_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_PACKET_BAD_FCS_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_stomped_fcs_low_cnt", .id = eHSES10G_STAT_RX_STOMPED_FCS_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_STOMPED_FCS_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_stomped_fcs_high_cnt", .id = eHSES10G_STAT_RX_STOMPED_FCS_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_STOMPED_FCS_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_ucast_low_cnt", .id = eHSES10G_STAT_RX_UNICAST_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_UNICAST_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_ucast_high_cnt", .id = eHSES10G_STAT_RX_UNICAST_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_UNICAST_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_mcast_low_cnt", .id = eHSES10G_STAT_RX_MULTICAST_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_MULTICAST_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_mcast_high_cnt", .id = eHSES10G_STAT_RX_MULTICAST_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_MULTICAST_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_bcast_low_cnt", .id = eHSES10G_STAT_RX_BROADCAST_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_BROADCAST_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_bcast_high_cnt", .id = eHSES10G_STAT_RX_BROADCAST_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_BROADCAST_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_vlan_low_cnt", .id = eHSES10G_STAT_RX_VLAN_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_VLAN_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_vlan_high_cnt", .id = eHSES10G_STAT_RX_VLAN_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_VLAN_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_pause_low_cnt", .id = eHSES10G_STAT_RX_PAUSE_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_PAUSE_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_pause_high_cnt", .id = eHSES10G_STAT_RX_PAUSE_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_PAUSE_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_user_pause_low_cnt", .id = eHSES10G_STAT_RX_USER_PAUSE_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_USER_PAUSE_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_user_pause_high_cnt", .id = eHSES10G_STAT_RX_USER_PAUSE_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_USER_PAUSE_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_inrangeerr_low_cnt", .id = eHSES10G_STAT_RX_INRANGEERR_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_INRANGEERR_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_inrangeerr_high_cnt", .id = eHSES10G_STAT_RX_INRANGEERR_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_INRANGEERR_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_truncated_low_cnt", .id = eHSES10G_STAT_RX_TRUNCATED_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_TRUNCATED_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_truncated_high_cnt", .id = eHSES10G_STAT_RX_TRUNCATED_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_TRUNCATED_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_test_pattern_mismatch_low_cnt", .id = eHSES10G_STAT_RX_TEST_PATTERN_MISMATCH_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_TEST_PATTERN_MISMATCH_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_test_pattern_mismatch_high_cnt", .id = eHSES10G_STAT_RX_TEST_PATTERN_MISMATCH_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_TEST_PATTERN_MISMATCH_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "fec_inc_correct_low_cnt", .id = eHSES10G_STAT_FEC_INC_CORRECT_COUNT_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_FEC_INC_CORRECT_COUNT_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "fec_inc_correct_high_cnt", .id = eHSES10G_STAT_FEC_INC_CORRECT_COUNT_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_FEC_INC_CORRECT_COUNT_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "fec_inc_cant_correct_low_cnt", .id = eHSES10G_STAT_FEC_INC_CANT_CORRECT_COUNT_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_FEC_INC_CANT_CORRECT_COUNT_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "fec_inc_cant_correct_high_cnt", .id = eHSES10G_STAT_FEC_INC_CANT_CORRECT_COUNT_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_FEC_INC_CANT_CORRECT_COUNT_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_mm_status_low_cnt", .id = eHSES10G_STAT_TX_MM_STATUS_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_MM_STATUS_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_mm_status_high_cnt", .id = eHSES10G_STAT_TX_MM_STATUS_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_MM_STATUS_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_mm_fragment_low_cnt", .id = eHSES10G_STAT_TX_MM_FRAGMENT_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_MM_FRAGMENT_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_mm_fragment_high_cnt", .id = eHSES10G_STAT_TX_MM_FRAGMENT_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_MM_FRAGMENT_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_mm_hold_low_cnt", .id = eHSES10G_STAT_TX_MM_HOLD_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_MM_HOLD_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_mm_hold_high_cnt", .id = eHSES10G_STAT_TX_MM_HOLD_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_TX_MM_HOLD_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_mm_assembly_error_low_cnt", .id = eHSES10G_STAT_RX_MM_ASSEMBLY_ERROR_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_MM_ASSEMBLY_ERROR_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_mm_assembly_error_high_cnt", .id = eHSES10G_STAT_RX_MM_ASSEMBLY_ERROR_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_MM_ASSEMBLY_ERROR_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_mm_frame_smd_error_low_cnt", .id = eHSES10G_STAT_RX_MM_FRAME_SMD_ERROR_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_MM_FRAME_SMD_ERROR_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_mm_frame_smd_error_high_cnt", .id = eHSES10G_STAT_RX_MM_FRAME_SMD_ERROR_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_MM_FRAME_SMD_ERROR_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_mm_frame_assembly_ok_low_cnt", .id = eHSES10G_STAT_RX_MM_FRAME_ASSEMBLY_OK_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_MM_FRAME_ASSEMBLY_OK_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_mm_frame_assembly_ok_high_cnt", .id = eHSES10G_STAT_RX_MM_FRAME_ASSEMBLY_OK_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_MM_FRAME_ASSEMBLY_OK_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_mm_fragment_low_cnt", .id = eHSES10G_STAT_RX_MM_FRAGMENT_LSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_MM_FRAGMENT_LSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_mm_fragment_high_cnt", .id = eHSES10G_STAT_RX_MM_FRAGMENT_MSB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = HSES10G_STAT_RX_MM_FRAGMENT_MSB,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "read_single_counter", .id = eHSES10G_STAT_SINGLE_COUNTER, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVMAN_IF_MIB_MAX, .size = sizeof(b_u64), 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "property", .id = eHSES10G_IF_PROPERTY, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVMAN_PROP_MAX, .size = 128, 
        .offset_sz = sizeof(b_u32),
    },
    {
        .name = "read_counters", .id = eHSES10G_STAT_COUNTERS, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = sizeof(port_stats_t), 
        .offset_sz = sizeof(b_u8),
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "init", .id = eHSES10G_INIT, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "config_test", .id = eHSES10G_CONFIGURE_TEST, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "run_test", .id = eHSES10G_RUN_TEST, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, 
        .size = sizeof(board_dev_stats_t), 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "counters_reset", .id = eHSES10G_COUNTERS_RESET, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
};

#ifdef MAC_COUNTERS_ENABLED
static mib_counter_map_t _hses10g_mib_cntr_map[] = {
    {
        .ifmib = DEVMAN_IF_MIB_BYTEREC, .offset = HSES10G_STAT_RX_TOTAL_BYTES_LSB
    },
    {
        .ifmib = DEVMAN_IF_MIB_BYTSENT, .offset = HSES10G_STAT_TX_TOTAL_BYTES_LSB
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRAREC, .offset = HSES10G_STAT_RX_TOTAL_PACKETS_LSB
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRASENT, .offset = HSES10G_STAT_TX_TOTAL_PACKETS_LSB
    },
    {
        .ifmib = DEVMAN_IF_MIB_TOTALBYTEREC, .offset = HSES10G_STAT_RX_TOTAL_BYTES_LSB
    },
    {
        .ifmib = DEVMAN_IF_MIB_TOTALFRAMEREC, .offset = HSES10G_STAT_RX_TOTAL_PACKETS_LSB
    },
    {
        .ifmib = DEVMAN_IF_MIB_BRDFRAREC, .offset = HSES10G_STAT_RX_BROADCAST_LSB
    },
    {
        .ifmib = DEVMAN_IF_MIB_MULFRAREC, .offset = HSES10G_STAT_RX_MULTICAST_LSB
    },
    {
        .ifmib = DEVMAN_IF_MIB_CRCERR, .offset = HSES10G_STAT_RX_BAD_FCS_LSB
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRAOVERSIZE, .offset = HSES10G_STAT_RX_OVERSIZE_LSB
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRAFRAGMENTS, .offset = HSES10G_STAT_RX_FRAGMENT_LSB
    },
    {
        .ifmib = DEVMAN_IF_MIB_JABBER, .offset = HSES10G_STAT_RX_JABBER_LSB
    },
    {
        .ifmib = DEVMAN_IF_MIB_COLL, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_LATECOL, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRA64, .offset = HSES10G_STAT_RX_PACKET_64_BYTES_LSB
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRA65T127, .offset = HSES10G_STAT_RX_PACKET_65_127_BYTES_LSB
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRA128T255, .offset = HSES10G_STAT_RX_PACKET_128_255_BYTES_LSB
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRA256T511, .offset = HSES10G_STAT_RX_PACKET_256_511_BYTES_LSB
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRA512T1023, .offset = HSES10G_STAT_RX_PACKET_512_1023_BYTES_LSB
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRA1024T1522, .offset = HSES10G_STAT_RX_PACKET_1024_1518_BYTES_LSB
    },
    {
        .ifmib = DEVMAN_IF_MIB_MACRXERR, .offset = HSES10G_STAT_RX_ERROR_LSB
    },
    {
        .ifmib = DEVMAN_IF_MIB_DROPPED, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_INUCASTPKT, .offset = HSES10G_STAT_RX_UNICAST_LSB
    },
    {
        .ifmib = DEVMAN_IF_MIB_INNOUCASTPKT, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_INERRORS, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_OUTERRORS, .offset = HSES10G_STAT_TX_FRAME_ERROR_LSB
    },
    {
        .ifmib = DEVMAN_IF_MIB_OUTNOUCASTPKT, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_OUTUCASTPKT, .offset = HSES10G_STAT_TX_UNICAST_LSB
    },
    {
        .ifmib = DEVMAN_IF_MIB_MULTIOUT, .offset = HSES10G_STAT_TX_MULTICAST_LSB
    },
    {
        .ifmib = DEVMAN_IF_MIB_BROADOUT, .offset = HSES10G_STAT_TX_BROADCAST_LSB
    },
    {
        .ifmib = DEVMAN_IF_MIB_UNDERSIZ, .offset = HSES10G_STAT_RX_UNDERSIZE_LSB
    },
    {
        .ifmib = DEVMAN_IF_MIB_INUNKNOWNPROTOS, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_ALIGN_ERR, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_FCS_ERR, .offset = HSES10G_STAT_RX_BAD_FCS_LSB
    },
    {
        .ifmib = DEVMAN_IF_MIB_SQETEST_ERR, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_CSE_ERR, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_SYMBOL_ERR, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_MACTX_ERR, .offset = HSES10G_STAT_TX_FRAME_ERROR_LSB
    },
    {
        .ifmib = DEVMAN_IF_MIB_MACRX_ERR, .offset = HSES10G_STAT_RX_FRAMING_ERR_LSB
    },
    {
        .ifmib = DEVMAN_IF_MIB_TOOLONGFRA, .offset = HSES10G_STAT_RX_TOOLONG_LSB
    },
    {
        .ifmib = DEVMAN_IF_MIB_SNGL_COLLISION, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_MULT_COLLISION, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_LATE_COLLISION, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_EXCESS_COLLISION, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_INUNKNOWNOPCODE, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_DEFERREDTX, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_INPAUSE_FRAMES, .offset = HSES10G_STAT_RX_PAUSE_LSB
    },
    {
        .ifmib = DEVMAN_IF_MIB_OUTPAUSE_FRAMES, .offset = HSES10G_STAT_TX_PAUSE_LSB
    },
    {
        .ifmib = DEVMAN_IF_MIB_INFRAOVERSIZE, .offset = HSES10G_STAT_RX_OVERSIZE_LSB
    },
    {
        .ifmib = DEVMAN_IF_MIB_OUTFRAOVERSIZE, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_INFRAFRAGMENTS, .offset = HSES10G_STAT_RX_FRAGMENT_LSB
    },
    {
        .ifmib = DEVMAN_IF_MIB_OUTFRAFRAGMENTS, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_INJABBERS, .offset = HSES10G_STAT_RX_JABBER_LSB
    },
    {
        .ifmib = DEVMAN_IF_MIB_OUTJABBERS, .offset = HSES10G_STAT_INVALID_OFFSET
    }
};

static eth_counter_t _hses10g_counters[] = {
    {
        .name = "rx_framing_err_low_cnt", .offset = HSES10G_STAT_RX_FRAMING_ERR_LSB
    },
    {
        .name = "rx_framing_err_high_cnt", .offset = HSES10G_STAT_RX_FRAMING_ERR_MSB
    },
    {
        .name = "rx_bad_code_low_cnt", .offset = HSES10G_STAT_RX_BAD_CODE_LSB
    },
    {
        .name = "rx_bad_code_high_cnt", .offset = HSES10G_STAT_RX_BAD_CODE_MSB
    },
    {
        .name = "rx_error_low_cnt", .offset = HSES10G_STAT_RX_ERROR_LSB
    },
    {
        .name = "rx_error_high_cnt", .offset = HSES10G_STAT_RX_ERROR_MSB
    },
    {
        .name = "rx_rsfec_corrected_cw_inc_low_cnt", .offset = HSES10G_STAT_RX_RSFEC_CORRECTED_CW_INC_LSB
    },
    {
        .name = "rx_rsfec_corrected_cw_inc_high_cnt", .offset = HSES10G_STAT_RX_RSFEC_CORRECTED_CW_INC_MSB
    },
    {
        .name = "rx_rsfec_uncorrected_cw_inc_low_cnt", .offset = HSES10G_STAT_RX_RSFEC_UNCORRECTED_CW_INC_LSB
    },
    {
        .name = "rx_rsfec_uncorrected_cw_inc_high_cnt", .offset = HSES10G_STAT_RX_RSFEC_UNCORRECTED_CW_INC_MSB
    },
    {
        .name = "rx_rsfec_err_count0_inc_low_cnt", .offset = HSES10G_STAT_RX_RSFEC_ERR_COUNT0_INC_LSB
    },
    {
        .name = "rx_rsfec_err_count0_inc_high_cnt", .offset = HSES10G_STAT_RX_RSFEC_ERR_COUNT0_INC_MSB
    },
    {
        .name = "tx_frame_error_low_cnt", .offset = HSES10G_STAT_TX_FRAME_ERROR_LSB
    },
    {
        .name = "tx_frame_error_high_cnt", .offset = HSES10G_STAT_TX_FRAME_ERROR_MSB
    },
    {
        .name = "tx_total_packets_low_cnt", .offset = HSES10G_STAT_TX_TOTAL_PACKETS_LSB
    },
    {
        .name = "tx_total_packets_high_cnt", .offset = HSES10G_STAT_TX_TOTAL_PACKETS_MSB
    },
    {
        .name = "tx_good_packets_low_cnt", .offset = HSES10G_STAT_TX_TOTAL_GOOD_PACKETS_LSB
    },
    {
        .name = "tx_good_packets_high_cnt", .offset = HSES10G_STAT_TX_TOTAL_GOOD_PACKETS_MSB
    },
    {
        .name = "tx_total_bytes_low_cnt", .offset = HSES10G_STAT_TX_TOTAL_BYTES_LSB
    },
    {
        .name = "tx_total_bytes_high_cnt", .offset = HSES10G_STAT_TX_TOTAL_BYTES_MSB
    },
    {
        .name = "tx_total_good_bytes_low_cnt", .offset = HSES10G_STAT_TX_TOTAL_GOOD_BYTES_LSB
    },
    {
        .name = "tx_total_good_bytes_high_cnt", .offset = HSES10G_STAT_TX_TOTAL_GOOD_BYTES_MSB
    },
    {
        .name = "tx_packet_64_bytes_low_cnt", .offset = HSES10G_STAT_TX_PACKET_64_BYTES_LSB
    },
    {
        .name = "tx_packet_64_bytes_high_cnt", .offset = HSES10G_STAT_TX_PACKET_64_BYTES_MSB
    },
    {
        .name = "tx_packet_65_127_low_cnt", .offset = HSES10G_STAT_TX_PACKET_65_127_BYTES_LSB
    },
    {
        .name = "tx_packet_65_127_high_cnt", .offset = HSES10G_STAT_TX_PACKET_65_127_BYTES_MSB
    },
    {
        .name = "tx_packet_128_255_low_cnt", .offset = HSES10G_STAT_TX_PACKET_128_255_BYTES_LSB
    },
    {
        .name = "tx_packet_128_255_high_cnt", .offset = HSES10G_STAT_TX_PACKET_128_255_BYTES_MSB
    },
    {
        .name = "tx_packet_256_511_low_cnt", .offset = HSES10G_STAT_TX_PACKET_256_511_BYTES_LSB
    },
    {
        .name = "tx_packet_256_511_high_cnt", .offset = HSES10G_STAT_TX_PACKET_256_511_BYTES_MSB
    },
    {
        .name = "tx_packet_512_1023_low_cnt", .offset = HSES10G_STAT_TX_PACKET_512_1023_BYTES_LSB
    },
    {
        .name = "tx_packet_512_1023_high_cnt", .offset = HSES10G_STAT_TX_PACKET_512_1023_BYTES_MSB
    },
    {
        .name = "tx_packet_1024_1518_low_cnt", .offset = HSES10G_STAT_TX_PACKET_1024_1518_BYTES_LSB
    },
    {
        .name = "tx_packet_1024_1518_high_cnt", .offset = HSES10G_STAT_TX_PACKET_1024_1518_BYTES_MSB
    },
    {
        .name = "tx_packet_1519_1522_low_cnt", .offset = HSES10G_STAT_TX_PACKET_1519_1522_BYTES_LSB
    },
    {
        .name = "tx_packet_1519_1522_high_cnt", .offset = HSES10G_STAT_TX_PACKET_1519_1522_BYTES_MSB
    },
    {
        .name = "tx_packet_1523_1548_low_cnt", .offset = HSES10G_STAT_TX_PACKET_1523_1548_BYTES_LSB
    },
    {
        .name = "tx_packet_1523_1548_high_cnt", .offset = HSES10G_STAT_TX_PACKET_1523_1548_BYTES_MSB
    },
    {
        .name = "tx_packet_1549_2047_low_cnt", .offset = HSES10G_STAT_TX_PACKET_1549_2047_BYTES_LSB
    },
    {
        .name = "tx_packet_1549_2047_high_cnt", .offset = HSES10G_STAT_TX_PACKET_1549_2047_BYTES_MSB
    },
    {
        .name = "tx_packet_2048_4095_low_cnt", .offset = HSES10G_STAT_TX_PACKET_2048_4095_BYTES_LSB
    },
    {
        .name = "tx_packet_2048_4095_high_cnt", .offset = HSES10G_STAT_TX_PACKET_2048_4095_BYTES_MSB
    },
    {
        .name = "tx_packet_4096_8191_low_cnt", .offset = HSES10G_STAT_TX_PACKET_4096_8191_BYTES_LSB
    },
    {
        .name = "tx_packet_4096_8191_high_cnt", .offset = HSES10G_STAT_TX_PACKET_4096_8191_BYTES_MSB
    },
    {
        .name = "tx_packet_8192_9215_low_cnt", .offset = HSES10G_STAT_TX_PACKET_8192_9215_BYTES_LSB
    },
    {
        .name = "tx_packet_8192_9215_high_cnt", .offset = HSES10G_STAT_TX_PACKET_8192_9215_BYTES_MSB
    },
    {
        .name = "tx_packet_large_low_cnt", .offset = HSES10G_STAT_TX_PACKET_LARGE_LSB
    },
    {
        .name = "tx_packet_large_high_cnt", .offset = HSES10G_STAT_TX_PACKET_LARGE_MSB
    },
    {
        .name = "tx_packet_small_low_cnt", .offset = HSES10G_STAT_TX_PACKET_SMALL_LSB
    },
    {
        .name = "tx_packet_small_high_cnt", .offset = HSES10G_STAT_TX_PACKET_SMALL_MSB
    },
    {
        .name = "tx_bad_fcs_low_cnt", .offset = HSES10G_STAT_TX_BAD_FCS_LSB
    },
    {
        .name = "tx_bad_fcs_high_cnt", .offset = HSES10G_STAT_TX_BAD_FCS_MSB
    },
    {
        .name = "tx_ucast_low_cnt", .offset = HSES10G_STAT_TX_UNICAST_LSB
    },
    {
        .name = "tx_ucast_high_cnt", .offset = HSES10G_STAT_TX_UNICAST_MSB
    },
    {
        .name = "tx_mcast_low_cnt", .offset = HSES10G_STAT_TX_MULTICAST_LSB
    },
    {
        .name = "tx_mcast_high_cnt", .offset = HSES10G_STAT_TX_MULTICAST_MSB
    },
    {
        .name = "tx_bcast_low_cnt", .offset = HSES10G_STAT_TX_BROADCAST_LSB
    },
    {
        .name = "tx_bcast_high_cnt", .offset = HSES10G_STAT_TX_BROADCAST_MSB
    },
    {
        .name = "tx_vlan_low_cnt", .offset = HSES10G_STAT_TX_VLAN_LSB
    },
    {
        .name = "tx_vlan_high_cnt", .offset = HSES10G_STAT_TX_VLAN_MSB
    },
    {
        .name = "tx_pause_low_cnt", .offset = HSES10G_STAT_TX_PAUSE_LSB
    },
    {
        .name = "tx_pause_high_cnt", .offset = HSES10G_STAT_TX_PAUSE_MSB
    },
    {
        .name = "tx_user_pause_low_cnt", .offset = HSES10G_STAT_TX_USER_PAUSE_LSB
    },
    {
        .name = "tx_user_pause_high_cnt", .offset = HSES10G_STAT_TX_USER_PAUSE_MSB
    },
    {
        .name = "rx_total_packets_low_cnt", .offset = HSES10G_STAT_RX_TOTAL_PACKETS_LSB
    },
    {
        .name = "rx_total_packets_high_cnt", .offset = HSES10G_STAT_RX_TOTAL_PACKETS_MSB
    },
    {
        .name = "rx_total_good_packets_low_cnt", .offset = HSES10G_STAT_RX_TOTAL_GOOD_PACKETS_LSB
    },
    {
        .name = "rx_total_good_packets_high_cnt", .offset = HSES10G_STAT_RX_TOTAL_GOOD_PACKETS_MSB
    },
    {
        .name = "rx_total_bytes_low_cnt", .offset = HSES10G_STAT_RX_TOTAL_BYTES_LSB
    },
    {
        .name = "rx_total_bytes_high_cnt", .offset = HSES10G_STAT_RX_TOTAL_BYTES_MSB
    },
    {
        .name = "rx_total_good_bytes_low_cnt", .offset = HSES10G_STAT_RX_TOTAL_GOOD_BYTES_LSB
    },
    {
        .name = "rx_total_good_bytes_high_cnt", .offset = HSES10G_STAT_RX_TOTAL_GOOD_BYTES_MSB
    },
    {
        .name = "rx_packet_64_bytes_low_cnt", .offset = HSES10G_STAT_RX_PACKET_64_BYTES_LSB
    },
    {
        .name = "rx_packet_64_bytes_high_cnt", .offset = HSES10G_STAT_RX_PACKET_64_BYTES_MSB
    },
    {
        .name = "rx_packet_65_127_bytes_low_cnt", .offset = HSES10G_STAT_RX_PACKET_65_127_BYTES_LSB
    },
    {
        .name = "rx_packet_65_127_bytes_high_cnt", .offset = HSES10G_STAT_RX_PACKET_65_127_BYTES_MSB
    },
    {
        .name = "rx_packet_128_255_bytes_low_cnt", .offset = HSES10G_STAT_RX_PACKET_128_255_BYTES_LSB
    },
    {
        .name = "rx_packet_128_255_bytes_high_cnt", .offset = HSES10G_STAT_RX_PACKET_128_255_BYTES_MSB
    },
    {
        .name = "rx_packet_256_511_bytes_low_cnt", .offset = HSES10G_STAT_RX_PACKET_256_511_BYTES_LSB
    },
    {
        .name = "rx_packet_256_511_bytes_high_cnt", .offset = HSES10G_STAT_RX_PACKET_256_511_BYTES_MSB
    },
    {
        .name = "rx_packet_512_1023_bytes_low_cnt", .offset = HSES10G_STAT_RX_PACKET_512_1023_BYTES_LSB
    },
    {
        .name = "rx_packet_512_1023_bytes_high_cnt", .offset = HSES10G_STAT_RX_PACKET_512_1023_BYTES_MSB
    },
    {
        .name = "rx_packet_1024_1518_bytes_low_cnt", .offset = HSES10G_STAT_RX_PACKET_1024_1518_BYTES_LSB
    },
    {
        .name = "rx_packet_1024_1518_bytes_high_cnt", .offset = HSES10G_STAT_RX_PACKET_1024_1518_BYTES_MSB
    },
    {
        .name = "rx_packet_1519_1522_bytes_low_cnt", .offset = HSES10G_STAT_RX_PACKET_1519_1522_BYTES_LSB
    },
    {
        .name = "rx_packet_1519_1522_bytes_high_cnt", .offset = HSES10G_STAT_RX_PACKET_1519_1522_BYTES_MSB
    },
    {
        .name = "rx_packet_1523_1548_bytes_low_cnt", .offset = HSES10G_STAT_RX_PACKET_1523_1548_BYTES_LSB
    },
    {
        .name = "rx_packet_1523_1548_bytes_high_cnt", .offset = HSES10G_STAT_RX_PACKET_1523_1548_BYTES_MSB
    },
    {
        .name = "rx_packet_1549_2047_bytes_low_cnt", .offset = HSES10G_STAT_RX_PACKET_1549_2047_BYTES_LSB
    },
    {
        .name = "rx_packet_1549_2047_bytes_high_cnt", .offset = HSES10G_STAT_RX_PACKET_1549_2047_BYTES_MSB
    },
    {
        .name = "rx_packet_2048_4095_bytes_low_cnt", .offset = HSES10G_STAT_RX_PACKET_2048_4095_BYTES_LSB
    },
    {
        .name = "rx_packet_2048_4095_bytes_high_cnt", .offset = HSES10G_STAT_RX_PACKET_2048_4095_BYTES_MSB
    },
    {
        .name = "rx_packet_4096_8191_bytes_low_cnt", .offset = HSES10G_STAT_RX_PACKET_4096_8191_BYTES_LSB
    },
    {
        .name = "rx_packet_4096_8191_bytes_high_cnt", .offset = HSES10G_STAT_RX_PACKET_4096_8191_BYTES_MSB
    },
    {
        .name = "rx_packet_8192_9215_bytes_low_cnt", .offset = HSES10G_STAT_RX_PACKET_8192_9215_BYTES_LSB
    },
    {
        .name = "rx_packet_8192_9215_bytes_high_cnt", .offset = HSES10G_STAT_RX_PACKET_8192_9215_BYTES_MSB
    },
    {
        .name = "rx_packet_large_low_cnt", .offset = HSES10G_STAT_RX_PACKET_LARGE_LSB
    },
    {
        .name = "rx_packet_large_high_cnt", .offset = HSES10G_STAT_RX_PACKET_LARGE_MSB
    },
    {
        .name = "rx_packet_small_low_cnt", .offset = HSES10G_STAT_RX_PACKET_SMALL_LSB
    },
    {
        .name = "rx_packet_small_high_cnt", .offset = HSES10G_STAT_RX_PACKET_SMALL_MSB
    },
    {
        .name = "rx_undersize_low_cnt", .offset = HSES10G_STAT_RX_UNDERSIZE_LSB
    },
    {
        .name = "rx_undersize_high_cnt", .offset = HSES10G_STAT_RX_UNDERSIZE_MSB
    },
    {
        .name = "rx_fragment_low_cnt", .offset = HSES10G_STAT_RX_FRAGMENT_LSB
    },
    {
        .name = "rx_fragment_high_cnt", .offset = HSES10G_STAT_RX_FRAGMENT_MSB
    },
    {
        .name = "rx_oversize_low_cnt", .offset = HSES10G_STAT_RX_OVERSIZE_LSB
    },
    {
        .name = "rx_oversize_high_cnt", .offset = HSES10G_STAT_RX_OVERSIZE_MSB
    },
    {
        .name = "rx_toolong_low_cnt", .offset = HSES10G_STAT_RX_TOOLONG_LSB
    },
    {
        .name = "rx_toolong_high_cnt", .offset = HSES10G_STAT_RX_TOOLONG_MSB
    },
    {
        .name = "rx_jabber_low_cnt", .offset = HSES10G_STAT_RX_JABBER_LSB
    },
    {
        .name = "rx_jabber_high_cnt", .offset = HSES10G_STAT_RX_JABBER_MSB
    },
    {
        .name = "rx_bad_fcs_low_cnt", .offset = HSES10G_STAT_RX_BAD_FCS_LSB
    },
    {
        .name = "rx_bad_fcs_high_cnt", .offset = HSES10G_STAT_RX_BAD_FCS_MSB
    },
    {
        .name = "rx_packet_bad_fcs_low_cnt", .offset = HSES10G_STAT_RX_PACKET_BAD_FCS_LSB
    },
    {
        .name = "rx_packet_bad_fcs_high_cnt", .offset = HSES10G_STAT_RX_PACKET_BAD_FCS_MSB
    },
    {
        .name = "rx_stomped_fcs_low_cnt", .offset = HSES10G_STAT_RX_STOMPED_FCS_LSB
    },
    {
        .name = "rx_stomped_fcs_high_cnt", .offset = HSES10G_STAT_RX_STOMPED_FCS_MSB
    },
    {
        .name = "rx_ucast_low_cnt", .offset = HSES10G_STAT_RX_UNICAST_LSB
    },
    {
        .name = "rx_ucast_high_cnt", .offset = HSES10G_STAT_RX_UNICAST_MSB
    },
    {
        .name = "rx_mcast_low_cnt", .offset = HSES10G_STAT_RX_MULTICAST_LSB
    },
    {
        .name = "rx_mcast_high_cnt", .offset = HSES10G_STAT_RX_MULTICAST_MSB
    },
    {
        .name = "rx_bcast_low_cnt", .offset = HSES10G_STAT_RX_BROADCAST_LSB
    },
    {
        .name = "rx_bcast_high_cnt", .offset = HSES10G_STAT_RX_BROADCAST_MSB
    },
    {
        .name = "rx_vlan_low_cnt", .offset = HSES10G_STAT_RX_VLAN_LSB
    },
    {
        .name = "rx_vlan_high_cnt", .offset = HSES10G_STAT_RX_VLAN_MSB
    },
    {
        .name = "rx_pause_low_cnt", .offset = HSES10G_STAT_RX_PAUSE_LSB
    },
    {
        .name = "rx_pause_high_cnt", .offset = HSES10G_STAT_RX_PAUSE_MSB
    },
    {
        .name = "rx_user_pause_low_cnt", .offset = HSES10G_STAT_RX_USER_PAUSE_LSB
    },
    {
        .name = "rx_user_pause_high_cnt", .offset = HSES10G_STAT_RX_USER_PAUSE_MSB
    },
    {
        .name = "rx_inrangeerr_low_cnt", .offset = HSES10G_STAT_RX_INRANGEERR_LSB
    },
    {
        .name = "rx_inrangeerr_high_cnt", .offset = HSES10G_STAT_RX_INRANGEERR_MSB
    },
    {
        .name = "rx_truncated_low_cnt", .offset = HSES10G_STAT_RX_TRUNCATED_LSB
    },
    {
        .name = "rx_truncated_high_cnt", .offset = HSES10G_STAT_RX_TRUNCATED_MSB
    },
    {
        .name = "rx_test_pattern_mismatch_low_cnt", .offset = HSES10G_STAT_RX_TEST_PATTERN_MISMATCH_LSB
    },
    {
        .name = "rx_test_pattern_mismatch_high_cnt", .offset = HSES10G_STAT_RX_TEST_PATTERN_MISMATCH_MSB
    },
    {
        .name = "fec_inc_correct_low_cnt", .offset = HSES10G_STAT_FEC_INC_CORRECT_COUNT_LSB
    },
    {
        .name = "fec_inc_correct_high_cnt", .offset = HSES10G_STAT_FEC_INC_CORRECT_COUNT_MSB
    },
    {
        .name = "fec_inc_cant_correct_low_cnt", .offset = HSES10G_STAT_FEC_INC_CANT_CORRECT_COUNT_LSB
    },
    {
        .name = "fec_inc_cant_correct_high_cnt", .offset = HSES10G_STAT_FEC_INC_CANT_CORRECT_COUNT_MSB
    },
    {
        .name = "tx_mm_status_low_cnt", .offset = HSES10G_STAT_TX_MM_STATUS_LSB
    },
    {
        .name = "tx_mm_status_high_cnt", .offset = HSES10G_STAT_TX_MM_STATUS_MSB
    },
    {
        .name = "tx_mm_fragment_low_cnt", .offset = HSES10G_STAT_TX_MM_FRAGMENT_LSB
    },
    {
        .name = "tx_mm_fragment_high_cnt", .offset = HSES10G_STAT_TX_MM_FRAGMENT_MSB
    },
    {
        .name = "tx_mm_hold_low_cnt", .offset = HSES10G_STAT_TX_MM_HOLD_LSB
    },
    {
        .name = "tx_mm_hold_high_cnt", .offset = HSES10G_STAT_TX_MM_HOLD_MSB
    },
    {
        .name = "rx_mm_assembly_error_low_cnt", .offset = HSES10G_STAT_RX_MM_ASSEMBLY_ERROR_LSB
    },
    {
        .name = "rx_mm_assembly_error_high_cnt", .offset = HSES10G_STAT_RX_MM_ASSEMBLY_ERROR_MSB
    },
    {
        .name = "rx_mm_frame_smd_error_low_cnt", .offset = HSES10G_STAT_RX_MM_FRAME_SMD_ERROR_LSB
    },
    {
        .name = "rx_mm_frame_smd_error_high_cnt", .offset = HSES10G_STAT_RX_MM_FRAME_SMD_ERROR_MSB
    },
    {
        .name = "rx_mm_frame_assembly_ok_low_cnt", .offset = HSES10G_STAT_RX_MM_FRAME_ASSEMBLY_OK_LSB
    },
    {
        .name = "rx_mm_frame_assembly_ok_high_cnt", .offset = HSES10G_STAT_RX_MM_FRAME_ASSEMBLY_OK_MSB
    },
    {
        .name = "rx_mm_fragment_low_cnt", .offset = HSES10G_STAT_RX_MM_FRAGMENT_LSB
    },
    {
        .name = "rx_mm_fragment_high_cnt", .offset = HSES10G_STAT_RX_MM_FRAGMENT_MSB
    },
};
#else
static mib_counter_map_t _hses10g_mib_cntr_map[] = {
    {
        .ifmib = DEVMAN_IF_MIB_BYTEREC, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_BYTSENT, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRAREC, .offset = HSES10G_STAT_RX_TOTAL_PACKETS_LSB
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRASENT, .offset = HSES10G_STAT_TX_TOTAL_PACKETS_LSB
    },
    {
        .ifmib = DEVMAN_IF_MIB_TOTALBYTEREC, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_TOTALFRAMEREC, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_BRDFRAREC, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_MULFRAREC, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_CRCERR, .offset = HSES10G_STAT_RX_BAD_FCS_LSB
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRAOVERSIZE, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRAFRAGMENTS, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_JABBER, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_COLL, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_LATECOL, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRA64, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRA65T127, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRA128T255, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRA256T511, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRA512T1023, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRA1024T1522, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_MACRXERR, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_DROPPED, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_INUCASTPKT, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_INNOUCASTPKT, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_INERRORS, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_OUTERRORS, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_OUTNOUCASTPKT, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_OUTUCASTPKT, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_MULTIOUT, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_BROADOUT, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_UNDERSIZ, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_INUNKNOWNPROTOS, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_ALIGN_ERR, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_FCS_ERR, .offset = HSES10G_STAT_RX_BAD_FCS_LSB
    },
    {
        .ifmib = DEVMAN_IF_MIB_SQETEST_ERR, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_CSE_ERR, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_SYMBOL_ERR, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_MACTX_ERR, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_MACRX_ERR, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_TOOLONGFRA, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_SNGL_COLLISION, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_MULT_COLLISION, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_LATE_COLLISION, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_EXCESS_COLLISION, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_INUNKNOWNOPCODE, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_DEFERREDTX, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_INPAUSE_FRAMES, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_OUTPAUSE_FRAMES, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_INFRAOVERSIZE, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_OUTFRAOVERSIZE, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_INFRAFRAGMENTS, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_OUTFRAFRAGMENTS, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_INJABBERS, .offset = HSES10G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_OUTJABBERS, .offset = HSES10G_STAT_INVALID_OFFSET
    }
};

static eth_counter_t _hses10g_counters[] = {
    {
        .name = "tx_total_packets_low_cnt"
    },
    {
        .name = "tx_total_packets_high_cnt"
    },
    {
        .name = "rx_total_packets_low_cnt"
    },
    {
        .name = "rx_total_packets_high_cnt"
    },
    {
        .name = "rx_bad_packets_low_cnt"
    },
    {
        .name = "rx_bad_packets_high_cnt"
    },
};
#endif

/* hses10g register read */
static ccl_err_t _hses10g_reg_read(__attribute__((unused))  void *p_priv, 
                                   b_u32 offset, 
                                   __attribute__((unused)) b_u32 offset_sz, 
                                   b_u32 idx, b_u8 *p_value, b_u32 size)
{
    hses10g_ctx_t *p_hses10g;

    if ( offset > HSES10G_MAX_OFFSET || 
         !p_value || idx || size != sizeof(b_u32) ) {
        PDEBUG("Error! Invalid input parameters\n");
        HSES10G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    PDEBUGG("123$=> offset=0x%03x, size=%d\n", offset, size);

    p_hses10g = (hses10g_ctx_t *)p_priv;     
    offset += p_hses10g->brdspec.offset;

    _ret = devspibus_read_buff(p_hses10g->brdspec.devname, offset, idx, p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to read over spi bus\n");
        HSES10G_ERROR_RETURN(_ret);
    }    
    PDEBUGG("p_hses10g->brdspec.offset=0x%x, offset=0x%03x, value=0x%08x\n", 
           p_hses10g->brdspec.offset, offset, *(b_u32 *)p_value);

    return _ret;
}

/* hses10g register write */
static ccl_err_t _hses10g_reg_write(__attribute__((unused)) void *p_priv, 
                                    b_u32 offset, 
                                    __attribute__((unused)) b_u32 offset_sz, 
                                    b_u32 idx, b_u8 *p_value, b_u32 size)
{
    hses10g_ctx_t *p_hses10g;

    if ( offset > HSES10G_MAX_OFFSET || 
         !p_value || idx || size != sizeof(b_u32) ) {
        PDEBUG("Error! Invalid input parameters\n");
        HSES10G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    PDEBUGG("123$=> offset=0x%03x, value=0x%08x\n", offset, *p_value);

    p_hses10g = (hses10g_ctx_t *)p_priv;     
    offset += p_hses10g->brdspec.offset;

    PDEBUG("p_hses10g->brdspec.offset=0x%x, offset=0x%03x, value=0x%08x\n", 
           p_hses10g->brdspec.offset, offset, *(b_u32 *)p_value);

    _ret = devspibus_write_buff(p_hses10g->brdspec.devname, offset, idx, p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to write over spi bus\n");
        HSES10G_ERROR_RETURN(_ret);
    }    

    return _ret;
}

/**
 * @brief Sets calibration parameters into DRP register space
 *
 * @param[in] p_hses10g is a pointer to hses10g_ctx_t instance to 
 *       be worked on
 * @return	error code
 */
static ccl_err_t _hses10g_calib_set(hses10g_ctx_t *p_hses10g)
{
    b_u32 i, j;

    if ( !p_hses10g ) {
        PDEBUG("Error! Invalid input parameters\n");
        HSES10G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    if (p_hses10g->brdspec.calib_ctl.enable) {
        for (i = 0; i < p_hses10g->brdspec.calib_ctl.channel_num; i++) {
            for (j = 0; j < CALIB_PARAMS_PER_CHANNEL_NUM; j++) {
                b_u32 offset = p_hses10g->brdspec.calib_ctl.channel_calib_params[i].calib[j].offset;
                b_u8 value = p_hses10g->brdspec.calib_ctl.channel_calib_params[i].calib[j].encoded_value;
                PDEBUG("channel=%d, param=%d, offset=0x%x, value=0x%02x\n",
                       i, j, offset, value);

                _ret = devspibus_write_buff(p_hses10g->brdspec.devname, 
                                            offset,
                                            0, 
                                            &value, 
                                            sizeof(b_u32));
                if ( _ret ) {
                    PDEBUG("Error! Failed to write over spi bus\n");
                    HSES10G_ERROR_RETURN(_ret);
                }    
            }
        }
    }

    return CCL_OK;
}

/**
 * @brief Start the HSES10G Ethernet device as follows: 
 *	- Enable transmitter
 *	- Enable receiver
 *	- Upon enabling RX, check for RX BLOCK LOCK bit to make sure
 *         RX channel is ready.
 *
 * @param[in] p_hses10g is a pointer to hses10g_ctx_t instance to 
 *       be worked on
 * @return	CCL_FAIL if RX is enabled and BLOCK LOCK is not 
 *         set, CCL_OK otherwise.
 */
static ccl_err_t _hses10g_start(hses10g_ctx_t *p_hses10g)
{
    b_u32 regval;
    b_i32 timeout = 10000;

    if ( !p_hses10g ) {
        PDEBUG("Error! Invalid input parameters\n");
        HSES10G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* Enable transmitter if not already enabled */
    HSES10G_REG_READ(p_hses10g, HSES10G_CONFIGURATION_TX_REG1, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));

    if (!(regval & HSES10G_TXCFG_TX_MASK)) {
        regval |= HSES10G_TXCFG_TX_MASK;
        HSES10G_REG_WRITE(p_hses10g, HSES10G_CONFIGURATION_TX_REG1, 
                          sizeof(b_u32), 0, 
                          (b_u8 *)&regval, sizeof(b_u32));
    }
    /* Enable receiver if not already enabled*/
    HSES10G_REG_READ(p_hses10g, HSES10G_CONFIGURATION_RX_REG1, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));
    if (!(regval & HSES10G_RXCFG_RX_MASK)) {
        regval |= HSES10G_RXCFG_RX_MASK;
        HSES10G_REG_WRITE(p_hses10g, HSES10G_CONFIGURATION_RX_REG1, 
                          sizeof(b_u32), 0, 
                          (b_u8 *)&regval, sizeof(b_u32));
    }

    /* RX block lock */
    /* Do a dummy read because this is a sticky bit */
    HSES10G_REG_READ(p_hses10g, HSES10G_STAT_RX_BLOCK_LOCK_REG, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));
#if 0
    do {
        timeout--;
        if (timeout <= 0) {
            PDEBUG("ERROR: Block lock is not set\n");
            HSES10G_ERROR_RETURN(CCL_FAIL);
        }
        HSES10G_REG_READ(p_hses10g, HSES10G_STAT_RX_BLOCK_LOCK_REG, 
                         sizeof(b_u32), 0, 
                         (b_u8 *)&regval, sizeof(b_u32));
    } while (!(regval & HSES10G_RXBLKLCK_MASK));
#endif
    return CCL_OK;
}

/**
 * @brief Gracefully stop the HSES10G Ethernet device by
 * disabling the receiver.
 *
 * @param[in] p_hses10g is a pointer to hses10g_ctx_t instance to 
 *       be worked on
 *  
 * @return	error code
 */
static ccl_err_t _hses10g_stop(hses10g_ctx_t *p_hses10g)
{
    b_u32 regval;

    if ( !p_hses10g ) {
        PDEBUG("Error! Invalid input parameters\n");
        HSES10G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* Disable the receiver */
    HSES10G_REG_READ(p_hses10g, HSES10G_CONFIGURATION_RX_REG1, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));
    regval &= ~HSES10G_RXCFG_RX_MASK;
    HSES10G_REG_WRITE(p_hses10g, HSES10G_CONFIGURATION_RX_REG1, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

static ccl_err_t _hses10g_kr_init(hses10g_ctx_t *p_hses10g)
{
    b_u32 offset;
    b_u32 regval;

    if ( !p_hses10g ) {
        PDEBUG("Error! Invalid input parameters\n");
        HSES10G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    offset = p_hses10g->brdspec.autoneg_ctl.autoneg[0].offset;
    regval = 1;
    _ret = devspibus_write_buff(p_hses10g->brdspec.devname, 
                                offset,
                                0, 
                                (b_u8 *)&regval, 
                                sizeof(b_u32));
    if ( _ret ) {
        PDEBUG("Error! Failed to write over spi bus\n");
        HSES10G_ERROR_RETURN(_ret);
    }

    regval = 0x09;
    HSES10G_REG_WRITE(p_hses10g, HSES10G_CONFIGURATION_AN_CONTROL_REG1, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));
#if 0
    regval = 0x10000;
    HSES10G_REG_WRITE(p_hses10g, HSES10G_CONFIGURATION_AN_CONTROL_REG2, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));
    regval = 0x03;
    HSES10G_REG_WRITE(p_hses10g, HSES10G_CONFIGURATION_FEC_REG, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));
#endif
    regval = 0x04;
    HSES10G_REG_WRITE(p_hses10g, HSES10G_CONFIGURATION_AN_ABILITY, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));
    regval = 0x01;
    HSES10G_REG_WRITE(p_hses10g, HSES10G_CONFIGURATION_LT_CONTROL_REG1, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));
    regval = 0x01;
    HSES10G_REG_WRITE(p_hses10g, HSES10G_CONFIGURATION_LT_TRAINED_REG, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));
    regval = 0x01;
    HSES10G_REG_WRITE(p_hses10g, HSES10G_CONFIGURATION_LT_PRESET_REG, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));
    regval = 0x01;
    HSES10G_REG_WRITE(p_hses10g, HSES10G_CONFIGURATION_LT_INIT_REG, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));
    regval = 0x06070605;
    HSES10G_REG_WRITE(p_hses10g, HSES10G_CONFIGURATION_LT_SEED_REG0, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));
    regval = 0x55555555;
    HSES10G_REG_WRITE(p_hses10g, HSES10G_CONFIGURATION_LT_COEFFICIENT_REG0, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));
    regval = 0x55555555;
    HSES10G_REG_WRITE(p_hses10g, HSES10G_USER_REG_0, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));
    regval = 0x33;
    HSES10G_REG_WRITE(p_hses10g, HSES10G_CONFIGURATION_RX_REG1, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));
    regval = 0x3003;
    HSES10G_REG_WRITE(p_hses10g, HSES10G_CONFIGURATION_TX_REG1, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));
    
    regval = 0;
    _ret = devspibus_write_buff(p_hses10g->brdspec.devname, 
                                offset,
                                0, 
                                (b_u8 *)&regval, 
                                sizeof(b_u32));
    if ( _ret ) {
        PDEBUG("Error! Failed to write over spi bus\n");
        HSES10G_ERROR_RETURN(_ret);
    }
        
    return CCL_OK;
}

/**
 * @brief Init the HSES10G Ethernet device
 *
 * @param[in] p_hses10g is a pointer to hses10g_ctx_t instance to 
 *       be worked on
 *  
 * @return	error code
 */
static ccl_err_t _hses10g_init(void *p_priv)
{
    hses10g_ctx_t *p_hses10g;
    b_u32 regval;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        HSES10G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_hses10g = (hses10g_ctx_t *)p_priv;

    _ret = _hses10g_calib_set(p_hses10g);
    if ( _ret ) {
        PDEBUG("Error! _hses10g_calib_set failed\n");
        HSES10G_ERROR_RETURN(CCL_FAIL);
    }

    /* Disable the receiver and turn on FCS stripping on receive packets*/
    HSES10G_REG_READ(p_hses10g, HSES10G_CONFIGURATION_RX_REG1, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));
    regval &= ~HSES10G_RXCFG_RX_MASK;
    regval |= HSES10G_RXCFG_DEL_FCS_MASK;
    HSES10G_REG_WRITE(p_hses10g, HSES10G_CONFIGURATION_RX_REG1, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));

    /* Disable the transmitter and turn on FCS insertion on transmit packets*/
    HSES10G_REG_READ(p_hses10g, HSES10G_CONFIGURATION_TX_REG1, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));
    regval &= ~HSES10G_TXCFG_TX_MASK;
    regval |= HSES10G_TXCFG_FCS_MASK;
    HSES10G_REG_WRITE(p_hses10g, HSES10G_CONFIGURATION_TX_REG1, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));

    if (p_hses10g->brdspec.iftype == eDEVBRD_IFTYPE_KR) {
        _ret = _hses10g_kr_init(p_hses10g);
        if ( _ret ) {
            PDEBUG("_hses10g_kr_init error\n");
            HSES10G_ERROR_RETURN(_ret);
        }
    }

    /* start the device */
    _ret = _hses10g_start(p_hses10g);
    if ( _ret ) {
        PDEBUG("_hses10g_start error\n");
        HSES10G_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

/**
 * @brief Issue tick so that statistics are copied to readable 
 * registers 
 *
 * @param None
 *
 * @return CCL_OK on success, error code on failure 
 */
static ccl_err_t _hses10g_latch_statistics(hses10g_ctx_t *p_hses10g)
{
    b_u32 regval;

    if ( !p_hses10g ) {
        PDEBUG("Error! Invalid input parameters\n");
        HSES10G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    HSES10G_REG_READ(p_hses10g, HSES10G_MODE_REG, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));
    if (!(regval & HSES10G_MODE_TICK_REG_MODE_MASK)) {
        regval |= HSES10G_MODE_TICK_REG_MODE_MASK;
        HSES10G_REG_WRITE(p_hses10g, HSES10G_MODE_REG, 
                          sizeof(b_u32), 0, 
                          (b_u8 *)&regval, sizeof(b_u32));
    }
    regval = 0x1;
    HSES10G_REG_WRITE(p_hses10g, HSES10G_TICK_REG, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/**
 * @brief Read 48 bit value from the registers
 *
 * @param[in] address_l is lower address of the register. 
 * @param[out] pvalue is a pointer to hold 48 bit value
 *
 * @return CCL_OK on success, error code on failure
 *
 */
static ccl_err_t _hses10g_read_48bit_value(hses10g_ctx_t *p_hses10g,
                                           b_u32 address_l, b_u64 *pvalue)
{
    b_u32 regval;
    b_u32 address_h = address_l + HSES10G_NUM_WORD_BYTES;

    if ( !p_hses10g || !pvalue ) {
        PDEBUG("Error! Invalid input parameters\n");
        HSES10G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    HSES10G_REG_READ(p_hses10g, address_h, 
                        sizeof(b_u32), 0, 
                        (b_u8 *)&regval, sizeof(b_u32));
    regval &= HSES10G_HALF_WORD_MASK;
    *pvalue = (b_u64)(regval << HSES10G_NUM_WORD_BITS);
    HSES10G_REG_READ(p_hses10g, address_l, 
                        sizeof(b_u32), 0, 
                        (b_u8 *)&regval, sizeof(b_u32));
    PDEBUG("addr_l=0x%08x, val=%d, addr_h=0x%08x, val=%d\n",
           address_l, regval, address_h, (b_u32)*pvalue>>HSES10G_NUM_WORD_BITS);
    *pvalue |= regval;

    return CCL_OK;
}

static ccl_err_t _hses10g_mtu_get(hses10g_ctx_t *p_hses10g, 
                                  b_u8 *pvalue, b_u32 size)
{
    b_u32 regval;
    b_u32 mtu; 

    if ( !p_hses10g || !pvalue || size != sizeof(b_u32)) {
        PDEBUG("Error! Invalid input parameters\n");
        HSES10G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    HSES10G_REG_READ(p_hses10g, HSES10G_CONFIGURATION_RX_MTU, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));

    mtu = (regval & HSES10G_RXMTU_MAX_JUM_MASK) >>
          HSES10G_RXMTU_MAX_JUM_SHIFT;
    memcpy(pvalue, &mtu, sizeof(mtu));

    return CCL_OK;
}

static ccl_err_t _hses10g_link_status_get(hses10g_ctx_t *p_hses10g, 
                                          b_u8 *pvalue, b_u32 size)
{
    b_u32 rx_block_lock;
    b_u32 tx_err;
    b_u32 rx_err;
    b_u32 link; 

    if ( !p_hses10g || !pvalue || size != sizeof(b_u8)) {
        PDEBUG("Error! Invalid input parameters\n");
        HSES10G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    HSES10G_REG_READ(p_hses10g, HSES10G_STAT_RX_BLOCK_LOCK_REG, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&rx_block_lock, sizeof(b_u32));
    HSES10G_REG_READ(p_hses10g, HSES10G_STAT_RX_BLOCK_LOCK_REG, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&rx_block_lock, sizeof(b_u32));

    HSES10G_REG_READ(p_hses10g, HSES10G_STAT_TX_STATUS_REG1, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&tx_err, sizeof(b_u32));
    HSES10G_REG_READ(p_hses10g, HSES10G_STAT_TX_STATUS_REG1, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&tx_err, sizeof(b_u32));

    HSES10G_REG_READ(p_hses10g, HSES10G_STAT_RX_STATUS_REG1, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&rx_err, sizeof(b_u32));
    HSES10G_REG_READ(p_hses10g, HSES10G_STAT_RX_STATUS_REG1, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&rx_err, sizeof(b_u32));

    rx_block_lock &= HSES10G_RXBLKLCK_MASK;
    tx_err &= HSES10G_STS_TX_ERROR_MASK;
    rx_err &= HSES10G_STS_RX_ERROR_MASK;

    *pvalue = (rx_block_lock && !tx_err && !rx_err) ? CCL_TRUE : CCL_FALSE;

    return CCL_OK;
}

static ccl_err_t _hses10g_if_property_get(void *priv, b_u32 prop, 
                                          b_u8 *pvalue, b_u32 size)
{
    hses10g_ctx_t *p_hses10g;
    port_stats_t  *port_stats;
    b_u32         address_l;

    if ( !priv || !pvalue) {
        PDEBUG("Error! Invalid input parameters\n");
        HSES10G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_hses10g = (hses10g_ctx_t *)priv;

    switch (prop) {
    case DEVMAN_IF_NAME:
        strncpy(pvalue, p_hses10g->brdspec.name, size);
        break;
    case DEVMAN_IF_MTU:
        _ret = _hses10g_mtu_get(p_hses10g, pvalue, size);
        break;
    case DEVMAN_IF_SPEED_MAX:
    case DEVMAN_IF_SPEED:
    case DEVMAN_IF_DUPLEX:
        _ret = CCL_NOT_SUPPORT;
        break;
    case DEVMAN_IF_AUTONEG:
    case DEVMAN_IF_ENCAP:
    case DEVMAN_IF_MEDIUM:
    case DEVMAN_IF_INTERFACE_TYPE:
    case DEVMAN_IF_MDIX:
    case DEVMAN_IF_MDIX_STATUS:
    case DEVMAN_IF_LEARN:
    case DEVMAN_IF_IFILTER:
    case DEVMAN_IF_SELF_EFILTER:
    case DEVMAN_IF_LOCK:
    case DEVMAN_IF_DROP_ON_LOCK:
    case DEVMAN_IF_FWD_UNK:
    case DEVMAN_IF_BCAST_LIMIT:
    case DEVMAN_IF_ABILITY:
    case DEVMAN_IF_FLOWCONTROL:
    case DEVMAN_IF_ENABLE:
    case DEVMAN_IF_SFP_PRESENT:
    case DEVMAN_IF_SFP_TX_STATE:
    case DEVMAN_IF_SFP_RX_STATE:
    case DEVMAN_IF_DEFAULT_VLAN:
    case DEVMAN_IF_PRIORITY:
    case DEVMAN_IF_LED_STATE:
    case DEVMAN_IF_MEDIA_PARAMETERS:
    case DEVMAN_IF_MEDIA_DETAILS:
    case DEVMAN_IF_LOCAL_ADVERT:
    case DEVMAN_IF_MEDIA_TYPE:
    case DEVMAN_IF_SFP_PORT_TYPE:
    case DEVMAN_IF_MEDIA_CONFIG_TYPE:
    case DEVMAN_IF_MEDIA_ERROR_CONFIG_TYPE:
    case DEVMAN_IF_MEDIA_SGMII_FIBER:
    case DEVMAN_IF_TX_ENABLE:
    case DEVMAN_IF_SFP_PORT_NUMBERS:
    case DEVMAN_IF_AUTONEG_DUPLEX:
    case DEVMAN_IF_AUTONEG_SPEED :
    case DEVMAN_IF_DDM_PARAMETERS:
    case DEVMAN_IF_DDM_STATUS:     
    case DEVMAN_IF_DDM_SUPPORTED:  
    case DEVMAN_IF_HWADDRESS_GET:
        break;
    case DEVMAN_IF_ACTIVE_LINK:
        _ret = _hses10g_link_status_get(p_hses10g, pvalue, size);
        break;
    default:
        PDEBUG("Error! Invalid property: %d\n", prop);
        HSES10G_ERROR_RETURN(CCL_BAD_PARAM_ERR);          
    }

    return _ret;
}

#ifdef MAC_COUNTERS_ENABLED
static ccl_err_t _hses10g_read_counters(hses10g_ctx_t *p_hses10g)
{
    b_u32 i;

    if ( !p_hses10g ) {
        PDEBUG("Error! Invalid input parameters\n");
        HSES10G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    
    _ret = _hses10g_latch_statistics(p_hses10g);
    if ( _ret ) {
        PDEBUG("_hses10g_latch_statistics error\n");
        HSES10G_ERROR_RETURN(_ret);
    }

    for (i = 0; i < p_hses10g->counters_num; i++) {
        HSES10G_REG_READ(p_hses10g, p_hses10g->counters[i].offset, 
                       sizeof(b_u32), 0, 
                       (b_u8 *)&p_hses10g->counters[i].value, 
                       sizeof(b_u32));
    }

    return CCL_OK;
}

static ccl_err_t _hses10g_read_single_counter(void *priv, b_u32 idx, b_u64 *pvalue)
{
    hses10g_ctx_t *p_hses10g;
    port_stats_t  *port_stats;
    b_u32         address_l;

    if ( !priv || !pvalue) {
        PDEBUG("Error! Invalid input parameters\n");
        HSES10G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_hses10g = (hses10g_ctx_t *)priv;

    address_l = _hses10g_mib_cntr_map[idx].offset;

    _ret = _hses10g_latch_statistics(p_hses10g);
    if ( _ret ) {
        PDEBUG("_hses10g_latch_statistics error\n");
        HSES10G_ERROR_RETURN(_ret);
    }

    if (address_l != HSES10G_STAT_INVALID_OFFSET) {
        /* read counter */
        _ret = _hses10g_read_48bit_value(p_hses10g, address_l, pvalue);
        if ( _ret ) {
            PDEBUG("_hses10g_read_48bit_value error, address: %u\n",
                   address_l);
            HSES10G_ERROR_RETURN(_ret);
        }
    }

    return CCL_OK;
}

static ccl_err_t _hses10g_read_stat_counters(void *priv, b_u8 *buf)
{
    hses10g_ctx_t *p_hses10g;
    port_stats_t  *port_stats;
    b_u32         counter_l;
    b_u32         counter_h;

    if ( !priv || !buf) {
        PDEBUG("Error! Invalid input parameters\n");
        HSES10G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_hses10g = (hses10g_ctx_t *)priv;
    port_stats = (port_stats_t *)buf;

    _ret = _hses10g_latch_statistics(p_hses10g);
    if ( _ret ) {
        PDEBUG("_hses10g_latch_statistics error\n");
        HSES10G_ERROR_RETURN(_ret);
    }

    /* read counters */

    /* tx bytes */
    _ret = _hses10g_read_48bit_value(p_hses10g, HSES10G_STAT_TX_TOTAL_BYTES_LSB,
                              &port_stats->tx_bytes.value);
    /* tx packets */
    _ret |= _hses10g_read_48bit_value(p_hses10g, HSES10G_STAT_TX_TOTAL_PACKETS_LSB,
                              &port_stats->tx_packets.value);
    /* tx packets 64 bytes */
    _ret |= _hses10g_read_48bit_value(p_hses10g, HSES10G_STAT_TX_PACKET_64_BYTES_LSB,
                              &port_stats->tx_packets_64.value);
    /* tx packets 65-127 bytes */
    _ret |= _hses10g_read_48bit_value(p_hses10g, HSES10G_STAT_TX_PACKET_65_127_BYTES_LSB,
                              &port_stats->tx_packets_65_127.value);
    /* tx packets 128-255 bytes */
    _ret |= _hses10g_read_48bit_value(p_hses10g, HSES10G_STAT_TX_PACKET_128_255_BYTES_LSB,
                              &port_stats->tx_packets_128_255.value);
    /* tx packets 256-511 bytes */
    _ret |= _hses10g_read_48bit_value(p_hses10g, HSES10G_STAT_TX_PACKET_256_511_BYTES_LSB,
                              &port_stats->tx_packets_256_511.value);
    /* tx packets 512-1023 bytes */
    _ret |= _hses10g_read_48bit_value(p_hses10g, HSES10G_STAT_TX_PACKET_512_1023_BYTES_LSB,
                              &port_stats->tx_packets_512_1023.value);
    /* tx packets 1024-max bytes */
    //TODO - ADD MISSING COUNTERS (> 1518 BYTES)
    _ret |= _hses10g_read_48bit_value(p_hses10g, HSES10G_STAT_TX_PACKET_1024_1518_BYTES_LSB,
                              &port_stats->tx_packets_1024_max.value);
    /* tx packets broadcast */
    _ret |= _hses10g_read_48bit_value(p_hses10g, HSES10G_STAT_TX_BROADCAST_LSB,
                              &port_stats->tx_bcast.value);
    /* tx packets multicast */
    _ret |= _hses10g_read_48bit_value(p_hses10g, HSES10G_STAT_TX_MULTICAST_LSB,
                              &port_stats->tx_mcast.value);
    /* tx packets oversize */
    //TODO - check oversize definition vs this counter
    _ret |= _hses10g_read_48bit_value(p_hses10g, HSES10G_STAT_RX_PACKET_LARGE_LSB,
                              &port_stats->tx_packets_oversize.value);
    /* tx packets underrun error */
    //TODO - check underrun definition vs this counter
    _ret |= _hses10g_read_48bit_value(p_hses10g, HSES10G_STAT_TX_PACKET_SMALL_LSB,
                              &port_stats->tx_packets_underrun_err.value);
    /* rx bytes */
    _ret |= _hses10g_read_48bit_value(p_hses10g, HSES10G_STAT_RX_TOTAL_BYTES_LSB,
                              &port_stats->rx_bytes.value);
    /* rx packets */
    _ret |= _hses10g_read_48bit_value(p_hses10g, HSES10G_STAT_RX_TOTAL_PACKETS_LSB,
                              &port_stats->rx_packets.value);
    /* rx packets 64 bytes */
    _ret |= _hses10g_read_48bit_value(p_hses10g, HSES10G_STAT_RX_PACKET_64_BYTES_LSB,
                              &port_stats->rx_packets_64.value);
    /* rx packets 65-127 bytes */
    _ret |= _hses10g_read_48bit_value(p_hses10g, HSES10G_STAT_RX_PACKET_65_127_BYTES_LSB,
                              &port_stats->rx_packets_65_127.value);
    /* rx packets 128-255 bytes */
    _ret |= _hses10g_read_48bit_value(p_hses10g, HSES10G_STAT_RX_PACKET_128_255_BYTES_LSB,
                              &port_stats->rx_packets_128_255.value);
    /* rx packets 256-511 bytes */
    _ret |= _hses10g_read_48bit_value(p_hses10g, HSES10G_STAT_RX_PACKET_256_511_BYTES_LSB,
                              &port_stats->rx_packets_256_511.value);
    /* rx packets 512-1023 bytes */
    _ret |= _hses10g_read_48bit_value(p_hses10g, HSES10G_STAT_RX_PACKET_512_1023_BYTES_LSB,
                              &port_stats->rx_packets_512_1023.value);
    /* rx packets 1024-max bytes */
    //TODO - ADD MISSING COUNTERS (> 1518 BYTES)
    _ret |= _hses10g_read_48bit_value(p_hses10g, HSES10G_STAT_RX_PACKET_1024_1518_BYTES_LSB,
                              &port_stats->rx_packets_1024_max.value);
    /* rx packets broadcast */
    _ret |= _hses10g_read_48bit_value(p_hses10g, HSES10G_STAT_RX_BROADCAST_LSB,
                              &port_stats->rx_bcast.value);
    /* rx packets multicast */
    _ret |= _hses10g_read_48bit_value(p_hses10g, HSES10G_STAT_RX_MULTICAST_LSB,
                              &port_stats->rx_mcast.value);
    /* rx packets oversize*/
    _ret |= _hses10g_read_48bit_value(p_hses10g, HSES10G_STAT_RX_OVERSIZE_LSB,
                              &port_stats->rx_packets_oversize.value);
    /* rx packets undersize*/
    _ret |= _hses10g_read_48bit_value(p_hses10g, HSES10G_STAT_RX_UNDERSIZE_LSB,
                              &port_stats->rx_packets_undersize.value);
    /* rx packets fragmented */
    _ret |= _hses10g_read_48bit_value(p_hses10g, HSES10G_STAT_RX_FRAGMENT_LSB,
                              &port_stats->rx_packets_frag.value);
    /* rx packets FCS error */
    _ret |= _hses10g_read_48bit_value(p_hses10g, HSES10G_STAT_RX_BAD_FCS_LSB,
                              &port_stats->rx_packets_fcs_err.value);
    if ( _ret ) {
        PDEBUG("_hses10g_read_48bit_value error\n");
        HSES10G_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}
static ccl_err_t _hses10g_reset_counters(void *priv)
{
    hses10g_ctx_t *p_hses10g;
 
    if ( !priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        HSES10G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    
    return CCL_OK;
}
#else
static ccl_err_t _hses10g_read_counters(hses10g_ctx_t *p_hses10g)
{
    b_u32 regval;
    b_u32 i;

    if ( !p_hses10g ) {
        PDEBUG("Error! Invalid input parameters\n");
        HSES10G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    
    /* Freeze counters */
    b_u32 offset = p_hses10g->brdspec.counter_ctl.ctl_offset;
    _ret = devspibus_read_buff(p_hses10g->brdspec.devname, 
                               offset,
                               0, 
                               (b_u8 *)&regval, 
                               sizeof(b_u32));
    PDEBUG("offset=0x%x, regval=0x%x\n",
           offset, regval);
    regval |= p_hses10g->brdspec.counter_ctl.ctl.freeze_counters_mask;
    PDEBUG("offset=0x%x, regval=0x%x\n",
           offset, regval);
    _ret |= devspibus_write_buff(p_hses10g->brdspec.devname, 
                                offset,
                                0, 
                                (b_u8 *)&regval, 
                                sizeof(b_u32));
    for (i = 0; i < p_hses10g->counters_num; i++) {
        offset = p_hses10g->brdspec.counter_ctl.counter[i].offset;
        PDEBUG("counter[%d] - offset=0x%x\n",
               i, offset);
        _ret |= devspibus_read_buff(p_hses10g->brdspec.devname, 
                                   offset,
                                   0, 
                                   (b_u8 *)&p_hses10g->counters[i].value, 
                                   sizeof(b_u32));
        PDEBUG("counter[%d] - offset=0x%x, value=%u\n",
               i, offset, p_hses10g->counters[i].value);
    }
    /* Unfreeze counters */
    offset = p_hses10g->brdspec.counter_ctl.ctl_offset;
    _ret |= devspibus_read_buff(p_hses10g->brdspec.devname, 
                               offset,
                               0, 
                               (b_u8 *)&regval, 
                               sizeof(b_u32));
    PDEBUG("offset=0x%x, regval=0x%x\n",
           offset, regval);
    regval &= ~p_hses10g->brdspec.counter_ctl.ctl.freeze_counters_mask;
    PDEBUG("offset=0x%x, regval=0x%x\n",
           offset, regval);
    _ret |= devspibus_write_buff(p_hses10g->brdspec.devname, 
                                offset,
                                0, 
                                (b_u8 *)&regval, 
                                sizeof(b_u32));
    if ( _ret ) {
        PDEBUG("Error! Failed to write over spi bus\n");
        HSES10G_ERROR_RETURN(_ret);
    }    

    return CCL_OK;
}

static ccl_err_t _hses10g_read_single_counter(void *priv, b_u32 idx, b_u64 *pvalue)
{
    hses10g_ctx_t *p_hses10g;
    b_u32         ifmib;

    if ( !priv || !pvalue) {
        PDEBUG("Error! Invalid input parameters\n");
        HSES10G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_hses10g = (hses10g_ctx_t *)priv;

    ifmib = _hses10g_mib_cntr_map[idx].ifmib;

    _ret = _hses10g_read_counters(p_hses10g);
    if ( _ret ) {
        PDEBUG("_hses10g_latch_statistics error\n");
        HSES10G_ERROR_RETURN(_ret);
    }

    if (ifmib == DEVMAN_IF_MIB_FRASENT) 
        *pvalue = ((b_u64)p_hses10g->counters[1].value << HSES10G_NUM_WORD_BITS) |
                  p_hses10g->counters[0].value;
    else if (ifmib == DEVMAN_IF_MIB_FRAREC)
        *pvalue = ((b_u64)p_hses10g->counters[3].value << HSES10G_NUM_WORD_BITS) |
                  p_hses10g->counters[2].value;
    else if (ifmib == DEVMAN_IF_MIB_CRCERR)
        *pvalue = ((b_u64)p_hses10g->counters[5].value << HSES10G_NUM_WORD_BITS) |
                  p_hses10g->counters[4].value;
    else {
        PDEBUG("invalid counter ifmib: %d, idx: %d\n", ifmib, idx);
        HSES10G_ERROR_RETURN(_ret);
    }

    PDEBUG("%d %d %d %d %d %d %llu %d\n",
           p_hses10g->counters[0].value,
           p_hses10g->counters[1].value,
           p_hses10g->counters[2].value,
           p_hses10g->counters[3].value,
           p_hses10g->counters[4].value,
           p_hses10g->counters[5].value,
           *pvalue, ifmib);

    return CCL_OK;
}

static ccl_err_t _hses10g_read_stat_counters(void *priv, b_u8 *buf)
{
    hses10g_ctx_t *p_hses10g;
    port_stats_t  *port_stats;

    if ( !priv || !buf) {
        PDEBUG("Error! Invalid input parameters\n");
        HSES10G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_hses10g = (hses10g_ctx_t *)priv;
    port_stats = (port_stats_t *)buf;

    _ret = _hses10g_read_counters(p_hses10g);
    if ( _ret ) {
        PDEBUG("_hses10g_latch_statistics error\n");
        HSES10G_ERROR_RETURN(_ret);
    }

    port_stats->tx_packets.value = 
                   ((b_u64)p_hses10g->counters[1].value << HSES10G_NUM_WORD_BITS) |
                    p_hses10g->counters[0].value;
    port_stats->rx_packets.value = 
                   ((b_u64)p_hses10g->counters[3].value << HSES10G_NUM_WORD_BITS) |
                    p_hses10g->counters[2].value;
    port_stats->rx_packets_fcs_err.value = 
                   ((b_u64)p_hses10g->counters[5].value << HSES10G_NUM_WORD_BITS) |
                    p_hses10g->counters[4].value;

    return CCL_OK;
}

static ccl_err_t _hses10g_reset_counters(void *priv)
{
    hses10g_ctx_t *p_hses10g;
    b_u32         regval;
    b_u32         i;

    if ( !priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        HSES10G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    
    p_hses10g = (hses10g_ctx_t *)priv;

    /* Reset counters */
    b_u32 offset = p_hses10g->brdspec.counter_ctl.ctl_offset;
    _ret = devspibus_read_buff(p_hses10g->brdspec.devname, 
                               offset,
                               0, 
                               (b_u8 *)&regval, 
                               sizeof(b_u32));
    PDEBUG("offset=0x%x, regval=0x%x\n",
           offset, regval);
    regval |= p_hses10g->brdspec.counter_ctl.ctl.tx_pkt_rst_mask |
              p_hses10g->brdspec.counter_ctl.ctl.rx_pkt_rst_mask |
              p_hses10g->brdspec.counter_ctl.ctl.rx_bad_pkt_rst_mask;
    PDEBUG("offset=0x%x, regval=0x%x\n",
           offset, regval);
    _ret |= devspibus_write_buff(p_hses10g->brdspec.devname, 
                                offset,
                                0, 
                                (b_u8 *)&regval, 
                                sizeof(b_u32));
    PDEBUG("offset=0x%x, regval=0x%x\n",
           offset, regval);
    regval &= ~(p_hses10g->brdspec.counter_ctl.ctl.tx_pkt_rst_mask |
              p_hses10g->brdspec.counter_ctl.ctl.rx_pkt_rst_mask |
              p_hses10g->brdspec.counter_ctl.ctl.rx_bad_pkt_rst_mask);
    PDEBUG("offset=0x%x, regval=0x%x\n",
           offset, regval);
    _ret |= devspibus_write_buff(p_hses10g->brdspec.devname, 
                                offset,
                                0, 
                                (b_u8 *)&regval, 
                                sizeof(b_u32));
    if ( _ret ) {
        PDEBUG("Error! Failed to write over spi bus\n");
        HSES10G_ERROR_RETURN(_ret);
    }    

    return CCL_OK;
}
#endif 


/**
 * @brief Report the speed (only 10G supported) from
 *      the Autonegotiation status register
 *
 * @param[in] p_hses10g is a pointer to hses10g_ctx_t instance to 
 *       be worked on
 *  
 * @return	reported speed
 */
static b_u16 _hses10g_get_autoneg_speed(hses10g_ctx_t *p_hses10g)
{
    b_u32 regval;

    if ( !p_hses10g ) {
        PDEBUG("Error! Invalid input parameters\n");
        HSES10G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    HSES10G_REG_READ(p_hses10g, HSES10G_STAT_AN_ABILITY, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));

    if (regval & HSES10G_ANA_10GKR_MASK)
        return HSES10G_SPEED_10_GBPS;
    else
        return 0;
}

/**
 * @brief Sets the speed (only 10G supported) in the Autonegotiation control register
 *
 * @param[in] p_hses10g is a pointer to hses10g_ctx_t instance to 
 *       be worked on
 *  
 * @return	CCL_OK on success, CCL_FAIL on failure
 */
static ccl_err_t _hses10g_set_autoneg_speed(hses10g_ctx_t *p_hses10g)
{

    b_u32 regval;

    if ( !p_hses10g ) {
        PDEBUG("Error! Invalid input parameters\n");
        HSES10G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* Set register and return */
    regval = HSES10G_ANA_10GKR_MASK;
    HSES10G_REG_WRITE(p_hses10g, HSES10G_CONFIGURATION_AN_ABILITY, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

static ccl_err_t _hses10g_configure_test(void *p_priv)
{
    hses10g_ctx_t *p_hses10g;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        HSES10G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_hses10g = (hses10g_ctx_t *)p_priv;

    return CCL_OK;
}

static ccl_err_t _hses10g_run_test(void *p_priv, 
                                   __attribute__((unused)) b_u8 *buf, 
                                   __attribute__((unused)) b_u32 size)
{
    hses10g_ctx_t *p_hses10g;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        HSES10G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_hses10g = (hses10g_ctx_t *)p_priv;

    return CCL_OK;
}

ccl_err_t hses10g_attr_read(void *p_priv, b_u32 attrid, 
                             b_u32 offset, b_u32 offset_sz, 
                             b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || offset > HSES10G_MAX_OFFSET || !p_value ) {
        PDEBUG("Error! Invalid input parameters\n");
        HSES10G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch (attrid) {
    case eHSES10G_GT_RESET_REG:
    case eHSES10G_RESET_REG:
    case eHSES10G_MODE_REG:
    case eHSES10G_CONFIGURATION_TX_REG1:
    case eHSES10G_CONFIGURATION_RX_REG1:
    case eHSES10G_CONFIGURATION_RX_MTU:
    case eHSES10G_CONFIGURATION_VL_LENGTH_REG:
    case eHSES10G_CONFIGURATION_REVISION_REG:
    case eHSES10G_CONFIGURATION_TX_TEST_PAT_SEED_A_LSB:
    case eHSES10G_CONFIGURATION_TX_TEST_PAT_SEED_B_LSB:
    case eHSES10G_CONFIGURATION_1588_REG:
    case eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_REG1:
    case eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_REFRESH_REG1:
    case eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_REFRESH_REG2:
    case eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_REFRESH_REG3:
    case eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_REFRESH_REG4:
    case eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_REFRESH_REG5:
    case eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_QUANTA_REG1:
    case eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_QUANTA_REG2:
    case eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_QUANTA_REG3:
    case eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_QUANTA_REG4:
    case eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_QUANTA_REG5:
    case eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_PPP_ETYPE_OP_REG:
    case eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_GPP_ETYPE_OP_REG:
    case eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_GPP_DA_REG_LSB:
    case eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_GPP_SA_REG_LSB:
    case eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_PPP_DA_REG_LSB:
    case eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_PPP_SA_REG_LSB:
    case eHSES10G_CONFIGURATION_RX_FLOW_CONTROL_REG1:
    case eHSES10G_CONFIGURATION_RX_FLOW_CONTROL_REG2:
    case eHSES10G_CONFIGURATION_RX_FLOW_CONTROL_PPP_ETYPE_OP_REG:
    case eHSES10G_CONFIGURATION_RX_FLOW_CONTROL_GPP_ETYPE_OP_REG:
    case eHSES10G_CONFIGURATION_RX_FLOW_CONTROL_GCP_PCP_TYPE_REG:
    case eHSES10G_CONFIGURATION_RX_FLOW_CONTROL_PCP_OP_REG:
    case eHSES10G_CONFIGURATION_RX_FLOW_CONTROL_GCP_OP_REG:
    case eHSES10G_CONFIGURATION_RX_FLOW_CONTROL_DA_REG1_LSB:
    case eHSES10G_CONFIGURATION_RX_FLOW_CONTROL_DA_REG2_LSB:
    case eHSES10G_CONFIGURATION_RX_FLOW_CONTROL_SA_REG1_LSB:
    case eHSES10G_CONFIGURATION_RSFEC_REG:
    case eHSES10G_CONFIGURATION_FEC_REG:
    case eHSES10G_CONFIGURATION_AN_CONTROL_REG1:
    case eHSES10G_CONFIGURATION_AN_CONTROL_REG2:
    case eHSES10G_CONFIGURATION_AN_ABILITY:
    case eHSES10G_CONFIGURATION_LT_CONTROL_REG1:
    case eHSES10G_CONFIGURATION_LT_TRAINED_REG:
    case eHSES10G_CONFIGURATION_LT_PRESET_REG:
    case eHSES10G_CONFIGURATION_LT_INIT_REG:
    case eHSES10G_CONFIGURATION_LT_SEED_REG0:
    case eHSES10G_CONFIGURATION_LT_COEFFICIENT_REG0:
    case eHSES10G_USER_REG_0:
    case eHSES10G_SWITCH_CORE_SPEED_REG:
    case eHSES10G_CONFIGURATION_1588_32BIT_REG:
    case eHSES10G_TX_CONFIGURATION_1588_REG:
    case eHSES10G_RX_CONFIGURATION_1588_REG:
    case eHSES10G_CONFIGURATION_TSN_REG:
    case eHSES10G_STAT_TX_STATUS_REG1:
    case eHSES10G_STAT_RX_STATUS_REG1:
    case eHSES10G_STAT_STATUS_REG1:
    case eHSES10G_STAT_RX_BLOCK_LOCK_REG:
    case eHSES10G_STAT_RX_RSFEC_STATUS_REG:
    case eHSES10G_STAT_RX_FEC_STATUS_REG:
    case eHSES10G_STAT_TX_RSFEC_STATUS_REG:
    case eHSES10G_STAT_TX_FLOW_CONTROL_REG1:
    case eHSES10G_STAT_RX_FLOW_CONTROL_REG1:
    case eHSES10G_STAT_AN_STATUS:
    case eHSES10G_STAT_AN_ABILITY:
    case eHSES10G_STAT_AN_LINK_CTL:
    case eHSES10G_STAT_LT_STATUS_REG1:
    case eHSES10G_STAT_LT_STATUS_REG2:
    case eHSES10G_STAT_LT_STATUS_REG3:
    case eHSES10G_STAT_LT_STATUS_REG4:
    case eHSES10G_STAT_LT_COEFFICIENT0_REG:
    case eHSES10G_STAT_RX_VALID_CTRL_CODE:
    case eHSES10G_STAT_CORE_SPEED_REG:
    case eHSES10G_STAT_TSN_REG:
        HSES10G_REG_READ(p_priv, offset, offset_sz, idx, p_value, size);
        break;
    case eHSES10G_STATUS_CYCLE_COUNT_LSB:
    case eHSES10G_STAT_RX_FRAMING_ERR_LSB:
    case eHSES10G_STAT_RX_BAD_CODE_LSB:
    case eHSES10G_STAT_RX_ERROR_LSB:
    case eHSES10G_STAT_RX_RSFEC_CORRECTED_CW_INC_LSB:
    case eHSES10G_STAT_RX_RSFEC_UNCORRECTED_CW_INC_LSB:
    case eHSES10G_STAT_RX_RSFEC_ERR_COUNT0_INC_LSB:
    case eHSES10G_STAT_TX_FRAME_ERROR_LSB:
    case eHSES10G_STAT_TX_TOTAL_PACKETS_LSB:
    case eHSES10G_STAT_TX_TOTAL_GOOD_PACKETS_LSB:
    case eHSES10G_STAT_TX_TOTAL_BYTES_LSB:
    case eHSES10G_STAT_TX_TOTAL_GOOD_BYTES_LSB:
    case eHSES10G_STAT_TX_PACKET_64_BYTES_LSB:
    case eHSES10G_STAT_TX_PACKET_65_127_BYTES_LSB:
    case eHSES10G_STAT_TX_PACKET_128_255_BYTES_LSB:
    case eHSES10G_STAT_TX_PACKET_256_511_BYTES_LSB:
    case eHSES10G_STAT_TX_PACKET_512_1023_BYTES_LSB:
    case eHSES10G_STAT_TX_PACKET_1024_1518_BYTES_LSB:
    case eHSES10G_STAT_TX_PACKET_1519_1522_BYTES_LSB:
    case eHSES10G_STAT_TX_PACKET_1523_1548_BYTES_LSB:
    case eHSES10G_STAT_TX_PACKET_1549_2047_BYTES_LSB:
    case eHSES10G_STAT_TX_PACKET_2048_4095_BYTES_LSB:
    case eHSES10G_STAT_TX_PACKET_4096_8191_BYTES_LSB:
    case eHSES10G_STAT_TX_PACKET_8192_9215_BYTES_LSB:
    case eHSES10G_STAT_TX_PACKET_LARGE_LSB:
    case eHSES10G_STAT_TX_PACKET_SMALL_LSB:
    case eHSES10G_STAT_TX_BAD_FCS_LSB:
    case eHSES10G_STAT_TX_UNICAST_LSB:
    case eHSES10G_STAT_TX_MULTICAST_LSB:
    case eHSES10G_STAT_TX_BROADCAST_LSB:
    case eHSES10G_STAT_TX_VLAN_LSB:
    case eHSES10G_STAT_TX_PAUSE_LSB:
    case eHSES10G_STAT_TX_USER_PAUSE_LSB:
    case eHSES10G_STAT_RX_TOTAL_PACKETS_LSB:
    case eHSES10G_STAT_RX_TOTAL_GOOD_PACKETS_LSB:
    case eHSES10G_STAT_RX_TOTAL_BYTES_LSB:
    case eHSES10G_STAT_RX_TOTAL_GOOD_BYTES_LSB:
    case eHSES10G_STAT_RX_PACKET_64_BYTES_LSB:
    case eHSES10G_STAT_RX_PACKET_65_127_BYTES_LSB:
    case eHSES10G_STAT_RX_PACKET_128_255_BYTES_LSB:
    case eHSES10G_STAT_RX_PACKET_256_511_BYTES_LSB:
    case eHSES10G_STAT_RX_PACKET_512_1023_BYTES_LSB:
    case eHSES10G_STAT_RX_PACKET_1024_1518_BYTES_LSB:
    case eHSES10G_STAT_RX_PACKET_1519_1522_BYTES_LSB:
    case eHSES10G_STAT_RX_PACKET_1523_1548_BYTES_LSB:
    case eHSES10G_STAT_RX_PACKET_1549_2047_BYTES_LSB:
    case eHSES10G_STAT_RX_PACKET_2048_4095_BYTES_LSB:
    case eHSES10G_STAT_RX_PACKET_4096_8191_BYTES_LSB:
    case eHSES10G_STAT_RX_PACKET_8192_9215_BYTES_LSB:
    case eHSES10G_STAT_RX_PACKET_LARGE_LSB:
    case eHSES10G_STAT_RX_PACKET_SMALL_LSB:
    case eHSES10G_STAT_RX_UNDERSIZE_LSB:
    case eHSES10G_STAT_RX_FRAGMENT_LSB:
    case eHSES10G_STAT_RX_OVERSIZE_LSB:
    case eHSES10G_STAT_RX_TOOLONG_LSB:
    case eHSES10G_STAT_RX_JABBER_LSB:
    case eHSES10G_STAT_RX_BAD_FCS_LSB:
    case eHSES10G_STAT_RX_PACKET_BAD_FCS_LSB:
    case eHSES10G_STAT_RX_STOMPED_FCS_LSB:
    case eHSES10G_STAT_RX_UNICAST_LSB:
    case eHSES10G_STAT_RX_MULTICAST_LSB:
    case eHSES10G_STAT_RX_BROADCAST_LSB:
    case eHSES10G_STAT_RX_VLAN_LSB:
    case eHSES10G_STAT_RX_PAUSE_LSB:
    case eHSES10G_STAT_RX_USER_PAUSE_LSB:
    case eHSES10G_STAT_RX_INRANGEERR_LSB:
    case eHSES10G_STAT_RX_TRUNCATED_LSB:
    case eHSES10G_STAT_RX_TEST_PATTERN_MISMATCH_LSB:
    case eHSES10G_STAT_FEC_INC_CORRECT_COUNT_LSB:
    case eHSES10G_STAT_FEC_INC_CANT_CORRECT_COUNT_LSB:
    case eHSES10G_STAT_TX_MM_STATUS_LSB:
    case eHSES10G_STAT_TX_MM_FRAGMENT_LSB:
    case eHSES10G_STAT_TX_MM_HOLD_LSB:
    case eHSES10G_STAT_RX_MM_ASSEMBLY_ERROR_LSB:
    case eHSES10G_STAT_RX_MM_FRAME_SMD_ERROR_LSB:
    case eHSES10G_STAT_RX_MM_FRAME_ASSEMBLY_OK_LSB:
    case eHSES10G_STAT_RX_MM_FRAGMENT_LSB:
    case eHSES10G_STAT_SINGLE_COUNTER:
        _ret = _hses10g_read_single_counter(p_priv, idx, (b_u64*)p_value);
        break;
    case eHSES10G_IF_PROPERTY:
        _ret = _hses10g_if_property_get(p_priv, idx, p_value, size);
        break;
    case eHSES10G_STAT_COUNTERS:
        _ret = _hses10g_read_stat_counters(p_priv, p_value);
        break;
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK) {
        PDEBUG("Failure in attribute id: %d\n", attrid);
    }

    return _ret;               
}

ccl_err_t hses10g_attr_write(void *p_priv, b_u32 attrid, 
                             b_u32 offset, b_u32 offset_sz, 
                             b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || offset > HSES10G_MAX_OFFSET || !p_value  ) {
        PDEBUG("Error! Invalid input parameters\n");
        HSES10G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch (attrid) {
    case eHSES10G_GT_RESET_REG:
    case eHSES10G_RESET_REG:
    case eHSES10G_MODE_REG:
    case eHSES10G_CONFIGURATION_TX_REG1:
    case eHSES10G_CONFIGURATION_RX_REG1:
    case eHSES10G_CONFIGURATION_RX_MTU:
    case eHSES10G_CONFIGURATION_VL_LENGTH_REG:
    case eHSES10G_TICK_REG:
    case eHSES10G_CONFIGURATION_TX_TEST_PAT_SEED_A_LSB:
    case eHSES10G_CONFIGURATION_TX_TEST_PAT_SEED_B_LSB:
    case eHSES10G_CONFIGURATION_1588_REG:
    case eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_REG1:
    case eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_REFRESH_REG1:
    case eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_REFRESH_REG2:
    case eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_REFRESH_REG3:
    case eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_REFRESH_REG4:
    case eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_REFRESH_REG5:
    case eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_QUANTA_REG1:
    case eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_QUANTA_REG2:
    case eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_QUANTA_REG3:
    case eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_QUANTA_REG4:
    case eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_QUANTA_REG5:
    case eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_PPP_ETYPE_OP_REG:
    case eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_GPP_ETYPE_OP_REG:
    case eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_GPP_DA_REG_LSB:
    case eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_GPP_SA_REG_LSB:
    case eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_PPP_DA_REG_LSB:
    case eHSES10G_CONFIGURATION_TX_FLOW_CONTROL_PPP_SA_REG_LSB:
    case eHSES10G_CONFIGURATION_RX_FLOW_CONTROL_REG1:
    case eHSES10G_CONFIGURATION_RX_FLOW_CONTROL_REG2:
    case eHSES10G_CONFIGURATION_RX_FLOW_CONTROL_PPP_ETYPE_OP_REG:
    case eHSES10G_CONFIGURATION_RX_FLOW_CONTROL_GPP_ETYPE_OP_REG:
    case eHSES10G_CONFIGURATION_RX_FLOW_CONTROL_GCP_PCP_TYPE_REG:
    case eHSES10G_CONFIGURATION_RX_FLOW_CONTROL_PCP_OP_REG:
    case eHSES10G_CONFIGURATION_RX_FLOW_CONTROL_GCP_OP_REG:
    case eHSES10G_CONFIGURATION_RX_FLOW_CONTROL_DA_REG1_LSB:
    case eHSES10G_CONFIGURATION_RX_FLOW_CONTROL_DA_REG2_LSB:
    case eHSES10G_CONFIGURATION_RX_FLOW_CONTROL_SA_REG1_LSB:
    case eHSES10G_CONFIGURATION_RSFEC_REG:
    case eHSES10G_CONFIGURATION_FEC_REG:
    case eHSES10G_CONFIGURATION_AN_CONTROL_REG1:
    case eHSES10G_CONFIGURATION_AN_CONTROL_REG2:
    case eHSES10G_CONFIGURATION_AN_ABILITY:
    case eHSES10G_CONFIGURATION_LT_CONTROL_REG1:
    case eHSES10G_CONFIGURATION_LT_TRAINED_REG:
    case eHSES10G_CONFIGURATION_LT_PRESET_REG:
    case eHSES10G_CONFIGURATION_LT_INIT_REG:
    case eHSES10G_CONFIGURATION_LT_SEED_REG0:
    case eHSES10G_CONFIGURATION_LT_COEFFICIENT_REG0:
    case eHSES10G_USER_REG_0:
    case eHSES10G_SWITCH_CORE_SPEED_REG:
    case eHSES10G_CONFIGURATION_1588_32BIT_REG:
    case eHSES10G_TX_CONFIGURATION_1588_REG:
    case eHSES10G_RX_CONFIGURATION_1588_REG:
    case eHSES10G_CONFIGURATION_TSN_REG:
        HSES10G_REG_WRITE(p_priv, offset, offset_sz, idx, p_value, size);
        break;
    case eHSES10G_INIT:
        _ret = _hses10g_init(p_priv);
        break;
    case eHSES10G_CONFIGURE_TEST:
        _ret = _hses10g_configure_test(p_priv);
        break;
    case eHSES10G_RUN_TEST:
        _ret = _hses10g_run_test(p_priv, p_value, size);
        break;
    case eHSES10G_COUNTERS_RESET:
        _ret = _hses10g_reset_counters(p_priv);
        break;
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK) {
        PDEBUG("Failure in attribute id: %d\n", attrid);
    }

    return _ret;               
}

static ccl_err_t _fhses10g_cmd_start(devo_t devo, 
                                     __attribute__((unused)) device_cmd_parm_t parms[], 
                                     __attribute__((unused)) b_u16 parms_num)
{
    hses10g_ctx_t *p_hses10g = devman_device_private(devo);

    /* start the device */
    _ret = _hses10g_start(p_hses10g);
    if ( _ret ) {
        PDEBUG("_hses10g_start error\n");
        HSES10G_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

static ccl_err_t _fhses10g_cmd_stop(devo_t devo, 
                                    __attribute__((unused)) device_cmd_parm_t parms[], 
                                    __attribute__((unused)) b_u16 parms_num)
{
    hses10g_ctx_t *p_hses10g = devman_device_private(devo);

    /* start the device */
    _ret = _hses10g_stop(p_hses10g);
    if ( _ret ) {
        PDEBUG("_hses10g_stop error\n");
        HSES10G_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

static ccl_err_t _fhses10g_cmd_init(devo_t devo, 
                                    __attribute__((unused)) device_cmd_parm_t parms[], 
                                    __attribute__((unused)) b_u16 parms_num)
{
    void *p_priv = devman_device_private(devo);

    /* start the device */
    _ret = _hses10g_init(p_priv);
    if ( _ret ) {
        PDEBUG("_hses10g_init error\n");
        HSES10G_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

static ccl_err_t _fhses10g_cmd_show_counters(devo_t devo, 
                                           __attribute__((unused)) device_cmd_parm_t parms[], 
                                           __attribute__((unused)) b_u16 parms_num)
{
    hses10g_ctx_t *p_hses10g = devman_device_private(devo);
    char        cname[100] = {0};
    b_u32       offset = 40;
    b_u32       clen;
    b_u32       i;

    /* init the device */
    _ret = _hses10g_read_counters(p_hses10g);
    if ( _ret ) {
        PDEBUG("_hses10g_read_counters\n");
        HSES10G_ERROR_RETURN(_ret);
    }

    for (i = 0; i < p_hses10g->counters_num; i++) {
        snprintf(cname, sizeof(cname), "%s:", p_hses10g->counters[i].name);
        clen = strnlen(cname, sizeof(cname));
        if(offset <= clen) 
            offset = clen + 1;
        snprintf(cname+clen, sizeof(cname), "%*s", 
                 offset-clen, " ");
        PRINTL("%s%u\n", 
               cname,
               p_hses10g->counters[i].value);
        p_hses10g->counters[i].value_prev = p_hses10g->counters[i].value;
    }

    return CCL_OK;
}

static ccl_err_t _fhses10g_cmd_show_counters_changed(devo_t devo, 
                                                   __attribute__((unused)) device_cmd_parm_t parms[], 
                                                   __attribute__((unused)) b_u16 parms_num)
{
    hses10g_ctx_t *p_hses10g = devman_device_private(devo);
    char        cname[100] = {0};
    b_u32       offset = 40;
    b_u32       clen;
    b_u32       i;
    b_u32       value;

    /* init the device */
    _ret = _hses10g_read_counters(p_hses10g);
    if ( _ret ) {
        PDEBUG("_hses10g_read_counters\n");
        HSES10G_ERROR_RETURN(_ret);
    }

    for (i = 0; i < p_hses10g->counters_num; i++) {
        if (p_hses10g->counters[i].value != p_hses10g->counters[i].value_prev) {
            value = p_hses10g->counters[i].value - p_hses10g->counters[i].value_prev;
            if (p_hses10g->counters[i].value_prev > p_hses10g->counters[i].value) {
                value = (p_hses10g->counters[i].value_prev - 
                         p_hses10g->counters[i].value + 
                         MAX_UINT32);
            }
            snprintf(cname, sizeof(cname), "%s:", p_hses10g->counters[i].name);
            clen = strnlen(cname, sizeof(cname));
            if(offset <= clen) 
                offset = clen + 1;
            snprintf(cname+clen, sizeof(cname), "%*s", 
                     offset-clen, " ");
            PRINTL("%s%u\n", 
                   cname,
                   value);
            p_hses10g->counters[i].value_prev = p_hses10g->counters[i].value;
        }
    }

    return CCL_OK;
}

static ccl_err_t _fhses10g_cmd_reset_counters(devo_t devo, 
                                              __attribute__((unused)) device_cmd_parm_t parms[], 
                                              __attribute__((unused)) b_u16 parms_num)
{
    void *p_priv = devman_device_private(devo);

    /* start the device */
    _ret = _hses10g_reset_counters(p_priv);
    if ( _ret ) {
        PDEBUG("_hses10g_reset_counters error\n");
        HSES10G_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

/* device commands */
static device_cmd_t _cmds[] = 
{
    { 
        .name = "start", .f_cmd = _fhses10g_cmd_start, .parms_num = 0,
        .help = "Start HSES10G device" 
    },
    { 
        .name = "init", .f_cmd = _fhses10g_cmd_init, .parms_num = 0,
        .help = "Init HSES10G device" 
    },
    { 
        .name = "stop", .f_cmd = _fhses10g_cmd_stop, .parms_num = 0,
        .help = "Stop HSES10G device" 
    },
    { 
        .name = "counters", .f_cmd = _fhses10g_cmd_show_counters, .parms_num = 0,
        .help = "Show HSES10G counters" 
    },
    { 
        .name = "counters_changed", .f_cmd = _fhses10g_cmd_show_counters_changed, .parms_num = 0,
        .help = "Show HSES10G changed counters" 
    },
    { 
        .name = "counters_reset", .f_cmd = _fhses10g_cmd_reset_counters, .parms_num = 0,
        .help = "Reset HSES10G counters" 
    },    
};

/* Device instance hw-specific initializations 
 */
static ccl_err_t _fhses10g_add(void *devo, void *brdspec) {
    hses10g_ctx_t       *p_hses10g;
    eth_brdspec_t       *p_brdspec;
    b_u32               i;
    
    if ( !devo || !brdspec ) {
        PDEBUG("Error! Invalid input parameters\n");
        HSES10G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_hses10g = devman_device_private(devo);
    if ( !p_hses10g ) {
        PDEBUG("NULL context area\n");
        HSES10G_ERROR_RETURN(CCL_FAIL);
    }    
    p_brdspec = (eth_brdspec_t *)brdspec;
    /* back reference the device manager object handle */    
    p_hses10g->devo = devo;    
    /* save the board specific info */
    memcpy(&p_hses10g->brdspec, p_brdspec, sizeof(eth_brdspec_t));
    p_hses10g->counters_num = sizeof(_hses10g_counters)/sizeof(_hses10g_counters[0]);
    p_hses10g->counters = calloc(p_hses10g->counters_num, sizeof(eth_counter_t));
    if (!p_hses10g->counters) {
        PDEBUG("Memory allocation failed\n");
        HSES10G_ERROR_RETURN(CCL_NO_MEM_ERR);
    }
    for (i = 0; i < p_hses10g->counters_num; i++) 
        memcpy(&p_hses10g->counters[i], &_hses10g_counters[i], sizeof(eth_counter_t));
#if 0
    /* start the device */
    _ret = _hses10g_start(p_hses10g);
    if ( _ret ) {
        PDEBUG("_hses10g_start error\n");
        HSES10G_ERROR_RETURN(CCL_FAIL);
    }    
#endif
    return CCL_OK;
}

/* Device instance hw-specific deinitializations 
 */
static ccl_err_t _fhses10g_cut(void *devo)
{
    hses10g_ctx_t  *p_hses10g;
    if ( !devo ) {
        PDEBUG("Error! Invalid input parameters\n");
        HSES10G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_hses10g = devman_device_private(devo);
    if ( !p_hses10g ) {
        PDEBUG("NULL context area\n");
        HSES10G_ERROR_RETURN(CCL_FAIL);
    } 
      
    /* stop the device */  
    _ret = _hses10g_stop(p_hses10g);
    if ( _ret ) {
        PDEBUG("_hses10g_stop error\n");
        HSES10G_ERROR_RETURN(CCL_FAIL);
    }    

    return CCL_OK;
}

/* device type description structure */
static device_driver_t _hses10g_driver = {
    .name = "hses10g",
    .f_add = _fhses10g_add,
    .f_cut = _fhses10g_cut,
    .attrs = _attrs,
    .attrs_num = sizeof(_attrs)/sizeof(_attrs[0]),
    .cmds = _cmds,
    .cmds_num = sizeof(_cmds)/sizeof(_cmds[0]),
    .priv_size = sizeof(hses10g_ctx_t)
}; 

/* Initialize HSES10G device type
 */
ccl_err_t devhses10g_init(void)
{
    /* register device type */
    _ret = devman_driver_register(&_hses10g_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to register driver\n");
        HSES10G_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}

/* Deinitialize HSES10G device type
 */
ccl_err_t devhses10g_fini(void)
{
    /* unregister device type */
    _ret = devman_driver_unregister(&_hses10g_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to unregister driver\n");
        HSES10G_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}
