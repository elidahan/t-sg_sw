/********************************************************************************/
/**
 * @file axieth1g.c
 * @brief AXI 1G/2.5G Ethernet Subsystem device manager driver
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/

#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <semaphore.h>
#include <linux/limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "axieth1g.h"


//#define AXIETH1G_DEBUG
/* printing/error-returning macros */
#ifdef AXIETH1G_DEBUG
 #define PDEBUG(fmt, args...) \
         ccl_syslog_err("AXIETH1G_DEBUG: %s(): " fmt, __FUNCTION__, ## args)
#else
 #define PDEBUG(fmt, args...)
#endif
#define AXIETH1G_ERROR_RETURN(err) do { \
   ccl_syslog_err("Error! %s[%d], error=%i\n", __FUNCTION__, __LINE__, (err)); \
   return err; \
} while (0)
#define AXIETH1G_CHECK_ERROR_RETURN(err) do { \
    if (err != CCL_OK) { \
        ccl_syslog_err("Error! %s[%d], error=%i\n", __FUNCTION__, __LINE__, (err)); \
        return err; \
    } \
} while (0)

#define AXIETH1G_REG_READ(context, offset, offset_sz, idx, pvalue, size) do { \
    _ret = _axieth1g_reg_read((context), \
                              (offset), \
                              (offset_sz), (idx), \
                              (pvalue), (size)); \
    if ( _ret ) { \
        PDEBUG("_axieth1g_reg_read error\n"); \
        AXIETH1G_ERROR_RETURN(_ret); \
    } \
} while (0)

#define AXIETH1G_REG_WRITE(context, offset, offset_sz, idx, pvalue, size) do { \
    _ret = _axieth1g_reg_write((context), \
                               (offset), \
                               (offset_sz), (idx), \
                               (pvalue), (size)); \
    if ( _ret ) { \
        PDEBUG("_axieth1g_reg_write error\n"); \
        AXIETH1G_ERROR_RETURN(_ret); \
    } \
} while (0)

/* AXIETH1G registers offsets */
#define AXIETH1G_RAF_OFFSET		0x00000000 /**< Reset and Address filter */
#define AXIETH1G_TPF_OFFSET		0x00000004 /**< Tx Pause Frame */
#define AXIETH1G_IFGP_OFFSET	0x00000008 /**< Tx Inter-frame gap adjustment*/
#define AXIETH1G_IS_OFFSET		0x0000000C /**< Interrupt status */
#define AXIETH1G_IP_OFFSET		0x00000010 /**< Interrupt pending */
#define AXIETH1G_IE_OFFSET		0x00000014 /**< Interrupt enable */
#define AXIETH1G_TTAG_OFFSET	0x00000018 /**< Tx VLAN TAG */
#define AXIETH1G_RTAG_OFFSET	0x0000001C /**< Rx VLAN TAG */
#define AXIETH1G_UAWL_OFFSET	0x00000020 /**< Unicast address word lower */
#define AXIETH1G_UAWU_OFFSET	0x00000024 /**< Unicast address word upper */
#define AXIETH1G_TPID0_OFFSET	0x00000028 /**< VLAN TPID0 register */
#define AXIETH1G_TPID1_OFFSET	0x0000002C /**< VLAN TPID1 register */

/*
 * Statistics Counter registers are from offset 0x200 to 0x3FF
 * They are defined from offset 0x200 to 0x34C in this device.
 * The offsets from 0x350 to 0x3FF are reserved.
 * The counters are 64 bit.
 * The Least Significant Word (LSW) are stored in one 32 bit register and
 * the Most Significant Word (MSW) are stored in one 32 bit register
 */
/* Start of Statistics Counter registers Definitions */
#define AXIETH1G_RXBL_OFFSET		0x00000200 /**< Received Bytes, LSW */
#define AXIETH1G_RXBU_OFFSET		0x00000204 /**< Received Bytes, MSW */
#define AXIETH1G_TXBL_OFFSET		0x00000208 /**< Transmitted Bytes, LSW */
#define AXIETH1G_TXBU_OFFSET		0x0000020C /**< Transmitted Bytes, MSW */
#define AXIETH1G_RXUNDRL_OFFSET	    0x00000210 /**< Count of undersize (less than
					                             *  64 bytes) frames received,
					                             *  LSW
					                             */
#define AXIETH1G_RXUNDRU_OFFSET	    0x00000214 /**< Count of undersize (less than
					                             *  64 bytes) frames received,
					                             *  MSW
					                             */
#define AXIETH1G_RXFRAGL_OFFSET	    0x00000218 /**< Count of undersized (less
					                             *  than 64 bytes) and bad FCS
					                             *  frames received, LSW
					                             */
#define AXIETH1G_RXFRAGU_OFFSET	    0x0000021C /**< Count of undersized (less
					                             *  than 64 bytes) and bad FCS
					                             *  frames received, MSW
                    					         */ 
#define AXIETH1G_RX64BL_OFFSET	    0x00000220 /**< Count of 64 bytes frames
					                             *  received, LSW
					                             */
#define AXIETH1G_RX64BU_OFFSET  	0x00000224 /**< Count of 64 bytes frames
					                             *  received, MSW
					                             */
#define AXIETH1G_RX65B127L_OFFSET	0x00000228 /**< Count of 65-127 bytes
					                             *  Frames received, LSW
					                             */
#define AXIETH1G_RX65B127U_OFFSET	0x0000022C /**< Count of 65-127 bytes
					                             *  Frames received, MSW
					                             */
#define AXIETH1G_RX128B255L_OFFSET	0x00000230 /**< Count of 128-255 bytes
					                             *  Frames received, LSW
					                             */
#define AXIETH1G_RX128B255U_OFFSET	0x00000234 /**< Count of 128-255 bytes
					                             *  frames received, MSW
					                             */
#define AXIETH1G_RX256B511L_OFFSET	0x00000238 /**< Count of 256-511 bytes
					                             *  Frames received, LSW
					                             */
#define AXIETH1G_RX256B511U_OFFSET	0x0000023C /**< Count of 256-511 bytes
					                             *  frames received, MSW
					                             */
#define AXIETH1G_RX512B1023L_OFFSET	0x00000240 /**< Count of 512-1023 bytes
					                             *  frames received, LSW
					                             */
#define AXIETH1G_RX512B1023U_OFFSET	0x00000244 /**< Count of 512-1023 bytes
					                             *  frames received, MSW
					                             */
#define AXIETH1G_RX1024BL_OFFSET	0x00000248 /**< Count of 1024-MAX bytes
					                             *  frames received, LSW
					                             */
#define AXIETH1G_RX1024BU_OFFSET	0x0000024C /**< Count of 1024-MAX bytes
					                             *  frames received, MSW
					                             */
#define AXIETH1G_RXOVRL_OFFSET	    0x00000250 /**< Count of oversize frames
					                             *  received, LSW
					                             */
#define AXIETH1G_RXOVRU_OFFSET	    0x00000254 /**< Count of oversize frames
					                             *  received, MSW
					                             */
#define AXIETH1G_TX64BL_OFFSET	    0x00000258 /**< Count of 64 bytes frames
					                             *  transmitted, LSW
					                             */
#define AXIETH1G_TX64BU_OFFSET	    0x0000025C /**< Count of 64 bytes frames
					                             *  transmitted, MSW
					                             */
#define AXIETH1G_TX65B127L_OFFSET	0x00000260 /**< Count of 65-127 bytes
					                             *  frames transmitted, LSW
					                             */
#define AXIETH1G_TX65B127U_OFFSET	0x00000264 /**< Count of 65-127 bytes
					                             *  frames transmitted, MSW
					                             */
#define AXIETH1G_TX128B255L_OFFSET	0x00000268 /**< Count of 128-255 bytes
					                             *  frames transmitted, LSW
					                             */
#define AXIETH1G_TX128B255U_OFFSET	0x0000026C /**< Count of 128-255 bytes
					                             *  frames transmitted, MSW
					                             */
#define AXIETH1G_TX256B511L_OFFSET	0x00000270 /**< Count of 256-511 bytes
					                             *  frames transmitted, LSW
					                             */
#define AXIETH1G_TX256B511U_OFFSET	0x00000274 /**< Count of 256-511 bytes
					                             *  frames transmitted, MSW
					                             */
#define AXIETH1G_TX512B1023L_OFFSET	0x00000278 /**< Count of 512-1023 bytes
					                             *  frames transmitted, LSW
					                             */
#define AXIETH1G_TX512B1023U_OFFSET	0x0000027C /**< Count of 512-1023 bytes
					                             *  frames transmitted, MSW
					                             */
#define AXIETH1G_TX1024L_OFFSET	    0x00000280 /**< Count of 1024-MAX bytes
					                             *  frames transmitted, LSW
					                             */
#define AXIETH1G_TX1024U_OFFSET	    0x00000284 /**< Count of 1024-MAX bytes
					                             *  frames transmitted, MSW
					                             */
#define AXIETH1G_TXOVRL_OFFSET	    0x00000288 /**< Count of oversize frames
					                             *  transmitted, LSW
					                             */
#define AXIETH1G_TXOVRU_OFFSET	    0x0000028C /**< Count of oversize frames
					                             *  transmitted, MSW
					                             */
#define AXIETH1G_RXFL_OFFSET		0x00000290 /**< Count of frames received OK,
					                             *  LSW
					                             */
#define AXIETH1G_RXFU_OFFSET		0x00000294 /**< Count of frames received OK,
					                             *  MSW
					                             */
#define AXIETH1G_RXFCSERL_OFFSET	0x00000298 /**< Count of frames received with
					                             *  FCS error and at least 64
					                             *  bytes, LSW
					                             */
#define AXIETH1G_RXFCSERU_OFFSET	0x0000029C /**< Count of frames received with
					                             *  FCS error and at least 64
					                             *  bytes,MSW
					                             */
#define AXIETH1G_RXBCSTFL_OFFSET	0x000002A0 /**< Count of broadcast frames
					                             *  received, LSW
					                             */
#define AXIETH1G_RXBCSTFU_OFFSET	0x000002A4 /**< Count of broadcast frames
					                             *  received, MSW
					                             */
#define AXIETH1G_RXMCSTFL_OFFSET	0x000002A8 /**< Count of multicast frames
					                             *  received, LSW
					                             */
#define AXIETH1G_RXMCSTFU_OFFSET	0x000002AC /**< Count of multicast frames
					                             *  received, MSW
					                             */
#define AXIETH1G_RXCTRFL_OFFSET	    0x000002B0 /**< Count of control frames
					                             *  received, LSW
					                             */
#define AXIETH1G_RXCTRFU_OFFSET	    0x000002B4 /**< Count of control frames
					                             *  received, MSW
					                             */
#define AXIETH1G_RXLTERL_OFFSET	    0x000002B8 /**< Count of frames received
					                             *  with length error, LSW
					                             */
#define AXIETH1G_RXLTERU_OFFSET	    0x000002BC /**< Count of frames received
					                             *  with length error, MSW
					                             */
#define AXIETH1G_RXVLANFL_OFFSET	0x000002C0 /**< Count of VLAN tagged
					                             *  frames received, LSW
					                             */
#define AXIETH1G_RXVLANFU_OFFSET	0x000002C4 /**< Count of VLAN tagged frames
					                             *  received, MSW
					                             */
#define AXIETH1G_RXPFL_OFFSET	    0x000002C8 /**< Count of pause frames received,
					                             *  LSW
					                             */
#define AXIETH1G_RXPFU_OFFSET	    0x000002CC /**< Count of pause frames received,
					                             *  MSW
					                             */
#define AXIETH1G_RXUOPFL_OFFSET	    0x000002D0 /**< Count of control frames
					                             *  received with unsupported
					                             *  opcode, LSW
					                             */
#define AXIETH1G_RXUOPFU_OFFSET	    0x000002D4 /**< Count of control frames
					                             *  received with unsupported
					                             *  opcode, MSW
					                             */
#define AXIETH1G_TXFL_OFFSET		0x000002D8 /**< Count of frames transmitted OK,
					                             *  LSW
					                             */
#define AXIETH1G_TXFU_OFFSET		0x000002DC /**< Count of frames transmitted OK,
					                             *  MSW
					                             */
#define AXIETH1G_TXBCSTFL_OFFSET	0x000002E0 /**< Count of broadcast frames
					                             *  transmitted OK, LSW
					                             */
#define AXIETH1G_TXBCSTFU_OFFSET	0x000002E4 /**< Count of broadcast frames
					                             *  transmitted, MSW
					                             */
#define AXIETH1G_TXMCSTFL_OFFSET	0x000002E8 /**< Count of multicast frames
					                             *  transmitted, LSW
					                             */
#define AXIETH1G_TXMCSTFU_OFFSET	0x000002EC /**< Count of multicast frames
					                             *  transmitted, MSW
					                             */
#define AXIETH1G_TXUNDRERL_OFFSET	0x000002F0 /**< Count of frames transmitted
					                             *  underrun error, LSW
					                             */
#define AXIETH1G_TXUNDRERU_OFFSET	0x000002F4 /**< Count of frames transmitted
					                             *  underrun error, MSW
					                             */
#define AXIETH1G_TXCTRFL_OFFSET 	0x000002F8 /**< Count of control frames
					                             *  transmitted, LSW
					                             */
#define AXIETH1G_TXCTRFU_OFFSET 	0x000002FC /**< Count of control frames,
					                             *  transmitted, MSW
					                             */
#define AXIETH1G_TXVLANFL_OFFSET	0x00000300 /**< Count of VLAN tagged frames
					                             *  transmitted, LSW
					                             */
#define AXIETH1G_TXVLANFU_OFFSET	0x00000304 /**< Count of VLAN tagged
    					                         *  frames transmitted, MSW
					                             */
#define AXIETH1G_TXPFL_OFFSET	    0x00000308 /**< Count of pause frames
					                             *  transmitted, LSW
					                             */
#define AXIETH1G_TXPFU_OFFSET	    0x0000030C /**< Count of pause frames
					                             *  transmitted, MSW
					                             */
#define AXIETH1G_TXSCL_OFFSET	    0x00000310 /**< Single Collision Frames
					                             *  Transmitted OK, LSW
					                             */
#define AXIETH1G_TXSCU_OFFSET	    0x00000314 /**< Single Collision Frames
					                             *  Transmitted OK, MSW
					                             */
#define AXIETH1G_TXMCL_OFFSET	    0x00000318 /**< Multiple Collision Frames
					                             *  Transmitted OK, LSW
					                             */
#define AXIETH1G_TXMCU_OFFSET	    0x0000031C /**< Multiple Collision Frames
					                             *  Transmitted OK, MSW
					                             */
#define AXIETH1G_TXDEFL_OFFSET	    0x00000320 /**< Deferred Tx Frames, LSW */
#define AXIETH1G_TXDEFU_OFFSET	    0x00000324 /**< Deferred Tx Frames, MSW */
#define AXIETH1G_TXLTCL_OFFSET	    0x00000328 /**< Frames transmitted with late
					                             *  Collisions, LSW
					                             */
#define AXIETH1G_TXLTCU_OFFSET	    0x0000032C /**< Frames transmitted with late
					                             *  Collisions, MSW
					                             */
#define AXIETH1G_TXAECL_OFFSET	    0x00000330 /**< Frames aborted with excessive
					                             *  Collisions, LSW
					                             */
#define AXIETH1G_TXAECU_OFFSET	    0x00000334 /**< Frames aborted with excessive
					                             *  Collisions, MSW
					                             */
#define AXIETH1G_TXEDEFL_OFFSET	    0x00000338 /**< Transmit Frames with excessive
					                             *  Defferal, LSW
					                             */
#define AXIETH1G_TXEDEFU_OFFSET	    0x0000033C /**< Transmit Frames with excessive
					                             *  Defferal, MSW
					                             */
#define AXIETH1G_RXAERL_OFFSET	    0x00000340 /**< Frames received with alignment
					                             *  errors, LSW
					                             */
#define AXIETH1G_RXAERU_OFFSET	    0x0000034C /**< Frames received with alignment
					                             *  errors, MSW
					                             */
/* End of Statistics Counter registers Offset definitions */

#define AXIETH1G_RCW0_OFFSET			   0x00000400 /**< Rx configuration Word 0 */
#define AXIETH1G_RCW1_OFFSET			   0x00000404 /**< Rx configuration Word 1 */
#define AXIETH1G_TC_OFFSET			       0x00000408 /**< Tx configuration */
#define AXIETH1G_FCC_OFFSET			       0x0000040C /**< Flow Control configuration */
#define AXIETH1G_EMMC_OFFSET               0x00000410 /**< EMAC mode configuration */
#define AXIETH1G_RXFC_OFFSET			   0x00000414 /**< Rx Max Frm config regvalister */
#define AXIETH1G_TXFC_OFFSET			   0x00000418 /**< Tx Max Frm config regvalister */
#define AXIETH1G_TX_TIMESTAMP_ADJ_OFFSET   0x0000041C /**< Transmitter time stamp
										                *  adjust control regvalister
										                */
#define AXIETH1G_PHYC_OFFSET		       0x00000420 /**< RGMII/SGMII configuration */

/* 0x00000424 to 0x000004F4 are reserved */

#define AXIETH1G_IDREG_OFFSET	           0x000004F8 /**< Identification regvalister */
#define AXIETH1G_ARREG_OFFSET	           0x000004FC /**< Ability regvalister */
#define AXIETH1G_MDIO_MC_OFFSET	           0x00000500 /**< MII Management config */
#define AXIETH1G_MDIO_MCR_OFFSET	       0x00000504 /**< MII Management Control */
#define AXIETH1G_MDIO_MWD_OFFSET	       0x00000508 /**< MII Management Write Data */
#define AXIETH1G_MDIO_MRD_OFFSET	       0x0000050C /**< MII Management Read Data */

/* 0x00000510 to 0x000005FC are reserved */

#define AXIETH1G_MDIO_MIS_OFFSET	       0x00000600 /**< MII Management Interrupt
					                                    *  Status
					                                    */
/* 0x00000604-0x0000061C are reserved */

#define AXIETH1G_MDIO_MIP_OFFSET	       0x00000610 /**< MII Management Interrupt
					                                    *  Pending register offse
					                                    */
/* 0x00000624-0x0000063C are reserved */

#define AXIETH1G_MDIO_MIE_OFFSET	       0x00000620 /**< MII Management Interrupt
					                                    *  Enable register offset
					                                    */
/* 0x00000644-0x0000065C are reserved */

#define AXIETH1G_MDIO_MIC_OFFSET	       0x00000630 /**< MII Management Interrupt
					                                    *  Clear register offset.
					                                    */

/* 0x00000664-0x000006FC are reserved */

#define AXIETH1G_UAW0_OFFSET		       0x00000700  /**< Unicast address word 0 */
#define AXIETH1G_UAW1_OFFSET		       0x00000704  /**< Unicast address word 1 */
#define AXIETH1G_FMFC_OFFSET		       0x00000708  /**< Frame Filter Control */
/* 0x0000070C is reserved */
#define AXIETH1G_FMFV_OFFSET		       0x00000710  /**< Frame Filter Value */
#define AXIETH1G_FMFMV_OFFSET		       0x00000750  /**< Frame Filter Mask Value */

/* 0x00000790-0x00003FFC are reserved */

/*
 * Transmit VLAN Table is from 0x00004000 to 0x00007FFC
 * This offset defines an offset to table that has provisioned transmit
 * VLAN data. The VLAN table will be used by hardware to provide
 * transmit VLAN tagging, stripping, and translation.
 */
#define AXIETH1G_TX_VLAN_DATA_OFFSET       0x00004000  /**< TX VLAN data table address */


/*
 * Receive VLAN Data Table is from 0x00008000 to 0x0000BFFC
 * This offset defines an offset to table that has provisioned receive
 * VLAN data. The VLAN table will be used by hardware to provide
 * receive VLAN tagging, stripping, and translation.
 */
#define AXIETH1G_RX_VLAN_DATA_OFFSET       0x00008000  /**< RX VLAN data table address */

/* 0x0000C000-0x0000FFFC are reserved */

/* 0x00010000-0x00013FFC are Ethenet AVB address offset */
#define AXIETH1G_ETH_AVB_OFFSET            0x00010000  /**< Ethernet AVB address */
/* 0x00014000-0x0001FFFC are reserved */

/*
 * Extended Multicast Address Table is from 0x0020000 to 0x0003FFFC.
 * This offset defines an offset to table that has provisioned multicast
 * addresses. It is stored in BRAM and will be used by hardware to provide
 * first line of address matching when a multicast frame is reveived. It
 * can minimize the use of CPU/software hence minimize performance impact.
 */
#define AXIETH1G_MCAST_TABLE_OFFSET        0x00020000  /**< Multicast table address */
/*@}*/

#define AXIETH1G_MAX_OFFSET                AXIETH1G_MCAST_TABLE_OFFSET

/* regvalister masks. The following constants define bit locations of various
 * bits in the registers. Constants are not defined for those registers
 * that have a single bit field representing all 32 bits. For further
 * information on the meaning of the various bit masks, refer to the HW spec.
 */

/** @name Reset and Address Filter (RAF) regvalister bit definitions.
 *  These bits are associated with the AXIETH1G_RAF_OFFSET register.
 * @{
 */
#define AXIETH1G_RAF_MCSTREJ_MASK	     	0x00000002  /**< Reject receive
						                                  *  multicast destination
						                                  *  address
						                                  */
#define AXIETH1G_RAF_BCSTREJ_MASK	     	0x00000004  /**< Reject receive
						                                  *  broadcast destination
						                                  *  address
						                                  */
#define AXIETH1G_RAF_TXVTAGMODE_MASK  	    0x00000018  /**< Tx VLAN TAG mode */
#define AXIETH1G_RAF_RXVTAGMODE_MASK  	    0x00000060  /**< Rx VLAN TAG mode */
#define AXIETH1G_RAF_TXVSTRPMODE_MASK 	    0x00000180  /**< Tx VLAN STRIP mode */
#define AXIETH1G_RAF_RXVSTRPMODE_MASK 	    0x00000600  /**< Rx VLAN STRIP mode */
#define AXIETH1G_RAF_NEWFNCENBL_MASK  	    0x00000800  /**< New function mode */
#define AXIETH1G_RAF_EMULTIFLTRENBL_MASK 	0x00001000  /**< Exteneded Multicast
						                                  *  Filtering mode
						                                  */
#define AXIETH1G_RAF_STATSRST_MASK  	    0x00002000 	/**< Statistics Counter
						                                  *  Reset
						                                  */
#define AXIETH1G_RAF_RXBADFRMEN_MASK     	0x00004000  /**< Receive Bad Frame
						                                  *  Enable
						                                  */
#define AXIETH1G_RAF_TXVTAGMODE_SHIFT 	    3       	/**< Tx Tag mode shift bits */
#define AXIETH1G_RAF_RXVTAGMODE_SHIFT 	    5       	/**< Rx Tag mode shift bits */
#define AXIETH1G_RAF_TXVSTRPMODE_SHIFT	    7	        /**< Tx strip mode shift bits*/
#define AXIETH1G_RAF_RXVSTRPMODE_SHIFT	    9	        /**< Rx Strip mode shift bits*/
/*@}*/

/** @name Transmit Pause Frame regvalister (TPF) bit definitions
 *  @{
 */
#define AXIETH1G_TPF_TPFV_MASK		        0x0000FFFF  /**< Tx pause frame value */
/*@}*/

/** @name Transmit Inter-Frame Gap Adjustement regvalister (TFGP) bit definitions
 *  @{
 */
#define AXIETH1G_TFGP_IFGP_MASK		        0x0000007F  /**< Transmit inter-frame
					                                      *  gap adjustment value
					                                      */
/*@}*/

/** @name Interrupt Status/Enable/Mask registers bit definitions
 *  The bit definition of these three interrupt registers are the same.
 *  These bits are associated with the AXIETH1G_IS_OFFSET, AXIETH1G_IP_OFFSET, and
 *  AXIETH1G_IE_OFFSET registers.
 * @{
 */
#define AXIETH1G_INT_HARDACSCMPLT_MASK	    0x00000001  /**< Hard register
						                                  *  access complete
						                                  */
#define AXIETH1G_INT_AUTONEG_MASK		    0x00000002  /**< Auto negotiation
						                                  *  complete
						                                  */
#define AXIETH1G_INT_RXCMPIT_MASK		    0x00000004  /**< Rx complete */
#define AXIETH1G_INT_RXRJECT_MASK		    0x00000008  /**< Rx frame rejected */
#define AXIETH1G_INT_RXFIFOOVR_MASK		    0x00000010  /**< Rx fifo overrun */
#define AXIETH1G_INT_TXCMPIT_MASK		    0x00000020  /**< Tx complete */
#define AXIETH1G_INT_RXDCMLOCK_MASK		    0x00000040  /**< Rx Dcm Lock */
#define AXIETH1G_INT_MGTRDY_MASK		    0x00000080  /**< MGT clock Lock */
#define AXIETH1G_INT_PHYRSTCMPLT_MASK	    0x00000100  /**< Phy Reset complete */

#define AXIETH1G_INT_ALL_MASK		        0x0000003F  /**< All the ints */

#define AXIETH1G_INT_RECV_ERROR_MASK			\
	(AXIETH1G_INT_RXRJECT_MASK | AXIETH1G_INT_RXFIFOOVR_MASK) /**< INT bits that
							                                    *  indicate receive
							                                    *  errors
							                                    */
/*@}*/


/** @name TPID regvalister (TPID) bit definitions
 *  @{
 */
#define AXIETH1G_TPID_0_MASK			    0x0000FFFF  /**< TPID 0 */
#define AXIETH1G_TPID_1_MASK			    0xFFFF0000  /**< TPID 1 */
/*@}*/


/** @name Receive configuration Word 1 (RCW1) regvalister bit definitions
 *  @{
 */
#define AXIETH1G_RCW1_RST_MASK	            0x80000000  /**< Reset */
#define AXIETH1G_RCW1_JUM_MASK	            0x40000000  /**< Jumbo frame enable */
#define AXIETH1G_RCW1_FCS_MASK	            0x20000000  /**< In-Band FCS enable
					                                      *  (FCS not stripped) 
                                                          */
#define AXIETH1G_RCW1_RX_MASK	            0x10000000  /**< Receiver enable */
#define AXIETH1G_RCW1_VLAN_MASK	            0x08000000  /**< VLAN frame enable */
#define AXIETH1G_RCW1_LT_DIS_MASK	        0x02000000  /**< Length/type field valid check
					                                      *  disable
					                                      */
#define AXIETH1G_RCW1_CL_DIS_MASK	        0x01000000  /**< Control frame Length check
					                                      *  disable
					                                      */
#define AXIETH1G_RCW1_1588_TIMESTAMP_EN_MASK 0x00400000 /**< Inband 1588 time
											              *  stamp enable
											              */
#define AXIETH1G_RCW1_PAUSEADDR_MASK        0x0000FFFF  /**< Pause frame source
					                                      *  address bits [47:32].Bits
					                                      *	 [31:0] are stored in register
					                                      *  RCW0
					                                      */
/*@}*/


/** @name Transmitter configuration (TC) regvalister bit definitions
 *  @{
 */
#define AXIETH1G_TC_RST_MASK		        0x80000000  /**< Reset */
#define AXIETH1G_TC_JUM_MASK		        0x40000000  /**< Jumbo frame enable */
#define AXIETH1G_TC_FCS_MASK		        0x20000000  /**< In-Band FCS enable
					                                      *  (FCS not generated)
					                                      */
#define AXIETH1G_TC_TX_MASK		            0x10000000  /**< Transmitter enable */
#define AXIETH1G_TC_VLAN_MASK	            0x08000000  /**< VLAN frame enable */
#define AXIETH1G_TC_IFG_MASK		        0x02000000  /**< Inter-frame gap adjustment
					                                      *  enable
					                                      */
#define AXIETH1G_TC_1588_CMD_EN_MASK		0x00400000  /**< 1588 Cmd field enable */
/*@}*/


/** @name Flow Control configuration (FCC) regvalister Bit definitions
 *  @{
 */
#define AXIETH1G_FCC_FCRX_MASK	            0x20000000  /**< Rx flow control enable */
#define AXIETH1G_FCC_FCTX_MASK	            0x40000000  /**< Tx flow control enable */
/*@}*/


/** @name Ethernet MAC Mode configuration (EMMC) regvalister bit definitions
 * @{
 */
#define AXIETH1G_EMMC_LINKSPEED_MASK 	    0xC0000000  /**< Link speed */
#define AXIETH1G_EMMC_RGMII_MASK	 	    0x20000000  /**< RGMII mode enable */
#define AXIETH1G_EMMC_SGMII_MASK	 	    0x10000000  /**< SGMII mode enable */
#define AXIETH1G_EMMC_GPCS_MASK	 	        0x08000000  /**< 1000BaseX mode enable*/
#define AXIETH1G_EMMC_HOST_MASK	 	        0x04000000  /**< Host interface enable*/
#define AXIETH1G_EMMC_TX16BIT	 	        0x02000000  /**< 16 bit Tx client
						                                  *  enable
						                                  */
#define AXIETH1G_EMMC_RX16BIT	 	        0x01000000  /**< 16 bit Rx client
						                                  *  enable
						                                  */
#define AXIETH1G_EMMC_LINKSPD_10		    0x00000000  /**< Link speed mask for
						                                  *  10 Mbit
						                                  */
#define AXIETH1G_EMMC_LINKSPD_100		    0x40000000  /**< Link speed mask for 100
						                                  *  Mbit
						                                  */
#define AXIETH1G_EMMC_LINKSPD_1000		    0x80000000  /**< Link speed mask for
						                                  *  1000 Mbit
						                                  */
/*@}*/


/** @name RGMII/SGMII configuration (PHYC) regvalister bit definitions
 * @{
 */
#define AXIETH1G_PHYC_SGMIILINKSPEED_MASK 	0xC0000000  /**< SGMII link speed mask*/     
#define AXIETH1G_PHYC_RGMIILINKSPEED_MASK 	0x0000000C  /**< RGMII link speed */         
#define AXIETH1G_PHYC_RGMIIHD_MASK	 	    0x00000002  /**< RGMII Half-duplex */        
#define AXIETH1G_PHYC_RGMIILINK_MASK 	    0x00000001  /**< RGMII link status */        
#define AXIETH1G_PHYC_RGLINKSPD_10		    0x00000000  /**< RGMII link 10 Mbit */       
#define AXIETH1G_PHYC_RGLINKSPD_100		    0x00000004  /**< RGMII link 100 Mbit */      
#define AXIETH1G_PHYC_RGLINKSPD_1000 	    0x00000008  /**< RGMII link 1000 Mbit */     
#define AXIETH1G_PHYC_SGLINKSPD_10  		0x00000000  /**< SGMII link 10 Mbit */       
#define AXIETH1G_PHYC_SGLINKSPD_100		    0x40000000  /**< SGMII link 100 Mbit */      
#define AXIETH1G_PHYC_SGLINKSPD_1000 	    0x80000000  /**< SGMII link 1000 Mbit */     
/*@}*/


/** @name MDIO Management configuration (MC) regvalister bit definitions
 * @{
 */
#define AXIETH1G_MDIO_MC_MDIOEN_MASK		0x00000040  /**< MII management enable*/
#define AXIETH1G_MDIO_MC_CLOCK_DIVIDE_MAX	0x3F        /**< Maximum MDIO divisor */
/*@}*/


/** @name MDIO Management Control regvalister (MCR) regvalister bit definitions
 * @{
 */
#define AXIETH1G_MDIO_MCR_PHYAD_MASK		0x1F000000  /**< Phy Address Mask */
#define AXIETH1G_MDIO_MCR_PHYAD_SHIFT	    24	        /**< Phy Address Shift */
#define AXIETH1G_MDIO_MCR_REGAD_MASK		0x001F0000  /**< regval Address Mask */
#define AXIETH1G_MDIO_MCR_REGAD_SHIFT	    16	        /**< regval Address Shift */
#define AXIETH1G_MDIO_MCR_OP_MASK		    0x0000C000  /**< Operation Code Mask */
#define AXIETH1G_MDIO_MCR_OP_SHIFT		    13	        /**< Operation Code Shift */
#define AXIETH1G_MDIO_MCR_OP_READ_MASK	    0x00008000  /**< Op Code Read Mask */
#define AXIETH1G_MDIO_MCR_OP_WRITE_MASK	    0x00004000  /**< Op Code Write Mask */
#define AXIETH1G_MDIO_MCR_INITIATE_MASK	    0x00000800  /**< Ready Mask */
#define AXIETH1G_MDIO_MCR_READY_MASK		0x00000080  /**< Ready Mask */

/*@}*/

/** @name MDIO Interrupt Enable/Mask/Status registers bit definitions
 *  The bit definition of these three interrupt registers are the same.
 *  These bits are associated with the AXIETH1G_IS_OFFSET, AXIETH1G_IP_OFFSET, and
 *  AXIETH1G_IE_OFFSET registers.
 * @{
 */
#define AXIETH1G_MDIO_INT_MIIM_RDY_MASK	    0x00000001  /**< MIIM Interrupt */
/*@}*/


/** @name Axi Ethernet Unicast Address regvalister Word 1 (UAW1) regvalister Bit
 *  definitions
 * @{
 */
#define AXIETH1G_UAW1_UNICASTADDR_MASK 	    0x0000FFFF  /**< Station address bits
						                                  *  [47:32]
						                                  *  Station address bits [31:0]
						                                  *  are stored in register
						                                  *  UAW0
                                                          */
/*@}*/


/** @name Filter Mask Index (FMI) regvalister bit definitions
 * @{
 */
#define AXIETH1G_FMI_PM_MASK			    0x80000000  /**< Promiscuous mode
						                                  *  enable
						                                  */
#define AXIETH1G_FMI_IND_MASK		        0x00000003  /**< Index Mask */

/*@}*/


/** @name Extended multicast buffer descriptor bit mask
 * @{
 */
#define AXIETH1G_BD_RX_USR2_BCAST_MASK	    0x00000004
#define AXIETH1G_BD_RX_USR2_IP_MCAST_MASK	0x00000002
#define AXIETH1G_BD_RX_USR2_MCAST_MASK	    0x00000001
/*@}*/

/** @name Axi Ethernet Multicast Address regvalister Word 1 (MAW1)
 * @{
 */
#define AXIETH1G_MAW1_RNW_MASK         	    0x00800000  /**< Multicast address
						                                  *  table register read
						                                  *  enable
						                                  */
#define AXIETH1G_MAW1_ADDR_MASK        	    0x00030000  /**< Multicast address
						                                  *  table register address
						                                  */
#define AXIETH1G_MAW1_MULTICADDR_MASK  	    0x0000FFFF  /**< Multicast address
						                                  *  bits [47:32]
						                                  *  Multicast address
						                                  *  bits [31:0] are stored
						                                  *  in register MAW0
						                                  */
#define AXIETH1G_MAW1_MATADDR_SHIFT_MASK 	16	        /**< Number of bits to shift
						                                  *  right to align with
						                                  *  AXIETH1G_MAW1_CAMADDR_MASK
						                                  */
/*@}*/


/** @name Other Constant definitions used in the driver
 * @{
 */

#define AXIETH1G_SPEED_10_MBPS		        10	        /**< speed of 10 Mbps */
#define AXIETH1G_SPEED_100_MBPS		        100	        /**< speed of 100 Mbps */
#define AXIETH1G_SPEED_1000_MBPS		    1000	    /**< speed of 1000 Mbps */
#define AXIETH1G_SPEED_2500_MBPS		    2500	    /**< speed of 2500 Mbps */

#define AXIETH1G_SOFT_TEMAC_LOW_SPEED	    0	        /**< For soft cores with 10/100
						                                  *  Mbps speed
						                                  */
#define AXIETH1G_SOFT_TEMAC_HIGH_SPEED	    1           /**< For soft cores with
						                                  *  10/100/1000 Mbps speed
						                                  */
#define AXIETH1G_HARD_TEMAC_TYPE		    2	        /**< For hard TEMAC cores used
						                                  *  virtex-6
						                                  */
#define AXIETH1G_PHY_ADDR_LIMIT		        31	        /**< Max limit while accessing
						                                  *  and searching for available
						                                  *  PHYs
						                                  */
#define AXIETH1G_PHY_REG_NUM_LIMIT		    31	        /**< Max register limit in PHY
						                                  *  as mandated by the spec
						                                  */
#define AXIETH1G_RST_DEFAULT_TIMEOUT_VAL    1000000     /**< Timeout in us used
						                                  *  while checking if the core
						                                  *  had come out of reset or for the driver
						                                  *  API to wait for before
						                                  *  returning a failure case
						                                  */
#define AXIETH1G_VLAN_TABL_STRP_FLD_LEN	    1	        /**< Strip field length in vlan
						                                  *  table used for extended
						                                  *  vlan features
						                                  */
#define AXIETH1G_VLAN_TABL_TAG_FLD_LEN	    1	        /**< Tag field length in vlan
						                                  *  table used for extended
						                                  *  vlan features.
						                                  */
#define AXIETH1G_MAX_VLAN_TABL_ENTRY		0xFFF	    /**< Max possible number of
						                                  *  entries in vlan table used
						                                  *  for extended vlan
						                                  *  features
						                                  */
#define AXIETH1G_VLAN_TABL_VID_START_OFFSET	2	        /**< VID field start offset in
						                                  *  each entry in the VLAN
						                                  *  table
						                                  */
#define AXIETH1G_VLAN_TABL_STRP_STRT_OFFSET	1	        /**< Strip field start offset
						                                  *  in each entry in the VLAN
						                                  * table
						                                  */
#define AXIETH1G_VLAN_TABL_STRP_ENTRY_MASK	0x01	    /**< Mask used to extract the
						                                  *  the strip field from an
						                                  *  entry in VLAN table.
						                                  */
#define AXIETH1G_VLAN_TABL_TAG_ENTRY_MASK	0x01	    /**< Mask used to extract the
						                                  *  the tag field from an
						                                  *  entry in VLAN table
						                                  */

/** @name Configuration options
 *
 * The following are device configuration options. See the
 * <i>XAxiEthernet_SetOptions</i>, <i>XAxiEthernet_ClearOptions</i> and
 * <i>XAxiEthernet_GetOptions</i> routines for information on how to use
 * options.
 *
 * The default state of the options are also noted below.
 *
 * @{
 */

/**< AXIETH1G_PROMISC_OPTION specifies the Axi Ethernet device to accept all
 *   incoming packets.
 *   This driver sets this option to disabled (cleared) by default.
 */
#define AXIETH1G_PROMISC_OPTION		        0x00000001

/**< AXIETH1G_JUMBO_OPTION specifies the Axi Ethernet device to accept jumbo
 *   frames for transmit and receive.
 *   This driver sets this option to disabled (cleared) by default.
 */
#define AXIETH1G_JUMBO_OPTION		        0x00000002

/**< AXIETH1G_VLAN_OPTION specifies the Axi Ethernet device to enable VLAN support
 *   for transmit and receive.
 *   This driver sets this option to disabled (cleared) by default.
 */
#define AXIETH1G_VLAN_OPTION			    0x00000004

/**< AXIETH1G_FLOW_CONTROL_OPTION specifies the Axi Ethernet device to recognize
 *   received flow control frames.
 *   This driver sets this option to enabled (set) by default.
 */
#define AXIETH1G_FLOW_CONTROL_OPTION		0x00000008

/**< AXIETH1G_FCS_STRIP_OPTION specifies the Axi Ethernet device to strip FCS and
 *   PAD from received frames. Note that PAD from VLAN frames is not stripped.
 *   This driver sets this option to enabled (set) by default.
 */
#define AXIETH1G_FCS_STRIP_OPTION		    0x00000010

/**< AXIETH1G_FCS_INSERT_OPTION specifies the Axi Ethernet device to generate the
 *   FCS field and add PAD automatically for outgoing frames.
 *   This driver sets this option to enabled (set) by default.
 */
#define AXIETH1G_FCS_INSERT_OPTION		    0x00000020

/**< AXIETH1G_LENTYPE_ERR_OPTION specifies the Axi Ethernet device to enable
 *   Length/Type error checking (mismatched type/length field) for received
 *   frames.
 *   This driver sets this option to enabled (set) by default.
 */
#define AXIETH1G_LENTYPE_ERR_OPTION		    0x00000040

/**< AXIETH1G_TRANSMITTER_ENABLE_OPTION specifies the Axi Ethernet device
 *   transmitter to be enabled.
 *   This driver sets this option to enabled (set) by default.
 */
#define AXIETH1G_TRANSMITTER_ENABLE_OPTION	0x00000080

/**< AXIETH1G_RECEIVER_ENABLE_OPTION specifies the Axi Ethernet device receiver to
 *   be enabled.
 *   This driver sets this option to enabled (set) by default.
 */
#define AXIETH1G_RECEIVER_ENABLE_OPTION	    0x00000100

/**< AXIETH1G_BROADCAST_OPTION specifies the Axi Ethernet device to receive frames
 *   sent to the broadcast Ethernet address.
 *   This driver sets this option to enabled (set) by default.
 */
#define AXIETH1G_BROADCAST_OPTION		    0x00000200

/**< AXIETH1G_MULTICAST_OPTION specifies the Axi Ethernet device to receive frames
 *   sent to Ethernet addresses that are programmed into the Multicast Address
 *   Table (MAT).
 *   This driver sets this option to disabled (cleared) by default.
 */
#define AXIETH1G_MULTICAST_OPTION		    0x00000400

/**< AXIETH1G_EXT_MULTICAST_OPTION specifies the Axi Ethernet device to receive
 *   frames sent to Ethernet addresses that are programmed into the Multicast
 *   Address Table.
 *   This driver sets this option to be dependent on HW configuration
 *   by default.
 */
#define AXIETH1G_EXT_MULTICAST_OPTION	    0x00000800

/**< AXIETH1G_EXT_TXVLAN_TRAN_OPTION specifies the Axi Ethernet device to enable
 *   transmit VLAN translation.
 *   This driver sets this option to be dependent on HW configuration
 *   by default.
 */
#define AXIETH1G_EXT_TXVLAN_TRAN_OPTION	    0x00001000

/**< AXIETH1G_EXT_RXVLAN_TRAN_OPTION specifies the Axi Ethernet device to enable
 *   receive VLAN translation.
 *   This driver sets this option to be dependent on HW configuration
 *   by default.
 */
#define AXIETH1G_EXT_RXVLAN_TRAN_OPTION	    0x00002000

/**< AXIETH1G_EXT_TXVLAN_TAG_OPTION specifies the Axi Ethernet device to enable
 *   transmit VLAN tagging.
 *   This driver sets this option to be dependent during HW build time
 *   by default.
 */
#define AXIETH1G_EXT_TXVLAN_TAG_OPTION	    0x00004000

/**< AXIETH1G_EXT_RXVLAN_TAG_OPTION specifies the Axi Ethernet device to enable
 *   receive VLAN tagging.
 *   This driver sets this option to be dependent during HW build time
 *   by default.
 */
#define AXIETH1G_EXT_RXVLAN_TAG_OPTION	    0x00008000

/**< AXIETH1G_EXT_TXVLAN_STRP_OPTION specifies the Axi Ethernet device to enable
 *   transmit VLAN stripping.
 *   This driver sets this option to be dependent during HW build time
 *   by default.
 */
#define AXIETH1G_EXT_TXVLAN_STRP_OPTION	    0x00010000

/**< AXIETH1G_EXT_RXVLAN_STRP_OPTION specifies the Axi Ethernet device to enable
 *   receive VLAN stripping.
 *   This driver sets this option to be dependent during HW build time
 *   by default.
 */
#define AXIETH1G_EXT_RXVLAN_STRP_OPTION	    0x00020000


#define AXIETH1G_DEFAULT_OPTIONS				\
		(AXIETH1G_FLOW_CONTROL_OPTION       |   \
		 AXIETH1G_BROADCAST_OPTION          |   \
		 AXIETH1G_FCS_INSERT_OPTION         |   \
		 AXIETH1G_FCS_STRIP_OPTION          |	\
		 AXIETH1G_LENTYPE_ERR_OPTION        |   \
		 AXIETH1G_TRANSMITTER_ENABLE_OPTION | 	\
		 AXIETH1G_RECEIVER_ENABLE_OPTION)
/**< AXIETH1G_DEFAULT_OPTIONS specify the options set in XAxiEthernet_Reset() and
 *   XAxiEthernet_CfgInitialize()
 */
/*@}*/

#define AXIETH1G_MDIO_DIV_DFT	            29	        /**< Default MDIO clock divisor */

/**<
 * The next few constants help upper layers determine the size of memory
 * pools used for Ethernet buffers and descriptor lists.
 */
#define AXIETH1G_MAC_ADDR_SIZE		        6	        /**< MAC addresses are 6 bytes */
#define AXIETH1G_MTU				        1500	    /**< Max MTU size of an Ethernet
						                                  *  frame
						                                  */
#define AXIETH1G_JUMBO_MTU			        8982        /**< Max MTU size of a jumbo
						                                  *  Ethernet frame
						                                  */
#define AXIETH1G_HDR_SIZE			        14	        /**< Size of an Ethernet header */
#define AXIETH1G_HDR_VLAN_SIZE		        18	        /**< Size of an Ethernet header
						                                  *  with VLAN
						                                  */
#define AXIETH1G_TRL_SIZE			        4	        /**< Size of an Ethernet trailer
						                                  *  (FCS)
						                                  */
#define AXIETH1G_MAX_FRAME_SIZE	 (AXIETH1G_MTU + AXIETH1G_HDR_SIZE + AXIETH1G_TRL_SIZE)
#define AXIETH1G_MAX_VLAN_FRAME_SIZE  (AXIETH1G_MTU + AXIETH1G_HDR_VLAN_SIZE + AXIETH1G_TRL_SIZE)
#define AXIETH1G_MAX_JUMBO_FRAME_SIZE (AXIETH1G_JUMBO_MTU + AXIETH1G_HDR_SIZE + AXIETH1G_TRL_SIZE)

/**<
 * Constant values returned by XAxiEthernet_GetPhysicalInterface(). Note that
 * these values match design parameters from the Axi Ethernet spec.
 */
#define AXIETH1G_PHY_TYPE_MII		        0
#define AXIETH1G_PHY_TYPE_GMII		        1
#define AXIETH1G_PHY_TYPE_RGMII_1_3		    2
#define AXIETH1G_PHY_TYPE_RGMII_2_0		    3
#define AXIETH1G_PHY_TYPE_SGMII		        4
#define AXIETH1G_PHY_TYPE_1000BASE_X		5

#define AXIETH1G_TPID_MAX_ENTRIES		    4           /**< Number of storable TPIDs in
					                                      *  Table
					                                      */

/**<
 * Constant values pass into XAxiEthernet_SetV[tag|Strp]Mode() and returned by
 * XAxiEthernet_GetV[tag|Strp]Mode().
 */
#define AXIETH1G_VTAG_NONE			        0	        /**< No tagging */
#define AXIETH1G_VTAG_ALL			        1	        /**< Tag all frames */
#define AXIETH1G_VTAG_EXISTED		        2	        /**< Tag already tagged frames */
#define AXIETH1G_VTAG_SELECT		  	    3	        /**< Tag selected already tagged
						                                  *  frames
						                                  */
#define AXIETH1G_DEFAULT_TXVTAG_MODE  	    AXIETH1G_VTAG_ALL
#define AXIETH1G_DEFAULT_RXVTAG_MODE  	    AXIETH1G_VTAG_ALL

#define AXIETH1G_VSTRP_NONE			        0	        /**< No stripping */
#define AXIETH1G_VSTRP_ALL			        1	        /**< Strip one tag from all
						                                  *  frames
						                                  */
#define AXIETH1G_VSTRP_SELECT		        3	        /**< Strip one tag from selected
						                                  *  frames
						                                  */
#define AXIETH1G_DEFAULT_TXVSTRP_MODE	    AXIETH1G_VSTRP_ALL
#define AXIETH1G_DEFAULT_RXVSTRP_MODE	    AXIETH1G_VSTRP_ALL

#define AXIETH1G_RX				            1           /**< Receive direction  */
#define AXIETH1G_TX				            2           /**< Transmit direction */

#define AXIETH1G_SOFT_TEMAC_10_100_MBPS	     0
#define AXIETH1G_SOFT_TEMAC_10_100_1000_MBPS 1
#define AXIETH1G_HARD_TEMC			         2

/**
 * This typedef contains configuration information for a Axi Ethernet device.
 */
typedef struct {
	b_u16 dev_id;	              /**< DeviceId is the unique ID  of the device */             
	b_u8 temac_type;              /**< Temac Type can have 3 possible values. They are         
			                        *  0 for SoftTemac at 10/100 Mbps, 1 for SoftTemac         
			                        *  at 10/100/1000 Mbps and 2 for Vitex6 Hard Temac         
			                        */                                                         
	b_u8 tx_csum;	              /**< TxCsum indicates that the device has checksum           
			                        *  offload on the Tx channel or not.                       
			                        */                                                         
	b_u8 rx_csum;	              /**< RxCsum indicates that the device has checksum           
			                        *  offload on the Rx channel or not.                       
			                        */                                                         
	b_u8 phy_type;	              /**< phy_type indicates which type of PHY interface is       
			                        *  used (MII, GMII, RGMII, etc.                            
			                        */                                                         
	b_u8 tx_vlan_tran;            /**< TX VLAN Translation indication */                      
	b_u8 rx_vlan_tran;            /**< RX VLAN Translation indication */                      
	b_u8 tx_vlan_tag;             /**< TX VLAN tagging indication */                          
	b_u8 rx_vlan_tag;             /**< RX VLAN tagging indication */                          
	b_u8 tx_vlan_strp;            /**< TX VLAN stripping indication */                        
	b_u8 rx_vlan_strp;            /**< RX VLAN stripping indication */                        
	b_u8 ext_mcast;               /**< Extend multicast indication */                          
	b_u8 stats;	                  /**< Statistics gathering option */
	b_u8 avb;		              /**< Avb option */
	b_u8 enable_sgmii_over_lvds;  /**< Enable LVDS option */
	b_u8 enable_1588;	          /**< Enable 1588 option */
	b_u32 speed;	              /**< Tells whether MAC is 1G or 2p5G */
	b_u8 num_table_entries;	      /**< Number of table entries */

	b_u8 temac_intr;	          /**< Axi Ethernet interrupt ID */

	b_i32 axi_dev_type;           /**< AxiDevType is the type of device attached to the
			                        *  Axi Ethernet's AXI4-Stream interface.
			                        */
	b_u8 axi_fifo_intr;	          /**< AxiFifoIntr interrupt ID (unused if DMA) */
	b_u8 axi_dma_rx_intr;         /**< Axi DMA RX interrupt ID (unused if FIFO) */
	b_u8 axi_dma_tx_intr;         /**< Axi DMA TX interrupt ID (unused if FIFO) */
	b_u8 axi_mc_dma_chan_cnt;     /**< Axi MCDMA Channel Count */
	b_u8 axi_mc_dma_rx_intr[16];  /**< Axi MCDMA Rx interrupt ID (unused if AXI DMA or FIFO) */
	b_u8 axi_mc_dma_tx_intr[16];  /**< AXI MCDMA TX interrupt ID (unused if AXIX DMA or FIFO) */
} axieth1g_config_t;

/** @struct axieth1g_ctx_t
 *  axieth1g context structure
 */
typedef struct axieth1g_ctx_t {
    void                *devo;
    eth_brdspec_t       brdspec;
    axieth1g_config_t   config;
    b_u32               options;
    b_bool              is_started;
} axieth1g_ctx_t;

static ccl_err_t    _ret;
static int          _rc;


/***************** Macros (Inline Functions) Definitions *********************/

/**
 * AXIETH1G_IS_STARTED reports if the device is in the started or stopped
 * state. To be in the started state, the calling code must have made a
 * successful call to <i>_axieth1g_start</i>. To be in the stopped state,
 * <i>_axieth1g_stop</i> function must have been called.
 *
 * @param[in] p_axieth1g is a pointer to the of Axi Ethernet 
 *      instance to be worked on.
 *
 * @return
 *		- CCL_TRUE if the device has been started.
 *		- CCL_FALSE.if the device has not been started
 */
#define AXIETH1G_IS_STARTED(p_axieth1g) \
	((p_axieth1g)->is_started == CCL_TRUE)

/**
 * AXIETH1G_IS_RECV_FRAME_DROPPED determines if the device 
 * thinks it has dropped a receive frame. The device interrupt 
 * status register is read to determine this. 
 *
 * @param[in] p_axieth1g is a pointer to the Axi Ethernet 
 *      instance to be worked on.
 *
 * @return
 *		- CCL_TRUE if a frame has been dropped
 *		- CCL_FALSE if a frame has NOT been dropped.
 */
#define AXIETH1G_IS_RECV_FRAME_DROPPED(p_axieth1g) ({ \
    b_u32 regval; \
    AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_IS_OFFSET, \
                      sizeof(b_u32), 0, \
                      (char *)&regval, sizeof(b_u32)); \
    ((regval & AXIETH1G_INT_RXRJECT_MASK) ? CCL_TRUE : CCL_FALSE); \
})

/**
 *
 * AXIETH1G_IS_RX_PARTIAL_CSUM determines if the device is 
 * configured with partial checksum offloading on the receive 
 * channel. 
 *
 * @param[in] p_axieth1g is a pointer to the Axi Ethernet 
 *      instance to be worked on.
 * @return
 *		- CCL_TRUE if the device is configured with partial checksum
 *		  offloading on the receive channel.
 *      - CCL_FALSE if the device is not configured with
 *        partial checksum offloading on the receive side.
 */
#define AXIETH1G_IS_RX_PARTIAL_CSUM(p_axieth1g)   \
	((((p_axieth1g)->config.rx_csum) == 0x01) ? CCL_TRUE : CCL_FALSE)

/**
 * AXIETH1G_IS_TX_PARTIAL_CSUM determines if the device is 
 * configured with partial checksum offloading on the transmit 
 * channel. 
 *
 * @param[in] p_axieth1g is a pointer to the Axi Ethernet 
 *      instance to be worked on.
 *
 * @return
 *		- CCL_TRUE if the device is configured with partial checksum
 *		  offloading on the transmit side.
 *      - CCL_FALSE if the device is not configured with
 *        partial checksum offloading on the transmit side.
 */
#define AXIETH1G_IS_TX_PARTIAL_CSUM(p_axieth1g) \
	((((p_axieth1g)->config.tx_csum) == 0x01) ? CCL_TRUE : CCL_FALSE)

/**
 *  AXIETH1G_IS_RX_FULL_CSUM determines if the device is
 *  configured with full checksum offloading on the receive
 *  channel.
 *
 *  @param[in] p_axieth1g is a pointer to the Axi Ethernet
 *       instance to be worked on.
 * @return
 *		- CCL_TRUE if the device is configured with full checksum
 *		  offloading on the receive channel.
 *       - CCL_FALSE if the device is not configured with
 *         full checksum offloading on the receive side.
 */
#define AXIETH1G_IS_RX_FULL_CSUM(p_axieth1g)   \
	((((p_axieth1g)->config.rx_csum) == 0x02) ? CCL_TRUE : CCL_FALSE)

/**
 * AXIETH1G_IS_TX_FULL_CSUM determines if the device is 
 * configured with full checksum offloading on the transmit 
 * channel. 
 *
 * @param[in] p_axieth1g is a pointer to the Axi Ethernet 
 *      instance to be worked on.
 *
 * @return
 *		- CCL_TRUE if the device is configured with full checksum
 *		  offloading on the transmit side.
 *      - CCL_FALSE if the device is not configured with
 *        full checksum offloading on the transmit side.
 */
#define AXIETH1G_IS_TX_FULL_CSUM(p_axieth1g) \
	((((p_axieth1g)->config.tx_csum) == 0x02) ? CCL_TRUE : CCL_FALSE)

/**
 * AXIETH1G_GET_PHYSICAL_IF returns the type of PHY interface being
 * used by the given instance, specified by <i>p_axieth1g</i>.
 *
 * @param[in] p_axieth1g is a pointer to the Axi Ethernet 
 *      instance to be worked on.
 *
 * @return	The Physical Interface type which is one of AXIETH1G_PHY_TYPE_x
 *		where x is MII, GMII, RGMII_1_3, RGMII_2_0, SGMII, or
 *		1000BASE_X (defined in xaxiethernet.h).
 *
 */
#define AXIETH1G_GET_PHYSICAL_IF(p_axieth1g)	   \
	((p_axieth1g)->config.phy_type)

/**
 * AXIETH1G_INT_ENABLE enables the interrupts specified in 
 * <i>mask</i>. The corresponding interrupt for each bit set to 
 * 1 in <i>mask</i>, will be enabled. 
 *
 * @param[in] p_axieth1g is a pointer to the Axi Ethernet 
 *      instance to be worked on.
 *
 * @param[in] mask contains a bit mask of the interrupts to 
 *      enable. The mask can be formed using a set of bit wise
 *      or'd values from the
 *		<code>AXIETH1G_INT_*_MASK</code> definitions
 *
 * @return	None.
 */
#define AXIETH1G_INT_ENABLE(p_axieth1g, mask) do { \
    b_u32 regval; \
    AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_IS_OFFSET, \
                      sizeof(b_u32), 0, \
                      (char *)&regval, sizeof(b_u32)); \
    regval |= ((mask) & AXIETH1G_INT_ALL_MASK); \
    AXIETH1G_REG_WRITE(p_axieth1g, AXIETH1G_IS_OFFSET, \
                       sizeof(b_u32), 0, \
                       (char *)&regval, sizeof(b_u32)); \
} while (0)

/**
 * AXIETH1G_INT_DISABLE disables the interrupts specified in 
 * <i>mask</i>. The corresponding interrupt for each bit set to
 * 1 in <i>mask</i>, will be disabled. In other words,
 * AXIETH1G_INT_DISABLE uses the "set a bit to clear it" scheme. 
 *
 * @param[in] p_axieth1g is a pointer to the Axi Ethernet 
 *      instance to be worked on.
 * @param[in] mask contains a bit mask of the interrupts to 
 *      disable. The mask can be formed using a set of bit wise
 *      or'd values from the <code>AXIETH1G_INT_*_MASK</code>
 *      definitions
 *
 * @return	None.
 */
#define AXIETH1G_INT_DISABLE(p_axieth1g, mask) do { \
    b_u32 regval; \
    AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_IS_OFFSET, \
                      sizeof(b_u32), 0, \
                      (char *)&regval, sizeof(b_u32)); \
    regval &= ~((mask) & AXIETH1G_INT_ALL_MASK); \
    AXIETH1G_REG_WRITE(p_axieth1g, AXIETH1G_IS_OFFSET, \
                       sizeof(b_u32), 0, \
                       (char *)&regval, sizeof(b_u32)); \
} while (0)

/**
 * AXIETH1G_INT_CLEAR clears pending interrupts specified in 
 * <i>mask</i>. The corresponding pending interrupt for each bit 
 * set to 1 in <i>mask</i>, will be cleared. In other words, 
 * AXIETH1G_INT_CLEAR uses the "set a bit to clear it" scheme. 
 *
 *  @param[in] p_axieth1g is a pointer to the Axi Ethernet
 *       instance to be worked on.
 * @param[in] mask contains a bit mask of the pending 
 *      interrupts to clear. The mask can be formed using a set
 *      of bit wise or'd values from
 *		the <code>AXIETH1G_INT_*_MASK</code> definitions
 *		file.
 */
#define AXIETH1G_INT_CLEAR(p_axieth1g, mask) do { \
    b_u32 regval = ((mask) & AXIETH1G_INT_ALL_MASK); \
    AXIETH1G_REG_WRITE(p_axieth1g, AXIETH1G_IS_OFFSET, \
                       sizeof(b_u32), 0, \
                       (char *)&regval, sizeof(b_u32)); \
} while (0)

/**
 * AXIETH1G_IS_EXT_FUNC_CAP determines if the device is capable of the
 * new/extend VLAN and multicast features.
 *
 *  @param[in] p_axieth1g is a pointer to the Axi Ethernet
 *       instance to be worked on.
 *
 * @return
 *		- CCL_TRUE if the device is capable and configured with extended
 *		  Multicast and VLAN Tagging/Stripping and Translation.
 *		- CCL_TRUE if the device is NOT capable and NOT configured with
 *		  extended Multicast and VLAN Tagging/Stripping and
 *		  Translation.
 */
#define AXIETH1G_IS_EXT_FUNC_CAP(p_axieth1g) ({ \
    b_u32 regval; \
    AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_RAF_OFFSET, \
                      sizeof(b_u32), 0, \
                      (char *)&regval, sizeof(b_u32)); \
    (regval & AXIETH1G_RAF_NEWFNCENBL_MASK) ? CCL_TRUE : CCL_FALSE; \
})

/**
 * AXIETH1G_IS_EXT_MCAST_ENABLE determines if the extended 
 * multicast features is enabled. 
 *
 *  @param[in] p_axieth1g is a pointer to the Axi Ethernet
 *       instance to be worked on.
 *
 * @return
 *		- CCL_TRUE if the extended multicast features are enabled.
 *		- CCL_FALSE if the extended multicast features are NOT enabled
 *
 * @note	This function indicates when extended Multicast is 
 *       enabled in HW, extended multicast mode in wrapper can be
 *       tested.
 */
#define AXIETH1G_IS_EXT_MCAST_ENABLE(p_axieth1g) ({ \
    b_u32 regval; \
    AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_RAF_OFFSET, \
                      sizeof(b_u32), 0, \
                      (char *)&regval, sizeof(b_u32)); \
    (regval &  AXIETH1G_RAF_EMULTIFLTRENBL_MASK) ? CCL_TRUE : CCL_FALSE; \
})

/**
 * AXIETH1G_IS_EXT_MCAST determines if the device is built with 
 * new/extended multicast features. 
 *
 *  @param[in] p_axieth1g is a pointer to the Axi Ethernet
 *       instance to be worked on.
 *
 * @return
 *		- CCL_TRUE if the device is built with extended multicast features.
 *		- CCL_FALSE if the device is not built with the extended multicast
 *		  features.
 *
 * @note This function indicates when hardware is built with 
 *      extended Multicast feature.
 */
#define AXIETH1G_IS_EXT_MCAST(p_axieth1g) \
	(((p_axieth1g)->config.ext_mcast) ? CCL_TRUE : CCL_FALSE)

/**
 * AXIETH1G_IS_TX_VLAN_STRP determines if the device is 
 * configured with transmit VLAN stripping on the transmit 
 * channel. 
 *
 *  @param[in] p_axieth1g is a pointer to the Axi Ethernet
 *       instance to be worked on.
 *
 * @return
 *	 	- CCL_TRUE if the device is configured with
 *		  VLAN stripping on the Transmit channel.
 *		- CCL_FALSE if the device is NOT configured with
 *		  VLAN stripping on the Transmit channel.
 */
#define AXIETH1G_IS_TX_VLAN_STRP(p_axieth1g) \
	(((p_axieth1g)->config.tx_vlan_strp) ? CCL_TRUE : CCL_FALSE)

/**
 * AXIETH1G_IS_RX_VLAN_STRP determines if the device is 
 * configured with receive VLAN stripping on the receive channel.
 *
 *  @param[in] p_axieth1g is a pointer to the Axi Ethernet
 *       instance to be worked on.
 *
 * @return
 *	 	- CCL_TRUE if the device is configured with
 *		  VLAN stripping on the Receive channel.
 *		- CCL_FALSE if the device is NOT configured with
 *		  VLAN stripping on the Receive channel.
 */
#define AXIETH1G_IS_RX_VLAN_STRP(p_axieth1g) \
	(((p_axieth1g)->config.rx_vlan_strp) ? CCL_TRUE : CCL_FALSE)

/**
 * AXIETH1G_IS_TX_VLAN_TRAN determines if the device is 
 * configured with transmit VLAN translation on the transmit 
 * channel. 
 *
 * @param[in] p_axieth1g is a pointer to the Axi Ethernet
 *       instance to be worked on.
 *
 * @return
 *	 	- CCL_TRUE if the device is configured with
 *		 VLAN translation on the Transmit channel.
 *		- CCL_FALSE if the device is NOT configured with
 *		 VLAN translation on the Transmit channel.
 */
#define AXIETH1G_IS_TX_VLAN_TRAN(p_axieth1g) \
	(((p_axieth1g)->config.tx_vlan_tran) ? CCL_TRUE : CCL_FALSE)

/**
 * AXIETH1G_IS_RX_VLAN_TRAN determines if the device is 
 * configured with receive VLAN translation on the receive 
 * channel. 
 *
 *  @param[in] p_axieth1g is a pointer to the Axi Ethernet
 *       instance to be worked on.
 *
 * @return
 *	 	- CCL_TRUE if the device is configured with
 *		  VLAN translation on the Receive channel.
 *		- CCL_FALSE if the device is NOT configured with
 *		  VLAN translation on the Receive channel.
 */
#define AXIETH1G_IS_RX_VLAN_TRAN(p_axieth1g) \
	(((p_axieth1g)->config.rx_vlan_tran) ? CCL_TRUE : CCL_FALSE)

/**
 * AXIETH1G_IS_TX_VLAN_TAG determines if the device is configured 
 * with transmit VLAN tagging on the transmit channel. 
 * 
 *  @param[in] p_axieth1g is a pointer to the Axi Ethernet
 *       instance to be worked on.
 *
 * @return
 *	 	- CCL_TRUE if the device is configured with
 *		  VLAN tagging on the Transmit channel.
 *		- CCL_FALSE if the device is NOT configured with
 *		  VLAN tagging on the Transmit channel.
 */
#define AXIETH1G_IS_TX_VLAN_TAG(p_axieth1g)	\
	(((p_axieth1g)->config.tx_vlan_tag) ? CCL_TRUE : CCL_FALSE)

/**
 * AXIETH1G_IS_RX_VLAN_TAG determines if the device is configured 
 * with receive VLAN tagging on the receive channel. 
 *
 *  @param[in] p_axieth1g is a pointer to the Axi Ethernet
 *       instance to be worked on.
 *
 * @return
 *	 	- CCL_TRUE if the device is configured with
 *		  VLAN tagging on the Receive channel.
 *		- CCL_FALSE if the device is NOT configured with
 *		  VLAN tagging on the Receive channel.
 */
#define AXIETH1G_IS_RX_VLAN_TAG(p_axieth1g) \
	(((p_axieth1g)->config.rx_vlan_tag) ? CCL_TRUE : CCL_FALSE)

/**
 * AXIETH1G_GET_TEMAC_TYPE returns the Axi Ethernet type of the 
 * core. 
 *
 * @param	p_axieth1g is a pointer to the axieth1g_ctx_t instance to be
 *		worked on.
 *
 * @return
 *	 	- Returns the values of temac_type, which can be
 *		  AXIETH1G_SOFT_TEMAC_10_100_MBPS (0) for Soft Temac Core with
 *							speed 10/100 Mbps.
 *		  AXIETH1G_SOFT_TEMAC_10_100_1000_MBPS (1) for Soft Temac core with
 *							speed 10/100/1000 Mbps
 *		  AXIETH1G_HARD_TEMC (2) for Hard Temac Core for Virtex-6
 */
#define AXIETH1G_GET_TEMAC_TYPE(p_axieth1g)	\
	((p_axieth1g)->config.temac_type)

/**
 *  AXIETH1G_IS_AVB_CONFIGURED returns determines if Ethernet AVB
 *  is configured in the harwdare or not.
 *
 *  @param[in]	p_axieth1g is a pointer to the Axi Ethernet
 *       instance to be worked on.
 *
 * @return
 *	 	- CCL_TRUE if the device is configured with
 *		  Ethernet AVB.
 *		- CCL_FALSE if the device is NOT configured with
 *		  Ethernet AVB.
 */
#define AXIETH1G_IS_AVB_CONFIGURED(p_axieth1g) \
	(((p_axieth1g)->config.avb) ? CCL_TRUE : CCL_FALSE)

/**
 * AXIETH1G_IS_SGMII_OVER_LVDS_ENABLED determines if SGMII over 
 * LVDS is enabled in the harwdare or not. 
 *
 *  @param[in] p_axieth1g is a pointer to the Axi Ethernet
 *       instance to be worked on.
 *
 * @return
 *	 	- CCL_TRUE if the device is configured with
 *		  SGMII over LVDS.
 *		- CCL_FALSE if the device is NOT configured with
 *		  SGMII over LVDS.
 */
#define AXIETH1G_IS_SGMII_OVER_LVDS_ENABLED(p_axieth1g) \
	(((p_axieth1g)->config.enable_sgmii_over_lvds) ? CCL_TRUE : CCL_FALSE)


/* axieth1g register read */
static ccl_err_t _axieth1g_reg_read(void *p_priv, b_u32 offset, 
                                    __attribute__((unused)) b_u32 offset_sz, 
                                    b_u32 idx, b_u8 *p_value, b_u32 size)
{
    axieth1g_ctx_t   *p_axieth1g;
    if ( !p_priv || offset > AXIETH1G_MAX_OFFSET || 
         !p_value || idx || size != sizeof(b_u32) ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIETH1G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    PDEBUGG("123$=> offset=0x%03x, size=%d\n", offset, size);

    p_axieth1g = (axieth1g_ctx_t *)p_priv;     

    _ret = devspibus_read_buff(p_axieth1g->brdspec.devname, offset, idx, p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to read over spi bus\n");
        AXIETH1G_ERROR_RETURN(_ret);
    }    

    return _ret;
}

/* axieth1g register write */
static ccl_err_t _axieth1g_reg_write(void *p_priv, b_u32 offset, 
                                     __attribute__((unused)) b_u32 offset_sz, 
                                     b_u32 idx, b_u8 *p_value, b_u32 size)
{
    axieth1g_ctx_t   *p_axieth1g;
    if ( !p_priv || offset > AXIETH1G_MAX_OFFSET || 
         !p_value || idx || size != sizeof(b_u32) ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIETH1G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    PDEBUG("123$=> offset=0x%03x, value=0x%08x\n", offset, *p_value);

    p_axieth1g = (axieth1g_ctx_t *)p_priv;     

    _ret = devspibus_write_buff(p_axieth1g->brdspec.devname, offset, idx, p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to write over spi bus\n");
        AXIETH1G_ERROR_RETURN(_ret);
    }    

    return _ret;
}

ccl_err_t axieth1g_attr_read(void *p_priv, b_u32 attrid, 
                             b_u32 offset, b_u32 offset_sz, 
                             b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || offset > AXIETH1G_MAX_OFFSET || 
         !p_value || idx || size != sizeof(b_u32) ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIETH1G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch (attrid) {
    case eAXIETH1G_RAF:                  
    case eAXIETH1G_TPF:                  
    case eAXIETH1G_IFGP:                 
    case eAXIETH1G_IS:                   
    case eAXIETH1G_IP:                   
    case eAXIETH1G_IE:                   
    case eAXIETH1G_TTAG:                 
    case eAXIETH1G_UAWL:                 
    case eAXIETH1G_UAWU:                 
    case eAXIETH1G_TPID0:                
    case eAXIETH1G_TPID1:                
    case eAXIETH1G_RXBL:                 
    case eAXIETH1G_TXBL:                 
    case eAXIETH1G_RXUNDRL:              
    case eAXIETH1G_RXFRAGL:              
    case eAXIETH1G_RX64BL:               
    case eAXIETH1G_RX65B127L:            
    case eAXIETH1G_RX128B255L:           
    case eAXIETH1G_RX256B511L:           
    case eAXIETH1G_RX512B1023L:          
    case eAXIETH1G_RX1024BL:             
    case eAXIETH1G_RXOVRL:               
    case eAXIETH1G_TX64BL:               
    case eAXIETH1G_TX65B127L:            
    case eAXIETH1G_TX128B255L:           
    case eAXIETH1G_TX256B511L:           
    case eAXIETH1G_TX512B1023L:          
    case eAXIETH1G_TX1024L:              
    case eAXIETH1G_TXOVRL:               
    case eAXIETH1G_RXFL:                 
    case eAXIETH1G_RXFCSERL:             
    case eAXIETH1G_RXBCSTFL:             
    case eAXIETH1G_RXMCSTFL:             
    case eAXIETH1G_RXCTRFL:              
    case eAXIETH1G_RXLTERL:              
    case eAXIETH1G_RXVLANFL:             
    case eAXIETH1G_RXPFL:                
    case eAXIETH1G_RXUOPFL:              
    case eAXIETH1G_TXFL:                 
    case eAXIETH1G_TXBCSTFL:             
    case eAXIETH1G_TXMCSTFL:             
    case eAXIETH1G_TXUNDRERL:            
    case eAXIETH1G_TXCTRFL:              
    case eAXIETH1G_TXVLANFL:             
    case eAXIETH1G_TXPFL:                
    case eAXIETH1G_TXSCL:                
    case eAXIETH1G_TXMCL:                
    case eAXIETH1G_TXDEF:                
    case eAXIETH1G_TXLTCL:               
    case eAXIETH1G_TXAECL:               
    case eAXIETH1G_TXEDEFL:              
    case eAXIETH1G_RXAERL:               
    case eAXIETH1G_RCW0:                 
    case eAXIETH1G_RCW1:
    case eAXIETH1G_TC:                   
    case eAXIETH1G_FCC:                  
    case eAXIETH1G_EMMC:                 
    case eAXIETH1G_RXFC:                 
    case eAXIETH1G_TXFC:                 
    case eAXIETH1G_TX_TIMESTAMP_ADJ:     
    case eAXIETH1G_PHYC:                 
    case eAXIETH1G_IDREG:                
    case eAXIETH1G_ARREG:                
    case eAXIETH1G_MDIO_MC:              
    case eAXIETH1G_MDIO_MCR:             
    case eAXIETH1G_MDIO_MWD:             
    case eAXIETH1G_MDIO_MRD:             
    case eAXIETH1G_MDIO_MIS:             
    case eAXIETH1G_MDIO_MIP:             
    case eAXIETH1G_MDIO_MIE:             
    case eAXIETH1G_MDIO_MIC:             
    case eAXIETH1G_UAW0:                 
    case eAXIETH1G_UAW1:                 
    case eAXIETH1G_FMFC:                 
    case eAXIETH1G_FMFV:                 
    case eAXIETH1G_FMFMV:                
    case eAXIETH1G_TX_VLAN_DATA:         
    case eAXIETH1G_RX_VLAN_DATA:         
    case eAXIETH1G_ETH_AVB:              
    case eAXIETH1G_MCAST_TABLE:   
        AXIETH1G_REG_READ(p_priv, offset, offset_sz, idx, p_value, size);
        break;
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK) 
        PDEBUG("Failure in attribute id: %d\n", attrid);

    return _ret;               
}

ccl_err_t axieth1g_attr_write(void *p_priv, b_u32 attrid, 
                             b_u32 offset, b_u32 offset_sz, 
                             b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || offset > AXIETH1G_MAX_OFFSET || 
         !p_value || idx || size != sizeof(b_u32) ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIETH1G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch (attrid) {
    case eAXIETH1G_RAF:                  
    case eAXIETH1G_TPF:                  
    case eAXIETH1G_IFGP:                 
    case eAXIETH1G_IS:                   
    case eAXIETH1G_IP:                   
    case eAXIETH1G_IE:                   
    case eAXIETH1G_TTAG:                 
    case eAXIETH1G_RTAG:                 
    case eAXIETH1G_UAWL:                 
    case eAXIETH1G_UAWU:                 
    case eAXIETH1G_TPID0:                
    case eAXIETH1G_TPID1:                
    case eAXIETH1G_RXBL:                 
    case eAXIETH1G_TXBL:                 
    case eAXIETH1G_RXUNDRL:              
    case eAXIETH1G_RXFRAGL:              
    case eAXIETH1G_RX64BL:               
    case eAXIETH1G_RX65B127L:            
    case eAXIETH1G_RX128B255L:           
    case eAXIETH1G_RX256B511L:           
    case eAXIETH1G_RX512B1023L:          
    case eAXIETH1G_RX1024BL:             
    case eAXIETH1G_RXOVRL:               
    case eAXIETH1G_TX64BL:               
    case eAXIETH1G_TX65B127L:            
    case eAXIETH1G_TX128B255L:           
    case eAXIETH1G_TX256B511L:           
    case eAXIETH1G_TX512B1023L:          
    case eAXIETH1G_TX1024L:              
    case eAXIETH1G_TXOVRL:               
    case eAXIETH1G_RXFL:                 
    case eAXIETH1G_RXFCSERL:             
    case eAXIETH1G_RXBCSTFL:             
    case eAXIETH1G_RXMCSTFL:             
    case eAXIETH1G_RXCTRFL:              
    case eAXIETH1G_RXLTERL:              
    case eAXIETH1G_RXVLANFL:             
    case eAXIETH1G_RXPFL:                
    case eAXIETH1G_RXUOPFL:              
    case eAXIETH1G_TXFL:                 
    case eAXIETH1G_TXBCSTFL:             
    case eAXIETH1G_TXMCSTFL:             
    case eAXIETH1G_TXUNDRERL:            
    case eAXIETH1G_TXCTRFL:              
    case eAXIETH1G_TXVLANFL:             
    case eAXIETH1G_TXPFL:                
    case eAXIETH1G_TXSCL:                
    case eAXIETH1G_TXMCL:                
    case eAXIETH1G_TXDEF:                
    case eAXIETH1G_TXLTCL:               
    case eAXIETH1G_TXAECL:               
    case eAXIETH1G_TXEDEFL:              
    case eAXIETH1G_RXAERL:               
    case eAXIETH1G_RCW0:                 
    case eAXIETH1G_RCW1:
    case eAXIETH1G_TC:                   
    case eAXIETH1G_FCC:                  
    case eAXIETH1G_EMMC:                 
    case eAXIETH1G_RXFC:                 
    case eAXIETH1G_TXFC:                 
    case eAXIETH1G_TX_TIMESTAMP_ADJ:     
    case eAXIETH1G_PHYC:                 
    case eAXIETH1G_IDREG:                
    case eAXIETH1G_ARREG:                
    case eAXIETH1G_MDIO_MC:              
    case eAXIETH1G_MDIO_MCR:             
    case eAXIETH1G_MDIO_MWD:             
    case eAXIETH1G_MDIO_MRD:             
    case eAXIETH1G_MDIO_MIS:             
    case eAXIETH1G_MDIO_MIP:             
    case eAXIETH1G_MDIO_MIE:             
    case eAXIETH1G_MDIO_MIC:             
    case eAXIETH1G_UAW0:                 
    case eAXIETH1G_UAW1:                 
    case eAXIETH1G_FMFC:                 
    case eAXIETH1G_FMFV:                 
    case eAXIETH1G_FMFMV:                
    case eAXIETH1G_TX_VLAN_DATA:         
    case eAXIETH1G_RX_VLAN_DATA:         
    case eAXIETH1G_ETH_AVB:              
    case eAXIETH1G_MCAST_TABLE:   
        AXIETH1G_REG_WRITE(p_priv, offset, offset_sz, idx, p_value, size);
        break;
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK) 
        PDEBUG("Failure in attribute id: %d\n", attrid);

    return _ret;
}

/* axieth1g device attributes definition */
static device_attr_t _attrs[] = {
    {
        .name = "raf", .id = eAXIETH1G_RAF, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_RAF_OFFSET
    },
    {
        .name = "tpf", .id = eAXIETH1G_TPF, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_TPF_OFFSET
    },
    {
        .name = "ifgp", .id = eAXIETH1G_IFGP, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_IFGP_OFFSET
    },
    {
        .name = "ints", .id = eAXIETH1G_IS, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_IS_OFFSET
    },
    {
        .name = "intp", .id = eAXIETH1G_IP, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_IP_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO, 
    },
    {
        .name = "inte", .id = eAXIETH1G_IE, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_IE_OFFSET
    },
    {
        .name = "ttag", .id = eAXIETH1G_TTAG, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_TTAG_OFFSET
    },
    {
        .name = "rtag", .id = eAXIETH1G_RTAG, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_RTAG_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_WO 
    },
    {
        .name = "uawl", .id = eAXIETH1G_UAWL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_UAWL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "uawu", .id = eAXIETH1G_UAWU, .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_UAWU_OFFSET
    },
    {
        .name = "tpid0", .id = eAXIETH1G_TPID0, .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_TPID0_OFFSET
    },
    {
        .name = "tpid1", .id = eAXIETH1G_TPID1, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_TPID1_OFFSET
    },
    {
        .name = "rx_bytes_cnt", .id = eAXIETH1G_RXBL, .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_RXBL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_bytes_cnt", .id = eAXIETH1G_TXBL, .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_TXBL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_undersized_cnt", .id = eAXIETH1G_RXUNDRL, .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_RXUNDRL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_frag_cnt", .id = eAXIETH1G_RXFRAGL, .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_RXFRAGL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_64_bytes_cnt", .id = eAXIETH1G_RX64BL, .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_RX64BL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_65_127_cnt", .id = eAXIETH1G_RX65B127L, .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_RX65B127L_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_128_255_cnt", .id = eAXIETH1G_RX128B255L, .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_RX128B255L_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_256_511_cnt", .id = eAXIETH1G_RX256B511L, .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_RX256B511L_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_512_1023_cnt", .id = eAXIETH1G_RX512B1023L, .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_RX512B1023L_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_1024_max_cnt", .id = eAXIETH1G_RX1024BL, .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_RX1024BL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_oversize_cnt", .id = eAXIETH1G_RXOVRL, .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_RXOVRL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_64_bytes_cnt", .id = eAXIETH1G_TX64BL, .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_TX64BL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_65_127_cnt", .id = eAXIETH1G_TX65B127L, .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_TX65B127L_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_128_255_cnt", .id = eAXIETH1G_TX128B255L, .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_TX128B255L_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_256_511_cnt", .id = eAXIETH1G_TX256B511L, .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_TX256B511L_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_512_1023_cnt", .id = eAXIETH1G_TX512B1023L, .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_TX512B1023L_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_1024_max_cnt", .id = eAXIETH1G_TX1024L, .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_TX1024L_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_oversize_cnt", .id = eAXIETH1G_TXOVRL, .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_TXOVRL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_good_packet", .id = eAXIETH1G_RXFL, .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_RXFL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_fcs_err", .id = eAXIETH1G_RXFCSERL, .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_RXFCSERL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_bcast", .id = eAXIETH1G_RXBCSTFL, .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_RXBCSTFL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_mcast", .id = eAXIETH1G_RXMCSTFL, .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_RXMCSTFL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_ctrl", .id = eAXIETH1G_RXCTRFL, .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_RXCTRFL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_len_err", .id = eAXIETH1G_RXLTERL, .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_RXLTERL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_vlan_tagged", .id = eAXIETH1G_RXVLANFL, .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_RXVLANFL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_pause", .id = eAXIETH1G_RXPFL, .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_RXPFL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_ctrl_unsup_opcode", .id = eAXIETH1G_RXUOPFL, .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_RXUOPFL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_good_packet", .id = eAXIETH1G_TXFL, .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_TXFL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_bcast", .id = eAXIETH1G_TXBCSTFL, .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_TXBCSTFL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_mcast", .id = eAXIETH1G_TXMCSTFL, .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_TXMCSTFL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_underrun_err", .id = eAXIETH1G_TXUNDRERL, .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_TXUNDRERL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_ctrl", .id = eAXIETH1G_TXCTRFL, .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_TXCTRFL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_vlan_tagged", .id = eAXIETH1G_TXVLANFL, .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_TXVLANFL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_pause", .id = eAXIETH1G_TXPFL, .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_TXPFL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_single_collision", .id = eAXIETH1G_TXSCL, .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_TXSCL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_multi_collision", .id = eAXIETH1G_TXMCL, .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_TXMCL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_deferred", .id = eAXIETH1G_TXDEF, .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_TXDEFL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_late_collision", .id = eAXIETH1G_TXLTCL, .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_TXLTCL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_aborted_ex_defferal", .id = eAXIETH1G_TXAECL, .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_TXAECL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_ex_defferal", .id = eAXIETH1G_TXEDEFL, .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_TXEDEFL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_alignment", .id = eAXIETH1G_RXAERL, .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_RXAERL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_config_word0", .id = eAXIETH1G_RCW0, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_RCW0_OFFSET
    },
    {
        .name = "rx_config_word1", .id = eAXIETH1G_RCW1, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_RCW1_OFFSET
    },
    {
        .name = "tx_config", .id = eAXIETH1G_TC, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_TC_OFFSET
    },
    {
        .name = "fc_config", .id = eAXIETH1G_FCC, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_FCC_OFFSET
    },
    {
        .name = "emac_mod_config", .id = eAXIETH1G_EMMC, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_EMMC_OFFSET
    },
    {
        .name = "rx_max_frame_config", .id = eAXIETH1G_RXFC, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_RXFC_OFFSET
    },
    {
        .name = "tx_max_frame_config", .id = eAXIETH1G_TXFC, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_TXFC_OFFSET
    },
    {
        .name = "tx_timestamp_adjust", .id = eAXIETH1G_TX_TIMESTAMP_ADJ, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_TX_TIMESTAMP_ADJ_OFFSET
    },
    {
        .name = "rgmii_config", .id = eAXIETH1G_PHYC, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_PHYC_OFFSET
    },
    {
        .name = "identification", .id = eAXIETH1G_IDREG, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_IDREG_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "ability", .id = eAXIETH1G_ARREG, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_ARREG_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "mii_config", .id = eAXIETH1G_MDIO_MC, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_MDIO_MC_OFFSET
    },
    {
        .name = "mii_ctrl", .id = eAXIETH1G_MDIO_MCR, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_MDIO_MCR_OFFSET
    },
    {
        .name = "mii_write_data", .id = eAXIETH1G_MDIO_MWD, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_MDIO_MWD_OFFSET
    },
    {
        .name = "mii_read_data", .id = eAXIETH1G_MDIO_MRD, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_MDIO_MRD_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "mii_ints", .id = eAXIETH1G_MDIO_MIS, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_MDIO_MIS_OFFSET
    },
    {
        .name = "mii_intp", .id = eAXIETH1G_MDIO_MIP, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_MDIO_MIP_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "mii_inte", .id = eAXIETH1G_MDIO_MIE, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_MDIO_MIE_OFFSET
    },
    {
        .name = "mii_intc", .id = eAXIETH1G_MDIO_MIC, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_MDIO_MIC_OFFSET
    },
    {
        .name = "ucast_address_word0", .id = eAXIETH1G_UAW0, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_UAW0_OFFSET
    },
    {
        .name = "ucast_address_word1", .id = eAXIETH1G_UAW1, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_UAW1_OFFSET
    },
    {
        .name = "frame_filter_ctrl", .id = eAXIETH1G_FMFC, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_FMFC_OFFSET
    },
    {
        .name = "frame_filter_value", .id = eAXIETH1G_FMFV, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_FMFV_OFFSET
    },
    {
        .name = "frame_filter_mask_value", .id = eAXIETH1G_FMFMV, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_FMFMV_OFFSET
    },
    {
        .name = "tx_vlan_data", .id = eAXIETH1G_TX_VLAN_DATA, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_TX_VLAN_DATA_OFFSET
    },
    {
        .name = "rx_vlan_data", .id = eAXIETH1G_RX_VLAN_DATA, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_RX_VLAN_DATA_OFFSET
    },
    {
        .name = "eth_avb", .id = eAXIETH1G_ETH_AVB, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_ETH_AVB_OFFSET
    },
    {
        .name = "mcast_addr_table", .id = eAXIETH1G_MCAST_TABLE, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIETH1G_MCAST_TABLE_OFFSET
    },
};

/**
 * _axieth1g_start starts the Axi Ethernet device as follows:
 *   - Enable transmitter if AXIETH1G_TRANSMIT_ENABLE_OPTION is
 *     set
 *	- Enable receiver if AXIETH1G_RECEIVER_ENABLE_OPTION is set
 *
 * @param[in] p_axieth1g is a pointer to axieth1g_ctx_t instance to 
 *       be worked on
 * 
 * @return error code
 */
ccl_err_t axieth1g_start(axieth1g_ctx_t *p_axieth1g)
{
    b_u32 regval;

    if ( !p_axieth1g ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIETH1G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* Enable transmitter if not already enabled */
    PDEBUG( "enabling transmitter\n");
    AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_TC_OFFSET, 
                      sizeof(b_u32), 0, 
                      (char *)&regval, sizeof(b_u32));
    if (!(regval & AXIETH1G_TC_TX_MASK)) {
        PDEBUG("transmitter not enabled, enabling now\n");
        regval |= AXIETH1G_TC_TX_MASK;
        AXIETH1G_REG_WRITE(p_axieth1g, AXIETH1G_TC_OFFSET, 
                           sizeof(b_u32), 0, 
                           (char *)&regval, sizeof(b_u32));
    }
    PDEBUG("transmitter enabled\n");

    /* Enable receiver */
    PDEBUG("enabling receiver\n");
    AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_RCW1_OFFSET, 
                      sizeof(b_u32), 0, 
                      (char *)&regval, sizeof(b_u32));
    if (!(regval & AXIETH1G_RCW1_RX_MASK)) {
        PDEBUG("receiver not enabled, enabling now\n");
        regval |= AXIETH1G_RCW1_RX_MASK;
        AXIETH1G_REG_WRITE(p_axieth1g, AXIETH1G_RCW1_OFFSET, 
                           sizeof(b_u32), 0, 
                           (char *)&regval, sizeof(b_u32));
    }
    PDEBUG( "receiver enabled\n");

    return CCL_OK;
}

/**
 * _axieth1g_stop gracefully stops the Axi Ethernet device as 
 * follows: 
 *	- Disable all interrupts from this device
 *	- Disable the receiver
 *
 *  _axieth1g_stop does not modify any of the current device
 *  options.
 *
 * Since the transmitter is not disabled, frames currently in internal buffers
 * or in process by a DMA engine are allowed to be transmitted.
 *
 * @param[in] p_axieth1g is a pointer to axieth1g_ctx_t instance to 
 *       be worked on
 * 
 * @return	error code
 */
ccl_err_t axieth1g_stop(axieth1g_ctx_t *p_axieth1g)
{
    b_u32 regval;

    if ( !p_axieth1g ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIETH1G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* Disable interrupts */
    regval = 0;
    AXIETH1G_REG_WRITE(p_axieth1g, AXIETH1G_IE_OFFSET, 
                       sizeof(b_u32), 0, 
                       (char *)&regval, sizeof(b_u32));

    /* Disable the receiver */
    AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_RCW1_OFFSET, 
                      sizeof(b_u32), 0, 
                      (char *)&regval, sizeof(b_u32));
    regval &= ~AXIETH1G_RCW1_RX_MASK;
    AXIETH1G_REG_WRITE(p_axieth1g, AXIETH1G_RCW1_OFFSET, 
                       sizeof(b_u32), 0, 
                       (char *)&regval, sizeof(b_u32));

    /*
     * Stopping the receiver in mid-packet causes a dropped packet
     * indication from HW. Clear it.
     */
    /* get the interrupt pending register */
    AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_IP_OFFSET, 
                      sizeof(b_u32), 0, 
                      (char *)&regval, sizeof(b_u32));
    if (regval & AXIETH1G_INT_RXRJECT_MASK) {
        /* set the interrupt status register to clear the interrupt */
        regval = AXIETH1G_INT_RXRJECT_MASK;
        AXIETH1G_REG_WRITE(p_axieth1g, AXIETH1G_IS_OFFSET, 
                           sizeof(b_u32), 0, 
                           (char *)&regval, sizeof(b_u32));
    }

    return CCL_OK;
}

/**
 * _axieth1g_update_depops check and update dependent options 
 * for new/extended features. This is a helper function that is 
 * meant to be called by _axieth1g_set_options() and 
 * _axieth1g_clear_options(). 
 *
 * @param[in] p_axieth1g is a pointer to the Axi Ethernet 
 *      instance to be worked on.
 *
 * @return	Dependent options that are required to set/clear per
 *		hardware requirement.
 *
 * @note
 *
 * This helper function collects the dependent OPTION(s) per hardware design.
 * When conflicts arises, extended features have precedence over legacy ones.
 * Two operations to be considered,
 * 1. Adding extended options. If XATE_VLAN_OPTION is enabled and enable one of
 *	extended VLAN options, XATE_VLAN_OPTION should be off and configure to
 *	hardware.
 *	However, axi-ethernet instance options variable still holds
 *	XATE_VLAN_OPTION so when all of the extended feature are removed,
 *	XATE_VLAN_OPTION can be effective and configured to hardware.
 * 2. Removing extended options. Remove extended option can not just remove
 *	the selected extended option and dependent options. All extended
 *	options need to be verified and remained when one or more extended
 *	options are enabled.
 *
 * Dependent options are :
 *	- AXIETH1G_VLAN_OPTION,
 *	- AXIETH1G_JUMBO_OPTION
 *	- AXIETH1G_FCS_INSERT_OPTION,
 *	- AXIETH1G_FCS_STRIP_OPTION
 *	- AXIETH1G_PROMISC_OPTION.
 */
static b_u32 _axieth1g_update_depops(axieth1g_ctx_t *p_axieth1g)
{
    if ( !p_axieth1g ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIETH1G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    b_u32 depops = p_axieth1g->options;

    /*
     * The extended/new features require some OPTIONS to be on/off per
     * hardware design. We determine these extended/new functions here
     * first and also on/off other OPTIONS later. So that dependent
     * OPTIONS are in sync and _[Set|Clear]options() can be performed
     * seamlessly.
     */

    /* Enable extended multicast option */
    if (depops & AXIETH1G_EXT_MULTICAST_OPTION) {
        /*
         * When extended multicast module is enabled in HW,
         * AXIETH1G_PROMISC_OPTION is required to be enabled.
         */
        if (AXIETH1G_IS_EXT_MCAST(p_axieth1g)) {
            depops |= AXIETH1G_PROMISC_OPTION;
            PDEBUG("Checkdepops: enabling ext multicast\n");
        } else {
            PDEBUG("EXT MULTICAST is not built in hardware\n");
        }
    }

    /* Enable extended transmit VLAN translation option */
    if (depops & AXIETH1G_EXT_TXVLAN_TRAN_OPTION) {
        /*
         * Check if hardware is built with extend TX VLAN translation.
         * if not, output an information message.
         */
        if (AXIETH1G_IS_TX_VLAN_TRAN(p_axieth1g)) {
            depops |= AXIETH1G_FCS_INSERT_OPTION;
            depops &= ~AXIETH1G_VLAN_OPTION;
            PDEBUG("Checkdepops: enabling ext Tx VLAN trans\n");
        } else {
            PDEBUG("TX VLAN TRANSLATION is not built in hardware\n");
        }
    }

    /* Enable extended receive VLAN translation option */
    if (depops & AXIETH1G_EXT_RXVLAN_TRAN_OPTION) {
        /*
         * Check if hardware is built with extend RX VLAN translation.
         * if not, output an information message.
         */
        if (AXIETH1G_IS_RX_VLAN_TRAN(p_axieth1g)) {
            depops |= AXIETH1G_FCS_STRIP_OPTION;
            depops &= ~AXIETH1G_VLAN_OPTION;
            PDEBUG("Checkdepops: enabling ext Rx VLAN trans\n");
        } else {
            PDEBUG("RX VLAN TRANSLATION is not built in hardware\n");
        }
    }

    /* Enable extended transmit VLAN tag option */
    if (depops & AXIETH1G_EXT_TXVLAN_TAG_OPTION) {
        /*
         * Check if hardware is built with extend TX VLAN tagging.
         * if not, output an information message.
         */
        if (AXIETH1G_IS_TX_VLAN_TAG(p_axieth1g)) {
            depops |= AXIETH1G_FCS_INSERT_OPTION;
            depops &= ~AXIETH1G_VLAN_OPTION;
            depops |= AXIETH1G_JUMBO_OPTION;
            PDEBUG("Checkdepops: enabling ext Tx VLAN tag\n");
        } else {
            PDEBUG("TX VLAN TAG is not built in hardware\n");
        }
    }

    /* Enable extended receive VLAN tag option */
    if (depops & AXIETH1G_EXT_RXVLAN_TAG_OPTION) {
        /*
         * Check if hardware is built with extend RX VLAN tagging.
         * if not, output an information message.
         */
        if (AXIETH1G_IS_RX_VLAN_TAG(p_axieth1g)) {
            depops |= AXIETH1G_FCS_STRIP_OPTION;
            depops &= ~AXIETH1G_VLAN_OPTION;
            depops |= AXIETH1G_JUMBO_OPTION;
            PDEBUG("Checkdepops: enabling ext Rx VLAN tag\n");
        } else {
            PDEBUG("RX VLAN TAG is not built in hardware\n");
        }
    }

    /* Enable extended transmit VLAN strip option */
    if (depops & AXIETH1G_EXT_TXVLAN_STRP_OPTION) {
        /*
         * Check if hardware is built with extend TX VLAN stripping.
         * if not, output an information message.
         */
        if (AXIETH1G_IS_TX_VLAN_STRP(p_axieth1g)) {
            depops |= AXIETH1G_FCS_INSERT_OPTION;
            depops &= ~AXIETH1G_VLAN_OPTION;
            depops |= AXIETH1G_JUMBO_OPTION;
            PDEBUG("Checkdepops: enabling ext Tx VLAN strip\n");
        } else {
            PDEBUG("TX VLAN STRIP is not built in hardware\n");
        }
    }

    /* Enable extended receive VLAN strip option */
    if (depops & AXIETH1G_EXT_RXVLAN_STRP_OPTION) {
        /*
         * Check if hardware is built with extend RX VLAN stripping.
         * if not, output an information message.
         */
        if (AXIETH1G_IS_RX_VLAN_STRP(p_axieth1g)) {
            depops |= AXIETH1G_FCS_STRIP_OPTION;
            depops &= ~AXIETH1G_VLAN_OPTION;
            depops |= AXIETH1G_JUMBO_OPTION;
            PDEBUG("Checkdepops: enabling ext Rx VLAN strip\n");
        } else {
            PDEBUG("RX VLAN STRIP is not built in hardware\n");
        }
    }

    /*
     * options and dependent options together is what hardware and user
     * are happy with. But we still need to keep original options
     * in case option(s) are set/cleared, overall options can be managed.
     * Return depops to AXIETH1G_[Set|Clear]options for final
     * configuration.
     */
    return depops;
}

/**
 * _axieth1g_set_options enables the options for 
 *  the Axi Ethernet, specified by p_axieth1g. Axi
 * Ethernet should be stopped with _axieth1g_stop() before 
 * changing options. 
 *
 * @param[in] p_axieth1g is a pointer to axieth1g_ctx_t instance to 
 *       be worked on
 * @param[in] options is a bitmask of OR'd values for options to 
 *      set. Options not specified are not affected.
 *
 * @return error code
 */
static ccl_err_t _axieth1g_set_options(axieth1g_ctx_t *p_axieth1g, b_u32 options)
{
    b_u32 regval;   /* Generic register contents */
    b_u32 rcw1; /* Reflects original contents of RCW1 */
    b_u32 tc;   /* Reflects original contents of TC  */
    b_u32 newrcw1;  /* Reflects new contents of RCW1 */
    b_u32 newtc;    /* Reflects new contents of TC  */
    b_u32 depops;   /* Required dependent options for new features */

    if ( !p_axieth1g ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIETH1G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Set options word to its new value.
     * The step is required before calling _Updatedepops() since
     * we are operating on updated options.
     */
    p_axieth1g->options |= options;

    /*
     * There are options required to be on/off per hardware requirement.
     * Invoke _Updatedepops to check hardware availability and update
     * options accordingly.
     */
    depops = _axieth1g_update_depops(p_axieth1g);

    /*
     * New/extended function bit should be on if any new/extended features
     * are on and hardware is built with them.
     */
    if (depops & (AXIETH1G_EXT_MULTICAST_OPTION   |
                  AXIETH1G_EXT_TXVLAN_TRAN_OPTION |
                  AXIETH1G_EXT_RXVLAN_TRAN_OPTION |
                  AXIETH1G_EXT_TXVLAN_TAG_OPTION  |
                  AXIETH1G_EXT_RXVLAN_TAG_OPTION  |
                  AXIETH1G_EXT_TXVLAN_STRP_OPTION |
                  AXIETH1G_EXT_RXVLAN_STRP_OPTION)) {

        AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_RAF_OFFSET, 
                          sizeof(b_u32), 0, 
                          (char *)&regval, sizeof(b_u32));
        regval |= AXIETH1G_RAF_NEWFNCENBL_MASK;
        AXIETH1G_REG_WRITE(p_axieth1g, AXIETH1G_RAF_OFFSET, 
                           sizeof(b_u32), 0, 
                           (char *)&regval, sizeof(b_u32));
    }

    /*
     * Many of these options will change the RCW1 or TC registers.
     * To reduce the amount of IO to the device, group these options here
     * and change them all at once.
     */
    /* Get current register contents */
    AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_RCW1_OFFSET, 
                      sizeof(b_u32), 0, 
                      (char *)&rcw1, sizeof(b_u32));
    AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_TC_OFFSET, 
                      sizeof(b_u32), 0, 
                      (char *)&tc, sizeof(b_u32));

    newrcw1 = rcw1;
    newtc = tc;

    PDEBUG("current control regs: RCW1: 0x%0x; TC: 0x%0x\n",rcw1, tc);
    PDEBUG("options: 0x%0x; default options: 0x%0x\n",options, AXIETH1G_DEFAULT_OPTIONS);

    /* Turn on jumbo packet support for both Rx and Tx */
    if (depops & AXIETH1G_JUMBO_OPTION) {
        newtc |= AXIETH1G_TC_JUM_MASK;
        newrcw1 |= AXIETH1G_RCW1_JUM_MASK;
    }

    /* Turn on VLAN packet support for both Rx and Tx */
    if (depops & AXIETH1G_VLAN_OPTION) {
        newtc |= AXIETH1G_TC_VLAN_MASK;
        newrcw1 |= AXIETH1G_RCW1_VLAN_MASK;
    }

    /* Turn on FCS stripping on receive packets */
    if (depops & AXIETH1G_FCS_STRIP_OPTION) {
        PDEBUG("setoptions: enabling fcs stripping\n");
        newrcw1 &= ~AXIETH1G_RCW1_FCS_MASK;
    }

    /* Turn on FCS insertion on transmit packets */
    if (depops & AXIETH1G_FCS_INSERT_OPTION) {
        PDEBUG("setoptions: enabling fcs insertion\n");
        newtc &= ~AXIETH1G_TC_FCS_MASK;
    }

    /* Turn on length/type field checking on receive packets */
    if (depops & AXIETH1G_LENTYPE_ERR_OPTION) {
        newrcw1 &= ~AXIETH1G_RCW1_LT_DIS_MASK;
    }

    /* Enable transmitter */
    if (depops & AXIETH1G_TRANSMITTER_ENABLE_OPTION) {
        newtc |= AXIETH1G_TC_TX_MASK;
    }

    /* Enable receiver */
    if (depops & AXIETH1G_RECEIVER_ENABLE_OPTION) {
        newrcw1 |= AXIETH1G_RCW1_RX_MASK;
    }

    /* Change the TC or RCW1 registers if they need to be modified */
    if (tc != newtc) {
        PDEBUG("setoptions: writing tc: 0x%0x\n", newtc);
        AXIETH1G_REG_WRITE(p_axieth1g, AXIETH1G_TC_OFFSET, 
                           sizeof(b_u32), 0, 
                           (char *)&newtc, sizeof(b_u32));
    }

    if (rcw1 != newrcw1) {
        PDEBUG("setoptions: writing rcw1: 0x%0x\n", newrcw1);
        AXIETH1G_REG_WRITE(p_axieth1g, AXIETH1G_RCW1_OFFSET, 
                           sizeof(b_u32), 0, 
                           (char *)&newrcw1, sizeof(b_u32));
    }

    /*
     * Rest of options twiddle bits of other registers. Handle them one at
     * a time
     */

    /* Turn on flow control */
    if (depops & AXIETH1G_FLOW_CONTROL_OPTION) {
        PDEBUG("setoptions: enabling flow control\n");
        AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_FCC_OFFSET, 
                          sizeof(b_u32), 0, 
                          (char *)&regval, sizeof(b_u32));
        regval |= AXIETH1G_FCC_FCRX_MASK;
        AXIETH1G_REG_WRITE(p_axieth1g, AXIETH1G_FCC_OFFSET, 
                           sizeof(b_u32), 0, 
                           (char *)&regval, sizeof(b_u32));
    }

    AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_RCW1_OFFSET, 
                      sizeof(b_u32), 0, 
                      (char *)&regval, sizeof(b_u32));
    PDEBUG( "setoptions: rcw1 is now (fcc):0x%0x\n", regval);

    /* Turn on promiscuous frame filtering (all frames are received ) */
    if (depops & AXIETH1G_PROMISC_OPTION) {
        PDEBUG("setoptions: enabling promiscuous mode\n");
        AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_FMFC_OFFSET, 
                          sizeof(b_u32), 0, 
                          (char *)&regval, sizeof(b_u32));
        regval |= AXIETH1G_FMI_PM_MASK;
        AXIETH1G_REG_WRITE(p_axieth1g, AXIETH1G_FMFC_OFFSET, 
                           sizeof(b_u32), 0, 
                           (char *)&regval, sizeof(b_u32));
    }

    AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_RCW1_OFFSET, 
                      sizeof(b_u32), 0, 
                      (char *)&regval, sizeof(b_u32));
    PDEBUG("setoptions: rcw1 is now (afm):0x%0x\n", regval);

    /* Allow broadcast address filtering */
    if (depops & AXIETH1G_BROADCAST_OPTION) {
        AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_RAF_OFFSET, 
                          sizeof(b_u32), 0, 
                          (char *)&regval, sizeof(b_u32));
        regval &= ~AXIETH1G_RAF_BCSTREJ_MASK;
        AXIETH1G_REG_WRITE(p_axieth1g, AXIETH1G_RAF_OFFSET, 
                           sizeof(b_u32), 0, 
                           (char *)&regval, sizeof(b_u32));
    }

    AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_RCW1_OFFSET, 
                      sizeof(b_u32), 0, 
                      (char *)&regval, sizeof(b_u32));

    PDEBUG("setoptions: rcw1 is now (raf):0x%0x\n", regval);

    /* Allow multicast address filtering */
    if (depops & (AXIETH1G_MULTICAST_OPTION | AXIETH1G_EXT_MULTICAST_OPTION)) {
        AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_RAF_OFFSET, 
                          sizeof(b_u32), 0, 
                          (char *)&regval, sizeof(b_u32));
        regval &= ~AXIETH1G_RAF_MCSTREJ_MASK;
        AXIETH1G_REG_WRITE(p_axieth1g, AXIETH1G_RAF_OFFSET, 
                           sizeof(b_u32), 0, 
                           (char *)&regval, sizeof(b_u32));
    }

    AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_RCW1_OFFSET, 
                      sizeof(b_u32), 0, 
                      (char *)&regval, sizeof(b_u32));
    PDEBUG("setoptions: rcw1 is now (raf2): 0x%0x\n", regval);

    /*
     * The remaining options not handled here are managed elsewhere in the
     * driver. No register modifications are needed at this time.
     * Reflecting the option in p_axieth1g->options is good enough for
     * now.
     */
    /* Enable extended multicast option */
    if (depops & AXIETH1G_EXT_MULTICAST_OPTION) {
        AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_RAF_OFFSET, 
                          sizeof(b_u32), 0, 
                          (char *)&regval, sizeof(b_u32));
        regval |= AXIETH1G_RAF_EMULTIFLTRENBL_MASK;
        AXIETH1G_REG_WRITE(p_axieth1g, AXIETH1G_RAF_OFFSET, 
                           sizeof(b_u32), 0, 
                           (char *)&regval, sizeof(b_u32));
    }

    /*
     * New/extended [TX|RX] VLAN translation option does not have specific
     * bits to on/off.
     */

    /* Enable extended transmit VLAN tag option */
    if (depops & AXIETH1G_EXT_TXVLAN_TAG_OPTION) {
        AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_RAF_OFFSET, 
                          sizeof(b_u32), 0, 
                          (char *)&regval, sizeof(b_u32));
        regval = (regval & ~AXIETH1G_RAF_TXVTAGMODE_MASK) |
                 (AXIETH1G_DEFAULT_TXVTAG_MODE <<
                  AXIETH1G_RAF_TXVTAGMODE_SHIFT);
        AXIETH1G_REG_WRITE(p_axieth1g, AXIETH1G_RAF_OFFSET, 
                           sizeof(b_u32), 0, 
                           (char *)&regval, sizeof(b_u32));
    }

    /* Enable extended receive VLAN tag option */
    if (depops & AXIETH1G_EXT_RXVLAN_TAG_OPTION) {
        AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_RAF_OFFSET, 
                          sizeof(b_u32), 0, 
                          (char *)&regval, sizeof(b_u32));
        regval = (regval & ~AXIETH1G_RAF_RXVTAGMODE_MASK) |
                 (AXIETH1G_DEFAULT_RXVTAG_MODE <<
                  AXIETH1G_RAF_RXVTAGMODE_SHIFT);
        AXIETH1G_REG_WRITE(p_axieth1g, AXIETH1G_RAF_OFFSET, 
                           sizeof(b_u32), 0, 
                           (char *)&regval, sizeof(b_u32));
    }

    /* Enable extended transmit VLAN strip option */
    if (options & AXIETH1G_EXT_TXVLAN_STRP_OPTION) {
        AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_RAF_OFFSET, 
                          sizeof(b_u32), 0, 
                          (char *)&regval, sizeof(b_u32));
        regval = (regval & ~AXIETH1G_RAF_TXVSTRPMODE_MASK) | 
                 (AXIETH1G_DEFAULT_TXVSTRP_MODE << 
                  AXIETH1G_RAF_TXVSTRPMODE_SHIFT);
        AXIETH1G_REG_WRITE(p_axieth1g, AXIETH1G_RAF_OFFSET, 
                           sizeof(b_u32), 0, 
                           (char *)&regval, sizeof(b_u32));
    }

    /* Enable extended receive VLAN strip option */
    if (options & AXIETH1G_EXT_RXVLAN_STRP_OPTION) {
        AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_RAF_OFFSET, 
                          sizeof(b_u32), 0, 
                          (char *)&regval, sizeof(b_u32));
        regval = (regval & ~AXIETH1G_RAF_RXVSTRPMODE_MASK) |
                 (AXIETH1G_DEFAULT_RXVSTRP_MODE <<
                  AXIETH1G_RAF_RXVSTRPMODE_SHIFT);
        AXIETH1G_REG_WRITE(p_axieth1g, AXIETH1G_RAF_OFFSET, 
                           sizeof(b_u32), 0, 
                           (char *)&regval, sizeof(b_u32));
    }



    /*
     * The remaining options not handled here are managed elsewhere in the
     * driver. No register modifications are needed at this time.
     * Reflecting the option in p_axieth1g->options is good enough for
     * now.
     */
    PDEBUG("setoptions: returning SUCCESS\n");
    return CCL_OK;
}

/**
 * _axieth1g_clear_options clears the options, <i>options</i> for 
 * the Axi Ethernet, specified by <i>p_axieth1g</i>. Axi Ethernet
 * should be stopped with _axieth1g_stop() before changing 
 * options. 
 *
 * @param[in] p_axieth1g is a pointer to axieth1g_ctx_t instance to 
 *       be worked on
 * @param[in] options is a bitmask of OR'd AXIETH1G_*_OPTION 
 *      values for options to clear. options not specified are
 *      not affected.
 *
 * @return error code
 */
static ccl_err_t _axieth1g_clear_options(axieth1g_ctx_t *p_axieth1g, b_u32 options)
{
    b_u32 regval;   /* Generic */
    b_u32 rcw1; /* Reflects original contents of RCW1 */
    b_u32 tc;   /* Reflects original contents of TC  */
    b_u32 newrcw1;  /* Reflects new contents of RCW1 */
    b_u32 newtc;    /* Reflects new contents of TC  */
    b_u32 depops;   /* Required dependent options for new features */

    if ( !p_axieth1g ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIETH1G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Set options word to its new value.
     * The step is required before calling _Updatedepops() since
     * we are operating on updated options.
     */
    p_axieth1g->options &= ~options;

    /*
     * There are options required to be on/off per hardware requirement.
     * Invoke _Updatedepops to check hardware availability and update
     * options accordingly.
     */
    depops = ~(_axieth1g_update_depops(p_axieth1g));

    /*
     * New/extended function bit should be off if none of new/extended
     * features is on after hardware is validated and gone through
     * _Updatedepops().
     */
    if (!(~(depops) &  (AXIETH1G_EXT_MULTICAST_OPTION   |
                        AXIETH1G_EXT_TXVLAN_TRAN_OPTION |
                        AXIETH1G_EXT_RXVLAN_TRAN_OPTION |
                        AXIETH1G_EXT_TXVLAN_TAG_OPTION  |
                        AXIETH1G_EXT_RXVLAN_TAG_OPTION  |
                        AXIETH1G_EXT_TXVLAN_STRP_OPTION |
                        AXIETH1G_EXT_RXVLAN_STRP_OPTION))) {
        AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_RAF_OFFSET, 
                          sizeof(b_u32), 0, 
                          (char *)&regval, sizeof(b_u32));
        regval &= ~AXIETH1G_RAF_NEWFNCENBL_MASK;
        AXIETH1G_REG_WRITE(p_axieth1g, AXIETH1G_RAF_OFFSET, 
                           sizeof(b_u32), 0, 
                           (char *)&regval, sizeof(b_u32));
    }

    /*
     * Many of these options will change the RCW1 or TC registers.
     * Group these options here and change them all at once. What we are
     * trying to accomplish is to reduce the amount of IO to the device
     */

    /* Get the current register contents */
    AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_RCW1_OFFSET, 
                      sizeof(b_u32), 0, 
                      (char *)&rcw1, sizeof(b_u32));
    AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_TC_OFFSET, 
                      sizeof(b_u32), 0, 
                      (char *)&tc, sizeof(b_u32));
    newrcw1 = rcw1;
    newtc = tc;

    /* Turn off jumbo packet support for both Rx and Tx */
    if (depops & AXIETH1G_JUMBO_OPTION) {
        PDEBUG("XAxiEthernet_Clearoptions: disabling jumbo\n");
        newtc &= ~AXIETH1G_TC_JUM_MASK;
        newrcw1 &= ~AXIETH1G_RCW1_JUM_MASK;
    }

    /* Turn off VLAN packet support for both Rx and Tx */
    if (depops & AXIETH1G_VLAN_OPTION) {
        PDEBUG("XAxiEthernet_Clearoptions: disabling vlan\n");
        newtc &= ~AXIETH1G_TC_VLAN_MASK;
        newrcw1 &= ~AXIETH1G_RCW1_VLAN_MASK;
    }

    /* Turn off FCS stripping on receive packets */
    if (depops & AXIETH1G_FCS_STRIP_OPTION) {
        PDEBUG("XAxiEthernet_Clearoptions: disabling fcs strip\n");
        newrcw1 |= AXIETH1G_RCW1_FCS_MASK;
    }

    /* Turn off FCS insertion on transmit packets */
    if (depops & AXIETH1G_FCS_INSERT_OPTION) {
        PDEBUG("XAxiEthernet_Clearoptions: disabling fcs insert\n");
        newtc |= AXIETH1G_TC_FCS_MASK;
    }

    /* Turn off length/type field checking on receive packets */
    if (depops & AXIETH1G_LENTYPE_ERR_OPTION) {
        PDEBUG("XAxiEthernet_Clearoptions: disabling lentype err\n");
        newrcw1 |= AXIETH1G_RCW1_LT_DIS_MASK;
    }

    /* Disable transmitter */
    if (depops & AXIETH1G_TRANSMITTER_ENABLE_OPTION) {
        PDEBUG("XAxiEthernet_Clearoptions: disabling transmitter\n");
        newtc &= ~AXIETH1G_TC_TX_MASK;
    }

    /* Disable receiver */
    if (depops & AXIETH1G_RECEIVER_ENABLE_OPTION) {
        PDEBUG("XAxiEthernet_Clearoptions: disabling receiver\n");
        newrcw1 &= ~AXIETH1G_RCW1_RX_MASK;
    }

    /* Change the TC and RCW1 registers if they need to be
     * modified
     */
    if (tc != newtc) {
        PDEBUG("XAxiEthernet_Clearoptions: setting TC: 0x%0x\n", newtc);
        AXIETH1G_REG_WRITE(p_axieth1g, AXIETH1G_TC_OFFSET, 
                           sizeof(b_u32), 0, 
                           (char *)&newtc, sizeof(b_u32));
    }

    if (rcw1 != newrcw1) {
        PDEBUG("XAxiEthernet_Clearoptions: setting RCW1: 0x%0x\n",newrcw1);
        AXIETH1G_REG_WRITE(p_axieth1g, AXIETH1G_RCW1_OFFSET, 
                           sizeof(b_u32), 0, 
                           (char *)&newrcw1, sizeof(b_u32));
    }

    /*
     * Rest of options twiddle bits of other registers. Handle them one at
     * a time
     */

    /* Turn off flow control */
    if (depops & AXIETH1G_FLOW_CONTROL_OPTION) {
        PDEBUG("XAxiEthernet_Clearoptions: disabling flow control\n");
        AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_FCC_OFFSET, 
                          sizeof(b_u32), 0, 
                          (char *)&regval, sizeof(b_u32));
        regval &= ~AXIETH1G_FCC_FCRX_MASK;
        AXIETH1G_REG_WRITE(p_axieth1g, AXIETH1G_FCC_OFFSET, 
                           sizeof(b_u32), 0, 
                           (char *)&regval, sizeof(b_u32));
    }

    /* Turn off promiscuous frame filtering */
    if (depops & AXIETH1G_PROMISC_OPTION) {
        PDEBUG("XAxiEthernet_Clearoptions: disabling promiscuous mode\n");
        AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_FMFC_OFFSET, 
                          sizeof(b_u32), 0, 
                          (char *)&regval, sizeof(b_u32));
        regval &= ~AXIETH1G_FMI_PM_MASK;
        AXIETH1G_REG_WRITE(p_axieth1g, AXIETH1G_FMFC_OFFSET, 
                           sizeof(b_u32), 0, 
                           (char *)&regval, sizeof(b_u32));
    }

    /* Disable broadcast address filtering */
    if (depops & AXIETH1G_BROADCAST_OPTION) {
        PDEBUG("XAxiEthernet_Clearoptions: disabling broadcast mode\n");
        AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_RAF_OFFSET, 
                          sizeof(b_u32), 0, 
                          (char *)&regval, sizeof(b_u32));
        regval |= AXIETH1G_RAF_BCSTREJ_MASK;
        AXIETH1G_REG_WRITE(p_axieth1g, AXIETH1G_RAF_OFFSET, 
                           sizeof(b_u32), 0, 
                           (char *)&regval, sizeof(b_u32));
    }

    /* Disable multicast address filtering */
    if ((depops & AXIETH1G_MULTICAST_OPTION) &&
        (depops & AXIETH1G_EXT_MULTICAST_OPTION)) {
        PDEBUG("XAxiEthernet_Clearoptions:disabling multicast mode\n");
        AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_RAF_OFFSET, 
                          sizeof(b_u32), 0, 
                          (char *)&regval, sizeof(b_u32));
        regval |= AXIETH1G_RAF_MCSTREJ_MASK;
        AXIETH1G_REG_WRITE(p_axieth1g, AXIETH1G_RAF_OFFSET, 
                           sizeof(b_u32), 0, 
                           (char *)&regval, sizeof(b_u32));
    }

    /* Disable extended multicast option */
    if (depops & AXIETH1G_EXT_MULTICAST_OPTION) {
        PDEBUG("XAxiEthernet_Clearoptions:disabling extended multicast mode\n");
        AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_RAF_OFFSET, 
                          sizeof(b_u32), 0, 
                          (char *)&regval, sizeof(b_u32));
        regval &= ~AXIETH1G_RAF_EMULTIFLTRENBL_MASK;
        AXIETH1G_REG_WRITE(p_axieth1g, AXIETH1G_RAF_OFFSET, 
                           sizeof(b_u32), 0, 
                           (char *)&regval, sizeof(b_u32));
    }

    /* Disable extended transmit VLAN tag option */
    if (depops & AXIETH1G_EXT_TXVLAN_TAG_OPTION) {
        PDEBUG("XAxiEthernet_Clearoptions:disabling extended TX VLAN tag mode\n");
        AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_RAF_OFFSET, 
                          sizeof(b_u32), 0, 
                          (char *)&regval, sizeof(b_u32));
        regval &= ~AXIETH1G_RAF_TXVTAGMODE_MASK;
        AXIETH1G_REG_WRITE(p_axieth1g, AXIETH1G_RAF_OFFSET, 
                           sizeof(b_u32), 0, 
                           (char *)&regval, sizeof(b_u32));
    }

    /* Disable extended receive VLAN tag option */
    if (depops & AXIETH1G_EXT_RXVLAN_TAG_OPTION) {
        PDEBUG("XAxiEthernet_Clearoptions:disabling extended RX VLAN tag mode\n");
        AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_RAF_OFFSET, 
                          sizeof(b_u32), 0, 
                          (char *)&regval, sizeof(b_u32));
        regval &= ~AXIETH1G_RAF_RXVTAGMODE_MASK;
        AXIETH1G_REG_WRITE(p_axieth1g, AXIETH1G_RAF_OFFSET, 
                           sizeof(b_u32), 0, 
                           (char *)&regval, sizeof(b_u32));
    }

    /* Disable extended transmit VLAN strip option */
    if (depops & AXIETH1G_EXT_TXVLAN_STRP_OPTION) {
        PDEBUG("XAxiEthernet_Clearoptions:disabling extended TX VLAN strip mode\n");
        AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_RAF_OFFSET, 
                          sizeof(b_u32), 0, 
                          (char *)&regval, sizeof(b_u32));
        regval &= ~AXIETH1G_RAF_TXVSTRPMODE_MASK;
        AXIETH1G_REG_WRITE(p_axieth1g, AXIETH1G_RAF_OFFSET, 
                           sizeof(b_u32), 0, 
                           (char *)&regval, sizeof(b_u32));
    }

    /* Disable extended receive VLAN strip option */
    if (depops & AXIETH1G_EXT_RXVLAN_STRP_OPTION) {
        PDEBUG("XAxiEthernet_Clearoptions:disabling extended RX VLAN strip mode\n");
        AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_RAF_OFFSET, 
                          sizeof(b_u32), 0, 
                          (char *)&regval, sizeof(b_u32));
        regval &= ~AXIETH1G_RAF_RXVSTRPMODE_MASK;
        AXIETH1G_REG_WRITE(p_axieth1g, AXIETH1G_RAF_OFFSET, 
                           sizeof(b_u32), 0, 
                           (char *)&regval, sizeof(b_u32));
    }

    /*
     * The remaining options not handled here are managed elsewhere in the
     * driver. No register modifications are needed at this time.
     * Reflecting the option in p_axieth1g->options is good enough for
     * now.
     */
    PDEBUG( "Clearoptions: returning SUCCESS\n");
    return CCL_OK;
}

/**
 * _axieth1g_phy_set_mdio_divisor sets the MDIO clock divisor in the
 * Axi Ethernet,specified by <i>p_axieth1g</i> to the value, <i>Divisor</i>.
 * This function must be called once after each reset prior to accessing
 * MII PHY registers.
 *
 * From the Virtex-6(TM) and Spartan-6 (TM) Embedded Tri-Mode Ethernet
 * MAC User's Guide, the following equation governs the MDIO clock to the PHY:
 *
 * <pre>
 * 			f[HOSTCLK]
 *	f[MDC] = -----------------------
 *			(1 + Divisor) * 2
 * </pre>
 *
 * where f[HOSTCLK] is the bus clock frequency in MHz, and f[MDC] is the
 * MDIO clock frequency in MHz to the PHY. Typically, f[MDC] should not
 * exceed 2.5 MHz. Some PHYs can tolerate faster speeds which means faster
 * access.
 *
 * @param[in] p_axieth1g references the axieth1g_ctx_t instance 
 *      on which to operate.
 * @param[in] Divisor is the divisor value to set within the 
 *      range of 0 to AXIETH1G_MDIO_MC_CLK_DVD_MAX.
 *
 * @return error code
 */
static ccl_err_t _axieth1g_phy_set_mdio_divisor(axieth1g_ctx_t *p_axieth1g, b_u8 divisor)
{
    b_u32 regval;

    if ( !p_axieth1g || (divisor > AXIETH1G_MDIO_MC_CLOCK_DIVIDE_MAX)) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIETH1G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    regval = divisor | AXIETH1G_MDIO_MC_MDIOEN_MASK;
    AXIETH1G_REG_WRITE(p_axieth1g, AXIETH1G_MDIO_MC_OFFSET, 
                       sizeof(b_u32), 0, 
                       (char *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/**
 * _axieth1g_set_mac_address sets the MAC address for the Axi 
 * Ethernet device, 
 * specified by <i>p_axieth1g</i> to the MAC address specified by
 * <i>p_addr</i>.
 * The Axi Ethernet device must be stopped before calling this function.
 *
 * @param[in] p_axieth1g is a pointer to the Axi Ethernet 
 *      instance to be worked on.
 * @param[in] p_addr is a reference to the 6-byte MAC address to
 *       set.
 *
 * @return error code 
 *  
 * @note
 * This routine also supports the extended/new VLAN and multicast mode. The
 * AXIETH1G_RAF_NEWFNCENBL_MASK bit dictates which offset will be configured.
 */
static ccl_err_t _axieth1g_set_mac_address(axieth1g_ctx_t *p_axieth1g, void *p_addr)
{
    b_u32 macaddr;
    b_u8 *paddr = (b_u8 *)p_addr;

    if ( !p_axieth1g || !p_addr) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIETH1G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    PDEBUG("XAxiEthernet_Setmacaddress: setting mac address to:0x%08x%8x%8x%8x%8x%8x\n",
           paddr[0],  paddr[1], paddr[2], paddr[3], paddr[4], paddr[5]);

    /* Prepare MAC bits in either UAW0/UAWL */
    macaddr = paddr[0];
    macaddr |= paddr[1] << 8;
    macaddr |= paddr[2] << 16;
    macaddr |= paddr[3] << 24;
    /* Check to see if it is in extended/new mode. */
    if (!(AXIETH1G_IS_EXT_FUNC_CAP(p_axieth1g))) {
        /*
         * Set the MAC bits [31:0] in UAW0.
         * Having paddr be unsigned type prevents the following
         * operations from sign extending.
         */
        AXIETH1G_REG_WRITE(p_axieth1g, AXIETH1G_UAW0_OFFSET, 
                           sizeof(b_u32), 0, 
                           (char *)&macaddr, sizeof(b_u32));
        /* There are reserved bits in UAW1 so don't affect them */
        AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_UAW1_OFFSET, 
                          sizeof(b_u32), 0, 
                          (char *)&macaddr, sizeof(b_u32));
        macaddr &= ~AXIETH1G_UAW1_UNICASTADDR_MASK;

        /* Set MAC bits [47:32] in UAW1 */
        macaddr |= paddr[4];
        macaddr |= paddr[5] << 8;
        AXIETH1G_REG_WRITE(p_axieth1g, AXIETH1G_UAW1_OFFSET, 
                           sizeof(b_u32), 0, 
                           (char *)&macaddr, sizeof(b_u32));

        return CCL_OK;
    } else { /* Extended mode */
        /*
         * Set the MAC bits [31:0] in UAWL register.
         * Having paddr be unsigned type prevents the following
         * operations from sign extending.
         */
        AXIETH1G_REG_WRITE(p_axieth1g, AXIETH1G_UAWL_OFFSET, 
                           sizeof(b_u32), 0, 
                           (char *)&macaddr, sizeof(b_u32));

        /* Set MAC bits [47:32] in UAWU register. */
        macaddr  = 0;
        macaddr |= paddr[4];
        macaddr |= paddr[5] << 8;
        AXIETH1G_REG_WRITE(p_axieth1g, AXIETH1G_UAWU_OFFSET, 
                           sizeof(b_u32), 0, 
                           (char *)&macaddr, sizeof(b_u32));

        return CCL_OK;
    }
}

/**
 * _axieth1g_get_mac_address gets the MAC address for the Axi Ethernet,
 * specified by <i>p_axieth1g</i> into the memory buffer specified by
 * <i>p_addr</i>.
 *
 * @param[in] p_axieth1g is a pointer to the Axi Ethernet 
 *      instance to be worked on.
 * @param[in] p_addr references the memory buffer to store the
 *      retrieved MAC address. This memory buffer must be at
 *      least 6 bytes in length.
 *
 * @return error code
 *
 * @note
 * This routine also supports the extended/new VLAN and multicast mode. The
 * AXIETH1G_RAF_NEWFNCENBL_MASK bit dictates which offset will be configured.
 */
static ccl_err_t _axieth1g_get_mac_address(axieth1g_ctx_t *p_axieth1g, void *p_addr)
{
    b_u32 macaddr;
    b_u8 *paddr = (b_u8 *)p_addr;

    if ( !p_axieth1g || !p_addr) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIETH1G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }


    /* Check to see if it is in extended/new mode. */
    if (!(AXIETH1G_IS_EXT_FUNC_CAP(p_axieth1g))) {
        /* Read MAC bits [31:0] in UAW0 */
        AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_UAW0_OFFSET, 
                          sizeof(b_u32), 0, 
                          (char *)&macaddr, sizeof(b_u32));
        paddr[0] = (b_u8) macaddr;
        paddr[1] = (b_u8) (macaddr >> 8);
        paddr[2] = (b_u8) (macaddr >> 16);
        paddr[3] = (b_u8) (macaddr >> 24);

        /* Read MAC bits [47:32] in UAW1 */
        AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_UAW1_OFFSET, 
                          sizeof(b_u32), 0, 
                          (char *)&macaddr, sizeof(b_u32));
        paddr[4] = (b_u8) macaddr;
        paddr[5] = (b_u8) (macaddr >> 8);
    } else { /* Extended/new mode */
        /* Read MAC bits [31:0] in UAWL register */
        AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_UAWL_OFFSET, 
                          sizeof(b_u32), 0, 
                          (char *)&macaddr, sizeof(b_u32));

        paddr[0] = (b_u8) macaddr;
        paddr[1] = (b_u8) (macaddr >> 8);
        paddr[2] = (b_u8) (macaddr >> 16);
        paddr[3] = (b_u8) (macaddr >> 24);

        /* Read MAC bits [47:32] in UAWU register */
        AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_UAWU_OFFSET, 
                          sizeof(b_u32), 0, 
                          (char *)&macaddr, sizeof(b_u32));
        paddr[4] = (b_u8) macaddr;
        paddr[5] = (b_u8) (macaddr >> 8);
    }
}

/**
 * _axieth1g_set_operating_speed sets the current operating link 
 * speed. For any traffic to be passed, this speed must match 
 * the current MII/GMII/SGMII/RGMII link speed. 
 *
 * @param[in] p_axieth1g references the Axi Ethernet on which 
 *      to operate.
 * @param[in] speed is the speed to set in units of Mbps.
 *		Valid values are 10, 100, or 1000.
 *
 * @return	error code
 */
static ccl_err_t _axieth1g_set_operating_speed(axieth1g_ctx_t *p_axieth1g, b_u16 speed)
{
    b_u32 emmc;
    b_u8  phy_type;
    b_bool set_speed = CCL_TRUE;

    if ( !p_axieth1g || 
         ((speed != AXIETH1G_SPEED_10_MBPS) && 
          (speed != AXIETH1G_SPEED_100_MBPS) && 
          (speed != AXIETH1G_SPEED_1000_MBPS) && 
          (speed != AXIETH1G_SPEED_2500_MBPS))) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIETH1G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    phy_type = AXIETH1G_GET_PHYSICAL_IF(p_axieth1g);

    /*
     * The following code checks for all allowable speed conditions before
     * writing to the register. Please refer to the hardware specs for
     * more information on it.
     * For PHY type 1000Base-x, 10 and 100 Mbps are not supported.
     * For soft/hard Axi Ethernet core, 1000 Mbps is supported in all PHY
     * types except MII.
     */
    if ((speed == AXIETH1G_SPEED_10_MBPS) || (speed == AXIETH1G_SPEED_100_MBPS)) {
        if (phy_type == AXIETH1G_PHY_TYPE_1000BASE_X) {
            set_speed = CCL_FALSE;
        }
    } else {
        if ((speed == AXIETH1G_SPEED_1000_MBPS) &&
            (phy_type == AXIETH1G_PHY_TYPE_MII)) {
            set_speed = CCL_FALSE;
        }
    }

    if (set_speed == CCL_TRUE) {
        /*
         * Get the current contents of the EMAC config register and
         * zero out speed bits
         */
        AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_EMMC_OFFSET, 
                          sizeof(b_u32), 0, 
                          (char *)&emmc, sizeof(b_u32));
        emmc &= ~AXIETH1G_EMMC_LINKSPEED_MASK;

        switch (speed) {
        case AXIETH1G_SPEED_10_MBPS:
            break;

        case AXIETH1G_SPEED_100_MBPS:
            emmc |= AXIETH1G_EMMC_LINKSPD_100;
            break;

        case AXIETH1G_SPEED_1000_MBPS:
            emmc |= AXIETH1G_EMMC_LINKSPD_1000;
            break;

        case AXIETH1G_SPEED_2500_MBPS:
            emmc |= AXIETH1G_EMMC_LINKSPD_1000;
            break;

        default:
            return CCL_FAIL;
        }

        PDEBUG("XAxiEthernet_SetOperatingspeed: new speed: 0x%0x\n", emmc);
        /* Set register and return */
        AXIETH1G_REG_WRITE(p_axieth1g, AXIETH1G_EMMC_OFFSET, 
                           sizeof(b_u32), 0, 
                           (char *)&emmc, sizeof(b_u32));
        PDEBUG("XAxiEthernet_SetOperatingspeed: done\n");
        return CCL_OK;
    } else {
        PDEBUG("speed not compatible with the Axi Ethernet Phy type\n");
        return CCL_FAIL;
    }
}

/**
 * _axieth1g_get_operating_speed gets the current operating link speed. This
 * may be the value set by _axieth1g_set_operating_speed() or a 
 * hardware default. 
 *
 * @param[in]	p_axieth1g is a pointer to the axieth1g_ctx_t instance to be
 *      worked on.
 * @param[out] p_speed current operating link speed 
 *
 * @return	error code
 *
 * @note
 * Returns the link speed in units of megabits per second (10 /
 * 100 / 1000).
 * Can return a value of 0, in case it does not get a valid
 * speed from EMMC.
 */
static ccl_err_t _axieth1g_get_operating_speed(axieth1g_ctx_t *p_axieth1g, b_u16 *p_speed)
{
    b_u32 regval;

    if ( !p_axieth1g || !p_speed) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIETH1G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    PDEBUG("XAxiEthernet_GetOperatingspeed\n");
    AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_EMMC_OFFSET, 
                      sizeof(b_u32), 0, 
                      (char *)&regval, sizeof(b_u32));
    regval &= AXIETH1G_EMMC_LINKSPEED_MASK;

    switch (regval) {
    
    case AXIETH1G_EMMC_LINKSPD_1000:
        PDEBUG("XAxiEthernet_GetOperatingspeed: returning 1000\n");
        *p_speed = AXIETH1G_SPEED_1000_MBPS;

    case AXIETH1G_EMMC_LINKSPD_100:
        PDEBUG("XAxiEthernet_GetOperatingspeed: returning 100\n");
        *p_speed = AXIETH1G_SPEED_100_MBPS;

    case AXIETH1G_EMMC_LINKSPD_10:
        PDEBUG("XAxiEthernet_GetOperatingspeed: returning 10\n");
        *p_speed = AXIETH1G_SPEED_10_MBPS;

    default:
        *p_speed = 0;
    }

    return CCL_OK;
}

/**
 * _axieth1g_set_bad_frame_rcv_option is used to enable the bad frame receive
 * option. If enabled, this option ensures that bad receive frames are allowed
 * and passed to the AXI4-Stream interface as if they are good frames.
 *
 *  @param[in] p_axieth1g is a pointer to the axieth1g_ctx_t
 *       instance to be worked on.
 *
 * @return	error code
 */
static ccl_err_t _axieth1g_set_bad_frame_rcv_option(axieth1g_ctx_t *p_axieth1g)
{
    b_u32 regval;

    if ( !p_axieth1g ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIETH1G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_RAF_OFFSET, 
                      sizeof(b_u32), 0, 
                      (char *)&regval, sizeof(b_u32));
    regval |= AXIETH1G_RAF_RXBADFRMEN_MASK;
    AXIETH1G_REG_WRITE(p_axieth1g, AXIETH1G_RAF_OFFSET, 
                       sizeof(b_u32), 0, 
                       (char *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/**
 * _axieth1g_clear_bad_frame_rcv_option is used to disable the 
 * bad frame receive option. 
 *
 *  @param[in] p_axieth1g is a pointer to the axieth1g_ctx_t
 *       instance to be worked on.
 *
 * @return	error code
 */
static ccl_err_t _axieth1g_clear_bad_frame_rcv_option(axieth1g_ctx_t *p_axieth1g)
{
    b_u32 regval;

    if ( !p_axieth1g ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIETH1G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_RAF_OFFSET, 
                      sizeof(b_u32), 0, 
                      (char *)&regval, sizeof(b_u32));
    regval &= ~AXIETH1G_RAF_RXBADFRMEN_MASK;
    AXIETH1G_REG_WRITE(p_axieth1g, AXIETH1G_RAF_OFFSET, 
                       sizeof(b_u32), 0, 
                       (char *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/**
 * _axieth1g_disable_control_frame_len_check is used to disable 
 * the length check for control frames (pause frames). This means 
 * once the API is called, control frames larger than the minimum 
 * frame length are accepted. 
 *
 *  @param[in] p_axieth1g is a pointer to the axieth1g_ctx_t
 *       instance to be worked on.
 *
 * @return	error code
 */
static ccl_err_t _axieth1g_disable_control_frame_len_check(axieth1g_ctx_t *p_axieth1g)
{
    b_u32 regval;

    if ( !p_axieth1g ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIETH1G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_RCW1_OFFSET, 
                      sizeof(b_u32), 0, 
                      (char *)&regval, sizeof(b_u32));
    regval |= AXIETH1G_RCW1_CL_DIS_MASK;
    AXIETH1G_REG_WRITE(p_axieth1g, AXIETH1G_RCW1_OFFSET, 
                       sizeof(b_u32), 0, 
                       (char *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/**
 * _axieth1g_enable_control_frame_len_check is used to enable the length check
 * for control frames (pause frames). After calling the API, all control frames
 * received will be checked for proper length (less than minimum frame length).
 * By default, upon normal start up, control frame length check is enabled.
 * Hence this API needs to be called only if previously the control frame length
 * check has been disabled by calling the API
 * _axieth1g_disable_control_frame_len_check.
 *
 * @param[in] p_axieth1g is a pointer to the axieth1g_ctx_t
 *      instance to be worked on.
 *
 * @return	error code
 */
static ccl_err_t _axieth1g_enable_control_frame_len_check(axieth1g_ctx_t *p_axieth1g)
{
    b_u32 regval;

    if ( !p_axieth1g ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIETH1G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_RCW1_OFFSET, 
                      sizeof(b_u32), 0, 
                      (char *)&regval, sizeof(b_u32));
    regval &= ~AXIETH1G_RCW1_CL_DIS_MASK;
    AXIETH1G_REG_WRITE(p_axieth1g, AXIETH1G_RCW1_OFFSET, 
                       sizeof(b_u32), 0, 
                       (char *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/**
 *
 * _axieth1g_hw_init (internal use only) performs a one-time setup of a Axi Ethernet device.
 * The setup performed here only need to occur once after any reset.
 *
 * @param[in] p_axieth1g is a pointer to axieth1g_ctx_t instance to 
 *       be worked on
 * 
 * @return	error code
 */
static ccl_err_t _axieth1g_hw_init(axieth1g_ctx_t *p_axieth1g)
{
    b_u32 regval;

    if ( !p_axieth1g ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIETH1G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* Disable the receiver */
    AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_RCW1_OFFSET, 
                      sizeof(b_u32), 0, 
                      (char *)&regval, sizeof(b_u32));
    regval &= ~AXIETH1G_RCW1_RX_MASK;
    AXIETH1G_REG_WRITE(p_axieth1g, AXIETH1G_RCW1_OFFSET, 
                       sizeof(b_u32), 0, 
                       (char *)&regval, sizeof(b_u32));

    /*
     * Stopping the receiver in mid-packet causes a dropped packet
     * indication from HW. Clear it.
     */
    /* Get the interrupt pending register */
    AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_IP_OFFSET, 
                      sizeof(b_u32), 0, 
                      (char *)&regval, sizeof(b_u32));

    if (regval & AXIETH1G_INT_RXRJECT_MASK) {
        /*
         * Set the interrupt status register to clear the pending
         * interrupt
         */
        regval = AXIETH1G_INT_RXRJECT_MASK;
        AXIETH1G_REG_WRITE(p_axieth1g, AXIETH1G_IS_OFFSET, 
                           sizeof(b_u32), 0, 
                           (char *)&regval, sizeof(b_u32));
    }

    /*
     * Sync default options with HW but leave receiver and transmitter
     * disabled. They get enabled with XAxiEthernet_Start() if
     * AXIETH1G_TRANSMITTER_ENABLE_OPTION and AXIETH1G_RECEIVER_ENABLE_OPTION
     * are set
     */
    _ret = _axieth1g_set_options(p_axieth1g, p_axieth1g->options &
                                 ~(AXIETH1G_TRANSMITTER_ENABLE_OPTION | 
                                   AXIETH1G_RECEIVER_ENABLE_OPTION));
    if ( _ret ) {
        PDEBUG("_axieth1g_set_options error\n");
        AXIETH1G_ERROR_RETURN(_ret);
    }

    _ret = _axieth1g_clear_options(p_axieth1g, ~p_axieth1g->options);
    if ( _ret ) {
        PDEBUG("_axieth1g_clear_options error\n");
        AXIETH1G_ERROR_RETURN(_ret);
    }

    /* Set default MDIO divisor */
    _ret = _axieth1g_phy_set_mdio_divisor(p_axieth1g, AXIETH1G_MDIO_DIV_DFT);
    if ( _ret ) {
        PDEBUG("_axieth1g_phy_set_mdio_divisor error\n");
        AXIETH1G_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

/**
 * _axieth1g_reset does not perform a soft reset of the AxiEthernet core.
 * AxiEthernet hardware is reset by the device connected to the AXI4-Stream
 * interface.
 * This function inserts some delay before proceeding to check for MgtRdy bit.
 * The delay is necessary to be at a safe side. It takes a while for the reset
 * process to complete and for any of the AxiEthernet registers to be accessed.
 * It then checks for MgtRdy bit in IS register to know if AxiEthernet reset
 * is completed or not. Subsequently it calls one more driver function to
 * complete the AxiEthernet hardware initialization.
 *
 * @param[in] p_axieth1g is a pointer to axieth1g_ctx_t instance to 
 *       be worked on
 * 
 * @return	error code
 *
 * @note		It is the responsibility of the user to reset the AxiEthernet
 *		hardware before using it. AxiEthernet hardware should be reset
 *		through the device connected to the AXI4-Stream interface of
 *		AxiEthernet.
 */
ccl_err_t _axieth1g_reset(axieth1g_ctx_t *p_axieth1g)
{
    b_i32 timeout = 1000;
    b_u32 regval;

    if ( !p_axieth1g ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIETH1G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Add delay of 1 sec to give enough time to the core to come
     * out of reset. Till the time core comes out of reset none of the
     * AxiEthernet registers are accessible including the IS register.
     */
    usleep(1000000);
    /*
     * Check the status of the MgtRdy bit in the interrupt status
     * registers. This must be done to allow the MGT clock to become stable
     * for the Sgmii and 1000BaseX PHY interfaces. No other register reads
     * will be valid until this bit is valid.
     * The bit is always a 1 for all other PHY interfaces.
     */
    do {
        timeout--;
        if (timeout <= 0) {
            PDEBUG("ERROR: MGT clock is not ready\n");
            return CCL_FAIL;
        }
        AXIETH1G_REG_READ(p_axieth1g, AXIETH1G_IS_OFFSET, 
                          sizeof(b_u32), 0, 
                          (char *)&regval, sizeof(b_u32));
    } while (!(regval & AXIETH1G_INT_MGTRDY_MASK));


    /* Stop the device and reset HW */
    _ret = axieth1g_stop(p_axieth1g);
    if ( _ret ) {
        PDEBUG("_axieth1g_stop error\n");
        AXIETH1G_ERROR_RETURN(_ret);
    }

    /* Setup HW */
    _ret = _axieth1g_hw_init(p_axieth1g);
    if ( _ret ) {
        PDEBUG("_axieth1g_hw_init error\n");
        AXIETH1G_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

/* Device instance hw-specific initializations 
 */
static ccl_err_t _faxieth1g_add(void *devo, void *brdspec) {
    axieth1g_ctx_t       *p_axieth1g;
    eth_brdspec_t        *p_brdspec;
    devo_t               device;

    if ( !devo || !brdspec ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIETH1G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_axieth1g = devman_device_private(devo);
    if ( !p_axieth1g ) {
        PDEBUG("NULL context area\n");
        AXIETH1G_ERROR_RETURN(CCL_FAIL);
    }
    p_brdspec = (eth_brdspec_t *)brdspec;
    /* back reference the device manager object handle */    
    p_axieth1g->devo = devo;    
    /* save the board specific info */
    memcpy(&p_axieth1g->brdspec, p_brdspec, sizeof(eth_brdspec_t));

    /* start the device */
    _ret = axieth1g_start(p_axieth1g);
    if ( _ret ) {
        PDEBUG("_axieth1g_start error\n");
        AXIETH1G_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

/* Device instance hw-specific deinitializations 
 */
static ccl_err_t _faxieth1g_cut(void *devo)
{
    axieth1g_ctx_t  *p_axieth1g;
    if ( !devo ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIETH1G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_axieth1g = devman_device_private(devo);
    if ( !p_axieth1g ) {
        PDEBUG("NULL context area\n");
        AXIETH1G_ERROR_RETURN(CCL_FAIL);
    }
    /* stop the device */
    _ret = axieth1g_stop(p_axieth1g);
    if ( _ret ) {
        PDEBUG("_axieth1g_stop error\n");
        AXIETH1G_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

/* device type description structure */
static device_driver_t _axieth1g_driver = {
    .name = "axieth1g",
    .f_add = _faxieth1g_add,
    .f_cut = _faxieth1g_cut,
    .attrs_num = sizeof(_attrs)/sizeof(_attrs[0]),
                 .attrs = _attrs,
    .priv_size = sizeof(axieth1g_ctx_t)
}; 

/* Initialize AXIETH1G device type
 */
ccl_err_t devaxieth1g_init(void)
{
    /* register device type */
    _ret = devman_driver_register(&_axieth1g_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to register driver\n");
        AXIETH1G_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}

/* Deinitialize AXIETH1G device type
 */
ccl_err_t devaxieth1g_fini(void)
{
    /* unregister device type */
    _ret = devman_driver_unregister(&_axieth1g_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to unregister driver\n");
        AXIETH1G_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}
