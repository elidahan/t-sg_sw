/********************************************************************************/
/**
 * @file usplus100g.c
 * @brief The Xilinx UltraScale+ Devices Integrated 100G Ethernet IP 
 *        subsystem (USPLUS100G) device manager driver
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/

#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <semaphore.h>
#include <linux/limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devman_port.h"
#include "devman_brd_gen.h"
#include "devboard.h"
#include "spibus.h"
#include "usplus100g.h"

#define USPLUS100G_DEBUG
/* printing/error-returning macros */
#ifdef USPLUS100G_DEBUG
    #define PDEBUG(fmt, args...) \
            ccl_syslog_err("USPLUS100G_DEBUG: %s(): " fmt, __FUNCTION__, ## args)
#else
    #define PDEBUG(fmt, args...)
#endif
#define USPLUS100G_ERROR_RETURN(err) do { \
   ccl_syslog_err("Error! %s[%d], error=%i\n", __FUNCTION__, __LINE__, (err)); \
   return err; \
} while (0)
#define USPLUS100G_CHECK_ERROR_RETURN(err) do { \
    if (err != CCL_OK) { \
        ccl_syslog_err("Error! %s[%d], error=%i\n", __FUNCTION__, __LINE__, (err)); \
        return err; \
    } \
} while (0)

#define USPLUS100G_REG_READ(context, offset, offset_sz, idx, pvalue, size) do { \
    _ret = _usplus100g_reg_read((context), \
                                (offset), \
                                (offset_sz), (idx), \
                                (pvalue), (size)); \
    if ( _ret ) { \
        PDEBUG("_usplus100g_reg_read error\n"); \
        USPLUS100G_ERROR_RETURN(_ret); \
    } \
} while (0)

#define USPLUS100G_REG_WRITE(context, offset, offset_sz, idx, pvalue, size) do { \
    _ret = _usplus100g_reg_write((context), \
                                 (offset), \
                                 (offset_sz), (idx), \
                                 (pvalue), (size)); \
    if ( _ret ) { \
        PDEBUG("_usplus100g_reg_write error\n"); \
        USPLUS100G_ERROR_RETURN(_ret); \
    } \
} while (0)

/* USPLUS100G registers offsets */

/* Configuration Register Map */
#define USPLUS100G_GT_RESET_REG                                    0x0000 /**< Reset for the serial transceiver (GT) */
#define USPLUS100G_RESET_REG                                       0x0004 /**< Reset for the RX/TX circuits */
#define USPLUS100G_SWITCH_CORE_MODE_REG                            0x0008 /**< Mode switch between CAUI-10 and CAUI-4*/
#define USPLUS100G_CONFIGURATION_TX_REG1                           0x000C /**< TX block configuration */
#define USPLUS100G_CONFIGURATION_RX_REG1                           0x0014 /**< RX block configuration */
#define USPLUS100G_CORE_MODE_REG                                   0x0020 /**< CAUI-10/CAUI-4 core mode configuration */
#define USPLUS100G_CORE_VERSION_REG                                0x0024 /**< Current version of the core */
#define USPLUS100G_CONFIGURATION_TX_BIP_OVERRIDE                   0x002C /**< Controls value of the bip7 byte of PCS lane0 */
#define USPLUS100G_CONFIGURATION_TX_FLOW_CONTROL_CONTROL_REG1      0x0030 /**< TX pause enable signal for the corresponding priority */
#define USPLUS100G_CONFIGURATION_TX_FLOW_CONTROL_REFRESH_REG1      0x0034 /**< Configuration of retransmission time of pause packets for priorities 0-1 */
#define USPLUS100G_CONFIGURATION_TX_FLOW_CONTROL_REFRESH_REG2      0x0038 /**< Configuration of retransmission time of pause packets for priorities 2-3 */
#define USPLUS100G_CONFIGURATION_TX_FLOW_CONTROL_REFRESH_REG3      0x003C /**< Configuration of retransmission time of pause packets for priorities 4-5 */
#define USPLUS100G_CONFIGURATION_TX_FLOW_CONTROL_REFRESH_REG4      0x0040 /**< Configuration of retransmission time of pause packets for priorities 6-7 in priority based pause operation */
#define USPLUS100G_CONFIGURATION_TX_FLOW_CONTROL_REFRESH_REG5      0x0044 /**< Configuration of retransmission time of pause packets in the global pause operation */
#define USPLUS100G_CONFIGURATION_TX_FLOW_CONTROL_QUANTA_REG1       0x0048 /**< Quanta to be transmitted for priorities 0-1 in priority based pause operation */
#define USPLUS100G_CONFIGURATION_TX_FLOW_CONTROL_QUANTA_REG2       0x004C /**< Quanta to be transmitted for priorities 2-3 in priority based pause operation */
#define USPLUS100G_CONFIGURATION_TX_FLOW_CONTROL_QUANTA_REG3       0x0050 /**< Quanta to be transmitted for priorities 4-5 in priority based pause operation */
#define USPLUS100G_CONFIGURATION_TX_FLOW_CONTROL_QUANTA_REG4       0x0054 /**< Quanta to be transmitted for priorities 6-7 in priority based pause operation */
#define USPLUS100G_CONFIGURATION_TX_FLOW_CONTROL_QUANTA_REG5       0x0058 /**< Quanta to be transmitted in global pause operation */
#define USPLUS100G_CONFIGURATION_TX_OTN_PKT_LEN_REG                0x005C /**< Limits for undersized/oversized packets */
#define USPLUS100G_CONFIGURATION_TX_OTN_CTL_REG                    0x0060 /**< Controls FCS and Preamble checks */
#define USPLUS100G_CONFIGURATION_RX_FLOW_CONTROL_CONTROL_REG1      0x0084 /**< RX Flow Control configuration */
#define USPLUS100G_CONFIGURATION_RX_FLOW_CONTROL_CONTROL_REG2      0x0088 /**< RX Flow Control configuration */
#define USPLUS100G_GT_LOOPBACK_REG                                 0x0090 /**< Controls GT loopback */
#define USPLUS100G_CONFIGURATION_AN_CONTROL_REG1                   0x00A0 /**< Configuration of Autonegotiation block */
#define USPLUS100G_CONFIGURATION_AN_CONTROL_REG2                   0x00A4 /**< Configuration of Autonegotiation block */
#define USPLUS100G_CONFIGURATION_AN_ABILITY                        0x00A8 /**< Configuration of Autonegotiation ability */
#define USPLUS100G_CONFIGURATION_LT_CONTROL_REG1                   0x00AC /**< Configuration of Link Training block */
#define USPLUS100G_CONFIGURATION_LT_TRAINED_REG                    0x00B0 /**< Indicates that the receiver portion of training is complete */
#define USPLUS100G_CONFIGURATION_LT_PRESET_REG                     0x00B4 /**< Sets the value of the preset bit that is transmitted to the link partner in the control block of the training frame */
#define USPLUS100G_CONFIGURATION_LT_INIT_REG                       0x00B8 /**< Sets the value of the initialize bit that is transmitted to the link partner in the control block of the training frame */
#define USPLUS100G_CONFIGURATION_LT_SEED_REG0                      0x00BC /**< 11-bit signal that seeds the training pattern generator */
#define USPLUS100G_CONFIGURATION_LT_SEED_REG1                      0x00C0 /**< 11-bit signal that seeds the training pattern generator */
#define USPLUS100G_CONFIGURATION_LT_COEFFICIENT_REG0               0x00C4 /**< Configuration of coefficients that is transmitted to the link partner in the control block of the training frame */ 
#define USPLUS100G_CONFIGURATION_LT_COEFFICIENT_REG1               0x00C8 /**< Configuration of coefficients that is transmitted to the link partner in the control block of the training frame */ 

#define USPLUS100G_RSFEC_CONFIG_INDICATION_CORRECTION              0x1000 /**< Controls RS-FEC indication and correction */
#define USPLUS100G_RSFEC_CONFIG_ENABLE                             0x107C /**< Enables RS-FEC */


/* Status Register Map */
#define USPLUS100G_STAT_TX_STATUS_REG                              0x0200 /**< TX local fault status */
#define USPLUS100G_STAT_RX_STATUS_REG                              0x0204 /**< RX statuses */  
#define USPLUS100G_STAT_STATUS_REG1                                0x0208 /**< TX/RX fifo statuses */
#define USPLUS100G_STAT_RX_BLOCK_LOCK_REG                          0x020C /**< Block lock status for each PCS lane */
#define USPLUS100G_STAT_RX_LANE_SYNC_REG                           0x0210 /**< Indicates whether a PCS lane is word boundary synchronized */
#define USPLUS100G_STAT_RX_LANE_SYNC_ERR_REG                       0x0214 /**< Indicates whether an error occured during word boundary synchronization in the respective PCS lane */
#define USPLUS100G_STAT_RX_AM_ERR_REG                              0x0218 /**< Indicates that an incorrectly formed PCS lane Marker Word was detected in the respective lane */
#define USPLUS100G_STAT_RX_AM_LEN_ERR_REG                          0x021C /**< Indicates that the corresponding lane is receiving PCS Lane Markers at wrong intervals */
#define USPLUS100G_STAT_RX_AM_REPEAT_ERR_REG                       0x0220 /**< Indicates whether four consecutive PCS Lane Marker errors occurred in the respective lane */
#define USPLUS100G_STAT_RX_PCSL_DEMUXED_REG                        0x0224 /**< Indicates whether the receiver has properly de-multiplexed that PCS lane */
#define USPLUS100G_STAT_RX_PCS_LANE_NUM_REG1                       0x0228 /**< Indicates which PCS lane is received on each physical lane */
#define USPLUS100G_STAT_RX_PCS_LANE_NUM_REG2                       0x022C /**< Indicates which PCS lane is received on each physical lane */
#define USPLUS100G_STAT_RX_PCS_LANE_NUM_REG3                       0x0230 /**< Indicates which PCS lane is received on each physical lane */
#define USPLUS100G_STAT_RX_PCS_LANE_NUM_REG4                       0x0234 /**< Indicates which PCS lane is received on each physical lane */
#define USPLUS100G_STAT_RX_BIP_OVERRIDE_REG                        0x0238 /**< Status of the received value of the bip7 byte in the PCS lane0 marker */
#define USPLUS100G_STAT_TX_OTN_STATUS_REG                          0x023C /**< Optical Transport Network (OTN) statuses */
#define USPLUS100G_STAT_AN_STATUS_REG                              0x0258 /**< Auto-Negotiation block statuses */
#define USPLUS100G_STAT_AN_ABILITY_REG                             0x025C /**< Auto-Negotiation block abilities */
#define USPLUS100G_STAT_AN_LINK_CTL_REG                            0x0260 /**< Auto-Negotiation block statuses */
#define USPLUS100G_STAT_LT_STATUS_REG1                             0x0264 /**< Link-Training block statuses */
#define USPLUS100G_STAT_LT_STATUS_REG2                             0x0268 /**< Link-Training block statuses */
#define USPLUS100G_STAT_LT_STATUS_REG3                             0x026C /**< Link-Training block statuses */
#define USPLUS100G_STAT_LT_STATUS_REG4                             0x0270 /**< Link-Training block statuses */
#define USPLUS100G_STAT_LT_COEFFICIENT0_REG                        0x0274 /**< Link-Training block statuses */
#define USPLUS100G_STAT_LT_COEFFICIENT1_REG                        0x0478 /**< Link-Training block statuses */

/* Currently not supported */
#define USPLUS100G_STAT_RSFEC_STATUS_REG                           0x1004 /**< RX RS-FEC statuses @note Currently not supported */
#define USPLUS100G_STAT_RSFEC_LANE_MAPPING_REG                     0x1018 /**< Indicates which PMA lane is mapped to each FEC lane @note Currently not supported  */
#define USPLUS100G_STAT_TX_OTN_RSFEC_STATUS_REG                    0x1044 /**< OTN interface statuses for each PCS lane @note Currently not supported */ 
/* End of Currently not supported */

/* Statisctics Counters */
#define USPLUS100G_TICK_REG                                        0x02B0 /**< Writing a 1 to the Tick bit will trigger a snapshot of all the Statistics counters into their readable registers */
#define USPLUS100G_STAT_CYCLE_COUNT                                0x02B8 /**< Number of RX core clock cycles between TICK_REG writes */    
#define USPLUS100G_STAT_RX_BIP_ERR_0                               0x02C0 /**< PCS lane 0 error indicator */
#define USPLUS100G_STAT_RX_BIP_ERR_1                               0x02C8 /**< PCS lane 1 error indicator */
#define USPLUS100G_STAT_RX_BIP_ERR_2                               0x02D0 /**< PCS lane 2 error indicator */
#define USPLUS100G_STAT_RX_BIP_ERR_3                               0x02D8 /**< PCS lane 3 error indicator */
#define USPLUS100G_STAT_RX_BIP_ERR_4                               0x02E0 /**< PCS lane 4 error indicator */
#define USPLUS100G_STAT_RX_BIP_ERR_5                               0x02E8 /**< PCS lane 5 error indicator */
#define USPLUS100G_STAT_RX_BIP_ERR_6                               0x02F0 /**< PCS lane 6 error indicator */
#define USPLUS100G_STAT_RX_BIP_ERR_7                               0x02F8 /**< PCS lane 7 error indicator */
#define USPLUS100G_STAT_RX_BIP_ERR_8                               0x0300 /**< PCS lane 8 error indicator */
#define USPLUS100G_STAT_RX_BIP_ERR_9                               0x0308 /**< PCS lane 9 error indicator */
#define USPLUS100G_STAT_RX_BIP_ERR_10                              0x0310 /**< PCS lane 10 error indicator */
#define USPLUS100G_STAT_RX_BIP_ERR_11                              0x0318 /**< PCS lane 11 error indicator */
#define USPLUS100G_STAT_RX_BIP_ERR_12                              0x0320 /**< PCS lane 12 error indicator */
#define USPLUS100G_STAT_RX_BIP_ERR_13                              0x0328 /**< PCS lane 13 error indicator */
#define USPLUS100G_STAT_RX_BIP_ERR_14                              0x0330 /**< PCS lane 14 error indicator */
#define USPLUS100G_STAT_RX_BIP_ERR_15                              0x0338 /**< PCS lane 15 error indicator */
#define USPLUS100G_STAT_RX_BIP_ERR_16                              0x0340 /**< PCS lane 16 error indicator */
#define USPLUS100G_STAT_RX_BIP_ERR_17                              0x0348 /**< PCS lane 17 error indicator */
#define USPLUS100G_STAT_RX_BIP_ERR_18                              0x0350 /**< PCS lane 18 error indicator */
#define USPLUS100G_STAT_RX_BIP_ERR_19                              0x0358 /**< PCS lane 19 error indicator */  
#define USPLUS100G_STAT_RX_FRAMING_ERR0                            0x0360 /**< RX number of bad sync header bits detected */          
#define USPLUS100G_STAT_RX_FRAMING_ERR1                            0x0368 /**< RX number of bad sync header bits detected */          
#define USPLUS100G_STAT_RX_FRAMING_ERR2                            0x0370 /**< RX number of bad sync header bits detected */          
#define USPLUS100G_STAT_RX_FRAMING_ERR3                            0x0378 /**< RX number of bad sync header bits detected */          
#define USPLUS100G_STAT_RX_FRAMING_ERR4                            0x0380 /**< RX number of bad sync header bits detected */          
#define USPLUS100G_STAT_RX_FRAMING_ERR5                            0x0388 /**< RX number of bad sync header bits detected */          
#define USPLUS100G_STAT_RX_FRAMING_ERR6                            0x0390 /**< RX number of bad sync header bits detected */          
#define USPLUS100G_STAT_RX_FRAMING_ERR7                            0x0398 /**< RX number of bad sync header bits detected */          
#define USPLUS100G_STAT_RX_FRAMING_ERR8                            0x03A0 /**< RX number of bad sync header bits detected */          
#define USPLUS100G_STAT_RX_FRAMING_ERR9                            0x03A8 /**< RX number of bad sync header bits detected */          
#define USPLUS100G_STAT_RX_FRAMING_ERR10                           0x03B0 /**< RX number of bad sync header bits detected */          
#define USPLUS100G_STAT_RX_FRAMING_ERR11                           0x03B8 /**< RX number of bad sync header bits detected */          
#define USPLUS100G_STAT_RX_FRAMING_ERR12                           0x03C0 /**< RX number of bad sync header bits detected */          
#define USPLUS100G_STAT_RX_FRAMING_ERR13                           0x03C8 /**< RX number of bad sync header bits detected */          
#define USPLUS100G_STAT_RX_FRAMING_ERR14                           0x03D0 /**< RX number of bad sync header bits detected */          
#define USPLUS100G_STAT_RX_FRAMING_ERR15                           0x03D8 /**< RX number of bad sync header bits detected */          
#define USPLUS100G_STAT_RX_FRAMING_ERR16                           0x03E0 /**< RX number of bad sync header bits detected */          
#define USPLUS100G_STAT_RX_FRAMING_ERR17                           0x03E8 /**< RX number of bad sync header bits detected */          
#define USPLUS100G_STAT_RX_FRAMING_ERR18                           0x03F0 /**< RX number of bad sync header bits detected */          
#define USPLUS100G_STAT_RX_FRAMING_ERR19                           0x03F8 /**< RX number of bad sync header bits detected */          
#define USPLUS100G_STAT_RX_BAD_CODE                                0x0418 /**< Number of 64B/66B code violations */
#define USPLUS100G_STAT_TX_FRAME_ERROR                             0x0458 /**< TX End Of Packet (EOP) abort */         
#define USPLUS100G_STAT_TX_TOTAL_PACKETS                           0x0500 /**< TX number of packets */         
#define USPLUS100G_STAT_TX_TOTAL_GOOD_PACKETS                      0x0508 /**< TX number of good packets packets */         
#define USPLUS100G_STAT_TX_TOTAL_BYTES                             0x0510 /**< TX number of bytes */                  
#define USPLUS100G_STAT_TX_TOTAL_GOOD_BYTES                        0x0518 /**< TX number of good bytes */                  
#define USPLUS100G_STAT_TX_PACKET_64_BYTES                         0x0520 /**< TX number of good and bad packets that contain 64 bytes */         
#define USPLUS100G_STAT_TX_PACKET_65_127_BYTES                     0x0528 /**< TX number of good and bad packets that contain between 65 and 127 bytes */                  
#define USPLUS100G_STAT_TX_PACKET_128_255_BYTES                    0x0530 /**< TX number of good and bad packets that contain between 128 and 255 bytes */                           
#define USPLUS100G_STAT_TX_PACKET_256_511_BYTES                    0x0538 /**< TX number of good and bad packets that contain between 256 and 511 bytes */                                    
#define USPLUS100G_STAT_TX_PACKET_512_1023_BYTES                   0x0540 /**< TX number of good and bad packets that contain between 512 and 1023 bytes */                                             
#define USPLUS100G_STAT_TX_PACKET_1024_1518_BYTES                  0x0548 /**< TX number of good and bad packets that contain between 1024 and 1518 bytes */                                                  
#define USPLUS100G_STAT_TX_PACKET_1519_1522_BYTES                  0x0550 /**< TX number of good and bad packets that contain between 1519 and 1522 bytes */           
#define USPLUS100G_STAT_TX_PACKET_1523_1548_BYTES                  0x0558 /**< TX number of good and bad packets that contain between 1523 and 1548 bytes */              
#define USPLUS100G_STAT_TX_PACKET_1549_2047_BYTES                  0x0560 /**< TX number of good and bad packets that contain between 1549 and 2047 bytes */                                                                                                                                        
#define USPLUS100G_STAT_TX_PACKET_2048_4095_BYTES                  0x0568 /**< TX number of good and bad packets that contain between 2048 and 4095 bytes */           
#define USPLUS100G_STAT_TX_PACKET_4096_8191_BYTES                  0x0570 /**< TX number of good and bad packets that contain between 4096 and 8191 bytes */           
#define USPLUS100G_STAT_TX_PACKET_8192_9215_BYTES                  0x0578 /**< TX number of good and bad packets that contain between 8192 and 9215 bytes */           
#define USPLUS100G_STAT_TX_PACKET_LARGE                            0x0580 /**< TX number of all packets that are more than 9215 bytes long */                          
#define USPLUS100G_STAT_TX_PACKET_SMALL                            0x0588 /**< TX number of all packets that are less than 64 bytes long */                            
#define USPLUS100G_STAT_TX_BAD_FCS                                 0x05B8 /**< TX number of packets greater than 64 bytes that have FCS errors */                      
#define USPLUS100G_STAT_TX_UNICAST                                 0x05D0 /**< TX number of good unicast packets */                                                    
#define USPLUS100G_STAT_TX_MULTICAST                               0x05D8 /**< TX number of good multicast packets */                                                  
#define USPLUS100G_STAT_TX_BROADCAST                               0x05E0 /**< TX number of good broadcast packets */                                                  
#define USPLUS100G_STAT_TX_VLAN                                    0x05E8 /**< TX number of good 802.1Q tagged VLAN packets */                                         
#define USPLUS100G_STAT_TX_PAUSE                                   0x05F0 /**< TX number of 802.3x MAC Pause Packet with good FCS */                                   
#define USPLUS100G_STAT_TX_USER_PAUSE                              0x05F8 /**< TX number of priority based pause packets with good FCS */                              
#define USPLUS100G_STAT_RX_TOTAL_PACKETS                           0x0608 /**< RX number of bytes */                                                                   
#define USPLUS100G_STAT_RX_TOTAL_GOOD_PACKETS                      0x0610 /**< RX number of good packets packets */                                                    
#define USPLUS100G_STAT_RX_TOTAL_BYTES                             0x0618 /**< RX number of bytes */                                                                   
#define USPLUS100G_STAT_RX_TOTAL_GOOD_BYTES                        0x0620 /**< RX number of good bytes */                                                              
#define USPLUS100G_STAT_RX_PACKET_64_BYTES                         0x0628 /**< RX number of good and bad packets that contain 64 bytes */                              
#define USPLUS100G_STAT_RX_PACKET_65_127_BYTES                     0x0630 /**< RX number of good and bad packets that contain between 65 and 127 bytes */              
#define USPLUS100G_STAT_RX_PACKET_128_255_BYTES                    0x0638 /**< RX number of good and bad packets that contain between 128 and 255 bytes */             
#define USPLUS100G_STAT_RX_PACKET_256_511_BYTES                    0x0640 /**< RX number of good and bad packets that contain between 256 and 511 bytes */             
#define USPLUS100G_STAT_RX_PACKET_512_1023_BYTES                   0x0648 /**< RX number of good and bad packets that contain between 512 and 1023 bytes */            
#define USPLUS100G_STAT_RX_PACKET_1024_1518_BYTES                  0x0650 /**< RX number of good and bad packets that contain between 1024 and 1518 bytes */           
#define USPLUS100G_STAT_RX_PACKET_1519_1522_BYTES                  0x0658 /**< RX number of good and bad packets that contain between 1519 and 1522 bytes */           
#define USPLUS100G_STAT_RX_PACKET_1523_1548_BYTES                  0x0660 /**< RX number of good and bad packets that contain between 1523 and 1548 bytes */           
#define USPLUS100G_STAT_RX_PACKET_1549_2047_BYTES                  0x0668 /**< RX number of good and bad packets that contain between 1549 and 2047 bytes */           
#define USPLUS100G_STAT_RX_PACKET_2048_4095_BYTES                  0x0670 /**< RX number of good and bad packets that contain between 2048 and 4095 bytes */           
#define USPLUS100G_STAT_RX_PACKET_4096_8191_BYTES                  0x0678 /**< RX number of good and bad packets that contain between 4096 and 8191 bytes */           
#define USPLUS100G_STAT_RX_PACKET_8192_9215_BYTES                  0x0680 /**< RX number of good and bad packets that contain between 8192 and 9215 bytes */           
#define USPLUS100G_STAT_RX_PACKET_LARGE                            0x0688 /**< RX number of all packets that are more than 9215 bytes long */                          
#define USPLUS100G_STAT_RX_PACKET_SMALL                            0x0690 /**< RX number of all packets that are less than 64 bytes long */                            
#define USPLUS100G_STAT_RX_UNDERSIZE                               0x0698 /**< RX number of packets shorter than ctl_rx_min_packet_len with good FCS */                
#define USPLUS100G_STAT_RX_FRAGMENT                                0x06A0 /**< RX number of packets shorter than ctl_rx_min_packet_len with bad FCS */                                                                
#define USPLUS100G_STAT_RX_OVERSIZE                                0x06A8 /**< RX number of packets longer than ctl_rx_max_packet_len with good FCS */                 
#define USPLUS100G_STAT_RX_TOOLONG                                 0x06B0 /**< RX number of packets longer than ctl_rx_max_packet_len with good and bad FCS */               
#define USPLUS100G_STAT_RX_JABBER                                  0x06B8 /**< RX number of packets longer than ctl_rx_max_packet_len with bad FCS */                  
#define USPLUS100G_STAT_RX_BAD_FCS                                 0x06C0 /**< RX number of packets with bad FCS, but not a stomped FCS */                             
#define USPLUS100G_STAT_RX_PACKET_BAD_FCS                          0x06C8 /**< RX number of packets between 64 and ctl_rx_max_packet_len with FCS errors */                                                       
#define USPLUS100G_STAT_RX_STOMPED_FCS                             0x06D0 /**< RX number of packets with a stomped FCS */                                              
#define USPLUS100G_STAT_RX_UNICAST                                 0x06D8 /**< RX number of good unicast packets */                                                    
#define USPLUS100G_STAT_RX_MULTICAST                               0x06E0 /**< RX number of good multicast packets */                                                  
#define USPLUS100G_STAT_RX_BROADCAST                               0x06E8 /**< RX number of good broadcast packets */                                                  
#define USPLUS100G_STAT_RX_VLAN                                    0x06F0 /**< RX number of good 802.1Q tagged VLAN packets */                                         
#define USPLUS100G_STAT_RX_PAUSE                                   0x06F8 /**< RX number of 802.3x MAC Pause Packet with good FCS */                                   
#define USPLUS100G_STAT_RX_USER_PAUSE                              0x0700 /**< RX number of priority based pause packets with good FCS */                              
#define USPLUS100G_STAT_RX_INRANGEERR                              0x0708 /**< RX number of packets with Length field error but with good FCS */                       
#define USPLUS100G_STAT_RX_TRUNCATED                               0x0710 /**< RX number of truncated packets due to the length exceeding ctl_rx_max_packet_len[14:0] */      
#define USPLUS100G_STAT_OTN_TX_JABBER                              0x0718 /**< TX number of packets of OTN interface longer than ctl_rx_max_packet_len with bad FCS */
#define USPLUS100G_STAT_OTN_TX_OVERSIZE                            0x0720 /**< TX number of packets of OTN interface longer than ctl_rx_max_packet_len with good FCS */
#define USPLUS100G_STAT_OTN_TX_UNDERSIZE                           0x0728 /**< TX number of packets of OTN interface shorter than ctl_rx_min_packet_len with good FCS */
#define USPLUS100G_STAT_OTN_TX_TOOLONG                             0x0730 /**< TX number of packets of OTN interface longer than ctl_rx_max_packet_len with good and bad FCS */
#define USPLUS100G_STAT_OTN_TX_FRAGMENT                            0x0738 /**< TX number of packets of OTN interface shorter than ctl_rx_min_packet_len with bad FCS */
#define USPLUS100G_STAT_OTN_TX_PACKET_BAD_FCS                      0x0740 /**< TX number of packets of OTN interface between 64 and ctl_rx_max_packet_len with FCS errors */                                                       
#define USPLUS100G_STAT_OTN_TX_STOMPED_FCS                         0x0748 /**< TX number of packets of OTN interface with a stomped FCS */
#define USPLUS100G_STAT_OTN_TX_BAD_CODE                            0x0750 /**< Number of 64B/66B code violations */

#define USPLUS100G_STAT_RX_RSFEC_CORRECTED_CW_INC                  0x1008 /**< Indicates when High that the RS decoder in the core successfully corrected an RS-FEC code word @note Currently not supported  */ 
#define USPLUS100G_STAT_RX_RSFEC_UNCORRECTED_CW_INC                0x1010 /**< Indicates when High that the RS decoder in the core failed to correct an RS-FEC code word @note Currently not supported */
#define USPLUS100G_STAT_RX_RSFEC_ERR_COUNT0_INC                    0x101C /**< RX number of symbol errors detected on lane 0 @note Currently not supported  */
#define USPLUS100G_STAT_RX_RSFEC_ERR_COUNT1_INC                    0x1024 /**< RX number of symbol errors detected on lane 1 @note Currently not supported  */
#define USPLUS100G_STAT_RX_RSFEC_ERR_COUNT2_INC                    0x102C /**< RX number of symbol errors detected on lane 2 @note Currently not supported  */
#define USPLUS100G_STAT_RX_RSFEC_ERR_COUNT3_INC                    0x1034 /**< RX number of symbol errors detected on lane 3 @note Currently not supported  */
#define USPLUS100G_STAT_RX_RSFEC_CW_INC                            0x103C /**< Indicates when High that cw_inc flags are valid for each corrected/uncorrected RS-FEC code word @note Currently not supported  */

#define USPLUS100G_MAX_OFFSET             USPLUS100G_RSFEC_CONFIG_ENABLE
#define USPLUS100G_STAT_INVALID_OFFSET    0xFFFF
/** @name Registers Masks
 *
 * Register masks for usplus100g register fields
 * @{
 */
/* All register masks */

/* GT reset masks */
#define USPLUS100G_GT_RESET_MASK 0x1

/* RESET_REG masks */
#define USPLUS100G_USR_RX_SERDES_RESET_MASK 0x3ff
#define USPLUS100G_RX_CORE_RESET_MASK 0x40000000
#define USPLUS100G_TX_CORE_RESET_MASK 0x80000000

/* CONFIGURATION_TX_FLOW_CONTROL_CONTROL_REG1 masks */
#define USPLUS100G_CONFIG_TX_FLOW_CONTROL_REG1_MASK 0x1FF

#define USPLUS100G_CTL_RX_ENABLE_GCP_BIT   10
#define USPLUS100G_CTL_RX_ENABLE_PCP_BIT   11
#define USPLUS100G_CTL_RX_ENABLE_GPP_BIT   12
#define USPLUS100G_CTL_RX_ENABLE_PPP_BIT   13

#define USPLUS100G_CTL_RX_ENABLE_GCP_BIT_MASK   0x400
#define USPLUS100G_CTL_RX_ENABLE_PCP_BIT_MASK   0x800
#define USPLUS100G_CTL_RX_ENABLE_GPP_BIT_MASK   0x1000
#define USPLUS100G_CTL_RX_ENABLE_PPP_BIT_MASK   0x2000


/* CONFIGURATION_RX_FLOW_CONTROL_CONTROL_REG2 masks */
#define USPLUS100G_CTL_RX_CHECK_MCAST_GCP_BIT 0  
#define USPLUS100G_CTL_RX_CHECK_UCAST_GCP_BIT 1  
#define USPLUS100G_CTL_RX_CHECK_SA_GCP_BIT 2  
#define USPLUS100G_CTL_RX_CHECK_ETYPE_GCP_BIT 3  
#define USPLUS100G_CTL_RX_CHECK_OPCODE_GCP_BIT 4 

#define USPLUS100G_CTL_RX_CHECK_MCAST_PCP_BIT 5  
#define USPLUS100G_CTL_RX_CHECK_UCAST_PCP_BIT 6  
#define USPLUS100G_CTL_RX_CHECK_SA_PCP_BIT 7  
#define USPLUS100G_CTL_RX_CHECK_ETYPE_PCP_BIT 8  
#define USPLUS100G_CTL_RX_CHECK_OPCODE_PCP_BIT 9  

#define USPLUS100G_CTL_RX_CHECK_MCAST_GPP_BIT 10 
#define USPLUS100G_CTL_RX_CHECK_UCAST_GPP_BIT 11 
#define USPLUS100G_CTL_RX_CHECK_SA_GPP_BIT 12 
#define USPLUS100G_CTL_RX_CHECK_ETYPE_GPP_BIT 13 
#define USPLUS100G_CTL_RX_CHECK_OPCODE_GPP_BIT 14 

#define USPLUS100G_CTL_RX_CHECK_OPCODE_PPP_BIT 15 
#define USPLUS100G_CTL_RX_CHECK_MCAST_PPP_BIT 16 
#define USPLUS100G_CTL_RX_CHECK_UCAST_PPP_BIT 17 
#define USPLUS100G_CTL_RX_CHECK_SA_PPP_BIT 18 
#define USPLUS100G_CTL_RX_CHECK_ETYPE_PPP_BIT 19 

#define USPLUS100G_CTL_RX_CHECK_MCAST_GCP_BIT_MASK 0x1  
#define USPLUS100G_CTL_RX_CHECK_UCAST_GCP_BIT_MASK 0x2  
#define USPLUS100G_CTL_RX_CHECK_SA_GCP_BIT_MASK 0x4  
#define USPLUS100G_CTL_RX_CHECK_ETYPE_GCP_BIT_MASK 0x8  
#define USPLUS100G_CTL_RX_CHECK_OPCODE_GCP_BIT_MASK 0x10

#define USPLUS100G_CTL_RX_CHECK_MCAST_PCP_BIT_MASK 0x20
#define USPLUS100G_CTL_RX_CHECK_UCAST_PCP_BIT_MASK 0x40 
#define USPLUS100G_CTL_RX_CHECK_SA_PCP_BIT_MASK 0x80  
#define USPLUS100G_CTL_RX_CHECK_ETYPE_PCP_BIT_MASK 0x100  
#define USPLUS100G_CTL_RX_CHECK_OPCODE_PCP_BIT_MASK 0x200

#define USPLUS100G_CTL_RX_CHECK_MCAST_GPP_BIT_MASK 0x400 
#define USPLUS100G_CTL_RX_CHECK_UCAST_GPP_BIT_MASK 0x800 
#define USPLUS100G_CTL_RX_CHECK_SA_GPP_BIT_MASK 0x1000 
#define USPLUS100G_CTL_RX_CHECK_ETYPE_GPP_BIT_MASK 0x2000 
#define USPLUS100G_CTL_RX_CHECK_OPCODE_GPP_BIT_MASK 0x4000 

#define USPLUS100G_CTL_RX_CHECK_OPCODE_PPP_BIT_MASK 0x8000 
#define USPLUS100G_CTL_RX_CHECK_MCAST_PPP_BIT_MASK 0x10000 
#define USPLUS100G_CTL_RX_CHECK_UCAST_PPP_BIT_MASK 0x20000 
#define USPLUS100G_CTL_RX_CHECK_SA_PPP_BIT_MASK 0x40000 
#define USPLUS100G_CTL_RX_CHECK_ETYPE_PPP_BIT_MASK 0x80000 


#define USPLUS100G_CAUI_MODE_MASK    0x1

/* CONFIGURATION_TX_REG1 masks */
#define USPLUS100G_CTL_TX_ENABLE_MASK 0x1
#define USPLUS100G_CTL_TX_ENABLE_EN_BIT 0
#define USPLUS100G_TX_TEST_PATTERN_EN_BIT 16
#define USPLUS100G_TX_TEST_PATTERN_EN_BIT_MASK 0x10000

#define USPLUS100G_CTL_TX_SEND_RFI_EN_BIT 4 
#define USPLUS100G_CTL_TX_SEND_RFI_EN_BIT_MASK 0x10
#define USPLUS100G_CTL_TX_SEND_IDLE_EN_BIT 5
#define USPLUS100G_CTL_TX_SEND_IDLE_EN_BIT_MASK 0x20


#define USPLUS100G_TICK_REGISTER_EN_MASK 0x1

/* Rx flow control masks */
#define USPLUS100G_CTL_RX_PAUSE_ENABLE_MASK 0x1FF
#define USPLUS100G_CTL_RX_PAUSE_ACK_ENABLE_MASK 0xFF8000
#define USPLUS100G_CTL_RX_PAUSE_ACK_ENABLE_BIT  15

/* CONFIGURATION_RX_REG1 masks and shifts */
#define USPLUS100G_CTL_RX_ENABLE_MASK 0x1
#define USPLUS100G_CTL_RX_ENABLE_EN_BIT 0
#define USPLUS100G_CTL_RX_FORCE_RESYNC_EN_BIT 7
#define USPLUS100G_CTL_RX_FORCE_RESYNC_EN_BIT_MASK 0x80
#define USPLUS100G_RX_TEST_PATTERN_EN_BIT 8
#define USPLUS100G_RX_TEST_PATTERN_EN_BIT_MASK 0x100


#define USPLUS100G_STAT_RX_BLOCK_LOCK_MASK 0xfffff
#define USPLUS100G_STAT_RX_SYNCED_MASK 0xfffff
#define USPLUS100G_STAT_RX_SYNCED_ERR_MASK  0xfffff 
#define USPLUS100G_STAT_RX_MF_ERR_MASK 0xfffff
#define USPLUS100G_STAT_RX_MF_LEN_ERR_MASK 0xfffff
#define USPLUS100G_STAT_RX_MF_REPEAT_ERR_MASK 0xfffff

/* STAT_RX_STATUS_OFFSET masks and shifts */

#define USPLUS100G_STAT_RX_MASK 0x1
#define USPLUS100G_STAT_RX_ALIGNED_MASK 0x2
#define USPLUS100G_STAT_RX_MISALIGNED_MASK    0x4
#define USPLUS100G_STAT_RX_ALIGNED_ERR_MASK   0x8
#define USPLUS100G_STAT_RX_HI_BER_MASK    0x10
#define USPLUS100G_STAT_RX_REMOTE_FAULT_MASK 0x20
#define USPLUS100G_STAT_RX_LOCAL_FAULT_MASK 0x40 
#define USPLUS100G_STAT_RX_INTERNAL_LOCAL_FAULT_MASK 0x80 
#define USPLUS100G_STAT_RX_RECEIVED_LOCAL_FAULT_MASK 0x100 
#define USPLUS100G_STAT_RX_TEST_PATTERN_MISMATCH_MASK 0xE00
#define USPLUS100G_STAT_RX_BAD_PREAMBLE_MASK 0x1000
#define USPLUS100G_STAT_RX_BAD_SFD_MASK 0x2000 
#define USPLUS100G_STAT_RX_GOT_SIGNAL_OS_MASK 0x4000

#define USPLUS100G_STAT_RX_STS_MASK 0x7fff
#define USPLUS100G_STAT_RX_STATUS_BIT 0 
#define USPLUS100G_STAT_RX_ALIGNED_BIT 1 
#define USPLUS100G_STAT_RX_MISALIGNED_BIT 2 
#define USPLUS100G_STAT_RX_ALIGNED_ERR_BIT 3
#define USPLUS100G_STAT_RX_HI_BER_BIT 4
#define USPLUS100G_STAT_RX_REMOTE_FAULT_BIT 5 
#define USPLUS100G_STAT_RX_LOCAL_FAULT_BIT 6 
#define USPLUS100G_STAT_RX_INTERNAL_LOCAL_FAULT_BIT 7 
#define USPLUS100G_STAT_RX_RECEIVED_LOCAL_FAULT_BIT 8
#define USPLUS100G_STAT_RX_TEST_PATTERN_MISMATCH_BIT 9 
#define USPLUS100G_STAT_RX_BAD_PREAMBLE_BIT 12
#define USPLUS100G_STAT_RX_BAD_SFD_BIT 13
#define USPLUS100G_STAT_RX_GOT_SIGNAL_OS_BIT 14


/* STAT_TX_STATUS_REG masks and shifts */
#define USPLUS100G_STAT_TX_LOCAL_FAULT_MASK  0x1
#define USPLUS100G_STAT_TX_LOCAL_FAULT_BIT  0

/* STAT_STATUS_REG1 masks and shifts */
#define USPLUS100G_STAT_TX_PTP_FIFO_R_ERR_MASK 0x10
#define USPLUS100G_STAT_TX_PTP_FIFO_R_ERR_BIT 4
#define USPLUS100G_STAT_TX_PTP_FIFO_W_ERR_MASK 0x20
#define USPLUS100G_STAT_TX_PTP_FIFO_W_ERR_BIT 5

/* STAT_RX_LANE_DEMUXED masks and shifts */
#define USPLUS100G_STAT_RX_VL_DEMUXED_MASK 0xfffff

/* STAT_RX_PCS_LANE_NUM_REG */
#define USPLUS100G_STAT_RX_VL_NUM_0_MASK 0x3FFFFFFF
#define USPLUS100G_STAT_RX_VL_NUM_6_MASK 0x3FFFFFFF
#define USPLUS100G_STAT_RX_VL_NUM_12_MASK 0x3FFFFFFF
#define USPLUS100G_STAT_RX_VL_NUM_18_MASK 0x1FF
#define USPLUS100G_STAT_RX_VL_NUM_MASK   0x1F

/* CONFIGURATION_TX_BIP_OVERRIDE masks */
#define USPLUS100G_CTL_TX_LANE0_VLM_BIP7_OVERRIDE_VALUE_MASK 0xFF 
#define USPLUS100G_CTL_TX_LANE0_VLM_BIP7_OVERRIDE_BIT 8
#define USPLUS100G_CTL_TX_LANE0_VLM_BIP7_OVERRIDE_BIT_MASK 0x100
/* STAT_RX_BIP_OVERRIDE masks */
#define USPLUS100G_STAT_RX_BIP_OVERRIDE_BIP7_MASK 0xFF
#define USPLUS100G_STAT_RX_BIP_OVERRIDE_VALID_MASK  0x100
#define USPLUS100G_STAT_RX_BIP_OVERRIDE_VALID_BIT 8

#define USPLUS100G_STAT_RX_VL_NUMBER_0_BIT 0   
#define USPLUS100G_STAT_RX_VL_NUMBER_1_BIT 5   
#define USPLUS100G_STAT_RX_VL_NUMBER_2_BIT 10 
#define USPLUS100G_STAT_RX_VL_NUMBER_3_BIT 15 
#define USPLUS100G_STAT_RX_VL_NUMBER_4_BIT 20 
#define USPLUS100G_STAT_RX_VL_NUMBER_5_BIT 25 

#define USPLUS100G_STAT_RX_VL_NUMBER_6_BIT  0   
#define USPLUS100G_STAT_RX_VL_NUMBER_7_BIT  5   
#define USPLUS100G_STAT_RX_VL_NUMBER_8_BIT  10 
#define USPLUS100G_STAT_RX_VL_NUMBER_9_BIT  15 
#define USPLUS100G_STAT_RX_VL_NUMBER_10_BIT 20 
#define USPLUS100G_STAT_RX_VL_NUMBER_11_BIT 25 

#define USPLUS100G_STAT_RX_VL_NUMBER_12 0   
#define USPLUS100G_STAT_RX_VL_NUMBER_13 5   
#define USPLUS100G_STAT_RX_VL_NUMBER_14 10 
#define USPLUS100G_STAT_RX_VL_NUMBER_15 15 
#define USPLUS100G_STAT_RX_VL_NUMBER_16 20 
#define USPLUS100G_STAT_RX_VL_NUMBER_17 25 
#define USPLUS100G_STAT_RX_VL_NUMBER_18 0   
#define USPLUS100G_STAT_RX_VL_NUMBER_19 5   

/** @struct usplus100g_ctx_t
 *  usplus100g context structure
 */
typedef struct usplus100g_ctx_t {
    void            *devo;
    eth_brdspec_t   brdspec;
    eth_counter_t   *counters;
    b_u32           counters_num;
} usplus100g_ctx_t;


static ccl_err_t    _ret;

static device_attr_t _attrs[] = {
    {
        .name = "name", 
        .type = eDEVMAN_ATTR_STRING, .maxnum = 1, 
        .offset = offsetof(struct usplus100g_ctx_t, brdspec.name),
        .size = DEVMAN_DEVNAME_LEN,
        .format = "%s"
    },
    {
        .name = "offset", 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset = offsetof(struct usplus100g_ctx_t, brdspec.offset),
        .format = "0x%08x"
    },
    {
        .name = "gt_reset", .id = eUSPLUS100G_GT_RESET_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_GT_RESET_REG
    },
    {
        .name = "reset", .id = eUSPLUS100G_RESET_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_RESET_REG
    },
    {
        .name = "switch_core_mode", .id = eUSPLUS100G_SWITCH_CORE_MODE_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_SWITCH_CORE_MODE_REG
    },
    {
        .name = "config_tx", .id = eUSPLUS100G_CONFIGURATION_TX_REG1, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_CONFIGURATION_TX_REG1
    },
    {
        .name = "config_rx", .id = eUSPLUS100G_CONFIGURATION_RX_REG1, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_CONFIGURATION_RX_REG1
    },
    {
        .name = "core_mode", .id = eUSPLUS100G_CORE_MODE_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_CORE_MODE_REG,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "core_version", .id = eUSPLUS100G_CORE_VERSION_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_CORE_VERSION_REG,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_bip_override", .id = eUSPLUS100G_CONFIGURATION_TX_BIP_OVERRIDE, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_CONFIGURATION_TX_BIP_OVERRIDE
    },
    {
        .name = "tx_flow_ctl", .id = eUSPLUS100G_CONFIGURATION_TX_FLOW_CONTROL_CONTROL_REG1, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_CONFIGURATION_TX_FLOW_CONTROL_CONTROL_REG1
    },
    {
        .name = "tx_flow_ctl_refresh", .id = eUSPLUS100G_CONFIGURATION_TX_FLOW_CONTROL_REFRESH_REG1, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 5, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_CONFIGURATION_TX_FLOW_CONTROL_REFRESH_REG1
    },
    {
        .name = "tx_flow_ctl_quanta", .id = eUSPLUS100G_CONFIGURATION_TX_FLOW_CONTROL_QUANTA_REG1, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 5, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_CONFIGURATION_TX_FLOW_CONTROL_QUANTA_REG1
    },
    {
        .name = "tx_otn_pkt_len", .id = eUSPLUS100G_CONFIGURATION_TX_OTN_PKT_LEN_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_CONFIGURATION_TX_OTN_PKT_LEN_REG
    },
    {
        .name = "tx_otn_ctl", .id = eUSPLUS100G_CONFIGURATION_TX_OTN_CTL_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_CONFIGURATION_TX_OTN_CTL_REG
    },
    {
        .name = "rx_flow_ctl1", .id = eUSPLUS100G_CONFIGURATION_RX_FLOW_CONTROL_CONTROL_REG1, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_CONFIGURATION_RX_FLOW_CONTROL_CONTROL_REG1
    },
    {
        .name = "rx_flow_ctl2", .id = eUSPLUS100G_CONFIGURATION_RX_FLOW_CONTROL_CONTROL_REG2, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_CONFIGURATION_RX_FLOW_CONTROL_CONTROL_REG2
    },
    {
        .name = "gt_loopback", .id = eUSPLUS100G_GT_LOOPBACK_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_GT_LOOPBACK_REG
    },
    {
        .name = "an_ctl1", .id = eUSPLUS100G_CONFIGURATION_AN_CONTROL_REG1, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_CONFIGURATION_AN_CONTROL_REG1
    },
    {
        .name = "an_ctl2", .id = eUSPLUS100G_CONFIGURATION_AN_CONTROL_REG2, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_CONFIGURATION_AN_CONTROL_REG2
    },
    {
        .name = "an_ability_ctl", .id = eUSPLUS100G_CONFIGURATION_AN_ABILITY, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_CONFIGURATION_AN_ABILITY
    },
    {
        .name = "lt_ctl", .id = eUSPLUS100G_CONFIGURATION_LT_CONTROL_REG1, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_CONFIGURATION_LT_CONTROL_REG1
    },
    {
        .name = "lt_trained", .id = eUSPLUS100G_CONFIGURATION_LT_TRAINED_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_CONFIGURATION_LT_TRAINED_REG
    },
    {
        .name = "lt_preset", .id = eUSPLUS100G_CONFIGURATION_LT_PRESET_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_CONFIGURATION_LT_PRESET_REG
    },
    {
        .name = "lt_init", .id = eUSPLUS100G_CONFIGURATION_LT_INIT_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_CONFIGURATION_LT_INIT_REG
    },
    {
        .name = "lt_seed", .id = eUSPLUS100G_CONFIGURATION_LT_SEED_REG0, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 2, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_CONFIGURATION_LT_SEED_REG0
    },
    {
        .name = "lt_coeff_ctl", .id = eUSPLUS100G_CONFIGURATION_LT_COEFFICIENT_REG0, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 2, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_CONFIGURATION_LT_COEFFICIENT_REG0
    },
    {
        .name = "indication_correction", .id = eUSPLUS100G_RSFEC_CONFIG_INDICATION_CORRECTION, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_RSFEC_CONFIG_INDICATION_CORRECTION
    },
    {
        .name = "rsfec_config_en", .id = eUSPLUS100G_RSFEC_CONFIG_ENABLE, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_RSFEC_CONFIG_ENABLE
    },
    {
        .name = "tx_status", .id = eUSPLUS100G_STAT_TX_STATUS_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_TX_STATUS_REG,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_status", .id = eUSPLUS100G_STAT_RX_STATUS_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_RX_STATUS_REG,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "fifo_status", .id = eUSPLUS100G_STAT_STATUS_REG1, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_STATUS_REG1,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_block_lock", .id = eUSPLUS100G_STAT_RX_BLOCK_LOCK_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_RX_BLOCK_LOCK_REG,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_lane_sync", .id = eUSPLUS100G_STAT_RX_LANE_SYNC_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_RX_LANE_SYNC_REG,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_lane_sync_err", .id = eUSPLUS100G_STAT_RX_LANE_SYNC_ERR_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_RX_LANE_SYNC_ERR_REG,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_lane_am_err", .id = eUSPLUS100G_STAT_RX_AM_ERR_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_RX_AM_ERR_REG,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_lane_am_len_err", .id = eUSPLUS100G_STAT_RX_AM_LEN_ERR_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_RX_AM_LEN_ERR_REG,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_lane_am_repeat_err", .id = eUSPLUS100G_STAT_RX_AM_REPEAT_ERR_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_RX_AM_REPEAT_ERR_REG,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_pcsl_demuxed", .id = eUSPLUS100G_STAT_RX_PCSL_DEMUXED_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_RX_PCSL_DEMUXED_REG,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_pcs_lane_num", .id = eUSPLUS100G_STAT_RX_PCS_LANE_NUM_REG1, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 4, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_RX_PCS_LANE_NUM_REG1,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_bip_override", .id = eUSPLUS100G_STAT_RX_BIP_OVERRIDE_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_RX_BIP_OVERRIDE_REG,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_otn_status", .id = eUSPLUS100G_STAT_TX_OTN_STATUS_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_TX_OTN_STATUS_REG,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "an_status", .id = eUSPLUS100G_STAT_AN_STATUS_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_AN_STATUS_REG,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "an_ability", .id = eUSPLUS100G_STAT_AN_ABILITY_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_AN_ABILITY_REG,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "an_link_ctl", .id = eUSPLUS100G_STAT_AN_LINK_CTL_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_AN_LINK_CTL_REG,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "lt_status1", .id = eUSPLUS100G_STAT_LT_STATUS_REG1, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_LT_STATUS_REG1,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "lt_status2", .id = eUSPLUS100G_STAT_LT_STATUS_REG2, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_LT_STATUS_REG2,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "lt_status3", .id = eUSPLUS100G_STAT_LT_STATUS_REG3, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_LT_STATUS_REG3,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "lt_status4", .id = eUSPLUS100G_STAT_LT_STATUS_REG4, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_LT_STATUS_REG4,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "lt_coeff", .id = eUSPLUS100G_STAT_LT_COEFFICIENT0_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 2, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_LT_COEFFICIENT0_REG,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tick", .id = eUSPLUS100G_TICK_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_TICK_REG,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "stat_cycle", .id = eUSPLUS100G_STAT_CYCLE_COUNT, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_CYCLE_COUNT,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_rx_bip_err", .id = eUSPLUS100G_STAT_RX_BIP_ERR_0, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 20, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_RX_BIP_ERR_0,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_rx_framing_err", .id = eUSPLUS100G_STAT_RX_FRAMING_ERR0, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 20, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_RX_FRAMING_ERR0,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_rx_bad_code", .id = eUSPLUS100G_STAT_RX_BAD_CODE, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_RX_BAD_CODE,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_rx_frame_err", .id = eUSPLUS100G_STAT_TX_FRAME_ERROR, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_TX_FRAME_ERROR,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_tx_total_pkts", .id = eUSPLUS100G_STAT_TX_TOTAL_PACKETS, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_TX_TOTAL_PACKETS,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_tx_total_good_pkts", .id = eUSPLUS100G_STAT_TX_TOTAL_GOOD_PACKETS, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_TX_TOTAL_GOOD_PACKETS,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_tx_total_bytes", .id = eUSPLUS100G_STAT_TX_TOTAL_BYTES, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_TX_TOTAL_BYTES,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_tx_total_good_bytes", .id = eUSPLUS100G_STAT_TX_TOTAL_GOOD_BYTES, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_TX_TOTAL_GOOD_BYTES,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_tx_pkt_64_bytes", .id = eUSPLUS100G_STAT_TX_PACKET_64_BYTES, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_TX_PACKET_64_BYTES,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_tx_pkt_65_127_bytes", .id = eUSPLUS100G_STAT_TX_PACKET_65_127_BYTES, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_TX_PACKET_65_127_BYTES,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_tx_pkt_128_255_bytes", .id = eUSPLUS100G_STAT_TX_PACKET_128_255_BYTES, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_TX_PACKET_128_255_BYTES,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_tx_pkt_256_511_bytes", .id = eUSPLUS100G_STAT_TX_PACKET_256_511_BYTES, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_TX_PACKET_256_511_BYTES,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_tx_pkt_512_1023_bytes", .id = eUSPLUS100G_STAT_TX_PACKET_512_1023_BYTES, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_TX_PACKET_512_1023_BYTES,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_tx_pkt_1024_1518_bytes", .id = eUSPLUS100G_STAT_TX_PACKET_1024_1518_BYTES, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_TX_PACKET_1024_1518_BYTES,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_tx_pkt_1519_1522_bytes", .id = eUSPLUS100G_STAT_TX_PACKET_1519_1522_BYTES, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_TX_PACKET_1519_1522_BYTES,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_tx_pkt_1523_1548_bytes", .id = eUSPLUS100G_STAT_TX_PACKET_1523_1548_BYTES, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_TX_PACKET_1523_1548_BYTES,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_tx_pkt_1549_2047_bytes", .id = eUSPLUS100G_STAT_TX_PACKET_1549_2047_BYTES, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_TX_PACKET_1549_2047_BYTES,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_tx_pkt_2048_4095_bytes", .id = eUSPLUS100G_STAT_TX_PACKET_2048_4095_BYTES, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_TX_PACKET_2048_4095_BYTES,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_tx_pkt_4096_8191_bytes", .id = eUSPLUS100G_STAT_TX_PACKET_4096_8191_BYTES, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_TX_PACKET_4096_8191_BYTES,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_tx_pkt_8192_9215_bytes", .id = eUSPLUS100G_STAT_TX_PACKET_8192_9215_BYTES, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_TX_PACKET_8192_9215_BYTES,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_tx_pkt_large", .id = eUSPLUS100G_STAT_TX_PACKET_LARGE, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_TX_PACKET_LARGE,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_tx_pkt_small", .id = eUSPLUS100G_STAT_TX_PACKET_SMALL, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_TX_PACKET_SMALL,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_tx_bad_fcs", .id = eUSPLUS100G_STAT_TX_BAD_FCS, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_TX_BAD_FCS,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_tx_ucast", .id = eUSPLUS100G_STAT_TX_UNICAST, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_TX_UNICAST,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_tx_mcast", .id = eUSPLUS100G_STAT_TX_MULTICAST, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_TX_MULTICAST,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_tx_bcast", .id = eUSPLUS100G_STAT_TX_BROADCAST, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_TX_BROADCAST,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_tx_vlan", .id = eUSPLUS100G_STAT_TX_VLAN, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_TX_VLAN,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_tx_pause", .id = eUSPLUS100G_STAT_TX_PAUSE, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_TX_PAUSE,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_tx_user_pause", .id = eUSPLUS100G_STAT_TX_USER_PAUSE, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_TX_USER_PAUSE,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_rx_total_pkts", .id = eUSPLUS100G_STAT_RX_TOTAL_PACKETS, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_RX_TOTAL_PACKETS,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_rx_total_good_pkts", .id = eUSPLUS100G_STAT_RX_TOTAL_GOOD_PACKETS, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_RX_TOTAL_GOOD_PACKETS,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_rx_total_bytes", .id = eUSPLUS100G_STAT_RX_TOTAL_BYTES, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_RX_TOTAL_BYTES,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_rx_total_good_bytes", .id = eUSPLUS100G_STAT_RX_TOTAL_GOOD_BYTES, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_RX_TOTAL_GOOD_BYTES,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_rx_pkt_64_bytes", .id = eUSPLUS100G_STAT_RX_PACKET_64_BYTES, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_RX_PACKET_64_BYTES,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_rx_pkt_65_127_bytes", .id = eUSPLUS100G_STAT_RX_PACKET_65_127_BYTES, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_RX_PACKET_65_127_BYTES,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_rx_pkt_128_255_bytes", .id = eUSPLUS100G_STAT_RX_PACKET_128_255_BYTES, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_RX_PACKET_128_255_BYTES,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_rx_pkt_256_511_bytes", .id = eUSPLUS100G_STAT_RX_PACKET_256_511_BYTES, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_RX_PACKET_256_511_BYTES,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_rx_pkt_512_1023_bytes", .id = eUSPLUS100G_STAT_RX_PACKET_512_1023_BYTES, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_RX_PACKET_512_1023_BYTES,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_rx_pkt_1024_1518_bytes", .id = eUSPLUS100G_STAT_RX_PACKET_1024_1518_BYTES, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_RX_PACKET_1024_1518_BYTES,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_rx_pkt_1519_1522_bytes", .id = eUSPLUS100G_STAT_RX_PACKET_1519_1522_BYTES, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_RX_PACKET_1519_1522_BYTES,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_rx_pkt_1523_1548_bytes", .id = eUSPLUS100G_STAT_RX_PACKET_1523_1548_BYTES, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_RX_PACKET_1523_1548_BYTES,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_rx_pkt_1549_2047_bytes", .id = eUSPLUS100G_STAT_RX_PACKET_1549_2047_BYTES, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_RX_PACKET_1549_2047_BYTES,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_rx_pkt_2048_4095_bytes", .id = eUSPLUS100G_STAT_RX_PACKET_2048_4095_BYTES, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_RX_PACKET_2048_4095_BYTES,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_rx_pkt_4096_8191_bytes", .id = eUSPLUS100G_STAT_RX_PACKET_4096_8191_BYTES, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_RX_PACKET_4096_8191_BYTES,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_rx_pkt_8192_9215_bytes", .id = eUSPLUS100G_STAT_RX_PACKET_8192_9215_BYTES, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_RX_PACKET_8192_9215_BYTES,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_rx_pkt_large", .id = eUSPLUS100G_STAT_RX_PACKET_LARGE, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_RX_PACKET_LARGE,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_rx_pkt_small", .id = eUSPLUS100G_STAT_RX_PACKET_SMALL, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_RX_PACKET_SMALL,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_rx_undersize", .id = eUSPLUS100G_STAT_RX_UNDERSIZE, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_RX_UNDERSIZE,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_rx_fragment", .id = eUSPLUS100G_STAT_RX_FRAGMENT, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_RX_FRAGMENT,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_rx_oversize", .id = eUSPLUS100G_STAT_RX_OVERSIZE, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_RX_OVERSIZE,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_rx_toolong", .id = eUSPLUS100G_STAT_RX_TOOLONG, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_RX_TOOLONG,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_rx_jabber", .id = eUSPLUS100G_STAT_RX_JABBER, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_RX_JABBER,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_rx_bad_fcs", .id = eUSPLUS100G_STAT_RX_BAD_FCS, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_RX_BAD_FCS,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_rx_pkt_bad_fcs", .id = eUSPLUS100G_STAT_RX_PACKET_BAD_FCS, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_RX_PACKET_BAD_FCS,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_rx_stomped_fcs", .id = eUSPLUS100G_STAT_RX_STOMPED_FCS, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_RX_STOMPED_FCS,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_rx_ucast", .id = eUSPLUS100G_STAT_RX_UNICAST, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_RX_UNICAST,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_rx_mcast", .id = eUSPLUS100G_STAT_RX_MULTICAST, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_RX_MULTICAST,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_rx_bcast", .id = eUSPLUS100G_STAT_RX_BROADCAST, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_RX_BROADCAST,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_rx_vlan", .id = eUSPLUS100G_STAT_RX_VLAN, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_RX_VLAN,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_rx_pause", .id = eUSPLUS100G_STAT_RX_PAUSE, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_RX_PAUSE,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_rx_user_pause", .id = eUSPLUS100G_STAT_RX_USER_PAUSE, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_RX_USER_PAUSE,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_rx_inrangeerr", .id = eUSPLUS100G_STAT_RX_INRANGEERR, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_RX_INRANGEERR,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_rx_truncated", .id = eUSPLUS100G_STAT_RX_TRUNCATED, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_RX_TRUNCATED,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_otn_tx_jabber", .id = eUSPLUS100G_STAT_OTN_TX_JABBER, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_OTN_TX_JABBER,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_otn_tx_oversize", .id = eUSPLUS100G_STAT_OTN_TX_OVERSIZE, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_OTN_TX_OVERSIZE,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_otn_tx_undersize", .id = eUSPLUS100G_STAT_OTN_TX_UNDERSIZE, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_OTN_TX_UNDERSIZE,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_otn_tx_toolong", .id = eUSPLUS100G_STAT_OTN_TX_TOOLONG, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_OTN_TX_TOOLONG,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_otn_tx_fragment", .id = eUSPLUS100G_STAT_OTN_TX_FRAGMENT, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_OTN_TX_FRAGMENT,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_otn_tx_pkt_bad_fcs", .id = eUSPLUS100G_STAT_OTN_TX_PACKET_BAD_FCS, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_OTN_TX_PACKET_BAD_FCS,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_otn_tx_stomped_fcs", .id = eUSPLUS100G_STAT_OTN_TX_STOMPED_FCS, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_OTN_TX_STOMPED_FCS,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "stat_otn_tx_bad_code", .id = eUSPLUS100G_STAT_OTN_TX_BAD_CODE, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = USPLUS100G_STAT_OTN_TX_BAD_CODE,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "read_single_counter", .id = eUSPLUS100G_STAT_SINGLE_COUNTER, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVMAN_IF_MIB_MAX, .size = sizeof(b_u64), 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "property", .id = eUSPLUS100G_IF_PROPERTY, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVMAN_PROP_MAX, .size = 128, 
        .offset_sz = sizeof(b_u32),
    },
    {
        .name = "read_counters", .id = eUSPLUS100G_STAT_COUNTERS, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = sizeof(port_stats_t), 
        .offset_sz = sizeof(b_u8),
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "init", .id = eUSPLUS100G_INIT, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "config_test", .id = eUSPLUS100G_CONFIGURE_TEST, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "run_test", .id = eUSPLUS100G_RUN_TEST, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, 
        .size = sizeof(board_dev_stats_t), 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "counters_reset", .id = eUSPLUS100G_COUNTERS_RESET, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
};

#ifdef MAC_COUNTERS_ENABLED
static mib_counter_map_t _usplus100g_mib_cntr_map[] = {
    {
        .ifmib = DEVMAN_IF_MIB_BYTEREC, .offset = USPLUS100G_STAT_RX_TOTAL_BYTES
    },
    {
        .ifmib = DEVMAN_IF_MIB_BYTSENT, .offset = USPLUS100G_STAT_TX_TOTAL_BYTES
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRAREC, .offset = USPLUS100G_STAT_RX_TOTAL_PACKETS
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRASENT, .offset = USPLUS100G_STAT_TX_TOTAL_PACKETS
    },
    {
        .ifmib = DEVMAN_IF_MIB_TOTALBYTEREC, .offset = USPLUS100G_STAT_RX_TOTAL_BYTES
    },
    {
        .ifmib = DEVMAN_IF_MIB_TOTALFRAMEREC, .offset = USPLUS100G_STAT_TX_TOTAL_BYTES
    },
    {
        .ifmib = DEVMAN_IF_MIB_BRDFRAREC, .offset = USPLUS100G_STAT_RX_BROADCAST
    },
    {
        .ifmib = DEVMAN_IF_MIB_MULFRAREC, .offset = USPLUS100G_STAT_RX_MULTICAST
    },
    {
        .ifmib = DEVMAN_IF_MIB_CRCERR, .offset = USPLUS100G_STAT_RX_BAD_FCS
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRAOVERSIZE, .offset = USPLUS100G_STAT_RX_OVERSIZE
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRAFRAGMENTS, .offset = USPLUS100G_STAT_RX_FRAGMENT
    },
    {
        .ifmib = DEVMAN_IF_MIB_JABBER, .offset = USPLUS100G_STAT_RX_JABBER
    },
    {
        .ifmib = DEVMAN_IF_MIB_COLL, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_LATECOL, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRA64, .offset = USPLUS100G_STAT_RX_PACKET_64_BYTES
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRA65T127, .offset = USPLUS100G_STAT_RX_PACKET_65_127_BYTES
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRA128T255, .offset = USPLUS100G_STAT_RX_PACKET_128_255_BYTES
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRA256T511, .offset = USPLUS100G_STAT_RX_PACKET_256_511_BYTES
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRA512T1023, .offset = USPLUS100G_STAT_RX_PACKET_512_1023_BYTES
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRA1024T1522, .offset = USPLUS100G_STAT_RX_PACKET_1024_1518_BYTES
    },
    {
        .ifmib = DEVMAN_IF_MIB_MACRXERR, .offset = USPLUS100G_STAT_RX_BAD_FCS
    },
    {
        .ifmib = DEVMAN_IF_MIB_DROPPED, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_INUCASTPKT, .offset = USPLUS100G_STAT_RX_UNICAST_LSB
    },
    {
        .ifmib = DEVMAN_IF_MIB_INNOUCASTPKT, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_INERRORS, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_OUTERRORS, .offset = USPLUS100G_STAT_TX_BAD_FCS
    },
    {
        .ifmib = DEVMAN_IF_MIB_OUTNOUCASTPKT, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_OUTUCASTPKT, .offset = USPLUS100G_STAT_TX_UNICAST
    },
    {
        .ifmib = DEVMAN_IF_MIB_MULTIOUT, .offset = USPLUS100G_STAT_TX_MULTICAST
    },
    {
        .ifmib = DEVMAN_IF_MIB_BROADOUT, .offset = USPLUS100G_STAT_TX_BROADCAST
    },
    {
        .ifmib = DEVMAN_IF_MIB_UNDERSIZ, .offset = USPLUS100G_STAT_RX_UNDERSIZE
    },
    {
        .ifmib = DEVMAN_IF_MIB_INUNKNOWNPROTOS, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_ALIGN_ERR, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_FCS_ERR, .offset = USPLUS100G_STAT_RX_BAD_FCS
    },
    {
        .ifmib = DEVMAN_IF_MIB_SQETEST_ERR, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_CSE_ERR, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_SYMBOL_ERR, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_MACTX_ERR, .offset = USPLUS100G_STAT_TX_BAD_FCS
    },
    {
        .ifmib = DEVMAN_IF_MIB_MACRX_ERR, .offset = USPLUS100G_STAT_RX_BAD_FCS
    },
    {
        .ifmib = DEVMAN_IF_MIB_TOOLONGFRA, .offset = USPLUS100G_STAT_RX_TOOLONG
    },
    {
        .ifmib = DEVMAN_IF_MIB_SNGL_COLLISION, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_MULT_COLLISION, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_LATE_COLLISION, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_EXCESS_COLLISION, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_INUNKNOWNOPCODE, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_DEFERREDTX, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_INPAUSE_FRAMES, .offset = USPLUS100G_STAT_RX_PAUSE
    },
    {
        .ifmib = DEVMAN_IF_MIB_OUTPAUSE_FRAMES, .offset = USPLUS100G_STAT_TX_PAUSE
    },
    {
        .ifmib = DEVMAN_IF_MIB_INFRAOVERSIZE, .offset = USPLUS100G_STAT_RX_OVERSIZE
    },
    {
        .ifmib = DEVMAN_IF_MIB_OUTFRAOVERSIZE, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_INFRAFRAGMENTS, .offset = USPLUS100G_STAT_RX_FRAGMENT
    },
    {
        .ifmib = DEVMAN_IF_MIB_OUTFRAFRAGMENTS, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_INJABBERS, .offset = USPLUS100G_STAT_RX_JABBER
    },
    {
        .ifmib = DEVMAN_IF_MIB_OUTJABBERS, .offset = USPLUS100G_STAT_INVALID_OFFSET
    }
};

static eth_counter_t _usplus100g_counters[] = {
    {
        .name = "stat_rx_bip_err", .offset = USPLUS100G_STAT_RX_BIP_ERR_0
    },
    {
        .name = "stat_rx_framing_err", .offset = USPLUS100G_STAT_RX_FRAMING_ERR0
    },
    {
        .name = "stat_rx_bad_code", .offset = USPLUS100G_STAT_RX_BAD_CODE
    },
    {
        .name = "stat_rx_frame_err", .offset = USPLUS100G_STAT_TX_FRAME_ERROR
    },
    {
        .name = "stat_tx_total_pkts", .offset = USPLUS100G_STAT_TX_TOTAL_PACKETS
    },
    {
        .name = "stat_tx_total_good_pkts", .offset = USPLUS100G_STAT_TX_TOTAL_GOOD_PACKETS
    },
    {
        .name = "stat_tx_total_bytes", .offset = USPLUS100G_STAT_TX_TOTAL_BYTES
    },
    {
        .name = "stat_tx_total_good_bytes", .offset = USPLUS100G_STAT_TX_TOTAL_GOOD_BYTES
    },
    {
        .name = "stat_tx_pkt_64_bytes", .offset = USPLUS100G_STAT_TX_PACKET_64_BYTES
    },
    {
        .name = "stat_tx_pkt_65_127_bytes", .offset = USPLUS100G_STAT_TX_PACKET_65_127_BYTES
    },
    {
        .name = "stat_tx_pkt_128_255_bytes", .offset = USPLUS100G_STAT_TX_PACKET_128_255_BYTES
    },
    {
        .name = "stat_tx_pkt_256_511_bytes", .offset = USPLUS100G_STAT_TX_PACKET_256_511_BYTES
    },
    {
        .name = "stat_tx_pkt_512_1023_bytes", .offset = USPLUS100G_STAT_TX_PACKET_512_1023_BYTES
    },
    {
        .name = "stat_tx_pkt_1024_1518_bytes", .offset = USPLUS100G_STAT_TX_PACKET_1024_1518_BYTES
    },
    {
        .name = "stat_tx_pkt_1519_1522_bytes", .offset = USPLUS100G_STAT_TX_PACKET_1519_1522_BYTES
    },
    {
        .name = "stat_tx_pkt_1523_1548_bytes", .offset = USPLUS100G_STAT_TX_PACKET_1523_1548_BYTES
    },
    {
        .name = "stat_tx_pkt_1549_2047_bytes", .offset = USPLUS100G_STAT_TX_PACKET_1549_2047_BYTES
    },
    {
        .name = "stat_tx_pkt_2048_4095_bytes", .offset = USPLUS100G_STAT_TX_PACKET_2048_4095_BYTES
    },
    {
        .name = "stat_tx_pkt_4096_8191_bytes", .offset = USPLUS100G_STAT_TX_PACKET_4096_8191_BYTES
    },
    {
        .name = "stat_tx_pkt_8192_9215_bytes", .offset = USPLUS100G_STAT_TX_PACKET_8192_9215_BYTES
    },
    {
        .name = "stat_tx_pkt_large", .offset = USPLUS100G_STAT_TX_PACKET_LARGE
    },
    {
        .name = "stat_tx_pkt_small", .offset = USPLUS100G_STAT_TX_PACKET_SMALL
    },
    {
        .name = "stat_tx_bad_fcs", .offset = USPLUS100G_STAT_TX_BAD_FCS
    },
    {
        .name = "stat_tx_ucast", .offset = USPLUS100G_STAT_TX_UNICAST
    },
    {
        .name = "stat_tx_mcast", .offset = USPLUS100G_STAT_TX_MULTICAST
    },
    {
        .name = "stat_tx_bcast", .offset = USPLUS100G_STAT_TX_BROADCAST
    },
    {
        .name = "stat_tx_vlan", .offset = USPLUS100G_STAT_TX_VLAN
    },
    {
        .name = "stat_tx_pause", .offset = USPLUS100G_STAT_TX_PAUSE
    },
    {
        .name = "stat_tx_user_pause", .offset = USPLUS100G_STAT_TX_USER_PAUSE
    },
    {
        .name = "stat_rx_total_pkts", .offset = USPLUS100G_STAT_RX_TOTAL_PACKETS
    },
    {
        .name = "stat_rx_total_good_pkts", .offset = USPLUS100G_STAT_RX_TOTAL_GOOD_PACKETS
    },
    {
        .name = "stat_rx_total_bytes", .offset = USPLUS100G_STAT_RX_TOTAL_BYTES
    },
    {
        .name = "stat_rx_total_good_bytes", .offset = USPLUS100G_STAT_RX_TOTAL_GOOD_BYTES
    },
    {
        .name = "stat_rx_pkt_64_bytes", .offset = USPLUS100G_STAT_RX_PACKET_64_BYTES
    },
    {
        .name = "stat_rx_pkt_65_127_bytes", .offset = USPLUS100G_STAT_RX_PACKET_65_127_BYTES
    },
    {
        .name = "stat_rx_pkt_128_255_bytes", .offset = USPLUS100G_STAT_RX_PACKET_128_255_BYTES
    },
    {
        .name = "stat_rx_pkt_256_511_bytes", .offset = USPLUS100G_STAT_RX_PACKET_256_511_BYTES
    },
    {
        .name = "stat_rx_pkt_512_1023_bytes", .offset = USPLUS100G_STAT_RX_PACKET_512_1023_BYTES
    },
    {
        .name = "stat_rx_pkt_1024_1518_bytes", .offset = USPLUS100G_STAT_RX_PACKET_1024_1518_BYTES
    },
    {
        .name = "stat_rx_pkt_1519_1522_bytes", .offset = USPLUS100G_STAT_RX_PACKET_1519_1522_BYTES
    },
    {
        .name = "stat_rx_pkt_1523_1548_bytes", .offset = USPLUS100G_STAT_RX_PACKET_1523_1548_BYTES
    },
    {
        .name = "stat_rx_pkt_1549_2047_bytes", .offset = USPLUS100G_STAT_RX_PACKET_1549_2047_BYTES
    },
    {
        .name = "stat_rx_pkt_2048_4095_bytes", .offset = USPLUS100G_STAT_RX_PACKET_2048_4095_BYTES
    },
    {
        .name = "stat_rx_pkt_4096_8191_bytes", .offset = USPLUS100G_STAT_RX_PACKET_4096_8191_BYTES
    },
    {
        .name = "stat_rx_pkt_8192_9215_bytes", .offset = USPLUS100G_STAT_RX_PACKET_8192_9215_BYTES
    },
    {
        .name = "stat_rx_pkt_large", .offset = USPLUS100G_STAT_RX_PACKET_LARGE
    },
    {
        .name = "stat_rx_pkt_small", .offset = USPLUS100G_STAT_RX_PACKET_SMALL
    },
    {
        .name = "stat_rx_undersize", .offset = USPLUS100G_STAT_RX_UNDERSIZE
    },
    {
        .name = "stat_rx_fragment", .offset = USPLUS100G_STAT_RX_FRAGMENT
    },
    {
        .name = "stat_rx_oversize", .offset = USPLUS100G_STAT_RX_OVERSIZE
    },
    {
        .name = "stat_rx_toolong", .offset = USPLUS100G_STAT_RX_TOOLONG
    },
    {
        .name = "stat_rx_jabber", .offset = USPLUS100G_STAT_RX_JABBER
    },
    {
        .name = "stat_rx_bad_fcs", .offset = USPLUS100G_STAT_RX_BAD_FCS
    },
    {
        .name = "stat_rx_pkt_bad_fcs", .offset = USPLUS100G_STAT_RX_PACKET_BAD_FCS
    },
    {
        .name = "stat_rx_stomped_fcs", .offset = USPLUS100G_STAT_RX_STOMPED_FCS
    },
    {
        .name = "stat_rx_ucast", .offset = USPLUS100G_STAT_RX_UNICAST
    },
    {
        .name = "stat_rx_mcast", .offset = USPLUS100G_STAT_RX_MULTICAST
    },
    {
        .name = "stat_rx_bcast", .offset = USPLUS100G_STAT_RX_BROADCAST
    },
    {
        .name = "stat_rx_vlan", .offset = USPLUS100G_STAT_RX_VLAN
    },
    {
        .name = "stat_rx_pause", .offset = USPLUS100G_STAT_RX_PAUSE
    },
    {
        .name = "stat_rx_user_pause", .offset = USPLUS100G_STAT_RX_USER_PAUSE
    },
    {
        .name = "stat_rx_inrangeerr", .offset = USPLUS100G_STAT_RX_INRANGEERR
    },
    {
        .name = "stat_rx_truncated", .offset = USPLUS100G_STAT_RX_TRUNCATED
    },
    {
        .name = "stat_otn_tx_jabber", .offset = USPLUS100G_STAT_OTN_TX_JABBER
    },
    {
        .name = "stat_otn_tx_oversize", .offset = USPLUS100G_STAT_OTN_TX_OVERSIZE
    },
    {
        .name = "stat_otn_tx_undersize", .offset = USPLUS100G_STAT_OTN_TX_UNDERSIZE
    },
    {
        .name = "stat_otn_tx_toolong", .offset = USPLUS100G_STAT_OTN_TX_TOOLONG
    },
    {
        .name = "stat_otn_tx_fragment", .offset = USPLUS100G_STAT_OTN_TX_FRAGMENT
    },
    {
        .name = "stat_otn_tx_pkt_bad_fcs", .offset = USPLUS100G_STAT_OTN_TX_PACKET_BAD_FCS
    },
    {
        .name = "stat_otn_tx_stomped_fcs", .offset = USPLUS100G_STAT_OTN_TX_STOMPED_FCS
    },
    {
        .name = "stat_otn_tx_bad_code", .offset = USPLUS100G_STAT_OTN_TX_BAD_CODE
    },
};
#else 
static mib_counter_map_t _usplus100g_mib_cntr_map[] = {
    {
        .ifmib = DEVMAN_IF_MIB_BYTEREC, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_BYTSENT, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRAREC, .offset = USPLUS100G_STAT_RX_TOTAL_PACKETS
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRASENT, .offset = USPLUS100G_STAT_TX_TOTAL_PACKETS
    },
    {
        .ifmib = DEVMAN_IF_MIB_TOTALBYTEREC, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_TOTALFRAMEREC, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_BRDFRAREC, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_MULFRAREC, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_CRCERR, .offset = USPLUS100G_STAT_RX_BAD_FCS
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRAOVERSIZE, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRAFRAGMENTS, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_JABBER, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_COLL, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_LATECOL, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRA64, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRA65T127, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRA128T255, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRA256T511, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRA512T1023, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRA1024T1522, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_MACRXERR, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_DROPPED, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_INUCASTPKT, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_INNOUCASTPKT, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_INERRORS, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_OUTERRORS, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_OUTNOUCASTPKT, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_OUTUCASTPKT, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_MULTIOUT, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_BROADOUT, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_UNDERSIZ, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_INUNKNOWNPROTOS, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_ALIGN_ERR, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_FCS_ERR, .offset = USPLUS100G_STAT_RX_BAD_FCS
    },
    {
        .ifmib = DEVMAN_IF_MIB_SQETEST_ERR, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_CSE_ERR, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_SYMBOL_ERR, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_MACTX_ERR, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_MACRX_ERR, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_TOOLONGFRA, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_SNGL_COLLISION, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_MULT_COLLISION, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_LATE_COLLISION, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_EXCESS_COLLISION, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_INUNKNOWNOPCODE, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_DEFERREDTX, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_INPAUSE_FRAMES, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_OUTPAUSE_FRAMES, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_INFRAOVERSIZE, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_OUTFRAOVERSIZE, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_INFRAFRAGMENTS, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_OUTFRAFRAGMENTS, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_INJABBERS, .offset = USPLUS100G_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_OUTJABBERS, .offset = USPLUS100G_STAT_INVALID_OFFSET
    }
};

static eth_counter_t _usplus100g_counters[] = {
    {
        .name = "tx_total_packets_low_cnt"
    },
    {
        .name = "tx_total_packets_high_cnt"
    },
    {
        .name = "rx_total_packets_low_cnt"
    },
    {
        .name = "rx_total_packets_high_cnt"
    },
    {
        .name = "rx_bad_packets_low_cnt"
    },
    {
        .name = "rx_bad_packets_high_cnt"
    },
};
#endif

/* usplus100g register read */
static ccl_err_t _usplus100g_reg_read(__attribute__((unused)) void *p_priv, 
                                      b_u32 offset, 
                                      __attribute__((unused)) b_u32 offset_sz, 
                                      b_u32 idx, b_u8 *p_value, b_u32 size)
{
    usplus100g_ctx_t *p_usplus100g; 

    if ( offset > USPLUS100G_MAX_OFFSET || 
         !p_value || idx || size != sizeof(b_u32) ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    PDEBUGG("123$=> offset=0x%03x, size=%d\n", offset, size);

    p_usplus100g = (usplus100g_ctx_t *)p_priv;     
    offset += p_usplus100g->brdspec.offset;
    _ret = devspibus_read_buff(p_usplus100g->brdspec.devname, 
                               offset, idx, p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to read over spi bus\n");
        USPLUS100G_ERROR_RETURN(_ret);
    }

    return _ret;
}

/* usplus100g register write */
static ccl_err_t _usplus100g_reg_write(__attribute__((unused)) void *p_priv, 
                                       b_u32 offset, 
                                       __attribute__((unused)) b_u32 offset_sz, 
                                       b_u32 idx, b_u8 *p_value, b_u32 size)
{
    usplus100g_ctx_t *p_usplus100g; 

    if ( offset > USPLUS100G_MAX_OFFSET || 
         !p_value || idx || size != sizeof(b_u32) ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    PDEBUG("123$=> offset=0x%03x, value=0x%08x\n", offset, *p_value);

    p_usplus100g = (usplus100g_ctx_t *)p_priv;     
    offset += p_usplus100g->brdspec.offset;
    _ret = devspibus_write_buff(p_usplus100g->brdspec.devname, 
                                offset, idx, p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to write over spi bus\n");
        USPLUS100G_ERROR_RETURN(_ret);
    }

    return _ret;
}

/**
 * @brief Read 48 bit value from the registers
 *
 * @param[in] address_l is lower address of the register. 
 * @param[out] pvalue is a pointer to hold 48 bit value
 *
 * @return CCL_OK on success, error code on failure
 *
 */
static ccl_err_t _usplus100g_read_48bit_value(usplus100g_ctx_t *p_usplus100g, 
                                              b_u32 address_l, b_u64 *pvalue)
{
    b_u32 regval;
    b_u32 address_h = address_l + USPLUS100G_NUM_WORD_BYTES;

    if ( !p_usplus100g || !pvalue ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    USPLUS100G_REG_READ(p_usplus100g, address_h, 
                        sizeof(b_u32), 0, 
                        (b_u8 *)&regval, sizeof(b_u32));
    regval &= USPLUS100G_HALF_WORD_MASK;
    *pvalue = (b_u64)(regval << USPLUS100G_NUM_WORD_BITS);
    USPLUS100G_REG_READ(p_usplus100g, address_l, 
                        sizeof(b_u32), 0, 
                        (b_u8 *)&regval, sizeof(b_u32));
    PDEBUG("addr_l=0x%08x, val=%d, addr_h=0x%08x, val=%d\n",
           address_l, regval, address_h, (b_u32)*pvalue>>USPLUS100G_NUM_WORD_BITS);
    *pvalue |= regval;

    return CCL_OK;
}

static ccl_err_t _usplus100g_mtu_get(usplus100g_ctx_t *p_usplus100g, 
                                     b_u8 *pvalue, b_u32 size)
{
    b_u32 regval;
    b_u32 mtu; 

    if ( !p_usplus100g || !pvalue || size != sizeof(b_u32)) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    PDEBUG("MTU is not supported for UltraScale+ Integrated 100G Ethernet Subsystem\n");
    return CCL_NOT_SUPPORT;
}

static ccl_err_t _usplus100g_link_status_get(usplus100g_ctx_t *p_usplus100g, 
                                          b_u8 *pvalue, b_u32 size)
{
    b_u32 rx_block_lock;
    b_u32 tx_stat;
    b_u32 rx_stat;
    b_u32 link; 

    if ( !p_usplus100g || !pvalue || size != sizeof(b_u8)) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_STAT_RX_BLOCK_LOCK_REG, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&rx_block_lock, sizeof(b_u32));
    USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_STAT_RX_BLOCK_LOCK_REG, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&rx_block_lock, sizeof(b_u32));

    USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_STAT_TX_STATUS_REG, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&tx_stat, sizeof(b_u32));
    USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_STAT_TX_STATUS_REG, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&tx_stat, sizeof(b_u32));

    USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_STAT_RX_STATUS_REG, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&rx_stat, sizeof(b_u32));
    USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_STAT_RX_STATUS_REG, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&rx_stat, sizeof(b_u32));

    rx_block_lock &= USPLUS100G_STAT_RX_BLOCK_LOCK_MASK;
    tx_stat &= USPLUS100G_STAT_TX_LOCAL_FAULT_MASK;
    rx_stat &= USPLUS100G_STAT_RX_STS_MASK;

    *pvalue = (rx_block_lock && !tx_stat && 
               (rx_stat == USPLUS100G_STAT_RX_STATUS_BIT | 
                           USPLUS100G_STAT_RX_ALIGNED_BIT)) ? CCL_TRUE : CCL_FALSE;

    return CCL_OK;
}

static ccl_err_t _usplus100g_if_property_get(void *priv, b_u32 prop, 
                                          b_u8 *pvalue, b_u32 size)
{
    usplus100g_ctx_t *p_usplus100g;
    port_stats_t  *port_stats;
    b_u32         address_l;

    if ( !priv || !pvalue) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_usplus100g = (usplus100g_ctx_t *)priv;

    switch (prop) {
    case DEVMAN_IF_NAME:
        strncpy(pvalue, p_usplus100g->brdspec.name, size);
        break;
    case DEVMAN_IF_MTU:
        _ret = _usplus100g_mtu_get(p_usplus100g, pvalue, size);
        break;
    case DEVMAN_IF_SPEED_MAX:
    case DEVMAN_IF_SPEED:
    case DEVMAN_IF_DUPLEX:
        _ret = CCL_NOT_SUPPORT;
        break;
    case DEVMAN_IF_AUTONEG:
    case DEVMAN_IF_ENCAP:
    case DEVMAN_IF_MEDIUM:
    case DEVMAN_IF_INTERFACE_TYPE:
    case DEVMAN_IF_MDIX:
    case DEVMAN_IF_MDIX_STATUS:
    case DEVMAN_IF_LEARN:
    case DEVMAN_IF_IFILTER:
    case DEVMAN_IF_SELF_EFILTER:
    case DEVMAN_IF_LOCK:
    case DEVMAN_IF_DROP_ON_LOCK:
    case DEVMAN_IF_FWD_UNK:
    case DEVMAN_IF_BCAST_LIMIT:
    case DEVMAN_IF_ABILITY:
    case DEVMAN_IF_FLOWCONTROL:
    case DEVMAN_IF_ENABLE:
    case DEVMAN_IF_SFP_PRESENT:
    case DEVMAN_IF_SFP_TX_STATE:
    case DEVMAN_IF_SFP_RX_STATE:
    case DEVMAN_IF_DEFAULT_VLAN:
    case DEVMAN_IF_PRIORITY:
    case DEVMAN_IF_LED_STATE:
    case DEVMAN_IF_MEDIA_PARAMETERS:
    case DEVMAN_IF_MEDIA_DETAILS:
    case DEVMAN_IF_LOCAL_ADVERT:
    case DEVMAN_IF_MEDIA_TYPE:
    case DEVMAN_IF_SFP_PORT_TYPE:
    case DEVMAN_IF_MEDIA_CONFIG_TYPE:
    case DEVMAN_IF_MEDIA_ERROR_CONFIG_TYPE:
    case DEVMAN_IF_MEDIA_SGMII_FIBER:
    case DEVMAN_IF_TX_ENABLE:
    case DEVMAN_IF_SFP_PORT_NUMBERS:
    case DEVMAN_IF_AUTONEG_DUPLEX:
    case DEVMAN_IF_AUTONEG_SPEED :
    case DEVMAN_IF_DDM_PARAMETERS:
    case DEVMAN_IF_DDM_STATUS:     
    case DEVMAN_IF_DDM_SUPPORTED:  
    case DEVMAN_IF_HWADDRESS_GET:
        break;
    case DEVMAN_IF_ACTIVE_LINK:
        _ret = _usplus100g_link_status_get(p_usplus100g, pvalue, size);
        break;
    default:
        PDEBUG("Error! Invalid property: %d\n", prop);
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);          
    }

    return _ret;
}

/**
 * @brief Reset the CMAC GT
 *
 * @param None
 *
 * @return CCL_OK on success, CCL_FAIL on failure 
 */
static ccl_err_t _usplus100g_reset_gt(usplus100g_ctx_t *p_usplus100g)
{
    b_u32 regval;

    if ( !p_usplus100g ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* read the value */
    USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_GT_RESET_REG, 
                        sizeof(b_u32), 0, 
                        (b_u8 *)&regval, sizeof(b_u32));
    /* Assert the bit */
    regval |= USPLUS100G_GT_RESET_MASK;
    USPLUS100G_REG_WRITE(p_usplus100g, USPLUS100G_GT_RESET_REG, 
                         sizeof(b_u32), 0, 
                         (b_u8 *)&regval, sizeof(b_u32));
    /* De-Assert the bit */
    regval &= ~(USPLUS100G_GT_RESET_MASK);
    USPLUS100G_REG_WRITE(p_usplus100g, USPLUS100G_GT_RESET_REG, 
                         sizeof(b_u32), 0, 
                         (b_u8 *)&regval, sizeof(b_u32));
    return CCL_OK;
}

/**
 * @brief Perform Rx SERDES reset and Rx, Tx Cores reset
 *
 * @param[in] core specifies Rx or Tx or both interfaces
 *
 * @return CCL_OK on success, error code on failure 
 */
static ccl_err_t _usplus100g_reset_core(usplus100g_ctx_t *p_usplus100g, 
                                        usplus100g_core_type_t core)
{
    char  regval;
    b_u32 mask;

    if ( !p_usplus100g ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_RESET_REG, 
                        sizeof(b_u32), 0, 
                        (b_u8 *)&regval, sizeof(b_u32));

    /* reset usr_rx_serdes_reset */
    mask = USPLUS100G_USR_RX_SERDES_RESET_MASK;
    switch (core) {
    case USPLUS100G_CORE_TX:
        mask |= USPLUS100G_TX_CORE_RESET_MASK;
        break;
    case USPLUS100G_CORE_RX:
        mask |= USPLUS100G_RX_CORE_RESET_MASK;
        break;
    case USPLUS100G_CORE_BOTH:
        mask |= (USPLUS100G_RX_CORE_RESET_MASK | 
                 USPLUS100G_TX_CORE_RESET_MASK);
        break;
    default:
        return -EINVAL;
    }
    /* Assert all the bits simultaneously */
    regval |= mask;
    USPLUS100G_REG_WRITE(p_usplus100g, USPLUS100G_RESET_REG, 
                         sizeof(b_u32), 0, 
                         (b_u8 *)&regval, sizeof(b_u32));
    usleep(10 * 1000);
    /* De-Assert all the bits simultaneously */
    regval &= ~(mask);
    USPLUS100G_REG_WRITE(p_usplus100g, USPLUS100G_RESET_REG, 
                         sizeof(b_u32), 0, 
                         (b_u8 *)&regval, sizeof(b_u32));
    return CCL_OK;
}

/**
 * @brief Get the USPLUS100G core version.
 *
 * @param[out] pver is the memory that is updated with the 
 *       USPLUS100G core version number
 *
 * @return error code
 */
static b_u32 _usplus100g_get_version(usplus100g_ctx_t *p_usplus100g, b_u32 *pver)
{
    if ( !p_usplus100g || !pver ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_CORE_VERSION_REG, 
                        sizeof(b_u32), 0, 
                        (b_u8 *)pver, sizeof(b_u32));

    return CCL_OK;
}

/**
 * @brief Enable the USPLUS100G Rx/Tx interface.
 *
 * @param[in] core is interface (rx/tx) that is to be enabled.
 *
 * @return	CCL_OK on success, error code on failure 
 */
static ccl_err_t _usplus100g_enable(usplus100g_ctx_t *p_usplus100g, 
                                    usplus100g_core_type_t core)
{
    b_u32 regval;

    if ( !p_usplus100g ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch (core) {
    case USPLUS100G_CORE_TX:
        USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_CONFIGURATION_TX_REG1, 
                            sizeof(b_u32), 0, 
                            (b_u8 *)&regval, sizeof(b_u32));
        set_bit(regval, USPLUS100G_CTL_TX_ENABLE_EN_BIT);
        USPLUS100G_REG_WRITE(p_usplus100g, USPLUS100G_CONFIGURATION_TX_REG1, 
                             sizeof(b_u32), 0, 
                             (b_u8 *)&regval, sizeof(b_u32));
        break;
    case USPLUS100G_CORE_RX:
        USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_CONFIGURATION_RX_REG1, 
                            sizeof(b_u32), 0, 
                            (b_u8 *)&regval, sizeof(b_u32));
        set_bit(regval, USPLUS100G_CTL_RX_ENABLE_EN_BIT);
        USPLUS100G_REG_WRITE(p_usplus100g, USPLUS100G_CONFIGURATION_RX_REG1, 
                             sizeof(b_u32), 0, 
                             (b_u8 *)&regval, sizeof(b_u32));
        break;
    case USPLUS100G_CORE_BOTH:
        /* Tx */
        USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_CONFIGURATION_TX_REG1, 
                            sizeof(b_u32), 0, 
                            (b_u8 *)&regval, sizeof(b_u32));
        set_bit(regval, USPLUS100G_CTL_TX_ENABLE_EN_BIT);
        USPLUS100G_REG_WRITE(p_usplus100g, USPLUS100G_CONFIGURATION_TX_REG1, 
                             sizeof(b_u32), 0, 
                             (b_u8 *)&regval, sizeof(b_u32));
        /* Rx */
        USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_CONFIGURATION_RX_REG1, 
                            sizeof(b_u32), 0, 
                            (b_u8 *)&regval, sizeof(b_u32));
        set_bit(regval, USPLUS100G_CTL_RX_ENABLE_EN_BIT);
        USPLUS100G_REG_WRITE(p_usplus100g, USPLUS100G_CONFIGURATION_RX_REG1, 
                             sizeof(b_u32), 0, 
                             (b_u8 *)&regval, sizeof(b_u32));
        break;
    default:
        return CCL_BAD_PARAM_ERR;
    }
    return CCL_OK;
}

/**
 * @brief Disable the USPLUS100G device.
 *
 * @param[in] core is interface (rx/tx) that is to be disabled.
 *
 * @return	CCL_OK on success, error code on failure 
 */
static ccl_err_t _usplus100g_disable(usplus100g_ctx_t *p_usplus100g, 
                                     usplus100g_core_type_t core)
{
    b_u32 regval;

    if ( !p_usplus100g ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch (core) {
    case USPLUS100G_CORE_TX:
        USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_CONFIGURATION_TX_REG1, 
                            sizeof(b_u32), 0, 
                            (b_u8 *)&regval, sizeof(b_u32));
        clear_bit(regval, USPLUS100G_CTL_TX_ENABLE_EN_BIT);
        USPLUS100G_REG_WRITE(p_usplus100g, USPLUS100G_CONFIGURATION_TX_REG1, 
                             sizeof(b_u32), 0, 
                             (b_u8 *)&regval, sizeof(b_u32));        
        break;
    case USPLUS100G_CORE_RX:
        USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_CONFIGURATION_RX_REG1, 
                            sizeof(b_u32), 0, 
                            (b_u8 *)&regval, sizeof(b_u32));
        clear_bit(regval, USPLUS100G_CTL_RX_ENABLE_EN_BIT);
        USPLUS100G_REG_WRITE(p_usplus100g, USPLUS100G_CONFIGURATION_RX_REG1, 
                             sizeof(b_u32), 0, 
                             (b_u8 *)&regval, sizeof(b_u32));
        break;
    case USPLUS100G_CORE_BOTH:
        /* Tx */
        USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_CONFIGURATION_TX_REG1, 
                            sizeof(b_u32), 0, 
                            (b_u8 *)&regval, sizeof(b_u32));
        clear_bit(regval, USPLUS100G_CTL_TX_ENABLE_EN_BIT);
        USPLUS100G_REG_WRITE(p_usplus100g, USPLUS100G_CONFIGURATION_TX_REG1, 
                             sizeof(b_u32), 0, 
                             (b_u8 *)&regval, sizeof(b_u32));        
        /* Rx */
        USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_CONFIGURATION_RX_REG1, 
                            sizeof(b_u32), 0, 
                            (b_u8 *)&regval, sizeof(b_u32));
        clear_bit(regval, USPLUS100G_CTL_RX_ENABLE_EN_BIT);
        USPLUS100G_REG_WRITE(p_usplus100g, USPLUS100G_CONFIGURATION_RX_REG1, 
                             sizeof(b_u32), 0, 
                             (b_u8 *)&regval, sizeof(b_u32));
        break;
    default:
        return CCL_BAD_PARAM_ERR;
    }
    return CCL_OK;
}

/**
 * @brief Get the current settings (rx/tx enabled status)
 *
 * @param[out] rx_en_status is a pointer to store the rx enabled
 *       status.
 * @param[out] tx_en_status is a pointer to store the tx enabled
 *       status.
 * @return	CCL_OK on success, error code on failure 
 */
static ccl_err_t _usplus100g_get_device_config(usplus100g_ctx_t *p_usplus100g, 
                                               b_u8 *rx_en_status, 
                                               b_u8 *tx_en_status)
{
    b_u32 regval;

    if ( !p_usplus100g || !rx_en_status || !tx_en_status ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_CONFIGURATION_RX_REG1, 
                        sizeof(b_u32), 0, 
                        (b_u8 *)&regval, sizeof(b_u32));
    *rx_en_status = regval & USPLUS100G_CTL_RX_ENABLE_MASK;
    USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_CONFIGURATION_TX_REG1, 
                        sizeof(b_u32), 0, 
                        (b_u8 *)&regval, sizeof(b_u32));
    *tx_en_status = regval & USPLUS100G_CTL_TX_ENABLE_MASK;

    return CCL_OK;
}

/**
 * @brief Set the 100ge PCS to the given CAUI mode
 *
 * @param[in] caui_mode is CAUI mode type.
 *
 * @return	CCL_OK on success, error code on failure 
 */
static ccl_err_t _usplus100g_set_caui_mode(usplus100g_ctx_t *p_usplus100g, 
                                           usplus100g_caui_mode_t caui_mode)
{
    b_u32 regval;

    if ( !p_usplus100g ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    if (caui_mode > USPLUS100G_CAUI_MODE_4)
        return CCL_BAD_PARAM_ERR;

    regval = caui_mode & USPLUS100G_CAUI_MODE_MASK;
    USPLUS100G_REG_WRITE(p_usplus100g, USPLUS100G_SWITCH_CORE_MODE_REG, 
                         sizeof(b_u32), 0, 
                         (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/**
 * @brief Get the current CAUI mode configuration
 *
 * @param[out] pmode is the memory that is updated with the 
 *       current CAUI mode
 *
 * @return error code
 */
static ccl_err_t _usplus100g_get_caui_mode(usplus100g_ctx_t *p_usplus100g, 
                                           usplus100g_caui_mode_t *pmode)
{
    b_u32 regval;

    if ( !p_usplus100g || !pmode ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_SWITCH_CORE_MODE_REG, 
                        sizeof(b_u32), 0, 
                        (b_u8 *)&regval, sizeof(b_u32));

    *pmode =  regval & USPLUS100G_CAUI_MODE_MASK;

    return CCL_OK;
}

/**
 * @brief Configure the fault indication signals
 *
 * @param[in] tx_send_rfi indicates to set/clear rfi.
 *
 * @return	CCL_OK on success, error code on failure 
 */
static ccl_err_t _usplus100g_set_fault_indication(usplus100g_ctx_t *p_usplus100g, 
                                                  b_u8 tx_send_rfi)
{
    b_u32 regval;

    if ( !p_usplus100g ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_CONFIGURATION_TX_REG1, 
                        sizeof(b_u32), 0, 
                        (b_u8 *)&regval, sizeof(b_u32));
    if (tx_send_rfi)
        set_bit(regval, USPLUS100G_CTL_TX_SEND_RFI_EN_BIT);
    else
        clear_bit(regval, USPLUS100G_CTL_TX_SEND_RFI_EN_BIT);
    USPLUS100G_REG_WRITE(p_usplus100g, USPLUS100G_CONFIGURATION_TX_REG1, 
                         sizeof(b_u32), 0, 
                         (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/**
 * @brief Get the current configuration of fault indication 
 *        signals
 *
 * @param[out] pfault is the memory that is updated with the 
 *       current configuration of fault indication signals
 *
 * @return error code
 */                                
static ccl_err_t _usplus100g_get_fault_indication(usplus100g_ctx_t *p_usplus100g, 
                                                  b_u8 *pfault)
{
    b_u32 regval;

    if ( !p_usplus100g ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_CONFIGURATION_TX_REG1, 
                        sizeof(b_u32), 0, 
                        (b_u8 *)&regval, sizeof(b_u32));

    *pfault =  (b_u8)( (regval & USPLUS100G_CTL_TX_SEND_RFI_EN_BIT_MASK) >> 
                       USPLUS100G_CTL_TX_SEND_RFI_EN_BIT );

    return CCL_OK;
}

/**
 * @brief Transmit idle packets
 *
 * @param None
 *
 * @return	CCL_OK on success, error code on failure 
 */                
static ccl_err_t _usplus100g_send_tx_idle(usplus100g_ctx_t *p_usplus100g)
{
    b_u32 regval;

    if ( !p_usplus100g ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_CONFIGURATION_TX_REG1, 
                        sizeof(b_u32), 0, 
                        (b_u8 *)&regval, sizeof(b_u32));
    set_bit(regval, USPLUS100G_CTL_TX_SEND_IDLE_EN_BIT);
    USPLUS100G_REG_WRITE(p_usplus100g, USPLUS100G_CONFIGURATION_TX_REG1, 
                         sizeof(b_u32), 0, 
                         (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/**
 * @brief Return the transmit idle insertion value representing
 * transmit idle is enabled or disabled.
 *
 * @param[out] pstatus is the memory that is updated with a 
 *      boolean value representing the current idle insertion
 *      status with true representing enabled and false
 *      representing disabled.
 *
 * @return	CCL_OK on success, error code on failure 
 */
static ccl_err_t _usplus100g_get_tx_idle_status(usplus100g_ctx_t *p_usplus100g, 
                                                b_u8 *pstatus)
{    
    b_u32 regval;

    if ( !p_usplus100g || !pstatus ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_CONFIGURATION_TX_REG1, 
                        sizeof(b_u32), 0, 
                        (b_u8 *)&regval, sizeof(b_u32));
    *pstatus = (b_u8)( (regval & USPLUS100G_CTL_TX_SEND_IDLE_EN_BIT_MASK) >> 
                         USPLUS100G_CTL_TX_SEND_IDLE_EN_BIT );

    return CCL_OK;
}

/**
 * @brief Enable test patterns in the rx, tx or both directions.
 *
 * @param[in] core is direction type
 *
 * @return	CCL_OK on success, error code on failure 
 *
 * @note Test pattern generation enable for the Rx and TX cores. A value of 1 enables
 * test mode.
 * 
 * 16 RW ctl_tx_test_pattern
 * 8 bit ctl_rx_test_pattern  
 */ 
static ccl_err_t _usplus100g_enable_test_pattern(usplus100g_ctx_t *p_usplus100g, 
                                                 usplus100g_core_type_t core)
{    
    b_u32 regval;

    if ( !p_usplus100g ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch (core) {
    case USPLUS100G_CORE_TX:
        USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_CONFIGURATION_TX_REG1, 
                            sizeof(b_u32), 0, 
                            (b_u8 *)&regval, sizeof(b_u32));
        set_bit(regval, USPLUS100G_TX_TEST_PATTERN_EN_BIT);
        USPLUS100G_REG_WRITE(p_usplus100g, USPLUS100G_CONFIGURATION_TX_REG1, 
                             sizeof(b_u32), 0, 
                             (b_u8 *)&regval, sizeof(b_u32));
        break;
    case USPLUS100G_CORE_RX:
        USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_CONFIGURATION_RX_REG1, 
                            sizeof(b_u32), 0, 
                            (b_u8 *)&regval, sizeof(b_u32));
        set_bit(regval, USPLUS100G_RX_TEST_PATTERN_EN_BIT);
        USPLUS100G_REG_WRITE(p_usplus100g, USPLUS100G_CONFIGURATION_RX_REG1, 
                             sizeof(b_u32), 0, 
                             (b_u8 *)&regval, sizeof(b_u32));
        break;
    case USPLUS100G_CORE_BOTH:
        /* Tx */
        USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_CONFIGURATION_TX_REG1, 
                            sizeof(b_u32), 0, 
                            (b_u8 *)&regval, sizeof(b_u32));
        set_bit(regval, USPLUS100G_TX_TEST_PATTERN_EN_BIT);
        USPLUS100G_REG_WRITE(p_usplus100g, USPLUS100G_CONFIGURATION_TX_REG1, 
                             sizeof(b_u32), 0, 
                             (b_u8 *)&regval, sizeof(b_u32));
        /* Rx */
        USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_CONFIGURATION_RX_REG1, 
                            sizeof(b_u32), 0, 
                            (b_u8 *)&regval, sizeof(b_u32));
        set_bit(regval, USPLUS100G_RX_TEST_PATTERN_EN_BIT);
        USPLUS100G_REG_WRITE(p_usplus100g, USPLUS100G_CONFIGURATION_RX_REG1, 
                             sizeof(b_u32), 0, 
                             (b_u8 *)&regval, sizeof(b_u32));
        break;
    default:
        return CCL_BAD_PARAM_ERR;
    }

    return CCL_OK;
}

/**
 * @brief Disable test patterns in the rx, tx or 
 *       both directions.
 *
 * @param[in] core is direction type
 *
 * @return CCL_OK on success, error code on failure 
 */  
static ccl_err_t _usplus100g_disable_test_pattern(usplus100g_ctx_t *p_usplus100g, 
                                                  usplus100g_core_type_t core)
{
    b_u32 regval;

    if ( !p_usplus100g ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch (core) {
    case USPLUS100G_CORE_TX:
        USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_CONFIGURATION_TX_REG1, 
                            sizeof(b_u32), 0, 
                            (b_u8 *)&regval, sizeof(b_u32));
        clear_bit(regval, USPLUS100G_TX_TEST_PATTERN_EN_BIT);
        USPLUS100G_REG_WRITE(p_usplus100g, USPLUS100G_CONFIGURATION_TX_REG1, 
                             sizeof(b_u32), 0, 
                             (b_u8 *)&regval, sizeof(b_u32));
        break;
    case USPLUS100G_CORE_RX:
        USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_CONFIGURATION_TX_REG1, 
                            sizeof(b_u32), 0, 
                            (b_u8 *)&regval, sizeof(b_u32));
        clear_bit(regval, USPLUS100G_TX_TEST_PATTERN_EN_BIT);
        USPLUS100G_REG_WRITE(p_usplus100g, USPLUS100G_CONFIGURATION_TX_REG1, 
                             sizeof(b_u32), 0, 
                             (b_u8 *)&regval, sizeof(b_u32));
        break;
    case USPLUS100G_CORE_BOTH:
        /* Tx */
        USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_CONFIGURATION_TX_REG1, 
                            sizeof(b_u32), 0, 
                            (b_u8 *)&regval, sizeof(b_u32));
        clear_bit(regval, USPLUS100G_TX_TEST_PATTERN_EN_BIT);
        USPLUS100G_REG_WRITE(p_usplus100g, USPLUS100G_CONFIGURATION_TX_REG1, 
                             sizeof(b_u32), 0, 
                             (b_u8 *)&regval, sizeof(b_u32));
        /* Rx */
        USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_CONFIGURATION_RX_REG1, 
                            sizeof(b_u32), 0, 
                            (b_u8 *)&regval, sizeof(b_u32));
        clear_bit(regval, USPLUS100G_RX_TEST_PATTERN_EN_BIT);
        USPLUS100G_REG_WRITE(p_usplus100g, USPLUS100G_CONFIGURATION_RX_REG1, 
                             sizeof(b_u32), 0, 
                             (b_u8 *)&regval, sizeof(b_u32));
        break;
    default:
        return CCL_BAD_PARAM_ERR;
    }

    return CCL_OK;
}

/**
 * @brief Read the current configuration of test 
 * pattern in each direction. 
 *
 * @param[in] core is direction type 
 * @param[out] pstatus is enable/disable status 
 *
 * @return CCL_OK on success, error code on failure 
 */ 
static ccl_err_t _usplus100g_get_test_pattern_status(usplus100g_ctx_t *p_usplus100g, 
                                                     usplus100g_core_type_t core, 
                                                     b_u8 *pstatus)
{
    b_u32 regval = 0;

    if ( !p_usplus100g || !pstatus  ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch (core) {
    case USPLUS100G_CORE_TX:
        USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_CONFIGURATION_TX_REG1, 
                            sizeof(b_u32), 0, 
                            (b_u8 *)&regval, sizeof(b_u32));
        *pstatus = (b_u8)( (regval & USPLUS100G_TX_TEST_PATTERN_EN_BIT_MASK) >>
                           USPLUS100G_TX_TEST_PATTERN_EN_BIT );
        break;
    case USPLUS100G_CORE_RX:
        USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_CONFIGURATION_RX_REG1, 
                            sizeof(b_u32), 0, 
                            (b_u8 *)&regval, sizeof(b_u32));
        *pstatus = (b_u8)( (regval & USPLUS100G_RX_TEST_PATTERN_EN_BIT_MASK) >>
                           USPLUS100G_RX_TEST_PATTERN_EN_BIT );
        break;
    default:
        return CCL_BAD_PARAM_ERR;
    }

    return CCL_OK;
}                                    

/**
 * This function forces a receiver resync.
 *
 * @param None
 *
 * @return CCL_OK on success, error code on failure 
 *
 * @note This signal is used to force the RX path to reset, re-synchronize, 
 * and realign. A value of 1 forces the reset operation. 
 * A value of 0 allows normal operation
 */ 
static ccl_err_t _usplus100g_force_rx_resync(usplus100g_ctx_t *p_usplus100g)
{   
    b_u32 regval;

    if ( !p_usplus100g ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_CONFIGURATION_RX_REG1, 
                        sizeof(b_u32), 0, 
                        (b_u8 *)&regval, sizeof(b_u32));
    set_bit(regval, USPLUS100G_RX_TEST_PATTERN_EN_BIT);
    USPLUS100G_REG_WRITE(p_usplus100g, USPLUS100G_CONFIGURATION_RX_REG1, 
                         sizeof(b_u32), 0, 
                         (b_u8 *)&regval, sizeof(b_u32));    

    return CCL_OK;
}

/**
 * @brief Configure tx bip7 parameters.
 *
 * @param[in] pbip7config is a pointer to hold bip7 
 *       configuration values.
 *
 * @return CCL_OK on success, error code on failure 
 *
 * @note This value is set for Tx interface
 */ 
static ccl_err_t _usplus100g_set_tx_bip7_config(usplus100g_ctx_t *p_usplus100g, 
                                                usplus100g_bip7_config_t *pbip7_config)
{
    b_u32 regval;

    if ( !p_usplus100g || !pbip7_config ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_CONFIGURATION_TX_BIP_OVERRIDE, 
                        sizeof(b_u32), 0, 
                        (b_u8 *)&regval, sizeof(b_u32));
    /* clear the Value field */
    regval &= ~(USPLUS100G_CTL_TX_LANE0_VLM_BIP7_OVERRIDE_VALUE_MASK);
    /* update Value field */
    regval |= pbip7_config->bip7_value;
    /* clear the Valid bit */
    regval &= ~(USPLUS100G_CTL_TX_LANE0_VLM_BIP7_OVERRIDE_BIT_MASK);
    /* set/unset the Valid bit */
    regval |= pbip7_config->bip7_valid;
    USPLUS100G_REG_WRITE(p_usplus100g, USPLUS100G_CONFIGURATION_TX_BIP_OVERRIDE, 
                         sizeof(b_u32), 0, 
                         (b_u8 *)&regval, sizeof(b_u32));    

    return CCL_OK;
}

/**
 * @brief Read the current bip7 value and its enabled or 
 * disabled status 
 *
 * @param[out] pbip7config is a pointer to hold bip7 
 *       configuration values.
 *
 * @return CCL_OK on success, error code on failure 
 *
 * @note This value is read from one of the Rx interface status register.
 */ 
static ccl_err_t _usplus100g_get_bip7_config(usplus100g_ctx_t *p_usplus100g, 
                                             usplus100g_bip7_config_t *pbip7_config)
{   
    b_u32 regval;

    if ( !p_usplus100g || !pbip7_config ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_STAT_RX_BIP_OVERRIDE_REG, 
                        sizeof(b_u32), 0, 
                        (b_u8 *)&regval, sizeof(b_u32));
    pbip7_config->bip7_value = (regval & USPLUS100G_STAT_RX_BIP_OVERRIDE_BIP7_MASK);
    pbip7_config->bip7_valid = (b_u8)((regval & USPLUS100G_STAT_RX_BIP_OVERRIDE_VALID_MASK) >>
                                      USPLUS100G_STAT_RX_BIP_OVERRIDE_VALID_BIT);
    return CCL_OK;
}

/**
 * @brief Configure the Tx flow control.
 *
 * @param[in] pflow_control is a pointer to hold flow control 
 *       configuration values.
 *
 * @return CCL_OK on success, error code on failure 
 */
static ccl_err_t _usplus100g_set_tx_flow_control_config(usplus100g_ctx_t *p_usplus100g, 
                                                        usplus100g_tx_flow_control_t *pflow_control)
{
    b_u32  refresh = 0, quanta = 0;
    b_u32 index = 0;
    b_u32 offset_1 = USPLUS100G_CONFIGURATION_TX_FLOW_CONTROL_REFRESH_REG1;
    b_u32 offset_2 = USPLUS100G_CONFIGURATION_TX_FLOW_CONTROL_QUANTA_REG1;

    if ( !p_usplus100g || !pflow_control ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* set refresh intervals and set quantas */
    for (index = 0; index <= USPLUS100G_TOTAL_TX_PAUSE_REFRESH_TIMERS; index += 2) {
        refresh = pflow_control->refresh_interval[index];
        quanta = pflow_control->pause_quanta[index];
        if (index != USPLUS100G_TOTAL_TX_PAUSE_REFRESH_TIMERS) {
            /* refresh */
            refresh <<= USPLUS100G_SIXTEEN_BITS;
            refresh = refresh |
                      pflow_control->refresh_interval[index + 1];
            /* quanta */
            quanta <<= USPLUS100G_SIXTEEN_BITS;
            quanta = quanta |
                     pflow_control->pause_quanta[index + 1];
        }
        /* This sets the retransmission time of pause packets for
         * priority 0 and priority 1 and 2, 3 and so on */
        USPLUS100G_REG_WRITE(p_usplus100g, 
                             USPLUS100G_CONFIGURATION_TX_FLOW_CONTROL_REFRESH_REG1, 
                             sizeof(b_u32), 0, 
                             (b_u8 *)&refresh, sizeof(b_u32));    
        USPLUS100G_REG_WRITE(p_usplus100g, 
                             USPLUS100G_CONFIGURATION_TX_FLOW_CONTROL_QUANTA_REG1, 
                             sizeof(b_u32), 0, 
                             (b_u8 *)&quanta, sizeof(b_u32));    
        offset_1 += USPLUS100G_NUM_WORD_BYTES;
        offset_2 += USPLUS100G_NUM_WORD_BYTES;
    }
    /* set CTL_TX_PAUSE_ENABLE bits */
    USPLUS100G_REG_READ(p_usplus100g, 
                        USPLUS100G_CONFIGURATION_TX_FLOW_CONTROL_CONTROL_REG1, 
                        sizeof(b_u32), 0, 
                        (b_u8 *)&refresh, sizeof(b_u32));
    /* clear the pause enable bit */
    refresh = refresh & (~USPLUS100G_CONFIG_TX_FLOW_CONTROL_REG1_MASK);
    /* set/unset the pause enable bit */
    refresh = refresh | pflow_control->pause_enable;
    USPLUS100G_REG_WRITE(p_usplus100g, 
                         USPLUS100G_CONFIGURATION_TX_FLOW_CONTROL_CONTROL_REG1, 
                         sizeof(b_u32), 0, 
                         (b_u8 *)&refresh, sizeof(b_u32));

    return CCL_OK;
}

/**
 * @brief Read the current the Tx flow control configuration.
 *
 * @param[out] pflow_control is a pointer to hold flow control
 *           configuration values.
 *
 * @return CCL_OK on success, error code on failure 
 *
 * @note none.
 */
static ccl_err_t _usplus100g_get_tx_flow_control_config(usplus100g_ctx_t *p_usplus100g, 
                                                        usplus100g_tx_flow_control_t *pflow_control)
{    
    b_u32  refresh = 0, quanta = 0;
    b_u32 index = 0;
    b_u32 offset_1 = USPLUS100G_CONFIGURATION_TX_FLOW_CONTROL_REFRESH_REG1;
    b_u32 offset_2 = USPLUS100G_CONFIGURATION_TX_FLOW_CONTROL_QUANTA_REG1;

    if ( !p_usplus100g || !pflow_control ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* read quanta and refresh timer values */
    for (index = 0; index <= USPLUS100G_TOTAL_TX_PAUSE_REFRESH_TIMERS; index += 2) {
        USPLUS100G_REG_READ(p_usplus100g, 
                            USPLUS100G_CONFIGURATION_TX_FLOW_CONTROL_REFRESH_REG1, 
                            sizeof(b_u32), 0, 
                            (b_u8 *)&refresh, sizeof(b_u32));    
        USPLUS100G_REG_READ(p_usplus100g, 
                            USPLUS100G_CONFIGURATION_TX_FLOW_CONTROL_QUANTA_REG1, 
                            sizeof(b_u32), 0, 
                            (b_u8 *)&quanta, sizeof(b_u32));    

        pflow_control->refresh_interval[index] = refresh & USPLUS100G_TWO_LSB_MASK;
        pflow_control->pause_quanta[index] = quanta & USPLUS100G_TWO_LSB_MASK;

        if (index != USPLUS100G_TOTAL_TX_PAUSE_REFRESH_TIMERS) {
            pflow_control->refresh_interval[index + 1] =
            (b_u16)(refresh >> USPLUS100G_SIXTEEN_BITS);
            pflow_control->pause_quanta[index + 1] =
            (b_u16)(quanta >> USPLUS100G_SIXTEEN_BITS);
        }
        offset_1 += USPLUS100G_NUM_WORD_BYTES;
        offset_2 += USPLUS100G_NUM_WORD_BYTES;
    }
    USPLUS100G_REG_READ(p_usplus100g, 
                        USPLUS100G_CONFIGURATION_TX_FLOW_CONTROL_CONTROL_REG1, 
                        sizeof(b_u32), 0, 
                        (b_u8 *)&refresh, sizeof(b_u32));

    /* get only pause enable bits */
    refresh = refresh & USPLUS100G_CONFIG_TX_FLOW_CONTROL_REG1_MASK;
    pflow_control->pause_enable = (b_u16)refresh;

    return CCL_OK;
}

/**
 * @brief Set the current the Rx flow control pause enable 
 *      status.
 *
 * @param[in] enable is a value to represent to enable or 
 *       disable.
 *
 * @return CCL_OK on success, error code on failure 
 */
static ccl_err_t _usplus100g_set_rx_flow_control_pause_enable(usplus100g_ctx_t *p_usplus100g, 
                                                              b_u16 enable)
{
    b_u32 regval;

    if ( !p_usplus100g ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    USPLUS100G_REG_READ(p_usplus100g, 
                        USPLUS100G_CONFIGURATION_RX_FLOW_CONTROL_CONTROL_REG1, 
                        sizeof(b_u32), 0, 
                        (b_u8 *)&regval, sizeof(b_u32));    
    /* clear all bits of pause enable field */
    regval &= ~(USPLUS100G_CTL_RX_PAUSE_ENABLE_MASK);
    regval |= enable;
    USPLUS100G_REG_WRITE(p_usplus100g, 
                         USPLUS100G_CONFIGURATION_RX_FLOW_CONTROL_CONTROL_REG1, 
                         sizeof(b_u32), 0, 
                         (b_u8 *)&regval, sizeof(b_u32));        

    return CCL_OK;
}

/**
 * @brief Read the current Rx flow control pause enable status.
 *
 * @param[out] pflow_control pointer to hold Rx flow control 
 *       pause enable status
 *
 * @return CCL_OK on success, error code on failure 
 */
static ccl_err_t _usplus100g_get_rx_flow_control_pause_enable(usplus100g_ctx_t *p_usplus100g, 
                                                              b_u16 *pflow_control)
{
    b_u32 regval;

    if ( !p_usplus100g ||!pflow_control ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    USPLUS100G_REG_READ(p_usplus100g, 
                        USPLUS100G_CONFIGURATION_RX_FLOW_CONTROL_CONTROL_REG1, 
                        sizeof(b_u32), 0, 
                        (b_u8 *)&regval, sizeof(b_u32));    

    *pflow_control = (b_u16)(regval & USPLUS100G_CTL_RX_PAUSE_ENABLE_MASK);

    return CCL_OK;
}

/**
 * @brief Configure the Rx packet priority and pause ack.
 *
 * @param[in] ack is used to set pause ack field.
 *
 * @return CCL_OK on success, error code on failure 
 */                                
static ccl_err_t _usplus100g_set_rx_flow_control_pause_ack(usplus100g_ctx_t *p_usplus100g, 
                                                           b_u16 ack)
{
    b_u32 regval;

    if ( !p_usplus100g ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    USPLUS100G_REG_READ(p_usplus100g, 
                        USPLUS100G_CONFIGURATION_RX_FLOW_CONTROL_CONTROL_REG1, 
                        sizeof(b_u32), 0, 
                        (b_u8 *)&regval, sizeof(b_u32));    
    /* clear all bits of pause enable ack field */
    regval &= ~(USPLUS100G_CTL_RX_PAUSE_ACK_ENABLE_MASK);
    regval |= (ack << USPLUS100G_CTL_RX_PAUSE_ACK_ENABLE_BIT);
    USPLUS100G_REG_WRITE(p_usplus100g, 
                         USPLUS100G_CONFIGURATION_RX_FLOW_CONTROL_CONTROL_REG1, 
                         sizeof(b_u32), 0, 
                         (b_u8 *)&regval, sizeof(b_u32));    

    return CCL_OK;
}

/**
 * @brief Read the current rx packet priority and pause ack 
 *     status.
 *
 * @param[out] pstatus pointer to hold Rx flow control pause 
 *       enable ack status
 *
 * @return CCL_OK on success, error code on failure 
 */
static ccl_err_t _usplus100g_get_rx_flow_control_pause_ack(usplus100g_ctx_t *p_usplus100g, 
                                                           b_u16 *pstatus)
{
    b_u32 regval;

    if ( !p_usplus100g || !pstatus ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    USPLUS100G_REG_READ(p_usplus100g, 
                        USPLUS100G_CONFIGURATION_RX_FLOW_CONTROL_CONTROL_REG1, 
                        sizeof(b_u32), 0, 
                        (b_u8 *)&regval, sizeof(b_u32));    
    /* retain all bits of pause enable ack field */
    regval &= USPLUS100G_CTL_RX_PAUSE_ACK_ENABLE_MASK;
    *pstatus = (b_u16)(regval >> USPLUS100G_CTL_RX_PAUSE_ACK_ENABLE_BIT);

    return CCL_OK;
}

/**
 * @brief Configure the Rx flow control packet types and enable
 * or disables the Rx flow control parameters. 
 *
 * @param[in] packet holds packet types parameters.
 * @param[in] flow_control is pointer to hold flow control 
 *           parameters to enable or disable
 *
 * @return CCL_OK on success, error code on failure 
 * 
 * @note This function will enable gcp, pcp, gpp, ppp.
 */
static ccl_err_t _usplus100g_set_rx_flow_control_packet_config(usplus100g_ctx_t *p_usplus100g, 
                                                               usplus100g_flow_control_packet_type_t packet, 
                                                               usplus100g_rx_flow_control_t *flow_control)
{
    b_u32 value = 0, check_value = 0;

    if ( !p_usplus100g || !flow_control ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* used to set ctl_rx_pause_enable */
    USPLUS100G_REG_READ(p_usplus100g, 
                        USPLUS100G_CONFIGURATION_RX_FLOW_CONTROL_CONTROL_REG1, 
                        sizeof(b_u32), 0, 
                        (b_u8 *)&value, sizeof(b_u32));    
    USPLUS100G_REG_READ(p_usplus100g, 
                        USPLUS100G_CONFIGURATION_RX_FLOW_CONTROL_CONTROL_REG2, 
                        sizeof(b_u32), 0, 
                        (b_u8 *)&check_value, sizeof(b_u32));    

    switch (packet) {
    case USPLUS100G_FC_GLOBAL_CONTROL_PACKET:
        if (flow_control->enable)
            set_bit(value, USPLUS100G_CTL_RX_ENABLE_GCP_BIT);
        else
            clear_bit(value, USPLUS100G_CTL_RX_ENABLE_GCP_BIT);

        USPLUS100G_REG_WRITE(p_usplus100g, 
                             USPLUS100G_CONFIGURATION_RX_FLOW_CONTROL_CONTROL_REG1, 
                             sizeof(b_u32), 0, 
                             (b_u8 *)&value, sizeof(b_u32));    
        /* multicast, unicast, source_address, ether_type,
         * opcode enable/disable */
        if (flow_control->check_multicast)
            set_bit(check_value, USPLUS100G_CTL_RX_CHECK_MCAST_GCP_BIT);
        if (flow_control->check_unicast)
            set_bit(check_value, USPLUS100G_CTL_RX_CHECK_UCAST_GCP_BIT);
        if (flow_control->check_source_address)
            set_bit(check_value, USPLUS100G_CTL_RX_CHECK_SA_GCP_BIT);
        if (flow_control->check_ether_type)
            set_bit(check_value, USPLUS100G_CTL_RX_CHECK_ETYPE_GCP_BIT);
        if (flow_control->check_opcode)
            set_bit(check_value, USPLUS100G_CTL_RX_CHECK_OPCODE_GCP_BIT);
        USPLUS100G_REG_WRITE(p_usplus100g, 
                             USPLUS100G_CONFIGURATION_RX_FLOW_CONTROL_CONTROL_REG2, 
                             sizeof(b_u32), 0, 
                             (b_u8 *)&check_value, sizeof(b_u32));    
        break;
    case USPLUS100G_FC_PRIORITY_CONTROL_PACKET:
        if (flow_control->enable)
            set_bit(value, USPLUS100G_CTL_RX_ENABLE_PCP_BIT);
        else
            clear_bit(value, USPLUS100G_CTL_RX_ENABLE_PCP_BIT);
        USPLUS100G_REG_WRITE(p_usplus100g, 
                             USPLUS100G_CONFIGURATION_RX_FLOW_CONTROL_CONTROL_REG1, 
                             sizeof(b_u32), 0, 
                             (b_u8 *)&value, sizeof(b_u32));    
        if (flow_control->check_multicast)
            set_bit(check_value, USPLUS100G_CTL_RX_CHECK_MCAST_PCP_BIT);
        if (flow_control->check_unicast)
            set_bit(check_value, USPLUS100G_CTL_RX_CHECK_UCAST_PCP_BIT);
        if (flow_control->check_source_address)
            set_bit(check_value, USPLUS100G_CTL_RX_CHECK_SA_PCP_BIT);
        if (flow_control->check_ether_type)
            set_bit(check_value, USPLUS100G_CTL_RX_CHECK_ETYPE_PCP_BIT);
        if (flow_control->check_opcode)
            set_bit(check_value, USPLUS100G_CTL_RX_CHECK_OPCODE_PCP_BIT);
        USPLUS100G_REG_WRITE(p_usplus100g, 
                             USPLUS100G_CONFIGURATION_RX_FLOW_CONTROL_CONTROL_REG2, 
                             sizeof(b_u32), 0, 
                             (b_u8 *)&check_value, sizeof(b_u32));    
        break;
    case USPLUS100G_FC_GLOBAL_PAUSE_PACKET:
        if (flow_control->enable)
            set_bit(value, USPLUS100G_CTL_RX_ENABLE_GPP_BIT);
        else
            clear_bit(value, USPLUS100G_CTL_RX_ENABLE_GPP_BIT);
        USPLUS100G_REG_WRITE(p_usplus100g, 
                             USPLUS100G_CONFIGURATION_RX_FLOW_CONTROL_CONTROL_REG1, 
                             sizeof(b_u32), 0, 
                             (b_u8 *)&value, sizeof(b_u32));    
        if (flow_control->check_multicast)
            set_bit(check_value, USPLUS100G_CTL_RX_CHECK_MCAST_GPP_BIT);
        if (flow_control->check_unicast)
            set_bit(check_value, USPLUS100G_CTL_RX_CHECK_UCAST_GPP_BIT);
        if (flow_control->check_source_address)
            set_bit(check_value, USPLUS100G_CTL_RX_CHECK_SA_GPP_BIT);
        if (flow_control->check_ether_type)
            set_bit(check_value, USPLUS100G_CTL_RX_CHECK_ETYPE_GPP_BIT);
        if (flow_control->check_opcode)
            set_bit(check_value, USPLUS100G_CTL_RX_CHECK_OPCODE_GPP_BIT);
        USPLUS100G_REG_WRITE(p_usplus100g, 
                             USPLUS100G_CONFIGURATION_RX_FLOW_CONTROL_CONTROL_REG2, 
                             sizeof(b_u32), 0, 
                             (b_u8 *)&check_value, sizeof(b_u32));    
        break;
    case USPLUS100G_FC_PRIORITY_PAUSE_PACKET:
        if (flow_control->enable)
            set_bit(value, USPLUS100G_CTL_RX_ENABLE_PPP_BIT);
        else
            clear_bit(value, USPLUS100G_CTL_RX_ENABLE_PPP_BIT);
        USPLUS100G_REG_WRITE(p_usplus100g, 
                             USPLUS100G_CONFIGURATION_RX_FLOW_CONTROL_CONTROL_REG1, 
                             sizeof(b_u32), 0, 
                             (b_u8 *)&value, sizeof(b_u32));    
        if (flow_control->check_multicast)
            set_bit(check_value, USPLUS100G_CTL_RX_CHECK_MCAST_PPP_BIT);
        if (flow_control->check_unicast)
            set_bit(check_value, USPLUS100G_CTL_RX_CHECK_UCAST_PPP_BIT);
        if (flow_control->check_source_address)
            set_bit(check_value, USPLUS100G_CTL_RX_CHECK_SA_PPP_BIT);
        if (flow_control->check_ether_type)
            set_bit(check_value, USPLUS100G_CTL_RX_CHECK_ETYPE_PPP_BIT);
        if (flow_control->check_opcode)
            set_bit(check_value, USPLUS100G_CTL_RX_CHECK_OPCODE_PPP_BIT);
        USPLUS100G_REG_WRITE(p_usplus100g, 
                             USPLUS100G_CONFIGURATION_RX_FLOW_CONTROL_CONTROL_REG2, 
                             sizeof(b_u32), 0, 
                             (b_u8 *)&check_value, sizeof(b_u32));    
        break;
    default:
        return CCL_BAD_PARAM_ERR;
    }

    return CCL_OK;
}

/**
 * @brief Read the current Rx flow control packet configuration.
 *
 * @param[in] packet holds packet types parameters.
 * @param[out] flow_control is pointer to hold flow control 
 *           parameters to enable or disable
 *
 * @return CCL_OK on success, error code on failure
 */
static ccl_err_t _usplus100g_get_rx_flow_control_packet_config(usplus100g_ctx_t *p_usplus100g, 
                                                               usplus100g_flow_control_packet_type_t packet, 
                                                               usplus100g_rx_flow_control_t *flow_control)
{
    b_u32 value = 0, check_value = 0;

    if ( !p_usplus100g || !flow_control ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    USPLUS100G_REG_READ(p_usplus100g, 
                        USPLUS100G_CONFIGURATION_RX_FLOW_CONTROL_CONTROL_REG1, 
                        sizeof(b_u32), 0, 
                        (b_u8 *)&value, sizeof(b_u32));    
    USPLUS100G_REG_READ(p_usplus100g, 
                        USPLUS100G_CONFIGURATION_RX_FLOW_CONTROL_CONTROL_REG2, 
                        sizeof(b_u32), 0, 
                        (b_u8 *)&check_value, sizeof(b_u32));    

    switch (packet) {
    case USPLUS100G_FC_GLOBAL_CONTROL_PACKET:
        flow_control->enable = (value &
                                USPLUS100G_CTL_RX_ENABLE_GCP_BIT_MASK) >>
                                USPLUS100G_CTL_RX_ENABLE_GCP_BIT;
        flow_control->check_multicast = (value &
                                         USPLUS100G_CTL_RX_CHECK_MCAST_GCP_BIT_MASK) >>
                                         USPLUS100G_CTL_RX_CHECK_MCAST_GCP_BIT;
        flow_control->check_unicast = (value &
                                       USPLUS100G_CTL_RX_CHECK_UCAST_GCP_BIT_MASK) >>
                                       USPLUS100G_CTL_RX_CHECK_UCAST_GCP_BIT;
        flow_control->check_source_address = (value &
                                              USPLUS100G_CTL_RX_CHECK_SA_GCP_BIT_MASK) >>
                                              USPLUS100G_CTL_RX_CHECK_SA_GCP_BIT;
        flow_control->check_multicast = (value &
                                         USPLUS100G_CTL_RX_CHECK_ETYPE_GCP_BIT_MASK) >>
                                         USPLUS100G_CTL_RX_CHECK_ETYPE_GCP_BIT;
        flow_control->check_opcode = (value &
                                      USPLUS100G_CTL_RX_CHECK_OPCODE_GCP_BIT_MASK) >>
                                      USPLUS100G_CTL_RX_CHECK_OPCODE_GCP_BIT;
        break;
    case USPLUS100G_FC_PRIORITY_CONTROL_PACKET:
        flow_control->enable = (value &
                                USPLUS100G_CTL_RX_ENABLE_PCP_BIT_MASK) >>
                                USPLUS100G_CTL_RX_ENABLE_PCP_BIT;
        /*
         * multicast, unicast, source_address, ether_type,
         * opcode enable/disable
         */
        flow_control->check_multicast = (value &
                                         USPLUS100G_CTL_RX_CHECK_MCAST_PCP_BIT_MASK) >>
                                         USPLUS100G_CTL_RX_CHECK_MCAST_PCP_BIT;
        flow_control->check_unicast = (value &
                                       USPLUS100G_CTL_RX_CHECK_UCAST_PCP_BIT_MASK) >>
                                       USPLUS100G_CTL_RX_CHECK_UCAST_PCP_BIT;
        flow_control->check_source_address = (value &
                                              USPLUS100G_CTL_RX_CHECK_SA_PCP_BIT_MASK) >>
                                              USPLUS100G_CTL_RX_CHECK_SA_PCP_BIT;
        flow_control->check_multicast = (value &
                                         USPLUS100G_CTL_RX_CHECK_ETYPE_PCP_BIT_MASK) >>
                                         USPLUS100G_CTL_RX_CHECK_ETYPE_PCP_BIT;
        flow_control->check_opcode = (value &
                                      USPLUS100G_CTL_RX_CHECK_OPCODE_PCP_BIT_MASK) >>
                                      USPLUS100G_CTL_RX_CHECK_OPCODE_PCP_BIT;
        break;
    case USPLUS100G_FC_GLOBAL_PAUSE_PACKET:
        flow_control->enable = (value &
                                USPLUS100G_CTL_RX_ENABLE_GPP_BIT_MASK) >> 
                                USPLUS100G_CTL_RX_ENABLE_GPP_BIT;
        flow_control->check_multicast = (value &
                                         USPLUS100G_CTL_RX_CHECK_MCAST_GPP_BIT_MASK) >>
                                         USPLUS100G_CTL_RX_CHECK_MCAST_GPP_BIT;
        flow_control->check_unicast = (value &
                                       USPLUS100G_CTL_RX_CHECK_UCAST_GPP_BIT_MASK) >>
                                       USPLUS100G_CTL_RX_CHECK_UCAST_GPP_BIT;
        flow_control->check_source_address = (value &
                                              USPLUS100G_CTL_RX_CHECK_SA_GPP_BIT_MASK) >>
                                              USPLUS100G_CTL_RX_CHECK_SA_GPP_BIT;
        flow_control->check_multicast = (value &
                                         USPLUS100G_CTL_RX_CHECK_ETYPE_GPP_BIT_MASK) >>
                                         USPLUS100G_CTL_RX_CHECK_ETYPE_GPP_BIT;
        flow_control->check_opcode = (value &
                                      USPLUS100G_CTL_RX_CHECK_OPCODE_GPP_BIT_MASK) >>
                                      USPLUS100G_CTL_RX_CHECK_OPCODE_GPP_BIT;
        break;
    case USPLUS100G_FC_PRIORITY_PAUSE_PACKET:
        flow_control->enable = (value &
                                USPLUS100G_CTL_RX_ENABLE_PPP_BIT_MASK) >>
                                USPLUS100G_CTL_RX_ENABLE_PPP_BIT;
        flow_control->check_multicast = (value &
                                         USPLUS100G_CTL_RX_CHECK_MCAST_PPP_BIT_MASK) >>
                                         USPLUS100G_CTL_RX_CHECK_MCAST_GCP_BIT;
        flow_control->check_unicast = (value &
                                       USPLUS100G_CTL_RX_CHECK_UCAST_PPP_BIT_MASK) >>
                                       USPLUS100G_CTL_RX_CHECK_UCAST_PPP_BIT;
        flow_control->check_source_address = (value &
                                              USPLUS100G_CTL_RX_CHECK_SA_PPP_BIT_MASK) >>
                                              USPLUS100G_CTL_RX_CHECK_SA_PPP_BIT;
        flow_control->check_multicast = (value &
                                         USPLUS100G_CTL_RX_CHECK_ETYPE_PPP_BIT_MASK) >>
                                         USPLUS100G_CTL_RX_CHECK_ETYPE_PPP_BIT;
        flow_control->check_opcode = (value &
                                      USPLUS100G_CTL_RX_CHECK_OPCODE_PPP_BIT_MASK) >>
                                      USPLUS100G_CTL_RX_CHECK_OPCODE_PPP_BIT;
        break;
    default:
        return CCL_BAD_PARAM_ERR;
    }

    return CCL_OK;
}

/**
 * @brief Read the current Tx status.
 *
 * @param[out] tx_status is a pointer to hold Tx status 
 *       parameters.
 *
 * @return CCL_OK on success, error code on failure 
 * 
 * @note STAT_RX_STATUS_REG and STAT_STATUS_REG1 both register stats are
 * combined in this function. 
 */
static ccl_err_t _usplus100g_get_tx_status(usplus100g_ctx_t *p_usplus100g, 
                                           usplus100g_tx_status_t *tx_status)
{     
    b_u32 regval;

    if ( !p_usplus100g || !tx_status ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_STAT_TX_STATUS_REG, 
                        sizeof(b_u32), 0, 
                        (b_u8 *)&regval, sizeof(b_u32));    
    tx_status->local_fault = (regval &
                              USPLUS100G_STAT_TX_LOCAL_FAULT_MASK) >>
                              USPLUS100G_STAT_TX_LOCAL_FAULT_BIT;
    USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_STAT_STATUS_REG1, 
                        sizeof(b_u32), 0, 
                        (b_u8 *)&regval, sizeof(b_u32));    
    tx_status->ptp_fifo_read_error = (regval &
                                      USPLUS100G_STAT_TX_PTP_FIFO_R_ERR_MASK) >>
                                      USPLUS100G_STAT_TX_PTP_FIFO_R_ERR_BIT;
    tx_status->ptp_fifo_write_error = (regval &
                                       USPLUS100G_STAT_TX_PTP_FIFO_W_ERR_MASK) >>
                                       USPLUS100G_STAT_TX_PTP_FIFO_W_ERR_BIT;
    return CCL_OK;
}

/**
 * @brief Read the current Rx lane status.
 *
 * @param[out] rx_lane_status is a pointer to hold Rx status 
 *       parameters.
 *
 * @return CCL_OK on success, error code on failure
 */
static ccl_err_t _usplus100g_get_rx_lane_status(usplus100g_ctx_t *p_usplus100g, 
                                                usplus100g_rx_lane_am_status_t *rx_lane_status)
{
    b_u32  block_lock = 0, sync = 0, sync_err = 0, lm_err = 0, lm_len_err = 0;
    b_u32  lm_repeat_err = 0, rx_status = 0;
    b_u32 lane = 0;

    if ( !p_usplus100g || !rx_lane_status ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_STAT_RX_STATUS_REG, 
                        sizeof(b_u32), 0, 
                        (b_u8 *)&rx_status, sizeof(b_u32));    
    rx_lane_status->pcs_status =
    (rx_status & USPLUS100G_STAT_RX_MASK) ? 1: 0;
    rx_lane_status->aligned =
    (rx_status & USPLUS100G_STAT_RX_ALIGNED_MASK) ? 1: 0;
    rx_lane_status->mis_aligned =
    (rx_status & USPLUS100G_STAT_RX_MISALIGNED_MASK) ? 1: 0;
    rx_lane_status->aligned_error =
    (rx_status & USPLUS100G_STAT_RX_ALIGNED_ERR_MASK) ? 1: 0;
    USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_STAT_RX_BLOCK_LOCK_REG, 
                        sizeof(b_u32), 0, 
                        (b_u8 *)&block_lock, sizeof(b_u32));   
    block_lock &= USPLUS100G_STAT_RX_BLOCK_LOCK_MASK; 
    USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_STAT_RX_LANE_SYNC_REG, 
                        sizeof(b_u32), 0, 
                        (b_u8 *)&sync, sizeof(b_u32)); 
    sync &= USPLUS100G_STAT_RX_SYNCED_MASK;  
    USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_STAT_RX_LANE_SYNC_ERR_REG, 
                        sizeof(b_u32), 0, 
                        (b_u8 *)&sync_err, sizeof(b_u32)); 
    sync_err |= USPLUS100G_STAT_RX_SYNCED_ERR_MASK;
    USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_STAT_RX_AM_ERR_REG, 
                        sizeof(b_u32), 0, 
                        (b_u8 *)&lm_err, sizeof(b_u32)); 
    lm_err |= USPLUS100G_STAT_RX_MF_ERR_MASK;
    USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_STAT_RX_AM_LEN_ERR_REG, 
                        sizeof(b_u32), 0, 
                        (b_u8 *)&lm_len_err, sizeof(b_u32)); 
    lm_len_err |= USPLUS100G_STAT_RX_MF_LEN_ERR_MASK;
    USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_STAT_RX_AM_REPEAT_ERR_REG, 
                        sizeof(b_u32), 0, 
                        (b_u8 *)&lm_repeat_err, sizeof(b_u32)); 
    lm_repeat_err |= USPLUS100G_STAT_RX_MF_REPEAT_ERR_MASK;
    for (lane = 0; lane < USPLUS100G_PCS_LANE_COUNT; lane++) {
        rx_lane_status->block_lock[lane] = (b_u8)(block_lock & 1);
        block_lock = block_lock >> 1;
        rx_lane_status->synced[lane] = (b_u8)(sync & 1);
        sync = sync >> 1;
        rx_lane_status->synced_error[lane] = (b_u8)(sync_err & 1);
        sync_err = sync_err >> 1;
        rx_lane_status->lane_marker_error[lane] = (b_u8)(lm_err & 1);
        lm_err = lm_err >> 1;
        rx_lane_status->lane_marker_len_error[lane] =
        (b_u8)(lm_len_err & 1);
        lm_len_err = lm_len_err >> 1;
        rx_lane_status->lane_marker_repeat_error[lane] =
        (b_u8)(lm_repeat_err & 1);
        lm_repeat_err = lm_repeat_err >> 1;
    }

    return CCL_OK;
}

/**
 * @brief Read the current rx fault status.
 *
 * @param[out] rx_fault_status is a pointer to hold rx fault 
 *       status parameters.
 *
 * @return CCL_OK on success, error code on failure
 */
static ccl_err_t _usplus100g_get_rx_fault_status(usplus100g_ctx_t *p_usplus100g, 
                                                 usplus100g_rx_fault_status_t *rx_fault_status)
{
    b_u32 regval;

    if ( !p_usplus100g || !rx_fault_status ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    USPLUS100G_REG_READ(p_usplus100g, 
                        USPLUS100G_STAT_RX_STATUS_REG, 
                        sizeof(b_u32), 0, 
                        (b_u8 *)&regval, sizeof(b_u32));    
    rx_fault_status->remote_fault = (regval &
                                     USPLUS100G_STAT_RX_REMOTE_FAULT_MASK) >>
                                     USPLUS100G_STAT_RX_REMOTE_FAULT_BIT;
    rx_fault_status->local_fault = (regval &
                                    USPLUS100G_STAT_RX_LOCAL_FAULT_MASK) >>
                                    USPLUS100G_STAT_RX_LOCAL_FAULT_BIT;
    rx_fault_status->internal_local_fault = (regval &
                                             USPLUS100G_STAT_RX_INTERNAL_LOCAL_FAULT_MASK) >>
                                             USPLUS100G_STAT_RX_INTERNAL_LOCAL_FAULT_BIT;
    rx_fault_status->received_local_fault = (regval &
                                             USPLUS100G_STAT_RX_RECEIVED_LOCAL_FAULT_MASK) >>
                                             USPLUS100G_STAT_RX_RECEIVED_LOCAL_FAULT_BIT;
    return CCL_OK;
}

/**
 * @brief Read the current Rx packet status.
 *
 * @param[out] rx_packet_status is a pointer to hold rx packet
 *       status parameters.
 *
 * @return CCL_OK on success, error code on failure
 */
static ccl_err_t _usplus100g_get_rx_packet_status(usplus100g_ctx_t *p_usplus100g, 
                                                  usplus100g_rx_packet_status_t *rx_packet_status)
{    
    b_u32 regval;

    if ( !p_usplus100g || !rx_packet_status ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_STAT_RX_STATUS_REG, 
                        sizeof(b_u32), 0, 
                        (b_u8 *)&regval, sizeof(b_u32));    
    rx_packet_status->hi_ber = (regval & USPLUS100G_STAT_RX_HI_BER_MASK) >>
                               USPLUS100G_STAT_RX_HI_BER_BIT;
    rx_packet_status->bad_preamble =
    (regval & USPLUS100G_STAT_RX_BAD_PREAMBLE_MASK) >>
    USPLUS100G_STAT_RX_BAD_PREAMBLE_BIT;
    rx_packet_status->bad_sfd = (regval & USPLUS100G_STAT_RX_BAD_SFD_MASK) >>
                                USPLUS100G_STAT_RX_BAD_SFD_BIT;
    rx_packet_status->got_signal_os = (regval &
                                       USPLUS100G_STAT_RX_GOT_SIGNAL_OS_MASK) >>
                                      USPLUS100G_STAT_RX_GOT_SIGNAL_OS_BIT;
    return CCL_OK;
}

/**
 * @brief Read the current Rx vl demux status of each lane.
 *
 * @param[out] rx_vldemux_status is a pointer to hold Rx vl 
 *       demux status parameters.
 *
 * @return CCL_OK on success, error code on failure
 */
static ccl_err_t _usplus100g_get_rx_vldemux_status(usplus100g_ctx_t *p_usplus100g, 
                                                   usplus100g_rx_vldemux_status_t *rx_vldemux_status)
{   
    b_u32  vldemuxed = 0, vlnumber = 0;
    b_u32 lane = 0, bit_num = 0, offset = 0, mask = 0;

    if ( !p_usplus100g || !rx_vldemux_status ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* vl demux status */
    USPLUS100G_REG_READ(p_usplus100g, 
                        USPLUS100G_STAT_RX_PCSL_DEMUXED_REG, 
                        sizeof(b_u32), 0, 
                        (b_u8 *)&vldemuxed, sizeof(b_u32));    
    vldemuxed |= USPLUS100G_STAT_RX_VL_DEMUXED_MASK;
    for (lane = 0; lane < USPLUS100G_PCS_LANE_COUNT; lane++) {
        rx_vldemux_status->vldemuxed[lane] = (b_u8)(vldemuxed & 1);
        vldemuxed = vldemuxed >> 1;
    }
    for (lane = 0; lane < USPLUS100G_PCS_LANE_COUNT; lane++, bit_num += 5) {
        /* 0 to 4 bits for lane 0
         * 5 to 9 bits for lane 1
         * 10 to 14 bits for lane 2
         * 15 to 19 bits for lane 3
         * 20 to 24 bits for lane 4
         * 25 to 29 bits for lane 5
         * */
        if (lane == 0) {
            offset = USPLUS100G_STAT_RX_PCS_LANE_NUM_REG1;
            /* reset bit number */
            bit_num = 0;
            mask = USPLUS100G_STAT_RX_VL_NUM_0_MASK;
        }
        if (lane == 6) {
            offset = USPLUS100G_STAT_RX_PCS_LANE_NUM_REG2;
            /* reset bit number */
            bit_num = 0;
            mask = USPLUS100G_STAT_RX_VL_NUM_6_MASK;
        }
        if (lane == 12) {
            offset = USPLUS100G_STAT_RX_PCS_LANE_NUM_REG3;
            /* reset bit number */
            bit_num = 0;
            mask = USPLUS100G_STAT_RX_VL_NUM_12_MASK;
        }
        if (lane == 18) {
            offset = USPLUS100G_STAT_RX_PCS_LANE_NUM_REG4;
            /* reset bit number */
            bit_num = 0;
            mask = USPLUS100G_STAT_RX_VL_NUM_18_MASK;
        }
        USPLUS100G_REG_READ(p_usplus100g, 
                            offset, 
                            sizeof(b_u32), 0, 
                            (b_u8 *)&vlnumber, sizeof(b_u32));    
        vlnumber &= mask;
        rx_vldemux_status->vlnumber[lane++] =(vlnumber >> bit_num) & USPLUS100G_STAT_RX_VL_NUM_MASK;
    }
    return CCL_OK;
}

/**
 * @brief Read the current Rx test pattern mismatch status.
 *  
 * @param[out] pstatus is a pointer to hold Rx pattern mismatch 
 *       status
 *
 * @return CCL_OK on success, error code on failure
 */                        
static ccl_err_t _usplus100g_get_rx_test_pattern_status(usplus100g_ctx_t *p_usplus100g, 
                                                        b_u8 *pstatus)
{
    b_u32 regval;

    if ( !p_usplus100g || !pstatus ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    USPLUS100G_REG_READ(p_usplus100g, USPLUS100G_STAT_RX_STATUS_REG, 
                        sizeof(b_u32), 0, 
                        (b_u8 *)&regval, sizeof(b_u32));    

    *pstatus = (b_u8) ((regval & USPLUS100G_STAT_RX_TEST_PATTERN_MISMATCH_MASK) >> 
                       USPLUS100G_STAT_RX_TEST_PATTERN_MISMATCH_BIT);

    return CCL_OK;
}

/**
 * @brief Sets calibration parameters into DRP register space
 *
 * @param[in] p_usplus100g is a pointer to usplus100g_ctx_t instance to 
 *       be worked on
 * @return	error code
 */
static ccl_err_t _usplus100g_calib_set(usplus100g_ctx_t *p_usplus100g)
{
    b_u32 i, j;

    if ( !p_usplus100g ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    if (p_usplus100g->brdspec.calib_ctl.enable) {
        for (i = 0; i < p_usplus100g->brdspec.calib_ctl.channel_num; i++) {
            for (j = 0; j < CALIB_PARAMS_PER_CHANNEL_NUM; j++) {
                b_u32 offset = p_usplus100g->brdspec.calib_ctl.channel_calib_params[i].calib[j].offset;
                b_u8 value = p_usplus100g->brdspec.calib_ctl.channel_calib_params[i].calib[j].encoded_value;
                _ret = devspibus_write_buff(p_usplus100g->brdspec.devname, 
                                            offset,
                                            0, 
                                            &value, 
                                            sizeof(b_u32));
                if ( _ret ) {
                    PDEBUG("Error! Failed to write over spi bus\n");
                    USPLUS100G_ERROR_RETURN(_ret);
                }    
            }
        }
    }

    return CCL_OK;
}

/**
 * @brief Init USPLUS100G instance
 *
 * @param[in] p_usplus100g pointer to the USPLUS100G instance to
 *       be worked on
 *
 * @return CCL_OK on success, error code on failure 
 */
static ccl_err_t _usplus100g_init(void *p_priv)
{
    usplus100g_ctx_t *p_usplus100g;
    b_u32            regval;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_usplus100g = (usplus100g_ctx_t *)p_priv;

    _ret = _usplus100g_calib_set(p_usplus100g);
    if ( _ret ) {
        PDEBUG("Error! _usplus_drp_set failed\n");
        USPLUS100G_ERROR_RETURN(CCL_FAIL);
    }

    /* Set RSFEC */
    regval = 0x3;
    USPLUS100G_REG_WRITE(p_usplus100g, USPLUS100G_RSFEC_CONFIG_ENABLE, 
                         sizeof(b_u32), 0, 
                         (b_u8 *)&regval, sizeof(b_u32));

    regval = 0;
    USPLUS100G_REG_WRITE(p_usplus100g, USPLUS100G_CONFIGURATION_TX_REG1, 
                         sizeof(b_u32), 0, 
                         (b_u8 *)&regval, sizeof(b_u32));
    USPLUS100G_REG_WRITE(p_usplus100g, USPLUS100G_CONFIGURATION_RX_REG1, 
                         sizeof(b_u32), 0, 
                         (b_u8 *)&regval, sizeof(b_u32));

    regval = 1;
    USPLUS100G_REG_WRITE(p_usplus100g, USPLUS100G_CONFIGURATION_RX_REG1, 
                         sizeof(b_u32), 0, 
                         (b_u8 *)&regval, sizeof(b_u32));
    _ret = _usplus100g_set_fault_indication(p_usplus100g, 1);
    if ( _ret ) {
        PDEBUG("Error! _usplus100g_set_fault_indication failed\n");
        USPLUS100G_ERROR_RETURN(CCL_FAIL);
    }
    regval = 1;
    USPLUS100G_REG_WRITE(p_usplus100g, USPLUS100G_CONFIGURATION_TX_REG1, 
                         sizeof(b_u32), 0, 
                         (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/**
 * @brief Issue tick so that statistics are copied to readable 
 * registers 
 *
 * @param None
 *
 * @return CCL_OK on success, error code on failure 
 */
static ccl_err_t _usplus100g_latch_statistics(usplus100g_ctx_t *p_usplus100g)
{
    b_u32 regval;

    if ( !p_usplus100g ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    regval = USPLUS100G_TICK_REGISTER_EN_MASK;
    /* write of 1 will trigger a snapshot of counters into registers */
    USPLUS100G_REG_WRITE(p_usplus100g, USPLUS100G_TICK_REG, 
                         sizeof(b_u32), 0, 
                         (b_u8 *)&regval, sizeof(b_u32));    

    return CCL_OK;
}

/**
 * @brief Read the cycle count, a count of the 
 * number of serdes clock cycles between tick_reg register 
 * writes. the values in the counters are held until the next 
 * write to the tick_reg register. 
 *
 * @param[out] pcount is a pointer to hold 64 bit cycle count
 *
 * @return CCL_OK on success, error code on failure
 *
 * @note The STAT_CYCLE_COUNT_MSB/LSB register contains a count of the number of SerDes clock
 * cycles between TICK_REG register writes.
 */
static ccl_err_t _usplus100g_get_cycle_count(usplus100g_ctx_t *p_usplus100g, b_u64 *pcount)
{
    if ( !p_usplus100g || !pcount ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    _ret = _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_CYCLE_COUNT, pcount);
    if ( _ret ) {
        PDEBUG("_usplus100g_read_48bit_value error\n");
        USPLUS100G_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

/**
 * @brief Read a list of values representing the parity errors 
 * for each lane in the system 
 *
 * @param[out] bip_err_count holds the list of values.
 *
 * @return CCL_OK on success, error code on failure 
 */
static ccl_err_t _usplus100g_get_rx_bip_error_count(usplus100g_ctx_t *p_usplus100g, 
                                                    usplus100g_bip8_error_t bip_err_count)
{
    int index = 0;
    b_u32 offset = 0;
    b_u64 value = 0;

    if ( !p_usplus100g ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    offset = USPLUS100G_STAT_RX_BIP_ERR_0;
    for (index = 0; index < USPLUS100G_PCS_LANE_COUNT; index++) {
        _ret = _usplus100g_read_48bit_value(p_usplus100g, offset, &value);
        if ( _ret ) {
            PDEBUG("_usplus100g_read_48bit_value error\n");
            USPLUS100G_ERROR_RETURN(_ret);
        }
        bip_err_count[index] = value;
        offset += USPLUS100G_NUM_DWORD_BYTES;
    }

    return CCL_OK;
}

/**
 * @brief Read the list of values representing the receive 
 * framing error counters. 
 *
 * @param[out] framing_err_count holds the list of framing error
 *       count values.
 *
 * @return CCL_OK on success, error code on failure 
 */
static ccl_err_t _usplus100g_get_rx_framing_error_count(usplus100g_ctx_t *p_usplus100g, 
                                                        usplus100g_framing_error_t framing_err_count)
{
    int index = 0;
    b_u32 offset = 0;
    b_u64 value = 0;

    if ( !p_usplus100g ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    offset = USPLUS100G_STAT_RX_FRAMING_ERR0;
    for (index = 0; index < USPLUS100G_PCS_LANE_COUNT; index++) {
        _ret = _usplus100g_read_48bit_value(p_usplus100g, offset, &value);
        if ( _ret ) {
            PDEBUG("_usplus100g_read_48bit_value error\n");
            USPLUS100G_ERROR_RETURN(_ret);
        }
        framing_err_count[index]  = value;
        offset += USPLUS100G_NUM_DWORD_BYTES;
    }
    return CCL_OK;
}

/**
 * @brief Read the bad code count.
 *
 * @param[out] pvalue is a pointer to hold 64 bit Bad code value
 *
 * @return CCL_OK on success, error code on failure
 */ 
static ccl_err_t _usplus100g_get_rx_bad_64b66b_code_count(usplus100g_ctx_t *p_usplus100g, 
                                                          b_u64 *pvalue)
{
    if ( !p_usplus100g || !pvalue ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    _ret = _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_BAD_CODE, pvalue);
    if ( _ret ) {
        PDEBUG("_usplus100g_read_48bit_value error\n");
        USPLUS100G_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

/**
 * @brief Read the frame error count.
 *
 * @param[out] pvalue is a pointer to hold 64 bit Tx frame error 
 *       count
 *
 * @return CCL_OK on success, error code on failure
 */
static ccl_err_t _usplus100g_get_tx_frame_error_count(usplus100g_ctx_t *p_usplus100g, 
                                                      b_u64 *pvalue)
{
    if ( !p_usplus100g || !pvalue ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    _ret = _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_TX_FRAME_ERROR, pvalue);
    if ( _ret ) {
        PDEBUG("_usplus100g_read_48bit_value error\n");
        USPLUS100G_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}   

/**
 * @brief Read the counters of different packet length for tx.
 *
 *  @param[out] packet_stats is a pointer to hold different
 *        packet counts.
 *
 * @return CCL_OK on success, error code on failure 
 */
static ccl_err_t _usplus100g_get_tx_packet_statistics(usplus100g_ctx_t *p_usplus100g, 
                                                      usplus100g_packet_count_statistics_t *packet_stats)
{
    if ( !p_usplus100g || !packet_stats ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    _ret = _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_TX_TOTAL_PACKETS, 
                                        &packet_stats->total_packets);
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_TX_TOTAL_GOOD_PACKETS,
                                         &packet_stats->total_good_packets);
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_TX_TOTAL_BYTES,
                                         &packet_stats->total_bytes);
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_TX_TOTAL_GOOD_BYTES,
                                         &packet_stats->total_good_bytes);
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_TX_PACKET_64_BYTES,
                                         &packet_stats->packets_0_to_64_bytes);
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_TX_PACKET_65_127_BYTES,
                                         &packet_stats->packets_65_to_127_bytes);
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_TX_PACKET_128_255_BYTES,
                                         &packet_stats->packets_128_to_255_bytes);
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_TX_PACKET_256_511_BYTES,
                                         &packet_stats->packets_256_to_511_bytes);
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_TX_PACKET_512_1023_BYTES,
                                         &packet_stats->packets_512_to_1023_bytes);
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_TX_PACKET_1024_1518_BYTES,
                                         &packet_stats->packets_1024_to_1518_bytes);
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_TX_PACKET_1519_1522_BYTES,
                                         &packet_stats->packets_1519_to_1522_bytes);
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_TX_PACKET_1523_1548_BYTES,
                                         &packet_stats->packets_1523_to_1548_bytes);
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_TX_PACKET_1549_2047_BYTES,
                                         &packet_stats->packets_1549_to_2047_bytes);
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_TX_PACKET_2048_4095_BYTES,
                                         &packet_stats->packets_2048_to_4095_bytes);
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_TX_PACKET_4096_8191_BYTES,
                                         &packet_stats->packets_4096_to_8191_bytes);
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_TX_PACKET_8192_9215_BYTES,
                                         &packet_stats->packets_8192_to_9215_bytes);
    _ret |=  _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_TX_PACKET_LARGE,
                                          &packet_stats->packets_large);
    _ret |=  _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_TX_PACKET_SMALL,
                                          &packet_stats->packets_small);
    if ( _ret ) {
        PDEBUG("_usplus100g_read_48bit_value error\n");
        USPLUS100G_ERROR_RETURN(_ret);
    }

    return CCL_OK;

}

/**
 * @brief Read the counters of different packet length for rx.
 *
 * @param[out] packet_stats is a pointer to hold different 
 *       packet counts.
 *
 * @return CCL_OK on success, error code on failure 
 */                   
static ccl_err_t _usplus100g_get_rx_packet_statistics(usplus100g_ctx_t *p_usplus100g, 
                                                      usplus100g_packet_count_statistics_t *packet_stats)
{
    if ( !p_usplus100g || !packet_stats ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    _ret = _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_TOTAL_PACKETS, 
                                        &packet_stats->total_packets);
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_TOTAL_GOOD_PACKETS,
                                         &packet_stats->total_good_packets);
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_TOTAL_BYTES,
                                         &packet_stats->total_bytes);
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_TOTAL_GOOD_BYTES,
                                         &packet_stats->total_good_bytes);
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_PACKET_64_BYTES,
                                         &packet_stats->packets_0_to_64_bytes);
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_PACKET_65_127_BYTES,
                                         &packet_stats->packets_65_to_127_bytes);
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_PACKET_128_255_BYTES,
                                         &packet_stats->packets_128_to_255_bytes);
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_PACKET_256_511_BYTES,
                                         &packet_stats->packets_256_to_511_bytes);
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_PACKET_512_1023_BYTES,
                                         &packet_stats->packets_512_to_1023_bytes);
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_PACKET_1024_1518_BYTES,
                                         &packet_stats->packets_1024_to_1518_bytes);
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_PACKET_1519_1522_BYTES,
                                         &packet_stats->packets_1519_to_1522_bytes);
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_PACKET_1523_1548_BYTES,
                                         &packet_stats->packets_1523_to_1548_bytes);
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_PACKET_1549_2047_BYTES,
                                         &packet_stats->packets_1549_to_2047_bytes);
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_PACKET_2048_4095_BYTES,
                                         &packet_stats->packets_2048_to_4095_bytes);
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_PACKET_4096_8191_BYTES,
                                         &packet_stats->packets_4096_to_8191_bytes);
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_PACKET_8192_9215_BYTES,
                                         &packet_stats->packets_8192_to_9215_bytes);
    _ret |=  _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_PACKET_LARGE,
                                          &packet_stats->packets_large);
    _ret |=  _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_PACKET_SMALL,
                                          &packet_stats->packets_small);
    if ( _ret ) {
        PDEBUG("_usplus100g_read_48bit_value error\n");
        USPLUS100G_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}                            

/**
 * @brief Read the malformed packets count for tx.
 *
 * @param[out] malformed_stats is a pointer to hold malformed
 *       packet counts.
 *
 * @return CCL_OK on success, error code on failure 
 */
static ccl_err_t _usplus100g_get_tx_malformed_statistics(usplus100g_ctx_t *p_usplus100g, 
                                                         usplus100g_malformed_statistics_t *malformed_stats)
{
    if ( !p_usplus100g || !malformed_stats ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    _ret = _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_TX_BAD_FCS, 
                                        &malformed_stats->bad_fcs_count);
    if ( _ret ) {
        PDEBUG("_usplus100g_read_48bit_value error\n");
        USPLUS100G_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

/**
 * @brief Read the malformed packets count for Rx.
 *
 * @param[out] malformed_stats is a pointer to hold malformed
 *       packet counts.
 *
 * @return CCL_OK on success, error code on failure 
 */
static ccl_err_t _usplus100g_get_rx_malformed_statistics(usplus100g_ctx_t *p_usplus100g, 
                                                         usplus100g_malformed_statistics_t *malformed_stats)
{
    if ( !p_usplus100g || !malformed_stats ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    _ret = _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_UNDERSIZE,
                                        &malformed_stats->under_size_count);
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_FRAGMENT,
                                         &malformed_stats->fragment_count);
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_OVERSIZE,
                                         &malformed_stats->oversize_count);
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_TOOLONG,
                                         &malformed_stats->toolong_count);
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_JABBER,
                                         &malformed_stats->jabber_count);
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_BAD_FCS,
                                         &malformed_stats->bad_fcs_count);
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_PACKET_BAD_FCS,
                                         &malformed_stats->packet_bad_fcs_count);
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_STOMPED_FCS,
                                         &malformed_stats->stomped_fcs_count);
    if ( _ret ) {
        PDEBUG("_usplus100g_read_48bit_value error\n");
        USPLUS100G_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

/**
 * @brief Read the counters of different packet types for tx.
 *
 * @param[out] packet_type_stats is a pointer to hold different
 *       packet type counts.
 *
 * @return CCL_OK on success, error code on failure 
 */ 
static ccl_err_t _usplus100g_get_tx_packet_type_statistics(usplus100g_ctx_t *p_usplus100g, 
                                                           usplus100g_packet_type_statistics_t *packet_type_stats)
{
    if ( !p_usplus100g || !packet_type_stats ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    _ret = _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_TX_UNICAST,
                                        &packet_type_stats->packets_unicast);
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_TX_MULTICAST,
                                         &packet_type_stats->packets_multicast);
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_TX_BROADCAST,
                                         &packet_type_stats->packets_broadcast);
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_TX_VLAN,
                                         &packet_type_stats->packets_vlan);
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_TX_PAUSE,
                                         &packet_type_stats->packets_pause);
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_TX_USER_PAUSE,
                                         &packet_type_stats->packets_priority_pause);
    if ( _ret ) {
        PDEBUG("_usplus100g_read_48bit_value error\n");
        USPLUS100G_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

/**
 * This function reads the counters of different packet types for Rx.
 *
 * @param[out] packet_type_stats is a pointer to hold different
 *       packet type counts.
 *
 * @return CCL_OK on success, error code on failure 
 */ 
static ccl_err_t _usplus100g_get_rx_packet_type_statistics(usplus100g_ctx_t *p_usplus100g, 
                                                           usplus100g_packet_type_statistics_t *packet_type_stats)
{
    if ( !p_usplus100g || !packet_type_stats ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    _ret = _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_UNICAST,
                                        &packet_type_stats->packets_unicast);
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_MULTICAST,
                                         &packet_type_stats->packets_multicast);
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_BROADCAST,
                                         &packet_type_stats->packets_broadcast);
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_VLAN,
                                         &packet_type_stats->packets_vlan);
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_PAUSE,
                                         &packet_type_stats->packets_pause);
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_USER_PAUSE,
                                         &packet_type_stats->packets_priority_pause);
    if ( _ret ) {
        PDEBUG("_usplus100g_read_48bit_value error\n");
        USPLUS100G_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

/**
 * @brief Read the range error count.
 *
 * @param[out] pcount is a pointer to hold 64 bit Rx range error 
 *       count
 *
 * @return CCL_OK on success, error code on failure
 */                             
static ccl_err_t _usplus100g_get_rx_range_error_count(usplus100g_ctx_t *p_usplus100g, 
                                                      b_u64 *pcount)
{  
    if ( !p_usplus100g || !pcount ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    _ret = _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_INRANGEERR, pcount);
    if ( _ret ) {
        PDEBUG("_usplus100g_read_48bit_value error\n");
        USPLUS100G_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

/**
 * @brief Read the Rx truncated error count.
 *
 * @param[out] pcount is a pointer to hold 64 bit truncated 
 *       error count
 *
 * @return CCL_OK on success, error code on failure
 *
 * @note Packet Truncation Indicator
 *    A value of 1 indicates that
 *    the current packet in flight is truncated due to its length.
 */ 
static ccl_err_t _usplus100g_get_rx_truncated_count(usplus100g_ctx_t *p_usplus100g, b_u64 *pcount)
{
    if ( !p_usplus100g || !pcount ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    _ret = _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_TRUNCATED, pcount);
    if ( _ret ) {
        PDEBUG("_usplus100g_read_48bit_value error\n");
        USPLUS100G_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

#ifdef MAC_COUNTERS_ENABLED
static ccl_err_t _usplus100g_read_counters(usplus100g_ctx_t *p_usplus100g)
{
    b_u32 i;

    if ( !p_usplus100g ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    _ret = _usplus100g_latch_statistics(p_usplus100g);
    if ( _ret ) {
        PDEBUG("_usplus100g_latch_statistics error\n");
        USPLUS100G_ERROR_RETURN(_ret);
    }

    for (i = 0; i < p_usplus100g->counters_num; i++) {
        USPLUS100G_REG_READ(p_usplus100g, p_usplus100g->counters[i].offset, 
                       sizeof(b_u32), 0, 
                       (b_u8 *)&p_usplus100g->counters[i].value, 
                       sizeof(b_u32));
    }

    return CCL_OK;
}

static ccl_err_t _usplus100g_read_single_counter(void *priv, b_u32 idx, b_u64 *pvalue)
{
    usplus100g_ctx_t *p_usplus100g;
    port_stats_t     *port_stats;
    b_u32            address_l;

    if ( !priv || !pvalue) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_usplus100g = (usplus100g_ctx_t *)priv;

    address_l = _usplus100g_mib_cntr_map[idx].offset;

    _ret = _usplus100g_latch_statistics(p_usplus100g);
    if ( _ret ) {
        PDEBUG("_usplus100g_latch_statistics error\n");
        USPLUS100G_ERROR_RETURN(_ret);
    }

    if (address_l != USPLUS100G_STAT_INVALID_OFFSET) {
        /* read counter */
        _ret = _usplus100g_read_48bit_value(p_usplus100g, address_l, pvalue);
        if ( _ret ) {
            PDEBUG("_usplus100g_read_48bit_value error, address: %u\n",
                   address_l);
            USPLUS100G_ERROR_RETURN(_ret);
        }
    }

    return CCL_OK;
}

static ccl_err_t _usplus100g_read_stat_counters(void *priv, b_u8 *buf)
{
    usplus100g_ctx_t *p_usplus100g;
    port_stats_t     *port_stats;
    b_u32            counter_l;
    b_u32            counter_h;

    if ( !priv || !buf) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_usplus100g = (usplus100g_ctx_t *)priv;
    port_stats = (port_stats_t *)buf;

    _ret = _usplus100g_latch_statistics(p_usplus100g);
    if ( _ret ) {
        PDEBUG("_usplus100g_latch_statistics error\n");
        USPLUS100G_ERROR_RETURN(_ret);
    }

    /* read counters */

    /* tx bytes */
    _ret = _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_TX_TOTAL_BYTES,
                                        &port_stats->tx_bytes.value);
    /* tx packets */
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_TX_TOTAL_PACKETS,
                                         &port_stats->tx_packets.value);
    /* tx packets 64 bytes */
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_TX_PACKET_64_BYTES,
                                         &port_stats->tx_packets_64.value);
    /* tx packets 65-127 bytes */
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_TX_PACKET_65_127_BYTES,
                                         &port_stats->tx_packets_65_127.value);
    /* tx packets 128-255 bytes */
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_TX_PACKET_128_255_BYTES,
                                         &port_stats->tx_packets_128_255.value);
    /* tx packets 256-511 bytes */
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_TX_PACKET_256_511_BYTES,
                                         &port_stats->tx_packets_256_511.value);
    /* tx packets 512-1023 bytes */
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_TX_PACKET_512_1023_BYTES,
                                         &port_stats->tx_packets_512_1023.value);
    /* tx packets 1024-max bytes */
    //TODO - ADD MISSING COUNTERS (> 1518 BYTES)
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_TX_PACKET_1024_1518_BYTES,
                                         &port_stats->tx_packets_1024_max.value);
    /* tx packets broadcast */
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_TX_BROADCAST,
                                         &port_stats->tx_bcast.value);
    /* tx packets multicast */
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_TX_MULTICAST,
                                         &port_stats->tx_mcast.value);
    /* tx packets oversize */
    //TODO - check oversize definition vs this counter
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_TX_PACKET_LARGE,
                                         &port_stats->tx_packets_oversize.value);
    /* tx packets underrun error */
    //TODO - check underrun definition vs this counter
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_TX_PACKET_SMALL,
                                         &port_stats->tx_packets_underrun_err.value);
    /* rx bytes */
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_TOTAL_BYTES,
                                         &port_stats->rx_bytes.value);
    /* rx packets */
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_TOTAL_PACKETS,
                                         &port_stats->rx_packets.value);
    /* rx packets 64 bytes */
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_PACKET_64_BYTES,
                                         &port_stats->rx_packets_64.value);
    /* rx packets 65-127 bytes */
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_PACKET_65_127_BYTES,
                                         &port_stats->rx_packets_65_127.value);
    /* rx packets 128-255 bytes */
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_PACKET_128_255_BYTES,
                                         &port_stats->rx_packets_128_255.value);
    /* rx packets 256-511 bytes */
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_PACKET_256_511_BYTES,
                                         &port_stats->rx_packets_256_511.value);
    /* rx packets 512-1023 bytes */
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_PACKET_512_1023_BYTES,
                                         &port_stats->rx_packets_512_1023.value);
    /* rx packets 1024-max bytes */
    //TODO - ADD MISSING COUNTERS (> 1518 BYTES)
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_PACKET_1024_1518_BYTES,
                                         &port_stats->rx_packets_1024_max.value);
    /* rx packets broadcast */
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_BROADCAST,
                                         &port_stats->rx_bcast.value);
    /* rx packets multicast */
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_MULTICAST,
                                         &port_stats->rx_mcast.value);
    /* rx packets oversize*/
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_OVERSIZE,
                                         &port_stats->rx_packets_oversize.value);
    /* rx packets undersize*/
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_UNDERSIZE,
                                         &port_stats->rx_packets_undersize.value);
    /* rx packets fragmented */
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_FRAGMENT,
                                         &port_stats->rx_packets_frag.value);
    /* rx packets FCS error */
    _ret |= _usplus100g_read_48bit_value(p_usplus100g, USPLUS100G_STAT_RX_BAD_FCS,
                                         &port_stats->rx_packets_fcs_err.value);
    if ( _ret ) {
        PDEBUG("_usplus100g_read_48bit_value error\n");
        USPLUS100G_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

static ccl_err_t _usplus100g_reset_counters(void *priv)
{
    usplus100g_ctx_t *p_usplus100g;
 
    if ( !priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    
    return CCL_OK;
}
#else
static ccl_err_t _usplus100g_read_counters(usplus100g_ctx_t *p_usplus100g)
{
    b_u32 regval;
    b_u32 i;

    if ( !p_usplus100g ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    
    /* Freeze counters */
    b_u32 offset = p_usplus100g->brdspec.counter_ctl.ctl_offset;
    _ret = devspibus_read_buff(p_usplus100g->brdspec.devname, 
                               offset,
                               0, 
                               (b_u8 *)&regval, 
                               sizeof(b_u32));
    PDEBUG("offset=0x%x, regval=0x%x\n",
           offset, regval);
    regval |= p_usplus100g->brdspec.counter_ctl.ctl.freeze_counters_mask;
    PDEBUG("offset=0x%x, regval=0x%x\n",
           offset, regval);
    _ret |= devspibus_write_buff(p_usplus100g->brdspec.devname, 
                                offset,
                                0, 
                                (b_u8 *)&regval, 
                                sizeof(b_u32));
    for (i = 0; i < p_usplus100g->counters_num; i++) {
        offset = p_usplus100g->brdspec.counter_ctl.counter[i].offset;
        PDEBUG("counter[%d] - offset=0x%x\n",
               i, offset);
        _ret |= devspibus_read_buff(p_usplus100g->brdspec.devname, 
                                   offset,
                                   0, 
                                   (b_u8 *)&p_usplus100g->counters[i].value, 
                                   sizeof(b_u32));
        PDEBUG("counter[%d] - offset=0x%x, value=%u\n",
               i, offset, p_usplus100g->counters[i].value);
    }
    /* Unfreeze counters */
    offset = p_usplus100g->brdspec.counter_ctl.ctl_offset;
    _ret |= devspibus_read_buff(p_usplus100g->brdspec.devname, 
                               offset,
                               0, 
                               (b_u8 *)&regval, 
                               sizeof(b_u32));
    PDEBUG("offset=0x%x, regval=0x%x\n",
           offset, regval);
    regval &= ~p_usplus100g->brdspec.counter_ctl.ctl.freeze_counters_mask;
    PDEBUG("offset=0x%x, regval=0x%x\n",
           offset, regval);
    _ret |= devspibus_write_buff(p_usplus100g->brdspec.devname, 
                                offset,
                                0, 
                                (b_u8 *)&regval, 
                                sizeof(b_u32));
    if ( _ret ) {
        PDEBUG("Error! Failed to write over spi bus\n");
        USPLUS100G_ERROR_RETURN(_ret);
    }    

    return CCL_OK;
}

static ccl_err_t _usplus100g_read_single_counter(void *priv, b_u32 idx, b_u64 *pvalue)
{
    usplus100g_ctx_t *p_usplus100g;
    b_u32            ifmib;

    if ( !priv || !pvalue) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_usplus100g = (usplus100g_ctx_t *)priv;

    ifmib = _usplus100g_mib_cntr_map[idx].ifmib;

    _ret = _usplus100g_read_counters(p_usplus100g);
    if ( _ret ) {
        PDEBUG("_usplus100g_latch_statistics error\n");
        USPLUS100G_ERROR_RETURN(_ret);
    }

    if (ifmib == DEVMAN_IF_MIB_FRASENT) 
        *pvalue = ((b_u64)p_usplus100g->counters[1].value << USPLUS100G_NUM_WORD_BITS) |
                  p_usplus100g->counters[0].value;
    else if (ifmib == DEVMAN_IF_MIB_FRAREC)
        *pvalue = ((b_u64)p_usplus100g->counters[3].value << USPLUS100G_NUM_WORD_BITS) |
                  p_usplus100g->counters[2].value;
    else if (ifmib == DEVMAN_IF_MIB_CRCERR)
        *pvalue = ((b_u64)p_usplus100g->counters[5].value << USPLUS100G_NUM_WORD_BITS) |
                  p_usplus100g->counters[4].value;
    else {
        PDEBUG("invalid counter ifmib: %d, idx: %d\n", ifmib, idx);
        USPLUS100G_ERROR_RETURN(_ret);
    }

    PDEBUG("%d %d %d %d %d %d %llu %d\n",
           p_usplus100g->counters[0].value,
           p_usplus100g->counters[1].value,
           p_usplus100g->counters[2].value,
           p_usplus100g->counters[3].value,
           p_usplus100g->counters[4].value,
           p_usplus100g->counters[5].value,
           *pvalue, ifmib);

    return CCL_OK;
}

static ccl_err_t _usplus100g_read_stat_counters(void *priv, b_u8 *buf)
{
    usplus100g_ctx_t *p_usplus100g;
    port_stats_t  *port_stats;

    if ( !priv || !buf) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_usplus100g = (usplus100g_ctx_t *)priv;
    port_stats = (port_stats_t *)buf;

    _ret = _usplus100g_read_counters(p_usplus100g);
    if ( _ret ) {
        PDEBUG("_usplus100g_latch_statistics error\n");
        USPLUS100G_ERROR_RETURN(_ret);
    }

    port_stats->tx_packets.value = 
                   ((b_u64)p_usplus100g->counters[1].value << USPLUS100G_NUM_WORD_BITS) |
                    p_usplus100g->counters[0].value;
    port_stats->rx_packets.value = 
                   ((b_u64)p_usplus100g->counters[3].value << USPLUS100G_NUM_WORD_BITS) |
                    p_usplus100g->counters[2].value;                   
    port_stats->rx_packets_fcs_err.value = 
                   ((b_u64)p_usplus100g->counters[5].value << USPLUS100G_NUM_WORD_BITS) |
                    p_usplus100g->counters[4].value;
    PDEBUG("tx_packets: lsb=%u, msb=%u aggregated=%llu\n",
           p_usplus100g->counters[0].value,
           p_usplus100g->counters[1].value,
           port_stats->tx_packets.value);

    return CCL_OK;
}

static ccl_err_t _usplus100g_reset_counters(void *priv)
{
    usplus100g_ctx_t *p_usplus100g;
    b_u32            regval;
    b_u32            i;

    if ( !priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    
    p_usplus100g = (usplus100g_ctx_t *)priv;

    /* Reset counters */
    b_u32 offset = p_usplus100g->brdspec.counter_ctl.ctl_offset;
    _ret = devspibus_read_buff(p_usplus100g->brdspec.devname, 
                               offset,
                               0, 
                               (b_u8 *)&regval, 
                               sizeof(b_u32));
    PDEBUG("offset=0x%x, regval=0x%x\n",
           offset, regval);
    regval |= p_usplus100g->brdspec.counter_ctl.ctl.tx_pkt_rst_mask |
              p_usplus100g->brdspec.counter_ctl.ctl.rx_pkt_rst_mask |
              p_usplus100g->brdspec.counter_ctl.ctl.rx_bad_pkt_rst_mask;
    PDEBUG("offset=0x%x, regval=0x%x\n",
           offset, regval);
    _ret |= devspibus_write_buff(p_usplus100g->brdspec.devname, 
                                offset,
                                0, 
                                (b_u8 *)&regval, 
                                sizeof(b_u32));
    PDEBUG("offset=0x%x, regval=0x%x\n",
           offset, regval);
    regval &= ~(p_usplus100g->brdspec.counter_ctl.ctl.tx_pkt_rst_mask |
              p_usplus100g->brdspec.counter_ctl.ctl.rx_pkt_rst_mask |
              p_usplus100g->brdspec.counter_ctl.ctl.rx_bad_pkt_rst_mask);
    PDEBUG("offset=0x%x, regval=0x%x\n",
           offset, regval);
    _ret |= devspibus_write_buff(p_usplus100g->brdspec.devname, 
                                offset,
                                0, 
                                (b_u8 *)&regval, 
                                sizeof(b_u32));
    if ( _ret ) {
        PDEBUG("Error! Failed to write over spi bus\n");
        USPLUS100G_ERROR_RETURN(_ret);
    }    

    return CCL_OK;
}
#endif

static ccl_err_t _usplus100g_configure_test(void *p_priv)
{
    usplus100g_ctx_t *p_usplus100g;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_usplus100g = (usplus100g_ctx_t *)p_priv;

    return CCL_OK;
}

static ccl_err_t _usplus100g_run_test(void *p_priv, 
                                      __attribute__((unused)) b_u8 *buf, 
                                      __attribute__((unused)) b_u32 size)
{
    usplus100g_ctx_t *p_usplus100g;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_usplus100g = (usplus100g_ctx_t *)p_priv;

    return CCL_OK;
}

ccl_err_t usplus100g_attr_read(void *p_priv, b_u32 attrid, 
                               b_u32 offset, b_u32 offset_sz, 
                               b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || offset > USPLUS100G_MAX_OFFSET || !p_value ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch (attrid) {
    case eUSPLUS100G_GT_RESET_REG:
    case eUSPLUS100G_RESET_REG:
    case eUSPLUS100G_SWITCH_CORE_MODE_REG:
    case eUSPLUS100G_CONFIGURATION_TX_REG1:
    case eUSPLUS100G_CONFIGURATION_RX_REG1:
    case eUSPLUS100G_CORE_MODE_REG:
    case eUSPLUS100G_CORE_VERSION_REG:
    case eUSPLUS100G_CONFIGURATION_TX_BIP_OVERRIDE:
    case eUSPLUS100G_CONFIGURATION_TX_FLOW_CONTROL_CONTROL_REG1:
    case eUSPLUS100G_CONFIGURATION_TX_FLOW_CONTROL_REFRESH_REG1:
    case eUSPLUS100G_CONFIGURATION_TX_FLOW_CONTROL_QUANTA_REG1:
    case eUSPLUS100G_CONFIGURATION_TX_OTN_PKT_LEN_REG:
    case eUSPLUS100G_CONFIGURATION_TX_OTN_CTL_REG:
    case eUSPLUS100G_CONFIGURATION_RX_FLOW_CONTROL_CONTROL_REG1:
    case eUSPLUS100G_CONFIGURATION_RX_FLOW_CONTROL_CONTROL_REG2:
    case eUSPLUS100G_GT_LOOPBACK_REG:
    case eUSPLUS100G_CONFIGURATION_AN_CONTROL_REG1:
    case eUSPLUS100G_CONFIGURATION_AN_CONTROL_REG2:
    case eUSPLUS100G_CONFIGURATION_AN_ABILITY:
    case eUSPLUS100G_CONFIGURATION_LT_CONTROL_REG1:
    case eUSPLUS100G_CONFIGURATION_LT_TRAINED_REG:
    case eUSPLUS100G_CONFIGURATION_LT_PRESET_REG:
    case eUSPLUS100G_CONFIGURATION_LT_INIT_REG:
    case eUSPLUS100G_CONFIGURATION_LT_SEED_REG0:
    case eUSPLUS100G_CONFIGURATION_LT_COEFFICIENT_REG0:
    case eUSPLUS100G_RSFEC_CONFIG_INDICATION_CORRECTION:
    case eUSPLUS100G_RSFEC_CONFIG_ENABLE:
    case eUSPLUS100G_STAT_TX_STATUS_REG:
    case eUSPLUS100G_STAT_RX_STATUS_REG:
    case eUSPLUS100G_STAT_STATUS_REG1:
    case eUSPLUS100G_STAT_RX_BLOCK_LOCK_REG:
    case eUSPLUS100G_STAT_RX_LANE_SYNC_REG:
    case eUSPLUS100G_STAT_RX_LANE_SYNC_ERR_REG:
    case eUSPLUS100G_STAT_RX_AM_ERR_REG:
    case eUSPLUS100G_STAT_RX_AM_LEN_ERR_REG:
    case eUSPLUS100G_STAT_RX_AM_REPEAT_ERR_REG:
    case eUSPLUS100G_STAT_RX_PCSL_DEMUXED_REG:
    case eUSPLUS100G_STAT_RX_PCS_LANE_NUM_REG1:
    case eUSPLUS100G_STAT_RX_BIP_OVERRIDE_REG:
    case eUSPLUS100G_STAT_TX_OTN_STATUS_REG:
    case eUSPLUS100G_STAT_AN_STATUS_REG:
    case eUSPLUS100G_STAT_AN_ABILITY_REG:
    case eUSPLUS100G_STAT_AN_LINK_CTL_REG:
    case eUSPLUS100G_STAT_LT_STATUS_REG1:
    case eUSPLUS100G_STAT_LT_STATUS_REG2:
    case eUSPLUS100G_STAT_LT_STATUS_REG3:
    case eUSPLUS100G_STAT_LT_STATUS_REG4:
    case eUSPLUS100G_STAT_LT_COEFFICIENT0_REG:
        USPLUS100G_REG_READ(p_priv, offset, offset_sz, idx, p_value, size);
        break;
    case eUSPLUS100G_STAT_CYCLE_COUNT:
    case eUSPLUS100G_STAT_RX_BIP_ERR_0:
    case eUSPLUS100G_STAT_RX_FRAMING_ERR0:
    case eUSPLUS100G_STAT_RX_BAD_CODE:
    case eUSPLUS100G_STAT_TX_FRAME_ERROR:
    case eUSPLUS100G_STAT_TX_TOTAL_PACKETS:
    case eUSPLUS100G_STAT_TX_TOTAL_GOOD_PACKETS:
    case eUSPLUS100G_STAT_TX_TOTAL_BYTES:
    case eUSPLUS100G_STAT_TX_TOTAL_GOOD_BYTES:
    case eUSPLUS100G_STAT_TX_PACKET_64_BYTES:
    case eUSPLUS100G_STAT_TX_PACKET_65_127_BYTES:
    case eUSPLUS100G_STAT_TX_PACKET_128_255_BYTES:
    case eUSPLUS100G_STAT_TX_PACKET_256_511_BYTES:
    case eUSPLUS100G_STAT_TX_PACKET_512_1023_BYTES:
    case eUSPLUS100G_STAT_TX_PACKET_1024_1518_BYTES:
    case eUSPLUS100G_STAT_TX_PACKET_1519_1522_BYTES:
    case eUSPLUS100G_STAT_TX_PACKET_1523_1548_BYTES:
    case eUSPLUS100G_STAT_TX_PACKET_1549_2047_BYTES:
    case eUSPLUS100G_STAT_TX_PACKET_2048_4095_BYTES:
    case eUSPLUS100G_STAT_TX_PACKET_4096_8191_BYTES:
    case eUSPLUS100G_STAT_TX_PACKET_8192_9215_BYTES:
    case eUSPLUS100G_STAT_TX_PACKET_LARGE:
    case eUSPLUS100G_STAT_TX_PACKET_SMALL:
    case eUSPLUS100G_STAT_TX_BAD_FCS:
    case eUSPLUS100G_STAT_TX_UNICAST:
    case eUSPLUS100G_STAT_TX_MULTICAST:
    case eUSPLUS100G_STAT_TX_BROADCAST:
    case eUSPLUS100G_STAT_TX_VLAN:
    case eUSPLUS100G_STAT_TX_PAUSE:
    case eUSPLUS100G_STAT_TX_USER_PAUSE:
    case eUSPLUS100G_STAT_RX_TOTAL_PACKETS:
    case eUSPLUS100G_STAT_RX_TOTAL_GOOD_PACKETS:
    case eUSPLUS100G_STAT_RX_TOTAL_BYTES:
    case eUSPLUS100G_STAT_RX_TOTAL_GOOD_BYTES:
    case eUSPLUS100G_STAT_RX_PACKET_64_BYTES:
    case eUSPLUS100G_STAT_RX_PACKET_65_127_BYTES:
    case eUSPLUS100G_STAT_RX_PACKET_128_255_BYTES:
    case eUSPLUS100G_STAT_RX_PACKET_256_511_BYTES:
    case eUSPLUS100G_STAT_RX_PACKET_512_1023_BYTES:
    case eUSPLUS100G_STAT_RX_PACKET_1024_1518_BYTES:
    case eUSPLUS100G_STAT_RX_PACKET_1519_1522_BYTES:
    case eUSPLUS100G_STAT_RX_PACKET_1523_1548_BYTES:
    case eUSPLUS100G_STAT_RX_PACKET_1549_2047_BYTES:
    case eUSPLUS100G_STAT_RX_PACKET_2048_4095_BYTES:
    case eUSPLUS100G_STAT_RX_PACKET_4096_8191_BYTES:
    case eUSPLUS100G_STAT_RX_PACKET_8192_9215_BYTES:
    case eUSPLUS100G_STAT_RX_PACKET_LARGE:
    case eUSPLUS100G_STAT_RX_PACKET_SMALL:
    case eUSPLUS100G_STAT_RX_UNDERSIZE:
    case eUSPLUS100G_STAT_RX_FRAGMENT:
    case eUSPLUS100G_STAT_RX_OVERSIZE:
    case eUSPLUS100G_STAT_RX_TOOLONG:
    case eUSPLUS100G_STAT_RX_JABBER:
    case eUSPLUS100G_STAT_RX_BAD_FCS:
    case eUSPLUS100G_STAT_RX_PACKET_BAD_FCS:
    case eUSPLUS100G_STAT_RX_STOMPED_FCS:
    case eUSPLUS100G_STAT_RX_UNICAST:
    case eUSPLUS100G_STAT_RX_MULTICAST:
    case eUSPLUS100G_STAT_RX_BROADCAST:
    case eUSPLUS100G_STAT_RX_VLAN:
    case eUSPLUS100G_STAT_RX_PAUSE:
    case eUSPLUS100G_STAT_RX_USER_PAUSE:
    case eUSPLUS100G_STAT_RX_INRANGEERR:
    case eUSPLUS100G_STAT_RX_TRUNCATED:
    case eUSPLUS100G_STAT_OTN_TX_JABBER:
    case eUSPLUS100G_STAT_OTN_TX_OVERSIZE:
    case eUSPLUS100G_STAT_OTN_TX_UNDERSIZE:
    case eUSPLUS100G_STAT_OTN_TX_TOOLONG:
    case eUSPLUS100G_STAT_OTN_TX_FRAGMENT:
    case eUSPLUS100G_STAT_OTN_TX_PACKET_BAD_FCS:
    case eUSPLUS100G_STAT_OTN_TX_STOMPED_FCS:
    case eUSPLUS100G_STAT_OTN_TX_BAD_CODE:
        _ret = _usplus100g_read_single_counter(p_priv, idx, (b_u64*)p_value);
        break;
    case eUSPLUS100G_IF_PROPERTY:
        _ret = _usplus100g_if_property_get(p_priv, idx, p_value, size);
        break;
    case eUSPLUS100G_STAT_COUNTERS:
        _ret = _usplus100g_read_stat_counters(p_priv, p_value);
        break;
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK) {
        PDEBUG("Failure in attribute id: %d\n", attrid);
    }

    return _ret;               
}

ccl_err_t usplus100g_attr_write(void *p_priv, b_u32 attrid, 
                                b_u32 offset, b_u32 offset_sz, 
                                b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || offset > USPLUS100G_MAX_OFFSET || !p_value ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch (attrid) {
    case eUSPLUS100G_GT_RESET_REG:
    case eUSPLUS100G_RESET_REG:
    case eUSPLUS100G_SWITCH_CORE_MODE_REG:
    case eUSPLUS100G_CONFIGURATION_TX_REG1:
    case eUSPLUS100G_CONFIGURATION_RX_REG1:
    case eUSPLUS100G_CONFIGURATION_TX_BIP_OVERRIDE:
    case eUSPLUS100G_CONFIGURATION_TX_FLOW_CONTROL_CONTROL_REG1:
    case eUSPLUS100G_CONFIGURATION_TX_FLOW_CONTROL_REFRESH_REG1:
    case eUSPLUS100G_CONFIGURATION_TX_FLOW_CONTROL_QUANTA_REG1:
    case eUSPLUS100G_CONFIGURATION_TX_OTN_PKT_LEN_REG:
    case eUSPLUS100G_CONFIGURATION_TX_OTN_CTL_REG:
    case eUSPLUS100G_CONFIGURATION_RX_FLOW_CONTROL_CONTROL_REG1:
    case eUSPLUS100G_CONFIGURATION_RX_FLOW_CONTROL_CONTROL_REG2:
    case eUSPLUS100G_GT_LOOPBACK_REG:
    case eUSPLUS100G_CONFIGURATION_AN_CONTROL_REG1:
    case eUSPLUS100G_CONFIGURATION_AN_CONTROL_REG2:
    case eUSPLUS100G_CONFIGURATION_AN_ABILITY:
    case eUSPLUS100G_CONFIGURATION_LT_CONTROL_REG1:
    case eUSPLUS100G_CONFIGURATION_LT_TRAINED_REG:
    case eUSPLUS100G_CONFIGURATION_LT_PRESET_REG:
    case eUSPLUS100G_CONFIGURATION_LT_INIT_REG:
    case eUSPLUS100G_CONFIGURATION_LT_SEED_REG0:
    case eUSPLUS100G_CONFIGURATION_LT_COEFFICIENT_REG0:
    case eUSPLUS100G_RSFEC_CONFIG_INDICATION_CORRECTION:
    case eUSPLUS100G_RSFEC_CONFIG_ENABLE:
    case eUSPLUS100G_TICK_REG:
        USPLUS100G_REG_WRITE(p_priv, offset, offset_sz, idx, p_value, size);
        break;
    case eUSPLUS100G_INIT:
        _ret = _usplus100g_init(p_priv);
        break;
    case eUSPLUS100G_CONFIGURE_TEST:
        _ret = _usplus100g_configure_test(p_priv);
        break;
    case eUSPLUS100G_RUN_TEST:
        _ret = _usplus100g_run_test(p_priv, p_value, size);
        break;
    case eUSPLUS100G_COUNTERS_RESET:
        _ret = _usplus100g_reset_counters(p_priv);
        break;
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK) {
        PDEBUG("Failure in attribute id: %d\n", attrid);
    }

    return _ret;               
}

static ccl_err_t _fusplus100g_cmd_show_counters(devo_t devo, 
                                                __attribute__((unused)) device_cmd_parm_t parms[], 
                                                __attribute__((unused)) b_u16 parms_num)
{
    usplus100g_ctx_t *p_usplus100g = devman_device_private(devo);
    char        cname[100] = {0};
    b_u32       offset = 40;
    b_u32       clen;
    b_u32       i;

    /* init the device */
    _ret = _usplus100g_read_counters(p_usplus100g);
    if ( _ret ) {
        PDEBUG("_usplus100g_read_counters\n");
        USPLUS100G_ERROR_RETURN(_ret);
    }

    for (i = 0; i < p_usplus100g->counters_num; i++) {
        snprintf(cname, sizeof(cname), "%s:", p_usplus100g->counters[i].name);
        clen = strnlen(cname, sizeof(cname));
        if (offset <= clen)
            offset = clen + 1;
        snprintf(cname+clen, sizeof(cname), "%*s", 
                 offset-clen, " ");
        PRINTL("%s%u\n", 
               cname,
               p_usplus100g->counters[i].value);
        p_usplus100g->counters[i].value_prev = p_usplus100g->counters[i].value;
    }

    return CCL_OK;
}

static ccl_err_t _fusplus100g_cmd_show_counters_changed(devo_t devo, 
                                                        __attribute__((unused)) device_cmd_parm_t parms[], 
                                                        __attribute__((unused)) b_u16 parms_num)
{
    usplus100g_ctx_t *p_usplus100g = devman_device_private(devo);
    char        cname[100] = {0};
    b_u32       offset = 40;
    b_u32       clen;
    b_u32       i;
    b_u32       value;

    /* init the device */
    _ret = _usplus100g_read_counters(p_usplus100g);
    if ( _ret ) {
        PDEBUG("_usplus100g_read_counters\n");
        USPLUS100G_ERROR_RETURN(_ret);
    }

    for (i = 0; i < p_usplus100g->counters_num; i++) {
        if (p_usplus100g->counters[i].value != p_usplus100g->counters[i].value_prev) {
            value = p_usplus100g->counters[i].value - p_usplus100g->counters[i].value_prev;
            if (p_usplus100g->counters[i].value_prev > p_usplus100g->counters[i].value) {
                value = (p_usplus100g->counters[i].value_prev - 
                         p_usplus100g->counters[i].value + 
                         MAX_UINT32);
            }
            snprintf(cname, sizeof(cname), "%s:", p_usplus100g->counters[i].name);
            clen = strnlen(cname, sizeof(cname));
            if (offset <= clen)
                offset = clen + 1;
            snprintf(cname+clen, sizeof(cname), "%*s", 
                     offset-clen, " ");
            PRINTL("%s%u\n", 
                   cname,
                   value);
            p_usplus100g->counters[i].value_prev = p_usplus100g->counters[i].value;
        }
    }

    return CCL_OK;
}

static ccl_err_t _fusplus100g_cmd_reset_counters(devo_t devo, 
                                                 __attribute__((unused)) device_cmd_parm_t parms[], 
                                                 __attribute__((unused)) b_u16 parms_num)
{
    void *p_priv = devman_device_private(devo);

    /* start the device */
    _ret = _usplus100g_reset_counters(p_priv);
    if ( _ret ) {
        PDEBUG("_usplus100g_reset_counters error\n");
        USPLUS100G_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

/* device commands */
static device_cmd_t _cmds[] = 
{
    { 
        .name = "counters", .f_cmd = _fusplus100g_cmd_show_counters, .parms_num = 0,
        .help = "Show USPLUS100G counters" 
    },
    { 
        .name = "counters_changed", .f_cmd = _fusplus100g_cmd_show_counters_changed, .parms_num = 0,
        .help = "Show USPLUS100G changed counters" 
    },
    { 
        .name = "counters_reset", .f_cmd = _fusplus100g_cmd_reset_counters, .parms_num = 0,
        .help = "Reset USPLUS100G counters" 
    },    
};

/* Device instance hw-specific initializations 
 */
static ccl_err_t _fusplus100g_add(void *devo, void *brdspec) 
{
    usplus100g_ctx_t       *p_usplus100g;
    eth_brdspec_t          *p_brdspec;
    b_u32                  i;

    if ( !devo || !brdspec ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_usplus100g = devman_device_private(devo);
    if ( !p_usplus100g ) {
        PDEBUG("NULL context area\n");
        USPLUS100G_ERROR_RETURN(CCL_FAIL);
    }
    p_brdspec = (eth_brdspec_t *)brdspec;
    /* back reference the device manager object handle */    
    p_usplus100g->devo = devo;    
    /* save the board specific info */
    memcpy(&p_usplus100g->brdspec, p_brdspec, sizeof(eth_brdspec_t));
    p_usplus100g->counters_num = sizeof(_usplus100g_counters)/sizeof(_usplus100g_counters[0]);
    p_usplus100g->counters = calloc(p_usplus100g->counters_num, sizeof(eth_counter_t));
    if (!p_usplus100g->counters) {
        PDEBUG("Memory allocation failed\n");
        USPLUS100G_ERROR_RETURN(CCL_NO_MEM_ERR);
    }
    for (i = 0; i < p_usplus100g->counters_num; i++) 
        memcpy(&p_usplus100g->counters[i], &_usplus100g_counters[i], sizeof(eth_counter_t));

    return CCL_OK;
}

/* Device instance hw-specific deinitializations 
 */
static ccl_err_t _fusplus100g_cut(void *devo)
{
    usplus100g_ctx_t  *p_usplus100g;
    if ( !devo ) {
        PDEBUG("Error! Invalid input parameters\n");
        USPLUS100G_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_usplus100g = devman_device_private(devo);
    if ( !p_usplus100g ) {
        PDEBUG("NULL context area\n");
        USPLUS100G_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

/* device type description structure */
static device_driver_t _usplus100g_driver = {
    .name = "usplus100g",
    .f_add = _fusplus100g_add,
    .f_cut = _fusplus100g_cut,
    .attrs = _attrs,
    .attrs_num = sizeof(_attrs)/sizeof(_attrs[0]),
    .cmds = _cmds,
    .cmds_num = sizeof(_cmds)/sizeof(_cmds[0]),
    .priv_size = sizeof(usplus100g_ctx_t)
}; 

/* Initialize USPLUS100G device type
 */
ccl_err_t devusplus100g_init(void)
{
    /* register device type */
    _ret = devman_driver_register(&_usplus100g_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to register driver\n");
        USPLUS100G_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}

/* Deinitialize USPLUS100G device type
 */
ccl_err_t devusplus100g_fini(void)
{
    /* unregister device type */
    _ret = devman_driver_unregister(&_usplus100g_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to unregister driver\n");
        USPLUS100G_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}

