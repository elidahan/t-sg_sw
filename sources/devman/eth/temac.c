/********************************************************************************/
/**
 * @file temac.c
 * @brief Tri-Mode Ethernet MAC device manager driver
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/

#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <semaphore.h>
#include <linux/limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devman_port.h"
#include "devman_brd_gen.h"
#include "devboard.h"
#include "spibus.h"
#include "temac.h"


//#define TEMAC_DEBUG
/* printing/error-returning macros */
#ifdef TEMAC_DEBUG
    #define PDEBUG(fmt, args...) \
         ccl_syslog_err("TEMAC_DEBUG: %s[%d]: " fmt, __FUNCTION__, __LINE__, ## args)
#else
    #define PDEBUG(fmt, args...)
#endif
#define TEMAC_ERROR_RETURN(err) do { \
   ccl_syslog_err("Error! %s[%d], error=%i\n", __FUNCTION__, __LINE__, (err)); \
   return err; \
} while (0)
#define TEMAC_CHECK_ERROR_RETURN(err) do { \
    if (err != CCL_OK) { \
        ccl_syslog_err("Error! %s[%d], error=%i\n", __FUNCTION__, __LINE__, (err)); \
        return err; \
    } \
} while (0)

#define TEMAC_REG_READ(context, offset, offset_sz, idx, pvalue, size) do { \
    _ret = _temac_reg_read((context), \
                           (offset), \
                           (offset_sz), (idx), \
                           (pvalue), (size)); \
    if ( _ret ) { \
        PDEBUG("_temac_reg_read error\n"); \
        TEMAC_ERROR_RETURN(_ret); \
    } \
} while (0)

#define TEMAC_REG_WRITE(context, offset, offset_sz, idx, pvalue, size) do { \
    _ret = _temac_reg_write((context), \
                            (offset), \
                            (offset_sz), (idx), \
                            (pvalue), (size)); \
    if ( _ret ) { \
        PDEBUG("_temac_reg_write error\n"); \
        TEMAC_ERROR_RETURN(_ret); \
    } \
} while (0)

/* TEMAC registers offsets */

/*
 * Statistics Counter registers are from offset 0x200 to 0x3FF
 * They are defined from offset 0x200 to 0x34C in this device.
 * The offsets from 0x350 to 0x3FF are reserved.
 * The counters are 64 bit.
 * The Least Significant Word (LSW) are stored in one 32 bit register and
 * the Most Significant Word (MSW) are stored in one 32 bit register
 */
/* Start of Statistics Counter registers Definitions */
#define TEMAC_RXBL_OFFSET		    0x00000200 /**< Received Bytes, LSW */
#define TEMAC_RXBU_OFFSET		    0x00000204 /**< Received Bytes, MSW */
#define TEMAC_TXBL_OFFSET		    0x00000208 /**< Transmitted Bytes, LSW */
#define TEMAC_TXBU_OFFSET		    0x0000020C /**< Transmitted Bytes, MSW */
#define TEMAC_RXUNDRL_OFFSET	    0x00000210 /**< Count of undersize (less than
					                             *  64 bytes) frames received,
					                             *  LSW
					                             */
#define TEMAC_RXUNDRU_OFFSET	    0x00000214 /**< Count of undersize (less than
					                             *  64 bytes) frames received,
					                             *  MSW
					                             */
#define TEMAC_RXFRAGL_OFFSET	    0x00000218 /**< Count of undersized (less
					                             *  than 64 bytes) and bad FCS
					                             *  frames received, LSW
					                             */
#define TEMAC_RXFRAGU_OFFSET	    0x0000021C /**< Count of undersized (less
					                             *  than 64 bytes) and bad FCS
					                             *  frames received, MSW
                    					         */ 
#define TEMAC_RX64BL_OFFSET	        0x00000220 /**< Count of 64 bytes frames
					                             *  received, LSW
					                             */
#define TEMAC_RX64BU_OFFSET  	    0x00000224 /**< Count of 64 bytes frames
					                             *  received, MSW
					                             */
#define TEMAC_RX65B127L_OFFSET	    0x00000228 /**< Count of 65-127 bytes
					                             *  Frames received, LSW
					                             */
#define TEMAC_RX65B127U_OFFSET	    0x0000022C /**< Count of 65-127 bytes
					                             *  Frames received, MSW
					                             */
#define TEMAC_RX128B255L_OFFSET	    0x00000230 /**< Count of 128-255 bytes
					                             *  Frames received, LSW
					                             */
#define TEMAC_RX128B255U_OFFSET	    0x00000234 /**< Count of 128-255 bytes
					                             *  frames received, MSW
					                             */
#define TEMAC_RX256B511L_OFFSET	    0x00000238 /**< Count of 256-511 bytes
					                             *  Frames received, LSW
					                             */
#define TEMAC_RX256B511U_OFFSET	    0x0000023C /**< Count of 256-511 bytes
					                             *  frames received, MSW
					                             */
#define TEMAC_RX512B1023L_OFFSET	0x00000240 /**< Count of 512-1023 bytes
					                             *  frames received, LSW
					                             */
#define TEMAC_RX512B1023U_OFFSET	0x00000244 /**< Count of 512-1023 bytes
					                             *  frames received, MSW
					                             */
#define TEMAC_RX1024BL_OFFSET	    0x00000248 /**< Count of 1024-MAX bytes
					                             *  frames received, LSW
					                             */
#define TEMAC_RX1024BU_OFFSET	    0x0000024C /**< Count of 1024-MAX bytes
					                             *  frames received, MSW
					                             */
#define TEMAC_RXOVRL_OFFSET	        0x00000250 /**< Count of oversize frames
					                             *  received, LSW
					                             */
#define TEMAC_RXOVRU_OFFSET	        0x00000254 /**< Count of oversize frames
					                             *  received, MSW
					                             */
#define TEMAC_TX64BL_OFFSET	        0x00000258 /**< Count of 64 bytes frames
					                             *  transmitted, LSW
					                             */
#define TEMAC_TX64BU_OFFSET	        0x0000025C /**< Count of 64 bytes frames
					                             *  transmitted, MSW
					                             */
#define TEMAC_TX65B127L_OFFSET	    0x00000260 /**< Count of 65-127 bytes
					                             *  frames transmitted, LSW
					                             */
#define TEMAC_TX65B127U_OFFSET	    0x00000264 /**< Count of 65-127 bytes
					                             *  frames transmitted, MSW
					                             */
#define TEMAC_TX128B255L_OFFSET	    0x00000268 /**< Count of 128-255 bytes
					                             *  frames transmitted, LSW
					                             */
#define TEMAC_TX128B255U_OFFSET	    0x0000026C /**< Count of 128-255 bytes
					                             *  frames transmitted, MSW
					                             */
#define TEMAC_TX256B511L_OFFSET	    0x00000270 /**< Count of 256-511 bytes
					                             *  frames transmitted, LSW
					                             */
#define TEMAC_TX256B511U_OFFSET	    0x00000274 /**< Count of 256-511 bytes
					                             *  frames transmitted, MSW
					                             */
#define TEMAC_TX512B1023L_OFFSET	0x00000278 /**< Count of 512-1023 bytes
					                             *  frames transmitted, LSW
					                             */
#define TEMAC_TX512B1023U_OFFSET	0x0000027C /**< Count of 512-1023 bytes
					                             *  frames transmitted, MSW
					                             */
#define TEMAC_TX1024L_OFFSET	    0x00000280 /**< Count of 1024-MAX bytes
					                             *  frames transmitted, LSW
					                             */
#define TEMAC_TX1024U_OFFSET	    0x00000284 /**< Count of 1024-MAX bytes
					                             *  frames transmitted, MSW
					                             */
#define TEMAC_TXOVRL_OFFSET	        0x00000288 /**< Count of oversize frames
					                             *  transmitted, LSW
					                             */
#define TEMAC_TXOVRU_OFFSET	        0x0000028C /**< Count of oversize frames
					                             *  transmitted, MSW
					                             */
#define TEMAC_RXFL_OFFSET		    0x00000290 /**< Count of frames received OK,
					                             *  LSW
					                             */
#define TEMAC_RXFU_OFFSET		    0x00000294 /**< Count of frames received OK,
					                             *  MSW
					                             */
#define TEMAC_RXFCSERL_OFFSET	    0x00000298 /**< Count of frames received with
					                             *  FCS error and at least 64
					                             *  bytes, LSW
					                             */
#define TEMAC_RXFCSERU_OFFSET	    0x0000029C /**< Count of frames received with
					                             *  FCS error and at least 64
					                             *  bytes,MSW
					                             */
#define TEMAC_RXBCSTFL_OFFSET	    0x000002A0 /**< Count of broadcast frames
					                             *  received, LSW
					                             */
#define TEMAC_RXBCSTFU_OFFSET	    0x000002A4 /**< Count of broadcast frames
					                             *  received, MSW
					                             */
#define TEMAC_RXMCSTFL_OFFSET	    0x000002A8 /**< Count of multicast frames
					                             *  received, LSW
					                             */
#define TEMAC_RXMCSTFU_OFFSET	    0x000002AC /**< Count of multicast frames
					                             *  received, MSW
					                             */
#define TEMAC_RXCTRFL_OFFSET	    0x000002B0 /**< Count of control frames
					                             *  received, LSW
					                             */
#define TEMAC_RXCTRFU_OFFSET	    0x000002B4 /**< Count of control frames
					                             *  received, MSW
					                             */
#define TEMAC_RXLTERL_OFFSET	    0x000002B8 /**< Count of frames received
					                             *  with length error, LSW
					                             */
#define TEMAC_RXLTERU_OFFSET	    0x000002BC /**< Count of frames received
					                             *  with length error, MSW
					                             */
#define TEMAC_RXVLANFL_OFFSET	    0x000002C0 /**< Count of VLAN tagged
					                             *  frames received, LSW
					                             */
#define TEMAC_RXVLANFU_OFFSET	    0x000002C4 /**< Count of VLAN tagged frames
					                             *  received, MSW
					                             */
#define TEMAC_RXPFL_OFFSET	        0x000002C8 /**< Count of pause frames received,
					                             *  LSW
					                             */
#define TEMAC_RXPFU_OFFSET	        0x000002CC /**< Count of pause frames received,
					                             *  MSW
					                             */
#define TEMAC_RXUOPFL_OFFSET	    0x000002D0 /**< Count of control frames
					                             *  received with unsupported
					                             *  opcode, LSW
					                             */
#define TEMAC_RXUOPFU_OFFSET	    0x000002D4 /**< Count of control frames
					                             *  received with unsupported
					                             *  opcode, MSW
					                             */
#define TEMAC_TXFL_OFFSET		    0x000002D8 /**< Count of frames transmitted OK,
					                             *  LSW
					                             */
#define TEMAC_TXFU_OFFSET		    0x000002DC /**< Count of frames transmitted OK,
					                             *  MSW
					                             */
#define TEMAC_TXBCSTFL_OFFSET	    0x000002E0 /**< Count of broadcast frames
					                             *  transmitted OK, LSW
					                             */
#define TEMAC_TXBCSTFU_OFFSET	    0x000002E4 /**< Count of broadcast frames
					                             *  transmitted, MSW
					                             */
#define TEMAC_TXMCSTFL_OFFSET	    0x000002E8 /**< Count of multicast frames
					                             *  transmitted, LSW
					                             */
#define TEMAC_TXMCSTFU_OFFSET	    0x000002EC /**< Count of multicast frames
					                             *  transmitted, MSW
					                             */
#define TEMAC_TXUNDRERL_OFFSET	    0x000002F0 /**< Count of frames transmitted
					                             *  underrun error, LSW
					                             */
#define TEMAC_TXUNDRERU_OFFSET	    0x000002F4 /**< Count of frames transmitted
					                             *  underrun error, MSW
					                             */
#define TEMAC_TXCTRFL_OFFSET 	    0x000002F8 /**< Count of control frames
					                             *  transmitted, LSW
					                             */
#define TEMAC_TXCTRFU_OFFSET 	    0x000002FC /**< Count of control frames,
					                             *  transmitted, MSW
					                             */
#define TEMAC_TXVLANFL_OFFSET	    0x00000300 /**< Count of VLAN tagged frames
					                             *  transmitted, LSW
					                             */
#define TEMAC_TXVLANFU_OFFSET	    0x00000304 /**< Count of VLAN tagged
    					                         *  frames transmitted, MSW
					                             */
#define TEMAC_TXPFL_OFFSET	        0x00000308 /**< Count of pause frames
					                             *  transmitted, LSW
					                             */
#define TEMAC_TXPFU_OFFSET	        0x0000030C /**< Count of pause frames
					                             *  transmitted, MSW
					                             */
#define TEMAC_TXSCL_OFFSET	        0x00000310 /**< Single Collision Frames
					                             *  Transmitted OK, LSW
					                             */
#define TEMAC_TXSCU_OFFSET	        0x00000314 /**< Single Collision Frames
					                             *  Transmitted OK, MSW
					                             */
#define TEMAC_TXMCL_OFFSET	        0x00000318 /**< Multiple Collision Frames
					                             *  Transmitted OK, LSW
					                             */
#define TEMAC_TXMCU_OFFSET	        0x0000031C /**< Multiple Collision Frames
					                             *  Transmitted OK, MSW
					                             */
#define TEMAC_TXDEFL_OFFSET	        0x00000320 /**< Deferred Tx Frames, LSW */
#define TEMAC_TXDEFU_OFFSET	        0x00000324 /**< Deferred Tx Frames, MSW */
#define TEMAC_TXLTCL_OFFSET	        0x00000328 /**< Frames transmitted with late
					                             *  Collisions, LSW
					                             */
#define TEMAC_TXLTCU_OFFSET	        0x0000032C /**< Frames transmitted with late
					                             *  Collisions, MSW
					                             */
#define TEMAC_TXAECL_OFFSET	        0x00000330 /**< Frames aborted with excessive
					                             *  Collisions, LSW
					                             */
#define TEMAC_TXAECU_OFFSET	        0x00000334 /**< Frames aborted with excessive
					                             *  Collisions, MSW
					                             */
#define TEMAC_TXEDEFL_OFFSET	    0x00000338 /**< Transmit Frames with excessive
					                             *  Defferal, LSW
					                             */
#define TEMAC_TXEDEFU_OFFSET	    0x0000033C /**< Transmit Frames with excessive
					                             *  Defferal, MSW
					                             */
#define TEMAC_RXAERL_OFFSET	        0x00000340 /**< Frames received with alignment
					                             *  errors, LSW
					                             */
#define TEMAC_RXAERU_OFFSET	        0x0000034C /**< Frames received with alignment
					                             *  errors, MSW
					                             */
/* End of Statistics Counter registers Offset definitions */

#define TEMAC_RCW0_OFFSET			   0x00000400 /**< Rx configuration Word 0 */
#define TEMAC_RCW1_OFFSET			   0x00000404 /**< Rx configuration Word 1 */
#define TEMAC_TC_OFFSET			       0x00000408 /**< Tx configuration */
#define TEMAC_FCC_OFFSET			   0x0000040C /**< Flow Control configuration */
#define TEMAC_MSC_OFFSET               0x00000410 /**< MAC Speed configuration */
#define TEMAC_RXFC_OFFSET			   0x00000414 /**< Rx Max Frm config register */
#define TEMAC_TXFC_OFFSET			   0x00000418 /**< Tx Max Frm config register */

/* 0x00000424 to 0x000004F4 are reserved */

#define TEMAC_IDREG_OFFSET	           0x000004F8 /**< Identification register */
#define TEMAC_ARREG_OFFSET	           0x000004FC /**< Ability register */
#define TEMAC_MDIO_MC_OFFSET	       0x00000500 /**< MII Management config */
#define TEMAC_MDIO_MCR_OFFSET	       0x00000504 /**< MII Management Control */
#define TEMAC_MDIO_MWD_OFFSET	       0x00000508 /**< MII Management Write Data */
#define TEMAC_MDIO_MRD_OFFSET	       0x0000050C /**< MII Management Read Data */

/* 0x00000510 to 0x000005FC are reserved */

#define TEMAC_IS_OFFSET	               0x00000600 /**< Interrupt Status */
/* 0x00000604-0x0000061C are reserved */

#define TEMAC_IP_OFFSET	               0x00000610 /**< Interrupt Pending register offset */
/* 0x00000624-0x0000063C are reserved */

#define TEMAC_IE_OFFSET	               0x00000620 /**<  Interrupt Enable register offset */
/* 0x00000644-0x0000065C are reserved */

#define TEMAC_IC_OFFSET	               0x00000630 /**< Interrupt Clear register offset */

/* 0x00000664-0x000006FC are reserved */

#define TEMAC_MAX_OFFSET               TEMAC_IC_OFFSET
#define TEMAC_STAT_INVALID_OFFSET      0xFFFF

/* register masks. The following constants define bit locations of various
 * bits in the registers. Constants are not defined for those registers
 * that have a single bit field representing all 32 bits. For further
 * information on the meaning of the various bit masks, refer to the HW spec.
 */

/** @name Rx Max Frm config register bit definitions
 *  @{
 */
#define TEMAC_RXFC_MASK                 0x00007FFF

/** @name Tx Max Frm config register bit definitions
 *  @{
 */
#define TEMAC_TXFC_MASK                 0x00007FFF

/** @name Transmit Pause Frame register (TPF) bit definitions
 *  @{
 */
#define TEMAC_TPF_TPFV_MASK		        0x0000FFFF  /**< Tx pause frame value */
/*@}*/

/** @name Transmit Inter-Frame Gap Adjustement register (TFGP) bit definitions
 *  @{
 */
#define TEMAC_TFGP_IFGP_MASK		        0x0000007F  /**< Transmit inter-frame
					                                      *  gap adjustment value
					                                      */
/*@}*/

/** @name Interrupt Status/Enable/Mask registers bit definitions
 *  The bit definition of these three interrupt registers are the same.
 *  These bits are associated with the TEMAC_IS_OFFSET, TEMAC_IP_OFFSET, and
 *  TEMAC_IE_OFFSET registers.
 * @{
 */
#define TEMAC_INT_HARDACSCMPLT_MASK	        0x00000001  /**< Hard register
						                                  *  access complete
						                                  */
#define TEMAC_INT_AUTONEG_MASK		        0x00000002  /**< Auto negotiation
						                                  *  complete
						                                  */
#define TEMAC_INT_RXCMPIT_MASK		        0x00000004  /**< Rx complete */        
#define TEMAC_INT_RXRJECT_MASK		        0x00000008  /**< Rx frame rejected */  
#define TEMAC_INT_RXFIFOOVR_MASK		    0x00000010  /**< Rx fifo overrun */    
#define TEMAC_INT_TXCMPIT_MASK		        0x00000020  /**< Tx complete */        
#define TEMAC_INT_RXDCMLOCK_MASK		    0x00000040  /**< Rx Dcm Lock */        
#define TEMAC_INT_MGTRDY_MASK		        0x00000080  /**< MGT clock Lock */     
#define TEMAC_INT_PHYRSTCMPLT_MASK	        0x00000100  /**< Phy Reset complete */ 
                                                                                   
#define TEMAC_INT_ALL_MASK		            0x0000003F  /**< All the ints */       

#define TEMAC_INT_RECV_ERROR_MASK			\
	(TEMAC_INT_RXRJECT_MASK | TEMAC_INT_RXFIFOOVR_MASK) /**< INT bits that
							                              *  indicate receive
							                              *  errors
							                              */
/** @name Receive configuration Word 1 (RCW1) register bit definitions
 *  @{
 */
#define TEMAC_RCW1_RST_MASK	                0x80000000  /**< Reset */
#define TEMAC_RCW1_JUM_MASK	                0x40000000  /**< Jumbo frame enable */
#define TEMAC_RCW1_FCS_MASK	                0x20000000  /**< In-Band FCS enable
					                                      *  (FCS not stripped) 
                                                          */
#define TEMAC_RCW1_RX_MASK	                0x10000000  /**< Receiver enable */
#define TEMAC_RCW1_VLAN_MASK	            0x08000000  /**< VLAN frame enable */
#define TEMAC_RCW1_LT_DIS_MASK	            0x02000000  /**< Length/type field valid check
					                                      *  disable
					                                      */
#define TEMAC_RCW1_CL_DIS_MASK	            0x01000000  /**< Control frame Length check
					                                      *  disable
					                                      */
#define TEMAC_RCW1_1588_TIMESTAMP_EN_MASK   0x00400000 /**< Inband 1588 time
											              *  stamp enable
											              */
#define TEMAC_RCW1_PAUSEADDR_MASK           0x0000FFFF  /**< Pause frame source
					                                      *  address bits [47:32].Bits
					                                      *	 [31:0] are stored in register
					                                      *  RCW0
					                                      */
/*@}*/


/** @name Transmitter configuration (TC) register bit definitions
 *  @{
 */
#define TEMAC_TC_RST_MASK		            0x80000000  /**< Reset */
#define TEMAC_TC_JUM_MASK		            0x40000000  /**< Jumbo frame enable */
#define TEMAC_TC_FCS_MASK		            0x20000000  /**< In-Band FCS enable
					                                      *  (FCS not generated)
					                                      */
#define TEMAC_TC_TX_MASK		            0x10000000  /**< Transmitter enable */
#define TEMAC_TC_VLAN_MASK	                0x08000000  /**< VLAN frame enable */
#define TEMAC_TC_IFG_MASK		            0x02000000  /**< Inter-frame gap adjustment
					                                      *  enable
					                                      */
/*@}*/


/** @name Flow Control configuration (FCC) register Bit definitions
 *  @{
 */
#define TEMAC_FCC_FCRX_MASK	                0x20000000  /**< Rx flow control enable */
#define TEMAC_FCC_FCTX_MASK	                0x40000000  /**< Tx flow control enable */
/*@}*/


/** @name Ethernet MAC Speed configuration (MSC) register bit
 * definitions @{ 
 */
#define TEMAC_MSC_LINKSPEED_MASK 	        0xC0000000  /**< Link speed */
#define TEMAC_MSC_LINKSPD_10		        0x00000000  /**< Link speed mask for
						                                  *  10 Mbit
						                                  */
#define TEMAC_MSC_LINKSPD_100		        0x40000000  /**< Link speed mask for 100
						                                  *  Mbit
						                                  */
#define TEMAC_MSC_LINKSPD_1000		        0x80000000  /**< Link speed mask for
						                                  *  1000 Mbit
						                                  */
/*@}*/
/** @name MDIO Management configuration (MC) register bit definitions
 * @{
 */
#define TEMAC_MDIO_MC_MDIOEN_MASK		    0x00000040  /**< MII management enable*/ 
#define TEMAC_MDIO_MC_CLOCK_DIVIDE_MAX	    0x3F        /**< Maximum MDIO divisor */ 
/*@}*/


/** @name MDIO Management Control register (MCR) register bit definitions
 * @{
 */
#define TEMAC_MDIO_MCR_PHYAD_MASK		    0x1F000000  /**< Phy Address Mask */      
#define TEMAC_MDIO_MCR_PHYAD_SHIFT	        24          /**< Phy Address Shift */     
#define TEMAC_MDIO_MCR_REGAD_MASK		    0x001F0000  /**< regval Address Mask */   
#define TEMAC_MDIO_MCR_REGAD_SHIFT	        16          /**< regval Address Shift */  
#define TEMAC_MDIO_MCR_OP_MASK		        0x0000C000  /**< Operation Code Mask */   
#define TEMAC_MDIO_MCR_OP_SHIFT		        13          /**< Operation Code Shift */  
#define TEMAC_MDIO_MCR_OP_READ_MASK	        0x00008000  /**< Op Code Read Mask */     
#define TEMAC_MDIO_MCR_OP_WRITE_MASK	    0x00004000  /**< Op Code Write Mask */
#define TEMAC_MDIO_MCR_INITIATE_MASK	    0x00000800  /**< Ready Mask */
#define TEMAC_MDIO_MCR_READY_MASK		    0x00000080  /**< Ready Mask */

/*@}*/

/** @name MDIO Interrupt Enable/Mask/Status registers bit definitions
 *  The bit definition of these three interrupt registers are the same.
 *  These bits are associated with the TEMAC_IS_OFFSET, TEMAC_IP_OFFSET, and
 *  TEMAC_IE_OFFSET registers.
 * @{
 */
#define TEMAC_MDIO_INT_MIIM_RDY_MASK	    0x00000001  /**< MIIM Interrupt */
/*@}*/


/** @name Other Constant definitions used in the driver
 * @{
 */

#define TEMAC_SPEED_10_MBPS		            10	        /**< speed of 10 Mbps */
#define TEMAC_SPEED_100_MBPS		        100	        /**< speed of 100 Mbps */
#define TEMAC_SPEED_1000_MBPS		        1000	    /**< speed of 1000 Mbps */
#define TEMAC_SPEED_2500_MBPS		        2500	    /**< speed of 2500 Mbps */

#define TEMAC_SOFT_TEMAC_LOW_SPEED	        0	        /**< For soft cores with 10/100
						                                  *  Mbps speed
						                                  */
#define TEMAC_SOFT_TEMAC_HIGH_SPEED	        1           /**< For soft cores with
						                                  *  10/100/1000 Mbps speed
						                                  */
#define TEMAC_HARD_TEMAC_TYPE		        2	        /**< For hard TEMAC cores used
						                                  *  virtex-6
						                                  */
#define TEMAC_PHY_ADDR_LIMIT		        31	        /**< Max limit while accessing
						                                  *  and searching for available
						                                  *  PHYs
						                                  */
#define TEMAC_PHY_REG_NUM_LIMIT		        31	        /**< Max register limit in PHY
						                                  *  as mandated by the spec
						                                  */
#define TEMAC_RST_DEFAULT_TIMEOUT_VAL       1000000     /**< Timeout in us used
						                                  *  while checking if the core
						                                  *  had come out of reset or for the driver
						                                  *  API to wait for before
						                                  *  returning a failure case
						                                  */
#define TEMAC_VLAN_TABL_STRP_FLD_LEN	    1	        /**< Strip field length in vlan
						                                  *  table used for extended
						                                  *  vlan features
						                                  */
#define TEMAC_VLAN_TABL_TAG_FLD_LEN	        1	        /**< Tag field length in vlan
						                                  *  table used for extended
						                                  *  vlan features.
						                                  */
#define TEMAC_MAX_VLAN_TABL_ENTRY		    0xFFF	    /**< Max possible number of
						                                  *  entries in vlan table used
						                                  *  for extended vlan
						                                  *  features
						                                  */
#define TEMAC_VLAN_TABL_VID_START_OFFSET	2	        /**< VID field start offset in
						                                  *  each entry in the VLAN
						                                  *  table
						                                  */
#define TEMAC_VLAN_TABL_STRP_STRT_OFFSET	1	        /**< Strip field start offset
						                                  *  in each entry in the VLAN
						                                  * table
						                                  */
#define TEMAC_VLAN_TABL_STRP_ENTRY_MASK	    0x01	    /**< Mask used to extract the
						                                  *  the strip field from an
						                                  *  entry in VLAN table.
						                                  */
#define TEMAC_VLAN_TABL_TAG_ENTRY_MASK	    0x01	    /**< Mask used to extract the
						                                  *  the tag field from an
						                                  *  entry in VLAN table
						                                  */

/** @name Configuration options
 *
 * The following are device configuration options. See the
 * <i>XAxiEthernet_SetOptions</i>, <i>XAxiEthernet_ClearOptions</i> and
 * <i>XAxiEthernet_GetOptions</i> routines for information on how to use
 * options.
 *
 * The default state of the options are also noted below.
 *
 * @{
 */

/**< TEMAC_JUMBO_OPTION specifies the TEMAC device to accept jumbo
 *   frames for transmit and receive.
 *   This driver sets this option to disabled (cleared) by default.
 */
#define TEMAC_JUMBO_OPTION		        0x00000002

/**< TEMAC_VLAN_OPTION specifies the TEMAC device to enable VLAN support
 *   for transmit and receive.
 *   This driver sets this option to disabled (cleared) by default.
 */
#define TEMAC_VLAN_OPTION			    0x00000004

/**< TEMAC_FLOW_CONTROL_OPTION specifies the TEMAC device to recognize
 *   received flow control frames.
 *   This driver sets this option to enabled (set) by default.
 */
#define TEMAC_FLOW_CONTROL_OPTION		0x00000008

/**< TEMAC_FCS_STRIP_OPTION specifies the TEMAC device to strip FCS and
 *   PAD from received frames. Note that PAD from VLAN frames is not stripped.
 *   This driver sets this option to enabled (set) by default.
 */
#define TEMAC_FCS_STRIP_OPTION		    0x00000010

/**< TEMAC_FCS_INSERT_OPTION specifies the TEMAC device to generate the
 *   FCS field and add PAD automatically for outgoing frames.
 *   This driver sets this option to enabled (set) by default.
 */
#define TEMAC_FCS_INSERT_OPTION		    0x00000020

/**< TEMAC_LENTYPE_ERR_OPTION specifies the TEMAC device to enable
 *   Length/Type error checking (mismatched type/length field) for received
 *   frames.
 *   This driver sets this option to enabled (set) by default.
 */
#define TEMAC_LENTYPE_ERR_OPTION		    0x00000040

/**< TEMAC_TRANSMITTER_ENABLE_OPTION specifies the TEMAC device
 *   transmitter to be enabled.
 *   This driver sets this option to enabled (set) by default.
 */
#define TEMAC_TRANSMITTER_ENABLE_OPTION	0x00000080

/**< TEMAC_RECEIVER_ENABLE_OPTION specifies the TEMAC device receiver to
 *   be enabled.
 *   This driver sets this option to enabled (set) by default.
 */
#define TEMAC_RECEIVER_ENABLE_OPTION	    0x00000100

/**< TEMAC_EXT_TXVLAN_TRAN_OPTION specifies the TEMAC device to enable
 *   transmit VLAN translation.
 *   This driver sets this option to be dependent on HW configuration
 *   by default.
 */
#define TEMAC_EXT_TXVLAN_TRAN_OPTION	    0x00001000

/**< TEMAC_EXT_RXVLAN_TRAN_OPTION specifies the TEMAC device to enable
 *   receive VLAN translation.
 *   This driver sets this option to be dependent on HW configuration
 *   by default.
 */
#define TEMAC_EXT_RXVLAN_TRAN_OPTION	    0x00002000

/**< TEMAC_EXT_TXVLAN_TAG_OPTION specifies the TEMAC device to enable
 *   transmit VLAN tagging.
 *   This driver sets this option to be dependent during HW build time
 *   by default.
 */
#define TEMAC_EXT_TXVLAN_TAG_OPTION	    0x00004000

/**< TEMAC_EXT_RXVLAN_TAG_OPTION specifies the TEMAC device to enable
 *   receive VLAN tagging.
 *   This driver sets this option to be dependent during HW build time
 *   by default.
 */
#define TEMAC_EXT_RXVLAN_TAG_OPTION	    0x00008000

/**< TEMAC_EXT_TXVLAN_STRP_OPTION specifies the TEMAC device to enable
 *   transmit VLAN stripping.
 *   This driver sets this option to be dependent during HW build time
 *   by default.
 */
#define TEMAC_EXT_TXVLAN_STRP_OPTION	    0x00010000

/**< TEMAC_EXT_RXVLAN_STRP_OPTION specifies the TEMAC device to enable
 *   receive VLAN stripping.
 *   This driver sets this option to be dependent during HW build time
 *   by default.
 */
#define TEMAC_EXT_RXVLAN_STRP_OPTION	    0x00020000


#define TEMAC_DEFAULT_OPTIONS \
		(TEMAC_FLOW_CONTROL_OPTION       | \
		 TEMAC_FCS_INSERT_OPTION         | \
		 TEMAC_FCS_STRIP_OPTION          | \
		 TEMAC_LENTYPE_ERR_OPTION        | \
		 TEMAC_TRANSMITTER_ENABLE_OPTION | \
		 TEMAC_RECEIVER_ENABLE_OPTION)
/**< TEMAC_DEFAULT_OPTIONS specify the options set in XAxiEthernet_Reset() and
 *   XAxiEthernet_CfgInitialize()
 */
/*@}*/

#define TEMAC_MDIO_DIV_DFT	            29	        /**< Default MDIO clock divisor */

/**<
 * The next few constants help upper layers determine the size of memory
 * pools used for Ethernet buffers and descriptor lists.
 */
#define TEMAC_MAC_ADDR_SIZE		        6	        /**< MAC addresses are 6 bytes */
#define TEMAC_MTU				        1500	    /**< Max MTU size of an Ethernet
						                                  *  frame
						                                  */
#define TEMAC_JUMBO_MTU			        8982        /**< Max MTU size of a jumbo
						                                  *  Ethernet frame
						                                  */
#define TEMAC_HDR_SIZE			        14	        /**< Size of an Ethernet header */
#define TEMAC_HDR_VLAN_SIZE		        18	        /**< Size of an Ethernet header
						                                  *  with VLAN
						                                  */
#define TEMAC_TRL_SIZE			        4	        /**< Size of an Ethernet trailer
						                                  *  (FCS)
						                                  */
#define TEMAC_MAX_FRAME_SIZE	 (TEMAC_MTU + TEMAC_HDR_SIZE + TEMAC_TRL_SIZE)
#define TEMAC_MAX_VLAN_FRAME_SIZE  (TEMAC_MTU + TEMAC_HDR_VLAN_SIZE + TEMAC_TRL_SIZE)
#define TEMAC_MAX_JUMBO_FRAME_SIZE (TEMAC_JUMBO_MTU + TEMAC_HDR_SIZE + TEMAC_TRL_SIZE)

/**<
 * Constant values returned by XAxiEthernet_GetPhysicalInterface(). Note that
 * these values match design parameters from the TEMAC spec.
 */
#define TEMAC_PHY_TYPE_MII		        0
#define TEMAC_PHY_TYPE_GMII		        1
#define TEMAC_PHY_TYPE_RGMII_1_3		    2
#define TEMAC_PHY_TYPE_RGMII_2_0		    3
#define TEMAC_PHY_TYPE_SGMII		        4
#define TEMAC_PHY_TYPE_1000BASE_X		5

#define TEMAC_TPID_MAX_ENTRIES		    4           /**< Number of storable TPIDs in
					                                      *  Table
					                                      */

/**<
 * Constant values pass into XAxiEthernet_SetV[tag|Strp]Mode() and returned by
 * XAxiEthernet_GetV[tag|Strp]Mode().
 */
#define TEMAC_VTAG_NONE			        0	        /**< No tagging */
#define TEMAC_VTAG_ALL			        1	        /**< Tag all frames */
#define TEMAC_VTAG_EXISTED		        2	        /**< Tag already tagged frames */
#define TEMAC_VTAG_SELECT		  	    3	        /**< Tag selected already tagged
						                                  *  frames
						                                  */
#define TEMAC_DEFAULT_TXVTAG_MODE  	    TEMAC_VTAG_ALL
#define TEMAC_DEFAULT_RXVTAG_MODE  	    TEMAC_VTAG_ALL

#define TEMAC_VSTRP_NONE			        0	        /**< No stripping */
#define TEMAC_VSTRP_ALL			        1	        /**< Strip one tag from all
						                                  *  frames
						                                  */
#define TEMAC_VSTRP_SELECT		        3	        /**< Strip one tag from selected
						                                  *  frames
						                                  */
#define TEMAC_DEFAULT_TXVSTRP_MODE	    TEMAC_VSTRP_ALL
#define TEMAC_DEFAULT_RXVSTRP_MODE	    TEMAC_VSTRP_ALL

#define TEMAC_RX				            1           /**< Receive direction  */
#define TEMAC_TX				            2           /**< Transmit direction */

#define TEMAC_SOFT_TEMAC_10_100_MBPS	    0
#define TEMAC_SOFT_TEMAC_10_100_1000_MBPS   1
#define TEMAC_HARD_TEMC			            2


/* This is true only for Marvell 88E1510 PHY */
#define TEMAC_PHY_LINK_STATUS_PAGE          0
#define TEMAC_PHY_LINK_STATUS_REG           17
#define TEMAC_PHY_LINK_STATUS_MASK          0x400
#define TEMAC_PHY_LINK_STATUS_SHIFT         10


/**
 * This typedef contains configuration information for a TEMAC device.
 */
typedef struct {
	b_u16 dev_id;	              /**< DeviceId is the unique ID  of the device */             
	b_u8 temac_type;              /**< Temac Type can have 3 possible values. They are         
			                        *  0 for SoftTemac at 10/100 Mbps, 1 for SoftTemac         
			                        *  at 10/100/1000 Mbps and 2 for Vitex6 Hard Temac         
			                        */                                                         
	b_u8 tx_csum;	              /**< TxCsum indicates that the device has checksum           
			                        *  offload on the Tx channel or not.                       
			                        */                                                         
	b_u8 rx_csum;	              /**< RxCsum indicates that the device has checksum           
			                        *  offload on the Rx channel or not.                       
			                        */                                                         
	b_u8 phy_type;	              /**< phy_type indicates which type of PHY interface is       
			                        *  used (MII, GMII, RGMII, etc.                            
			                        */                                                         
	b_u8 tx_vlan_tran;            /**< TX VLAN Translation indication */                      
	b_u8 rx_vlan_tran;            /**< RX VLAN Translation indication */                      
	b_u8 tx_vlan_tag;             /**< TX VLAN tagging indication */                          
	b_u8 rx_vlan_tag;             /**< RX VLAN tagging indication */                          
	b_u8 tx_vlan_strp;            /**< TX VLAN stripping indication */                        
	b_u8 rx_vlan_strp;            /**< RX VLAN stripping indication */                        
	b_u8 ext_mcast;               /**< Extend multicast indication */                          
	b_u8 stats;	                  /**< Statistics gathering option */
	b_u8 avb;		              /**< Avb option */
	b_u8 enable_sgmii_over_lvds;  /**< Enable LVDS option */
	b_u8 enable_1588;	          /**< Enable 1588 option */
	b_u32 speed;	              /**< Tells whether MAC is 1G or 2p5G */
	b_u8 num_table_entries;	      /**< Number of table entries */

	b_u8 temac_intr;	          /**< TEMAC interrupt ID */

	b_i32 axi_dev_type;           /**< AxiDevType is the type of device attached to the
			                        *  TEMAC's AXI4-Stream interface.
			                        */
	b_u8 axi_fifo_intr;	          /**< AxiFifoIntr interrupt ID (unused if DMA) */
	b_u8 axi_dma_rx_intr;         /**< Axi DMA RX interrupt ID (unused if FIFO) */
	b_u8 axi_dma_tx_intr;         /**< Axi DMA TX interrupt ID (unused if FIFO) */
	b_u8 axi_mc_dma_chan_cnt;     /**< Axi MCDMA Channel Count */
	b_u8 axi_mc_dma_rx_intr[16];  /**< Axi MCDMA Rx interrupt ID (unused if AXI DMA or FIFO) */
	b_u8 axi_mc_dma_tx_intr[16];  /**< AXI MCDMA TX interrupt ID (unused if AXIX DMA or FIFO) */
} temac_config_t;

/** @struct temac_ctx_t
 *  temac context structure
 */
typedef struct temac_ctx_t {
    void             *devo;
    eth_brdspec_t    brdspec;
    temac_config_t   config;
    b_u32            options;
    eth_counter_t    *counters;
    b_u32            counters_num;
    b_bool           is_started;
} temac_ctx_t;

static ccl_err_t    _ret;

static ccl_err_t _temac_phy_write(temac_ctx_t *p_temac, b_u32 phy_addr, 
                                  b_u32 reg, b_u32 page, b_u32 value);
static ccl_err_t _temac_phy_read(temac_ctx_t *p_temac, b_u32 phy_addr, 
                                 b_u32 reg, b_u32 page, b_u32 *value);
static ccl_err_t _temac_init(void *p_priv);

/***************** Macros (Inline Functions) Definitions *********************/

/**
 * TEMAC_IS_STARTED reports if the device is in the started or stopped
 * state. To be in the started state, the calling code must have made a
 * successful call to <i>_temac_start</i>. To be in the stopped state,
 * <i>_temac_stop</i> function must have been called.
 *
 * @param[in] p_temac is a pointer to the of TEMAC 
 *      instance to be worked on.
 *
 * @return
 *		- CCL_TRUE if the device has been started.
 *		- CCL_FALSE.if the device has not been started
 */
#define TEMAC_IS_STARTED(p_temac) \
	((p_temac)->is_started == CCL_TRUE)

/**
 * TEMAC_IS_RECV_FRAME_DROPPED determines if the device 
 * thinks it has dropped a receive frame. The device interrupt 
 * status register is read to determine this. 
 *
 * @param[in] p_temac is a pointer to the TEMAC 
 *      instance to be worked on.
 *
 * @return
 *		- CCL_TRUE if a frame has been dropped
 *		- CCL_FALSE if a frame has NOT been dropped.
 */
#define TEMAC_IS_RECV_FRAME_DROPPED(p_temac) ({ \
    b_u32 regval; \
    TEMAC_REG_READ(p_temac, TEMAC_IS_OFFSET, \
                      sizeof(b_u32), 0, \
                      (b_u8 *)&regval, sizeof(b_u32)); \
    ((regval & TEMAC_INT_RXRJECT_MASK) ? CCL_TRUE : CCL_FALSE); \
})

/**
 *
 * TEMAC_IS_RX_PARTIAL_CSUM determines if the device is 
 * configured with partial checksum offloading on the receive 
 * channel. 
 *
 * @param[in] p_temac is a pointer to the TEMAC 
 *      instance to be worked on.
 * @return
 *		- CCL_TRUE if the device is configured with partial checksum
 *		  offloading on the receive channel.
 *      - CCL_FALSE if the device is not configured with
 *        partial checksum offloading on the receive side.
 */
#define TEMAC_IS_RX_PARTIAL_CSUM(p_temac)   \
	((((p_temac)->config.rx_csum) == 0x01) ? CCL_TRUE : CCL_FALSE)

/**
 * TEMAC_IS_TX_PARTIAL_CSUM determines if the device is 
 * configured with partial checksum offloading on the transmit 
 * channel. 
 *
 * @param[in] p_temac is a pointer to the TEMAC 
 *      instance to be worked on.
 *
 * @return
 *		- CCL_TRUE if the device is configured with partial checksum
 *		  offloading on the transmit side.
 *      - CCL_FALSE if the device is not configured with
 *        partial checksum offloading on the transmit side.
 */
#define TEMAC_IS_TX_PARTIAL_CSUM(p_temac) \
	((((p_temac)->config.tx_csum) == 0x01) ? CCL_TRUE : CCL_FALSE)

/**
 *  TEMAC_IS_RX_FULL_CSUM determines if the device is
 *  configured with full checksum offloading on the receive
 *  channel.
 *
 *  @param[in] p_temac is a pointer to the TEMAC
 *       instance to be worked on.
 * @return
 *		- CCL_TRUE if the device is configured with full checksum
 *		  offloading on the receive channel.
 *       - CCL_FALSE if the device is not configured with
 *         full checksum offloading on the receive side.
 */
#define TEMAC_IS_RX_FULL_CSUM(p_temac)   \
	((((p_temac)->config.rx_csum) == 0x02) ? CCL_TRUE : CCL_FALSE)

/**
 * TEMAC_IS_TX_FULL_CSUM determines if the device is 
 * configured with full checksum offloading on the transmit 
 * channel. 
 *
 * @param[in] p_temac is a pointer to the TEMAC 
 *      instance to be worked on.
 *
 * @return
 *		- CCL_TRUE if the device is configured with full checksum
 *		  offloading on the transmit side.
 *      - CCL_FALSE if the device is not configured with
 *        full checksum offloading on the transmit side.
 */
#define TEMAC_IS_TX_FULL_CSUM(p_temac) \
	((((p_temac)->config.tx_csum) == 0x02) ? CCL_TRUE : CCL_FALSE)

/**
 * TEMAC_GET_PHYSICAL_IF returns the type of PHY interface being
 * used by the given instance, specified by <i>p_temac</i>.
 *
 * @param[in] p_temac is a pointer to the TEMAC 
 *      instance to be worked on.
 *
 * @return	The Physical Interface type which is one of TEMAC_PHY_TYPE_x
 *		where x is MII, GMII, RGMII_1_3, RGMII_2_0, SGMII, or
 *		1000BASE_X (defined in xaxiethernet.h).
 *
 */
#define TEMAC_GET_PHYSICAL_IF(p_temac)	   \
	((p_temac)->config.phy_type)

/**
 * TEMAC_INT_ENABLE enables the interrupts specified in 
 * <i>mask</i>. The corresponding interrupt for each bit set to 
 * 1 in <i>mask</i>, will be enabled. 
 *
 * @param[in] p_temac is a pointer to the TEMAC 
 *      instance to be worked on.
 *
 * @param[in] mask contains a bit mask of the interrupts to 
 *      enable. The mask can be formed using a set of bit wise
 *      or'd values from the
 *		<code>TEMAC_INT_*_MASK</code> definitions
 *
 * @return	None.
 */
#define TEMAC_INT_ENABLE(p_temac, mask) do { \
    b_u32 regval; \
    TEMAC_REG_READ(p_temac, TEMAC_IS_OFFSET, \
                      sizeof(b_u32), 0, \
                      (b_u8 *)&regval, sizeof(b_u32)); \
    regval |= ((mask) & TEMAC_INT_ALL_MASK); \
    TEMAC_REG_WRITE(p_temac, TEMAC_IS_OFFSET, \
                       sizeof(b_u32), 0, \
                       (b_u8 *)&regval, sizeof(b_u32)); \
} while (0)

/**
 * TEMAC_INT_DISABLE disables the interrupts specified in 
 * <i>mask</i>. The corresponding interrupt for each bit set to
 * 1 in <i>mask</i>, will be disabled. In other words,
 * TEMAC_INT_DISABLE uses the "set a bit to clear it" scheme. 
 *
 * @param[in] p_temac is a pointer to the TEMAC 
 *      instance to be worked on.
 * @param[in] mask contains a bit mask of the interrupts to 
 *      disable. The mask can be formed using a set of bit wise
 *      or'd values from the <code>TEMAC_INT_*_MASK</code>
 *      definitions
 *
 * @return	None.
 */
#define TEMAC_INT_DISABLE(p_temac, mask) do { \
    b_u32 regval; \
    TEMAC_REG_READ(p_temac, TEMAC_IS_OFFSET, \
                      sizeof(b_u32), 0, \
                      (b_u8 *)&regval, sizeof(b_u32)); \
    regval &= ~((mask) & TEMAC_INT_ALL_MASK); \
    TEMAC_REG_WRITE(p_temac, TEMAC_IS_OFFSET, \
                       sizeof(b_u32), 0, \
                       (b_u8 *)&regval, sizeof(b_u32)); \
} while (0)

/**
 * TEMAC_INT_CLEAR clears pending interrupts specified in 
 * <i>mask</i>. The corresponding pending interrupt for each bit 
 * set to 1 in <i>mask</i>, will be cleared. In other words, 
 * TEMAC_INT_CLEAR uses the "set a bit to clear it" scheme. 
 *
 *  @param[in] p_temac is a pointer to the TEMAC
 *       instance to be worked on.
 * @param[in] mask contains a bit mask of the pending 
 *      interrupts to clear. The mask can be formed using a set
 *      of bit wise or'd values from
 *		the <code>TEMAC_INT_*_MASK</code> definitions
 *		file.
 */
#define TEMAC_INT_CLEAR(p_temac, mask) do { \
    b_u32 regval = ((mask) & TEMAC_INT_ALL_MASK); \
    TEMAC_REG_WRITE(p_temac, TEMAC_IS_OFFSET, \
                       sizeof(b_u32), 0, \
                       (b_u8 *)&regval, sizeof(b_u32)); \
} while (0)

/**
 * TEMAC_IS_EXT_MCAST determines if the device is built with 
 * new/extended multicast features. 
 *
 *  @param[in] p_temac is a pointer to the TEMAC
 *       instance to be worked on.
 *
 * @return
 *		- CCL_TRUE if the device is built with extended multicast features.
 *		- CCL_FALSE if the device is not built with the extended multicast
 *		  features.
 *
 * @note This function indicates when hardware is built with 
 *      extended Multicast feature.
 */
#define TEMAC_IS_EXT_MCAST(p_temac) \
	(((p_temac)->config.ext_mcast) ? CCL_TRUE : CCL_FALSE)

/**
 * TEMAC_IS_TX_VLAN_STRP determines if the device is 
 * configured with transmit VLAN stripping on the transmit 
 * channel. 
 *
 *  @param[in] p_temac is a pointer to the TEMAC
 *       instance to be worked on.
 *
 * @return
 *	 	- CCL_TRUE if the device is configured with
 *		  VLAN stripping on the Transmit channel.
 *		- CCL_FALSE if the device is NOT configured with
 *		  VLAN stripping on the Transmit channel.
 */
#define TEMAC_IS_TX_VLAN_STRP(p_temac) \
	(((p_temac)->config.tx_vlan_strp) ? CCL_TRUE : CCL_FALSE)

/**
 * TEMAC_IS_RX_VLAN_STRP determines if the device is 
 * configured with receive VLAN stripping on the receive channel.
 *
 *  @param[in] p_temac is a pointer to the TEMAC
 *       instance to be worked on.
 *
 * @return
 *	 	- CCL_TRUE if the device is configured with
 *		  VLAN stripping on the Receive channel.
 *		- CCL_FALSE if the device is NOT configured with
 *		  VLAN stripping on the Receive channel.
 */
#define TEMAC_IS_RX_VLAN_STRP(p_temac) \
	(((p_temac)->config.rx_vlan_strp) ? CCL_TRUE : CCL_FALSE)

/**
 * TEMAC_IS_TX_VLAN_TRAN determines if the device is 
 * configured with transmit VLAN translation on the transmit 
 * channel. 
 *
 * @param[in] p_temac is a pointer to the TEMAC
 *       instance to be worked on.
 *
 * @return
 *	 	- CCL_TRUE if the device is configured with
 *		 VLAN translation on the Transmit channel.
 *		- CCL_FALSE if the device is NOT configured with
 *		 VLAN translation on the Transmit channel.
 */
#define TEMAC_IS_TX_VLAN_TRAN(p_temac) \
	(((p_temac)->config.tx_vlan_tran) ? CCL_TRUE : CCL_FALSE)

/**
 * TEMAC_IS_RX_VLAN_TRAN determines if the device is 
 * configured with receive VLAN translation on the receive 
 * channel. 
 *
 *  @param[in] p_temac is a pointer to the TEMAC
 *       instance to be worked on.
 *
 * @return
 *	 	- CCL_TRUE if the device is configured with
 *		  VLAN translation on the Receive channel.
 *		- CCL_FALSE if the device is NOT configured with
 *		  VLAN translation on the Receive channel.
 */
#define TEMAC_IS_RX_VLAN_TRAN(p_temac) \
	(((p_temac)->config.rx_vlan_tran) ? CCL_TRUE : CCL_FALSE)

/**
 * TEMAC_IS_TX_VLAN_TAG determines if the device is configured 
 * with transmit VLAN tagging on the transmit channel. 
 * 
 *  @param[in] p_temac is a pointer to the TEMAC
 *       instance to be worked on.
 *
 * @return
 *	 	- CCL_TRUE if the device is configured with
 *		  VLAN tagging on the Transmit channel.
 *		- CCL_FALSE if the device is NOT configured with
 *		  VLAN tagging on the Transmit channel.
 */
#define TEMAC_IS_TX_VLAN_TAG(p_temac)	\
	(((p_temac)->config.tx_vlan_tag) ? CCL_TRUE : CCL_FALSE)

/**
 * TEMAC_IS_RX_VLAN_TAG determines if the device is configured 
 * with receive VLAN tagging on the receive channel. 
 *
 *  @param[in] p_temac is a pointer to the TEMAC
 *       instance to be worked on.
 *
 * @return
 *	 	- CCL_TRUE if the device is configured with
 *		  VLAN tagging on the Receive channel.
 *		- CCL_FALSE if the device is NOT configured with
 *		  VLAN tagging on the Receive channel.
 */
#define TEMAC_IS_RX_VLAN_TAG(p_temac) \
	(((p_temac)->config.rx_vlan_tag) ? CCL_TRUE : CCL_FALSE)

/**
 * TEMAC_GET_TEMAC_TYPE returns the TEMAC type of the 
 * core. 
 *
 * @param	p_temac is a pointer to the temac_ctx_t instance to be
 *		worked on.
 *
 * @return
 *	 	- Returns the values of temac_type, which can be
 *		  TEMAC_SOFT_TEMAC_10_100_MBPS (0) for Soft Temac Core with
 *							speed 10/100 Mbps.
 *		  TEMAC_SOFT_TEMAC_10_100_1000_MBPS (1) for Soft Temac core with
 *							speed 10/100/1000 Mbps
 *		  TEMAC_HARD_TEMC (2) for Hard Temac Core for Virtex-6
 */
#define TEMAC_GET_TEMAC_TYPE(p_temac)	\
	((p_temac)->config.temac_type)

/**
 *  TEMAC_IS_AVB_CONFIGURED returns determines if Ethernet AVB
 *  is configured in the harwdare or not.
 *
 *  @param[in]	p_temac is a pointer to the TEMAC
 *       instance to be worked on.
 *
 * @return
 *	 	- CCL_TRUE if the device is configured with
 *		  Ethernet AVB.
 *		- CCL_FALSE if the device is NOT configured with
 *		  Ethernet AVB.
 */
#define TEMAC_IS_AVB_CONFIGURED(p_temac) \
	(((p_temac)->config.avb) ? CCL_TRUE : CCL_FALSE)

/**
 * TEMAC_IS_SGMII_OVER_LVDS_ENABLED determines if SGMII over 
 * LVDS is enabled in the harwdare or not. 
 *
 *  @param[in] p_temac is a pointer to the TEMAC
 *       instance to be worked on.
 *
 * @return
 *	 	- CCL_TRUE if the device is configured with
 *		  SGMII over LVDS.
 *		- CCL_FALSE if the device is NOT configured with
 *		  SGMII over LVDS.
 */
#define TEMAC_IS_SGMII_OVER_LVDS_ENABLED(p_temac) \
	(((p_temac)->config.enable_sgmii_over_lvds) ? CCL_TRUE : CCL_FALSE)

/* temac device attributes definition */
static device_attr_t _attrs[] = {
    {
        .name = "name", 
        .type = eDEVMAN_ATTR_STRING, .maxnum = 1, 
        .offset = offsetof(struct temac_ctx_t, brdspec.name),
        .size = DEVMAN_DEVNAME_LEN,
        .format = "%s"
    },
    {
        .name = "offset", 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset = offsetof(struct temac_ctx_t, brdspec.offset),
        .format = "0x%08x"
    },
    {
        .name = "rx_bytes_low_cnt", .id = eTEMAC_RXBL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_RXBL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_bytes_high_cnt", .id = eTEMAC_RXBL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_RXBU_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_bytes_low_cnt", .id = eTEMAC_TXBL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_TXBL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_bytes_high_cnt", .id = eTEMAC_TXBL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_TXBU_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_undersized_low_cnt", .id = eTEMAC_RXUNDRL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_RXUNDRL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_undersized_high_cnt", .id = eTEMAC_RXUNDRL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_RXUNDRU_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_frag_low_cnt", .id = eTEMAC_RXFRAGL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_RXFRAGL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_frag_high_cnt", .id = eTEMAC_RXFRAGL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_RXFRAGU_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_64_bytes_low_cnt", .id = eTEMAC_RX64BL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_RX64BL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_64_bytes_high_cnt", .id = eTEMAC_RX64BL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_RX64BU_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_65_127_low_cnt", .id = eTEMAC_RX65B127L, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_RX65B127L_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_65_127_high_cnt", .id = eTEMAC_RX65B127L, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_RX65B127U_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_128_255_low_cnt", .id = eTEMAC_RX128B255L, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_RX128B255L_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_128_255_high_cnt", .id = eTEMAC_RX128B255L, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_RX128B255U_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_256_511_low_cnt", .id = eTEMAC_RX256B511L, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_RX256B511L_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_256_511_high_cnt", .id = eTEMAC_RX256B511L, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_RX256B511U_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_512_1023_low_cnt", .id = eTEMAC_RX512B1023L, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_RX512B1023L_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_512_1023_high_cnt", .id = eTEMAC_RX512B1023L, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_RX512B1023U_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_1024_max_low_cnt", .id = eTEMAC_RX1024BL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_RX1024BL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_1024_max_high_cnt", .id = eTEMAC_RX1024BL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_RX1024BU_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_oversize_low_cnt", .id = eTEMAC_RXOVRL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_RXOVRL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_high_low_cnt", .id = eTEMAC_RXOVRL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_RXOVRU_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_64_bytes_low_cnt", .id = eTEMAC_TX64BL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_TX64BL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_64_bytes_high_cnt", .id = eTEMAC_TX64BL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_TX64BU_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_65_127_low_cnt", .id = eTEMAC_TX65B127L, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_TX65B127L_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_65_127_high_cnt", .id = eTEMAC_TX65B127L, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_TX65B127U_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_128_255_low_cnt", .id = eTEMAC_TX128B255L, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_TX128B255L_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_128_255_high_cnt", .id = eTEMAC_TX128B255L, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_TX128B255U_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_256_511_low_cnt", .id = eTEMAC_TX256B511L, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_TX256B511L_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_256_511_high_cnt", .id = eTEMAC_TX256B511L, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_TX256B511U_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_512_1023_low_cnt", .id = eTEMAC_TX512B1023L, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_TX512B1023L_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_512_1023_high_cnt", .id = eTEMAC_TX512B1023L, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_TX512B1023U_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_1024_max_low_cnt", .id = eTEMAC_TX1024L, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_TX1024L_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_1024_max_high_cnt", .id = eTEMAC_TX1024L, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_TX1024U_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_oversize_low_cnt", .id = eTEMAC_TXOVRL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_TXOVRL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packet_oversize_high_cnt", .id = eTEMAC_TXOVRL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_TXOVRU_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_good_packet_low", .id = eTEMAC_RXFL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_RXFL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_good_packet_high", .id = eTEMAC_RXFL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_RXFU_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_fcs_err_low", .id = eTEMAC_RXFCSERL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_RXFCSERL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_fcs_err_high", .id = eTEMAC_RXFCSERL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_RXFCSERU_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_bcast_low", .id = eTEMAC_RXBCSTFL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_RXBCSTFL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_bcast_high", .id = eTEMAC_RXBCSTFL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_RXBCSTFU_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_mcast_low", .id = eTEMAC_RXMCSTFL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_RXMCSTFL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_mcast_high", .id = eTEMAC_RXMCSTFL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_RXMCSTFU_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_ctrl_low", .id = eTEMAC_RXCTRFL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_RXCTRFL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_ctrl_high", .id = eTEMAC_RXCTRFL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_RXCTRFU_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_len_err_low", .id = eTEMAC_RXLTERL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_RXLTERL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_len_err_high", .id = eTEMAC_RXLTERL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_RXLTERU_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_vlan_tagged_low", .id = eTEMAC_RXVLANFL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_RXVLANFL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_vlan_tagged_high", .id = eTEMAC_RXVLANFL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_RXVLANFU_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_pause_low", .id = eTEMAC_RXPFL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_RXPFL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_pause_high", .id = eTEMAC_RXPFL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_RXPFU_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_ctrl_unsup_opcode_low", .id = eTEMAC_RXUOPFL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_RXUOPFL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_ctrl_unsup_opcode_high", .id = eTEMAC_RXUOPFL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_RXUOPFU_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_good_packet_low", .id = eTEMAC_TXFL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_TXFL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_good_packet_high", .id = eTEMAC_TXFL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_TXFU_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_bcast_low", .id = eTEMAC_TXBCSTFL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_TXBCSTFL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_bcast_high", .id = eTEMAC_TXBCSTFL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_TXBCSTFU_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_mcast_low", .id = eTEMAC_TXMCSTFL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_TXMCSTFL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_mcast_high", .id = eTEMAC_TXMCSTFL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_TXMCSTFU_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_underrun_err_low", .id = eTEMAC_TXUNDRERL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_TXUNDRERL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_underrun_err_high", .id = eTEMAC_TXUNDRERL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_TXUNDRERU_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_ctrl_low", .id = eTEMAC_TXCTRFL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_TXCTRFL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_ctrl_high", .id = eTEMAC_TXCTRFL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_TXCTRFU_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_vlan_tagged_low", .id = eTEMAC_TXVLANFL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_TXVLANFL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_vlan_tagged_high", .id = eTEMAC_TXVLANFL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_TXVLANFU_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_pause_low", .id = eTEMAC_TXPFL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_TXPFL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_pause_high", .id = eTEMAC_TXPFL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_TXPFU_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_single_collision_low", .id = eTEMAC_TXSCL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_TXSCL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_single_collision_high", .id = eTEMAC_TXSCL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_TXSCU_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_multi_collision_low", .id = eTEMAC_TXMCL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_TXMCL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_multi_collision_high", .id = eTEMAC_TXMCL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_TXMCU_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_deferred_low", .id = eTEMAC_TXDEF, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_TXDEFL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_deferred_high", .id = eTEMAC_TXDEF, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_TXDEFU_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_late_collision_low", .id = eTEMAC_TXLTCL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_TXLTCL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_late_collision_high", .id = eTEMAC_TXLTCL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_TXLTCU_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_aborted_ex_defferal_low", .id = eTEMAC_TXAECL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_TXAECL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_aborted_ex_defferal_high", .id = eTEMAC_TXAECL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_TXAECU_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_ex_defferal_low", .id = eTEMAC_TXEDEFL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_TXEDEFL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_ex_defferal_high", .id = eTEMAC_TXEDEFL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_TXEDEFU_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_alignment_low", .id = eTEMAC_RXAERL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_RXAERL_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_alignment_high", .id = eTEMAC_RXAERL, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_RXAERU_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_config_word0", .id = eTEMAC_RCW0, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_RCW0_OFFSET
    },
    {
        .name = "rx_config_word1", .id = eTEMAC_RCW1, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_RCW1_OFFSET
    },
    {
        .name = "tx_config", .id = eTEMAC_TC, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_TC_OFFSET
    },
    {
        .name = "fc_config", .id = eTEMAC_FCC, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_FCC_OFFSET
    },
    {
        .name = "emac_mod_config", .id = eTEMAC_MSC, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_MSC_OFFSET
    },
    {
        .name = "rx_max_frame_config", .id = eTEMAC_RXFC, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_RXFC_OFFSET
    },
    {
        .name = "tx_max_frame_config", .id = eTEMAC_TXFC, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_TXFC_OFFSET
    },
    {
        .name = "identification", .id = eTEMAC_IDREG, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_IDREG_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "ability", .id = eTEMAC_ARREG, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_ARREG_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "mii_config", .id = eTEMAC_MDIO_MC, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_MDIO_MC_OFFSET
    },
    {
        .name = "mii_ctrl", .id = eTEMAC_MDIO_MCR, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_MDIO_MCR_OFFSET
    },
    {
        .name = "mii_write_data", .id = eTEMAC_MDIO_MWD, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_MDIO_MWD_OFFSET
    },
    {
        .name = "mii_read_data", .id = eTEMAC_MDIO_MRD, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_MDIO_MRD_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "mii_ints", .id = eTEMAC_IS, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_IS_OFFSET
    },
    {
        .name = "mii_intp", .id = eTEMAC_IP, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_IP_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "mii_inte", .id = eTEMAC_IE, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_IE_OFFSET
    },
    {
        .name = "mii_intc", .id = eTEMAC_IC, .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TEMAC_IC_OFFSET
    },
    {
        .name = "read_single_counter", .id = eTEMAC_STAT_SINGLE_COUNTER, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVMAN_IF_MIB_MAX, .size = sizeof(b_u64), 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "property", .id = eTEMAC_IF_PROPERTY, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVMAN_PROP_MAX, .size = 128, 
        .offset_sz = sizeof(b_u32),
    },
    {
        .name = "read_counters", .id = eTEMAC_STAT_COUNTERS, .type = eDEVMAN_ATTR_BUFF, 
        .maxnum = 1, .size = sizeof(port_stats_t), .offset_sz = sizeof(b_u8),
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "init", .id = eTEMAC_INIT, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "config_test", .id = eTEMAC_CONFIGURE_TEST, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "run_test", .id = eTEMAC_RUN_TEST, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .size = sizeof(board_dev_stats_t), 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
};

static mib_counter_map_t _temac_mib_cntr_map[] = {
    {
        .ifmib = DEVMAN_IF_MIB_BYTEREC, .offset = TEMAC_RXBL_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_BYTSENT, .offset = TEMAC_TXBL_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRAREC, .offset = TEMAC_RXFL_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRASENT, .offset = TEMAC_TXFL_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_TOTALBYTEREC, .offset = TEMAC_RXBL_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_TOTALFRAMEREC, .offset = TEMAC_RXFL_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_BRDFRAREC, .offset = TEMAC_RXBCSTFL_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_MULFRAREC, .offset = TEMAC_RXMCSTFL_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_CRCERR, .offset = TEMAC_RXFCSERL_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRAOVERSIZE, .offset = TEMAC_RXOVRL_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRAFRAGMENTS, .offset = TEMAC_RXFRAGL_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_JABBER, .offset = TEMAC_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_COLL, .offset = TEMAC_TXSCL_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_LATECOL, .offset = TEMAC_TXLTCL_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRA64, .offset = TEMAC_RX64BL_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRA65T127, .offset = TEMAC_RX65B127L_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRA128T255, .offset = TEMAC_RX128B255L_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRA256T511, .offset = TEMAC_RX256B511L_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRA512T1023, .offset = TEMAC_RX512B1023L_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_FRA1024T1522, .offset = TEMAC_RX1024BL_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_MACRXERR, .offset = TEMAC_RXFCSERL_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_DROPPED, .offset = TEMAC_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_INUCASTPKT, .offset = TEMAC_STAT_INVALID_OFFSET  //TODO: must be total - bcast - mcast
    },
    {
        .ifmib = DEVMAN_IF_MIB_INNOUCASTPKT, .offset = TEMAC_STAT_INVALID_OFFSET  //TODO: must be bcast + mcast
    },
    {
        .ifmib = DEVMAN_IF_MIB_INERRORS, .offset = TEMAC_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_OUTERRORS, .offset = TEMAC_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_OUTNOUCASTPKT, .offset = TEMAC_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_OUTUCASTPKT, .offset = TEMAC_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_MULTIOUT, .offset = TEMAC_TXMCSTFL_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_BROADOUT, .offset = TEMAC_TXBCSTFL_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_UNDERSIZ, .offset = TEMAC_RXUNDRL_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_INUNKNOWNPROTOS, .offset = TEMAC_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_ALIGN_ERR, .offset = TEMAC_RXAERL_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_FCS_ERR, .offset = TEMAC_RXFCSERL_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_SQETEST_ERR, .offset = TEMAC_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_CSE_ERR, .offset = TEMAC_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_SYMBOL_ERR, .offset = TEMAC_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_MACTX_ERR, .offset = TEMAC_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_MACRX_ERR, .offset = TEMAC_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_TOOLONGFRA, .offset = TEMAC_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_SNGL_COLLISION, .offset = TEMAC_TXSCL_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_MULT_COLLISION, .offset = TEMAC_TXMCL_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_LATE_COLLISION, .offset = TEMAC_TXLTCL_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_EXCESS_COLLISION, .offset = TEMAC_TXAECL_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_INUNKNOWNOPCODE, .offset = TEMAC_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_DEFERREDTX, .offset = TEMAC_TXEDEFL_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_INPAUSE_FRAMES, .offset = TEMAC_RXPFL_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_OUTPAUSE_FRAMES, .offset = TEMAC_TXPFL_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_INFRAOVERSIZE, .offset = TEMAC_RXOVRL_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_OUTFRAOVERSIZE, .offset = TEMAC_TXOVRL_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_INFRAFRAGMENTS, .offset = TEMAC_RXFRAGL_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_OUTFRAFRAGMENTS, .offset = TEMAC_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_INJABBERS, .offset = TEMAC_STAT_INVALID_OFFSET
    },
    {
        .ifmib = DEVMAN_IF_MIB_OUTJABBERS, .offset = TEMAC_STAT_INVALID_OFFSET
    }
};

static eth_counter_t _temac_counters[] = {
    {
        .name = "rx_bytes_low_cnt", .offset = TEMAC_RXBL_OFFSET
    },
    {
        .name = "rx_bytes_high_cnt", .offset = TEMAC_RXBU_OFFSET
    },
    {
        .name = "tx_bytes_low_cnt", .offset = TEMAC_TXBL_OFFSET
    },
    {
        .name = "tx_bytes_high_cnt", .offset = TEMAC_TXBU_OFFSET
    },
    {
        .name = "rx_packet_undersized_low_cnt", .offset = TEMAC_RXUNDRL_OFFSET
    },
    {
        .name = "rx_packet_undersized_high_cnt", .offset = TEMAC_RXUNDRU_OFFSET
    },
    {
        .name = "rx_packet_frag_low_cnt", .offset = TEMAC_RXFRAGL_OFFSET
    },
    {
        .name = "rx_packet_frag_high_cnt", .offset = TEMAC_RXFRAGU_OFFSET
    },
    {
        .name = "rx_packet_64_bytes_low_cnt", .offset = TEMAC_RX64BL_OFFSET
    },
    {
        .name = "rx_packet_64_bytes_high_cnt", .offset = TEMAC_RX64BU_OFFSET
    },
    {
        .name = "rx_packet_65_127_low_cnt", .offset = TEMAC_RX65B127L_OFFSET
    },
    {
        .name = "rx_packet_65_127_high_cnt", .offset = TEMAC_RX65B127U_OFFSET
    },
    {
        .name = "rx_packet_128_255_low_cnt", .offset = TEMAC_RX128B255L_OFFSET
    },
    {
        .name = "rx_packet_128_255_high_cnt", .offset = TEMAC_RX128B255U_OFFSET
    },
    {
        .name = "rx_packet_256_511_low_cnt", .offset = TEMAC_RX256B511L_OFFSET
    },
    {
        .name = "rx_packet_256_511_high_cnt", .offset = TEMAC_RX256B511U_OFFSET
    },
    {
        .name = "rx_packet_512_1023_low_cnt", .offset = TEMAC_RX512B1023L_OFFSET
    },
    {
        .name = "rx_packet_512_1023_high_cnt", .offset = TEMAC_RX512B1023U_OFFSET
    },
    {
        .name = "rx_packet_1024_max_low_cnt", .offset = TEMAC_RX1024BL_OFFSET
    },
    {
        .name = "rx_packet_1024_max_high_cnt", .offset = TEMAC_RX1024BU_OFFSET
    },
    {
        .name = "rx_packet_oversize_low_cnt", .offset = TEMAC_RXOVRL_OFFSET
    },
    {
        .name = "rx_packet_oversize_high_cnt", .offset = TEMAC_RXOVRU_OFFSET
    },
    {
        .name = "tx_packet_64_bytes_low_cnt", .offset = TEMAC_TX64BL_OFFSET
    },
    {
        .name = "tx_packet_64_bytes_high_cnt", .offset = TEMAC_TX64BU_OFFSET
    },
    {
        .name = "tx_packet_65_127_low_cnt", .offset = TEMAC_TX65B127L_OFFSET
    },
    {
        .name = "tx_packet_65_127_high_cnt", .offset = TEMAC_TX65B127U_OFFSET
    },
    {
        .name = "tx_packet_128_255_low_cnt", .offset = TEMAC_TX128B255L_OFFSET
    },
    {
        .name = "tx_packet_128_255_high_cnt", .offset = TEMAC_TX128B255U_OFFSET
    },
    {
        .name = "tx_packet_256_511_low_cnt", .offset = TEMAC_TX256B511L_OFFSET
    },
    {
        .name = "tx_packet_256_511_high_cnt", .offset = TEMAC_TX256B511U_OFFSET
    },
    {
        .name = "tx_packet_512_1023_low_cnt", .offset = TEMAC_TX512B1023L_OFFSET
    },
    {
        .name = "tx_packet_512_1023_high_cnt", .offset = TEMAC_TX512B1023U_OFFSET
    },
    {
        .name = "tx_packet_1024_max_low_cnt", .offset = TEMAC_TX1024L_OFFSET
    },
    {
        .name = "tx_packet_1024_max_high_cnt", .offset = TEMAC_TX1024U_OFFSET
    },
    {
        .name = "tx_packet_oversize_low_cnt", .offset = TEMAC_TXOVRL_OFFSET
    },
    {
        .name = "tx_packet_oversize_high_cnt", .offset = TEMAC_TXOVRU_OFFSET
    },
    {
        .name = "rx_good_packet_low", .offset = TEMAC_RXFL_OFFSET
    },
    {
        .name = "rx_good_packet_high", .offset = TEMAC_RXFU_OFFSET
    },
    {
        .name = "rx_packet_fcs_err_low", .offset = TEMAC_RXFCSERL_OFFSET
    },
    {
        .name = "rx_packet_fcs_err_high", .offset = TEMAC_RXFCSERU_OFFSET
    },
    {
        .name = "rx_bcast_low", .offset = TEMAC_RXBCSTFL_OFFSET
    },
    {
        .name = "rx_bcast_high", .offset = TEMAC_RXBCSTFU_OFFSET
    },
    {
        .name = "rx_mcast_low", .offset = TEMAC_RXMCSTFL_OFFSET
    },
    {
        .name = "rx_mcast_high", .offset = TEMAC_RXMCSTFU_OFFSET
    },
    {
        .name = "rx_ctrl_low", .offset = TEMAC_RXCTRFL_OFFSET
    },
    {
        .name = "rx_ctrl_high", .offset = TEMAC_RXCTRFU_OFFSET
    },
    {
        .name = "rx_len_err_low", .offset = TEMAC_RXLTERL_OFFSET
    },
    {
        .name = "rx_len_err_high", .offset = TEMAC_RXLTERU_OFFSET
    },
    {
        .name = "rx_vlan_tagged_low", .offset = TEMAC_RXVLANFL_OFFSET
    },
    {
        .name = "rx_vlan_tagged_high", .offset = TEMAC_RXVLANFU_OFFSET
    },
    {
        .name = "rx_pause_low", .offset = TEMAC_RXPFL_OFFSET
    },
    {
        .name = "rx_pause_high", .offset = TEMAC_RXPFU_OFFSET
    },
    {
        .name = "rx_ctrl_unsup_opcode_low", .offset = TEMAC_RXUOPFL_OFFSET
    },
    {
        .name = "rx_ctrl_unsup_opcode_high", .offset = TEMAC_RXUOPFU_OFFSET
    },
    {
        .name = "tx_good_packet_low", .offset = TEMAC_TXFL_OFFSET
    },
    {
        .name = "tx_good_packet_high", .offset = TEMAC_TXFU_OFFSET
    },
    {
        .name = "tx_bcast_low", .offset = TEMAC_TXBCSTFL_OFFSET
    },
    {
        .name = "tx_bcast_high", .offset = TEMAC_TXBCSTFU_OFFSET
    },
    {
        .name = "tx_mcast_low", .offset = TEMAC_TXMCSTFL_OFFSET
    },
    {
        .name = "tx_mcast_high", .offset = TEMAC_TXMCSTFU_OFFSET
    },
    {
        .name = "tx_underrun_err_low", .offset = TEMAC_TXUNDRERL_OFFSET
    },
    {
        .name = "tx_underrun_err_high", .offset = TEMAC_TXUNDRERU_OFFSET
    },
    {
        .name = "tx_ctrl_low", .offset = TEMAC_TXCTRFL_OFFSET
    },
    {
        .name = "tx_ctrl_high", .offset = TEMAC_TXCTRFU_OFFSET
    },
    {
        .name = "tx_vlan_tagged_low", .offset = TEMAC_TXVLANFL_OFFSET
    },
    {
        .name = "tx_vlan_tagged_high", .offset = TEMAC_TXVLANFU_OFFSET
    },
    {
        .name = "tx_pause_low", .offset = TEMAC_TXPFL_OFFSET
    },
    {
        .name = "tx_pause_high", .offset = TEMAC_TXPFU_OFFSET
    },
    {
        .name = "tx_single_collision_low", .offset = TEMAC_TXSCL_OFFSET
    },
    {
        .name = "tx_single_collision_high", .offset = TEMAC_TXSCU_OFFSET
    },
    {
        .name = "tx_multi_collision_low", .offset = TEMAC_TXMCL_OFFSET
    },
    {
        .name = "tx_multi_collision_high", .offset = TEMAC_TXMCU_OFFSET
    },
    {
        .name = "tx_deferred_low", .offset = TEMAC_TXDEFL_OFFSET
    },
    {
        .name = "tx_deferred_high", .offset = TEMAC_TXDEFU_OFFSET
    },
    {
        .name = "tx_late_collision_low", .offset = TEMAC_TXLTCL_OFFSET
    },
    {
        .name = "tx_late_collision_high", .offset = TEMAC_TXLTCU_OFFSET
    },
    {
        .name = "tx_aborted_ex_defferal_low", .offset = TEMAC_TXAECL_OFFSET
    },
    {
        .name = "tx_aborted_ex_defferal_high", .offset = TEMAC_TXAECU_OFFSET
    },
    {
        .name = "tx_ex_defferal_low", .offset = TEMAC_TXEDEFL_OFFSET
    },
    {
        .name = "tx_ex_defferal_high", .offset = TEMAC_TXEDEFU_OFFSET
    },
    {
        .name = "rx_alignment_low", .offset = TEMAC_RXAERL_OFFSET
    },
    {
        .name = "rx_alignment_high", .offset = TEMAC_RXAERU_OFFSET
    },
};

/* temac register read */
static ccl_err_t _temac_reg_read(__attribute__((unused)) void *p_priv, 
                                 b_u32 offset, 
                                 __attribute__((unused)) b_u32 offset_sz, 
                                 b_u32 idx, b_u8 *p_value, b_u32 size)
{
    temac_ctx_t *p_temac;

    if ( offset > TEMAC_MAX_OFFSET || 
         !p_value || idx || size != sizeof(b_u32) ) {
        PDEBUG("Error! Invalid input parameters\n"
               "offset=0x%x, size=%d, p_value=%p, idx=%d\n", 
               offset, size, p_value, idx);
        TEMAC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    PDEBUGG("offset=0x%x, size=%d, p_value=%p, idx=%d\n", 
           offset, size, p_value, idx);

    p_temac = (temac_ctx_t *)p_priv;     
    offset += p_temac->brdspec.offset;
    _ret = devspibus_read_buff(p_temac->brdspec.devname, offset, idx, p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to read over spi bus\n");
        TEMAC_ERROR_RETURN(_ret);
    }    
    PDEBUG("p_temac->brdspec.offset=0x%x, offset=0x%04x, value=0x%08x\n", 
           p_temac->brdspec.offset, offset, *(b_u32 *)p_value);

    return _ret;
}

/* temac register write */
static ccl_err_t _temac_reg_write(__attribute__((unused)) void *p_priv, 
                                  b_u32 offset, 
                                  __attribute__((unused)) b_u32 offset_sz, 
                                  b_u32 idx, b_u8 *p_value, b_u32 size)
{
    temac_ctx_t *p_temac;

    if ( offset > TEMAC_MAX_OFFSET || 
         !p_value || idx || size != sizeof(b_u32) ) {
        PDEBUG("Error! Invalid input parameters\n");
        TEMAC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    PDEBUGG("123$=> offset=0x%03x, value=0x%08x\n", offset, *p_value);

    p_temac = (temac_ctx_t *)p_priv;     
    offset += p_temac->brdspec.offset;
    PDEBUG("p_temac->brdspec.offset=0x%x, offset=0x%04x, value=0x%08x\n", 
           p_temac->brdspec.offset, offset, *(b_u32 *)p_value);

    _ret = devspibus_write_buff(p_temac->brdspec.devname, offset, idx, p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to write over spi bus\n");
        TEMAC_ERROR_RETURN(_ret);
    }    

    return _ret;
}

/**
 * @brief Read 64 bit value from the registers
 *
 * @param[in] address_l is lower address of the register. 
 * @param[out] pvalue is a pointer to hold 48 bit value
 *
 * @return CCL_OK on success, error code on failure
 *
 */
static ccl_err_t _temac_read_64bit_value(temac_ctx_t *p_temac,
                                           b_u32 address_l, b_u64 *pvalue)
{
    b_u32 regval;
    b_u32 address_h = address_l + TEMAC_NUM_WORD_BYTES;

    if ( !p_temac || !pvalue ) {
        PDEBUG("Error! Invalid input parameters\n");
        TEMAC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    TEMAC_REG_READ(p_temac, address_h, 
                        sizeof(b_u32), 0, 
                        (b_u8 *)&regval, sizeof(b_u32));
    *pvalue = (b_u64)(regval << TEMAC_NUM_WORD_BITS);
    TEMAC_REG_READ(p_temac, address_l, 
                        sizeof(b_u32), 0, 
                        (b_u8 *)&regval, sizeof(b_u32));
    PDEBUG("addr_l=0x%08x, val=%d, addr_h=0x%08x, val=%d\n",
           address_l, regval, address_h, (b_u32)*pvalue>>TEMAC_NUM_WORD_BITS);
    *pvalue |= regval;

    return CCL_OK;
}

static ccl_err_t _temac_mtu_get(temac_ctx_t *p_temac, 
                                  b_u8 *pvalue, b_u32 size)
{
    b_u32 regval;
    b_u32 mtu; 

    if ( !p_temac || !pvalue || size != sizeof(b_u32)) {
        PDEBUG("Error! Invalid input parameters\n");
        TEMAC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    TEMAC_REG_READ(p_temac, TEMAC_RXFC_OFFSET, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));

    mtu = (regval & TEMAC_RXFC_MASK);

    memcpy(pvalue, &mtu, sizeof(mtu));

    return CCL_OK;
}

static ccl_err_t _temac_link_status_get(temac_ctx_t *p_temac, 
                                        b_u8 *pvalue, b_u32 size)
{
    b_u32  regval;
    b_u32  reg = TEMAC_PHY_LINK_STATUS_REG;
    b_u32  page = TEMAC_PHY_LINK_STATUS_PAGE;

    if ( !p_temac || !pvalue || size != sizeof(b_u8)) {
        PDEBUG("Error! Invalid input parameters\n");
        TEMAC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    _ret = _temac_phy_read(p_temac, p_temac->brdspec.phy_addr, 
                           reg, page, &regval);
    if ( _ret ) {
        PDEBUG("_temac_phy_read error: phy_addr=%d, reg=0x%04x, page=0x%02x\n", 
               p_temac->brdspec.phy_addr, reg, page);
        TEMAC_ERROR_RETURN(_ret);
    }
    PDEBUG("regval=0x%x\n", regval);
    *pvalue = ((regval & TEMAC_PHY_LINK_STATUS_MASK) >>
               TEMAC_PHY_LINK_STATUS_SHIFT) ? CCL_TRUE : CCL_FALSE;

    return CCL_OK;
}

static ccl_err_t _temac_if_property_get(void *priv, b_u32 prop, 
                                        b_u8 *pvalue, b_u32 size)
{
    temac_ctx_t *p_temac;
    port_stats_t  *port_stats;
    b_u32         address_l;

    if ( !priv || !pvalue) {
        PDEBUG("Error! Invalid input parameters\n");
        TEMAC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_temac = (temac_ctx_t *)priv;

    switch (prop) {
    case DEVMAN_IF_NAME:
        strncpy(pvalue, p_temac->brdspec.name, size);
        break;
    case DEVMAN_IF_MTU:
        _ret = _temac_mtu_get(p_temac, pvalue, size);
        break;
    case DEVMAN_IF_SPEED_MAX:
    case DEVMAN_IF_SPEED:
    case DEVMAN_IF_DUPLEX:
        _ret = CCL_NOT_SUPPORT;
        break;
    case DEVMAN_IF_AUTONEG:
    case DEVMAN_IF_ENCAP:
    case DEVMAN_IF_MEDIUM:
    case DEVMAN_IF_INTERFACE_TYPE:
    case DEVMAN_IF_MDIX:
    case DEVMAN_IF_MDIX_STATUS:
    case DEVMAN_IF_LEARN:
    case DEVMAN_IF_IFILTER:
    case DEVMAN_IF_SELF_EFILTER:
    case DEVMAN_IF_LOCK:
    case DEVMAN_IF_DROP_ON_LOCK:
    case DEVMAN_IF_FWD_UNK:
    case DEVMAN_IF_BCAST_LIMIT:
    case DEVMAN_IF_ABILITY:
    case DEVMAN_IF_FLOWCONTROL:
    case DEVMAN_IF_ENABLE:
    case DEVMAN_IF_SFP_PRESENT:
    case DEVMAN_IF_SFP_TX_STATE:
    case DEVMAN_IF_SFP_RX_STATE:
    case DEVMAN_IF_DEFAULT_VLAN:
    case DEVMAN_IF_PRIORITY:
    case DEVMAN_IF_LED_STATE:
    case DEVMAN_IF_MEDIA_PARAMETERS:
    case DEVMAN_IF_MEDIA_DETAILS:
    case DEVMAN_IF_LOCAL_ADVERT:
    case DEVMAN_IF_MEDIA_TYPE:
    case DEVMAN_IF_SFP_PORT_TYPE:
    case DEVMAN_IF_MEDIA_CONFIG_TYPE:
    case DEVMAN_IF_MEDIA_ERROR_CONFIG_TYPE:
    case DEVMAN_IF_MEDIA_SGMII_FIBER:
    case DEVMAN_IF_TX_ENABLE:
    case DEVMAN_IF_SFP_PORT_NUMBERS:
    case DEVMAN_IF_AUTONEG_DUPLEX:
    case DEVMAN_IF_AUTONEG_SPEED :
    case DEVMAN_IF_DDM_PARAMETERS:
    case DEVMAN_IF_DDM_STATUS:     
    case DEVMAN_IF_DDM_SUPPORTED:  
    case DEVMAN_IF_HWADDRESS_GET:
        break;
    case DEVMAN_IF_ACTIVE_LINK:
        _ret = _temac_link_status_get(p_temac, pvalue, size);
        break;
    default:
        PDEBUG("Error! Invalid property: %d\n", prop);
        TEMAC_ERROR_RETURN(CCL_BAD_PARAM_ERR);          
    }

    return _ret;
}

static ccl_err_t _temac_read_counters(temac_ctx_t *p_temac)
{
    b_u32 i;

    if ( !p_temac ) {
        PDEBUG("Error! Invalid input parameters\n");
        TEMAC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    for (i = 0; i < p_temac->counters_num; i++) {
        TEMAC_REG_READ(p_temac, p_temac->counters[i].offset, 
                       sizeof(b_u32), 0, 
                       (b_u8 *)&p_temac->counters[i].value, 
                       sizeof(b_u32));
    }

    return CCL_OK;
}

static ccl_err_t _temac_read_single_counter(void *priv, b_u32 idx, b_u64 *pvalue)
{
    temac_ctx_t   *p_temac;
    port_stats_t  *port_stats;
    b_u32         address_l;

    if ( !priv || !pvalue) {
        PDEBUG("Error! Invalid input parameters\n");
        TEMAC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_temac = (temac_ctx_t *)priv;
    address_l = _temac_mib_cntr_map[idx].offset;

    if (address_l != TEMAC_STAT_INVALID_OFFSET) {
        _ret = _temac_read_64bit_value(p_temac, address_l, pvalue);
        if ( _ret ) {
            PDEBUG("_temac_read_64bit_value error, address: %u\n",
                   address_l);
            TEMAC_ERROR_RETURN(_ret);
        }
    }

    return CCL_OK;
}

static ccl_err_t _temac_read_stat_counters(void *priv, b_u8 *buf)
{
    temac_ctx_t   *p_temac;
    port_stats_t  *port_stats;

    if ( !priv || !buf) {
        PDEBUG("Error! Invalid input parameters\n");
        TEMAC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_temac = (temac_ctx_t *)priv;
    port_stats = (port_stats_t *)buf;

    /* read counters */

    /* tx bytes */
    _ret = _temac_read_64bit_value(p_temac, TEMAC_TXBL_OFFSET,
                              &port_stats->tx_bytes.value);
    /* tx packets */
    //TODO - need to display total tx packets
    _ret |= _temac_read_64bit_value(p_temac, TEMAC_TXFL_OFFSET,
                              &port_stats->tx_packets.value);
    /* tx packets 64 bytes */
    _ret |= _temac_read_64bit_value(p_temac, TEMAC_TX64BL_OFFSET,
                              &port_stats->tx_packets_64.value);
    /* tx packets 65-127 bytes */
    _ret |= _temac_read_64bit_value(p_temac, TEMAC_TX65B127L_OFFSET,
                              &port_stats->tx_packets_65_127.value);
    /* tx packets 128-255 bytes */
    _ret |= _temac_read_64bit_value(p_temac, TEMAC_TX128B255L_OFFSET,
                              &port_stats->tx_packets_128_255.value);
    /* tx packets 256-511 bytes */
    _ret |= _temac_read_64bit_value(p_temac, TEMAC_TX256B511L_OFFSET,
                              &port_stats->tx_packets_256_511.value);
    /* tx packets 512-1023 bytes */
    _ret |= _temac_read_64bit_value(p_temac, TEMAC_TX512B1023L_OFFSET,
                              &port_stats->tx_packets_512_1023.value);
    /* tx packets 1024-max bytes */
    _ret |= _temac_read_64bit_value(p_temac, TEMAC_TX1024L_OFFSET,
                              &port_stats->tx_packets_1024_max.value);
    /* tx packets broadcast */
    _ret |= _temac_read_64bit_value(p_temac, TEMAC_TXBCSTFL_OFFSET,
                              &port_stats->tx_bcast.value);
    /* tx packets multicast */
    _ret |= _temac_read_64bit_value(p_temac, TEMAC_TXMCSTFL_OFFSET,
                              &port_stats->tx_mcast.value);
    /* tx packets oversize */
    _ret |= _temac_read_64bit_value(p_temac, TEMAC_TXOVRL_OFFSET,
                              &port_stats->tx_packets_oversize.value);
    /* tx packets underrun error */
    _ret |= _temac_read_64bit_value(p_temac, TEMAC_TXUNDRERL_OFFSET,
                              &port_stats->tx_packets_underrun_err.value);
    /* rx bytes */
    _ret |= _temac_read_64bit_value(p_temac, TEMAC_RXBL_OFFSET,
                              &port_stats->rx_bytes.value);
    PDEBUG("rx_bytes: %llu\n", port_stats->rx_bytes.value);
    /* rx packets */
    //TODO - need to display total rx packets
    _ret |= _temac_read_64bit_value(p_temac, TEMAC_RXFL_OFFSET,
                              &port_stats->rx_packets.value);
    /* rx packets 64 bytes */
    _ret |= _temac_read_64bit_value(p_temac, TEMAC_RX64BL_OFFSET,
                              &port_stats->rx_packets_64.value);
    /* rx packets 65-127 bytes */
    _ret |= _temac_read_64bit_value(p_temac, TEMAC_RX65B127L_OFFSET,
                              &port_stats->rx_packets_65_127.value);
    /* rx packets 128-255 bytes */
    _ret |= _temac_read_64bit_value(p_temac, TEMAC_RX128B255L_OFFSET,
                              &port_stats->rx_packets_128_255.value);
    /* rx packets 256-511 bytes */
    _ret |= _temac_read_64bit_value(p_temac, TEMAC_RX256B511L_OFFSET,
                              &port_stats->rx_packets_256_511.value);
    /* rx packets 512-1023 bytes */
    _ret |= _temac_read_64bit_value(p_temac, TEMAC_RX512B1023L_OFFSET,
                              &port_stats->rx_packets_512_1023.value);
    /* rx packets 1024-max bytes */
    _ret |= _temac_read_64bit_value(p_temac, TEMAC_RX1024BL_OFFSET,
                              &port_stats->rx_packets_1024_max.value);
    /* rx packets broadcast */
    _ret |= _temac_read_64bit_value(p_temac, TEMAC_RXBCSTFL_OFFSET,
                              &port_stats->rx_bcast.value);
    /* rx packets multicast */
    _ret |= _temac_read_64bit_value(p_temac, TEMAC_RXMCSTFL_OFFSET,
                              &port_stats->rx_mcast.value);
    /* rx packets oversize*/
    _ret |= _temac_read_64bit_value(p_temac, TEMAC_RXOVRL_OFFSET,
                              &port_stats->rx_packets_oversize.value);
    /* rx packets undersize*/
    _ret |= _temac_read_64bit_value(p_temac, TEMAC_RXUNDRL_OFFSET,
                              &port_stats->rx_packets_undersize.value);
    /* rx packets fragmented */
    _ret |= _temac_read_64bit_value(p_temac, TEMAC_RXFRAGL_OFFSET,
                              &port_stats->rx_packets_frag.value);
    /* rx packets FCS error */
    _ret |= _temac_read_64bit_value(p_temac, TEMAC_RXFCSERL_OFFSET,
                              &port_stats->rx_packets_fcs_err.value);
    if ( _ret ) {
        PDEBUG("_temac_read_64bit_value error\n");
        TEMAC_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

static ccl_err_t _temac_configure_test(void *p_priv)
{
    temac_ctx_t   *p_temac;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        TEMAC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_temac = (temac_ctx_t *)p_priv;

    return CCL_OK;
}

static ccl_err_t _temac_run_test(void *p_priv, 
                                 __attribute__((unused)) b_u8 *buf, 
                                 __attribute__((unused)) b_u32 size)
{
    temac_ctx_t   *p_temac;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        TEMAC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_temac = (temac_ctx_t *)p_priv;

    return CCL_OK;
}

ccl_err_t temac_attr_read(void *p_priv, b_u32 attrid, 
                             b_u32 offset, b_u32 offset_sz, 
                             b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || offset > TEMAC_MAX_OFFSET || !p_value ) {
        PDEBUG("Error! Invalid input parameters\n");
        TEMAC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch (attrid) {
    case eTEMAC_RXBL:                 
    case eTEMAC_TXBL:                 
    case eTEMAC_RXUNDRL:              
    case eTEMAC_RXFRAGL:              
    case eTEMAC_RX64BL:               
    case eTEMAC_RX65B127L:            
    case eTEMAC_RX128B255L:           
    case eTEMAC_RX256B511L:           
    case eTEMAC_RX512B1023L:          
    case eTEMAC_RX1024BL:             
    case eTEMAC_RXOVRL:               
    case eTEMAC_TX64BL:               
    case eTEMAC_TX65B127L:            
    case eTEMAC_TX128B255L:           
    case eTEMAC_TX256B511L:           
    case eTEMAC_TX512B1023L:          
    case eTEMAC_TX1024L:              
    case eTEMAC_TXOVRL:               
    case eTEMAC_RXFL:                 
    case eTEMAC_RXFCSERL:             
    case eTEMAC_RXBCSTFL:             
    case eTEMAC_RXMCSTFL:             
    case eTEMAC_RXCTRFL:              
    case eTEMAC_RXLTERL:              
    case eTEMAC_RXVLANFL:             
    case eTEMAC_RXPFL:                
    case eTEMAC_RXUOPFL:              
    case eTEMAC_TXFL:                 
    case eTEMAC_TXBCSTFL:             
    case eTEMAC_TXMCSTFL:             
    case eTEMAC_TXUNDRERL:            
    case eTEMAC_TXCTRFL:              
    case eTEMAC_TXVLANFL:             
    case eTEMAC_TXPFL:                
    case eTEMAC_TXSCL:                
    case eTEMAC_TXMCL:                
    case eTEMAC_TXDEF:                
    case eTEMAC_TXLTCL:               
    case eTEMAC_TXAECL:               
    case eTEMAC_TXEDEFL:              
    case eTEMAC_RXAERL:
    case eTEMAC_STAT_SINGLE_COUNTER:
        _ret = _temac_read_single_counter(p_priv, idx, (b_u64*)p_value);
        break;                      
    case eTEMAC_RCW0:                 
    case eTEMAC_RCW1:
    case eTEMAC_TC:                   
    case eTEMAC_FCC:                  
    case eTEMAC_MSC:                 
    case eTEMAC_RXFC:                 
    case eTEMAC_TXFC:                 
    case eTEMAC_TX_TIMESTAMP_ADJ:     
    case eTEMAC_PHYC:                 
    case eTEMAC_IDREG:                
    case eTEMAC_ARREG:                
    case eTEMAC_MDIO_MC:              
    case eTEMAC_MDIO_MCR:             
    case eTEMAC_MDIO_MWD:             
    case eTEMAC_MDIO_MRD:             
    case eTEMAC_IS:             
    case eTEMAC_IP:             
    case eTEMAC_IE:             
        TEMAC_REG_READ(p_priv, offset, offset_sz, idx, p_value, size);
        break;
    case eTEMAC_IF_PROPERTY:
        _ret = _temac_if_property_get(p_priv, idx, p_value, size);
        break;
    case eTEMAC_STAT_COUNTERS:
        _ret = _temac_read_stat_counters(p_priv, p_value);
        break;
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK) 
        PDEBUG("Failure in attribute id: %d\n", attrid);

    return _ret;               
}

ccl_err_t temac_attr_write(void *p_priv, b_u32 attrid, 
                             b_u32 offset, b_u32 offset_sz, 
                             b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || offset > TEMAC_MAX_OFFSET || !p_value ) {
        PDEBUG("Error! Invalid input parameters\n");
        TEMAC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch (attrid) {
    case eTEMAC_RXBL:                 
    case eTEMAC_TXBL:                 
    case eTEMAC_RXUNDRL:              
    case eTEMAC_RXFRAGL:              
    case eTEMAC_RX64BL:               
    case eTEMAC_RX65B127L:            
    case eTEMAC_RX128B255L:           
    case eTEMAC_RX256B511L:           
    case eTEMAC_RX512B1023L:          
    case eTEMAC_RX1024BL:             
    case eTEMAC_RXOVRL:               
    case eTEMAC_TX64BL:               
    case eTEMAC_TX65B127L:            
    case eTEMAC_TX128B255L:           
    case eTEMAC_TX256B511L:           
    case eTEMAC_TX512B1023L:          
    case eTEMAC_TX1024L:              
    case eTEMAC_TXOVRL:               
    case eTEMAC_RXFL:                 
    case eTEMAC_RXFCSERL:             
    case eTEMAC_RXBCSTFL:             
    case eTEMAC_RXMCSTFL:             
    case eTEMAC_RXCTRFL:              
    case eTEMAC_RXLTERL:              
    case eTEMAC_RXVLANFL:             
    case eTEMAC_RXPFL:                
    case eTEMAC_RXUOPFL:              
    case eTEMAC_TXFL:                 
    case eTEMAC_TXBCSTFL:             
    case eTEMAC_TXMCSTFL:             
    case eTEMAC_TXUNDRERL:            
    case eTEMAC_TXCTRFL:              
    case eTEMAC_TXVLANFL:             
    case eTEMAC_TXPFL:                
    case eTEMAC_TXSCL:                
    case eTEMAC_TXMCL:                
    case eTEMAC_TXDEF:                
    case eTEMAC_TXLTCL:               
    case eTEMAC_TXAECL:               
    case eTEMAC_TXEDEFL:              
    case eTEMAC_RXAERL:               
    case eTEMAC_RCW0:                 
    case eTEMAC_RCW1:
    case eTEMAC_TC:                   
    case eTEMAC_FCC:                  
    case eTEMAC_MSC:                 
    case eTEMAC_RXFC:                 
    case eTEMAC_TXFC:                 
    case eTEMAC_TX_TIMESTAMP_ADJ:     
    case eTEMAC_PHYC:                 
    case eTEMAC_IDREG:                
    case eTEMAC_ARREG:                
    case eTEMAC_MDIO_MC:              
    case eTEMAC_MDIO_MCR:             
    case eTEMAC_MDIO_MWD:             
    case eTEMAC_MDIO_MRD:             
    case eTEMAC_IE:             
    case eTEMAC_IC:             
        TEMAC_REG_WRITE(p_priv, offset, offset_sz, idx, p_value, size);
        break;
    case eTEMAC_INIT:
        _ret = _temac_init(p_priv);
        break;
    case eTEMAC_CONFIGURE_TEST:
        _ret = _temac_configure_test(p_priv);
        break;
    case eTEMAC_RUN_TEST:
        _ret = _temac_run_test(p_priv, p_value, size);
        break;
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK) 
        PDEBUG("Failure in attribute id: %d\n", attrid);

    return _ret;
}

/**
 * Starts the TEMAC device as follows:
 *  - Enable transmitter
 *	- Enable receiver
 * @param[in] p_temac is a pointer to temac_ctx_t instance to 
 *       be worked on
 * 
 * @return error code
 */
static ccl_err_t _temac_start(temac_ctx_t *p_temac)
{
    b_u32 regval;
    b_u32 reg;
    b_u32 page;

    if ( !p_temac ) {
        PDEBUG("Error! Invalid input parameters\n");
        TEMAC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* Configure operational mode to SGMII for SGMII interface*/
    if (p_temac->brdspec.iftype == eDEVBRD_IFTYPE_SGMII) {
        reg = 20;
        page = 18;
        regval = 0x1;
        _ret = _temac_phy_write(p_temac, p_temac->brdspec.phy_addr, 
                                reg, page, regval);
        regval = 0x8001;
        _ret |= _temac_phy_write(p_temac, p_temac->brdspec.phy_addr, 
                                 reg, page, regval);
        if ( _ret ) {
            PDEBUG("_temac_phy_write error: phy_addr=%d, reg=0x%04x, page=0x%02x, value=0x%x\n", 
                   p_temac->brdspec.phy_addr, reg, page, regval);
            TEMAC_ERROR_RETURN(_ret);
        }
    }

    /* Enable transmitter if not already enabled */
    PDEBUG( "enabling transmitter\n");
    TEMAC_REG_READ(p_temac, TEMAC_TC_OFFSET, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));
    if (!(regval & TEMAC_TC_TX_MASK)) {
        PDEBUG("transmitter not enabled, enabling now\n");
        regval |= TEMAC_TC_TX_MASK;
        TEMAC_REG_WRITE(p_temac, TEMAC_TC_OFFSET, 
                           sizeof(b_u32), 0, 
                           (b_u8 *)&regval, sizeof(b_u32));
    }
    PDEBUG("transmitter enabled\n");

    /* Enable receiver */
    PDEBUG("enabling receiver\n");
    TEMAC_REG_READ(p_temac, TEMAC_RCW1_OFFSET, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));
    if (!(regval & TEMAC_RCW1_RX_MASK)) {
        PDEBUG("receiver not enabled, enabling now\n");
        regval |= TEMAC_RCW1_RX_MASK;
        TEMAC_REG_WRITE(p_temac, TEMAC_RCW1_OFFSET, 
                           sizeof(b_u32), 0, 
                           (b_u8 *)&regval, sizeof(b_u32));
    }
    PDEBUG( "receiver enabled\n");

    return CCL_OK;
}

/**
 * Gracefully stops the TEMAC device as follows: 
 *	- Disable all interrupts from this device
 *	- Disable the receiver
 *
 *  _temac_stop does not modify any of the current device
 *  options.
 *
 * Since the transmitter is not disabled, frames currently in internal buffers
 * or in process by a DMA engine are allowed to be transmitted.
 *
 * @param[in] p_temac is a pointer to temac_ctx_t instance to 
 *       be worked on
 * 
 * @return	error code
 */
static ccl_err_t _temac_stop(temac_ctx_t *p_temac)
{
    b_u32 regval;

    if ( !p_temac ) {
        PDEBUG("Error! Invalid input parameters\n");
        TEMAC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* Disable interrupts */
    regval = 0;
    TEMAC_REG_WRITE(p_temac, TEMAC_IE_OFFSET, 
                       sizeof(b_u32), 0, 
                       (b_u8 *)&regval, sizeof(b_u32));

    /* Disable the receiver */
    TEMAC_REG_READ(p_temac, TEMAC_RCW1_OFFSET, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));
    regval &= ~TEMAC_RCW1_RX_MASK;
    TEMAC_REG_WRITE(p_temac, TEMAC_RCW1_OFFSET, 
                       sizeof(b_u32), 0, 
                       (b_u8 *)&regval, sizeof(b_u32));

    /*
     * Stopping the receiver in mid-packet causes a dropped packet
     * indication from HW. Clear it.
     */
    /* get the interrupt pending register */
    TEMAC_REG_READ(p_temac, TEMAC_IP_OFFSET, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));
    if (regval & TEMAC_INT_RXRJECT_MASK) {
        /* set the interrupt status register to clear the interrupt */
        regval = TEMAC_INT_RXRJECT_MASK;
        TEMAC_REG_WRITE(p_temac, TEMAC_IS_OFFSET, 
                           sizeof(b_u32), 0, 
                           (b_u8 *)&regval, sizeof(b_u32));
    }

    return CCL_OK;
}

/**
 * _temac_update_depops check and update dependent options 
 * for new/extended features. This is a helper function that is 
 * meant to be called by _temac_set_options() and 
 * _temac_clear_options(). 
 *
 * @param[in] p_temac is a pointer to the TEMAC 
 *      instance to be worked on.
 *
 * @return	Dependent options that are required to set/clear per
 *		hardware requirement.
 *
 * @note
 *
 * This helper function collects the dependent OPTION(s) per hardware design.
 * When conflicts arises, extended features have precedence over legacy ones.
 * Two operations to be considered,
 * 1. Adding extended options. If XATE_VLAN_OPTION is enabled and enable one of
 *	extended VLAN options, XATE_VLAN_OPTION should be off and configure to
 *	hardware.
 *	However, axi-ethernet instance options variable still holds
 *	XATE_VLAN_OPTION so when all of the extended feature are removed,
 *	XATE_VLAN_OPTION can be effective and configured to hardware.
 * 2. Removing extended options. Remove extended option can not just remove
 *	the selected extended option and dependent options. All extended
 *	options need to be verified and remained when one or more extended
 *	options are enabled.
 *
 * Dependent options are :
 *	- TEMAC_VLAN_OPTION,
 *	- TEMAC_JUMBO_OPTION
 *	- TEMAC_FCS_INSERT_OPTION,
 *	- TEMAC_FCS_STRIP_OPTION
 *	- TEMAC_PROMISC_OPTION.
 */
static b_u32 _temac_update_depops(temac_ctx_t *p_temac)
{
    if ( !p_temac ) {
        PDEBUG("Error! Invalid input parameters\n");
        TEMAC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    b_u32 depops = p_temac->options;

    /*
     * The extended/new features require some OPTIONS to be on/off per
     * hardware design. We determine these extended/new functions here
     * first and also on/off other OPTIONS later. So that dependent
     * OPTIONS are in sync and _[Set|Clear]options() can be performed
     * seamlessly.
     */


    /* Enable extended transmit VLAN translation option */
    if (depops & TEMAC_EXT_TXVLAN_TRAN_OPTION) {
        /*
         * Check if hardware is built with extend TX VLAN translation.
         * if not, output an information message.
         */
        if (TEMAC_IS_TX_VLAN_TRAN(p_temac)) {
            depops |= TEMAC_FCS_INSERT_OPTION;
            depops &= ~TEMAC_VLAN_OPTION;
            PDEBUG("Checkdepops: enabling ext Tx VLAN trans\n");
        } else {
            PDEBUG("TX VLAN TRANSLATION is not built in hardware\n");
        }
    }

    /* Enable extended receive VLAN translation option */
    if (depops & TEMAC_EXT_RXVLAN_TRAN_OPTION) {
        /*
         * Check if hardware is built with extend RX VLAN translation.
         * if not, output an information message.
         */
        if (TEMAC_IS_RX_VLAN_TRAN(p_temac)) {
            depops |= TEMAC_FCS_STRIP_OPTION;
            depops &= ~TEMAC_VLAN_OPTION;
            PDEBUG("Checkdepops: enabling ext Rx VLAN trans\n");
        } else {
            PDEBUG("RX VLAN TRANSLATION is not built in hardware\n");
        }
    }

    /* Enable extended transmit VLAN tag option */
    if (depops & TEMAC_EXT_TXVLAN_TAG_OPTION) {
        /*
         * Check if hardware is built with extend TX VLAN tagging.
         * if not, output an information message.
         */
        if (TEMAC_IS_TX_VLAN_TAG(p_temac)) {
            depops |= TEMAC_FCS_INSERT_OPTION;
            depops &= ~TEMAC_VLAN_OPTION;
            depops |= TEMAC_JUMBO_OPTION;
            PDEBUG("Checkdepops: enabling ext Tx VLAN tag\n");
        } else {
            PDEBUG("TX VLAN TAG is not built in hardware\n");
        }
    }

    /* Enable extended receive VLAN tag option */
    if (depops & TEMAC_EXT_RXVLAN_TAG_OPTION) {
        /*
         * Check if hardware is built with extend RX VLAN tagging.
         * if not, output an information message.
         */
        if (TEMAC_IS_RX_VLAN_TAG(p_temac)) {
            depops |= TEMAC_FCS_STRIP_OPTION;
            depops &= ~TEMAC_VLAN_OPTION;
            depops |= TEMAC_JUMBO_OPTION;
            PDEBUG("Checkdepops: enabling ext Rx VLAN tag\n");
        } else {
            PDEBUG("RX VLAN TAG is not built in hardware\n");
        }
    }

    /* Enable extended transmit VLAN strip option */
    if (depops & TEMAC_EXT_TXVLAN_STRP_OPTION) {
        /*
         * Check if hardware is built with extend TX VLAN stripping.
         * if not, output an information message.
         */
        if (TEMAC_IS_TX_VLAN_STRP(p_temac)) {
            depops |= TEMAC_FCS_INSERT_OPTION;
            depops &= ~TEMAC_VLAN_OPTION;
            depops |= TEMAC_JUMBO_OPTION;
            PDEBUG("Checkdepops: enabling ext Tx VLAN strip\n");
        } else {
            PDEBUG("TX VLAN STRIP is not built in hardware\n");
        }
    }

    /* Enable extended receive VLAN strip option */
    if (depops & TEMAC_EXT_RXVLAN_STRP_OPTION) {
        /*
         * Check if hardware is built with extend RX VLAN stripping.
         * if not, output an information message.
         */
        if (TEMAC_IS_RX_VLAN_STRP(p_temac)) {
            depops |= TEMAC_FCS_STRIP_OPTION;
            depops &= ~TEMAC_VLAN_OPTION;
            depops |= TEMAC_JUMBO_OPTION;
            PDEBUG("Checkdepops: enabling ext Rx VLAN strip\n");
        } else {
            PDEBUG("RX VLAN STRIP is not built in hardware\n");
        }
    }

    /*
     * options and dependent options together is what hardware and user
     * are happy with. But we still need to keep original options
     * in case option(s) are set/cleared, overall options can be managed.
     * Return depops to TEMAC_[Set|Clear]options for final
     * configuration.
     */
    return depops;
}

/**
 * _temac_set_options enables the options for 
 *  the TEMAC, specified by p_temac. Axi
 * Ethernet should be stopped with _temac_stop() before 
 * changing options. 
 *
 * @param[in] p_temac is a pointer to temac_ctx_t instance to 
 *       be worked on
 * @param[in] options is a bitmask of OR'd values for options to 
 *      set. Options not specified are not affected.
 *
 * @return error code
 */
static ccl_err_t _temac_set_options(temac_ctx_t *p_temac, b_u32 options)
{
    b_u32 regval;   /* Generic register contents */
    b_u32 rcw1; /* Reflects original contents of RCW1 */
    b_u32 tc;   /* Reflects original contents of TC  */
    b_u32 newrcw1;  /* Reflects new contents of RCW1 */
    b_u32 newtc;    /* Reflects new contents of TC  */
    b_u32 depops;   /* Required dependent options for new features */

    if ( !p_temac ) {
        PDEBUG("Error! Invalid input parameters\n");
        TEMAC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Set options word to its new value.
     * The step is required before calling _Updatedepops() since
     * we are operating on updated options.
     */
    p_temac->options |= options;

    /*
     * There are options required to be on/off per hardware requirement.
     * Invoke _Updatedepops to check hardware availability and update
     * options accordingly.
     */
    depops = _temac_update_depops(p_temac);

    /*
     * Many of these options will change the RCW1 or TC registers.
     * To reduce the amount of IO to the device, group these options here
     * and change them all at once.
     */
    /* Get current register contents */
    TEMAC_REG_READ(p_temac, TEMAC_RCW1_OFFSET, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&rcw1, sizeof(b_u32));
    TEMAC_REG_READ(p_temac, TEMAC_TC_OFFSET, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&tc, sizeof(b_u32));

    newrcw1 = rcw1;
    newtc = tc;

    PDEBUG("current control regs: RCW1: 0x%0x; TC: 0x%0x\n",rcw1, tc);
    PDEBUG("options: 0x%0x; default options: 0x%0x\n",options, TEMAC_DEFAULT_OPTIONS);

    /* Turn on jumbo packet support for both Rx and Tx */
    if (depops & TEMAC_JUMBO_OPTION) {
        newtc |= TEMAC_TC_JUM_MASK;
        newrcw1 |= TEMAC_RCW1_JUM_MASK;
    }

    /* Turn on VLAN packet support for both Rx and Tx */
    if (depops & TEMAC_VLAN_OPTION) {
        newtc |= TEMAC_TC_VLAN_MASK;
        newrcw1 |= TEMAC_RCW1_VLAN_MASK;
    }

    /* Turn on FCS stripping on receive packets */
    if (depops & TEMAC_FCS_STRIP_OPTION) {
        PDEBUG("setoptions: enabling fcs stripping\n");
        newrcw1 &= ~TEMAC_RCW1_FCS_MASK;
    }

    /* Turn on FCS insertion on transmit packets */
    if (depops & TEMAC_FCS_INSERT_OPTION) {
        PDEBUG("setoptions: enabling fcs insertion\n");
        newtc &= ~TEMAC_TC_FCS_MASK;
    }

    /* Turn on length/type field checking on receive packets */
    if (depops & TEMAC_LENTYPE_ERR_OPTION) {
        newrcw1 &= ~TEMAC_RCW1_LT_DIS_MASK;
    }

    /* Enable transmitter */
    if (depops & TEMAC_TRANSMITTER_ENABLE_OPTION) {
        newtc |= TEMAC_TC_TX_MASK;
    }

    /* Enable receiver */
    if (depops & TEMAC_RECEIVER_ENABLE_OPTION) {
        newrcw1 |= TEMAC_RCW1_RX_MASK;
    }

    /* Change the TC or RCW1 registers if they need to be modified */
    if (tc != newtc) {
        PDEBUG("setoptions: writing tc: 0x%0x\n", newtc);
        TEMAC_REG_WRITE(p_temac, TEMAC_TC_OFFSET, 
                           sizeof(b_u32), 0, 
                           (b_u8 *)&newtc, sizeof(b_u32));
    }

    if (rcw1 != newrcw1) {
        PDEBUG("setoptions: writing rcw1: 0x%0x\n", newrcw1);
        TEMAC_REG_WRITE(p_temac, TEMAC_RCW1_OFFSET, 
                           sizeof(b_u32), 0, 
                           (b_u8 *)&newrcw1, sizeof(b_u32));
    }

    /*
     * Rest of options twiddle bits of other registers. Handle them one at
     * a time
     */

    /* Turn on flow control */
    if (depops & TEMAC_FLOW_CONTROL_OPTION) {
        PDEBUG("setoptions: enabling flow control\n");
        TEMAC_REG_READ(p_temac, TEMAC_FCC_OFFSET, 
                          sizeof(b_u32), 0, 
                          (b_u8 *)&regval, sizeof(b_u32));
        regval |= TEMAC_FCC_FCRX_MASK;
        TEMAC_REG_WRITE(p_temac, TEMAC_FCC_OFFSET, 
                           sizeof(b_u32), 0, 
                           (b_u8 *)&regval, sizeof(b_u32));
    }

    /*
     * The remaining options not handled here are managed elsewhere in the
     * driver. No register modifications are needed at this time.
     * Reflecting the option in p_temac->options is good enough for
     * now.
     */
    PDEBUG("setoptions: returning SUCCESS\n");
    return CCL_OK;
}

/**
 * _temac_clear_options clears the options, <i>options</i> for 
 * the TEMAC, specified by <i>p_temac</i>. TEMAC
 * should be stopped with _temac_stop() before changing 
 * options. 
 *
 * @param[in] p_temac is a pointer to temac_ctx_t instance to 
 *       be worked on
 * @param[in] options is a bitmask of OR'd TEMAC_*_OPTION 
 *      values for options to clear. options not specified are
 *      not affected.
 *
 * @return error code
 */
static ccl_err_t _temac_clear_options(temac_ctx_t *p_temac, b_u32 options)
{
    b_u32 regval;   /* Generic */
    b_u32 rcw1; /* Reflects original contents of RCW1 */
    b_u32 tc;   /* Reflects original contents of TC  */
    b_u32 newrcw1;  /* Reflects new contents of RCW1 */
    b_u32 newtc;    /* Reflects new contents of TC  */
    b_u32 depops;   /* Required dependent options for new features */

    if ( !p_temac ) {
        PDEBUG("Error! Invalid input parameters\n");
        TEMAC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Set options word to its new value.
     * The step is required before calling _Updatedepops() since
     * we are operating on updated options.
     */
    p_temac->options &= ~options;

    /*
     * There are options required to be on/off per hardware requirement.
     * Invoke _Updatedepops to check hardware availability and update
     * options accordingly.
     */
    depops = ~(_temac_update_depops(p_temac));

    /*
     * Many of these options will change the RCW1 or TC registers.
     * Group these options here and change them all at once. What we are
     * trying to accomplish is to reduce the amount of IO to the device
     */

    /* Get the current register contents */
    TEMAC_REG_READ(p_temac, TEMAC_RCW1_OFFSET, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&rcw1, sizeof(b_u32));
    TEMAC_REG_READ(p_temac, TEMAC_TC_OFFSET, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&tc, sizeof(b_u32));
    newrcw1 = rcw1;
    newtc = tc;
    PDEBUG("XAxiEthernet_Clearoptions: newrcw1=%x, newtc=%x\n",
           newrcw1, newtc);
    /* Turn off jumbo packet support for both Rx and Tx */
    if (depops & TEMAC_JUMBO_OPTION) {
        PDEBUG("XAxiEthernet_Clearoptions: disabling jumbo\n");
        newtc &= ~TEMAC_TC_JUM_MASK;
        newrcw1 &= ~TEMAC_RCW1_JUM_MASK;
    }

    /* Turn off VLAN packet support for both Rx and Tx */
    if (depops & TEMAC_VLAN_OPTION) {
        PDEBUG("XAxiEthernet_Clearoptions: disabling vlan\n");
        newtc &= ~TEMAC_TC_VLAN_MASK;
        newrcw1 &= ~TEMAC_RCW1_VLAN_MASK;
    }

    /* Turn off FCS stripping on receive packets */
    if (depops & TEMAC_FCS_STRIP_OPTION) {
        PDEBUG("XAxiEthernet_Clearoptions: disabling fcs strip\n");
        newrcw1 |= TEMAC_RCW1_FCS_MASK;
    }

    /* Turn off FCS insertion on transmit packets */
    if (depops & TEMAC_FCS_INSERT_OPTION) {
        PDEBUG("XAxiEthernet_Clearoptions: disabling fcs insert\n");
        newtc |= TEMAC_TC_FCS_MASK;
    }

    /* Turn off length/type field checking on receive packets */
    if (depops & TEMAC_LENTYPE_ERR_OPTION) {
        PDEBUG("XAxiEthernet_Clearoptions: disabling lentype err\n");
        newrcw1 |= TEMAC_RCW1_LT_DIS_MASK;
    }

    /* Disable transmitter */
    if (depops & TEMAC_TRANSMITTER_ENABLE_OPTION) {
        PDEBUG("XAxiEthernet_Clearoptions: disabling transmitter\n");
        newtc &= ~TEMAC_TC_TX_MASK;
    }

    /* Disable receiver */
    if (depops & TEMAC_RECEIVER_ENABLE_OPTION) {
        PDEBUG("XAxiEthernet_Clearoptions: disabling receiver\n");
        newrcw1 &= ~TEMAC_RCW1_RX_MASK;
    }

    /* Change the TC and RCW1 registers if they need to be
     * modified
     */
    if (tc != newtc) {
        PDEBUG("XAxiEthernet_Clearoptions: setting TC: 0x%0x\n", newtc);
        TEMAC_REG_WRITE(p_temac, TEMAC_TC_OFFSET, 
                           sizeof(b_u32), 0, 
                           (b_u8 *)&newtc, sizeof(b_u32));
    }

    if (rcw1 != newrcw1) {
        PDEBUG("XAxiEthernet_Clearoptions: setting RCW1: 0x%0x\n",newrcw1);
        TEMAC_REG_WRITE(p_temac, TEMAC_RCW1_OFFSET, 
                           sizeof(b_u32), 0, 
                           (b_u8 *)&newrcw1, sizeof(b_u32));
    }

    /*
     * Rest of options twiddle bits of other registers. Handle them one at
     * a time
     */

    /* Turn off flow control */
    if (depops & TEMAC_FLOW_CONTROL_OPTION) {
        PDEBUG("XAxiEthernet_Clearoptions: disabling flow control\n");
        TEMAC_REG_READ(p_temac, TEMAC_FCC_OFFSET, 
                          sizeof(b_u32), 0, 
                          (b_u8 *)&regval, sizeof(b_u32));
        regval &= ~TEMAC_FCC_FCRX_MASK;
        TEMAC_REG_WRITE(p_temac, TEMAC_FCC_OFFSET, 
                           sizeof(b_u32), 0, 
                           (b_u8 *)&regval, sizeof(b_u32));
    }

    /*
     * The remaining options not handled here are managed elsewhere in the
     * driver. No register modifications are needed at this time.
     * Reflecting the option in p_temac->options is good enough for
     * now.
     */
    PDEBUG( "Clearoptions: returning SUCCESS\n");
    return CCL_OK;
}

/**
 * _temac_phy_set_mdio_divisor sets the MDIO clock divisor in the
 * TEMAC,specified by <i>p_temac</i> to the value, <i>Divisor</i>.
 * This function must be called once after each reset prior to accessing
 * MII PHY registers.
 *
 * From the Virtex-6(TM) and Spartan-6 (TM) Embedded Tri-Mode Ethernet
 * MAC User's Guide, the following equation governs the MDIO clock to the PHY:
 *
 * <pre>
 * 			f[HOSTCLK]
 *	f[MDC] = -----------------------
 *			(1 + Divisor) * 2
 * </pre>
 *
 * where f[HOSTCLK] is the bus clock frequency in MHz, and f[MDC] is the
 * MDIO clock frequency in MHz to the PHY. Typically, f[MDC] should not
 * exceed 2.5 MHz. Some PHYs can tolerate faster speeds which means faster
 * access.
 *
 * @param[in] p_temac references the temac_ctx_t instance 
 *      on which to operate.
 * @param[in] Divisor is the divisor value to set within the 
 *      range of 0 to TEMAC_MDIO_MC_CLK_DVD_MAX.
 *
 * @return error code
 */
static ccl_err_t _temac_phy_set_mdio_divisor(temac_ctx_t *p_temac, b_u8 divisor)
{
    b_u32 regval;

    if ( !p_temac || (divisor > TEMAC_MDIO_MC_CLOCK_DIVIDE_MAX)) {
        PDEBUG("Error! Invalid input parameters\n");
        TEMAC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    regval = divisor | TEMAC_MDIO_MC_MDIOEN_MASK;
    TEMAC_REG_WRITE(p_temac, TEMAC_MDIO_MC_OFFSET, 
                       sizeof(b_u32), 0, 
                       (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/**
 * _temac_set_operating_speed sets the current operating link 
 * speed. For any traffic to be passed, this speed must match 
 * the current MII/GMII/SGMII/RGMII link speed. 
 *
 * @param[in] p_temac references the TEMAC on which 
 *      to operate.
 * @param[in] speed is the speed to set in units of Mbps.
 *		Valid values are 10, 100, or 1000.
 *
 * @return	error code
 */
static ccl_err_t _temac_set_operating_speed(temac_ctx_t *p_temac, b_u16 speed)
{
    b_u32 msc;
    b_u8  phy_type;
    b_bool set_speed = CCL_TRUE;

    if ( !p_temac || 
         ((speed != TEMAC_SPEED_10_MBPS) && 
          (speed != TEMAC_SPEED_100_MBPS) && 
          (speed != TEMAC_SPEED_1000_MBPS) && 
          (speed != TEMAC_SPEED_2500_MBPS))) {
        PDEBUG("Error! Invalid input parameters\n");
        TEMAC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    phy_type = TEMAC_GET_PHYSICAL_IF(p_temac);

    /*
     * The following code checks for all allowable speed conditions before
     * writing to the register. Please refer to the hardware specs for
     * more information on it.
     * For PHY type 1000Base-x, 10 and 100 Mbps are not supported.
     * For soft/hard TEMAC core, 1000 Mbps is supported in all PHY
     * types except MII.
     */
    if ((speed == TEMAC_SPEED_10_MBPS) || (speed == TEMAC_SPEED_100_MBPS)) {
        if (phy_type == TEMAC_PHY_TYPE_1000BASE_X) {
            set_speed = CCL_FALSE;
        }
    } else {
        if ((speed == TEMAC_SPEED_1000_MBPS) &&
            (phy_type == TEMAC_PHY_TYPE_MII)) {
            set_speed = CCL_FALSE;
        }
    }

    if (set_speed == CCL_TRUE) {
        /*
         * Get the current contents of the EMAC config register and
         * zero out speed bits
         */
        TEMAC_REG_READ(p_temac, TEMAC_MSC_OFFSET, 
                          sizeof(b_u32), 0, 
                          (b_u8 *)&msc, sizeof(b_u32));
        msc &= ~TEMAC_MSC_LINKSPEED_MASK;

        switch (speed) {
        case TEMAC_SPEED_10_MBPS:
            break;

        case TEMAC_SPEED_100_MBPS:
            msc |= TEMAC_MSC_LINKSPD_100;
            break;

        case TEMAC_SPEED_1000_MBPS:
            msc |= TEMAC_MSC_LINKSPD_1000;
            break;

        case TEMAC_SPEED_2500_MBPS:
            msc |= TEMAC_MSC_LINKSPD_1000;
            break;

        default:
            return CCL_FAIL;
        }

        PDEBUG("_temac_set_operating_speed: new speed: 0x%0x\n", msc);
        /* Set register and return */
        TEMAC_REG_WRITE(p_temac, TEMAC_MSC_OFFSET, 
                           sizeof(b_u32), 0, 
                           (b_u8 *)&msc, sizeof(b_u32));
        PDEBUG("_temac_set_operating_speed: done\n");
        return CCL_OK;
    } else {
        PDEBUG("speed not compatible with the TEMAC Phy type\n");
        return CCL_FAIL;
    }
}

/**
 * _temac_get_operating_speed gets the current operating link speed. This
 * may be the value set by _temac_set_operating_speed() or a 
 * hardware default. 
 *
 * @param[in]	p_temac is a pointer to the temac_ctx_t instance to be
 *      worked on.
 * @param[out] p_speed current operating link speed 
 *
 * @return	error code
 *
 * @note
 * Returns the link speed in units of megabits per second (10 /
 * 100 / 1000).
 * Can return a value of 0, in case it does not get a valid
 * speed from MSC.
 */
static ccl_err_t _temac_get_operating_speed(temac_ctx_t *p_temac, b_u16 *p_speed)
{
    b_u32 regval;

    if ( !p_temac || !p_speed) {
        PDEBUG("Error! Invalid input parameters\n");
        TEMAC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    PDEBUG("_temac_get_operating_speed\n");
    TEMAC_REG_READ(p_temac, TEMAC_MSC_OFFSET, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));
    regval &= TEMAC_MSC_LINKSPEED_MASK;

    switch (regval) {
    
    case TEMAC_MSC_LINKSPD_1000:
        PDEBUG("_temac_get_operating_speed: returning 1000\n");
        *p_speed = TEMAC_SPEED_1000_MBPS;

    case TEMAC_MSC_LINKSPD_100:
        PDEBUG("_temac_get_operating_speed: returning 100\n");
        *p_speed = TEMAC_SPEED_100_MBPS;

    case TEMAC_MSC_LINKSPD_10:
        PDEBUG("_temac_get_operating_speed: returning 10\n");
        *p_speed = TEMAC_SPEED_10_MBPS;

    default:
        *p_speed = 0;
    }

    return CCL_OK;
}

/**
 * _temac_disable_control_frame_len_check is used to disable 
 * the length check for control frames (pause frames). This means 
 * once the API is called, control frames larger than the minimum 
 * frame length are accepted. 
 *
 *  @param[in] p_temac is a pointer to the temac_ctx_t
 *       instance to be worked on.
 *
 * @return	error code
 */
static ccl_err_t _temac_disable_control_frame_len_check(temac_ctx_t *p_temac)
{
    b_u32 regval;

    if ( !p_temac ) {
        PDEBUG("Error! Invalid input parameters\n");
        TEMAC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    TEMAC_REG_READ(p_temac, TEMAC_RCW1_OFFSET, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));
    regval |= TEMAC_RCW1_CL_DIS_MASK;
    TEMAC_REG_WRITE(p_temac, TEMAC_RCW1_OFFSET, 
                       sizeof(b_u32), 0, 
                       (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/**
 * _temac_enable_control_frame_len_check is used to enable the length check
 * for control frames (pause frames). After calling the API, all control frames
 * received will be checked for proper length (less than minimum frame length).
 * By default, upon normal start up, control frame length check is enabled.
 * Hence this API needs to be called only if previously the control frame length
 * check has been disabled by calling the API
 * _temac_disable_control_frame_len_check.
 *
 * @param[in] p_temac is a pointer to the temac_ctx_t
 *      instance to be worked on.
 *
 * @return	error code
 */
static ccl_err_t _temac_enable_control_frame_len_check(temac_ctx_t *p_temac)
{
    b_u32 regval;

    if ( !p_temac ) {
        PDEBUG("Error! Invalid input parameters\n");
        TEMAC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    TEMAC_REG_READ(p_temac, TEMAC_RCW1_OFFSET, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));
    regval &= ~TEMAC_RCW1_CL_DIS_MASK;
    TEMAC_REG_WRITE(p_temac, TEMAC_RCW1_OFFSET, 
                       sizeof(b_u32), 0, 
                       (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/**
 *
 * _temac_hw_init (internal use only) performs a one-time setup of a TEMAC device.
 * The setup performed here only need to occur once after any reset.
 *
 * @param[in] p_temac is a pointer to temac_ctx_t instance to 
 *       be worked on
 * 
 * @return	error code
 */
static ccl_err_t _temac_hw_init(temac_ctx_t *p_temac)
{
    b_u32 regval;

    if ( !p_temac ) {
        PDEBUG("Error! Invalid input parameters\n");
        TEMAC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* Disable the receiver */
    TEMAC_REG_READ(p_temac, TEMAC_RCW1_OFFSET, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));
    regval &= ~TEMAC_RCW1_RX_MASK;
    TEMAC_REG_WRITE(p_temac, TEMAC_RCW1_OFFSET, 
                       sizeof(b_u32), 0, 
                       (b_u8 *)&regval, sizeof(b_u32));

    /* Disable the transmitter */
    TEMAC_REG_READ(p_temac, TEMAC_TC_OFFSET, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));
    regval &= ~TEMAC_TC_TX_MASK;
    TEMAC_REG_WRITE(p_temac, TEMAC_TC_OFFSET, 
                       sizeof(b_u32), 0, 
                       (b_u8 *)&regval, sizeof(b_u32));

    /*
     * Stopping the receiver in mid-packet causes a dropped packet
     * indication from HW. Clear it.
     */
    /* Get the interrupt pending register */
    TEMAC_REG_READ(p_temac, TEMAC_IP_OFFSET, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));

    if (regval & TEMAC_INT_RXRJECT_MASK) {
        /*
         * Set the interrupt status register to clear the pending
         * interrupt
         */
        regval = TEMAC_INT_RXRJECT_MASK;
        TEMAC_REG_WRITE(p_temac, TEMAC_IS_OFFSET, 
                           sizeof(b_u32), 0, 
                           (b_u8 *)&regval, sizeof(b_u32));
    }

    /*
     * Sync default options with HW but leave receiver and transmitter
     * disabled. They get enabled with XAxiEthernet_Start() if
     * TEMAC_TRANSMITTER_ENABLE_OPTION and TEMAC_RECEIVER_ENABLE_OPTION
     * are set
     */

    p_temac->options = TEMAC_FLOW_CONTROL_OPTION |
		               TEMAC_FCS_INSERT_OPTION | 
                       TEMAC_FCS_STRIP_OPTION | 
                       TEMAC_LENTYPE_ERR_OPTION;

    _ret = _temac_set_options(p_temac, p_temac->options &
                                 ~(TEMAC_TRANSMITTER_ENABLE_OPTION | 
                                   TEMAC_RECEIVER_ENABLE_OPTION));
    if ( _ret ) {
        PDEBUG("_temac_set_options error\n");
        TEMAC_ERROR_RETURN(_ret);
    }

    _ret = _temac_clear_options(p_temac, ~p_temac->options);
    if ( _ret ) {
        PDEBUG("_temac_clear_options error\n");
        TEMAC_ERROR_RETURN(_ret);
    }

    /* Set default MDIO divisor */
    _ret = _temac_phy_set_mdio_divisor(p_temac, TEMAC_MDIO_DIV_DFT);
    if ( _ret ) {
        PDEBUG("_temac_phy_set_mdio_divisor error\n");
        TEMAC_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

/**
 * _temac_reset does not perform a soft reset of the AxiEthernet core.
 * AxiEthernet hardware is reset by the device connected to the AXI4-Stream
 * interface.
 * This function inserts some delay before proceeding to check for MgtRdy bit.
 * The delay is necessary to be at a safe side. It takes a while for the reset
 * process to complete and for any of the AxiEthernet registers to be accessed.
 * It then checks for MgtRdy bit in IS register to know if AxiEthernet reset
 * is completed or not. Subsequently it calls one more driver function to
 * complete the AxiEthernet hardware initialization.
 *
 * @param[in] p_temac is a pointer to temac_ctx_t instance to 
 *       be worked on
 * 
 * @return	error code
 *
 * @note		It is the responsibility of the user to reset the AxiEthernet
 *		hardware before using it. AxiEthernet hardware should be reset
 *		through the device connected to the AXI4-Stream interface of
 *		AxiEthernet.
 */
static ccl_err_t _temac_reset(temac_ctx_t *p_temac)
{
    b_i32 timeout = 1000;
    b_u32 regval;

    if ( !p_temac ) {
        PDEBUG("Error! Invalid input parameters\n");
        TEMAC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Add delay of 1 sec to give enough time to the core to come
     * out of reset. Till the time core comes out of reset none of the
     * AxiEthernet registers are accessible including the IS register.
     */
    usleep(1000000);
#if 0
    /*
     * Check the status of the MgtRdy bit in the interrupt status
     * registers. This must be done to allow the MGT clock to become stable
     * for the Sgmii and 1000BaseX PHY interfaces. No other register reads
     * will be valid until this bit is valid.
     * The bit is always a 1 for all other PHY interfaces.
     */
    do {
        timeout--;
        if (timeout <= 0) {
            PDEBUG("ERROR: MGT clock is not ready\n");
            return CCL_FAIL;
        }
        TEMAC_REG_READ(p_temac, TEMAC_IS_OFFSET, 
                          sizeof(b_u32), 0, 
                          (b_u8 *)&regval, sizeof(b_u32));
    } while (!(regval & TEMAC_INT_MGTRDY_MASK));
#endif

    /* Stop the device and reset HW */
    _ret = _temac_stop(p_temac);
    if ( _ret ) {
        PDEBUG("_temac_stop error\n");
        TEMAC_ERROR_RETURN(_ret);
    }

    /* Setup HW */
    _ret = _temac_hw_init(p_temac);
    if ( _ret ) {
        PDEBUG("_temac_hw_init error\n");
        TEMAC_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

static ccl_err_t _temac_init(void *p_priv)
{
    temac_ctx_t   *p_temac;
    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        TEMAC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_temac = (temac_ctx_t *)p_priv;

    /* reset the device */
    _ret = _temac_reset(p_temac);
    if ( _ret ) {
        PDEBUG("_temac_reset error\n");
        TEMAC_ERROR_RETURN(_ret);
    }

    /* start the device */
    _ret = _temac_start(p_temac);
    if ( _ret ) {
        PDEBUG("_temac_start error\n");
        TEMAC_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

#define MV88E1510_PAGE_ADDRESS 0x16
static ccl_err_t _temac_phy_page_update(temac_ctx_t *p_temac, b_u32 phy_addr, b_u32 page)
{
    b_u32 regval;
    b_u32 cnt;

    if ( !p_temac ) {
        PDEBUG("Error! Invalid input parameters\n");
        TEMAC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* Write page number to MDIO Write Data register*/
    TEMAC_REG_WRITE(p_temac, TEMAC_MDIO_MWD_OFFSET, 
                    sizeof(b_u32), 0, 
                    (b_u8 *)&page, sizeof(b_u32));

    /* Perform write command to PHY Page address */
    regval = TEMAC_MDIO_MCR_INITIATE_MASK | 
             TEMAC_MDIO_MCR_OP_WRITE_MASK |
             ((MV88E1510_PAGE_ADDRESS << TEMAC_MDIO_MCR_REGAD_SHIFT) & 
              TEMAC_MDIO_MCR_REGAD_MASK) |
             ((phy_addr << TEMAC_MDIO_MCR_PHYAD_SHIFT) & 
              TEMAC_MDIO_MCR_PHYAD_MASK);

    TEMAC_REG_WRITE(p_temac, TEMAC_MDIO_MCR_OFFSET, 
                    sizeof(b_u32), 0, 
                    (b_u8 *)&regval, sizeof(b_u32));
    /* Check MDIO Ready asserted*/
    cnt = 0;
    do {
        if (cnt++ > 10000) {
            TEMAC_ERROR_RETURN(CCL_FAIL);
        }
        TEMAC_REG_READ(p_temac, TEMAC_MDIO_MCR_OFFSET, 
                       sizeof(b_u32), 0, 
                       (b_u8 *)&regval, sizeof(b_u32));

    } while (!(regval & TEMAC_MDIO_MCR_READY_MASK));

    return CCL_OK;
}

static ccl_err_t _temac_phy_read(temac_ctx_t *p_temac, b_u32 phy_addr, 
                                 b_u32 reg, b_u32 page, b_u32 *value)
{
    b_u32       regval;
    b_u32       cnt = 0;

    if ( !p_temac || !value ) {
        PDEBUG("Error! Invalid input parameters\n");
        TEMAC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    _ret = _temac_phy_page_update(p_temac, phy_addr, page);
    if ( _ret ) {
        PDEBUG("_temac_phy_page_update error: page=0x%02x\n", 
               page);
        TEMAC_ERROR_RETURN(_ret);
    }

    regval = TEMAC_MDIO_MCR_INITIATE_MASK | 
             TEMAC_MDIO_MCR_OP_READ_MASK |
             ((reg << TEMAC_MDIO_MCR_REGAD_SHIFT) & 
              TEMAC_MDIO_MCR_REGAD_MASK) | 
             ((phy_addr << TEMAC_MDIO_MCR_PHYAD_SHIFT) &
              TEMAC_MDIO_MCR_PHYAD_MASK);
    TEMAC_REG_WRITE(p_temac, TEMAC_MDIO_MCR_OFFSET, 
                    sizeof(b_u32), 0, 
                    (b_u8 *)&regval, sizeof(b_u32));
    /* Check MDIO Ready asserted*/
    cnt = 0;
    do {
        if (cnt++ > 10000) {
            TEMAC_ERROR_RETURN(CCL_FAIL);
        }
        TEMAC_REG_READ(p_temac, TEMAC_MDIO_MCR_OFFSET, 
                       sizeof(b_u32), 0, 
                       (b_u8 *)&regval, sizeof(b_u32));

    } while (!(regval & TEMAC_MDIO_MCR_READY_MASK));

    TEMAC_REG_READ(p_temac, TEMAC_MDIO_MRD_OFFSET, 
                   sizeof(b_u32), 0, 
                   (b_u8 *)value, sizeof(b_u32));

    return CCL_OK;
}

static ccl_err_t _temac_phy_write(temac_ctx_t *p_temac, b_u32 phy_addr, 
                                  b_u32 reg, b_u32 page, b_u32 value)
{
    b_u32       regval;
    b_u32       cnt = 0;

    if ( !p_temac ) {
        PDEBUG("Error! Invalid input parameters\n");
        TEMAC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    _ret = _temac_phy_page_update(p_temac, phy_addr, page);
    if ( _ret ) {
        PDEBUG("_temac_phy_page_update error: page=0x%02x\n", 
               page);
        TEMAC_ERROR_RETURN(_ret);
    }

    /* Write value to MDIO Write Data register*/
    TEMAC_REG_WRITE(p_temac, TEMAC_MDIO_MWD_OFFSET, 
                    sizeof(b_u32), 0, 
                    (b_u8 *)&value, sizeof(b_u32));

    /* Perform write command to reg address */
    regval = TEMAC_MDIO_MCR_INITIATE_MASK | 
             TEMAC_MDIO_MCR_OP_WRITE_MASK |
             ((reg << TEMAC_MDIO_MCR_REGAD_SHIFT) & 
              TEMAC_MDIO_MCR_REGAD_MASK) | 
             ((phy_addr << TEMAC_MDIO_MCR_PHYAD_SHIFT) &
              TEMAC_MDIO_MCR_PHYAD_MASK);

    TEMAC_REG_WRITE(p_temac, TEMAC_MDIO_MCR_OFFSET, 
                    sizeof(b_u32), 0, 
                    (b_u8 *)&regval, sizeof(b_u32));
    /* Check MDIO Ready asserted*/
    cnt = 0;
    do {
        if (cnt++ > 10000) {
            TEMAC_ERROR_RETURN(CCL_FAIL);
        }
        TEMAC_REG_READ(p_temac, TEMAC_MDIO_MCR_OFFSET, 
                       sizeof(b_u32), 0, 
                       (b_u8 *)&regval, sizeof(b_u32));

    } while (!(regval & TEMAC_MDIO_MCR_READY_MASK));

    return CCL_OK;
}

static ccl_err_t _ftemac_cmd_reset(devo_t devo, 
                                   __attribute__((unused)) device_cmd_parm_t parms[], 
                                   __attribute__((unused)) b_u16 parms_num)
{
    temac_ctx_t *p_temac = devman_device_private(devo);

    /* reset the device */
    _ret = _temac_reset(p_temac);
    if ( _ret ) {
        PDEBUG("_temac_reset error\n");
        TEMAC_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

static ccl_err_t _ftemac_cmd_start(devo_t devo, 
                                   __attribute__((unused)) device_cmd_parm_t parms[], 
                                   __attribute__((unused)) b_u16 parms_num)
{
    temac_ctx_t *p_temac = devman_device_private(devo);
    b_u32       regval;

    /* start the device */
    _ret = _temac_start(p_temac);
    if ( _ret ) {
        PDEBUG("_temac_start error\n");
        TEMAC_ERROR_RETURN(_ret);
    }

    regval = TEMAC_MDIO_MC_CLOCK_DIVIDE_MAX;
    /* Enable MDIO */
    TEMAC_REG_WRITE(p_temac, TEMAC_MDIO_MC_OFFSET, 
                       sizeof(b_u32), 0, 
                       (b_u8 *)&regval, sizeof(b_u32));
    regval |= TEMAC_MDIO_MC_MDIOEN_MASK;
    TEMAC_REG_WRITE(p_temac, TEMAC_MDIO_MC_OFFSET, 
                       sizeof(b_u32), 0, 
                       (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

static ccl_err_t _ftemac_cmd_init(devo_t devo, 
                                  __attribute__((unused)) device_cmd_parm_t parms[], 
                                  __attribute__((unused)) b_u16 parms_num)
{
    temac_ctx_t *p_temac = devman_device_private(devo);

    /* init the device */
    _ret = _temac_init(p_temac);
    if ( _ret ) {
        PDEBUG("_temac_init error\n");
        TEMAC_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

static ccl_err_t _ftemac_cmd_stop(devo_t devo, 
                                  __attribute__((unused)) device_cmd_parm_t parms[], 
                                  __attribute__((unused)) b_u16 parms_num)
{
    temac_ctx_t *p_temac = devman_device_private(devo);

    /* init the device */
    _ret = _temac_stop(p_temac);
    if ( _ret ) {
        PDEBUG("_temac_stop error\n");
        TEMAC_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

static ccl_err_t _ftemac_cmd_phy_read(devo_t devo, 
                                      device_cmd_parm_t parms[], 
                                      b_u16 parms_num)
{
    temac_ctx_t *p_temac = devman_device_private(devo);
    b_u32   reg;
    b_u32   page;
    b_u32   phy_addr;
    b_u32   value;

    phy_addr = (b_u32)parms[0].value;
    reg = (b_u32)parms[1].value;
    page = (b_u32)parms[2].value;

    _ret = _temac_phy_read(p_temac, phy_addr, reg, page, &value);
    if ( _ret ) {
        PDEBUG("_temac_phy_read error: phy_addr=%d, reg=0x%04x, page=0x%02x\n", 
               phy_addr, reg, page);
        TEMAC_ERROR_RETURN(_ret);
    }

    PRINTL("0x%08x\n", value);

    return CCL_OK;
}

static ccl_err_t _ftemac_cmd_phy_write(devo_t devo, 
                                       device_cmd_parm_t parms[], 
                                       b_u16 parms_num)
{
    temac_ctx_t *p_temac = devman_device_private(devo);
    b_u32   reg;
    b_u32   page;
    b_u32   phy_addr;
    b_u32   value;

    phy_addr = (b_u32)parms[0].value;
    reg = (b_u32)parms[1].value;
    page = (b_u32)parms[2].value;
    value = (b_u32)parms[3].value;

    _ret = _temac_phy_write(p_temac, phy_addr, reg, page, value);
    if ( _ret ) {
        PDEBUG("_temac_phy_write error: phy_addr=%d, reg=0x%04x, page=0x%02x, value=0x%x\n", 
               phy_addr, reg, page, value);
        TEMAC_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

static ccl_err_t _ftemac_cmd_show_counters(devo_t devo, 
                                           __attribute__((unused)) device_cmd_parm_t parms[], 
                                           __attribute__((unused)) b_u16 parms_num)
{
    temac_ctx_t *p_temac = devman_device_private(devo);
    char        cname[100] = {0};
    b_u32       offset = 40;
    b_u32       clen;
    b_u32       i;

    /* init the device */
    _ret = _temac_read_counters(p_temac);
    if ( _ret ) {
        PDEBUG("_temac_read_counters\n");
        TEMAC_ERROR_RETURN(_ret);
    }

    for (i = 0; i < p_temac->counters_num; i++) {
        snprintf(cname, sizeof(cname), "%s:", p_temac->counters[i].name);
        clen = strnlen(cname, sizeof(cname));
        if(offset <= clen) 
            offset = clen + 1;
        snprintf(cname+clen, sizeof(cname), "%*s", 
                 offset-clen, " ");
        PRINTL("%s%u\n", 
               cname,
               p_temac->counters[i].value);
        p_temac->counters[i].value_prev = p_temac->counters[i].value;
    }

    return CCL_OK;
}

static ccl_err_t _ftemac_cmd_show_counters_changed(devo_t devo, 
                                                   __attribute__((unused)) device_cmd_parm_t parms[], 
                                                   __attribute__((unused)) b_u16 parms_num)
{
    temac_ctx_t *p_temac = devman_device_private(devo);
    char        cname[100] = {0};
    b_u32       offset = 40;
    b_u32       clen;
    b_u32       i;
    b_u32       value;

    /* init the device */
    _ret = _temac_read_counters(p_temac);
    if ( _ret ) {
        PDEBUG("_temac_read_counters\n");
        TEMAC_ERROR_RETURN(_ret);
    }

    for (i = 0; i < p_temac->counters_num; i++) {
        if (p_temac->counters[i].value != p_temac->counters[i].value_prev) {
            value = p_temac->counters[i].value - p_temac->counters[i].value_prev;
            if (p_temac->counters[i].value_prev > p_temac->counters[i].value) {
                value = (p_temac->counters[i].value_prev - 
                         p_temac->counters[i].value + 
                         MAX_UINT32);
            }
            snprintf(cname, sizeof(cname), "%s:", p_temac->counters[i].name);
            clen = strnlen(cname, sizeof(cname));
            if(offset <= clen) 
                offset = clen + 1;
            snprintf(cname+clen, sizeof(cname), "%*s", 
                     offset-clen, " ");
            PRINTL("%s%u\n", 
                   cname,
                   value);
            p_temac->counters[i].value_prev = p_temac->counters[i].value;
        }
    }

    return CCL_OK;
}

/* device commands */
static device_cmd_t _cmds[] = 
{
    { 
        .name = "reset", .f_cmd = _ftemac_cmd_reset, .parms_num = 0,
        .help = "Reset Temac device" 
    },
    { 
        .name = "start", .f_cmd = _ftemac_cmd_start, .parms_num = 0,
        .help = "Start Temac device" 
    },
    { 
        .name = "init", .f_cmd = _ftemac_cmd_init, .parms_num = 0,
        .help = "Init Temac device" 
    },
    { 
        .name = "stop", .f_cmd = _ftemac_cmd_stop, .parms_num = 0,
        .help = "Stop Temac device" 
    },
    { 
        .name = "phy_read", .f_cmd = _ftemac_cmd_phy_read, .parms_num = 3,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "phy_addr", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%d"},
            { .name = "reg", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%d" }, 
            { .name = "page", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%d" }
        }, 
        .help = "Read phy register"
    },
    { 
        .name = "phy_write", .f_cmd = _ftemac_cmd_phy_write, .parms_num = 4,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "phy_addr", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%d"},
            { .name = "reg", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%d" }, 
            { .name = "page", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%d" }, 
            { .name = "value", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" }
        }, 
        .help = "Write phy register"
    },
    { 
        .name = "counters", .f_cmd = _ftemac_cmd_show_counters, .parms_num = 0,
        .help = "Show Temac counters" 
    },
    { 
        .name = "counters_changed", .f_cmd = _ftemac_cmd_show_counters_changed, .parms_num = 0,
        .help = "Show Temac change counters" 
    },
};

/* Device instance hw-specific initializations 
 */
static ccl_err_t _ftemac_add(void *devo, void *brdspec) 
{
    temac_ctx_t       *p_temac;
    eth_brdspec_t     *p_brdspec;
    b_u32             i;

    if ( !devo || !brdspec ) {
        PDEBUG("Error! Invalid input parameters\n");
        TEMAC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_temac = devman_device_private(devo);
    if ( !p_temac ) {
        PDEBUG("NULL context area\n");
        TEMAC_ERROR_RETURN(CCL_FAIL);
    }
    p_brdspec = (eth_brdspec_t *)brdspec;
    /* back reference the device manager object handle */    
    p_temac->devo = devo;    
    /* save the board specific info */
    memcpy(&p_temac->brdspec, p_brdspec, sizeof(eth_brdspec_t));

    p_temac->counters_num = sizeof(_temac_counters)/sizeof(_temac_counters[0]);
    p_temac->counters = calloc(p_temac->counters_num, sizeof(eth_counter_t));
    if (!p_temac->counters) {
        PDEBUG("Memory allocation failed\n");
        TEMAC_ERROR_RETURN(CCL_NO_MEM_ERR);
    }
    for (i = 0; i < p_temac->counters_num; i++) 
        memcpy(&p_temac->counters[i], &_temac_counters[i], sizeof(eth_counter_t));

#if 0
    /* reset the device */
    _ret = _temac_reset(p_temac);
    if ( _ret ) {
        PDEBUG("_temac_reset error\n");
        TEMAC_ERROR_RETURN(_ret);
    }

    /* start the device */
    _ret = _temac_start(p_temac);
    if ( _ret ) {
        PDEBUG("_temac_start error\n");
        TEMAC_ERROR_RETURN(_ret);
    }
#endif
    return CCL_OK;
}

/* Device instance hw-specific deinitializations 
 */
static ccl_err_t _ftemac_cut(void *devo)
{
    temac_ctx_t  *p_temac;
    if ( !devo ) {
        PDEBUG("Error! Invalid input parameters\n");
        TEMAC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_temac = devman_device_private(devo);
    if ( !p_temac ) {
        PDEBUG("NULL context area\n");
        TEMAC_ERROR_RETURN(CCL_FAIL);
    }
    /* stop the device */
    _ret = _temac_stop(p_temac);
    if ( _ret ) {
        PDEBUG("_temac_stop error\n");
        TEMAC_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

/* device type description structure */
static device_driver_t _temac_driver = {
    .name = "temac",
    .f_add = _ftemac_add,
    .f_cut = _ftemac_cut,
    .attrs = _attrs,
    .attrs_num = sizeof(_attrs)/sizeof(_attrs[0]),
    .cmds = _cmds,
    .cmds_num = sizeof(_cmds)/sizeof(_cmds[0]),
    .priv_size = sizeof(temac_ctx_t)
}; 

/* Initialize TEMAC device type
 */
ccl_err_t devtemac_init(void)
{
    /* register device type */
    _ret = devman_driver_register(&_temac_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to register driver\n");
        TEMAC_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}

/* Deinitialize TEMAC device type
 */
ccl_err_t devtemac_fini(void)
{
    /* unregister device type */
    _ret = devman_driver_unregister(&_temac_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to unregister driver\n");
        TEMAC_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}
