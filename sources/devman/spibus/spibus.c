/********************************************************************************/
/**
 * @file spibus.c
 * @brief SPIBUS SPI device manager driver 
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <semaphore.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <limits.h>
#include <pthread.h>

#include "ccl_types.h"
#include "ccl_logger.h"
#include "ccl_util.h"
#include "devman.h"
#include "devman_mod.h"
#include "devboard.h"
#include "spibus.h"

//#define SPIBUS_DEBUG
/* printing/error-returning macros */
#ifdef SPIBUS_DEBUG
    #define PDEBUG(fmt, args...) \
            ccl_syslog_err("SPIBUS_DEBUG: %s[%d]: " fmt, __FUNCTION__, __LINE__, ## args)
#else
    #define PDEBUG(fmt, args...)
#endif
#define SPIBUS_ERROR_RETURN(err) do { \
   ccl_syslog_err("Error! %s[%d], error=%i\n", __FUNCTION__, __LINE__, (err)); \
   return err; \
} while (0)                                                                                                     

#define SPIBUS_MAX_OFFSET 0xFFFF

/* the spibus internal control structure */      
typedef struct spibus_ctx_t {
    void            *devo;
    pthread_mutex_t mutex;
} spibus_ctx_t;

static ccl_err_t    _ret;
static int          _rc;

static spibus_ctx_t *_p_spibus;

/* get device register
 */
static ccl_err_t _spibus_get_sz(char *devname, b_u8 page, b_u32 offset, b_u8 *p_value, b_u32 size)
{
    b_i32               opcode;
    devman_ioctl_parm_t ioctl_parm;

    if ( !devname || !p_value ) {
        PDEBUG("Error! Invalid input parameters\n");
        SPIBUS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    PDEBUG("line: %d, page=%d, offset=0x%x, p_value=%p, size=%d\n", __LINE__, page, offset, p_value, size);
    memset(&ioctl_parm, 0, sizeof(devman_ioctl_parm_t));
    opcode = DEVMAN_IOC_READR;
    strcpy(ioctl_parm.name, devname);
    ioctl_parm.page   = page;
    ioctl_parm.offset = offset;
    ioctl_parm.p      = (char *)p_value;
    ioctl_parm.length = size;

    _ret = devman_ioctl(opcode, &ioctl_parm);    
    if ( _ret ) {
        PDEBUG("Error! Can't operate the kernel device! _ret=%d\n", _ret);
        SPIBUS_ERROR_RETURN(CCL_ERRNO);
    }

    return CCL_OK;
}

/* set device register
 */
static ccl_err_t _spibus_set_sz(char *devname, b_u8 page, b_u32 offset, b_u8 *p_value, b_u32 size)
{
    b_i32               opcode;
    devman_ioctl_parm_t ioctl_parm;

    if ( !devname || !p_value ) {
        PDEBUG("Error! Invalid input parameters\n");
        SPIBUS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    memset(&ioctl_parm, 0, sizeof(devman_ioctl_parm_t));
    opcode = DEVMAN_IOC_WRITER;
    strcpy(ioctl_parm.name, devname);
    ioctl_parm.page   = page;
    ioctl_parm.offset = offset;
    ioctl_parm.p      = (char *)p_value;
    ioctl_parm.length = size;

    _ret = devman_ioctl(opcode, &ioctl_parm);    
    if ( _ret ) {
        PDEBUG("Error! Can't operate the kernel device! _ret=%d\n", _ret);
        SPIBUS_ERROR_RETURN(CCL_ERRNO);
    }

    return CCL_OK;
}

/* set device register in burst mode
 */
static ccl_err_t _spibus_set_bsz(char *devname, b_u8 page, b_u32 offset, b_u8 *p_value, b_u32 size)
{
    b_i32               opcode;
    devman_ioctl_parm_t ioctl_parm;

    if ( !devname || !p_value ) {
        PDEBUG("Error! Invalid input parameters\n");
        SPIBUS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    memset(&ioctl_parm, 0, sizeof(devman_ioctl_parm_t));
    opcode = DEVMAN_IOC_WRITEBUF;
    strcpy(ioctl_parm.name, devname);
    ioctl_parm.page   = page;
    ioctl_parm.offset = offset;
    ioctl_parm.p      = (char *)p_value;
    ioctl_parm.length = size;

    _ret = devman_ioctl(opcode, &ioctl_parm);    
    if ( _ret ) {
        PDEBUG("Error! Can't operate the kernel device! _ret=%d\n", _ret);
        SPIBUS_ERROR_RETURN(CCL_ERRNO);
    }

    return CCL_OK;
}

static ccl_err_t _spibus_register_read(char *devname, b_u32 offset, b_u8 *p_value, b_u32 size)
{
    if ( !devname || !p_value ) {
        PDEBUG("Error! Invalid input parameters\n");
        SPIBUS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    PDEBUG("line: %d\n", __LINE__);
    _ret = _spibus_get_sz(devname, 0, offset, p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Can't read reg=0x%x ,value=0x%x, size=%d, ret=%d\n", offset, p_value, size, _ret);
        return CCL_FAIL;
    }

    return CCL_OK;
}

/*
 */
static ccl_err_t _spibus_register_write(char *devname, b_u32 offset, b_u8 *p_value, b_u32 size)
{
    if ( !devname || !p_value ) {
        PDEBUG("Error! Invalid input parameters\n");
        SPIBUS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    _ret = _spibus_set_sz(devname, 0, offset, p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Can't write reg=0x%x ,Value=0x%x,ret=%d\n", 
               offset, p_value, _ret);
        return CCL_FAIL;
    }

    return CCL_OK;
}

/*
 */
static ccl_err_t _spibus_burst_write(char *devname, b_u32 offset, b_u8 *p_value, b_u32 size)
{
    if ( !devname || !p_value ) {
        PDEBUG("Error! Invalid input parameters\n");
        SPIBUS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    _ret = _spibus_set_bsz(devname, 0, offset, p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Can't write reg=0x%x ,Value=0x%x,ret=%d\n", 
               offset, p_value, _ret);
        return CCL_FAIL;
    }

    return CCL_OK;
}

/* Simple read/write register accessors, array elements indexation within the given page
 */
ccl_err_t devspibus_read_buff(char *devname, 
                            b_u32 offset, b_u32 idx, 
                            b_u8 *p_value, b_u32 size)
{
    if ( !devname || !p_value || !size ) {
        PDEBUG("Error! Invalid input parameters\n");
        SPIBUS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    _rc = pthread_mutex_lock(&_p_spibus->mutex);
    if (_rc) {
        PDEBUG("pthread_mutex_lock error\n");
        SPIBUS_ERROR_RETURN(CCL_FAIL);
    }
    _ret = _spibus_register_read(devname, offset+idx*size, p_value, size);    
    if ( _ret ) {
        PDEBUG("_devspibus_register_read error\n");
    }
    _rc = pthread_mutex_unlock(&_p_spibus->mutex);
    if (_rc) {
        PDEBUG("pthread_mutex_unlock error\n");
        SPIBUS_ERROR_RETURN(CCL_FAIL);
    }

    return _ret;
}

ccl_err_t devspibus_write_buff(char *devname, 
                             b_u32 offset, b_u32 idx, 
                             b_u8 *p_value, b_u32 size)
{
    if ( !devname || !p_value || !size ) {
        PDEBUG("Error! Invalid input parameters\n");
        SPIBUS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    _rc = pthread_mutex_lock(&_p_spibus->mutex);
    if (_rc) {
        PDEBUG("pthread_mutex_lock error\n");
        SPIBUS_ERROR_RETURN(CCL_FAIL);
    }
    _ret = _spibus_register_write(devname, offset+idx*size, p_value, size);
    if ( _ret ) {
        PDEBUG("_devspibus_register_write error\n");
    }
    _rc = pthread_mutex_unlock(&_p_spibus->mutex);
    if (_rc) {
        PDEBUG("pthread_mutex_unlock error\n");
        SPIBUS_ERROR_RETURN(CCL_FAIL);
    }

    return _ret;
}

ccl_err_t devspibus_write_burst(char *devname, 
                                b_u32 offset, b_u32 idx, 
                                b_u8 *p_value, b_u32 size)
{
    if ( !devname || !p_value || !size ) {
        PDEBUG("Error! Invalid input parameters\n");
        SPIBUS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    _rc = pthread_mutex_lock(&_p_spibus->mutex);
    if (_rc) {
        PDEBUG("pthread_mutex_lock error\n");
        SPIBUS_ERROR_RETURN(CCL_FAIL);
    }
    _ret = _spibus_burst_write(devname, offset+idx*size, p_value, size);
    if ( _ret ) {
        PDEBUG("_devspibus_register_write error\n");
    }
    _rc = pthread_mutex_unlock(&_p_spibus->mutex);
    if (_rc) {
        PDEBUG("pthread_mutex_unlock error\n");
        SPIBUS_ERROR_RETURN(CCL_FAIL);
    }

    return _ret;
}

static device_attr_t _attrs[] = {
};

/* Device instance hw-specific initializations 
 */
static ccl_err_t _fspibus_add(void *devo, __attribute__((unused)) void *brdspec)
{
    spibus_ctx_t  *p_spibus;

    if ( !devo ) {
        PDEBUG("Error! Invalid input parameters\n");
        SPIBUS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_spibus = devman_device_private(devo);
    if ( !p_spibus ) {
        PDEBUG("NULL context area\n");
        SPIBUS_ERROR_RETURN(CCL_FAIL);
    }
    /* get mutex */
    _rc = pthread_mutex_init(&p_spibus->mutex, NULL);
    if ( _rc ) {
        PDEBUG("pthread_mutex_init error\n");
        SPIBUS_ERROR_RETURN(CCL_FAIL);
    }

    p_spibus->devo = devo;    
    /* store the context */
    _p_spibus = p_spibus;

    return CCL_OK;
}

/* Device instance hw-specific deinitializations 
 */
static ccl_err_t _fspibus_cut(void *devo)
{
    spibus_ctx_t  *p_spibus;

    if ( !devo ) {
        PDEBUG("Error! Invalid input parameters\n");
        SPIBUS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/

    /* get context area/board specific info*/
    p_spibus = devman_device_private(devo);
    if ( !p_spibus ) {
        PDEBUG("NULL context area\n");
        SPIBUS_ERROR_RETURN(CCL_FAIL);
    }

    _rc = pthread_mutex_destroy(&p_spibus->mutex);
    if ( _rc ) {
        PDEBUG("pthread_mutex_destroy error\n");
        SPIBUS_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

/* device type description structure */
static device_driver_t _spibus_driver = 
{
    .name = "spibus",
    .f_add = _fspibus_add,
    .f_cut = _fspibus_cut,
    .attrs_num = sizeof(_attrs)/sizeof(_attrs[0]),
    .attrs = _attrs,
    .priv_size = sizeof(spibus_ctx_t)
}; 

/* Initialize SPIBUS device type
 */
ccl_err_t devspibus_init(void)
{
    /* register device type */
    _ret = devman_driver_register(&_spibus_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to register driver\n");
        SPIBUS_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}

/* Deinitialize I2CBUS device type
 */
ccl_err_t devspibus_fini(void)
{
    /* unregister device type */
    _ret = devman_driver_unregister(&_spibus_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to unregister driver\n");
        SPIBUS_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}
