/********************************************************************************/
/**
 * @file eeprom.c
 * @brief EEPROM device manager driver 
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <semaphore.h>
#include <linux/limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <byteswap.h>

#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "i2ccore.h"       
#include "eeprom.h"

//#define EEPROM_DEBUG
/* printing/error-returning macros */
#ifdef EEPROM_DEBUG
    #define PDEBUG(fmt, args...) \
            ccl_syslog_err("EEPROM: %s[%d]: " fmt, __FUNCTION__, __LINE__, ## args)
#else
    #define PDEBUG(fmt, args...)
#endif
#define EEPROM_ERROR_RETURN(err) do { \
   ccl_syslog_err("Error! %s[%d], error=%i\n", __FUNCTION__, __LINE__, (err)); \
   return err; \
} while (0)

/* EEPROM SPD constants and offsets */
#define SPD_MANUFACTURER_ID_CODE_OFFSET 321 
#define SPD_MANUFACTURER_ID_CODE_LEN    1 
#define SPD_MODULE_SERIAL_NUMBER_OFFSET 325 
#define SPD_MODULE_SERIAL_NUMBER_LEN    4  
#define SPD_MODULE_PART_NUMBER_OFFSET   329 
#define SPD_MODULE_PART_NUMBER_LEN      20

/* the eeprom internal control structure */
typedef struct eeprom_ctx_t {
    void                *devo;
    eeprom_brdspec_t    brdspec;
} eeprom_ctx_t;

static ccl_err_t _ret;                     

static ccl_err_t _eeprom_validate_offset(eeprom_ctx_t *p_eeprom, b_u32 offset)
{
    b_u8 i;

    return CCL_OK; //TODO: recheck offset validation
    for (i = 0; i < eDEVBRD_EEPROM_FIELDS_MAXNUM; i++) {
        if (p_eeprom->brdspec.param[i].offset == offset) {
            break;
        }
    }
    _ret = CCL_OK;
    if (i == eDEVBRD_EEPROM_FIELDS_MAXNUM)
        _ret = CCL_FAIL;

    return _ret;
}

static ccl_err_t _eeprom_spd_set_bank_addr(eeprom_ctx_t *p_eeprom,
                                           b_u8 mux_pin,b_u32 offset)
{
    i2c_route_t i2c_route;
    b_u8        *buf;
    b_u16       baddr = p_eeprom->brdspec.spd_ctrl.sba0;

    buf = calloc(1, p_eeprom->brdspec.spd_ctrl.ndummy);
    if (!buf) {
        PDEBUG("Can't allocate memory\n");
        return CCL_NO_MEM_ERR;
    }

    if (offset >= p_eeprom->brdspec.spd_ctrl.bsize)
        baddr = p_eeprom->brdspec.spd_ctrl.sba1;

    memcpy(&i2c_route, &p_eeprom->brdspec.i2c.bus_id, sizeof(i2c_route_t));
    i2c_route.dev_i2ca = baddr;

    _ret = devi2ccore_write_buff(&i2c_route, mux_pin,
                                 offset, p_eeprom->brdspec.addr_width, 
                                 buf, p_eeprom->brdspec.spd_ctrl.ndummy);
    if ( _ret ) {
        PDEBUG("Error! Failed to write over i2c bus\n");
        free(buf);
        EEPROM_ERROR_RETURN(_ret);
    }
    free(buf);

    return CCL_OK;
}

/* eeprom register read */
static ccl_err_t _eeprom_buf_read(void *p_priv, b_u32 offset, 
                                  __attribute__((unused)) b_u32 offset_sz, 
                                  b_u32 idx, b_u8 *p_value, b_u32 size)
{
    eeprom_ctx_t   *p_eeprom;
    i2c_route_t      i2c_route;

    if ( !p_priv || !p_value || idx || !size ) {
        PDEBUG("Error! Invalid input parameters\n");
        EEPROM_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    PDEBUG("123$=> offset=0x%x, size=%d\n", offset, size);

    p_eeprom = (eeprom_ctx_t *)p_priv; 
    if (offset + size > p_eeprom->brdspec.size) {
        PDEBUG("Error! Invalid input parameters\n");
        EEPROM_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    _ret = _eeprom_validate_offset(p_eeprom, offset);
    if ( _ret ) {
        PDEBUG("Error! Invalid offset\n");
        EEPROM_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    if (p_eeprom->brdspec.is_spd) {
        _ret = _eeprom_spd_set_bank_addr(p_eeprom, 
                                         p_eeprom->brdspec.extra_mux_pin,
                                         offset);
        if ( _ret ) {
            PDEBUG("Error! _eeprom_spd_set_bank_addr() failed\n");
            EEPROM_ERROR_RETURN(_ret);
        }
        offset %= p_eeprom->brdspec.spd_ctrl.bsize;
    }

    memcpy(&i2c_route, &p_eeprom->brdspec.i2c.bus_id, sizeof(i2c_route_t));
    _ret = devi2ccore_read_buff(&i2c_route, 
                                p_eeprom->brdspec.extra_mux_pin, 
                                offset, p_eeprom->brdspec.addr_width, 
                                p_value, size);

    if ( _ret ) {
        PDEBUG("Error! Failed to read over i2c bus\n");
        EEPROM_ERROR_RETURN(_ret);
    }

    return _ret;
}

/* eeprom register write */
static ccl_err_t _eeprom_buf_write(void *p_priv, b_u32 offset, 
                                   __attribute__((unused)) b_u32 offset_sz, 
                                   b_u32 idx, b_u8 *p_value, b_u32 size)
{
    eeprom_ctx_t   *p_eeprom;
    i2c_route_t      i2c_route;
    b_u32            bytes_to_write;

    if ( !p_priv || !p_value || idx || !size ) {
        PDEBUG("Error! Invalid input parameters\n");
        EEPROM_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    PDEBUG("123$=> offset=0x%03x, size=%d\n", offset, size);

    p_eeprom = (eeprom_ctx_t *)p_priv; 
    if (offset + size > p_eeprom->brdspec.size) {
        PDEBUG("Error! Invalid input parameters\n");
        EEPROM_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    _ret = _eeprom_validate_offset(p_eeprom, offset);
    if ( _ret ) {
        PDEBUG("Error! Invalid offset\n");
        EEPROM_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    if (p_eeprom->brdspec.is_spd) {
        _ret = _eeprom_spd_set_bank_addr(p_eeprom, 
                                         p_eeprom->brdspec.extra_mux_pin,
                                         offset);
        if ( _ret ) {
            PDEBUG("Error! _eeprom_spd_set_bank_addr() failed\n");
            EEPROM_ERROR_RETURN(_ret);
        }
        offset %= p_eeprom->brdspec.spd_ctrl.bsize;
    }

    memcpy(&i2c_route, &p_eeprom->brdspec.i2c.bus_id, sizeof(i2c_route_t));

    while (offset % p_eeprom->brdspec.page_size + size > 
           p_eeprom->brdspec.page_size) {
        bytes_to_write = p_eeprom->brdspec.page_size -
                         offset % p_eeprom->brdspec.page_size;
        _ret = devi2ccore_write_buff(&i2c_route, 
                                     p_eeprom->brdspec.extra_mux_pin, 
                                     offset, p_eeprom->brdspec.addr_width, 
                                     p_value, bytes_to_write);
        if ( _ret ) {
            PDEBUG("Error! Failed to write over i2c bus\n");
            EEPROM_ERROR_RETURN(_ret);
        }
        size -= bytes_to_write;
        offset += bytes_to_write;
        p_value += bytes_to_write;
    }
    usleep(1000000);
    if (size > 0) {
        _ret = devi2ccore_write_buff(&i2c_route, 
                                     p_eeprom->brdspec.extra_mux_pin, 
                                     offset, p_eeprom->brdspec.addr_width, 
                                     p_value, size);
        if ( _ret ) {
            PDEBUG("Error! Failed to write over i2c bus\n");
            EEPROM_ERROR_RETURN(_ret);
        }
    }

    return _ret;
}

static ccl_err_t _eeprom_get_spd_info(void *p_priv,
                                      eeprom_spd_info_t *spd_info)
{
    eeprom_ctx_t   *p_eeprom;
    b_u8           *buf;
    b_u32          sn;

    if ( !p_priv || !spd_info ) {
        PDEBUG("Error! Invalid input parameters\n");
        EEPROM_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    memset(spd_info, 0, sizeof(*spd_info));

    p_eeprom = (eeprom_ctx_t *)p_priv;
    if (!p_eeprom->brdspec.is_spd) 
        return CCL_OK;

    buf = calloc(1, p_eeprom->brdspec.spd_ctrl.bsize);
    if (!buf) {
        PDEBUG("Can't allocate memory\n");
        EEPROM_ERROR_RETURN(CCL_NO_MEM_ERR);
    }

    _ret = _eeprom_buf_read(p_priv, 
                            p_eeprom->brdspec.spd_ctrl.bsize, 
                            0, 0, buf, 
                            p_eeprom->brdspec.spd_ctrl.bsize);
    if ( _ret ) {
        free(buf);
        PDEBUG("Error! _eeprom_buf_read() failed\n");
        EEPROM_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    b_i32 sn_offset = SPD_MODULE_SERIAL_NUMBER_OFFSET % p_eeprom->brdspec.spd_ctrl.bsize;
    b_i32 pn_offset = SPD_MODULE_PART_NUMBER_OFFSET % p_eeprom->brdspec.spd_ctrl.bsize;
    b_i32 mid_offset = SPD_MANUFACTURER_ID_CODE_OFFSET % p_eeprom->brdspec.spd_ctrl.bsize;

    memcpy(&sn, &buf[sn_offset], 4); 
    snprintf(spd_info->module_sn, DEVMAN_DEVNAME_LEN, "%x", bswap_32(sn));
    strncpy(spd_info->module_pn, &buf[pn_offset], 
            SPD_MODULE_PART_NUMBER_LEN);
    snprintf(spd_info->manufacturer_id, DEVMAN_DEVNAME_LEN, "%x", buf[mid_offset]);

    free(buf);

    return CCL_OK;
}

static ccl_err_t _eeprom_init(void *p_priv)
{
    eeprom_ctx_t   *p_eeprom;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        EEPROM_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_eeprom = (eeprom_ctx_t *)p_priv;

    return CCL_OK;
}

static ccl_err_t _eeprom_configure_test(void *p_priv)
{
    eeprom_ctx_t   *p_eeprom;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        EEPROM_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_eeprom = (eeprom_ctx_t *)p_priv;

    return CCL_OK;
}

static ccl_err_t _eeprom_run_test(void *p_priv, 
                                  b_u8 *buf, 
                                  b_u32 size)
{
    eeprom_ctx_t       *p_eeprom;
    eeprom_spd_info_t  spd_info;
    b_u8               value1[16];      
    b_u8               value2[16];      
    b_u32              offset = 20;     
    b_u32              len = 16;        
    b_i32              i;   
    eeprom_spd_stats_t *stats;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        EEPROM_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_eeprom = (eeprom_ctx_t *)p_priv;

    if (p_eeprom->brdspec.is_spd) {
        if (size == sizeof(eeprom_spd_stats_t)) {
            stats = (eeprom_spd_stats_t *)buf;
            _ret = _eeprom_get_spd_info(p_priv, &stats->eeprom_spd_info);
            if (_ret)
                EEPROM_ERROR_RETURN(_ret);
        }
    } else {
        for (i = 0; i < len; i++) {
            if (i%2)
                value1[i] = 0x55;
            else
                value1[i] = 0xaa;
        }

        ccl_syslog_err("tested value:\n");
        for (i = 0; i < 16; i++)
            ccl_syslog_err("%02x ", value1[i]);
        ccl_syslog_err("\n");

        _ret = _eeprom_buf_write(p_eeprom, offset, 0, 0, value1, len);
        if (_ret)
            EEPROM_ERROR_RETURN(_ret);

        _ret = _eeprom_buf_read(p_eeprom, offset, 0, 0, value2, len);
        if (_ret)
            EEPROM_ERROR_RETURN(_ret);

        ccl_syslog_err("read value:\n");
        for (i = 0; i < len; i++)
            ccl_syslog_err("%02x ", value2[i]);
        ccl_syslog_err("\n");
        for (i = 0; i < len; i++) {
            if (value1[i] != value2[i]) {
                ccl_syslog_err("Error: value1[%d]=0x%02x, value2[%d]=0x%02x\n",
                               i, value1[i], i, value2[i]);
                EEPROM_ERROR_RETURN(CCL_FAIL);
            }
        }
    }

    return CCL_OK;
}

ccl_err_t eeprom_attr_read(void *p_priv, b_u32 attrid, 
                           b_u32 offset, b_u32 offset_sz, 
                           b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || !p_value || idx || !size ) {
        PDEBUG("Error! Invalid input parameters\n");
        EEPROM_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch ( attrid ) {
    case eEEPROM_BOARD_SN:
    case eEEPROM_ASSEMBLY_NUM:
    case eEEPROM_HW_REV:
    case eEEPROM_HW_SUB_REV:
    case eEEPROM_PART_NUM:
    case eEEPROM_CLEI:
    case eEEPROM_MFG_DATE:
    case eEEPROM_LICENSE:
    case eEEPROM_DEV_TYPE_ID:
    case eEEPROM_BUFFER:
        _ret = _eeprom_buf_read(p_priv, offset, offset_sz, idx, p_value, size);
        break;
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK) {
        PDEBUG("Failure in attribute id: %d\n", attrid);
    }

    return _ret;
}

ccl_err_t eeprom_attr_write(void *p_priv, b_u32 attrid, 
                            b_u32 offset, b_u32 offset_sz, 
                            b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || !p_value || idx || !size ) {
        PDEBUG("Error! Invalid input parameters\n");
        EEPROM_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch ( attrid ) {
    case eEEPROM_BOARD_SN:
    case eEEPROM_ASSEMBLY_NUM:
    case eEEPROM_HW_REV:
    case eEEPROM_HW_SUB_REV:
    case eEEPROM_PART_NUM:
    case eEEPROM_CLEI:
    case eEEPROM_MFG_DATE:
    case eEEPROM_LICENSE:
    case eEEPROM_DEV_TYPE_ID:
    case eEEPROM_BUFFER:
        _ret = _eeprom_buf_write(p_priv, offset, offset_sz, idx, p_value, size);
        break;
    case eEEPROM_INIT:
        _ret = _eeprom_init(p_priv);
        break;
    case eEEPROM_CONFIGURE_TEST:
        _ret = _eeprom_configure_test(p_priv);
        break;
    case eEEPROM_RUN_TEST:
        _ret = _eeprom_run_test(p_priv, p_value, size);
        break;
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK) {
        PDEBUG("Failure in attribute id: %d\n", attrid);
    }

    return _ret;
}

/* eeprom attributes definition */
static device_attr_t _attrs[] = {
    {
        .name = "i2c_bus", 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset = offsetof(struct eeprom_ctx_t, brdspec.i2c.bus_id),
        .format = "%d"
    },
    {
        .name = "i2c_addr", 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset = offsetof(struct eeprom_ctx_t, brdspec.i2c.dev_i2ca),
        .format = "0x%03x"
    },
    {
        .name = "mux_pin", 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset = offsetof(struct eeprom_ctx_t, brdspec.extra_mux_pin),
        .flags = eDEVMAN_ATTRFLAG_RO, .format = "%d"
    },
    {
        .name = "name", 
        .type = eDEVMAN_ATTR_STRING, .maxnum = 1, 
        .offset = offsetof(struct eeprom_ctx_t, brdspec.name),
        .size = DEVMAN_DEVNAME_LEN,
        .format = "%s"
    },
    {
        .name = "serial_number", .id = eEEPROM_BOARD_SN, 
        .type = eDEVMAN_ATTR_STRING, .maxnum = 1, 
        .offset_sz = sizeof(b_u16) 
    },
    {
        .name = "assembly_number", .id = eEEPROM_ASSEMBLY_NUM, 
        .type = eDEVMAN_ATTR_STRING, .maxnum = 1, 
        .offset_sz = sizeof(b_u16) 
    },
    {
        .name = "hw_rev", .id = eEEPROM_HW_REV, 
        .type = eDEVMAN_ATTR_STRING, .maxnum = 1, 
        .offset_sz = sizeof(b_u16)
    },
    {
        .name = "hw_subrev", .id = eEEPROM_HW_SUB_REV, 
        .type = eDEVMAN_ATTR_STRING, .maxnum = 1, 
        .offset_sz = sizeof(b_u16)
    },
    {
        .name = "part_number", .id = eEEPROM_PART_NUM, 
        .type = eDEVMAN_ATTR_STRING, .maxnum = 1, 
        .offset_sz = sizeof(b_u16)
    },
    {
        .name = "clei", .id = eEEPROM_CLEI, 
        .type = eDEVMAN_ATTR_STRING, .maxnum = 1, 
        .offset_sz = sizeof(b_u16)
    },
    {
        .name = "mfg_date", .id = eEEPROM_MFG_DATE, 
        .type = eDEVMAN_ATTR_STRING, .maxnum = 1, 
        .offset_sz = sizeof(b_u16)
    },
    {
        .name = "license", .id = eEEPROM_LICENSE, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u16)
    },
    {
        .name = "device_type", .id = eEEPROM_DEV_TYPE_ID, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u16)
    },
    {
        .name = "buffer", .id = eEEPROM_BUFFER,
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = 0
    },
    {
        .name = "init", .id = eEEPROM_INIT, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "config_test", .id = eEEPROM_CONFIGURE_TEST, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "run_test", .id = eEEPROM_RUN_TEST, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, 
        .size = sizeof(board_dev_stats_t), 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
};

static ccl_err_t _feeprom_cmd_spd_info(devo_t devo, 
                                       __attribute__((unused)) device_cmd_parm_t parms[], 
                                       __attribute__((unused)) b_u16 parms_num)
{
    void              *p_priv = devman_device_private(devo);
    eeprom_spd_info_t spd_info;

    _ret = _eeprom_get_spd_info(p_priv, &spd_info);
    if ( _ret ) {
        PDEBUG("Error! _eeprom_get_spd_info() failed\n");
        EEPROM_ERROR_RETURN(_ret);
    }

    PRINTL("module_sn: %s, module_pn: %s, manufacturer_id: %s\n",
           spd_info.module_sn, spd_info.module_pn, spd_info.manufacturer_id);

    return CCL_OK;
}

/* device commands */
static device_cmd_t _cmds[] = 
{
    { 
        .name = "spd_info", .f_cmd = _feeprom_cmd_spd_info, .parms_num = 0,
        .help = "Read DDR4 SPD information" 
    },
};


/* Device instance hw-specific initializations 
 */
static ccl_err_t _feeprom_add(void *devo, void *brdspec)
{
    eeprom_ctx_t       *p_eeprom;
    eeprom_brdspec_t     *p_brdspec;
    b_u32                i;

    if ( !devo || !brdspec ) {
        PDEBUG("Error! Invalid input parameters\n");
        EEPROM_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_eeprom = devman_device_private(devo);
    if ( !p_eeprom ) {
        PDEBUG("NULL context area\n");
        EEPROM_ERROR_RETURN(CCL_FAIL);
    }
    p_brdspec = (eeprom_brdspec_t *)brdspec;

    PDEBUG("123$=> p_brdspec->bus_id=%d, dev_i2ca=0x%x\n", 
           p_brdspec->i2c.bus_id, p_brdspec->i2c.dev_i2ca);

    p_eeprom->devo = devo;    
    memcpy(&p_eeprom->brdspec, p_brdspec, sizeof(eeprom_brdspec_t));

    /* set i2c muxes attributes array real elements number */
    for ( i = 0; i < sizeof(_attrs)/sizeof(_attrs[0]); i++ ) {
        if ( !strcmp(_attrs[i].name, "serial_number") ) {
            _attrs[i].size = p_brdspec->param[eDEVBRD_EEPROM_FIELD_BOARD_SN].size;
            _attrs[i].offset = p_brdspec->param[eDEVBRD_EEPROM_FIELD_BOARD_SN].offset;
        }
        else if ( !strcmp(_attrs[i].name, "assembly_number") ) {
            _attrs[i].size = p_brdspec->param[eDEVBRD_EEPROM_FIELD_ASSEMBLY_NUM].size;
            _attrs[i].offset = p_brdspec->param[eDEVBRD_EEPROM_FIELD_ASSEMBLY_NUM].offset;
        }
        else if ( !strcmp(_attrs[i].name, "hw_rev") ) {
            _attrs[i].size = p_brdspec->param[eDEVBRD_EEPROM_FIELD_HW_REV].size;
            _attrs[i].offset = p_brdspec->param[eDEVBRD_EEPROM_FIELD_HW_REV].offset;
        }
        else if ( !strcmp(_attrs[i].name, "hw_subrev") ) {
            _attrs[i].size = p_brdspec->param[eDEVBRD_EEPROM_FIELD_HW_SUB_REV].size;
            _attrs[i].offset = p_brdspec->param[eDEVBRD_EEPROM_FIELD_HW_SUB_REV].offset;
        }
        else if ( !strcmp(_attrs[i].name, "part_number") ) {
            _attrs[i].size = p_brdspec->param[eDEVBRD_EEPROM_FIELD_PART_NUM].size;
            _attrs[i].offset = p_brdspec->param[eDEVBRD_EEPROM_FIELD_PART_NUM].offset;
        }
        else if ( !strcmp(_attrs[i].name, "clei") ) {
            _attrs[i].size = p_brdspec->param[eDEVBRD_EEPROM_FIELD_CLEI].size;
            _attrs[i].offset = p_brdspec->param[eDEVBRD_EEPROM_FIELD_CLEI].offset;
        }
        else if ( !strcmp(_attrs[i].name, "mfg_date") ) {
            _attrs[i].size = p_brdspec->param[eDEVBRD_EEPROM_FIELD_MFG_DATE].size;
            _attrs[i].offset = p_brdspec->param[eDEVBRD_EEPROM_FIELD_MFG_DATE].offset;
        }
        else if ( !strcmp(_attrs[i].name, "license") ) {
            _attrs[i].size = p_brdspec->param[eDEVBRD_EEPROM_FIELD_LICENSE].size;
            _attrs[i].offset = p_brdspec->param[eDEVBRD_EEPROM_FIELD_LICENSE].offset;
        }
        else if ( !strcmp(_attrs[i].name, "device_type") ) {
            _attrs[i].size = p_brdspec->param[eDEVBRD_EEPROM_FIELD_DEV_TYPE_ID].size;
            _attrs[i].offset = p_brdspec->param[eDEVBRD_EEPROM_FIELD_DEV_TYPE_ID].offset;
        }
    }

    return CCL_OK;
}


/* Device instance hw-specific deinitializations 
 */
static ccl_err_t _feeprom_cut(void *devo)
{
    if ( !devo ) {
        PDEBUG("Error! Invalid input parameters\n");
        EEPROM_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    return CCL_OK;
}

/* device type description structure */
static device_driver_t _eeprom_driver = {
    .name = "eeprom",
    .f_add = _feeprom_add,
    .f_cut = _feeprom_cut,
    .attrs = _attrs,
    .attrs_num = sizeof(_attrs)/sizeof(_attrs[0]),
    .cmds = _cmds,
    .cmds_num = sizeof(_cmds)/sizeof(_cmds[0]),
    .priv_size = sizeof(eeprom_ctx_t)
}; 

/* Initialize EEPROM device type
 */
ccl_err_t deveeprom_init(void)
{
    /* register device type */
    _ret = devman_driver_register(&_eeprom_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to register driver\n");
        EEPROM_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

/* Deinitialize EEPROM device type
 */
ccl_err_t deveeprom_fini(void)
{
    /* unregister device type */
    _ret = devman_driver_unregister(&_eeprom_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to unregister driver\n");
        EEPROM_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

