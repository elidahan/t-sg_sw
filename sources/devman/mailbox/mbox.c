/********************************************************************************/
/**
 * @file mbox.c
 * @brief Xilinx Mailbox Core device manager driver
 * @copyright 2019 CELARE. All rights reserved.
 */
/********************************************************************************/
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <semaphore.h>
#include <linux/limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "i2cbus.h"
#include "mbox.h"

//#define MBOX_DEBUG
/* printing/error-returning macros */
#ifdef MBOX_DEBUG
    #define PDEBUG(fmt, args...) ccl_syslog_err("MBOX: %s(): " fmt, __FUNCTION__, ## args)
#else
    #define PDEBUG(fmt, args...)
#endif
#define MBOX_ERROR_RETURN(err) do { \
   ccl_syslog_err("Error! %s[%d], error=%i\n", __FUNCTION__, __LINE__, (err)); \
   return err; \
} while (0)

#define MBOX_REG_READ(context, offset, offset_sz, idx, pvalue, size) do { \
    _ret = _mbox_reg_read((context), \
                          (offset), \
                          (offset_sz), (idx), \
                          (pvalue), (size)); \
    if ( _ret ) { \
        PDEBUG("_mbox_reg_read error\n"); \
        MBOX_ERROR_RETURN(_ret); \
    } \
} while (0)

#define MBOX_REG_WRITE(context, offset, offset_sz, idx, pvalue, size) do { \
    _ret = _mbox_reg_write((context), \
                           (offset), \
                           (offset_sz), (idx), \
                           (pvalue), (size)); \
    if ( _ret ) { \
        PDEBUG("_mbox_reg_write error\n"); \
        MBOX_ERROR_RETURN(_ret); \
    } \
} while (0)

/** @name Register Offset Definitions
 * Register offsets within a mbox.
 * @{
 */
#define MBOX_WRITE_REG_OFFSET	         0x00	/**< Mbox write register */
#define MBOX_READ_REG_OFFSET	         0x08	/**< Mbox read register */
#define MBOX_STATUS_REG_OFFSET	         0x10	/**< Mbox status reg  */
#define MBOX_ERROR_REG_OFFSET	         0x14	/**< Mbox Error reg  */
#define MBOX_SIT_REG_OFFSET	             0x18	/**< Mbox send interrupt threshold register */
#define MBOX_RIT_REG_OFFSET	             0x1C	/**< Mbox receive interrupt threshold register */
#define MBOX_IS_REG_OFFSET	             0x20	/**< Mbox interrupt status register */
#define MBOX_IE_REG_OFFSET	             0x24	/**< Mbox interrupt enable register */
#define MBOX_IP_REG_OFFSET	             0x28	/**< Mbox interrupt pending register */
#define MBOX_CTRL_REG_OFFSET	         0x2C	/**< Mbox control register */

#define MBOX_MAX_OFFSET MBOX_CTRL_REG_OFFSET

/*@}*/

/**
 * @name Status register bit definitions
 * These status bits are used to poll the FIFOs
 * @{
 */
#define MBOX_STATUS_FIFO_EMPTY	         0x00000001 /**< Receive FIFO is Empty */
#define MBOX_STATUS_FIFO_FULL	         0x00000002 /**< Send FIFO is Full */
#define MBOX_STATUS_STA		             0x00000004 /**< Send FIFO Threshold Status */
#define MBOX_STATUS_RTA		             0x00000008 /**< Receive FIFO Threshold Status */

/* @} */

/**
 * @name Interrupt Registers(s) bits definitions.
 * The IS, IE, and IP registers all have the same bit definition.
 * @{
 */
#define MBOX_IX_STA		                    0x01    /**< Send Threshold Active, when the           
					                                  *  number of Send FIFO entries is less       
					                                  * than and equal to Send Interrupt           
					                                  * Threshold                                  
                                                      */                                           
#define MBOX_IX_RTA		                    0x02    /**< Receive Threshold Active, when the        
					                                  *  number of Receive FIFO entries is         
					                                  *  greater than Receive Interrupt            
					                                  *  Threshold                                 
                                                      */                                           
#define MBOX_IX_ERR		                    0x04    /**< Mailbox Error, when read on empty or      
					                                  *  write on full                             
                                                      */                                           

/* @} */

/**
 * @name Error bits definition.
 * @{
 */
#define MBOX_ERROR_FIFO_EMPTY	            0x00000001 /**< Receive FIFO is Empty */
#define MBOX_ERROR_FIFO_FULL	            0x00000002 /**< Send FIFO is Full */

/* @} */

/**
 * @name Control register bits definition.
 * @{
 */
#define MBOX_CTRL_RESET_SEND_FIFO       	0x00000001 /**< Clear Send FIFO */
#define MBOX_CTRL_RESET_RECV_FIFO       	0x00000002 /**< Clear Receive FIFO */

/** @struct mbox_ctx_t
 *  mbox context structure
 */
typedef struct mbox_ctx_t {
    void                *devo;
    mbox_brdspec_t      brdspec;
} mbox_ctx_t;

static ccl_err_t    _ret;

/** 
 *  mailboxes cores references
 */
static mbox_ctx_t *_p_mbox[eDEVBRD_MBOX_NUM];

/* mbox register write */
static ccl_err_t _mbox_reg_write(void *p_priv, int offset, 
                                 int offset_sz, int idx, 
                                 b_u8 *p_value, int size)
{
    mbox_ctx_t   *p_mbox;
    i2c_route_t  i2c_route;

    if ( !p_priv || offset > MBOX_MAX_OFFSET || 
         !p_value || idx || size != sizeof(b_u8) ) {
        PDEBUG("Error! Invalid input parameters\n");
        MBOX_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    PDEBUG("123$=> offset=0x%03x, value=0x%02x\n", offset, *p_value);

    p_mbox = (mbox_ctx_t *)p_priv;     
    memcpy(&i2c_route, &p_mbox->brdspec.i2c.bus_id, sizeof(i2c_route_t));
    offset += p_mbox->brdspec.offset;
    _ret = devi2cbus_write_buff(&i2c_route, offset, offset_sz, p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to write over i2c bus\n");
        MBOX_ERROR_RETURN(_ret);
    }

    return _ret;
}

/* mbox register read */
static ccl_err_t _mbox_reg_read(void *p_priv, int offset, 
                                int offset_sz, int idx, 
                                b_u8 *p_value, int size)
{
    mbox_ctx_t   *p_mbox;
    i2c_route_t      i2c_route;

    if ( !p_priv || offset > MBOX_MAX_OFFSET || 
         !p_value || idx || size != sizeof(b_u8) ) {
        PDEBUG("Error! Invalid input parameters\n");
        MBOX_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    PDEBUGG("123$=> offset=0x%03x, size=%d\n", offset, size);

    p_mbox = (mbox_ctx_t *)p_priv;     
    memcpy(&i2c_route, &p_mbox->brdspec.i2c.bus_id, sizeof(i2c_route_t));
    offset += p_mbox->brdspec.offset;
    _ret = devi2cbus_read_buff(&i2c_route, offset, offset_sz, p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to read over i2c bus\n");
        MBOX_ERROR_RETURN(_ret);
    }

    return _ret;
}

ccl_err_t mbox_attr_read(void *p_priv, b_u32 attrid, 
                         b_u32 offset, b_u32 offset_sz, 
                         b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || offset > MBOX_MAX_OFFSET || 
         !p_value || idx || size != sizeof(b_u32) ) {
        PDEBUG("Error! Invalid input parameters\n");
        MBOX_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch (attrid) {
    case eMBOX_READ_REG:
    case eMBOX_STATUS_REG:
    case eMBOX_ERROR_REG:
    case eMBOX_SIT_REG:
    case eMBOX_RIT_REG:
    case eMBOX_IS_REG:
    case eMBOX_IE_REG:
    case eMBOX_IP_REG:
        MBOX_REG_READ(p_priv, offset, offset_sz, idx, p_value, size);
        break;
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK) {
        PDEBUG("Failure in attribute id: %d\n", attrid);
    }

    return _ret;               

}

ccl_err_t mbox_attr_write(void *p_priv, b_u32 attrid, 
                          b_u32 offset, b_u32 offset_sz, 
                          b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || offset > MBOX_MAX_OFFSET || 
         !p_value || idx || size != sizeof(b_u32) ) {
        PDEBUG("Error! Invalid input parameters\n");
        MBOX_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch (attrid) {
    case eMBOX_WRITE_REG:
    case eMBOX_SIT_REG:
    case eMBOX_RIT_REG:
    case eMBOX_IS_REG:
    case eMBOX_IE_REG:
    case eMBOX_CTRL_REG:
        MBOX_REG_WRITE(p_priv, offset, offset_sz, idx, p_value, size);
        break;
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK) {
        PDEBUG("Failure in attribute id: %d\n", attrid);
    }

    return _ret;               

}

/* mbox device attributes definition */
static device_attr_t _attrs[] = {
    {
        .name = "wrdata", .id = eMBOX_WRITE_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = MBOX_WRITE_REG_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "rddata", .id = eMBOX_READ_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = MBOX_READ_REG_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "status", .id = eMBOX_STATUS_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = MBOX_STATUS_REG_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "error", .id = eMBOX_ERROR_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = MBOX_ERROR_REG_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "sit", .id = eMBOX_SIT_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = MBOX_SIT_REG_OFFSET
    },
    {
        .name = "rit", .id = eMBOX_RIT_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = MBOX_RIT_REG_OFFSET
    },
    {
        .name = "is", .id = eMBOX_IS_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = MBOX_IS_REG_OFFSET
    },
    {
        .name = "ie", .id = eMBOX_IE_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = MBOX_IE_REG_OFFSET
    },
    {
        .name = "ip", .id = eMBOX_IP_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = MBOX_IP_REG_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "ctrl", .id = eMBOX_CTRL_REG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = MBOX_CTRL_REG_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
};

/**
 * Checks if the Read FIFO is Empty.
 *
 * @param[in] p_mbox is a pointer to the MBOX
 *      instance to be worked on.
 * @param[out] isempty is the pointer to hold boolean value 
 *       indicating whether FIFO is empty
 * @return error code
 */
static inline ccl_err_t _mbox_is_empty(mbox_ctx_t *p_mbox, b_bool *isempty)              
{
    b_u32 regval;

    MBOX_REG_READ(p_mbox, MBOX_STATUS_REG_OFFSET, 
                  sizeof(b_u8), 0, 
                  (b_u8 *)&regval, sizeof(b_u32));

    *isempty = (regval & MBOX_STATUS_FIFO_EMPTY) ? CCL_TRUE : CCL_FALSE;

    return CCL_OK;
}

/**
 * Checks if there is room in the Write FIFO.
 *
 * @param[in] p_mbox is a pointer to the MBOX
 *      instance to be worked on.
 * @param[out] isfull is a pointer to hold boolean value 
 *       indicating whether FIFO is full
 * @return error code
 */
static inline ccl_err_t _mbox_is_full(mbox_ctx_t *p_mbox, b_bool *isfull)               
{
    b_u32 regval;
    MBOX_REG_READ(p_mbox, MBOX_STATUS_REG_OFFSET, 
                  sizeof(b_u8), 0, 
                  (b_u8 *)&regval, sizeof(b_u32));
    *isfull = (regval & MBOX_STATUS_FIFO_FULL) ? CCL_TRUE : CCL_FALSE;

    return CCL_OK;
}

/**
 * Write a word to MBOX write register.
 *
 * @param[in] p_mbox is a pointer to the MBOX
 *      instance to be worked on.
 * @param[in] val contains the value to be written.
 *
 * @return	None.
 */
static inline ccl_err_t _mbox_write_word(mbox_ctx_t *p_mbox, b_u32 val)
{
    MBOX_REG_WRITE(p_mbox, MBOX_WRITE_REG_OFFSET, 
                   sizeof(b_u8), 0, 
                   (b_u8 *)&val, sizeof(b_u32));
}

/**
 * Read a word from MBOX read FIFO.
 *
 * @param[in] p_mbox is a pointer to the MBOX
 *      instance to be worked on.
 * @param[out] regval is a pointer to hold a 32 bit value read 
 *       from the register
 *
 * @return	The value read from the register, a 32 bit value.
 */
static inline ccl_err_t _mbox_read_word(mbox_ctx_t *p_mbox, b_u32 *regval)
{

    MBOX_REG_WRITE(p_mbox, MBOX_READ_REG_OFFSET, 
                   sizeof(b_u8), 0, 
                   (b_u8 *)regval, sizeof(b_u32));

    return CCL_OK;
}

/**
 * Reads requested bytes from the mailbox referenced by InstancePtr,into the
 * buffer pointed to by the provided pointer. The number of bytes must be a
 * multiple of 4 (bytes)
 *
 * This function is non blocking.
 *
 * @param[in] p_mbox is a pointer to the MBOX
 *      instance to be worked on.
 * @param[in] p_buf is the buffer to read the mailbox contents into,
 *		aligned to a word boundary.
 * @param[in] len is the number of bytes of data requested.
 * @param[out] p_rcvd is the memory that is updated with the number of
 *		bytes of data actually read.
 *
 * @return error code
 *
 * @note
 * On success, the number of bytes read is returned through the pointer.  The
 * call may return with fewer bytes placed in the buffer than requested  (not
 * including zero). This is not necessarily an error condition and indicates
 * the amount of data that was currently available in the mailbox.
 */
static ccl_err_t _mbox_read(mbox_ctx_t *p_mbox, b_u32 *p_buf, b_u32 len, b_u32 *p_rcvd)
{
    b_u32 bytes = 0;
    b_bool isempty;

    if ( !p_mbox || !len || (len % 4) || !p_rcvd ) {
        PDEBUG("Error! Invalid input parameters\n");
        MBOX_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    _ret = _mbox_is_empty(p_mbox, &isempty);
    if (_ret) {
        PDEBUG("_mbox_is_empty error\n");
        MBOX_ERROR_RETURN(_ret);
    }

    /* For memory mapped IO */
    if (isempty) {
        *p_rcvd = 0;
        return CCL_OK;
    }

    /*
     * Read the Mailbox until empty or the length requested is
     * satisfied
     */
    do {
        _ret = _mbox_read_word(p_mbox, p_buf);
        if (_ret) {
            PDEBUG("_mbox_read_word error\n");
            MBOX_ERROR_RETURN(_ret);
        }
        p_buf++;
        bytes += 4;
        _ret = _mbox_is_empty(p_mbox, &isempty);
        if (_ret) {
            PDEBUG("_mbox_is_empty error\n");
            MBOX_ERROR_RETURN(_ret);
        }
    } while ((bytes != len) &&
             !isempty);

    *p_rcvd = bytes;

    return CCL_OK;
}

/**
 *
 * Reads requested bytes from the mailbox referenced by InstancePtr,into the
 * buffer pointed to by the provided pointer. The number of bytes must be a
 * multiple of 4 (bytes).
 *
 * @param[in] p_mbox is a pointer to the MBOX
 *      instance to be worked on.
 * @param[in] p_buf is the buffer to read the mailbox contents into,
 *		aligned to a word boundary.
 * @param[in] len is the number of bytes of data requested.
 *
 * @return	error code.
 */
static ccl_err_t _mbox_read_blocking(mbox_ctx_t *p_mbox, b_u32 *p_buf, b_u32 len)
{
    b_u32 bytes = 0;
    b_bool isempty;

    if ( !p_mbox || !len || (len % 4) ) {
        PDEBUG("Error! Invalid input parameters\n");
        MBOX_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    _ret = _mbox_is_empty(p_mbox, &isempty);
    if (_ret) {
        PDEBUG("_mbox_is_empty error\n");
        MBOX_ERROR_RETURN(_ret);
    }

    /* Block while the mailbox FIFO has at-least some data */
    do {
        while (isempty) {
            _ret = _mbox_is_empty(p_mbox, &isempty);
            if (_ret) {
                PDEBUG("_mbox_is_empty error\n");
                MBOX_ERROR_RETURN(_ret);
            }
        }
        /*
         * Read the Mailbox until empty or the length
         * requested is satisfied
         */
        _ret = _mbox_read_word(p_mbox, p_buf);
        if (_ret) {
            PDEBUG("_mbox_read_word error\n");
            MBOX_ERROR_RETURN(_ret);
        }
        p_buf++;
        bytes += 4;
    } while (bytes != len);

    return CCL_OK;
}

/**
 * Writes the requested bytes from the buffer pointed to by the provided
 * pointer into the mailbox referenced by p_mbox.The number of 
 * bytes must be a multiple of 4 (bytes)
 *
 * This function is non blocking.
 *
 * @param[in] p_mbox is a pointer to the MBOX
 *      instance to be worked on.
 * @param[in] p_buf is the source data buffer, aligned to a word
 *		boundary.
 * @param[in] len is the number of bytes requested to be written.
 * @param[out] p_xmit points to memory which is updated with the actual
 *       number of bytes written, return value.
 *  
 * @return error code
 *
 * @note 
 * On success, the number of bytes successfully written into the destination
 * mailbox is returned in the provided pointer. The call may  return with
 * zero. This is not necessarily an error condition and indicates that the
 * mailbox is currently full.
 */
static ccl_err_t _mbox_write(mbox_ctx_t *p_mbox, b_u32 *p_buf, b_u32 len, b_u32 *p_xmit)
{
    b_u32 bytes = 0;
    b_bool isfull;

    if ( !p_mbox || !len || (len % 4) || !p_xmit ) {
        PDEBUG("Error! Invalid input parameters\n");
        MBOX_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    _ret = _mbox_is_full(p_mbox, &isfull);
    if (_ret) {
        PDEBUG("_mbox_is_full error\n");
        MBOX_ERROR_RETURN(_ret);
    }

    if (isfull) {
        *p_xmit = 0;
        return CCL_OK;
    }

    /*
     * Write to the Mailbox until full or the length requested is
     * satisfied.
     */

    do {
        _ret = _mbox_write_word(p_mbox, *p_buf++);
        if (_ret) {
            PDEBUG("_mbox_write_word error\n");
            MBOX_ERROR_RETURN(_ret);
        }
        bytes += 4;
        _ret = _mbox_is_full(p_mbox, &isfull);
        if (_ret) {
            PDEBUG("_mbox_is_empty error\n");
            MBOX_ERROR_RETURN(_ret);
        }
    } while ((bytes != len) &&
             !isfull);

    *p_xmit = bytes;

    return CCL_OK;
}

/**
 * Writes the requested bytes from the buffer pointed to by the provided
 * pointer into the mailbox referenced by p_mbox. The number of bytes must
 * be a multiple of 4 (bytes)
 *
 * @param[in] p_mbox is a pointer to the MBOX
 *      instance to be worked on.
 * @param[in] p_buf is the source data buffer, aligned to a word
 *		boundary.
 * @param[in] len is the number of bytes requested to be written.
 *
 * @return	error code
 *
 * @note 
 * The call blocks until the number of bytes requested are 
 * written. The provided buffer pointed to by p_buf must be
 * aligned to a word boundary.
 */
static ccl_err_t _mbox_write_blocking(mbox_ctx_t *p_mbox, b_u32 *p_buf, b_u32 len)
{
    b_u32 bytes = 0;
    b_bool isfull;

    if ( !p_mbox || !len || (len % 4) ) {
        PDEBUG("Error! Invalid input parameters\n");
        MBOX_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    _ret = _mbox_is_full(p_mbox, &isfull);
    if (_ret) {
        PDEBUG("_mbox_is_full error\n");
        MBOX_ERROR_RETURN(_ret);
    }
    /* Block while the mailbox FIFO becomes free to transfer
     * at-least one word
     */
    do {
        while (isfull) {
            _ret = _mbox_is_full(p_mbox, &isfull);
            if (_ret) {
                PDEBUG("_mbox_is_full error\n");
                MBOX_ERROR_RETURN(_ret);
            }
        }
        _ret = _mbox_write_word(p_mbox, *p_buf++);
        if (_ret) {
            PDEBUG("_mbox_write_word error\n");
            MBOX_ERROR_RETURN(_ret);
        }

        bytes += 4;
    } while (bytes != len);

    return CCL_OK;
}

/* Device instance hw-specific initializations 
 */
static ccl_err_t _fmbox_add(void *devo, void *brdspec) {
    mbox_ctx_t       *p_mbox;
    mbox_brdspec_t   *p_brdspec;

    if ( !devo || !brdspec ) {
        PDEBUG("Error! Invalid input parameters\n");
        MBOX_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_mbox = devman_device_private(devo);
    if ( !p_mbox ) {
        PDEBUG("NULL context area\n");
        MBOX_ERROR_RETURN(CCL_FAIL);
    }

    p_brdspec = (mbox_brdspec_t *)brdspec;
    /* back reference the device manager object handle */    
    p_mbox->devo = devo;    
    /* save the board specific info */
    memcpy(&p_mbox->brdspec, p_brdspec, sizeof(mbox_brdspec_t));

    /* store the context */
    _p_mbox[p_brdspec->dev_id] = p_mbox;

    return CCL_OK;
}

/* Device instance hw-specific deinitializations 
 */
static ccl_err_t _fmbox_cut(void *devo)
{
    mbox_ctx_t  *p_mbox;
    if ( !devo ) {
        PDEBUG("Error! Invalid input parameters\n");
        MBOX_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_mbox = devman_device_private(devo);
    if ( !p_mbox ) {
        PDEBUG("NULL context area\n");
        MBOX_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

/* device type description structure */
static device_driver_t _mbox_driver = {
    .name = "mbox",
    .f_add = _fmbox_add,
    .f_cut = _fmbox_cut,
    .attrs_num = sizeof(_attrs)/sizeof(_attrs[0]),
                 .attrs = _attrs,
    .priv_size = sizeof(mbox_ctx_t)
}; 

/* Initialize MBOX device type
 */
ccl_err_t devmbox_init(void)
{
    /* register device type */
    _ret = devman_driver_register(&_mbox_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to register driver\n");
        MBOX_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

/* Deinitialize MBOX device type
 */
ccl_err_t devmbox_fini(void)
{
    /* unregister device type */
    _ret = devman_driver_unregister(&_mbox_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to unregister driver\n");
        MBOX_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}
