/********************************************************************************/
/**
 * @file sysmon.c
 * @brief UltraScale System Monitor (SYSMON) device manager driver
 * @copyright 2019 CELARE. All rights reserved.
 */
/********************************************************************************/

#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <semaphore.h>
#include <linux/limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "spibus.h"
#include "sysmon.h"

//#define SYSMON_DEBUG
/* pointing/error-returning macros */
#ifdef SYSMON_DEBUG
 #define PDEBUG(fmt, args...) \
         ccl_syslog_err("SYSMON_DEBUG:  %s[%d]: " fmt, __FUNCTION__, __LINE__, ## args)
#else
 #define PDEBUG(fmt, args...)
#endif
#define SYSMON_ERROR_RETURN(err) do { \
   ccl_syslog_err("Error! %s[%d], error=%i\n", __FUNCTION__, __LINE__, (err)); \
   return err; \
} while (0)

#define SYSMON_REG_READ(context, offset, offset_sz, idx, pvalue, size) do { \
    _ret = _sysmon_reg_read((context), \
                            (offset), \
                            (offset_sz), (idx), \
                            (pvalue), (size)); \
    if ( _ret ) { \
        PDEBUG("_sysmon_reg_read error\n"); \
        SYSMON_ERROR_RETURN(_ret); \
    } \
} while (0)

#define SYSMON_REG_WRITE(context, offset, offset_sz, idx, pvalue, size) do { \
    _ret = _sysmon_reg_write((context), \
                             (offset), \
                             (offset_sz), (idx), \
                             (pvalue), (size)); \
    if ( _ret ) { \
        PDEBUG("_sysmon_reg_write error\n"); \
        SYSMON_ERROR_RETURN(_ret); \
    } \
} while (0)

/**@name Register offsets of the SYSMON
 *
 * The following constants provide access to each of the registers of
 * the SYSMON device.
 * @{
 */

#define SYSMON_GIER_OFFSET                  0x5C
#define SYSMON_IPISR_OFFSET                 0x60
#define SYSMON_IPIER_OFFSET                 0x68
#define SYSMON_TEMP_OFFSET		            0xC00 /**< On-chip Temperature Reg */
#define SYSMON_VCCINT_OFFSET		        0xC04 /**< On-chip VCCINT data Reg */
#define SYSMON_VCCAUX_OFFSET		        0xC08 /**< On-chip VCCAUX data Reg */
#define SYSMON_VPVN_OFFSET		            0xC0C /**< ADC out of VP/VN */
#define SYSMON_VREFP_OFFSET		            0xC10 /**< On-chip VREFP data Reg */
#define SYSMON_VREFN_OFFSET		            0xC14 /**< On-chip VREFN data Reg */
#define SYSMON_VBRAM_OFFSET		            0xC18 /**< On-chip VBRAM , 7 Series */
#define SYSMON_SUPPLY_CALIB_OFFSET          0xC20 /**< ADC A Supply Offset Reg */
#define SYSMON_ADC_CALIB_OFFSET             0xC24 /**< ADC A Offset data Reg */
#define SYSMON_GAINERR_CALIB_OFFSET         0xC28 /**< ADC A Gain Error Reg */

/*
 * SYSMON External channel Registers
 */
#define SYSMON_AUX00_OFFSET	0xC40 /**< ADC out of VAUXP0/VAUXN0 */
#define SYSMON_AUX01_OFFSET	0xC44 /**< ADC out of VAUXP1/VAUXN1 */
#define SYSMON_AUX02_OFFSET	0xC48 /**< ADC out of VAUXP2/VAUXN2 */
#define SYSMON_AUX03_OFFSET	0xC4C /**< ADC out of VAUXP3/VAUXN3 */
#define SYSMON_AUX04_OFFSET	0xC50 /**< ADC out of VAUXP4/VAUXN4 */
#define SYSMON_AUX05_OFFSET	0xC54 /**< ADC out of VAUXP5/VAUXN5 */
#define SYSMON_AUX06_OFFSET	0xC58 /**< ADC out of VAUXP6/VAUXN6 */
#define SYSMON_AUX07_OFFSET	0xC5C /**< ADC out of VAUXP7/VAUXN7 */
#define SYSMON_AUX08_OFFSET	0xC60 /**< ADC out of VAUXP8/VAUXN8 */
#define SYSMON_AUX09_OFFSET	0xC64 /**< ADC out of VAUXP9/VAUXN9 */
#define SYSMON_AUX10_OFFSET	0xC68 /**< ADC out of VAUXP10/VAUXN10 */
#define SYSMON_AUX11_OFFSET	0xC6C /**< ADC out of VAUXP11/VAUXN11 */
#define SYSMON_AUX12_OFFSET	0xC70 /**< ADC out of VAUXP12/VAUXN12 */
#define SYSMON_AUX13_OFFSET	0xC74 /**< ADC out of VAUXP13/VAUXN13 */
#define SYSMON_AUX14_OFFSET	0xC78 /**< ADC out of VAUXP14/VAUXN14 */
#define SYSMON_AUX15_OFFSET	0xC7C /**< ADC out of VAUXP15/VAUXN15 */

/*
 * SYSMON Registers for Maximum/Minimum data captured for the
 * on chip Temperature/VCCINT/VCCAUX data.
 */
#define SYSMON_MAX_TEMP_OFFSET		    0xC80 /**< Max Temperature Reg */
#define SYSMON_MAX_VCCINT_OFFSET	    0xC84 /**< Max VCCINT Register */
#define SYSMON_MAX_VCCAUX_OFFSET	    0xC88 /**< Max VCCAUX Register */
#define SYSMON_MAX_VCCBRAM_OFFSET	    0xC8C /**< Max BRAM Register, 7 series */
#define SYSMON_MIN_TEMP_OFFSET		    0xC90 /**< Min Temperature Reg */
#define SYSMON_MIN_VCCINT_OFFSET	    0xC94 /**< Min VCCINT Register */
#define SYSMON_MIN_VCCAUX_OFFSET	    0xC98 /**< Min VCCAUX Register */
#define SYSMON_MIN_VCCBRAM_OFFSET	    0xC9C /**< Min BRAM Register, 7 series */
 /* Undefined 0x2F to 0x3E */
#define SYSMON_FLAG_OFFSET		        0x3F /**< Flag Register */

/*
 * SYSMON Configuration Registers
 */
#define SYSMON_CFR0_OFFSET	0xD00	/**< Configuration Register 0 */
#define SYSMON_CFR1_OFFSET	0xD04	/**< Configuration Register 1 */
#define SYSMON_CFR2_OFFSET	0xD08	/**< Configuration Register 2 */

/* Test Registers 0x43 to 0x47 */

/*
 * SYSMON Sequence Registers
 */
#define SYSMON_SEQ00_OFFSET	0xD20 /**< Seq Reg 00 Adc channel Selection */
#define SYSMON_SEQ01_OFFSET	0xD24 /**< Seq Reg 01 Adc channel Selection */
#define SYSMON_SEQ02_OFFSET	0xD28 /**< Seq Reg 02 Adc average Enable */
#define SYSMON_SEQ03_OFFSET	0xD2C /**< Seq Reg 03 Adc average Enable */
#define SYSMON_SEQ04_OFFSET	0xD30 /**< Seq Reg 04 Adc Input mode Select */
#define SYSMON_SEQ05_OFFSET	0xD34 /**< Seq Reg 05 Adc Input mode Select */
#define SYSMON_SEQ06_OFFSET	0xD38 /**< Seq Reg 06 Adc Acquisition Select */
#define SYSMON_SEQ07_OFFSET	0xD3C /**< Seq Reg 07 Adc Acquisition Select */

/*
 * SYSMON Alarm Threshold/Limit Registers (ATR)
 */
#define SYSMON_ATR_TEMP_UPPER_OFFSET	    0xD40 /**< Temp Upper Alarm Register */
#define SYSMON_ATR_VCCINT_UPPER_OFFSET	    0xD44 /**< VCCINT Upper Alarm Reg */
#define SYSMON_ATR_VCCAUX_UPPER_OFFSET	    0xD48 /**< VCCAUX Upper Alarm Reg */
#define SYSMON_ATR_OT_UPPER_OFFSET	        0xD4C /**< Over Temp Upper Alarm Reg */
#define SYSMON_ATR_TEMP_LOWER_OFFSET	    0xD50 /**< Temp Lower Alarm Register */
#define SYSMON_ATR_VCCINT_LOWER_OFFSET	    0xD54 /**< VCCINT Lower Alarm Reg */
#define SYSMON_ATR_VCCAUX_LOWER_OFFSET	    0xD58 /**< VCCAUX Lower Alarm Reg */
#define SYSMON_ATR_OT_LOWER_OFFSET	        0xD5C /**< Over Temp Lower Alarm Reg */
#define SYSMON_ATR_VBRAM_UPPER_OFFSET	    0xD60 /**< VBRAM Upper Alarm, 7 series */
#define SYSMON_ATR_VBRAM_LOWER_OFFSET	    0xD70 /**< VRBAM Lower Alarm, 7 Series */

#define SYSMON_MAX_OFFSET SYSMON_ATR_VBRAM_LOWER_OFFSET

/**
 * @name Configuration Register 0 (CFR0) mask(s)
 * @{
 */
#define SYSMON_CFR0_CAL_AVG_MASK	    0x8000 /**< Averaging enable Mask */
#define SYSMON_CFR0_AVG_VALID_MASK	    0x3000 /**< Averaging bit Mask */
#define SYSMON_CFR0_AVG1_MASK		    0x0000 /**< No Averaging */
#define SYSMON_CFR0_AVG16_MASK		    0x1000 /**< average 16 samples */
#define SYSMON_CFR0_AVG64_MASK	 	    0x2000 /**< average 64 samples */
#define SYSMON_CFR0_AVG256_MASK 	    0x3000 /**< average 256 samples */
#define SYSMON_CFR0_AVG_SHIFT	 	    12     /**< Averaging bits shift */
#define SYSMON_CFR0_MUX_MASK	 	    0x0800 /**< External Mask Enable */
#define SYSMON_CFR0_DU_MASK	 	        0x0400 /**< Bipolar/Unipolar mode */
#define SYSMON_CFR0_EC_MASK	 	        0x0200 /**< Event driven/
						                         *  Continuous mode selection
						                         */
#define SYSMON_CFR0_ACQ_MASK	 	    0x0100 /**< Add acquisition by 6 ADCCLK */
#define SYSMON_CFR0_CHANNEL_MASK	    0x001F /**< channel number bit Mask */

/*@}*/

/**
 * @name Configuration Register 1 (CFR1) mask(s)
 * @{
 */
#define SYSMON_CFR1_SEQ_VALID_MASK	             0xF000 /**< Sequence bit Mask */
#define SYSMON_CFR1_SEQ_SAFEMODE_MASK	         0x0000 /**< Default Safe mode */
#define SYSMON_CFR1_SEQ_ONEPASS_MASK	         0x1000 /**< Onepass through Seq */
#define SYSMON_CFR1_SEQ_CONTINPASS_MASK	         0x2000 /**< Continuous Cycling Seq */
#define SYSMON_CFR1_SEQ_SINGCHAN_MASK	         0x3000 /**< Single channel - No Seq */
#define SYSMON_CFR1_SEQ_SIMUL_SAMPLING_MASK      0x4000 /**< Simulataneous Sampling Mask */
#define SYSMON_CFR1_SEQ_INDEPENDENT_MASK         0x8000 /**< Independent mode */
#define SYSMON_CFR1_SEQ_SHIFT     		         12     /**< Sequence bit shift */
#define SYSMON_CFR1_ALM_VCCPDRO_MASK	         0x0800 /**< Alm 6 - VCCPDRO, Zynq  */
#define SYSMON_CFR1_ALM_VCCPAUX_MASK	         0x0400 /**< Alm 5 - VCCPAUX, Zynq */
#define SYSMON_CFR1_ALM_VCCPINT_MASK	         0x0200 /**< Alm 4 - VCCPINT, Zynq */
#define SYSMON_CFR1_ALM_VBRAM_MASK	             0x0100 /**< Alm 3 - VBRAM, 7 series */
#define SYSMON_CFR1_CAL_VALID_MASK	             0x00F0 /**< Valid calibration Mask */
#define SYSMON_CFR1_CAL_PS_GAIN_OFFSET_MASK      0x0080 /**< calibration 3 -Power
							                              *  Supply Gain/Offset Enable 
                                                          */
#define SYSMON_CFR1_CAL_PS_OFFSET_MASK	         0x0040 /**< calibration 2 - Power
							                              *  Supply Offset Enable 
                                                          */
#define SYSMON_CFR1_CAL_ADC_GAIN_OFFSET_MASK     0x0020 /**< calibration 1 -ADC Gain
							                              *  Offset Enable 
                                                          */
#define SYSMON_CFR1_CAL_ADC_OFFSET_MASK	         0x0010 /**< calibration 0 -ADC Offset
							                              *  Enable 
                                                          */
#define SYSMON_CFR1_CAL_DISABLE_MASK	         0x0000 /**< No calibration */
#define SYSMON_CFR1_ALM_ALL_MASK	             0x0F0F /**< Mask for all alarms */
#define SYSMON_CFR1_ALM_VCCAUX_MASK	             0x0008 /**< Alarm 2 - VCCAUX Enable */
#define SYSMON_CFR1_ALM_VCCINT_MASK	             0x0004 /**< Alarm 1 - VCCINT Enable */
#define SYSMON_CFR1_ALM_TEMP_MASK	             0x0002 /**< Alarm 0 - Temperature */
#define SYSMON_CFR1_OT_MASK		                 0x0001 /**< Over Temperature Enable */

/*@}*/

/**
 * @name Configuration Register 2 (CFR2) mask(s)
 * @{
 */
#define SYSMON_CFR2_CD_VALID_MASK	             0xFF00  /**< Clock divisor bit Mask   */
#define SYSMON_CFR2_CD_SHIFT		             8	     /**< Num of shift on division */
#define SYSMON_CFR2_CD_MIN		                 8	     /**< Minimum value of divisor */
#define SYSMON_CFR2_CD_MAX		                 255	 /**< Maximum value of divisor */

#define SYSMON_CFR2_CD_MIN		                 8	     /**< Minimum value of divisor */
#define SYSMON_CFR2_PD_MASK		                 0x0030	 /**< Power Down Mask */
#define SYSMON_CFR2_PD_SYSMON_MASK	             0x0030	 /**< Power Down SYSMON Mask */
#define SYSMON_CFR2_PD_ADC1_MASK	             0x0020	 /**< Power Down ADC1 Mask */
#define SYSMON_CFR2_PD_SHIFT		             4	     /**< Power Down Shift */
/*@}*/

/**
 * @name Sequence Register (SEQ) Bit Definitions
 * @{
 */
#define SYSMON_SEQ_CH_CALIB	    0x00000001 /**< ADC calibration channel */
#define SYSMON_SEQ_CH_VCCPINT	0x00000020 /**< VCCPINT, Zynq Only */
#define SYSMON_SEQ_CH_VCCPAUX	0x00000040 /**< VCCPAUX, Zynq Only */
#define SYSMON_SEQ_CH_VCCPDRO	0x00000080 /**< VCCPDRO, Zynq Only */
#define SYSMON_SEQ_CH_TEMP	    0x00000100 /**< On Chip Temperature channel */
#define SYSMON_SEQ_CH_VCCINT	0x00000200 /**< VCCINT channel */
#define SYSMON_SEQ_CH_VCCAUX	0x00000400 /**< VCCAUX channel */
#define SYSMON_SEQ_CH_VPVN	    0x00000800 /**< VP/VN analog inputs channel */
#define SYSMON_SEQ_CH_VREFP	    0x00001000 /**< VREFP channel */
#define SYSMON_SEQ_CH_VREFN	    0x00002000 /**< VREFN channel */
#define SYSMON_SEQ_CH_VBRAM	    0x00004000 /**< VBRAM channel, 7 series */
#define SYSMON_SEQ_CH_AUX00	    0x00010000 /**< 1st Aux channel */
#define SYSMON_SEQ_CH_AUX01	    0x00020000 /**< 2nd Aux channel */
#define SYSMON_SEQ_CH_AUX02	    0x00040000 /**< 3rd Aux channel */
#define SYSMON_SEQ_CH_AUX03	    0x00080000 /**< 4th Aux channel */
#define SYSMON_SEQ_CH_AUX04	    0x00100000 /**< 5th Aux channel */  
#define SYSMON_SEQ_CH_AUX05	    0x00200000 /**< 6th Aux channel */  
#define SYSMON_SEQ_CH_AUX06	    0x00400000 /**< 7th Aux channel */  
#define SYSMON_SEQ_CH_AUX07	    0x00800000 /**< 8th Aux channel */  
#define SYSMON_SEQ_CH_AUX08	    0x01000000 /**< 9th Aux channel */  
#define SYSMON_SEQ_CH_AUX09	    0x02000000 /**< 10th Aux channel */ 
#define SYSMON_SEQ_CH_AUX10	    0x04000000 /**< 11th Aux channel */ 
#define SYSMON_SEQ_CH_AUX11	    0x08000000 /**< 12th Aux channel */ 
#define SYSMON_SEQ_CH_AUX12	    0x10000000 /**< 13th Aux channel */ 
#define SYSMON_SEQ_CH_AUX13	    0x20000000 /**< 14th Aux channel */ 
#define SYSMON_SEQ_CH_AUX14	    0x40000000 /**< 15th Aux channel */ 
#define SYSMON_SEQ_CH_AUX15	    0x80000000 /**< 16th Aux channel */ 

#define SYSMON_SEQ00_CH_VALID_MASK  0x7FE1 /**< Mask for the valid channels */
#define SYSMON_SEQ01_CH_VALID_MASK  0xFFFF /**< Mask for the valid channels */

#define SYSMON_SEQ02_CH_VALID_MASK  0x7FE0 /**< Mask for the valid channels */
#define SYSMON_SEQ03_CH_VALID_MASK  0xFFFF /**< Mask for the valid channels */

#define SYSMON_SEQ04_CH_VALID_MASK  0x0800 /**< Mask for the valid channels */
#define SYSMON_SEQ05_CH_VALID_MASK  0xFFFF /**< Mask for the valid channels */

#define SYSMON_SEQ06_CH_VALID_MASK  0x0800 /**< Mask for the valid channels */
#define SYSMON_SEQ07_CH_VALID_MASK  0xFFFF /**< Mask for the valid channels */


#define SYSMON_SEQ_CH_AUX_SHIFT	    16     /**< Shift for the Aux channel */

/*@}*/

/**
 * @name OT Upper Alarm Threshold Register Bit Definitions
 * @{
 */

#define SYSMON_ATR_OT_UPPER_ENB_MASK	0x000F /**< Mask for OT enable */
#define SYSMON_ATR_OT_UPPER_VAL_MASK	0xFFF0 /**< Mask for OT value */
#define SYSMON_ATR_OT_UPPER_VAL_SHIFT	4      /**< Shift for OT value */
#define SYSMON_ATR_OT_UPPER_ENB_VAL	    0x0003 /**< value for OT enable */
#define SYSMON_ATR_OT_UPPER_VAL_MAX	    0x0FFF /**< Max OT value */

/*@}*/


/**
 * @name JTAG DRP Bit Definitions
 * @{
 */
#define SYSMON_JTAG_DATA_MASK		    0x0000FFFF /**< Mask for the data */
#define SYSMON_JTAG_ADDR_MASK		    0x03FF0000 /**< Mask for the Addr */
#define SYSMON_JTAG_ADDR_SHIFT		    16	       /**< Shift for the Addr */
#define SYSMON_JTAG_CMD_MASK		    0x3C000000 /**< Mask for the Cmd */
#define SYSMON_JTAG_CMD_WRITE_MASK	    0x08000000 /**< Mask for CMD Write */
#define SYSMON_JTAG_CMD_READ_MASK	    0x04000000 /**< Mask for CMD Read */
#define SYSMON_JTAG_CMD_SHIFT		    26	       /**< Shift for the Cmd */

/*@}*/

/** @name Unlock Register Definitions
  * @{
 */
#define SYSMON_UNLK_OFFSET	     0x034      /**< Unlock Register */
#define SYSMON_UNLK_VALUE	     0x757BDF0D /**< Unlock value */

/* @} */
/**
 * @name Indexes for the different channels.
 * @{
 */
#define SYSMON_CH_TEMP		        0x0  /**< On Chip Temperature */
#define SYSMON_CH_VCCINT	        0x1  /**< VCCINT */
#define SYSMON_CH_VCCAUX	        0x2  /**< VCCAUX */
#define SYSMON_CH_VPVN		        0x3  /**< VP/VN Dedicated analog inputs */     
#define SYSMON_CH_VREFP		        0x4  /**< VREFP */                             
#define SYSMON_CH_VREFN		        0x5  /**< VREFN */                             
#define SYSMON_CH_VBRAM		        0x6  /**< On-chip VBRAM Data Reg, 7 series */  
#define SYSMON_CH_SUPPLY_CALIB	    0x07 /**< Supply Calib Data Reg */             
#define SYSMON_CH_ADC_CALIB	        0x08 /**< ADC Offset Channel Reg */            
#define SYSMON_CH_GAINERR_CALIB     0x09 /**< Gain Error Channel Reg  */
#define SYSMON_CH_VCCPINT	        0x0D /**< On-chip PS VCCPINT Channel , Zynq */
#define SYSMON_CH_VCCPAUX	        0x0E /**< On-chip PS VCCPAUX Channel , Zynq */
#define SYSMON_CH_VCCPDRO	        0x0F /**< On-chip PS VCCPDRO Channel , Zynq */
#define SYSMON_CH_AUX_MIN	        16   /**< Channel number for 1st Aux Channel */
#define SYSMON_CH_AUX_MAX	        31   /**< Channel number for Last Aux channel */

/*@}*/


/**
 * @name Indexes for reading the calibration Coefficient Data.
 * @{
 */
#define SYSMON_CALIB_SUPPLY_COEFF     0 /**< Supply Offset Calib Coefficient */
#define SYSMON_CALIB_ADC_COEFF        1 /**< ADC Offset Calib Coefficient */
#define SYSMON_CALIB_GAIN_ERROR_COEFF 2 /**< Gain Error Calib Coefficient*/
/*@}*/


/**
 * @name Indexes for reading the Minimum/Maximum Measurement Data.
 * @{
 */
#define SYSMON_MAX_TEMP		    0   /**< Maximum Temperature Data */
#define SYSMON_MAX_VCCINT	    1   /**< Maximum VCCINT Data */
#define SYSMON_MAX_VCCAUX	    2   /**< Maximum VCCAUX Data */
#define SYSMON_MAX_VBRAM	    3   /**< Maximum VBRAM Data */
#define SYSMON_MIN_TEMP		    4   /**< Minimum Temperature Data */
#define SYSMON_MIN_VCCINT	    5   /**< Minimum VCCINT Data */
#define SYSMON_MIN_VCCAUX	    6   /**< Minimum VCCAUX Data */
#define SYSMON_MIN_VBRAM	    7   /**< Minimum VBRAM Data */
#define SYSMON_MAX_VCCPINT	    8   /**< Maximum VCCPINT Register , Zynq */  
#define SYSMON_MAX_VCCPAUX	    9   /**< Maximum VCCPAUX Register , Zynq */  
#define SYSMON_MAX_VCCPDRO	    0xA /**< Maximum VCCPDRO Register , Zynq */  
#define SYSMON_MIN_VCCPINT	    0xC /**< Minimum VCCPINT Register , Zynq */  
#define SYSMON_MIN_VCCPAUX	    0xD /**< Minimum VCCPAUX Register , Zynq */  
#define SYSMON_MIN_VCCPDRO	    0xE /**< Minimum VCCPDRO Register , Zynq */  

/*@}*/


/**
 * @name Alarm Threshold(Limit) Register (ATR) indexes.
 * @{
 */
#define SYSMON_ATR_TEMP_UPPER	    0 /**< High user Temperature */                  
#define SYSMON_ATR_VCCINT_UPPER     1 /**< VCCINT high voltage limit register */     
#define SYSMON_ATR_VCCAUX_UPPER     2 /**< VCCAUX high voltage limit register */     
#define SYSMON_ATR_OT_UPPER	        3 /**< VCCAUX high voltage limit register */     
#define SYSMON_ATR_TEMP_LOWER	    4 /**< Upper Over Temperature limit Reg */       
#define SYSMON_ATR_VCCINT_LOWER     5 /**< VCCINT high voltage limit register */     
#define SYSMON_ATR_VCCAUX_LOWER     6 /**< VCCAUX low voltage limit register  */     
#define SYSMON_ATR_OT_LOWER	        7 /**< Lower Over Temperature limit */           
#define SYSMON_ATR_VBRAM_UPPER_     8 /**< VRBAM Upper Alarm Reg, 7 Series */        
#define SYSMON_ATR_VCCPINT_UPPER    9 /**< VCCPINT Upper Alarm Reg, Zynq */          
#define SYSMON_ATR_VCCPAUX_UPPER    0xA /**< VCCPAUX Upper Alarm Reg, Zynq */        
#define SYSMON_ATR_VCCPDRO_UPPER    0xB /**< VCCPDRO Upper Alarm Reg, Zynq */        
#define SYSMON_ATR_VBRAM_LOWER      0xC /**< VRBAM Lower Alarm Reg, 7 Series */      
#define SYSMON_ATR_VCCPINT_LOWER    0xD /**< VCCPINT Lower Alarm Reg , Zynq */       
#define SYSMON_ATR_VCCPAUX_LOWER    0xE /**< VCCPAUX Lower Alarm Reg , Zynq */       
#define SYSMON_ATR_VCCPDRO_LOWER    0xF /**< VCCPDRO Lower Alarm Reg , Zynq */       

/*@}*/


/**
 * @name Averaging to be done for the channels.
 * @{
 */
#define SYSMON_AVG_0_SAMPLES	    0  /**< No Averaging */            
#define SYSMON_AVG_16_SAMPLES	    1  /**< average 16 samples */      
#define SYSMON_AVG_64_SAMPLES	    2  /**< average 64 samples */      
#define SYSMON_AVG_256_SAMPLES	    3  /**< average 256 samples */

/*@}*/


/**
 * @name Channel Sequencer modes of operation
 * @{
 */
#define SYSMON_SEQ_MODE_SAFE		        0  /**< Default Safe mode */
#define SYSMON_SEQ_MODE_ONEPASS		        1  /**< Onepass through Sequencer */
#define SYSMON_SEQ_MODE_CONTINPASS	        2  /**< Continuous Cycling Sequencer */
#define SYSMON_SEQ_MODE_SINGCHAN	        3  /**< Single channel -No Sequencing */
#define SYSMON_SEQ_MODE_SIMUL_SAMPLING	    4  /**< Simultaneous sampling */
#define SYSMON_SEQ_MODE_INDEPENDENT	        8  /**< Independent mode */

/*@}*/



/**
 * @name Power Down modes
 * @{
 */
#define SYSMON_PD_MODE_NONE   	    0  /**< No Power Down  */
#define SYSMON_PD_MODE_ADCB		    1  /**< Power Down ADC B */
#define SYSMON_PD_MODE_SYSMON		2  /**< Power Down ADC A and ADC B */
/*@}*/

#define SYSMON_CALC_TEMPERATURE(regval) \
    ((b_double64)((((regval) >> 6) & 0x3ff)*501.3743)/1024 - 273.6777)

#define SYSMON_CALC_VOLTAGE(regval) \
    ((b_double64)((((regval) >> 6) & 0x3ff)*3)/1024)

/** @struct sysmon_ctx_t
 *  sysmon context structure
 */
typedef struct sysmon_ctx_t {
    void                  *devo;
    sysmon_brdspec_t      brdspec;
} sysmon_ctx_t;

static ccl_err_t   _ret;
static ccl_err_t   _sysmon_init(void *p_priv);

/* sysmon register read */
static ccl_err_t _sysmon_reg_read(void *p_priv, 
                                  b_u32 offset, 
                                  __attribute__((unused)) b_u32 offset_sz, 
                                  b_u32 idx, b_u8 *p_value, b_u32 size)
{
    sysmon_ctx_t *p_sysmon;

    if ( !p_priv || offset > SYSMON_MAX_OFFSET || 
         !p_value || idx || size != sizeof(b_u32) ) {
        PDEBUG("Error! Invalid input parameters\n");
        SYSMON_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    PDEBUGG("123$=> offset=0x%03x, size=%d\n", offset, size);

    p_sysmon = (sysmon_ctx_t *)p_priv;
    offset += p_sysmon->brdspec.offset;

    _ret = devspibus_read_buff(p_sysmon->brdspec.devname, offset, idx, p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to read over spi bus\n");
        SYSMON_ERROR_RETURN(_ret);
    }    

    PDEBUG("p_sysmon->brdspec.offset=0x%x, offset=0x%04x, value=0x%08x\n", 
           p_sysmon->brdspec.offset, offset, *(b_u32 *)p_value);

    return _ret;
}

/* sysmon register write */
static ccl_err_t _sysmon_reg_write(void *p_priv, 
                                   b_u32 offset, 
                                   __attribute__((unused)) b_u32 offset_sz, 
                                   b_u32 idx, b_u8 *p_value, b_u32 size)
{
    sysmon_ctx_t *p_sysmon;

    if ( !p_priv || offset > SYSMON_MAX_OFFSET || 
         !p_value || idx || size != sizeof(b_u32) ) {
        PDEBUG("Error! Invalid input parameters\n");
        SYSMON_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    PDEBUGG("123$=> offset=0x%03x, value=0x%08x\n", offset, *p_value);

    p_sysmon = (sysmon_ctx_t *)p_priv;
    offset += p_sysmon->brdspec.offset;

    PDEBUG("p_sysmon->brdspec.offset=0x%x, offset=0x%04x, value=0x%08x\n", 
           p_sysmon->brdspec.offset, offset, *(b_u32 *)p_value);

    _ret = devspibus_write_buff(p_sysmon->brdspec.devname, offset, idx, p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to write over spi bus\n");
        SYSMON_ERROR_RETURN(_ret);
    }    

    return _ret;
}

static ccl_err_t _sysmon_configure_test(void *p_priv)
{
    sysmon_ctx_t *p_sysmon;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        SYSMON_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_sysmon = (sysmon_ctx_t *)p_priv;

    return CCL_OK;

}

static ccl_err_t _sysmon_run_test(void *p_priv, 
                                  __attribute__((unused)) b_u8 *buf, 
                                  __attribute__((unused)) b_u32 size)
{
    sysmon_ctx_t     *p_sysmon;
    b_u32            regval;
    b_double64       temp;
    b_double64       vccint;
    b_double64       vccaux;
    b_double64       vbram;
    sysmon_stats_t   *stats;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        SYSMON_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_sysmon = (sysmon_ctx_t *)p_priv;

    if (size == sizeof(sysmon_stats_t)) {
        stats = (sysmon_stats_t *)buf;
        memset(&stats->temp, 0, sizeof(*stats)-DEVMAN_ATTRS_NAME_LEN);
    }
    _ret = _sysmon_reg_read(p_priv, SYSMON_TEMP_OFFSET, sizeof(b_u32), 
                          0, (b_u8 *)&regval, sizeof(b_u32));
    if ( _ret == CCL_OK ) { 
        temp = SYSMON_CALC_TEMPERATURE(regval);
        if (size == sizeof(sysmon_stats_t)) 
            stats->temp = temp;
    }
    _ret |= _sysmon_reg_read(p_priv, SYSMON_VCCINT_OFFSET, sizeof(b_u32), 
                             0, (b_u8 *)&regval, sizeof(b_u32));
    if ( _ret == CCL_OK ) { 
        vccint = SYSMON_CALC_VOLTAGE(regval);
        if (size == sizeof(sysmon_stats_t)) 
            stats->vccint = vccint;
    }
    _ret |= _sysmon_reg_read(p_priv, SYSMON_VCCAUX_OFFSET, sizeof(b_u32), 
                             0, (b_u8 *)&regval, sizeof(b_u32));
    if ( _ret == CCL_OK ) { 
        vccaux = SYSMON_CALC_VOLTAGE(regval);
        if (size == sizeof(sysmon_stats_t)) 
            stats->vccaux = vccaux;
    }
    _ret |= _sysmon_reg_read(p_priv, SYSMON_VBRAM_OFFSET, sizeof(b_u32), 
                             0, (b_u8 *)&regval, sizeof(b_u32));
    if ( _ret == CCL_OK ) { 
        vbram = SYSMON_CALC_VOLTAGE(regval);
        if (size == sizeof(sysmon_stats_t)) 
            stats->vbram = vbram;
    }

    if ( _ret ) 
        SYSMON_ERROR_RETURN(_ret);

    PDEBUG("temp=%lf vccint=%lf, vccaux=%lf, vbram=%lf\n",
           temp, vccint, vccaux, vbram);

    return CCL_OK;
}

ccl_err_t sysmon_attr_read(void *p_priv, b_u32 attrid, 
                         b_u32 offset, b_u32 offset_sz, 
                         b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || offset > SYSMON_MAX_OFFSET || 
         !p_value || idx ) {
        PDEBUG("Error! Invalid input parameters\n");
        SYSMON_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch ( attrid ) {
    case eSYSMON_GIER:
    case eSYSMON_IPISR:
    case eSYSMON_IPIER:
    case eSYSMON_TEMP:
    case eSYSMON_VCCINT:
    case eSYSMON_VCCAUX:
    case eSYSMON_VPVN:
    case eSYSMON_VREFP:
    case eSYSMON_VREFN:
    case eSYSMON_VBRAM:
    case eSYSMON_SUPPLY_CALIB:
    case eSYSMON_ADC_CALIB:
    case eSYSMON_GAINERR_CALIB:
    case eSYSMON_MAX_TEMP:
    case eSYSMON_MAX_VCCINT:
    case eSYSMON_MAX_VCCAUX:
    case eSYSMON_MAX_VCCBRAM:
    case eSYSMON_MIN_TEMP:
    case eSYSMON_MIN_VCCINT:
    case eSYSMON_MIN_VCCAUX:
    case eSYSMON_MIN_VCCBRAM:
    case eSYSMON_CFR0:
    case eSYSMON_CFR1:
    case eSYSMON_CFR2:
    case eSYSMON_ATR_TEMP_UPPER:
    case eSYSMON_ATR_VCCINT_UPPER:
    case eSYSMON_ATR_VCCAUX_UPPER:
    case eSYSMON_ATR_OT_UPPER:
    case eSYSMON_ATR_TEMP_LOWER:
    case eSYSMON_ATR_VCCINT_LOWER:
    case eSYSMON_ATR_VCCAUX_LOWER:
    case eSYSMON_ATR_OT_LOWER:
    case eSYSMON_ATR_VBRAM_UPPER:
    case eSYSMON_ATR_VBRAM_LOWER:
        SYSMON_REG_READ(p_priv, offset, offset_sz, idx, p_value, size);
        break;
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK) {
        PDEBUG("Failure in attribute id: %d\n", attrid);
    }

    return _ret;
}

ccl_err_t sysmon_attr_write(void *p_priv, b_u32 attrid, 
                          b_u32 offset, b_u32 offset_sz, 
                          b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || offset > SYSMON_MAX_OFFSET || 
         !p_value || idx ) {
        PDEBUG("Error! Invalid input parameters\n");
        SYSMON_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch ( attrid ) {
    case eSYSMON_GIER:
    case eSYSMON_IPISR:
    case eSYSMON_IPIER:
    case eSYSMON_CFR0:
    case eSYSMON_CFR1:
    case eSYSMON_CFR2:
    case eSYSMON_ATR_TEMP_UPPER:
    case eSYSMON_ATR_VCCINT_UPPER:
    case eSYSMON_ATR_VCCAUX_UPPER:
    case eSYSMON_ATR_OT_UPPER:
    case eSYSMON_ATR_TEMP_LOWER:
    case eSYSMON_ATR_VCCINT_LOWER:
    case eSYSMON_ATR_VCCAUX_LOWER:
    case eSYSMON_ATR_OT_LOWER:
    case eSYSMON_ATR_VBRAM_UPPER:
    case eSYSMON_ATR_VBRAM_LOWER:
        SYSMON_REG_WRITE(p_priv, offset, offset_sz, idx, p_value, size);
        break;
    case eSYSMON_INIT:
        _ret = _sysmon_init(p_priv);
        break;
    case eSYSMON_CONFIGURE_TEST:
        _ret = _sysmon_configure_test(p_priv);
        break;
    case eSYSMON_RUN_TEST:
        _ret = _sysmon_run_test(p_priv, p_value, size);
        break;
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK) {
        PDEBUG("Failure in attribute id: %d\n", attrid);
    }

    return _ret;
}

/* sysmon device attributes definition */
static device_attr_t _attrs[] = {
    {
        .name = "name", 
        .type = eDEVMAN_ATTR_STRING, .maxnum = 1, 
        .offset = offsetof(struct sysmon_ctx_t, brdspec.name),
        .size = DEVMAN_DEVNAME_LEN,
        .format = "%s"
    },
    {
        .name = "offset", 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset = offsetof(struct sysmon_ctx_t, brdspec.offset),
        .format = "0x%08x"
    },
    {
        .name = "gier", .id = eSYSMON_GIER, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = SYSMON_GIER_OFFSET
    },
    {
        .name = "ipisr", .id = eSYSMON_IPISR, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = SYSMON_IPISR_OFFSET
    },
    {
        .name = "ipier", .id = eSYSMON_IPIER, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = SYSMON_IPIER_OFFSET
    },
    {
        .name = "temp", .id = eSYSMON_TEMP, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = SYSMON_TEMP_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "vccint", .id = eSYSMON_VCCINT, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = SYSMON_VCCINT_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "vccaux", .id = eSYSMON_VCCAUX, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = SYSMON_VCCAUX_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "vpvn", .id = eSYSMON_VPVN, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = SYSMON_VPVN_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "vrefp", .id = eSYSMON_VREFP, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = SYSMON_VREFP_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "vrefn", .id = eSYSMON_VREFN, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = SYSMON_VREFN_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "vbram", .id = eSYSMON_VBRAM, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = SYSMON_VBRAM_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "sup_calib", .id = eSYSMON_SUPPLY_CALIB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = SYSMON_SUPPLY_CALIB_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "adc_calib", .id = eSYSMON_ADC_CALIB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = SYSMON_ADC_CALIB_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "gainerr_calib", .id = eSYSMON_GAINERR_CALIB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = SYSMON_GAINERR_CALIB_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "max_temp", .id = eSYSMON_MAX_TEMP, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = SYSMON_MAX_TEMP_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "max_vccint", .id = eSYSMON_MAX_VCCINT, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = SYSMON_MAX_VCCINT_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "max_vccaux", .id = eSYSMON_MAX_VCCAUX, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = SYSMON_MAX_VCCAUX_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "max_vccbram", .id = eSYSMON_MAX_VCCBRAM, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = SYSMON_MAX_VCCBRAM_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "min_temp", .id = eSYSMON_MIN_TEMP, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = SYSMON_MIN_TEMP_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "min_vccint", .id = eSYSMON_MIN_VCCINT, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = SYSMON_MIN_VCCINT_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "min_vccaux", .id = eSYSMON_MIN_VCCAUX, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = SYSMON_MIN_VCCAUX_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "min_vccbram", .id = eSYSMON_MIN_VCCBRAM, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = SYSMON_MIN_VCCBRAM_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "cfr0", .id = eSYSMON_CFR0, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = SYSMON_CFR0_OFFSET
    },
    {
        .name = "cfr1", .id = eSYSMON_CFR1, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = SYSMON_CFR1_OFFSET
    },
    {
        .name = "cfr2", .id = eSYSMON_CFR2, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = SYSMON_CFR2_OFFSET
    },
    {
        .name = "alm_temp_upper", .id = eSYSMON_ATR_TEMP_UPPER, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = SYSMON_ATR_TEMP_UPPER_OFFSET
    },
    {
        .name = "alm_vccint_upper", .id = eSYSMON_ATR_VCCINT_UPPER, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = SYSMON_ATR_VCCINT_UPPER_OFFSET
    },
    {
        .name = "alm_vccaux_upper", .id = eSYSMON_ATR_VCCAUX_UPPER, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = SYSMON_ATR_VCCAUX_UPPER_OFFSET
    },
    {
        .name = "alm_ot_upper", .id = eSYSMON_ATR_OT_UPPER, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = SYSMON_ATR_OT_UPPER_OFFSET
    },
    {
        .name = "alm_temp_lower", .id = eSYSMON_ATR_TEMP_LOWER, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = SYSMON_ATR_TEMP_LOWER_OFFSET
    },
    {
        .name = "alm_vccint_lower", .id = eSYSMON_ATR_VCCINT_LOWER, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = SYSMON_ATR_VCCINT_LOWER_OFFSET
    },
    {
        .name = "alm_vccaux_lower", .id = eSYSMON_ATR_VCCAUX_LOWER, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = SYSMON_ATR_VCCAUX_LOWER_OFFSET
    },
    {
        .name = "alm_ot_lower", .id = eSYSMON_ATR_OT_LOWER, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = SYSMON_ATR_OT_LOWER_OFFSET
    },
    {
        .name = "alm_vbram_upper", .id = eSYSMON_ATR_VBRAM_UPPER, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = SYSMON_ATR_VBRAM_UPPER_OFFSET
    },
    {
        .name = "alm_vbram_lower", .id = eSYSMON_ATR_VBRAM_LOWER, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = SYSMON_ATR_VBRAM_LOWER_OFFSET
    },
    {
        .name = "init", .id = eSYSMON_INIT, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "config_test", .id = eSYSMON_CONFIGURE_TEST, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "run_test", .id = eSYSMON_RUN_TEST, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, 
        .size = sizeof(board_dev_stats_t), 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
};

/**
 * This function initializes a specific SYSMON device/instance. 
 * It must be called prior to using the SYSMON device. 
 *
 * @param[in]	p_sysmon is a pointer to the sysmon_ctx_t instance.
 *
 * @return error code
 */
static ccl_err_t _sysmon_init(void *p_priv)
{
    sysmon_ctx_t *p_sysmon;
    b_u32        regval;

    /*
     * Assert the input arguments.
     */
    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        SYSMON_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_sysmon = (sysmon_ctx_t *)p_priv;

    return CCL_OK;
}

/**
 * This function gets the ADC converted data for the specified 
 * channel. 
 *
 * @param[in] p_sysmon is a pointer to the sysmon_ctx_t instance.
 * @param[in] channel is the channel number. Use the SYSMON_CH_*
 *		The valid channels are
 *		- 0 to 6
 *		- 13 to 31
 * @param[out] p_data is the memory that is updated with a
 *       16-bit value representing the ADC converted data for
 *       the specified channel. The SYSMON Monitor/ADC device
 *       guarantees a 10 bit resolution for the ADC converted
 *       data and data is the 10 MSB bits of the 16 data read
 *       from the device
 * @return error code
 * @note The channels 7,8,9 are used for calibration of the 
 *       device and hence there is no associated data with 
 *		 this channel.
 */
static ccl_err_t _sysmon_get_adc_data(sysmon_ctx_t *p_sysmon, 
                                    b_u8 channel, b_u32 *p_data)
{
    /*
     * Assert the input arguments.
     */
    if ( !p_sysmon || !p_data || 
         ((channel > SYSMON_CH_VBRAM) || 
          ((channel < SYSMON_CH_VCCPINT) ||
           (channel > SYSMON_CH_AUX_MAX)))) {
        PDEBUG("Error! Invalid input parameters\n");
        SYSMON_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    SYSMON_REG_READ(p_sysmon, SYSMON_TEMP_OFFSET + channel, 
                    sizeof(b_u32), 0, 
                    (b_u8 *)p_data, sizeof(b_u32));

    return CCL_OK;
}

/**
 * This function gets the calibration coefficient data for the specified
 * parameter.
 *
 * @param[in] p_sysmon is a pointer to the sysmon_ctx_t instance.
 * @param[in] coeff_type specifies the calibration coefficient
 *		to be read. Use SYSMON_CALIB_* constants to
 *		specify the calibration coefficient to be read.
 * @param[out] p_data is the memory that is updated with a 16-bit
 *       value representing the calibration coefficient. The SYSMON
 *       device guarantees a 10 bit resolution for the ADC
 *       converted data and data is the 10 MSB bits of the 16
 *       data read from the device.
 *  
 * @return error code
 */
static ccl_err_t _sysmon_get_calib_coefficient(sysmon_ctx_t *p_sysmon, 
                                             b_u8 coeff_type, b_u32 *p_data)
{
    /*
     * Assert the input arguments.
     */
    if ( !p_sysmon || ! p_data || 
         (coeff_type > SYSMON_CALIB_GAIN_ERROR_COEFF)) {
        PDEBUG("Error! Invalid input parameters\n");
        SYSMON_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Read the selected calibration coefficient.
     */
    SYSMON_REG_READ(p_sysmon, 
                           (SYSMON_SUPPLY_CALIB_OFFSET + coeff_type),
                             sizeof(b_u32), 0, 
                             (b_u8 *)p_data, sizeof(b_u32));

    return CCL_OK;
}

/**
 * This function reads the Minimum/Maximum measurement for one of the
 * specified parameters. Use SYSMON_MAX_* and SYSMON_MIN_* constants
 * to specify the parameters (Temperature, VccInt, VccAux, 
 * VBram, VccPInt, VccPAux and VccPDro). 
 *
 * @param[in] p_sysmon is a pointer to the sysmon_ctx_t instance.
 * @param[in] mtype specifies the parameter for which the
 *		Minimum/Maximum measurement has to be read.
 *		Use SYSMON_MAX_* and SYSMON_MIN_* constants defined in sysmonps.h to
 *		specify the data to be read.
 * @param[out] p_data is the memory that is updated with a 16-bit
 *       value representing the maximum/minimum measurement for
 *       specified parameter. The SYSMON device guarantees a 10 bit
 *       resolution for the ADC converted data and data is the 10
 *       MSB bits of the 16 data read from the device.
 * @return error code
 */
static ccl_err_t _sysmon_get_min_max_measurement(sysmon_ctx_t *p_sysmon, 
                                               b_u8 mtype, b_u32 *p_data)
{
    /*
     * Assert the input arguments.
     */
    if ( !p_sysmon || !p_data || 
         ((mtype > SYSMON_MAX_VCCPDRO) && 
          ((mtype < SYSMON_MIN_VCCPINT) || 
           (mtype > SYSMON_MIN_VCCPDRO)))) {
        PDEBUG("Error! Invalid input parameters\n");
        SYSMON_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Read and return the specified Minimum/Maximum measurement.
     */
    SYSMON_REG_READ(p_sysmon,
                           (SYSMON_MAX_TEMP_OFFSET + mtype), 
                    sizeof(b_u32), 0, 
                    (b_u8 *)p_data, sizeof(b_u32));

    return CCL_OK;
}

/** 
 *
 * This function sets the number of samples of averaging that is to be done for
 * all the channels in both the single channel mode and sequence mode of
 * operations.
 *
 * @param[in] p_sysmon is a pointer to the sysmon_ctx_t instance.
 * @param[in] average is the number of samples of averaging
 *       programmed to the Configuration Register 0. Use the
 *       SYSMON_AVG_* definitions:
 *		- SYSMON_AVG_0_SAMPLES for no averaging
 *		- SYSMON_AVG_16_SAMPLES for 16 samples of averaging
 *		- SYSMON_AVG_64_SAMPLES for 64 samples of averaging
 *		- SYSMON_AVG_256_SAMPLES for 256 samples of averaging
 *
 * @return	error code
 */
static ccl_err_t _sysmon_set_avg(sysmon_ctx_t *p_sysmon, b_u8 average)
{
    b_u32 regval;

    /*
     * Assert the input arguments.
     */
    if ( !p_sysmon || (average > SYSMON_AVG_256_SAMPLES) ) {
        PDEBUG("Error! Invalid input parameters\n");
        SYSMON_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Write the averaging value into the Configuration Register 0.
     */
    SYSMON_REG_READ(p_sysmon, SYSMON_CFR0_OFFSET, 
                    sizeof(b_u32), 0, 
                    (b_u8 *)&regval, sizeof(b_u32));

    regval &= ~SYSMON_CFR0_AVG_VALID_MASK;
    regval |=  (((b_u32) average << SYSMON_CFR0_AVG_SHIFT));
    SYSMON_REG_WRITE(p_sysmon, SYSMON_CFR0_OFFSET, 
                    sizeof(b_u32), 0, 
                    (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/**
 * This function gets the number of samples of averaging 
 * configured for all the channels in the Configuration Register 
 * 0. 
 *
 * @param[in]	p_sysmon is a pointer to the sysmon_ctx_t instance.
 * @param[out] p_data is the memory that is updated with the 
 *       averaging read from the Configuration Register 0 is
 *       returned. Use the SYSMON_AVG_* bit definitions to
 *       interpret the returned value:
 *		- SYSMON_AVG_0_SAMPLES means no averaging
 *		- SYSMON_AVG_16_SAMPLES means 16 samples of averaging
 *		- SYSMON_AVG_64_SAMPLES means 64 samples of averaging
 *		- SYSMON_AVG_256_SAMPLES means 256 samples of averaging
 *  
 * @return error code
 */
static ccl_err_t _sysmon_get_avg(sysmon_ctx_t *p_sysmon, b_u8 *p_data)
{
    b_u32 regval;

    /*
     * Assert the input arguments.
     */
    if ( !p_sysmon || !p_data ) {
        PDEBUG("Error! Invalid input parameters\n");
        SYSMON_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Read the averaging value from the Configuration Register 0.
     */
    SYSMON_REG_READ(p_sysmon, SYSMON_CFR0_OFFSET, 
                    sizeof(b_u32), 0, 
                    (b_u8 *)&regval, sizeof(b_u32));
    regval &= SYSMON_CFR0_AVG_VALID_MASK;

    *p_data = (b_u8) (regval >> SYSMON_CFR0_AVG_SHIFT);

    return CCL_OK;
}

/**
 * This function sets the specified channel Sequencer mode in the Configuration
 * Register 1 :
 *		- Default safe mode (SYSMON_SEQ_MODE_SAFE)
 *		- One pass through sequence (SYSMON_SEQ_MODE_ONEPASS)
 *		- Continuous channel sequencing (SYSMON_SEQ_MODE_CONTINPASS)
 *		- Single channel/Sequencer off (SYSMON_SEQ_MODE_SINGCHAN)
 *		- Simulataneous sampling mode (SYSMON_SEQ_MODE_SIMUL_SAMPLING)
 *		- Independent mode (SYSMON_SEQ_MODE_INDEPENDENT)
 *
 * @param[in] p_sysmon is a pointer to the sysmon_ctx_t instance.
 * @param[in] mode is the sequencer mode to be set.
 *		Use SYSMON_SEQ_MODE_* bits
 *  
 * @return	error code
 *
 * @note Only one of the modes can be enabled at a time. 
 *       Please read the Spec of the SYSMON for further 
 *       information about the sequencer modes.
 */
static ccl_err_t _sysmon_set_sequencer_mode(sysmon_ctx_t *p_sysmon, b_u8 mode)
{
    b_u32 regval;

    /*
     * Assert the arguments.
     */
    /*
     * Assert the input arguments.
     */
    if ( !p_sysmon || 
         ((mode > SYSMON_SEQ_MODE_SIMUL_SAMPLING) && 
          (mode != SYSMON_SEQ_MODE_INDEPENDENT)) ) {
        PDEBUG("Error! Invalid input parameters\n");
        SYSMON_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /*
     * Set the specified sequencer mode in the Configuration Register 1.
     */
    SYSMON_REG_READ(p_sysmon, SYSMON_CFR1_OFFSET, 
                    sizeof(b_u32), 0, 
                    (b_u8 *)&regval, sizeof(b_u32));
    regval &= (~ SYSMON_CFR1_SEQ_VALID_MASK);
    regval |= ((mode  << SYSMON_CFR1_SEQ_SHIFT) &
               SYSMON_CFR1_SEQ_VALID_MASK);
    SYSMON_REG_WRITE(p_sysmon, SYSMON_CFR1_OFFSET, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/** 
 * This function gets the channel sequencer mode from the Configuration
 * Register 1.
 *
 * @param[in] p_sysmon is a pointer to the sysmon_ctx_t instance.
 * @param[out] p_data is the memory that is updated with the
 *        channel sequencer mode :
 *		- SYSMON_SEQ_MODE_SAFE : Default safe mode
 *		- SYSMON_SEQ_MODE_ONEPASS : One pass through sequence
 *		- SYSMON_SEQ_MODE_CONTINPASS : Continuous channel sequencing
 *		- SYSMON_SEQ_MODE_SINGCHAN : Single channel/Sequencer off
 *		- SYSMON_SEQ_MODE_SIMUL_SAMPLING : Simulataneous sampling mode
 *		- SYSMON_SEQ_MODE_INDEPENDENT : Independent mode
 *
 * @return error code
 */
static ccl_err_t _sysmon_get_sequencer_mode(sysmon_ctx_t *p_sysmon, b_u8 *p_data)
{
    b_u32 regval;

    /*
     * Assert the input arguments.
     */
    if ( !p_sysmon || !p_data) {
        PDEBUG("Error! Invalid input parameters\n");
        SYSMON_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Read the channel sequencer mode from the Configuration Register 1.
     */
    SYSMON_REG_READ(p_sysmon, SYSMON_CFR1_OFFSET, 
                    sizeof(b_u32), 0, 
                    (b_u8 *)&regval, sizeof(b_u32));
    regval &= SYSMON_CFR1_SEQ_VALID_MASK;
    *p_data = (b_u8)(regval >> SYSMON_CFR1_SEQ_SHIFT);

    return CCL_OK;
}

/**
 *
 * The function sets the given parameters in the Configuration Register 0 in
 * the single channel mode.
 *
 * @param[in]	p_sysmon is a pointer to the sysmon_ctx_t instance.
 * @param[in]	channel is the channel number for the singel channel mode.
 *		The valid channels are 0 to 6, 8, and 13 to 31.
 *		If the external Mux is used then this specifies the channel
 *		oonnected to the external Mux. Please read the Device Spec
 *		to know which channels are valid.
 * @param[in] 	incr_acq_cycles is a boolean parameter which specifies whether
 *		the Acquisition time for the external channels has to be
 *		increased to 10 ADCCLK cycles (specify CCL_TRUE) or remain at the
 *		default 4 ADCCLK cycles (specify CCL_FALSE). This parameter is
 *		only valid for the external channels.
 * @param[in] 	is_event_mode specifies whether the operation of the ADC is Event
 * 		driven or Continuous mode.
 * @param[in] 	is_diff_mode is a boolean parameter which specifies
 *		unipolar(specify CCL_FALSE) or differential mode (specify CCL_TRUE) for
 *		the analog inputs. The 	input mode is only valid for the
 *		external channels.
 *
 * @return error code
 *
 * @note The number of samples for the averaging for all the channels
 *		 is set by using the function _sysmon_SetAvg.
 *		 The calibration of the device is done by doing a ADC
 *		 conversion on the calibration channel(channel 8). The input
 *		 parameters incr_acq_cycles, is_diff_mode and
 *		 is_event_mode are not valid for this channel
 *
 */
static ccl_err_t _sysmon_set_single_ch_params(sysmon_ctx_t *p_sysmon,
                                            b_u8 channel,
                                            b_bool incr_acq_cycles,
                                            b_bool is_event_mode,
                                            b_bool is_diff_mode)
{
    b_u32 regval;
    b_u8 mode;

    /*
     * Assert the input arguments.
     */
    if ( !p_sysmon || 
         ((channel > SYSMON_CH_VBRAM) &&  
          (channel != SYSMON_CH_ADC_CALIB) &&
          ((channel < SYSMON_CH_VCCPINT) || 
           (channel > SYSMON_CH_AUX_MAX)))) {
        PDEBUG("Error! Invalid input parameters\n");
        SYSMON_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Check if the device is in single channel mode else return failure
     */
    _ret = _sysmon_get_sequencer_mode(p_sysmon, &mode);
    if (_ret) {
        PDEBUG("_sysmon_get_sequencer_mode error\n");
        SYSMON_ERROR_RETURN(_ret);
    }
    if (mode != SYSMON_SEQ_MODE_SINGCHAN) {
        PDEBUG("invalid sequencer mode: %d\n", mode);
        SYSMON_ERROR_RETURN(CCL_FAIL);
    }

    /*
     * Read the Configuration Register 0.
     */
    SYSMON_REG_READ(p_sysmon, SYSMON_CFR0_OFFSET, 
                    sizeof(b_u32), 0, 
                    (b_u8 *)&regval, sizeof(b_u32));
    regval &= SYSMON_CFR0_AVG_VALID_MASK;

    /*
     * Select the number of acquisition cycles. The acquisition cycles is
     * only valid for the external channels.
     */
    if (incr_acq_cycles == CCL_TRUE) {
        if (((channel >= SYSMON_CH_AUX_MIN) &&
             (channel <= SYSMON_CH_AUX_MAX)) ||
            (channel == SYSMON_CH_VPVN)) {
            regval |= SYSMON_CFR0_ACQ_MASK;
        } else {
            SYSMON_ERROR_RETURN(CCL_FAIL);
        }
    }

    /*
     * Select the input mode. The input mode is only valid for the
     * external channels.
     */
    if (is_diff_mode == CCL_TRUE) {
        if (((channel >= SYSMON_CH_AUX_MIN) &&
             (channel <= SYSMON_CH_AUX_MAX)) ||
            (channel == SYSMON_CH_VPVN)) {
            regval |= SYSMON_CFR0_DU_MASK;
        } else {
            SYSMON_ERROR_RETURN(CCL_FAIL);
        }
    }

    /*
     * Select the ADC mode.
     */
    if (is_event_mode == CCL_TRUE)
        regval |= SYSMON_CFR0_EC_MASK;

    /*
     * Write the given values into the Configuration Register 0.
     */
    regval |= (channel & SYSMON_CFR0_CHANNEL_MASK);
    SYSMON_REG_WRITE(p_sysmon, SYSMON_CFR0_OFFSET, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/**
 * This function enables the alarm outputs for the specified alarms in the
 * Configuration Register 1.
 *
 * @param[in] p_sysmon is a pointer to the sysmon_ctx_t instance.
 * @param[in] mask is the bit-mask of the alarm outputs to be 
 *       enabled in the Configuration Register 1. Bit positions
 *       of 1 will be enabled. Bit positions of 0 will be
 *       disabled. This mask is formed by OR'ing
 *       SYSMON_CFR1_ALM_*_MASK and SYSMON_CFR1_OT_MASK masks
 *
 * @return	error code
 *
 * @note The implementation of the alarm enables in the 
 *       Configuration register 1 is such that the alarms for bit
 *       positions of 1 will be disabled and alarms for bit
 *       positions of 0 will be enabled. The alarm outputs
 *       specified by the mask are negated before writing to the
 *       Configuration Register 1.
 */
static ccl_err_t _sysmon_set_alarm_enables(sysmon_ctx_t *p_sysmon, b_u16 mask)
{
    b_u32 regval;

    /*
     * Assert the input arguments.
     */
    if ( !p_sysmon ) {
        PDEBUG("Error! Invalid input parameters\n");
        SYSMON_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    SYSMON_REG_READ(p_sysmon, SYSMON_CFR1_OFFSET, 
                    sizeof(b_u32), 0, 
                    (b_u8 *)&regval, sizeof(b_u32));

    regval &= (b_u32)~SYSMON_CFR1_ALM_ALL_MASK;
    regval |= (~mask & SYSMON_CFR1_ALM_ALL_MASK);

    /*
     * Enable/disables the alarm enables for the specified alarm bits in the
     * Configuration Register 1.
     */
    SYSMON_REG_WRITE(p_sysmon, SYSMON_CFR1_OFFSET, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/**
 * This function gets the status of the alarm output enables in the
 * Configuration Register 1.
 *
 * @param[in] p_sysmon is a pointer to the sysmon_ctx_t instance.
 * @param[out] p_data is the memory that is updated with
 *      the bit-mask of the enabled alarm outputs in the
 *      Configuration Register 1. Use the masks SYSMON_CFR1_ALM*_*
 *      and SYSMON_CFR1_OT_MASK to interpret the returned value.
 *      Bit positions of 1 indicate that the alarm output is
 *      enabled. Bit positions of 0 indicate that the alarm
 *      output is disabled.
 * 
 * @return	error code
 *
 *  @note The implementation of the alarm enables in the
 *       Configuration register 1 is such that alarms for the bit
 *       positions of 1 will be disabled and alarms for bit
 *       positions of 0 will be enabled. The enabled alarm
 *       outputs returned by this function is the negated value
 *       of the the data read from the Configuration Register 1.
 */
static ccl_err_t _sysmon_get_alarm_enables(sysmon_ctx_t *p_sysmon, b_u16 *p_data)
{
    b_u32 regval;

    /*
     * Assert the input arguments.
     */
    if ( !p_sysmon || !p_data) {
        PDEBUG("Error! Invalid input parameters\n");
        SYSMON_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Read the status of alarm output enables from the Configuration
     * Register 1.
     */
    SYSMON_REG_READ(p_sysmon, SYSMON_CFR1_OFFSET, 
                    sizeof(b_u32), 0, 
                    (b_u8 *)&regval, sizeof(b_u32));
    regval &= SYSMON_CFR1_ALM_ALL_MASK;
    *p_data = (b_u16) (~regval & SYSMON_CFR1_ALM_ALL_MASK);

    return CCL_OK;
}

/**
 * This function enables the specified calibration in the Configuration
 * Register 1 :
 *
 * - SYSMON_CFR1_CAL_ADC_OFFSET_MASK : calibration 0 -ADC offset correction
 * - SYSMON_CFR1_CAL_ADC_GAIN_OFFSET_MASK : calibration 1 -ADC gain and offset
 *						correction
 * - SYSMON_CFR1_CAL_PS_OFFSET_MASK : calibration 2 -Power Supply sensor
 *					offset correction
 * - SYSMON_CFR1_CAL_PS_GAIN_OFFSET_MASK : calibration 3 -Power Supply sensor
 *						gain and offset correction
 * - SYSMON_CFR1_CAL_DISABLE_MASK : No calibration
 *
 * @param[in] p_sysmon is a pointer to the sysmon_ctx_t instance.
 * @param[in] calibration is the calibration to be applied.
 *		Use SYSMON_CFR1_CAL*_* bits.
 *		Multiple calibrations can be enabled at a time by oring the
 *		SYSMON_CFR1_CAL_ADC_* and SYSMON_CFR1_CAL_PS_* bits.
 *		calibration can be disabled by specifying
 *		SYSMON_CFR1_CAL_DISABLE_MASK;
 *
 * @return	error code
 */
static ccl_err_t _sysmon_set_calib_enables(sysmon_ctx_t *p_sysmon, b_u16 calibration)
{
    b_u32 regval;

    /*
     * Assert the input arguments.
     */
    if ( !p_sysmon || 
         (((calibration < SYSMON_CFR1_CAL_ADC_OFFSET_MASK) ||
           (calibration > SYSMON_CFR1_CAL_VALID_MASK)) &&
          (calibration != SYSMON_CFR1_CAL_DISABLE_MASK)) ) {
        PDEBUG("Error! Invalid input parameters\n");
        SYSMON_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Set the specified calibration in the Configuration Register 1.
     */
    SYSMON_REG_READ(p_sysmon, SYSMON_CFR1_OFFSET, 
                    sizeof(b_u32), 0, 
                    (b_u8 *)&regval, sizeof(b_u32));
    regval &= (~ SYSMON_CFR1_CAL_VALID_MASK);
    regval |= (calibration & SYSMON_CFR1_CAL_VALID_MASK);
    SYSMON_REG_WRITE(p_sysmon, SYSMON_CFR1_OFFSET, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/**
 * This function reads the value of the calibration enables from the
 * Configuration Register 1.
 *
 * @param[in] p_sysmon is a pointer to the sysmon_ctx_t instance.
 * @param[out] p_data is the memory that is updated with value of
 *       the calibration enables in the Configuration Register 1:
 *		- SYSMON_CFR1_CAL_ADC_OFFSET_MASK : ADC offset correction
 *		- SYSMON_CFR1_CAL_ADC_GAIN_OFFSET_MASK : ADC gain and offset
 *				correction
 *		- SYSMON_CFR1_CAL_PS_OFFSET_MASK : Power Supply sensor offset
 *				correction
 *		- SYSMON_CFR1_CAL_PS_GAIN_OFFSET_MASK : Power Supply sensor
 *				gain and offset correction
 *		- SYSMON_CFR1_CAL_DISABLE_MASK : No calibration
 * 
 * @return error code
 */
static ccl_err_t _sysmon_get_calib_enables(sysmon_ctx_t *p_sysmon, b_u16 *p_data)
{
    b_u32 regval;

    /*
     * Assert the input arguments.
     */
    if ( !p_sysmon || !p_data) {
        PDEBUG("Error! Invalid input parameters\n");
        SYSMON_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Read the calibration enables from the Configuration Register 1.
     */
    SYSMON_REG_READ(p_sysmon, SYSMON_CFR1_OFFSET, 
                    sizeof(b_u32), 0, 
                    (b_u8 *)&regval, sizeof(b_u32));
    regval &= SYSMON_CFR1_CAL_VALID_MASK;
    *p_data = (b_u16)regval;

    return CCL_OK;
}

/**
 * The function sets the frequency of the ADCCLK by configuring the DCLK to
 * ADCCLK ratio in the Configuration Register #2
 *
 * @param[in] p_sysmon is a pointer to the sysmon_ctx_t instance.
 * @param[in] divisor is clock divisor used to derive ADCCLK from
 *       DCLK. Valid values of the divisor are
 *		 - 0 to 255. values 0, 1, 2 are all mapped to 2.
 *		Refer to the device specification for more details
 *
 * @return	error code
 *
 * @note  The ADCCLK is an int clock used by the ADC and is
 *		  synchronized to the DCLK clock. The ADCCLK is equal to DCLK
 *        divided by the user selection in the Configuration
 *        Register 2. There is no Assert on the minimum value of
 *        the divisor.
 */
static ccl_err_t _sysmon_set_adc_clk_divisor(sysmon_ctx_t *p_sysmon, b_u8 divisor)
{
    b_u32 regval;

    /*
     * Assert the input arguments.
     */
    if ( !p_sysmon ) {
        PDEBUG("Error! Invalid input parameters\n");
        SYSMON_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Write the divisor value into the Configuration Register #2.
     */
    regval = divisor << SYSMON_CFR2_CD_SHIFT;
    SYSMON_REG_WRITE(p_sysmon, SYSMON_CFR2_OFFSET, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/**
 * The function gets the ADCCLK divisor from the Configuration Register 2.
 *
 * @param[in]	p_sysmon is a pointer to the sysmon_ctx_t instance.
 * @param[out] p_data is the memory that is updated with the 
 *       divisor read from the Configuration Register 2.
 *  
 * @return error code
 *
 * @note The ADCCLK is an int clock used by the ADC and is
 *		synchronized to the DCLK clock. The ADCCLK is equal to DCLK
 *		divided by the user selection in the Configuration Register 2.
 */
static ccl_err_t _sysmon_get_adc_clk_divisor(sysmon_ctx_t *p_sysmon, b_u8 *p_data)
{
    b_u32 regval;

    /*
     * Assert the input arguments.
     */
    if ( !p_sysmon || !p_data ) {
        PDEBUG("Error! Invalid input parameters\n");
        SYSMON_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Read the divisor value from the Configuration Register 2.
     */
    SYSMON_REG_READ(p_sysmon, SYSMON_CFR2_OFFSET, 
                    sizeof(b_u32), 0, 
                    (b_u8 *)&regval, sizeof(b_u32));

    *p_data =  (b_u8)(regval >> SYSMON_CFR2_CD_SHIFT);

    return CCL_OK;
}

/**
 * This function enables the specified channels in the ADC channel Selection
 * Sequencer Registers. The sequencer must be disabled before writing to these
 * registers.
 *
 * @param[in]	p_sysmon is a pointer to the sysmon_ctx_t instance.
 * @param[in]	mask is the bit mask of all the channels to be enabled.
 *       Use SYSMON_SEQ_CH__* to specify the channel numbers. Bit
 *       masks of 1 will be enabled and bit mask of 0 will be
 *       disabled. The mask is a 32 bit mask that is written to
 *       the two 16 bit ADC channel Selection Sequencer
 *       Registers.
 *
 * @return error code
 */
static ccl_err_t _sysmon_set_seq_ch_enables(sysmon_ctx_t *p_sysmon, b_u32 mask)
{
    b_u32 regval;
    b_u8 mode;

    /*
     * Assert the input arguments.
     */
    if ( !p_sysmon ) {
        PDEBUG("Error! Invalid input parameters\n");
        SYSMON_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * The sequencer must be disabled for writing any of these registers
     * Return XST_FAILURE if the channel sequencer is enabled.
     */
    _ret = _sysmon_get_sequencer_mode(p_sysmon, &mode);
    if (_ret) {
        PDEBUG("_sysmon_get_sequencer_mode error\n");
        SYSMON_ERROR_RETURN(_ret);
    }
    if (mode != SYSMON_SEQ_MODE_SAFE) {
        PDEBUG("Invalid sequencer mode: %d\n", mode);
        return CCL_FAIL;
    }

    /*
     * Enable the specified channels in the ADC channel Selection Sequencer
     * Registers.
     */
    regval = mask & SYSMON_SEQ00_CH_VALID_MASK;
    SYSMON_REG_WRITE(p_sysmon, SYSMON_SEQ00_OFFSET, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));
    regval = ((mask >> SYSMON_SEQ_CH_AUX_SHIFT) & SYSMON_SEQ01_CH_VALID_MASK);
    SYSMON_REG_WRITE(p_sysmon, SYSMON_SEQ01_OFFSET, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/**
 * This function gets the channel enable bits status from the ADC channel
 * Selection Sequencer Registers.
 *
 * @param[in] p_sysmon is a pointer to the sysmon_ctx_t instance. 
 * @param[out] p_data is the memory that is updated with the 
 *       channel enable bits. Use SYSMON_SEQ_CH__* defined in
 *       sysmonps_hw.h to interpret the channel numbers. Bit masks
 *       of 1 are the channels that are enabled and bit mask of 0
 *       are the channels that are disabled.
 *
 * @return error code
 */
static ccl_err_t _sysmon_get_seq_ch_enables(sysmon_ctx_t *p_sysmon, b_u32 *p_data)
{
    b_u32 regval, regval1;

    /*
     * Assert the input arguments.
     */
    if ( !p_sysmon || !p_data) {
        PDEBUG("Error! Invalid input parameters\n");
        SYSMON_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     *  Read the channel enable bits for all the channels from the ADC
     *  channel Selection Register.
     */
    SYSMON_REG_READ(p_sysmon,SYSMON_SEQ00_OFFSET, 
                    sizeof(b_u32), 0, 
                    (b_u8 *)&regval, sizeof(b_u32));
    regval &= SYSMON_SEQ00_CH_VALID_MASK;
    SYSMON_REG_READ(p_sysmon, SYSMON_SEQ01_OFFSET, 
                    sizeof(b_u32), 0, 
                    (b_u8 *)&regval1, sizeof(b_u32));
    regval1 &= SYSMON_SEQ01_CH_VALID_MASK;
    regval |= regval1 << SYSMON_SEQ_CH_AUX_SHIFT;
    *p_data = regval;

    return CCL_OK;
}

/**
 * This function enables the averaging for the specified channels in the ADC
 * channel Averaging Enable Sequencer Registers. The sequencer must be disabled
 * before writing to these regsiters.
 *
 * @param[in] p_sysmon is a pointer to the sysmon_ctx_t instance.
 * @param[in] mask is the bit mask of all the channels for which
 *       averaging is to be enabled. Use SYSMON_SEQ_CH__* to specify the channel numbers.
 *       Averaging will be enabled for bit masks of 1 and disabled for bit mask of 0.
 *		The mask is a 32 bit mask that is written to the two
 *		16 bit ADC channel Averaging Enable Sequencer Registers.
 *
 * @return error code
 */
static ccl_err_t _sysmon_set_seq_avg_enables(sysmon_ctx_t *p_sysmon, b_u32 mask)
{
    b_u32 regval;
    b_u8 mode;

    /*
     * Assert the input arguments.
     */
    if ( !p_sysmon ) {
        PDEBUG("Error! Invalid input parameters\n");
        SYSMON_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * The sequencer must be disabled for writing any of these registers
     * Return XST_FAILURE if the channel sequencer is enabled.
     */
    _ret = _sysmon_get_sequencer_mode(p_sysmon, &mode);
    if (_ret) {
        PDEBUG("_sysmon_get_sequencer_mode error\n");
        SYSMON_ERROR_RETURN(_ret);
    }
    if (mode != SYSMON_SEQ_MODE_SAFE) {
        PDEBUG("Invalid sequencer mode: %d\n", mode);
        return CCL_FAIL;
    }

    /*
     * Enable/disable the averaging for the specified channels in the
     * ADC channel Averaging Enables Sequencer Registers.
     */
    regval = mask & SYSMON_SEQ02_CH_VALID_MASK;
    SYSMON_REG_WRITE(p_sysmon, SYSMON_SEQ02_OFFSET, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));
    regval = ((mask >> SYSMON_SEQ_CH_AUX_SHIFT) & SYSMON_SEQ03_CH_VALID_MASK);
    SYSMON_REG_WRITE(p_sysmon, SYSMON_SEQ03_OFFSET, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/**
 * This function return the channels for which the averaging has been enabled
 * in the ADC channel Averaging Enables Sequencer Registers.
 *
 * @param[in] p_sysmon is a pointer to the sysmon_ctx_t instance.
 * @param[out] p_data is the memory that is updated with the 
 *       status of averaging (enabled/disabled) for all the
 *       channels. Use SYSMON_SEQ_CH__* to interpret the channel
 *       numbers. Bit masks of 1 are the channels for which
 *       averaging is enabled and bit mask of 0 are the channels
 *       for averaging is disabled
 *  
 * @return error code
 */
static ccl_err_t _sysmon_get_seq_avg_enables(sysmon_ctx_t *p_sysmon, b_u32 *p_data)
{
    b_u32 regval, regval1;

    /*
     * Assert the input arguments.
     */
    if ( !p_sysmon || !p_data) {
        PDEBUG("Error! Invalid input parameters\n");
        SYSMON_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Read the averaging enable status for all the channels from the
     * ADC channel Averaging Enables Sequencer Registers.
     */
    SYSMON_REG_READ(p_sysmon,  SYSMON_SEQ02_OFFSET, 
                    sizeof(b_u32), 0, 
                    (b_u8 *)&regval, sizeof(b_u32));
    regval &= SYSMON_SEQ02_CH_VALID_MASK;
    SYSMON_REG_READ(p_sysmon, SYSMON_SEQ03_OFFSET, 
                    sizeof(b_u32), 0, 
                    (b_u8 *)&regval1, sizeof(b_u32));
    regval1 &= SYSMON_SEQ03_CH_VALID_MASK;
    regval |= (regval1 << SYSMON_SEQ_CH_AUX_SHIFT);
    *p_data = regval;

    return CCL_OK;
}

/**
 *
 * This function sets the Analog input mode for the specified channels in the ADC
 * channel Analog-Input mode Sequencer Registers. The sequencer must be disabled
 * before writing to these regsiters.
 *
 * @param[in] p_sysmon is a pointer to the sysmon_ctx_t instance.
 * @param[in] mask is the bit mask of all the channels for which
 *		the input mode is differential mode. Use SYSMON_SEQ_CH__* defined
 *		in sysmonps_hw.h to specify the channel numbers. Differential
 *		input mode will be set for bit masks of 1 and unipolar input
 *		mode for bit masks of 0.
 *		The mask is a 32 bit mask that is written to the two
 *		16 bit ADC channel Analog-Input mode Sequencer Registers.
 *
 * @return error code
 */
static ccl_err_t _sysmon_set_seq_input_mode(sysmon_ctx_t *p_sysmon, b_u32 mask)
{
    b_u32 regval; 
    b_u8 mode;

    /*
     * Assert the input arguments.
     */
    if ( !p_sysmon ) {
        PDEBUG("Error! Invalid input parameters\n");
        SYSMON_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * The sequencer must be disabled for writing any of these registers
     * Return XST_FAILURE if the channel sequencer is enabled.
     */
    _ret = _sysmon_get_sequencer_mode(p_sysmon, &mode);
    if (_ret) {
        PDEBUG("_sysmon_get_sequencer_mode error\n");
        SYSMON_ERROR_RETURN(_ret);
    }
    if (mode != SYSMON_SEQ_MODE_SAFE) {
        PDEBUG("Invalid sequencer mode: %d\n", mode);
        return CCL_FAIL;
    }

    /*
     * Set the input mode for the specified channels in the ADC channel
     * Analog-Input mode Sequencer Registers.
     */
    regval = mask & SYSMON_SEQ04_CH_VALID_MASK;
    SYSMON_REG_WRITE(p_sysmon, SYSMON_SEQ04_OFFSET, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));
    regval = (mask >> SYSMON_SEQ_CH_AUX_SHIFT) & SYSMON_SEQ05_CH_VALID_MASK;
    SYSMON_REG_WRITE(p_sysmon, SYSMON_SEQ05_OFFSET, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/**
 * This function gets the Analog input mode for all the channels from
 * the ADC channel Analog-Input mode Sequencer Registers.
 *
 * @param[in] p_sysmon is a pointer to the sysmon_ctx_t instance. 
 * @param[out] p_data is the memory that is updated with the 
 *       input mode for all the channels. Use SYSMON_SEQ_CH_* to
 *       interpret the channel numbers. Bit masks of 1 are the
 *       channels for which input mode is differential and bit
 *       mask of 0 are the channels for which input mode is
 *       unipolar.
 *
 * @return error code
 */
static ccl_err_t _sysmon_get_seq_input_mode(sysmon_ctx_t *p_sysmon, b_u32 *p_data)
{
    b_u32 regval, regval1;

    /*
     * Assert the input arguments.
     */
    if ( !p_sysmon || !p_data) {
        PDEBUG("Error! Invalid input parameters\n");
        SYSMON_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     *  Get the input mode for all the channels from the ADC channel
     * Analog-Input mode Sequencer Registers.
     */
    SYSMON_REG_READ(p_sysmon, SYSMON_SEQ04_OFFSET, 
                    sizeof(b_u32), 0, 
                    (b_u8 *)&regval, sizeof(b_u32));
    regval &= SYSMON_SEQ04_CH_VALID_MASK;
    SYSMON_REG_READ(p_sysmon, SYSMON_SEQ05_OFFSET, 
                    sizeof(b_u32), 0, 
                    (b_u8 *)&regval1, sizeof(b_u32));
    regval1 &= SYSMON_SEQ05_CH_VALID_MASK;
    regval |= regval1 << SYSMON_SEQ_CH_AUX_SHIFT;
    *p_data = regval;

    return CCL_OK;
}

/**
 * This function sets the number of Acquisition cycles in the ADC channel
 * Acquisition Time Sequencer Registers. The sequencer must be disabled
 * before writing to these regsiters.
 *
 * @param[in] p_sysmon is a pointer to the sysmon_ctx_t instance.
 * @param[in] mask is the bit mask of all the
 *       channels for which the number of acquisition cycles is
 *       to be extended. Use SYSMON_SEQ_CH__* to specify the
 *       channel numbers. Acquisition cycles will be extended to
 *       10 ADCCLK cycles for bit masks of 1 and will be the
 *       default 4 ADCCLK cycles for bit masks of 0.
 *		The mask is a 32 bit mask that is written to the two
 *		16 bit ADC channel Acquisition Time Sequencer Registers.
 *
 * @return error code
 */
static ccl_err_t _sysmon_set_seq_acq_time(sysmon_ctx_t *p_sysmon, b_u32 mask)
{
    b_u32 regval; 
    b_u8 mode;

    /*
     * Assert the input arguments.
     */
    if ( !p_sysmon ) {
        PDEBUG("Error! Invalid input parameters\n");
        SYSMON_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * The sequencer must be disabled for writing any of these registers
     * Return XST_FAILURE if the channel sequencer is enabled.
     */
    _sysmon_get_sequencer_mode(p_sysmon, &mode);
    if (_ret) {
        PDEBUG("_sysmon_get_sequencer_mode error\n");
        SYSMON_ERROR_RETURN(_ret);
    }
    if (mode != SYSMON_SEQ_MODE_SAFE) {
        PDEBUG("Invalid sequencer mode: %d\n", mode);
        return CCL_FAIL;
    }

    /*
     * Set the Acquisition time for the specified channels in the
     * ADC channel Acquisition Time Sequencer Registers.
     */
    regval = mask & SYSMON_SEQ06_CH_VALID_MASK;
    SYSMON_REG_WRITE(p_sysmon, SYSMON_SEQ06_OFFSET,  
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));
    regval = (mask >> SYSMON_SEQ_CH_AUX_SHIFT) & SYSMON_SEQ07_CH_VALID_MASK;
    SYSMON_REG_WRITE(p_sysmon, SYSMON_SEQ07_OFFSET, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/**
 * This function gets the status of acquisition from the ADC channel Acquisition
 * Time Sequencer Registers.
 *
 * @param[in] p_sysmon is a pointer to the sysmon_ctx_t instance. 
 * @param[out] p_data is the memory that is updated with the 
 *       acquisition time for all the channels. Use
 *       SYSMON_SEQ_CH__* to interpret the channel numbers. Bit
 *       masks of 1 are the channels for which acquisition cycles
 *       are extended and bit mask of 0 are the channels for
 *       which acquisition cycles are not extended.
 * 
 *
 * @return error code
 */
static ccl_err_t _sysmon_get_seq_acq_time(sysmon_ctx_t *p_sysmon, b_u32 *p_data)
{
    b_u32 regval, regval1;

    /*
     * Assert the input arguments.
     */
    if ( !p_sysmon || !p_data) {
        PDEBUG("Error! Invalid input parameters\n");
        SYSMON_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Get the Acquisition cycles for the specified channels from the ADC
     * channel Acquisition Time Sequencer Registers.
     */
    SYSMON_REG_READ(p_sysmon,  SYSMON_SEQ06_OFFSET, 
                    sizeof(b_u32), 0, 
                    (b_u8 *)&regval, sizeof(b_u32));
    regval &= SYSMON_SEQ06_CH_VALID_MASK;
    SYSMON_REG_READ(p_sysmon, SYSMON_SEQ07_OFFSET, 
                    sizeof(b_u32), 0, 
                    (b_u8 *)&regval1, sizeof(b_u32));
    regval1 &= SYSMON_SEQ07_CH_VALID_MASK; 
    regval |= regval1 << SYSMON_SEQ_CH_AUX_SHIFT;
    *p_data = regval;

    return CCL_OK;
}

/**
 * This functions sets the contents of the given Alarm Threshold Register.
 *
 * @param[in] p_sysmon is a pointer to the sysmon_ctx_t instance.
 * @param[in] alarm_thr is the index of an Alarm Threshold
 *       Register to be set. Use SYSMON_ATR_* constants to specify
 *       the index.
 * @param[in] value is the 16-bit threshold value to write into
 *        the register.
 *
 * @return	error code
 */
static ccl_err_t _sysmon_set_alarm_threshold(sysmon_ctx_t *p_sysmon, b_u8 alarm_thr, b_u16 value)
{
    b_u32 regval = value;
    /*
     * Assert the input arguments.
     */
    if ( !p_sysmon || 
         (alarm_thr > SYSMON_ATR_VCCPDRO_LOWER) ) {
        PDEBUG("Error! Invalid input parameters\n");
        SYSMON_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Write the value into the specified Alarm Threshold Register.
     */
    SYSMON_REG_WRITE(p_sysmon, SYSMON_ATR_TEMP_UPPER_OFFSET + alarm_thr, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;

}

/**
 * This function return the contents of the specified Alarm Threshold Register.
 *
 * @param[in] p_sysmon is a pointer to the sysmon_ctx_t instance.
 * @param[in] alarm_thr is the index of an Alarm Threshold 
 *       Register to be read. Use SYSMON_ATR_* constants to specify
 *       the index.
 * @param[out] p_data is the memory that is updated with a 16-bit
 *             value representing the contents of the selected
 *             Alarm Threshold Register 
 *  
 * @return error code
 */
static ccl_err_t _sysmon_get_alarm_threshold(sysmon_ctx_t *p_sysmon, b_u8 alarm_thr, b_u16 *p_data)
{
    b_u32 regval;

    /*
     * Assert the input arguments.
     */
    if ( !p_sysmon || 
         (alarm_thr > SYSMON_ATR_VCCPDRO_LOWER) ) {
        PDEBUG("Error! Invalid input parameters\n");
        SYSMON_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Read the specified Alarm Threshold Register and return
     * the value
     */
    SYSMON_REG_READ(p_sysmon, 
                    (SYSMON_ATR_TEMP_UPPER_OFFSET + alarm_thr), 
                    sizeof(b_u32), 0, 
                    (b_u8 *)&regval, sizeof(b_u32));

    *p_data = (b_u16)regval;

    return CCL_OK;
}

/**
 *
 * This function enables programming of the powerdown temperature for the
 * OverTemp signal in the OT Powerdown register.
 *
 * @param[in]	p_sysmon is a pointer to the sysmon_ctx_t instance.
 *
 * @return	error code
 *
 */
static ccl_err_t _sysmon_enable_user_over_temp(sysmon_ctx_t *p_sysmon)
{
    b_u32 regval;

    /*
     * Assert the input arguments.
     */
    if ( !p_sysmon ) {
        PDEBUG("Error! Invalid input parameters\n");
        SYSMON_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Read the OT upper Alarm Threshold Register.
     */
    SYSMON_REG_READ(p_sysmon, SYSMON_ATR_OT_UPPER_OFFSET, 
                    sizeof(b_u32), 0, 
                    (b_u8 *)&regval, sizeof(b_u32));
    regval &= ~(SYSMON_ATR_OT_UPPER_ENB_MASK);

    /*
     * Preserve the powerdown value and write OT enable value the into the
     * OT Upper Alarm Threshold Register.
     */
    regval |= SYSMON_ATR_OT_UPPER_ENB_VAL;

    SYSMON_REG_WRITE(p_sysmon, SYSMON_ATR_OT_UPPER_OFFSET, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/**
 *
 * This function disables programming of the powerdown temperature for the
 * OverTemp signal in the OT Powerdown register.
 *
 * @param[in]	p_sysmon is a pointer to the sysmon_ctx_t instance.
 *
 * @return	error code
 *
 */
static ccl_err_t _sysmon_disable_user_over_temp(sysmon_ctx_t *p_sysmon)
{
    b_u32 regval;

    /*
     * Assert the input arguments.
     */
    if ( !p_sysmon ) {
        PDEBUG("Error! Invalid input parameters\n");
        SYSMON_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Read the OT Upper Alarm Threshold Register.
     */
    SYSMON_REG_READ(p_sysmon, SYSMON_ATR_OT_UPPER_OFFSET, 
                    sizeof(b_u32), 0, 
                    (b_u8 *)&regval, sizeof(b_u32));
    regval &= ~SYSMON_ATR_OT_UPPER_ENB_MASK;
    SYSMON_REG_WRITE(p_sysmon, SYSMON_ATR_OT_UPPER_OFFSET, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/**
 * The function enables the Event mode or Continuous mode in the sequencer mode.
 *
 * @param[in] p_sysmon is a pointer to the sysmon_ctx_t instance.
 *  @param[in] is_event_mode is a boolean parameter that
 *       specifies continuous sampling (specify CCL_FALSE) or
 *       event driven sampling mode (specify CCL_TRUE) for the
 *       given channel.
 *
 * @return	error code
 *
 */
static ccl_err_t _sysmon_set_sequencer_event(sysmon_ctx_t *p_sysmon, 
                                           b_bool is_event_mode)
{
    b_u32 regval;

    /*
     * Assert the input arguments.
     */
    if ( !p_sysmon ) {
        PDEBUG("Error! Invalid input parameters\n");
        SYSMON_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Read the Configuration Register 0.
     */
    SYSMON_REG_READ(p_sysmon, SYSMON_CFR0_OFFSET, 
                    sizeof(b_u32), 0, 
                    (b_u8 *)&regval, sizeof(b_u32));
    regval &= ~SYSMON_CFR0_EC_MASK;

    /*
     * Set the ADC mode.
     */
    if (is_event_mode == CCL_TRUE)
        regval |= SYSMON_CFR0_EC_MASK;
    else
        regval &= ~SYSMON_CFR0_EC_MASK;

    SYSMON_REG_WRITE(p_sysmon, SYSMON_CFR0_OFFSET, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/**
 * This function return the sampling mode.
 *
 * @param[in] p_sysmon is a pointer to the sysmon_ctx_t instance.
 * @param[out] p_data is the memory that is updated with the sampling mode
 *		- 0 specifies continuous sampling
 *		- 1 specifies event driven sampling mode
 *
 * @return error code
 */
static ccl_err_t _sysmon_get_sampling_mode(sysmon_ctx_t *p_sysmon, b_u32 *p_data)
{
    b_u32 regval;

    /*
     * Assert the input arguments.
     */
    if ( !p_sysmon || !p_data ) {
        PDEBUG("Error! Invalid input parameters\n");
        SYSMON_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Read the sampling mode from the Configuration Register 0.
     */
    SYSMON_REG_READ(p_sysmon, SYSMON_CFR0_OFFSET, 
                    sizeof(b_u32), 0, 
                    (b_u8 *)&regval, sizeof(b_u32));
    regval &= SYSMON_CFR0_EC_MASK;
    *p_data = !!regval;

    return CCL_OK;
}

/**
 * This function sets the External Mux mode.
 *
 * @param[in]	p_sysmon is a pointer to the sysmon_ctx_t instance.
 * @param[in] 	mode specifies whether External Mux is used
 *		- CCL_FALSE specifies NO external MUX
 *		- CCL_TRUE specifies External Mux is used
 * @param[in]	channel specifies the channel to be used for the
 *		external Mux. Please read the Device Spec for which
 *		channels are valid for which mode.
 *
 * @return	error code
 */
static ccl_err_t _sysmon_set_mode(sysmon_ctx_t *p_sysmon, b_bool mode, b_u8 channel)
{
    b_u32 regval;

    /*
     * Assert the input arguments.
     */
    if ( !p_sysmon ) {
        PDEBUG("Error! Invalid input parameters\n");
        SYSMON_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Read the Configuration Register 0.
     */
    SYSMON_REG_READ(p_sysmon, SYSMON_CFR0_OFFSET, 
                    sizeof(b_u32), 0, 
                    (b_u8 *)&regval, sizeof(b_u32));
    regval &= ~SYSMON_CFR0_MUX_MASK;

    /*
     * Select the Mux mode and the channel to be used.
     */
    if (mode == CCL_TRUE) {
        regval |= (channel & SYSMON_CFR0_CHANNEL_MASK) | 
                  SYSMON_CFR0_MUX_MASK;
    }

    /*
     * Write the mux mode into the Configuration Register 0.
     */
    SYSMON_REG_WRITE(p_sysmon, SYSMON_CFR0_OFFSET, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/**
 * This function sets the Power Down mode.
 *
 * @param[in] p_sysmon is a pointer to the sysmon_ctx_t instance.
 * @param[in] mode specifies the Power Down mode
 *		- SYSMON_PD_MODE_NONE specifies NO Power Down (Both ADC A and
 *		ADC B are enabled)
 *		- SYSMON_PD_MODE_ADCB specfies the Power Down of ADC B
 *		- SYSMON_PD_MODE_SYSMON specifies the Power Down of
 *		both ADC A and ADC B.
 *
 * @return	error code
 */
static ccl_err_t _sysmon_set_powerdown_mode(sysmon_ctx_t *p_sysmon, b_u32 mode)
{
    b_u32 regval;

    /*
     * Assert the input arguments.
     */
    if ( !p_sysmon ||
         (mode >= SYSMON_PD_MODE_SYSMON) ) {
        PDEBUG("Error! Invalid input parameters\n");
        SYSMON_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Read the Configuration Register 2.
     */
    SYSMON_REG_READ(p_sysmon, SYSMON_CFR2_OFFSET, 
                    sizeof(b_u32), 0, 
                    (b_u8 *)&regval, sizeof(b_u32));
    regval &= ~SYSMON_CFR2_PD_MASK;
    /*
     * Select the Power Down mode.
     */
    regval |= (mode << SYSMON_CFR2_PD_SHIFT);

    SYSMON_REG_WRITE(p_sysmon, SYSMON_CFR2_OFFSET, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/**
 * This function gets the Power Down mode.
 *
 * @param[in] p_sysmon is a pointer to the sysmon_ctx_t instance.
 * @param[out] p_data is the memory that is updated with the Power Down mode
 *		- SYSMON_PD_MODE_NONE specifies NO Power Down (Both ADC A and
 *		ADC B are enabled)
 *		- SYSMON_PD_MODE_ADCB specfies the Power Down of ADC B
 *		- SYSMON_PD_MODE_SYSMON specifies the Power Down of
 *		both ADC A and ADC B.
 * 
 * @return error code
 */
static ccl_err_t _sysmon_get_powerdown_mode(sysmon_ctx_t *p_sysmon, b_u32 *p_data)
{
    b_u32 regval;

    /*
     * Assert the input arguments.
     */
    if ( !p_sysmon || !p_data ) {
        PDEBUG("Error! Invalid input parameters\n");
        SYSMON_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Read the Power Down mode.
     */
    SYSMON_REG_READ(p_sysmon, SYSMON_CFR2_OFFSET, 
                    sizeof(b_u32), 0, 
                    (b_u8 *)&regval, sizeof(b_u32));
    regval &= ~SYSMON_CFR2_PD_MASK;
    /*
     * Return the Power Down mode.
     */
    *p_data =  (regval >> SYSMON_CFR2_PD_SHIFT);

    return CCL_OK;
}

static ccl_err_t _fsysmon_cmd_init(devo_t devo, 
                                   __attribute__((unused)) device_cmd_parm_t parms[], 
                                   __attribute__((unused)) b_u16 parms_num)
{
    void *p_priv = devman_device_private(devo);

    /* start the device */
    _ret = _sysmon_init(p_priv);
    if ( _ret ) {
        PDEBUG("_sysmon_init error\n");
        SYSMON_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

/* device commands */
static device_cmd_t _cmds[] = 
{
    { 
        .name = "init", .f_cmd = _fsysmon_cmd_init, .parms_num = 0,
        .help = "Init SYSMON device" 
    },
};

/* Device instance hw-specific initializations 
 */
static ccl_err_t _fsysmon_add(void *devo, void *brdspec) {
    sysmon_ctx_t       *p_sysmon;
    sysmon_brdspec_t   *p_brdspec;

    if ( !devo || !brdspec ) {
        PDEBUG("Error! Invalid input parameters\n");
        SYSMON_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_sysmon = devman_device_private(devo);
    if ( !p_sysmon ) {
        PDEBUG("NULL context area\n");
        SYSMON_ERROR_RETURN(CCL_FAIL);
    }
    p_brdspec = (sysmon_brdspec_t *)brdspec;
    /* back reference the device manager object handle */    
    p_sysmon->devo = devo;    
    /* save the board specific info */
    memcpy(&p_sysmon->brdspec, p_brdspec, sizeof(sysmon_brdspec_t));
#if 0
    /* init the device */
    _ret = _sysmon_init(p_sysmon);
    if ( _ret ) {
        PDEBUG("_sysmon_init error: %d\n", _ret);
        SYSMON_ERROR_RETURN(_ret);
    }
#endif
    return CCL_OK;
}

/* Device instance hw-specific deinitializations 
 */
static ccl_err_t _fsysmon_cut(void *devo)
{
    sysmon_ctx_t  *p_sysmon;

    if ( !devo ) {
        PDEBUG("Error! Invalid input parameters\n");
        SYSMON_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_sysmon = devman_device_private(devo);
    if ( !p_sysmon ) {
        PDEBUG("NULL context area\n");
        SYSMON_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

/* device type description structure */
static device_driver_t _sysmon_driver = {
    .name = "sysmon",
    .f_add = _fsysmon_add,
    .f_cut = _fsysmon_cut,
    .attrs = _attrs,
    .attrs_num = sizeof(_attrs)/sizeof(_attrs[0]),
    .cmds = _cmds,
    .cmds_num = sizeof(_cmds)/sizeof(_cmds[0]),
    .priv_size = sizeof(sysmon_ctx_t)
}; 

/* Initialize SYSMON device type
 */
ccl_err_t devsysmon_init(void)
{
    /* register device type */
    _ret = devman_driver_register(&_sysmon_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to register driver\n");
        SYSMON_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}

/* Deinitialize SYSMON device type
 */
ccl_err_t devsysmon_fini(void)
{
    /* unregister device type */
    _ret = devman_driver_unregister(&_sysmon_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to unregister driver\n");
        SYSMON_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}
