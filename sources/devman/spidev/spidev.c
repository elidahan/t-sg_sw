/********************************************************************************/
/**
 * @file spidev.c
 * @brief Generic SPI device manager driver
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <semaphore.h>
#include <linux/limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "axispi.h"
#include "spidev.h"

//#define SPIDEV_DEBUG
/* printing/error-returning macros */
#ifdef SPIDEV_DEBUG
    #define PDEBUG(fmt, args...) \
         ccl_syslog_err("SPIDEV_DEBUG: %s[%d]: " fmt, __FUNCTION__, __LINE__, ## args)
#else
    #define PDEBUG(fmt, args...)
#endif
#define SPIDEV_ERROR_RETURN(err) do { \
   ccl_syslog_err("Error! %s[%d], error=%i\n", __FUNCTION__, __LINE__, (err)); \
   return err; \
} while (0)

/* spidev instructions */
#define SPIDEV_CMD_WRITE_STATUS_REGISTER    0x01
#define SPIDEV_CMD_PAGE_PROGRAM             0x02
#define SPIDEV_CMD_READ_DATA                0x03
#define SPIDEV_CMD_READ_STATUS_REGISTER     0x05
#define SPIDEV_CMD_WRITE_EN                 0x06
#define SPIDEV_CMD_PROGRAM_EXECUTE          0x10
#define SPIDEV_CMD_PAGE_READ_DATA           0x13
#define SPIDEV_CMD_MANUFACTURER_ID          0x90
#define SPIDEV_CMD_JEDEC_ID                 0x9F
#define SPIDEV_CMD_UNIQUE_ID                0x4B

/* the spidev internal control structure */
typedef struct spidev_ctx_t {
    void                  *devo;
    gen_spi_dev_brdspec_t brdspec;
} spidev_ctx_t;

static ccl_err_t    _ret;

/* spidev register read */
static ccl_err_t _spidev_reg_read(void *p_priv, b_u32 offset, 
                                  b_u32 offset_sz, b_u32 idx, 
                                  b_u8 *p_value, b_u32 size)
{
    spidev_ctx_t   *p_spidev;
    spi_route_t    spi_route;
    b_u8 *buf;
    b_u32 i;

    if ( !p_priv || !p_value || idx || !size ) {
        PDEBUG("Error! Invalid input parameters\n");
        SPIDEV_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    p_spidev = (spidev_ctx_t *)p_priv;     
    buf = calloc(1, p_spidev->brdspec.addr_width);
    if (!buf) {
        PDEBUG("Can't allocate memory\n");
        return CCL_NO_MEM_ERR;
    }

    for (i = 0; i < p_spidev->brdspec.addr_width; i++) {
        buf[i] = (offset >> (8*i)) & 0xff;
    }

    memcpy(&spi_route, &p_spidev->brdspec.dev_id, sizeof(spi_route_t));
    PDEBUG("axispi_devid=%d, axispi_slaveid=%d offset=0x%03x, size=%d\n", 
           p_spidev->brdspec.dev_id, p_spidev->brdspec.cs, offset, size);

    _ret = devaxispi_read_buff(&spi_route, buf, p_value, size);
    if ( _ret ) {
        free(buf);
        PDEBUG("Error! Failed to write over spi bus\n");
        SPIDEV_ERROR_RETURN(_ret);
    }
    free(buf);

    return _ret;
}

/* spidev register write */
static ccl_err_t _spidev_reg_write(void *p_priv, b_u32 offset, 
                                   __attribute__((unused)) b_u32 offset_sz, 
                                   b_u32 idx, b_u8 *p_value, b_u32 size)
{
    spidev_ctx_t   *p_spidev;
    spi_route_t    spi_route;
    b_u8 *buf;
    b_u32 i;


    if ( !p_priv || !p_value || idx || !size ) {
        PDEBUG("Error! Invalid input parameters\n");
        SPIDEV_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    p_spidev = (spidev_ctx_t *)p_priv;     
    buf = calloc(1, p_spidev->brdspec.addr_width + size);
    if (!buf) {
        PDEBUG("Can't allocate memory\n");
        return CCL_NO_MEM_ERR;
    }

    for (i = 0; i < p_spidev->brdspec.addr_width + size; i++) {
        if (i < p_spidev->brdspec.addr_width) {
            buf[i] = (offset >> (8*i)) & 0xff;
        } else {
            buf[i] = p_value[i];
        }
    }

    memcpy(&spi_route, &p_spidev->brdspec.dev_id, sizeof(spi_route_t));

    PDEBUG("axispi_devid=%d, axispi_slaveid=%d offset=0x%03x, size=%d\n", 
           p_spidev->brdspec.dev_id, p_spidev->brdspec.cs, offset, size);

    _ret = devaxispi_write_buff(&spi_route, buf, buf, size);
    if ( _ret ) {
        free(buf);
        PDEBUG("Error! Failed to write over spi bus\n");
        SPIDEV_ERROR_RETURN(_ret);
    }
    free(buf);

    return _ret;
}

/* spidev command and register read */
static ccl_err_t _spidev_cmd_reg_read(void *p_priv, b_u32 cmd, 
                                      b_u32 cmd_sz, 
                                      b_u8 *p_value, b_u32 size)
{
    spidev_ctx_t   *p_spidev;
    spi_route_t    spi_route;
    b_u8           *buf;
    b_u32          i;


    if ( !p_priv || !p_value || !cmd_sz || !size ) {
        PDEBUG("Error! Invalid input parameters\n");
        SPIDEV_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    p_spidev = (spidev_ctx_t *)p_priv;     
    buf = calloc(1, cmd_sz);
    if (!buf) {
        PDEBUG("Can't allocate memory\n");
        return CCL_NO_MEM_ERR;
    }

    /* write command */
    for (i = 0; i < cmd_sz; i++) 
        buf[i] = (cmd >> (8*i)) & 0xff;

    memcpy(&spi_route, &p_spidev->brdspec.dev_id, sizeof(spi_route_t));

    PDEBUG("axispi_devid=%d, axispi_slaveid=%d cmd=0x%03x, size=%d\n", 
           p_spidev->brdspec.dev_id, p_spidev->brdspec.cs, cmd, size);

    _ret = devaxispi_read_buff(&spi_route, buf, p_value, size);
    if ( _ret ) {
        free(buf);
        PDEBUG("Error! Failed to read over spi bus\n");
        SPIDEV_ERROR_RETURN(_ret);
    }

    free(buf);

    return _ret;
}

/* spidev command and data read */
static ccl_err_t _spidev_cmd_data_read(void *p_priv, b_u32 cmd, 
                                        b_u32 cmd_sz, b_u32 offset, 
                                        b_u8 *p_value, b_u32 size)
{
    spidev_ctx_t   *p_spidev;
    spi_route_t    spi_route;
    b_u8           *buf;
    b_u32          i, j;


    if ( !p_priv || !p_value || !cmd_sz || !size ) {
        PDEBUG("Error! Invalid input parameters\n");
        SPIDEV_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    p_spidev = (spidev_ctx_t *)p_priv;     
    buf = calloc(1, p_spidev->brdspec.addr_width + cmd_sz);
    if (!buf) {
        PDEBUG("Can't allocate memory\n");
        return CCL_NO_MEM_ERR;
    }

    /* write command */
    for (i = 0; i < cmd_sz; i++) 
        buf[i] = (cmd >> (8*i)) & 0xff;
    /* write offset */
    for (i = 0; i < p_spidev->brdspec.addr_width; i++) {
        j = (p_spidev->brdspec.addr_width - i - 1) * 8;
        buf[cmd_sz + i] = (offset >> j) & 0xff;
    }

    memcpy(&spi_route, &p_spidev->brdspec.dev_id, sizeof(spi_route_t));

    PDEBUG("axispi_devid=%d, axispi_slaveid=%d cmd=0x%03x, size=%d\n", 
           p_spidev->brdspec.dev_id, p_spidev->brdspec.cs, cmd, size);

    _ret = devaxispi_read_buff(&spi_route, buf, p_value, size);
    if ( _ret ) {
        free(buf);
        PDEBUG("Error! Failed to read over spi bus\n");
        SPIDEV_ERROR_RETURN(_ret);
    }

    free(buf);

    return _ret;
}

/* spidev command and register write */
static ccl_err_t _spidev_cmd_reg_write(void *p_priv, b_u32 cmd, 
                                        b_u32 cmd_sz, b_u8 *p_value, 
                                        b_u32 size)
{
    spidev_ctx_t   *p_spidev;
    spi_route_t    spi_route;
    b_u8           *buf;
    b_u32          i;


    if ( !p_priv || !p_value || !cmd_sz || !size ) {
        PDEBUG("Error! Invalid input parameters\n");
        SPIDEV_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    p_spidev = (spidev_ctx_t *)p_priv;     
    buf = calloc(1, cmd_sz + size);
    if (!buf) {
        PDEBUG("Can't allocate memory\n");
        return CCL_NO_MEM_ERR;
    }

    /* write command */
    for (i = 0; i < cmd_sz; i++) 
        buf[i] = (cmd >> (8*i)) & 0xff;
    for (i = 0; i < size; i++) 
        buf[cmd_sz + i] = p_value[i];

    memcpy(&spi_route, &p_spidev->brdspec.dev_id, sizeof(spi_route_t));

    PDEBUG("axispi_devid=%d, axispi_slaveid=%d cmd=0x%03x, size=%d\n", 
           p_spidev->brdspec.dev_id, p_spidev->brdspec.cs, cmd, size);

    _ret = devaxispi_write_buff(&spi_route, buf, buf, cmd_sz+size);
    if ( _ret ) {
        free(buf);
        PDEBUG("Error! Failed to write over spi bus\n");
        SPIDEV_ERROR_RETURN(_ret);
    }
    free(buf);

    return _ret;
}

/* spidev command and data write */
static ccl_err_t _spidev_cmd_data_write(void *p_priv, b_u32 cmd, 
                                        b_u32 cmd_sz, b_u32 offset, 
                                        b_u8 *p_value, b_u32 size)
{
    spidev_ctx_t   *p_spidev;
    spi_route_t    spi_route;
    b_u8           *buf;
    b_u32          i, j;


    if ( !p_priv || !cmd_sz ) {
        PDEBUG("Error! Invalid input parameters\n");
        SPIDEV_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    p_spidev = (spidev_ctx_t *)p_priv;     
    buf = calloc(1, p_spidev->brdspec.addr_width + cmd_sz + size);
    if (!buf) {
        PDEBUG("Can't allocate memory\n");
        return CCL_NO_MEM_ERR;
    }

    /* write command */
    for (i = 0; i < cmd_sz; i++) 
        buf[i] = (cmd >> (8*i)) & 0xff;
    /* write offset */
    for (i = 0; i < p_spidev->brdspec.addr_width; i++) {
        j = (p_spidev->brdspec.addr_width - i - 1) * 8;
        buf[cmd_sz + i] = (offset >> j) & 0xff;
    }
    for (i = 0; i < size; i++) 
        buf[p_spidev->brdspec.addr_width + cmd_sz + i] = p_value[i];

    memcpy(&spi_route, &p_spidev->brdspec.dev_id, sizeof(spi_route_t));

    PDEBUG("axispi_devid=%d, axispi_slaveid=%d cmd=0x%03x, size=%d\n", 
           p_spidev->brdspec.dev_id, p_spidev->brdspec.cs, cmd, size);

    _ret = devaxispi_write_buff(&spi_route, buf, buf, 
                                cmd_sz + p_spidev->brdspec.addr_width + size);
    if ( _ret ) {
        free(buf);
        PDEBUG("Error! Failed to write over spi bus\n");
        SPIDEV_ERROR_RETURN(_ret);
    }
    free(buf);

    return _ret;
}

/* spidev command and data write */
static ccl_err_t _spidev_cmd_data_burst_write(void *p_priv, b_u32 cmd, 
                                              b_u32 cmd_sz, b_u32 offset, 
                                              b_u8 *p_value, b_u32 size)
{
    spidev_ctx_t   *p_spidev;
    spi_route_t    spi_route;
    b_u8           *buf;
    b_u32          i, j;


    if ( !p_priv || !cmd_sz ) {
        PDEBUG("Error! Invalid input parameters\n");
        SPIDEV_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    p_spidev = (spidev_ctx_t *)p_priv;     
    buf = calloc(1, p_spidev->brdspec.addr_width + cmd_sz + size);
    if (!buf) {
        PDEBUG("Can't allocate memory\n");
        return CCL_NO_MEM_ERR;
    }

    /* write command */
    for (i = 0; i < cmd_sz; i++) 
        buf[i] = (cmd >> (8*i)) & 0xff;
    /* write offset */
    for (i = 0; i < p_spidev->brdspec.addr_width; i++) {
        j = (p_spidev->brdspec.addr_width - i - 1) * 8;
        buf[cmd_sz + i] = (offset >> j) & 0xff;
    }
    for (i = 0; i < size; i++) 
        buf[p_spidev->brdspec.addr_width + cmd_sz + i] = p_value[i];

    memcpy(&spi_route, &p_spidev->brdspec.dev_id, sizeof(spi_route_t));

    PDEBUG("axispi_devid=%d, axispi_slaveid=%d cmd=0x%03x, size=%d\n", 
           p_spidev->brdspec.dev_id, p_spidev->brdspec.cs, cmd, size);

    _ret = devaxispi_write_burst(&spi_route, buf, buf, 
                                 p_spidev->brdspec.burst_l,
                                 cmd_sz + p_spidev->brdspec.addr_width + size);
    if ( _ret ) {
        free(buf);
        PDEBUG("Error! Failed to write over spi bus\n");
        SPIDEV_ERROR_RETURN(_ret);
    }
    free(buf);

    return _ret;
}

/* spidev command and data write */
static ccl_err_t _spidev_data_write(void *p_priv, b_u8 *p_value, b_u32 size)
{
    spidev_ctx_t   *p_spidev;
    spi_route_t    spi_route;
    b_u32          i;


    if ( !p_priv || !p_value || !size ) {
        PDEBUG("Error! Invalid input parameters\n");
        SPIDEV_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    p_spidev = (spidev_ctx_t *)p_priv;     

    memcpy(&spi_route, &p_spidev->brdspec.dev_id, sizeof(spi_route_t));

    PDEBUG("axispi_devid=%d, axispi_slaveid=%d\n", 
           p_spidev->brdspec.dev_id, p_spidev->brdspec.cs);

    _ret = devaxispi_write_buff(&spi_route, p_value, p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to write over spi bus\n");
        SPIDEV_ERROR_RETURN(_ret);
    }

    return _ret;
}

/* spidev command write */
static ccl_err_t _spidev_cmd_write(void *p_priv, b_u32 cmd, b_u32 cmd_sz)
{
    spidev_ctx_t   *p_spidev;
    spi_route_t    spi_route;
    b_u8 *buf;
    b_u32 i;


    if ( !p_priv || !cmd_sz ) {
        PDEBUG("Error! Invalid input parameters\n");
        SPIDEV_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    p_spidev = (spidev_ctx_t *)p_priv;     
    buf = calloc(1, cmd_sz);
    if (!buf) {
        PDEBUG("Can't allocate memory\n");
        return CCL_NO_MEM_ERR;
    }

    for (i = 0; i < cmd_sz; i++) 
        buf[i] = (cmd >> (8*i)) & 0xff;

    memcpy(&spi_route, &p_spidev->brdspec.dev_id, sizeof(spi_route_t));

    PDEBUG("axispi_devid=%d, axispi_slaveid=%d cmd=0x%03x, cmd_sz=%d\n", 
           p_spidev->brdspec.dev_id, p_spidev->brdspec.cs, cmd, cmd_sz);

    _ret = devaxispi_write_buff(&spi_route, buf, buf, cmd_sz);
    if ( _ret ) {
        free(buf);
        PDEBUG("Error! Failed to write over spi bus\n");
        SPIDEV_ERROR_RETURN(_ret);
    }
    free(buf);

    return _ret;
}

static ccl_err_t _spidev_init(void *p_priv)
{
    spidev_ctx_t   *p_spidev;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        SPIDEV_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_spidev = (spidev_ctx_t *)p_priv;

    return CCL_OK;
}

static ccl_err_t _spidev_configure_test(void *p_priv)
{
    spidev_ctx_t   *p_spidev;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        SPIDEV_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_spidev = (spidev_ctx_t *)p_priv;

    return CCL_OK;
}

static ccl_err_t _spidev_run_test(void *p_priv, b_u8 *buf, b_u32 size)
{
    spidev_ctx_t   *p_spidev;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        SPIDEV_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_spidev = (spidev_ctx_t *)p_priv;
    if (size == sizeof(spidev_stats_t)) {
        spidev_stats_t *stats = (spidev_stats_t *)buf;
        _ret = _spidev_reg_read(p_spidev, SPIDEV_CMD_JEDEC_ID, 
                                0, 0, stats->jedec, 
                                DEVBRD_SPIDEV_JEDEC_ID_SIZE);
        if ( _ret ) 
            SPIDEV_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

ccl_err_t spidev_attr_read(void *p_priv, b_u32 attrid, 
                           b_u32 offset, 
                           __attribute__((unused)) b_u32 offset_sz, 
                           b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || !p_value || idx || !size ) {
        PDEBUG("Error! Invalid input parameters\n");
        SPIDEV_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    PDEBUG("attrid: %d, offset: %d, size:%d\n\n", attrid, offset, size);
    switch (attrid) {
    case eSPIDEV_CMD_MANUFACTURER_ID:
    case eSPIDEV_CMD_JEDEC_ID:
    case eSPIDEV_CMD_UNIQUE_ID:
    case eSPIDEV_BUFFER:
       _ret = _spidev_reg_read(p_priv, offset, offset_sz, idx, p_value, size);
        break;
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK) {
        PDEBUG("Failure in attribute id: %d\n", attrid);
    }

    return _ret;
}

ccl_err_t spidev_attr_write(void *p_priv, b_u32 attrid, 
                            b_u32 offset, 
                            __attribute__((unused)) b_u32 offset_sz, 
                            b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || !p_value || idx || !size ) {
        PDEBUG("Error! Invalid input parameters\n");
        SPIDEV_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    PDEBUG("attrid: %d, offset: %d, size:%d\n\n", attrid, offset, size);
    switch (attrid) {
    case eSPIDEV_BUFFER:
       _ret = _spidev_reg_write(p_priv, offset, offset_sz, idx, p_value, size);
        break;
    case eSPIDEV_INIT:
        _ret = _spidev_init(p_priv);
        break;
    case eSPIDEV_CONFIGURE_TEST:
        _ret = _spidev_configure_test(p_priv);
        break;
    case eSPIDEV_RUN_TEST:
        _ret = _spidev_run_test(p_priv, p_value, size);
        break;
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK) {
        PDEBUG("Failure in attribute id: %d\n", attrid);
    }

    return _ret;
}

/* spidev attributes definition */
static device_attr_t _attrs[] = {
    {
        .name = "name", 
        .type = eDEVMAN_ATTR_STRING, .maxnum = 1, 
        .offset = offsetof(struct spidev_ctx_t, brdspec.name),
        .size = DEVMAN_DEVNAME_LEN,
        .format = "%s"
    },
    {
        .name = "mfc_id", .id = eSPIDEV_CMD_MANUFACTURER_ID,
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = SPIDEV_CMD_MANUFACTURER_ID
    },
    {
        .name = "jedec_id", .id = eSPIDEV_CMD_JEDEC_ID,
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, 
        .size = DEVBRD_SPIDEV_JEDEC_ID_SIZE, 
        .offset_sz = sizeof(b_u32), .offset = SPIDEV_CMD_JEDEC_ID
    },
    {
        .name = "unique_id", .id = eSPIDEV_CMD_UNIQUE_ID,
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = SPIDEV_CMD_UNIQUE_ID
    },
    {
        .name = "buffer", .id = eSPIDEV_BUFFER,
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = 0
    },
    {
        .name = "init", .id = eSPIDEV_INIT, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "config_test", .id = eSPIDEV_CONFIGURE_TEST, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "run_test", .id = eSPIDEV_RUN_TEST, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, 
        .size = sizeof(spidev_stats_t), 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
};

static ccl_err_t _spidev_program(spidev_ctx_t  *p_spidev, char *file_name)
{
    FILE    *filep = NULL;
    int      file_size, buf_len;
    char    *buf, *pb;    
    b_u32   pages, start_page, sectors;
    b_u32   offset;
    b_u32   i;
    
    
    if ( !p_spidev || !file_name ) {
        PDEBUG("Error! Invalid input parameters\n");
        SPIDEV_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    _ret = CCL_OK;

    /* check the input stream(openability, readability, size) */ 
    filep = fopen(file_name, "r");
    if ( !filep )
    {
        PDEBUG("Error! Can't open FPGA image file %s\n", file_name);
        SPIDEV_ERROR_RETURN(CCL_FAIL);
    }
    /* get length of the file, check errors */
    fseek(filep, 0, SEEK_END);
    file_size = ftell(filep);	
    fseek(filep, 0, SEEK_SET);
    if ( file_size < 0 ) {
        fclose(filep);
        PDEBUG("Error! The file %s is not a seekable\n", file_name);
        SPIDEV_ERROR_RETURN(CCL_FAIL);
    } else if ( !file_size ) {
        fclose(filep);
        PDEBUG("Error! The file %s is empty\n", file_name);
        SPIDEV_ERROR_RETURN(CCL_FAIL);
    }

    /* buffer size must be page size multiple */
    buf_len = !(file_size % p_spidev->brdspec.page_size) ? 
                file_size : 
                file_size + (p_spidev->brdspec.page_size - 
                             (file_size % p_spidev->brdspec.page_size));

    /* buffer allocation */
    buf = malloc(buf_len);
    if ( !buf ) {
        fclose(filep);
        PDEBUG("Error! Can't allocate the image temporary room\n");
        SPIDEV_ERROR_RETURN(CCL_FAIL);
    }

    pb = buf;

    /* set 0xFF padding */
    memset(buf, 0xff, buf_len);

    /* copy image into the memory */
    if ( fread(buf, 1, file_size, filep) < file_size ) {
        PDEBUG("Error! Can't read the file %s\n", file_name);
        _ret = CCL_FAIL;
        goto _lspidev_program_exit;
    }
   
    sectors = (buf_len / p_spidev->brdspec.sector_size) + 1;
    ccl_syslog_err("SPI: Passed image file=%s, file_size=%d, padded to %d, sectors to erase=%d\n", 
           file_name, file_size, buf_len, sectors);
    for (i = 0; i < sectors; i++) {
#if 1
        /* 1. Perform write enable */
        _ret = _spidev_cmd_write(p_spidev, 
                                 SPIDEV_CMD_WRITE_EN, 
                                 1);
        if ( _ret ) {
            PDEBUG("Error! _spidev_cmd_write: %d\n",
                   SPIDEV_CMD_WRITE_EN);
            SPIDEV_ERROR_RETURN(_ret);
        }
        offset = i * p_spidev->brdspec.sector_size;
        /* check if page/byte addressable */
        if (p_spidev->brdspec.is_nand) 
            offset >>= p_spidev->brdspec.page_width;
        ccl_syslog_err("offset=0x%x\n", offset);
        /* 2. Erase the sector */
        _ret = _spidev_cmd_data_write(p_spidev, 
                                      p_spidev->brdspec.sector_erase_cmd, 
                                      p_spidev->brdspec.sector_erase_cmd_width, 
                                      offset, NULL, 0);
        if ( _ret ) {
            PDEBUG("Error! _spidev_cmd_write: %d\n",
                   p_spidev->brdspec.sector_erase_cmd);
            SPIDEV_ERROR_RETURN(_ret);
        }
#endif
    }

    usleep(1000000);
    /* 4. Write down buffer */
    pages = buf_len / p_spidev->brdspec.page_size;
    start_page = p_spidev->brdspec.bloff / p_spidev->brdspec.page_size;
    for (i = 0; i < pages; i++) {
        /* 3. Perform write enable */
        _ret = _spidev_cmd_write(p_spidev, 
                                 SPIDEV_CMD_WRITE_EN, 
                                 1);
        if ( _ret ) {
            PDEBUG("Error! _spidev_cmd_write: %d\n",
                   SPIDEV_CMD_WRITE_EN);
            SPIDEV_ERROR_RETURN(_ret);
        }
        if (!p_spidev->brdspec.is_nand) {
            offset = p_spidev->brdspec.bloff + 
                     (i * p_spidev->brdspec.page_size);
            PDEBUG("offset=0x%x, pb=0x%x, buf=0x%x\n",
                    offset, pb, buf);
            _ret = _spidev_cmd_data_write(p_spidev, SPIDEV_CMD_PAGE_PROGRAM, 1, 
                                          offset, buf, p_spidev->brdspec.page_size);
            if ( _ret ) {
                PDEBUG("Error! _spidev_cmd_data_write\n");
                SPIDEV_ERROR_RETURN(_ret);
            }
        } else {
            _ret = _spidev_cmd_data_write(p_spidev, SPIDEV_CMD_PAGE_PROGRAM, 2, 
                                          0, buf, p_spidev->brdspec.page_size);
            if ( _ret ) {
                PDEBUG("Error! _spidev_cmd_data_write. cmd: 0x%02x\n",
                       SPIDEV_CMD_PAGE_PROGRAM);
                SPIDEV_ERROR_RETURN(_ret);
            }
            _ret = _spidev_cmd_data_write(p_spidev, SPIDEV_CMD_PROGRAM_EXECUTE, 2, 
                                          start_page+i, NULL, 0);
            if ( _ret ) {
                PDEBUG("Error! _spidev_cmd_data_write. cmd: 0x%02x\n",
                       SPIDEV_CMD_PROGRAM_EXECUTE);
                SPIDEV_ERROR_RETURN(_ret);
            }            
        }

        if (!(i%10)) 
            ccl_syslog_err("%d ", i);
        if (!((i+1)%100)) 
            ccl_syslog_err("\n");
        buf += p_spidev->brdspec.page_size;
    }
    ccl_syslog_err("\n");
    
_lspidev_program_exit:    
    /* release the resources */
    free(pb);
    fclose(filep);
    
    return _ret;
}

static ccl_err_t _spidev_cmd_program(devo_t devo, device_cmd_parm_t parms[], b_u16 parms_num)
{
    spidev_ctx_t  *p_spidev;
    char          *fname = (char *)parms[0].value;
    
    p_spidev = devman_device_private(devo);

    if (fname) {
        /* show image revision, show current running revision, program, force the operation, 
           even if the file is not a new */
        _ret = _spidev_program( p_spidev,fname );
        if ( _ret ) 
            PDEBUG("Error! Can't program SPI flash using the %s image file\n", fname);
    }

    return CCL_OK;
}

static ccl_err_t _spidev_cmd_read_mem(devo_t devo, 
                                      device_cmd_parm_t parms[], 
                                      b_u16 parms_num)
{
    spidev_ctx_t  *p_spidev;
    b_u32         i;
    b_u32         offset = 0;
    b_u32         count = 0;
    char          *buf, *pb;    
    FILE          *filep = NULL;
    char          *fname;
    b_u32         pages, start_page, buf_size;

    offset = (b_u32)parms[0].value;
    count = (b_u32)parms[1].value;
    fname = (char *)parms[2].value;

    _ret = CCL_OK;

    /* check the input stream(openability, writeability, size) */ 
    filep = fopen(fname, "w");
    if ( !filep )
    {
        PDEBUG("Error! Can't open FPGA image file %s\n", fname);
        SPIDEV_ERROR_RETURN(CCL_FAIL);
    }

    p_spidev = devman_device_private(devo);

    if (!p_spidev->brdspec.is_nand) {
        buf = calloc(1, count);
        if (!buf) {
            PDEBUG("Can't allocate memory\n");
            SPIDEV_ERROR_RETURN(CCL_NO_MEM_ERR);
        }
        buf_size = count;
        _ret = _spidev_cmd_data_read(p_spidev, SPIDEV_CMD_READ_DATA,
                                     1, offset, buf, count);
        if (_ret) {
            PDEBUG("Error! _spidev_cmd_data_read. cmd: 0x%02x\n",
                   SPIDEV_CMD_READ_DATA);
            _ret = CCL_FAIL;
            goto _lspidev_program_exit;
        }
    } else {
        pages = count / p_spidev->brdspec.page_size + 1;
        buf = calloc(pages, p_spidev->brdspec.page_size);
        if (!buf) {
            PDEBUG("Can't allocate memory\n");
            SPIDEV_ERROR_RETURN(CCL_NO_MEM_ERR);
        }
        buf_size = pages * p_spidev->brdspec.page_size;
        pb = buf;
        start_page = offset / p_spidev->brdspec.page_size;
        PRINTL("pages to read %d, starting from page %d, buf size=%d\n",
               pages, start_page, buf_size);
        for (i = 0; i < pages; i++) {
            _ret = _spidev_cmd_data_write(p_spidev, SPIDEV_CMD_PAGE_READ_DATA, 2, 
                                          start_page+i, NULL, 0);
            if ( _ret ) {
                PDEBUG("Error! _spidev_cmd_data_write. cmd: 0x%02x\n",
                       SPIDEV_CMD_PAGE_READ_DATA);
                _ret = CCL_FAIL;
                goto _lspidev_program_exit;
            }
            _ret = _spidev_cmd_data_read(p_spidev, SPIDEV_CMD_READ_DATA,
                                         2, 0, pb, p_spidev->brdspec.page_size);
            if (_ret) {
                PDEBUG("Error! _spidev_cmd_data_read. cmd: 0x%02x\n",
                       SPIDEV_CMD_READ_DATA);
                _ret = CCL_FAIL;
                goto _lspidev_program_exit;
            }
            pb += p_spidev->brdspec.page_size;
            if (!(i%10)) 
                PRINTL("%d ", i);
            if (!((i+1)%100)) 
                PRINTL("\n");
        }
    }
    /* copy buffer into the file */
    if ( fwrite(buf+4, 1, buf_size-4, filep) < (buf_size-4) )  {
        PDEBUG("Error! Can't write to the file %s %d bytes\n", 
               fname, buf_size-4);
        _ret = CCL_FAIL;
        goto _lspidev_program_exit;
    }

_lspidev_program_exit:    
    free(buf);
    fclose(filep);

    return _ret;
}

static ccl_err_t _spidev_cmd_read_reg(devo_t devo, 
                                      device_cmd_parm_t parms[], 
                                      b_u16 parms_num)
{
    spidev_ctx_t  *p_spidev;
    b_u32         cmd = 0;
    b_u32         cmd_sz = 0;
    b_u32         size = 0;
    b_u8          *buf;

    cmd = (b_u32)parms[0].value;
    cmd_sz = (b_u32)parms[1].value;
    size = (b_u32)parms[2].value;

    buf = calloc(1, size);
    if (!buf) {
        PDEBUG("Can't allocate memory\n");
        return CCL_NO_MEM_ERR;
    }

    p_spidev = devman_device_private(devo);

    PRINTL("p_spidev=0x%x, cmd=0x%02x, cmd_sz=%d, buf=0x%x, size=%d\n", 
           p_spidev, cmd, cmd_sz, buf, size);

    _ret = _spidev_cmd_reg_read(p_spidev, cmd,
                                cmd_sz, buf, size);
    if (_ret) {
        free(buf);
        PDEBUG("_spidev_cmd_reg_read error\n");
        SPIDEV_ERROR_RETURN(_ret);
    }

    DUMPBUF(buf, size, 16);

    free(buf);

    return CCL_OK;
}

static ccl_err_t _spidev_cmd_write_reg(devo_t devo, 
                                      device_cmd_parm_t parms[], 
                                      b_u16 parms_num)
{
    spidev_ctx_t  *p_spidev;
    b_u32         i;
    b_u32         cmd = 0;
    b_u32         cmd_sz = 0;
    b_u32         value = 0;
    b_u32         size = 0;

    cmd = (b_u32)parms[0].value;
    cmd_sz = (b_u32)parms[1].value;
    value = (b_u32)parms[2].value;
    size = (b_u32)parms[3].value;

    p_spidev = devman_device_private(devo);

    /* 1. Perform write enable */
    _ret = _spidev_cmd_write(p_spidev, 
                             SPIDEV_CMD_WRITE_EN, 
                             1);
    if ( _ret ) {
        PDEBUG("Error! _spidev_cmd_write: %d\n",
               SPIDEV_CMD_WRITE_EN);
        SPIDEV_ERROR_RETURN(_ret);
    }

    _ret = _spidev_cmd_reg_write(p_spidev, cmd,
                                cmd_sz, (b_u8 *)&value, size);
    if (_ret) {
        PDEBUG("_spidev_cmd_reg_write error\n");
        SPIDEV_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

static ccl_err_t _spidev_cmd_send(devo_t devo, 
                                  device_cmd_parm_t parms[], 
                                  b_u16 parms_num)
{
    spidev_ctx_t  *p_spidev;
    b_u32         i;
    b_u32         cmd = 0;
    b_u32         cmd_sz = 0;

    cmd = (b_u32)parms[0].value;
    cmd_sz = (b_u32)parms[1].value;

    p_spidev = devman_device_private(devo);

    _ret = _spidev_cmd_write(p_spidev, cmd, cmd_sz);
    if ( _ret ) {
        PDEBUG("Error! _spidev_cmd_write: %d\n",
               SPIDEV_CMD_WRITE_EN);
        SPIDEV_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

static ccl_err_t _spidev_cmd_offset_send(devo_t devo, 
                                         device_cmd_parm_t parms[], 
                                         b_u16 parms_num)
{
    spidev_ctx_t  *p_spidev;
    b_u32         i;
    b_u32         cmd = 0;
    b_u32         cmd_sz = 0;
    b_u32         offset = 0;

    cmd = (b_u32)parms[0].value;
    cmd_sz = (b_u32)parms[1].value;
    offset = (b_u32)parms[2].value;

    p_spidev = devman_device_private(devo);

    _ret = _spidev_cmd_data_write(p_spidev, cmd, cmd_sz, 
                                  offset, NULL, 0);
    if ( _ret ) {
        PDEBUG("Error! _spidev_cmd_data_write: %d\n",
               SPIDEV_CMD_WRITE_EN);
        SPIDEV_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

static ccl_err_t _spidev_cmd_data_send(devo_t devo, 
                                       device_cmd_parm_t parms[], 
                                       b_u16 parms_num)
{
    spidev_ctx_t  *p_spidev;
    b_u32         i;
    b_u32         cmd = 0;
    b_u32         cmd_sz = 0;
    b_u32         offset = 0;
    b_u8          *value;
    b_u32         count = 0;

    cmd = (b_u32)parms[0].value;
    cmd_sz = (b_u32)parms[1].value;
    offset = (b_u32)parms[2].value;
    value = (b_u32)parms[3].value;
    count = (b_u32)parms[3].rsize;

    p_spidev = devman_device_private(devo);

    /* 1. Perform write enable */
    _ret = _spidev_cmd_write(p_spidev, 
                             SPIDEV_CMD_WRITE_EN, 
                             1);
    if ( _ret ) {
        PDEBUG("Error! _spidev_cmd_write: %d\n",
               SPIDEV_CMD_WRITE_EN);
        SPIDEV_ERROR_RETURN(_ret);
    }
    _ret = _spidev_cmd_data_write(p_spidev, cmd, cmd_sz, 
                                  offset, value, count);
    if ( _ret ) {
        PDEBUG("Error! _spidev_cmd_data_write: %d\n",
               SPIDEV_CMD_WRITE_EN);
        SPIDEV_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

#define SPI_IMAGE_FILENAME_MAXLEN      50
/* device commands */
static device_cmd_t _cmds[] = 
{
    { 
        .name = "program", .f_cmd = _spidev_cmd_program, .parms_num = 1,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "file_pathname", .type = eDEVMAN_ATTR_STRING, .size = SPI_IMAGE_FILENAME_MAXLEN, .format = "%s" }, 
        }, 
        .help = "Program SPI Flash memory" 
    },
    { 
        .name = "read_mem", .f_cmd = _spidev_cmd_read_mem, .parms_num = 3,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "offset", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" }, 
            { .name = "count", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%d" }, 
            { .name = "file_pathname", .type = eDEVMAN_ATTR_STRING, .size = SPI_IMAGE_FILENAME_MAXLEN, .format = "%s" }, 
        }, 
        .help = "Read SPI Flash memory and save it to file" 
    },
    { 
        .name = "reg_read", .f_cmd = _spidev_cmd_read_reg, .parms_num = 3,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "cmd", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" }, 
            { .name = "cmd_sz", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%d" }, 
            { .name = "size", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%d" }, 
        }, 
        .help = "Read SPI Flash register" 
    },
    { 
        .name = "reg_write", .f_cmd = _spidev_cmd_write_reg, .parms_num = 4,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "cmd", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" }, 
            { .name = "cmd_sz", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%d" }, 
            { .name = "value", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" }, 
            { .name = "size", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%d" }, 
        }, 
        .help = "Write SPI Flash register" 
    },
    { 
        .name = "cmd", .f_cmd = _spidev_cmd_send, .parms_num = 2,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "cmd", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" }, 
            { .name = "cmd_sz", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%d" }, 
        }, 
        .help = "Send command to SPI Flash" 
    },
    { 
        .name = "cmd_offset", .f_cmd = _spidev_cmd_offset_send, .parms_num = 3,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "cmd", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" }, 
            { .name = "cmd_sz", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%d" }, 
            { .name = "offset", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" }, 
        }, 
        .help = "Send command to SPI Flash" 
    },
    { 
        .name = "cmd_data", .f_cmd = _spidev_cmd_data_send, .parms_num = 4,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "cmd", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" }, 
            { .name = "cmd_sz", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%d" }, 
            { .name = "offset", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" }, 
            { .name = "value", .type = eDEVMAN_ATTR_BUFF, .size = 128 }, 
        }, 
        .help = "Send command to SPI Flash" 
    },
};

/* Device instance hw-specific initializations 
 */
static ccl_err_t _fspidev_add(void *devo, void *brdspec)
{
    spidev_ctx_t           *p_spidev;
    gen_spi_dev_brdspec_t  *p_brdspec;

    if ( !devo || !brdspec ) {
        PDEBUG("Error! Invalid input parameters\n");
        SPIDEV_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_spidev = devman_device_private(devo);
    if ( !p_spidev ) {
        PDEBUG("NULL context area\n");
        SPIDEV_ERROR_RETURN(CCL_FAIL);
    }
    p_brdspec = (gen_spi_dev_brdspec_t *)brdspec;

    PDEBUG("123$=> p_brdspec->dev_id=%d, p_brdspec->cs=%d\n", 
           p_brdspec->dev_id, p_brdspec->cs);

    p_spidev->devo = devo;    
    memcpy(&p_spidev->brdspec, p_brdspec, sizeof(gen_spi_dev_brdspec_t));

    return CCL_OK;
}


/* Device instance hw-specific deinitializations 
 */
static ccl_err_t _fspidev_cut(void *devo)
{
    spidev_ctx_t  *p_spidev;
    if ( !devo ) {
        PDEBUG("Error! Invalid input parameters\n");
        SPIDEV_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_spidev = devman_device_private(devo);
    if ( !p_spidev ) {
        PDEBUG("NULL context area\n");
        SPIDEV_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

/* device type description structure */
static device_driver_t _spidev_driver = {
    .name = "spidev",
    .f_add = _fspidev_add,
    .f_cut = _fspidev_cut,
    .attrs_num = sizeof(_attrs)/sizeof(_attrs[0]),                 
    .attrs = _attrs,
    .cmds = _cmds,
    .cmds_num = sizeof(_cmds)/sizeof(_cmds[0]),
    .priv_size = sizeof(spidev_ctx_t)
}; 

/* Initialize SPIDEV device type
 */
ccl_err_t devspidev_init(void)
{
    /* register device type */
    _ret = devman_driver_register(&_spidev_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to register driver\n");
        SPIDEV_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

/* Deinitialize SPIDEV device type
 */
ccl_err_t devspidev_fini(void)
{
    /* unregister device type */
    _ret = devman_driver_unregister(&_spidev_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to unregister driver\n");
        SPIDEV_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}


