/********************************************************************************/
/**
 * @file devman_brd_nvm.h
 * @brief HAL NVM (Non Volatile Memory) interface
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#ifndef _DEVMAN_BRD_NVM_H
#define _DEVMAN_BRD_NVM_H

/** EEPROMs enumerator */
typedef enum {
    DEVMAN_NVM_MAIN_BRD,
    DEVMAN_NVM_ATP_CARD,
    DEVMAN_NVM_APP_CPU_1,
    DEVMAN_NVM_APP_CPU_2,
    DEVMAN_NVM_APP_CPU_3,
    DEVMAN_NVM_APP_CPU_4,
    DEVMAN_NVM_PS_1,
    DEVMAN_NVM_PS_2,
    DEVMAN_NVM_MAX_NUM
} devman_nvm_dev_t;

/** NVM parameters names */
typedef enum {
	DEVMAN_NVM_NAME_BOARD_SN,         /**< Board Serial Number */
	DEVMAN_NVM_NAME_ASSEMBLY_NUM,     /**< Assembly Number */
	DEVMAN_NVM_NAME_HW_REV,           /**< Hardware Revision */
	DEVMAN_NVM_NAME_HW_SUB_REV,       /**< Hardware Subrevision */
	DEVMAN_NVM_NAME_PART_NUM,         /**< Part Number */
	DEVMAN_NVM_NAME_CLEI,             /**< CLEI */
	DEVMAN_NVM_NAME_MFG_DATE,         /**< Manufacturing Date */
    DEVMAN_NVM_NAME_LICENSE,          /**< License value */
    DEVMAN_NVM_NAME_DEV_TYPE_ID,      /**< Device type ID */
	DEVMAN_NVM_NAME_LAST              /**< Should remain last */
} devman_nvm_param_names_t;

/** NVM parameters types */
typedef enum {
	DEVMAN_NVM_TYPE_BYTE,
    DEVMAN_NVM_TYPE_CHAR,
	DEVMAN_NVM_TYPE_I16,
    DEVMAN_NVM_TYPE_U16,
	DEVMAN_NVM_TYPE_I32,
    DEVMAN_NVM_TYPE_U32,
    DEVMAN_NVM_TYPE_I64,
    DEVMAN_NVM_TYPE_U64,
	DEVMAN_NVM_TYPE_STRING,
	DEVMAN_NVM_TYPE_BIN,
	DEVMAN_NVM_TYPE_LAST,/* Should remain last */
} devman_nvm_param_types_t;

/**
 * @brief Get number of NVM parameters 
 * @param[in]  nvm - NVM device index 
 * @param[out]  pnum - number of parameters
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_nvm_num_of_fields_get(devman_nvm_dev_t nvm,  b_u32 *pnum);

/**
 * @brief Get storage type of NVM field
 * @param[in]  nvm - reference NVM module index 
 * @param[in] field - NVM field index 
 * @param[out]  ptype - type of referenced field
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_nvm_field_type_get(devman_nvm_dev_t nvm,  devman_nvm_param_names_t field,  devman_nvm_param_types_t *ptype);

/**
 * @brief Get maximum length of NVM field
 * @param[in]  nvm - NVM device index 
 * @param[in] field - NVM field index 
 * @param[out]  plen - maximum length of NVM field
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_nvm_field_size_get(devman_nvm_dev_t nvm,  devman_nvm_param_names_t field,  b_u32 *plen);

/**
 * @brief Get contents of NVM field
 * @param[in]  nvm - reference NVM module index 
 * @param[in] field - NVM field index 
 * @param[out]  pbuf - buffer for returned value
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_nvm_field_get(devman_nvm_dev_t nvm,  devman_nvm_param_names_t field,  void *pbuf);

/**
 * @brief Set contents of NVM field
 * @param[in]  nvm - reference NVM module index 
 * @param[in] field - NVM field index 
 * @param[in]  pbuf - buffer for written value
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_nvm_field_set(devman_nvm_dev_t nvm,  devman_nvm_param_names_t field,  void *pbuf);


#endif /*_DEVMAN_BRD_NVM_H*/
