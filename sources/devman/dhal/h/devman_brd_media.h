/********************************************************************************/
/**
 * @file devman_brd_media.h
 * @brief Ports media definitions  
 * @copyright 2020 CELARE. All rights reserved.
 */
/********************************************************************************/
#ifndef _DEVMAN_MEDIA_H_
#define _DEVMAN_MEDIA_H_

/************* Definitions of media details description ************/
/* General */
#define SINGLE_DETAIL_BUF_SIZE           30                             /* One detail buffer size */
#define MEDIA_DETAILS_NEW_LINE           "\n\r                       "  /* New media detail line */
/* Detail titles */
#define MEDIA_DETAILS_TITLE_CON_TYPE     "Connector:"                   /* Connectot type title */                              
#define MEDIA_DETAILS_TITLE_VEND_NAME    "Vendor:"                      /* Vendor name title */                                 
#define MEDIA_DETAILS_TITLE_PRT_NUM      "P/N:"                         /* Part number title */                                 
#define MEDIA_DETAILS_TITLE_VEND_REV     "Rev:"                         /* Vendor revision title */                             
#define MEDIA_DETAILS_TITLE_LENG_9K      "SMF     :"                    /* Link length, single mode 9 micron, Km units */       
#define MEDIA_DETAILS_TITLE_LENG_50M     "50uM    :"                    /* Link length, single mode 50 micron, meter units */   
#define MEDIA_DETAILS_TITLE_LENG_EB_50M  "EBW-50uM:"                    /* Extended bandwidth 50 micron, meter units */         
#define MEDIA_DETAILS_TITLE_LENG_625M    "62.5uM  :"                    /* Link length, single mode 62.5 micron, meter units */ 
#define MEDIA_DETAILS_TITLE_LENG_COPPER  "Copper  :"                    /* Link length, copper, meter units */                  

#define FIELD_NOT_SUPPORTED              "Not supported"                /* Field not supported/relevant */
#define MEDIA_CONNECTOR_DETAIL_LIST_END  -1
/****** Link length, single-mode 9 micron, Km units ******/
/* Transceiver does not support single mode fiber or length information must be
   determined from the transceiver technology */
#define LENGTH_NOT_SUPPORTED              0
#define LENGTH_LONG                       0xff
#define LENGTH_KM_RATE                    1000                          /* Transceiver supports link length greater than 254 Km */
#define LENGTH_10M_RATE                   10                            /* Transceiver supports link length greater than 254 Km */
#define LENGTH_KM_LONG_STR                "Greater than 254Km"
#define LENGTH_10M_LONG_STR               "Greater than 2.54Km"
#define LENGTH_KM_UNIT_STR                "Km"
#define LENGTH_M_UNIT_STR                 "M" 

/********** Link length, multi-mode, extended bandwidth, 50 micron ***********/
/* Transceiver does not support extended bandwidth 50 micron multi-mode fiber */
#define LENGTH_EB50M_NOT_SUPPORTED         0

/* SFP transceiver supports link length greater than 508 m */
#define LENGTH_EB50M_LONG                  0xff
/* Ratio between value stored in EEPROM & the represented value */
#define LENGTH_EB50M_VAL_RATIO             2
#define LENGTH_EB50M_LONG_STR              "Greater than 508M"
#define LENGTH_EB50M_UNIT_STR              "M" /* Unit string */

#define VENDOR_NAME_LEN                  17            
#define VENDOR_OUI_LEN                   4             
#define VENDOR_PART_NUM_LEN              17            
#define VENDOR_REV_LEN                   5             
#define VENDOR_SERIAL_NUM_LEN            17            

#define PORT_REV_NAME_SHORT              4             
#define PORT_REV_NAME_LONG               8             

#define MEDIA_ID_SFP                     0x03          
#define MEDIA_ID_SFP_PLUS                MEDIA_ID_SFP  
#define MEDIA_ID_QSFP28                  0x11          

#define SFP_TRANSLATION_PAGES            4
#define QSFP28_TRANSLATION_PAGES         4

typedef enum {
    MEDIA_DDM_DIAG_MAP_SUPPORTED,
    MEDIA_DDM_DIAG_MAP_INTERN_CLBR,
    MEDIA_DDM_DIAG_MAP_EXTERN_CLBR,
    MEDIA_DDM_DIAG_MAP_AVG_PWR_MSRMT,
    MEDIA_DDM_DIAG_MAP_AVG_CHNG_REQ,
    MEDIA_DDM_DIAG_MAP_BER_SUPPORT,
    MEDIA_DDM_DIAG_MAP_LAST
} devman_media_ddm_diag_map_t;

typedef struct {
    b_u8 page_index;       /* EEPROM page index */
    b_u8 page_offset;      /* offset in page */
    b_u8 block_size;       /* block size */
    b_u8 access_width;     /* access to data - bytes*/
} devman_media_data_block_t;

/* Describes type of media slot */
typedef enum {
    MEDIA_PORT_TYPE_UNKNOWN,
    MEDIA_PORT_TYPE_SFP,
    MEDIA_PORT_TYPE_SFP_PLUS,
    MEDIA_PORT_TYPE_QSFP28,
    MEDIA_PORT_TYPE_QSFP,
    MEDIA_PORT_TYPE_XFP,
} devman_sfp_port_type_t;

typedef enum {
    MEDIA_DATA_TYPE_MEDIA_DESCRIPTION,
    MEDIA_DATA_TYPE_DDM_PARAMETERS,
    MEDIA_DATA_TYPE_DDM_VALUES,
    MEDIA_DATA_TYPE_MEDIA_REGISTERS,
    MEDIA_DATA_TYPE_MEDIA_CONTROL
} devman_media_data_type_t;

typedef enum {
    MEDIA_IF_TYPE_NOT_INSTALLED = 1,
    MEDIA_IF_TYPE_UNKNOWN,                           
    MEDIA_IF_TYPE_1000BASE_CX_MM_SFP,
    MEDIA_IF_TYPE_1000BASE_CX_SM_SFP,                
    MEDIA_IF_TYPE_1000BASE_T_SFP_COPPER_MR,          
    MEDIA_IF_TYPE_1000BASE_T_SFP_COPPER_SR,          
    MEDIA_IF_TYPE_1000BASE_T_SFP_COPPER_SR_WO_PHY,   
    MEDIA_IF_TYPE_1000BASE_T_SFP_COPPER_DISABLED,    
    MEDIA_IF_TYPE_1000BASE_BX_SM_SFP,                
    MEDIA_IF_TYPE_1000BASE_BX_MM_SFP,                
    MEDIA_IF_TYPE_1000BASE_PX_SM_SFP,                
    MEDIA_IF_TYPE_1000BASE_PX_MM_SFP,                
    MEDIA_IF_TYPE_1000BASE_DWDM_SM_SFP,              
    MEDIA_IF_TYPE_1000BASE_DWDM_MM_SFP,              
    MEDIA_IF_TYPE_1000BASE_SX_MM_SFP,                
    MEDIA_IF_TYPE_1000BASE_SX_SM_SFP,                
    MEDIA_IF_TYPE_1000BASE_LX_SM_SFP,                
    MEDIA_IF_TYPE_1000BASE_LX_MM_SFP,                
    MEDIA_IF_TYPE_1000BASE_XD_SM_SFP,                    
    MEDIA_IF_TYPE_1000BASE_XD_MM_SFP,                
    MEDIA_IF_TYPE_1000BASE_ZX_SM_SFP,                
    MEDIA_IF_TYPE_1000BASE_ZX_MM_SFP,                
    MEDIA_IF_TYPE_1000BASE_EX_SM_SFP,                
    MEDIA_IF_TYPE_1000BASE_EX_MM_SFP,                
    MEDIA_IF_TYPE_1000BASE_XWDM_SM_SFP,              
    MEDIA_IF_TYPE_1000BASE_XWDM_MM_SFP,              
    MEDIA_IF_TYPE_100BASE_BX_SM_SFP,                 
    MEDIA_IF_TYPE_100BASE_BX_MM_SFP,                 
    MEDIA_IF_TYPE_100BASE_PX_SM_SFP,                 
    MEDIA_IF_TYPE_100BASE_PX_MM_SFP,                 
    MEDIA_IF_TYPE_100BASE_FX_SM_SFP,                 
    MEDIA_IF_TYPE_100BASE_FX_MM_SFP,                 
    MEDIA_IF_TYPE_100BASE_SX_SM_SFP,                 
    MEDIA_IF_TYPE_100BASE_SX_MM_SFP,                 
    MEDIA_IF_TYPE_100BASE_LX_SM_SFP,                 
    MEDIA_IF_TYPE_100BASE_LX_MM_SFP,                 
    MEDIA_IF_TYPE_100BASE_XD_SM_SFP,                     
    MEDIA_IF_TYPE_100BASE_XD_MM_SFP,                 
    MEDIA_IF_TYPE_100BASE_ZX_SM_SFP,                 
    MEDIA_IF_TYPE_100BASE_ZX_MM_SFP,                 
    MEDIA_IF_TYPE_100BASE_EX_SM_SFP,                 
    MEDIA_IF_TYPE_100BASE_EX_MM_SFP,                 
    MEDIA_IF_TYPE_100BASE_XWDM_SM_SFP,               
    MEDIA_IF_TYPE_100BASE_XWDM_MM_SFP,               
    MEDIA_IF_TYPE_10G_BASE_SR_MM_XFP,                
    MEDIA_IF_TYPE_10G_BASE_LR_MM_XFP,                
    MEDIA_IF_TYPE_10G_BASE_LRM_MM_XFP,               
    MEDIA_IF_TYPE_10G_BASE_LR_SM_XFP,                
    MEDIA_IF_TYPE_10G_BASE_SR_SM_XFP,                
    MEDIA_IF_TYPE_10G_BASE_LRM_SM_XFP,               
    MEDIA_IF_TYPE_10G_BASE_SX_MM_XFP,                
    MEDIA_IF_TYPE_10G_BASE_SX_SM_XFP,                
    MEDIA_IF_TYPE_10G_BASE_LX_MM_XFP,                
    MEDIA_IF_TYPE_10G_BASE_LX_SM_XFP,                
    MEDIA_IF_TYPE_10G_BASE_XD_SM_XFP,                    
    MEDIA_IF_TYPE_10G_BASE_XD_MM_XFP,                
    MEDIA_IF_TYPE_10G_BASE_ZX_SM_XFP,                
    MEDIA_IF_TYPE_10G_BASE_ZX_MM_XFP,                
    MEDIA_IF_TYPE_10G_BASE_EX_SM_XFP,                
    MEDIA_IF_TYPE_10G_BASE_EX_MM_XFP,                
    MEDIA_IF_TYPE_10G_BASE_XWDM_SM_XFP,              
    MEDIA_IF_TYPE_10G_BASE_XWDM_MM_XFP,              
    MEDIA_IF_TYPE_10G_BASE_SX_SM_SFP_PLUS,           
    MEDIA_IF_TYPE_10G_BASE_SX_MM_SFP_PLUS,           
    MEDIA_IF_TYPE_40G_BASE_QSFP_SPI,                 
    MEDIA_IF_TYPE_RJ45,                              
    MEDIA_IF_TYPE_10G_BASE_ZX_SM_SFP_PLUS,           
    MEDIA_IF_TYPE_10G_BASE_ZX_MM_SFP_PLUS,           
    MEDIA_IF_TYPE_10G_BASE_XD_SM_SFP_PLUS,               
    MEDIA_IF_TYPE_10G_BASE_XD_MM_SFP_PLUS,           
    MEDIA_IF_TYPE_10G_BASE_LX_SM_SFP_PLUS,               
    MEDIA_IF_TYPE_10G_BASE_LX_MM_SFP_PLUS,           
    MEDIA_IF_TYPE_10G_BASE_DWDM_SM_SFP_PLUS,         
    MEDIA_IF_TYPE_10G_BASE_DWDM_MM_SFP_PLUS,         
    MEDIA_IF_TYPE_MAX                        
} devman_media_link_type_t;   
  
typedef enum {
	MEDIA_XFP_TYPE_CATEGORY_10GBASE_SX,
	MEDIA_XFP_TYPE_CATEGORY_10GBASE_LX,
	MEDIA_XFP_TYPE_CATEGORY_10GBASE_XD,
	MEDIA_XFP_TYPE_CATEGORY_10GBASE_ZX,
	MEDIA_XFP_TYPE_CATEGORY_10GBASE_EX,
	MEDIA_XFP_TYPE_CATEGORY_10GBASE_XWDM,
	MEDIA_XFP_TYPE_CATEGORY_10GBASE_UNKNOWN,
	MEDIA_XFP_TYPE_CATEGORY_10GBASE_MAX
} devman_media_xfp_type_category_t; 
  

typedef struct {
    b_i8                        rev_name_short[PORT_REV_NAME_SHORT];
    b_i8                        rev_name_long[PORT_REV_NAME_LONG];
} devman_media_port_revision_t;

typedef enum {
    MEDIA_TRANSCEIVER_UNKNOWN,
    MEDIA_TRANSCEIVER_GBIC,
    MEDIA_TRANSCEIVER_SOLDERED,
    MEDIA_TRANSCEIVER_SFP,
    MEDIA_TRANSCEIVER_XBI,
    MEDIA_TRANSCEIVER_XENPAK,
    MEDIA_TRANSCEIVER_XFP,
    MEDIA_TRANSCEIVER_XFF,
    MEDIA_TRANSCEIVER_XFP_E,
    MEDIA_TRANSCEIVER_XPAK,
    MEDIA_TRANSCEIVER_X2,
    MEDIA_TRANSCEIVER_DWDM_SFP,
    MEDIA_TRANSCEIVER_QSFP
} devman_media_transceiver_type_t;

typedef enum {
    MEDIA_TRANSCEIVER_EX_UNKNOWN,
    MEDIA_TRANSCEIVER_EX_MOD_DEF_1,
    MEDIA_TRANSCEIVER_EX_MOD_DEF_2,
    MEDIA_TRANSCEIVER_EX_MOD_DEF_3,
    MEDIA_TRANSCEIVER_EX_MOD_TWO_WIRE_ID,
    MEDIA_TRANSCEIVER_EX_MOD_DEF_5,
    MEDIA_TRANSCEIVER_EX_MOD_DEF_6,
    MEDIA_TRANSCEIVER_EX_MOD_DEF_7
} devman_media_extended_type_t;

typedef enum {
    MEDIA_CONNECTOR_UNKNOWN,
    MEDIA_CONNECTOR_SC,
    MEDIA_CONNECTOR_FCS1_COPPER,
    MEDIA_CONNECTOR_FCS2_COPPER,
    MEDIA_CONNECTOR_BNC_TNC,
    MEDIA_CONNECTOR_FC_COAX,
    MEDIA_CONNECTOR_FIBER_JACK,
    MEDIA_CONNECTOR_LC,
    MEDIA_CONNECTOR_MT_RJ,
    MEDIA_CONNECTOR_MU,
    MEDIA_CONNECTOR_SG,
    MEDIA_CONNECTOR_OPTICAL_PIGTAIL,
    MEDIA_CONNECTOR_MPO_PARALLEL_OPTIC,
    MEDIA_CONNECTOR_HSSDC,
    MEDIA_CONNECTOR_COPPER_PIGTAIL,
    MEDIA_CONNECTOR_RJ45,
    MEDIA_CONNECTOR_LAST
} devman_media_connector_type_t;

typedef enum {
    MEDIA_TRNSVR_10G_UNKNOWN,
    MEDIA_TRNSVR_10G_SR,
    MEDIA_TRNSVR_10G_LR,
    MEDIA_TRNSVR_10G_LRM
} devman_media_trnsvr_10g_compliance_t;

typedef enum {
    MEDIA_TRNSVR_INFINIBAND_UNKNOWN,
    MEDIA_TRNSVR_INFINIBAND_1X_COPPER_PASSIVE,
    MEDIA_TRNSVR_INFINIBAND_1X_COPPER_ACTIVE,
    MEDIA_TRNSVR_INFINIBAND_1X_LX,
    MEDIA_TRNSVR_INFINIBAND_1X_SX
} devman_media_trnsvr_infiniband_compliance_t;

typedef enum {
    MEDIA_TRNSVR_ESCON_UNKNOWN,
    MEDIA_TRNSVR_ESCON_SMF,
    MEDIA_TRNSVR_ESCON_MMF
} devman_media_trnsvr_escon_compliance_t;

typedef enum {
    MEDIA_TRNSVR_SONET_UNKNOWN,
    MEDIA_TRNSVR_SONET_OC3_SHORT,
    MEDIA_TRNSVR_SONET_OC3_SM_INTER,
    MEDIA_TRNSVR_SONET_OC3_SM_LONG,
    MEDIA_TRNSVR_SONET_OC12_SHORT,
    MEDIA_TRNSVR_SONET_OC12_SM_INTER,
    MEDIA_TRNSVR_SONET_OC12_SM_LONG,
    MEDIA_TRNSVR_SONET_OC48_SHORT,
    MEDIA_TRNSVR_SONET_OC48_SM_INTER,
    MEDIA_TRNSVR_SONET_OC48_SM_LONG,
    MEDIA_TRNSVR_SONET_SPEC_BIT1,
    MEDIA_TRNSVR_SONET_SPEC_BIT2,
    MEDIA_TRNSVR_SONET_192_SHORT,
    MEDIA_TRNSVR_SONET_OC3_INTER_SR1_IR1_LR1
} devman_media_trnsvr_sonet_compliance_t;

typedef enum {
    MEDIA_TRNSVR_ETH_UNKNOWN,
    MEDIA_TRNSVR_ETH_1000SX,
    MEDIA_TRNSVR_ETH_1000LX,
    MEDIA_TRNSVR_ETH_1000CX,
    MEDIA_TRNSVR_ETH_1000T,
    MEDIA_TRNSVR_ETH_100LX,
    MEDIA_TRNSVR_ETH_100FX,
    MEDIA_TRNSVR_ETH_BX10,
    MEDIA_TRNSVR_ETH_PX
} devman_media_trnsvr_ethernet_compliance_t;

typedef enum {
    MEDIA_TRNSVR_FCL_UNKNOWN,
    MEDIA_TRNSVR_FCL_MEDIUM,
    MEDIA_TRNSVR_FCL_LONG,
    MEDIA_TRNSVR_FCL_INTERMEDIATE,
    MEDIA_TRNSVR_FCL_SHORT,
    MEDIA_TRNSVR_FCL_VERY_LONG
} devman_media_trnsvr_fibre_chnl_length_t;

typedef enum {
    MEDIA_TRNSVR_FCT_UNKNOWN,
    MEDIA_TRNSVR_FCT_COPPER_FC_BASE_T,
    MEDIA_TRNSVR_FCT_COPPER_PASSIVE,
    MEDIA_TRNSVR_FCT_COPPER_ACTIVE,
    MEDIA_TRNSVR_FCT_LONGWAVE_LL,
    MEDIA_TRNSVR_FCT_SHORTWAVE_W_OFC_SL,
    MEDIA_TRNSVR_FCT_SHORTWAVE_WO_OFC_SN,
    MEDIA_TRNSVR_FCT_ELECTRIC_INTRA_ENCL_EL,
    MEDIA_TRNSVR_FCT_ELECTRIC_INTER_ENCL_EL,
    MEDIA_TRNSVR_FCT_LONGWAVE_LC,
    MEDIA_TRNSVR_FCT_SHORTWAVE_LINEAR_RX_SA
} devman_media_trnsvr_fibre_chnl_technology_t;

typedef enum {
    MEDIA_TRNSVR_FCM_UNKNOWN,
    MEDIA_TRNSVR_FCM_SINGLE_SM,
    MEDIA_TRNSVR_FCM_MULTI_50UM_M5,
    MEDIA_TRNSVR_FCM_MULTI_62_5UM_M6,
    MEDIA_TRNSVR_FCM_VIDEO_COAX_TV,
    MEDIA_TRNSVR_FCM_MINI_COAX_MI,
    MEDIA_TRNSVR_FCM_TWIST_PAIR_TP,
    MEDIA_TRNSVR_FCM_TWIN_AX_PAIR_TW
} devman_media_trnsvr_fibre_chnl_media_t;

typedef enum {
    MEDIA_TRNSVR_FCS_UNKNOWN,
    MEDIA_TRNSVR_FCS_100,
    MEDIA_TRNSVR_FCS_200,
    MEDIA_TRNSVR_FCS_400,
    MEDIA_TRNSVR_FCS_800,
    MEDIA_TRNSVR_FCS_1200,
    MEDIA_TRNSVR_FCS_1600
} devman_media_trnsvr_fibre_chnl_speed_t;

typedef struct {
    devman_media_trnsvr_10g_compliance_t        g10_compliance;         /* 3-3 B4-7 10GBASE-SR, 10GBASE-LR, 10GBASE-LRM*/
    devman_media_trnsvr_infiniband_compliance_t infiniband_compliance;  /* 3-3 B0-B3 */
    devman_media_trnsvr_escon_compliance_t      escon_compliance;       /* 4-4 B6-B7 */
    devman_media_trnsvr_sonet_compliance_t      sonet_compliance;       /* 4(B0-B5)-5 */
    devman_media_trnsvr_ethernet_compliance_t   eth_compliance;         /* 6-6 - 1000BASE-T, 1000BASE-CX, etc. */
    devman_media_trnsvr_fibre_chnl_length_t     fbr_chnl_len;           /* 7(B3-B7) */
    devman_media_trnsvr_fibre_chnl_technology_t fbr_chnl_technology;    /* 7(B0-B2)-8 */
    devman_media_trnsvr_fibre_chnl_media_t      fbr_chnl_media  ;       /* 9 */
    devman_media_trnsvr_fibre_chnl_speed_t      fbr_chnl_speed;         /* 10-10 */
} devman_media_transceiver_t;

typedef enum {
    MEDIA_ENCODING_UNKNOWN,
    MEDIA_ENCODING_8B_10B,
    MEDIA_ENCODING_4B_5B,
    MEDIA_ENCODING_NRZ,
    MEDIA_ENCODING_MANCHESTER,
    MEDIA_ENCODING_SONET_SCRAMBLED,
    MEDIA_ENCODING_64B_66B
} devman_media_encoding_t;

typedef enum {
    MEDIA_RATE_ID_UNKNOWN,
    MEDIA_RATE_ID_4_2_1G__AS0_AS1,
    MEDIA_RATE_ID_8_4_2G_RX,
    MEDIA_RATE_ID_8_4_2G_RX_TX,
    MEDIA_RATE_ID_8_4_2G_TX,
} devman_media_rate_id_t;

typedef struct {
    b_i32     year;
    b_i32     month;
    b_i32     day;
    b_i32     lot;
} devman_media_mfg_date_t;

typedef struct {
    b_bool    relevance;
    b_bool    status;
} devman_media_diag_param_type_t;

typedef struct {
    devman_media_diag_param_type_t      ddm_supported;        /* B6 on 92*/
    devman_media_diag_param_type_t      intern_calibr;        /* B5 on 92*/
    devman_media_diag_param_type_t      extern_calibr;        /* B4 on 92*/
    devman_media_diag_param_type_t      avg_power_measure;    /* B3 on 92*/
    devman_media_diag_param_type_t      adr_chng_required;    /* B2 on 92*/
    devman_media_diag_param_type_t      ber_support;          /* B4 on 92*/
}devman_media_diag_type_t;

/* vendor data describing media parameters */
typedef struct {
    devman_media_transceiver_type_t  transceiver_type;                 /* 0-0 - Unknown, GIBIC, etc.*/                     
    devman_media_extended_type_t     extended_type;                    /* 1-1 - MOD_DEF N*/                                
    devman_media_connector_type_t    connector_type;                   /* 2-2 - FiberJack, LC, MT-RJ, etc */              
    devman_media_transceiver_t       transceiver;                      /* 3-10 - Elecric or optical compability */       
    devman_media_encoding_t          encoding;                         /* 11-11 */                                        
    devman_media_rate_id_t           rate_id;                          /* 13-13 */                                        
    b_i32                            bit_rate_nom;                     /* 12-12 - Nominal signalling rate */                                 
    b_i32                            bit_rate_max;                     /* 12 & 66-66 - Maximal signalling rate */                            
    b_i32                            bit_rate_min;                     /* 12 & 67-67 - Minimal signalling rate */                            
    b_i32                            length_smf;                       /* 14-15 Length for single mode fiber */                              
    b_i32                            length50um;                       /* 16-16 Length for 50 */                                             
    b_i32                            length62_5um;                     /* 17-17 Length for 62.5 um */                                        
    b_i32                            length_copper;                    /* 18-18 Length for copper */                                         
    b_i32                            length_om3;                       /* 19-19 Length for 50 um OM3 fiber */                                
    b_i8                             vendor_name[VENDOR_NAME_LEN];     /* 20-35 SFP vendor name (ASCII) */                                   
    b_i8                             vendor_oui[VENDOR_OUI_LEN];       /* 37-39 SFP vendor IEEE company ID */                                
    b_i8                             vendor_pn[VENDOR_PART_NUM_LEN];   /* 40-55 Part number provided by SFP vendor (ASCII) */                
    b_i8                             vendor_rev[VENDOR_REV_LEN];       /* 56-59 Revision level for part number provided by vendor (ASCII) */ 
    b_i8                             vendor_sn[VENDOR_SERIAL_NUM_LEN]; /* 68-83 Serial number provided by vendor (ASCII) */                  
    devman_media_mfg_date_t          mfg_date;                         /* 84-91 Vendor's manufacturing date */                               
    devman_media_diag_type_t         diagnostic;                       /* 92-92 Supporting diagnostic parameters */                          
} devman_media_description_t;

/* Main struct of media parameters */
typedef struct {
    devman_sfp_port_type_t            media_type;     /* SFP/XFP */
    devman_media_link_type_t          link_type;      /* specific media in slot */
    devman_media_port_revision_t      revision;       /* transmission type ID */
    devman_media_description_t        description;    /* vendor data describing media parameters */
} devman_media_param_t;


/* Parameters of treshold */
typedef struct {
    union {
        b_u8    msb;
        b_i8    modulo;
    };
    union {
        b_u8    lsb;
        b_u8    fraction;
    };
} devman_media_diag_tresh_value_t;

typedef struct {
    b_bool                             applicable;
    devman_media_diag_tresh_value_t    high_alarm;
    devman_media_diag_tresh_value_t    low_alarm;
    devman_media_diag_tresh_value_t    high_warn;
    devman_media_diag_tresh_value_t    low_warn;
} devman_media_diag_tresh_unit_t;


/* vendor-defined tresholds */
typedef struct {
    devman_media_diag_tresh_unit_t     temperature; /* Temperature treshold */
    devman_media_diag_tresh_unit_t     voltage;     /* Voltage treshold */
    devman_media_diag_tresh_unit_t     bias;        /* Bias treshold */
    devman_media_diag_tresh_unit_t     tx_power;    /* Tx power treshold */
    devman_media_diag_tresh_unit_t     rx_power;    /* Rx power treshold */
} devman_media_diag_treshold_t;


typedef struct {
    devman_media_diag_tresh_value_t     slope;
    devman_media_diag_tresh_value_t     offset;
} devman_media_diag_calib_item_t;


typedef struct {
    b_bool                              applicable;
    b_float32                           rx_power[5];
    devman_media_diag_calib_item_t      txi;
    devman_media_diag_calib_item_t      tx_power;
    devman_media_diag_calib_item_t      t;
    devman_media_diag_calib_item_t      v;
} devman_media_diag_calibration_t;

/* Main structure of media digital diagnostic */
typedef struct {
	devman_media_diag_treshold_t	    threshold; 		/* vendor-defined tresholds */
    devman_media_diag_calibration_t     calibration;       /* DDM calibration parameters */
} devman_media_diag_param_t;

/* Main structure of media digital measured status */
typedef struct
{
    devman_media_diag_tresh_value_t     temperature;
    devman_media_diag_tresh_value_t     vcc;
    devman_media_diag_tresh_value_t     tx_bias;
    devman_media_diag_tresh_value_t     tx_power;
    devman_media_diag_tresh_value_t     rx_power;
    devman_media_diag_tresh_value_t     aux1;
    devman_media_diag_tresh_value_t     aux2;
    b_bool                              tx_disable;
    b_bool                              soft_tx_disable;
    b_bool                              rate_select;
    b_bool                              soft_rate_select;
    b_bool                              tx_fault;
    b_bool                              los;
    b_bool                              data_ready_bar;
} devman_media_diag_status_t;


typedef enum {
    MEDIA_DDM_THRESHOLD_MAP_TEMPERATURE,
    MEDIA_DDM_THRESHOLD_MAP_VOLTAGE,
    MEDIA_DDM_THRESHOLD_MAP_BIAS,
    MEDIA_DDM_THRESHOLD_MAP_TX_PWR,
    MEDIA_DDM_THRESHOLD_MAP_RX_PWR,
    MEDIA_DDM_THRESHOLD_MAP_AUX1,
    MEDIA_DDM_THRESHOLD_MAP_AUX2,
    MEDIA_DDM_THRESHOLD_MAP_CALIBRATION,
    MEDIA_DDM_THRESHOLD_MAP_LAST  //Must be last
} ddm_threshold_map_t;

typedef enum {
    MEDIA_DDM_STATUS_MAP_TEMPERATURE,
    MEDIA_DDM_STATUS_MAP_VOLTAGE,
    MEDIA_DDM_STATUS_MAP_BIAS,
    MEDIA_DDM_STATUS_MAP_TX_PWR,
    MEDIA_DDM_STATUS_MAP_RX_PWR,
    MEDIA_DDM_STATUS_MAP_AUX1,
    MEDIA_DDM_STATUS_MAP_AUX2,
    MEDIA_DDM_STATUS_MAP_STAT_CTL1,
    MEDIA_DDM_STATUS_MAP_STAT_CTL2,
    MEDIA_DDM_STATUS_MAP_LAST  //Must be last
} ddm_status_map_t;

typedef struct  {
  b_u32  type;
  char   *name;
} devman_media_mapping_entry_str_t;

typedef struct  {
  b_u32 from;
  b_u32 to;
} devman_media_mapping_entry_int_t;

extern const devman_media_mapping_entry_str_t media_trnsvr_10g_compliance_type_str[];
extern const devman_media_mapping_entry_str_t media_trnsvr_infiniband_compliance_type_str[];
extern const devman_media_mapping_entry_str_t media_trnsvr_escon_compliance_type_str[];
extern const devman_media_mapping_entry_str_t media_trnsvr_sonet_compliance_type_str[];
extern const devman_media_mapping_entry_str_t media_trnsvr_eth_compliance_type_str[];
extern const devman_media_mapping_entry_str_t media_trnsvr_fbr_chnl_length_type_str[];
extern const devman_media_mapping_entry_str_t media_trnsvr_fbr_chnl_technology_type_str[];
extern const devman_media_mapping_entry_str_t media_trnsvr_fbr_chnl_media_type_str[];
extern const devman_media_mapping_entry_str_t media_trnsvr_fbr_chnl_speed_type_str[];
extern const devman_media_mapping_entry_str_t media_type_str[];
extern const devman_media_mapping_entry_int_t media_link_type_str[];
extern const devman_media_mapping_entry_str_t media_port_type_str[];




#endif /* _DEVMAN_MEDIA_H_ */
