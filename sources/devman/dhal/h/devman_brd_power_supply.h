/********************************************************************************/
/**
 * @file devman_brd_power_supply.h
 * @brief HAL power supply interface
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#ifndef _DEVMAN_BRD_POWER_SUPPLY_H
#define _DEVMAN_BRD_POWER_SUPPLY_H

#include <ccl_types.h>

#define DEVMAN_PS_FANS_NUM 2

/** power supply types enumeration */
typedef enum {
    DEVMAN_PS_TYPE_UNKNOWN,
    DEVMAN_PS_TYPE_AC,
    DEVMAN_PS_TYPE_DC
} devman_ps_type_t;

/** available modules inside power supply enumeration */
typedef enum {
    DEVMAN_PS_STATUS_AVAIL_POWER = 1,
    DEVMAN_PS_STATUS_AVAIL_FAN
} devman_ps_avail_module_t;

/** power supply status enumeration */
typedef enum {
    DEVMAN_PS_STATUS_OK,
    DEVMAN_PS_STATUS_FAIL,
    DEVMAN_PS_STATUS_ABSENT
} devman_ps_status_e;

/** power supply output voltage status enumeration */
typedef enum {
    DEVMAN_PS_VOUT_OVLT_FAULT = 1,   /**< Vout over voltage fault */
    DEVMAN_PS_VOUT_OVLT_WARN,        /**< Vout over voltage warning */
    DEVMAN_PS_VOUT_UVLT_FAULT,       /**< Vout under voltage fault */
    DEVMAN_PS_VOUT_UVLT_WARN         /**< Vout under voltage warning */
} devman_ps_vout_status_t;

/** power supply output current status enumeration */
typedef enum {
    DEVMAN_PS_IOUT_OCRNT_FAULT = 1,  /**< Iout over current fault */
    DEVMAN_PS_IOUT_OCRNT_WARN        /**< Iout over current warning */
} devman_ps_iout_status_t;

/** power supply output power status enumeration */
typedef enum {
    DEVMAN_PS_POUT_OPWR_FAULT = 1,   /**< Pout over power fault */
    DEVMAN_PS_POUT_OPWR_WARN         /**< Pout over power warning */
} devman_ps_pout_status_t;

/** power supply input power status enumeration */
typedef enum {
    DEVMAN_PS_PIN_OPWR_WARN = 1,    /**< Pin over power warning */
} devman_ps_pin_status_t;

/** power supply temperature status enumeration */
typedef enum {
    DEVMAN_PS_TEMP_OVER_FAULT = 1,   /**< Over temperature fault */
    DEVMAN_PS_TEMP_OVER_WARN,        /**< Over temperature warning */
    DEVMAN_PS_TEMP_UNDER_FAULT,      /**< Under temperature fault */
    DEVMAN_PS_TEMP_UNDER_WARN        /**< Under temperature warning */
} devman_ps_temp_status_t;

/** power supply fans status enumeration */
typedef enum {
    DEVMAN_PS_FAN_FAULT = 1,         /**< Fan fault */
    DEVMAN_PS_FAN_WARN               /**< Fan warning */
} devman_ps_fan_status_t;

/** power supply status structure */
typedef struct {
    devman_ps_type_t           type;                        /**< Type of power supply (AC/DC) */ 
    devman_ps_status_e         ps_status;                   /**< Power Supply overall status (Logical OR of statuses of its entities) */
    devman_ps_vout_status_t    vout_status;                 /**< Indicate status of output voltage of power supply */
    devman_ps_iout_status_t    iout_status;                 /**< Indicate status of output current of power supply */
    devman_ps_pout_status_t    pout_status;                 /**< Indicate status of output power of power supply */
    devman_ps_pin_status_t     pin_status;                  /**< Indicate status of input power of power supply */
    devman_ps_temp_status_t    temp_status;                 /**< Indicate status of temperature of power supply */
    devman_ps_fan_status_t     fan_status[DEVMAN_PS_FANS_NUM]; /**< Indicate status of fans of power supply */
} devman_ps_status_t;

/**
 * @brief Get number of PS units
 * @param[out] pnum - number of PS units
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_ps_number_of_psu_get(int *pnum);

/**
 * @brief Get PS's data: type, list of available state 
 *        parameters and main operative status of PS
 * @param[in] psu - PS type 
 * @param[out] pstatus - PS status 
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_ps_status_get(devman_ps_type_t psu,  devman_ps_status_t *pstatus);


/**
 * @brief Get actual measured output voltage in volts
 * @param[in] psu - PS type 
 * @param[out] vout - measured output voltage 
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_ps_vout_get(devman_ps_type_t psu, b_i32 *vout);

/**
 * @brief Get actual measured output current in amperes
 * @param[in] psu - PS type 
 * @param[out] iout - measured output current 
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_ps_iout_get(devman_ps_type_t psu, b_i32 *iout);

/**
 * @brief Get output power in watts
 * @param[in] psu - PS type 
 * @param[out] pout - output power 
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_ps_pout_get(devman_ps_type_t psu, b_i32 *pout);

/**
 * @brief Get input power in watts
 * @param[in] psu - PS type 
 * @param[out] pin - input power 
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_ps_pin_get(devman_ps_type_t psu, b_i32 *pin);

/**
 * @brief Get temperature in degree Celsius
 * @param[in] psu - PS type 
 * @param[out] temp - temperature 
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_ps_temperature_get(devman_ps_type_t psu, b_i32 *temp);

/**
 * @brief Get speed of the fan
 * @param[in] psu - PS type 
 * @param[in] fan - fan number 
 * @param[out] speed - fan speed 
 * @return CCL_OK on success, error code on failure 
 * @note 0 <= fan < DEVMAN_PS_FANS_NUM  
 */
ccl_err_t devman_brd_ps_fan_speed_get(devman_ps_type_t psu, b_i32 fan, b_i32 *speed);

#endif /*_DEVMAN_BRD_POWER_SUPPLY_H*/
