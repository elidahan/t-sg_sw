/********************************************************************************/
/**
 * @file devman_brd_mailbox.h
 * @brief HAL clear switch interface
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#ifndef _DEVMAN_BRD_MAILBOX_H
#define _DEVMAN_BRD_MAILBOX_H

#include <ccl_types.h>


/** mailbox users enumeration */
typedef enum {
    DEVMAN_MAILBOX_ATP_CARD = 1,
    DEVMAN_MAILBOX_APP_CPU_1,
    DEVMAN_MAILBOX_APP_CPU_2,
    DEVMAN_MAILBOX_APP_CPU_3,
    DEVMAN_MAILBOX_APP_CPU_4,
} devman_mailbox_user_t;


/**
 * @brief Set mailbox value for specific mailbox user
 * @param[in] muser - mailbox user
 * @param[in] val - mailbox value 
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_mailbox_value_set(devman_mailbox_user_t muser, b_u32 val);

/**
 * @brief Get mailbox value from specific mailbox user
 * @param[in] muser - mailbox user
 * @param[out] pval - mailbox value 
 * @return CCL_OK on success, error code on failure 
 * @note Mailbox location must be cleared on read
 */
ccl_err_t devman_brd_mailbox_value_get(devman_mailbox_user_t muser, b_u32 *pval);

/**
 * @brief Broadcast mailbox value for all mailbox users
 * @param[in] val - mailbox value 
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_mailbox_value_bcast(b_u32 val);


#endif /*_DEVMAN_BRD_MAILBOX_H*/
