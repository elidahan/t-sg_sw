/********************************************************************************/
/**
 * @file devman_brd_power_seq.h
 * @brief HAL power sequencer interface
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#ifndef _DEVMAN_BRD_POWER_SEQ_H
#define _DEVMAN_BRD_POWER_SEQ_H

#include <ccl_types.h>

#define DEVMAN_PSEQ_VOLTAGES 8

/** structure representing power sequencer monitored voltages */
typedef struct {
    b_u8 vmon[DEVMAN_PSEQ_VOLTAGES]
} devman_pseq_voltages_t;


/**
 * @brief Get monitored voltages of power sequencer
 * @param[out] pv - monitored voltages
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_pseq_voltages_read(devman_pseq_voltages_t *pv);

#endif /*_DEVMAN_BRD_POWER_SEQ_H*/
