/********************************************************************************/
/**
 * @file devman_brd_trans.h
 * @brief HAL Transceivers interface
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#ifndef _DEVMAN_BRD_TRANS_H
#define _DEVMAN_BRD_TRANS_H

typedef enum { 
    DEVMAN_TRANS_TYPE_NOT_INSTALLED             = 1,
    DEVMAN_TRANS_TYPE_UNKNOWN                   = 2,
    DEVMAN_TRANS_TYPE_1000BASE_CX_MM_SFP        = 3,
    DEVMAN_TRANS_TYPE_1000BASE_CX_SM_SFP        = 4,
    DEVMAN_TRANS_TYPE_1000BASE_T_SFP_COPPER_MR  = 5,
    DEVMAN_TRANS_TYPE_1000BASE_T_SFP_COPPER_SR  = 6,
    DEVMAN_TRANS_TYPE_1000BASE_T_SFP_COPPER_SR_WO_PHY  = 7,
    DEVMAN_TRANS_TYPE_1000BASE_T_SFP_COPPER_DISABLED    = 8,
    DEVMAN_TRANS_TYPE_1000BASE_BX_SM_SFP                = 9,
    DEVMAN_TRANS_TYPE_1000BASE_BX_MM_SFP                = 10,
    DEVMAN_TRANS_TYPE_1000BASE_PX_SM_SFP                = 11,
    DEVMAN_TRANS_TYPE_1000BASE_PX_MM_SFP                = 12,
    DEVMAN_TRANS_TYPE_1000BASE_DWDM_SM_SFP              = 13,
    DEVMAN_TRANS_TYPE_1000BASE_DWDM_MM_SFP              = 14,
    DEVMAN_TRANS_TYPE_1000BASE_SX_MM_SFP                = 15,
    DEVMAN_TRANS_TYPE_1000BASE_SX_SM_SFP                = 16,
    DEVMAN_TRANS_TYPE_1000BASE_LX_SM_SFP                = 17,
    DEVMAN_TRANS_TYPE_1000BASE_LX_MM_SFP                = 18,
    DEVMAN_TRANS_TYPE_1000BASE_XD_SM_SFP                = 19,     
    DEVMAN_TRANS_TYPE_1000BASE_XD_MM_SFP                = 20,
    DEVMAN_TRANS_TYPE_1000BASE_ZX_SM_SFP                = 21,
    DEVMAN_TRANS_TYPE_1000BASE_ZX_MM_SFP                = 22,
    DEVMAN_TRANS_TYPE_1000BASE_EX_SM_SFP                = 23,
    DEVMAN_TRANS_TYPE_1000BASE_EX_MM_SFP                = 24,
    DEVMAN_TRANS_TYPE_1000BASE_XWDM_SM_SFP              = 25,
    DEVMAN_TRANS_TYPE_1000BASE_XWDM_MM_SFP              = 26,
    DEVMAN_TRANS_TYPE_100BASE_BX_SM_SFP                 = 27,
    DEVMAN_TRANS_TYPE_100BASE_BX_MM_SFP                 = 28,
    DEVMAN_TRANS_TYPE_100BASE_PX_SM_SFP                 = 29,
    DEVMAN_TRANS_TYPE_100BASE_PX_MM_SFP                 = 30,
    DEVMAN_TRANS_TYPE_100BASE_FX_SM_SFP                 = 31,
    DEVMAN_TRANS_TYPE_100BASE_FX_MM_SFP                 = 32,
    DEVMAN_TRANS_TYPE_100BASE_SX_SM_SFP                 = 33,
    DEVMAN_TRANS_TYPE_100BASE_SX_MM_SFP                 = 34,
    DEVMAN_TRANS_TYPE_100BASE_LX_SM_SFP                 = 35,
    DEVMAN_TRANS_TYPE_100BASE_LX_MM_SFP                 = 36,
    DEVMAN_TRANS_TYPE_100BASE_XD_SM_SFP                 = 37,     
    DEVMAN_TRANS_TYPE_100BASE_XD_MM_SFP                 = 38,
    DEVMAN_TRANS_TYPE_100BASE_ZX_SM_SFP                 = 39,
    DEVMAN_TRANS_TYPE_100BASE_ZX_MM_SFP                 = 40,
    DEVMAN_TRANS_TYPE_100BASE_EX_SM_SFP                 = 41,
    DEVMAN_TRANS_TYPE_100BASE_EX_MM_SFP                 = 42,
    DEVMAN_TRANS_TYPE_100BASE_XWDM_SM_SFP               = 43,
    DEVMAN_TRANS_TYPE_100BASE_XWDM_MM_SFP               = 44,
    DEVMAN_TRANS_TYPE_10G_BASE_SR_MM_XFP                = 45,
    DEVMAN_TRANS_TYPE_10G_BASE_LR_MM_XFP                = 46,
    DEVMAN_TRANS_TYPE_10G_BASE_LRM_MM_XFP               = 47,
    DEVMAN_TRANS_TYPE_10G_BASE_LR_SM_XFP                = 48,
    DEVMAN_TRANS_TYPE_10G_BASE_SR_SM_XFP                = 49,
    DEVMAN_TRANS_TYPE_10G_BASE_LRM_SM_XFP               = 50,
    DEVMAN_TRANS_TYPE_10G_BASE_SX_MM_XFP                = 51,
    DEVMAN_TRANS_TYPE_10G_BASE_SX_SM_XFP                = 52,
    DEVMAN_TRANS_TYPE_10G_BASE_LX_MM_XFP                = 53,
    DEVMAN_TRANS_TYPE_10G_BASE_LX_SM_XFP                = 54,
    DEVMAN_TRANS_TYPE_10G_BASE_XD_SM_XFP                = 55,     
    DEVMAN_TRANS_TYPE_10G_BASE_XD_MM_XFP                = 56,
    DEVMAN_TRANS_TYPE_10G_BASE_ZX_SM_XFP                = 57,
    DEVMAN_TRANS_TYPE_10G_BASE_ZX_MM_XFP                = 58,
    DEVMAN_TRANS_TYPE_10G_BASE_EX_SM_XFP                = 59,
    DEVMAN_TRANS_TYPE_10G_BASE_EX_MM_XFP                = 60,
    DEVMAN_TRANS_TYPE_10G_BASE_XWDM_SM_XFP              = 61,
    DEVMAN_TRANS_TYPE_10G_BASE_XWDM_MM_XFP              = 62,
    DEVMAN_TRANS_TYPE_10G_BASE_SX_SM_SFP_PLUS           = 63,
    DEVMAN_TRANS_TYPE_10G_BASE_SX_MM_SFP_PLUS           = 64,
    DEVMAN_TRANS_TYPE_40G_BASE_QSFP_SPI                 = 65,
    DEVMAN_TRANS_TYPE_RJ45                              = 66,
    DEVMAN_TRANS_TYPE_10G_BASE_ZX_SM_SFP_PLUS           = 67,
    DEVMAN_TRANS_TYPE_10G_BASE_ZX_MM_SFP_PLUS           = 68,
    DEVMAN_TRANS_TYPE_10G_BASE_XD_SM_SFP_PLUS           = 69,     
    DEVMAN_TRANS_TYPE_10G_BASE_XD_MM_SFP_PLUS           = 70,
    DEVMAN_TRANS_TYPE_10G_BASE_LX_SM_SFP_PLUS           = 71,     
    DEVMAN_TRANS_TYPE_10G_BASE_LX_MM_SFP_PLUS           = 72,
    DEVMAN_TRANS_TYPE_10G_BASE_DWDM_SM_SFP_PLUS         = 73,
    DEVMAN_TRANS_TYPE_10G_BASE_DWDM_MM_SFP_PLUS         = 74,
	DEVMAN_TRANS_TYPE_MAX                        
} devman_trans_link_type_t;

/** transceiver revision structure */
typedef struct {
	b_u32	                rev_id;
	b_i8					rev_name_short[PORT_REV_NAME_SHORT];
	b_i8					rev_name_long[PORT_REV_NAME_LONG];
} devman_trans_revision_t;

/** transceiver types enumeration   */
typedef enum {
    DEVMAN_TRANS_UNKNOWN,
    DEVMAN_TRANS_GBIC,
    DEVMAN_TRANS_SOLDERED,
    DEVMAN_TRANS_SFP,
    DEVMAN_TRANS_XBI,
    DEVMAN_TRANS_XENPAK,
    DEVMAN_TRANS_XFP,
    DEVMAN_TRANS_XFF,
    DEVMAN_TRANS_XFP_E,
    DEVMAN_TRANS_XPAK,
    DEVMAN_TRANS_X2,
    DEVMAN_TRANS_SFP_PLUS,
    DEVMAN_TRANS_DWDM_SFP,
    DEVMAN_TRANS_QSFP
} devman_trans_transeiver_type_t;

typedef enum {
    DEVMAN_TRANS_EX_UNKNOWN,
    DEVMAN_TRANS_EX_MOD_DEF_1,
    DEVMAN_TRANS_EX_MOD_DEF_2,
    DEVMAN_TRANS_EX_MOD_DEF_3,
    DEVMAN_TRANS_EX_MOD_TWO_WIRE_ID,
    DEVMAN_TRANS_EX_MOD_DEF_5,
    DEVMAN_TRANS_EX_MOD_DEF_6,
    DEVMAN_TRANS_EX_MOD_DEF_7
} devman_trans_extended_type_t;

typedef enum {
    DEVMAN_TRANS_CONNECTOR_UNKNOWN,
    DEVMAN_TRANS_CONNECTOR_SC,
    DEVMAN_TRANS_CONNECTOR_FCS1_COPPER,
    DEVMAN_TRANS_CONNECTOR_FCS2_COPPER,
    DEVMAN_TRANS_CONNECTOR_BNC_TNC,
    DEVMAN_TRANS_CONNECTOR_FC_COAX,
    DEVMAN_TRANS_CONNECTOR_FIBER_JACK,
    DEVMAN_TRANS_CONNECTOR_LC,
    DEVMAN_TRANS_CONNECTOR_MT_RJ,
    DEVMAN_TRANS_CONNECTOR_MU,
    DEVMAN_TRANS_CONNECTOR_SG,
    DEVMAN_TRANS_CONNECTOR_OPTICAL_PIGTAIL,
    DEVMAN_TRANS_CONNECTOR_MPO_PARALLEL_OPTIC,
    DEVMAN_TRANS_CONNECTOR_HSSDC,
    DEVMAN_TRANS_CONNECTOR_COPPER_PIGTAIL,
    DEVMAN_TRANS_CONNECTOR_RJ45,
    DEVMAN_TRANS_CONNECTOR_LAST
} devman_trans_connector_type_t;

typedef enum {
    DEVMAN_TRANS_10G_UNKNOWN,
    DEVMAN_TRANS_10G_SR,
    DEVMAN_TRANS_10G_LR,
    DEVMAN_TRANS_10G_LRM
} devman_trans_10g_compliance_t;

typedef enum {
    DEVMAN_TRANS_INFINIBAND_UNKNOWN,
    DEVMAN_TRANS_INFINIBAND_1X_COPPER_PASSIVE,
    DEVMAN_TRANS_INFINIBAND_1X_COPPER_ACTIVE,
    DEVMAN_TRANS_INFINIBAND_1X_LX,
    DEVMAN_TRANS_INFINIBAND_1X_SX
} devman_trans_infiniband_compliance_t;

typedef enum {
    DEVMAN_TRANS_ESCON_UNKNOWN,
    DEVMAN_TRANS_ESCON_SMF,
    DEVMAN_TRANS_ESCON_MMF
} devman_trans_escon_compliance_t;

typedef enum {
    DEVMAN_TRANS_SONET_UNKNOWN,
    DEVMAN_TRANS_SONET_OC3_SHORT,
    DEVMAN_TRANS_SONET_OC3_SM_INTER,
    DEVMAN_TRANS_SONET_OC3_SM_LONG,
    DEVMAN_TRANS_SONET_OC12_SHORT,
    DEVMAN_TRANS_SONET_OC12_SM_INTER,
    DEVMAN_TRANS_SONET_OC12_SM_LONG,
    DEVMAN_TRANS_SONET_OC48_SHORT,
    DEVMAN_TRANS_SONET_OC48_SM_INTER,
    DEVMAN_TRANS_SONET_OC48_SM_LONG,
    DEVMAN_TRANS_SONET_SPEC_BIT1,
    DEVMAN_TRANS_SONET_SPEC_BIT2,
    DEVMAN_TRANS_SONET_192_SHORT,
    DEVMAN_TRANS_SONET_OC3_INTER_SR1_IR1_LR1
} devman_trans_sonet_compliance_t;

typedef enum {
    DEVMAN_TRANS_ETH_UNKNOWN,
    DEVMAN_TRANS_ETH_1000SX,
    DEVMAN_TRANS_ETH_1000LX,
    DEVMAN_TRANS_ETH_1000CX,
    DEVMAN_TRANS_ETH_1000T,
    DEVMAN_TRANS_ETH_100LX,
    DEVMAN_TRANS_ETH_100FX,
    DEVMAN_TRANS_ETH_BX10,
    DEVMAN_TRANS_ETH_PX
} devman_trans_ethernet_compliance_t;

typedef enum {
    DEVMAN_TRANS_FCL_UNKNOWN,
    DEVMAN_TRANS_FCL_MEDIUM,
    DEVMAN_TRANS_FCL_LONG,
    DEVMAN_TRANS_FCL_INTERMEDIATE,
    DEVMAN_TRANS_FCL_SHORT,
    DEVMAN_TRANS_FCL_VERY_LONG
} devman_trans_fibre_chnl_length_t;

typedef enum {
    DEVMAN_TRANS_FCT_UNKNOWN,
    DEVMAN_TRANS_FCT_COPPER_FC_BASE_T,
    DEVMAN_TRANS_FCT_COPPER_PASSIVE,
    DEVMAN_TRANS_FCT_COPPER_ACTIVE,
    DEVMAN_TRANS_FCT_LONGWAVE_LL,
    DEVMAN_TRANS_FCT_SHORTWAVE_W_OFC_SL,
    DEVMAN_TRANS_FCT_SHORTWAVE_WO_OFC_SN,
    DEVMAN_TRANS_FCT_ELECTRIC_INTRA_ENCL_EL,
    DEVMAN_TRANS_FCT_ELECTRIC_INTER_ENCL_EL,
    DEVMAN_TRANS_FCT_LONGWAVE_LC,
    DEVMAN_TRANS_FCT_SHORTWAVE_LINEAR_RX_SA
} devman_trans_fibre_chnl_technology_t;

typedef enum {
    DEVMAN_TRANS_FCM_UNKNOWN,
    DEVMAN_TRANS_FCM_SINGLE_SM,
    DEVMAN_TRANS_FCM_MULTI_50UM_M5,
    DEVMAN_TRANS_FCM_MULTI_62_5UM_M6,
    DEVMAN_TRANS_FCM_VIDEO_COAX_TV,
    DEVMAN_TRANS_FCM_MINI_COAX_MI,
    DEVMAN_TRANS_FCM_TWIST_PAIR_TP,
    DEVMAN_TRANS_FCM_TWIN_AX_PAIR_TW
} devman_trans_fibre_chnl_media_t;

typedef enum {
    DEVMAN_TRANS_FCS_UNKNOWN,
    DEVMAN_TRANS_FCS_100,
    DEVMAN_TRANS_FCS_200,
    DEVMAN_TRANS_FCS_400,
    DEVMAN_TRANS_FCS_800,
    DEVMAN_TRANS_FCS_1200,
    DEVMAN_TRANS_FCS_1600
} devman_trans_fibre_chnl_speed_t;

typedef struct {
    devman_trans_10g_compliance_t        g10_compliance;  /* 3-3 B4-7 10GBASE-SR, 10GBASE-LR, 10GBASE-LRM*/
    devman_trans_infiniband_compliance_t infiniband_compliance;  /* 3-3 B0-B3 */
    devman_trans_escon_compliance_t      escon_compliance;  /* 4-4 B6-B7 */
    devman_trans_sonet_compliance_t      sonet_compliance;  /* 4(B0-B5)-5 */
    devman_trans_ethernet_compliance_t   eth_compliance;     /* 6-6 - 1000BASE-T, 1000BASE-CX, etc. */
    devman_trans_fibre_chnl_length_t     fibre_chn_lenfth;   /* 7(B3-B7) */
    devman_trans_fibre_chnl_technology_t fibre_chn_technlgy; /* 7(B0-B2)-8 */
    devman_trans_fibre_chnl_media_t      fibre_chn_media  ; /* 9 */
    devman_trans_fibre_chnl_speed_t      fibre_chn_speed;   /* 10-10 */
} devman_trans_tranceiver_t;

typedef struct {
    b_i32     Year;
    b_i32     Month;
    b_i32     Day;
    b_i32     Lot;
} devman_trans_date_t;

typedef enum {
    DEVMAN_TRANS_ENCODING_UNKNOWN,
    DEVMAN_TRANS_ENCODING_8B_10B,
    DEVMAN_TRANS_ENCODING_4B_5B,
    DEVMAN_TRANS_ENCODING_NRZ,
    DEVMAN_TRANS_ENCODING_MANCHESTER,
    DEVMAN_TRANS_ENCODING_SONET_SCRAMBLED,
    DEVMAN_TRANS_ENCODING_64B_66B
} devman_trans_encoding_t;

typedef enum {
    DEVMAN_TRANS_RATE_ID_UNKNOWN,
    DEVMAN_TRANS_RATE_ID_4_2_1G__AS0_AS1,
    DEVMAN_TRANS_RATE_ID_8_4_2G_RX,
    DEVMAN_TRANS_RATE_ID_8_4_2G_RX_TX,
    DEVMAN_TRANS_RATE_ID_8_4_2G_TX,
} devman_trans_rate_id_t;

typedef struct {
    b_bool      relevance;
    b_bool      status;
} devman_trans_diag_param_type_t;

typedef struct {
    devman_trans_diag_param_type_t      ddm_supported;       /* B6 on 92*/
    devman_trans_diag_param_type_t      intern_calibr;       /* B5 on 92*/
    devman_trans_diag_param_type_t      extern_calibr;       /* B4 on 92*/
    devman_trans_diag_param_type_t      avg_power_measure;    /* B3 on 92*/
    devman_trans_diag_param_type_t      adr_chng_required;    /* B2 on 92*/
    devman_trans_diag_param_type_t      ber_support;         /* B4 on 92*/
} devman_trans_diag_type_t;

/** vendor data describing media parameters */
typedef struct {
    devman_trans_transeiver_type_t  transeiver_type;     /*0-0 - Unknown, GIBIC, etc.*/
    devman_trans_extended_type_t    extended_type;       /*1-1 - MOD_DEF N*/
	devman_trans_connector_type_t   connector_type;	    /* 2-2 - FiberJack, LC, MT-RJ, etc */
    devman_trans_tranceiver_t       tranceiver;         /* 3-10 - Elecric or optical compability */
    devman_trans_encoding_t         encoding;           /* 11-11 */
    devman_trans_rate_id_t          rate_id;             /* 13-13 */
    b_i32                        bit_rate_nom;         /* 12-12 - Nominal signalling rate */
    b_i32                        bitrate_max;         /* 12 & 66-66 - Maximal signalling rate */
    b_i32                        bit_rate_min;         /* 12 & 67-67 - Minimal signalling rate */
    b_i32                        length_smf;          /* 14-15 Length for single mode fiber */
    b_i32                        length50um;         /* 16-16 Length for 50 */
    b_i32                        length62_5um;       /* 17-17 Length for 62.5 um */
    b_i32                        length_copper;       /* 18-18 Length for copper */
    b_i32                        length_om3;          /* 19-19 Length for 50 um OM3 fiber */
	b_i8                         vendor_name[17];	    /* 20-35 SFP vendor name (ASCII) */
	b_i8                         vendor_oui[4];		/* 37-39 SFP vendor IEEE company ID */
	b_i8                         vendor_pn[17];		/* 40-55 Part number provided by SFP vendor (ASCII) */
	b_i8                         vendor_rev[5];		/* 56-59 Revision level for part number provided by vendor (ASCII) */
	b_i8                         vendor_sn[17];		/* 68-83 Serial number provided by vendor (ASCII) */
    devman_trans_date_t             mfg_date;            /* 84-91 Vendor's manufacturing date */
	devman_trans_diag_type_t        diagnostic;		    /* 92-92 Supporting diagnostic parameters */
} devman_trans_description_t;

/** Main struct of medial parameters */
typedef struct {
	devman_trans_link_type_t		link_type;		/* specific transceiver in slot */
	devman_trans_revision_t	    revision; 		/* transmission type ID */
	devman_trans_description_t		description;    /* vendor data describing media parameters */
} devman_trans_param_t;

/** Parameters of treshold */
typedef struct {
    union {
        b_u8	msb;
        b_i8	modulo;
    };
    union {
        b_u8	lsb;
	    b_u8	fraction;
    };
} devman_trans_diag_tresh_value_t;

typedef struct {
    b_bool                          is_applicable;
	devman_trans_diag_tresh_value_t	 hi_alarm;
	devman_trans_diag_tresh_value_t	lo_alarm;
	devman_trans_diag_tresh_value_t	hi_warn;
	devman_trans_diag_tresh_value_t	lo_warn;
} devman_trans_diag_tresh_unit_t;


/** vendor-defined tresholds */
typedef struct {
	devman_trans_diag_tresh_unit_t	    temperature;	/* Temperature treshold */
	devman_trans_diag_tresh_unit_t	    voltage;	    /* Voltage treshold */
	devman_trans_diag_tresh_unit_t  	bias;	        /* Bias treshold */
    devman_trans_diag_tresh_unit_t     tx_power;		/* Tx power treshold */
	devman_trans_diag_tresh_unit_t     rx_power;		/* Rx power treshold */
} devman_trans_diag_tresh_t;


typedef struct {
    devman_trans_diag_tresh_value_t     slope;
    devman_trans_diag_tresh_value_t     offset;
} devman_trans_diag_calib_item_t;


typedef struct {
    b_bool                         is_applicable;
    b_float32                      rx_pwr[5];
    devman_trans_diag_calib_item_t    txi;
    devman_trans_diag_calib_item_t    tx_pwr;
    devman_trans_diag_calib_item_t    temperature;
    devman_trans_diag_calib_item_t    voltage;
} devman_trans_diag_calib_t;


/* Main structure of media digital diagnostic */
typedef struct {
	devman_trans_diag_tresh_t	    threshold;			/* vendor-defined tresholds */
    devman_trans_diag_calib_t      calibration;        /* DDM calibration parameters */
} devman_trans_diag_param_t;


/** Main structure of media digital measured status */
typedef struct {
    devman_trans_diag_tresh_value_t    temperature;
    devman_trans_diag_tresh_value_t    bcc;
    devman_trans_diag_tresh_value_t    tx_bias;
    devman_trans_diag_tresh_value_t    tx_power;
    devman_trans_diag_tresh_value_t    rx_power;
    devman_trans_diag_tresh_value_t    aux1;
    devman_trans_diag_tresh_value_t    aux2;
    b_bool                          tx_disable;
    b_bool                          soft_tx_disable;
    b_bool                          rate_select;
    b_bool                          soft_rate_select;
    b_bool                          tx_fault;
    b_bool                          los;
    b_bool                          data_ready_bar;
} devman_trans_diag_status_t;

/** IDs of media internal PHY's registers*/
typedef enum {
    DEVMAN_TRANS_REG_ID_CONTROL,
    DEVMAN_TRANS_REG_ID_STATUS,
    DEVMAN_TRANS_REG_ID_PHY_IDENTIFIER1,
    DEVMAN_TRANS_REG_ID_PHY_IDENTIFIER2,
    DEVMAN_TRANS_REG_ID_AUTONEG_ADVERTISE,
    DEVMAN_TRANS_REG_ID_LINK_PARTNER_ABILITY,
    DEVMAN_TRANS_REG_ID_AUTONEG_EXPANSION,
    DEVMAN_TRANS_REG_ID_NEXT_PAGE_TX,
    DEVMAN_TRANS_REG_ID_LINK_PARTNER_NEXT_PAGE,
    DEVMAN_TRANS_REG_ID_1000B_T_CONTROL,
    DEVMAN_TRANS_REG_ID_1000B_T_STATUS,
    DEVMAN_TRANS_REG_ID_EXT_STATUS,
    DEVMAN_TRANS_REG_ID_PHY_SPECIFIC_CONTROL,
    DEVMAN_TRANS_REG_ID_PHY_SPECIFIC_STATUS,
    DEVMAN_TRANS_REG_ID_INTERRUPT_ENABLE,
    DEVMAN_TRANS_REG_ID_INTERRUPT_STATUS,
    DEVMAN_TRANS_REG_ID_EXT_PHY_SPECIFIC_CONTROL,
    DEVMAN_TRANS_REG_ID_RX_ERROR_COUNTER,
    DEVMAN_TRANS_REG_ID_EXT_ADDR_CABLE_DIAGNOSTIC,
    DEVMAN_TRANS_REG_ID_LED_CONTROL,
    DEVMAN_TRANS_REG_ID_MANUAL_LED_OVERRIDE,
    DEVMAN_TRANS_REG_ID_EXT_PHY_SPECIFIC_CONTROL2,
    DEVMAN_TRANS_REG_ID_EXT_PHY_SPECIFIC_STATUS,
    DEVMAN_TRANS_REG_ID_CABLE_DIAGNOSTIC,
    DEVMAN_TRANS_REG_ID_LAST_REGISTER  /* This must be the last */
} devman_trans_reg_id_t;


/**
 * @brief Check if transceiver is present for specific port of 
 *        specific device
 * @param[in] dev - device the port belongs to 
 * @param[in] port - port index 
 * @return CCL_TRUE transceiver is present, CCL_FALSE - 
 *         otherwise
 */
b_bool devman_brd_trans_is_present(devman_dev_t dev, int port);

/**
 * @brief Retrieve transceiver parameters for specific port of
 *        specific device
 * @param[in] dev - device the port belongs to 
 * @param[in] port - port index 
 * @param[out] pparam - transceiver parameters
 * @return CCL_TRUE transceiver is present, CCL_FALSE - 
 *         otherwise
 */
ccl_err_t devman_brd_trans_data_get(devman_dev_t dev, int port, devman_trans_param_t *pparam);

/**
 * @brief Set transceiver parameters for specific port of
 *        specific device
 * @param[in] dev - device the port belongs to 
 * @param[in] port - port index 
 * @param[in] pparam - transceiver parameters
 * @return CCL_TRUE transceiver is present, CCL_FALSE - 
 *         otherwise
 * @note Currently supported dwdm wave length settings, dmm 
 *       thresholds settings
 */
ccl_err_t devman_brd_trans_data_set(devman_dev_t dev, int port, devman_trans_param_t *pparam);

#endif /*_DEVMAN_BRD_TRANS_H*/
