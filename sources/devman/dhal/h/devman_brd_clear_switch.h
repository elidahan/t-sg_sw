/********************************************************************************/
/**
 * @file devman_brd_clear_switch.h
 * @brief HAL clear switch interface
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#ifndef _DEVMAN_BRD_CLEAR_SWITCH_H
#define _DEVMAN_BRD_CLEAR_SWITCH_H

#include <ccl_types.h>


/** clear contact switches enumeration */
typedef enum {
    DEVMAN_CLEAR_SWITCH_EMERGENCY = 1,
    DEVMAN_CLEAR_SWITCH_CLR,
    DEVMAN_CLEAR_SWITCH_INVALID
} devman_clear_switch_t;


/**
 * @brief Read value of input clear switch
 * @param[in] cswitch - clear contact switch 
 * @param[out] pval - retrieved value 
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_clear_switch_get(devman_clear_switch_t cswitch, b_u32 *pval);

/**
 * @brief Set led of input clear switch
 * @param[in] cswitch - clear contact switch 
 * @param[out] ledstate - led's value (on/off) 
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_clear_switch_led_set(devman_clear_switch_t cswitch, b_bool ledstate);

#endif /*_DEVMAN_BRD_CLEAR_SWITCH_H*/
