/********************************************************************************/
/**
 * @file devman_brd_port.h
 * @brief HAL ports interface
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#ifndef _DEVMAN_BRD_PORT_H
#define _DEVMAN_BRD_PORT_H

#include "devman_brd_gen.h"

/** port link statuses enumeration */
typedef enum {
    DEVMAN_IF_LINK_UP = 1,
    DEVMAN_IF_LINK_DOWN
} devman_if_link_status_t;

/** Interface type enumeration */
typedef enum {
    DEVMAN_IF_TYPE_ETHERNET_CSMACD = 1,
    DEVMAN_IF_TYPE_IEEE8023ADLAG,
    DEVMAN_IF_TYPE_L2VLAN
} devman_if_type_t;

/**
 * @brief Initialize the port given by index (logical port) 
 * @param[in] logport - port index
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_port_init(b_u32 logport);

/**
 * @brief Get number of the ports in the system 
 * @param[out]  pports - number of ports
 * @return CCL_OK on success, error code on failure 
 * @note number of the ports includes both front panel and 
 *       inband ports. Ports between 2 devices are counted as
 *       separate ports
 */
ccl_err_t devman_brd_port_number_of_ports_get(b_u32 *pports);

/**
 * @brief Get specific counter of the port of the specific 
 *        device
 * @param[in] port - port index 
 * @param[in] counter_type - type of the counter 
 * @param[out] pcounter - counter's value
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_port_counter_get(b_u32 logport, devman_if_counter_t counter_type, b_u64 *pcounter);

/**
 * @brief Clear specific counter of the port of the specific 
 *        device
 * @param[in] port - port index 
 * @param[in] counter_type - type of the counter 
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_port_counter_clear(b_u32 logport, devman_if_counter_t counter_type);

/**
 * @brief Set Tx enable/disable state of the port of the 
 *        specific device
 * @param[in] port - port index 
 * @param[in] state - Tx state 
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_port_tx_state_set(b_u32 logport, b_bool state);

/**
 * @brief Get Tx enable/disable state of the port of the 
 *        specific device
 * @param[in] port - port index 
 * @param[out] pstate - Tx state 
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_port_tx_state_get(b_u32 logport, b_bool *pstate);

/**
 * @brief Get port's link status
 * @param[in] port - port index 
 * @param[out] pstatus - link status
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_port_link_status_get(b_u32 logport, devman_if_link_status_t *pstatus);

/**
 * @brief Control port's led (on/off)
 * @param[in] port - port index 
 * @param[in] state - led state
 * @return CCL_OK on success, error code on failure 
 * @note Only front panel ports are relevant 
 */
ccl_err_t devman_brd_port_led_state_set(b_u32 logport,  b_bool state);

/**
 * @brief Set specific property of the port of the specific 
 *        device
 * Properties are complied with RFC8343 - A YANG Data Model for 
 * Interface Management 
 * @param[in] dev - device the port belongs to 
 * @param[in] port - port index 
 * @param[in] prop - property to be configured
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_port_property_set(b_u32 logport, devman_if_property_t prop, 
                                    void *pdata, b_u32 size);

/**
 * @brief Get specific property of the port of the specific 
 *        device
 * Properties are complied with RFC8343 - A YANG Data Model for 
 * Interface Management 
 * @param[in] port - port index 
 * @param[out] pprop - the value of the property
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_port_property_get(b_u32 logport, devman_if_property_t prop, 
                                    void *pdata, b_u32 size);

#endif /*_DEVMAN_BRD_PORT_H*/
