/********************************************************************************/
/**
 * @file devman_brd_misc.h
 * @brief HAL Miscelaneous Devices interface
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#ifndef _DEVMAN_BRD_MISC_H
#define _DEVMAN_BRD_MISC_H

/** Supported HW device states */
typedef enum {
    DEVMAN_DEV_OPERATIONAL = 1,
    DEVMAN_DEV_FAILED,
    DEVMAN_DEV_STATE_INVALID
} devman_dev_state_t;

/** FPGA boot devices enumeration */
typedef enum {
    DEVMAN_FPGA_BOOT_MAIN_CPU = 1,
    DEVMAN_FPGA_BOOT_SPI_FLASH,
} devman_fpga_boot_dev_t;

/** SPI serial flashes enumeration */
typedef enum {
    DEVMAN_SPI_FLASH_MAIN_CPU = 1,
    DEVMAN_SPI_FLASH_CTRL_CPU,
    DEVMAN_SPI_FLASH_APP_CPU_1,
    DEVMAN_SPI_FLASH_APP_CPU_2,
    DEVMAN_SPI_FLASH_APP_CPU_3,
    DEVMAN_SPI_FLASH_APP_CPU_4,
} devman_spi_boot_flash_t;

#define DEVMAN_FLASH_INFO_FIELD_MAX_LEN 64
/** structure containing flash information */
typedef struct {
    b_bool is_present;
    b_u16 size;
    char uboot_ver[DEVMAN_FLASH_INFO_FIELD_MAX_LEN];
    char os_ver[DEVMAN_FLASH_INFO_FIELD_MAX_LEN];
} devman_flash_info_t;

/** structure containing RAM usage information */
typedef struct {
    b_u32 total;
    b_u32 used;
    b_u32 free;
    b_u32 buffered;
    b_u32 shared;
    b_u32 available
} devman_ram_usage_info_t;

#define DEVMAN_CPU_INFO_FIELD_MAX_LEN 64
/** structure containing cpu information */
typedef struct {
    char model_name[DEVMAN_CPU_INFO_FIELD_MAX_LEN];
    /** @todo: complete this structure */
} devman_cpu_info_t;

/**
 * @brief Reset HW device
 * @param[in] dev - resetted device
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_misc_device_reset(devman_dev_t dev);

/**
 * @brief Unreset HW device
 * @param[in] dev - unresetted device
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_misc_device_unreset(devman_dev_t dev);

/**
 * @brief Check whether HW device is present
 * @param[in] dev - HW device
 * @return CCL_TRUE - if present, CCL_FALSE - otherwise
 */
b_bool devman_brd_misc_device_is_present(devman_dev_t dev);

/**
 * @brief Check status of HW device
 * @param[in] dev - HW device 
 * @param[out] state - state of the device 
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_misc_device_status_get(devman_dev_t dev, devman_dev_state_t state);

/**
 * @brief Control boot device of FPGA
 * @param[in] bootdev - boot device 
 * @return CCL_OK on success, error code on failure 
 * @note FPGA can be booted from main cpu or from directly 
 *       attached SPI flash
 */
ccl_err_t devman_brd_misc_fpga_boot_select_set(devman_fpga_boot_dev_t bootdev);

/**
 * @brief Burn FPGA directly from host
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_misc_fpga_program(void);

/**
 * @brief Burn FPGA SPI flash
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_misc_fpga_flash_program(void);

/**
 * @brief Burn SPI boot flash for specific CPU 
 * @param[in] bootflash - flash corresponded to the CPU
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_misc_flash_program(devman_spi_boot_flash_t bootflash);

/**
 * @brief Read firmware version from the device containing 
 *        firmware
 * @param[in] dev - device containing firmware 
 * @param[out] buf - firmware version string representation 
 * @param[in] len - buffer length 
 * @return CCL_OK on success, error code on failure 
 * @note valid devices are: DEVMAN_FPGA, DEVMAN_CPLD, DEVMAN_POWER_SEQ; 
 *       buffer is allocated by the caller
 */
ccl_err_t devman_brd_misc_firmware_version_get(devman_dev_t dev, char *buf, int len);

/**
 * @brief Reads SW version from the specific host
 * @param[in] host - CPU device
 * @param[out] buf - sw version string representation 
 * @param[in] len - buffer length 
 * @return CCL_OK on success, error code on failure 
 * @note valid devices are: DEVMAN_MAIN_CPU, DEVMAN_CTRL_CPU, 
 *       DEVMAN_APP_CPU_x, DEVMAN_ATP_CARD; buffer is allocated by the
 *       caller
 */
ccl_err_t devman_brd_misc_sw_version_get(devman_dev_t host, char *buf, int len);

/**
 * @brief Get flash info
 * @param[in] flash - flash device
 * @param[out] info - retrieved information
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_misc_flash_info_get(devman_brd_flash_t flash, devman_flash_info_t *info);

/**
 * @brief Get RAM memory usage from the specific host
 * @param[in] host - CPU device
 * @param[out] usage - RAM usage info
 * @return CCL_OK on success, error code on failure 
 * @note valid devices are: DEVMAN_MAIN_CPU, DEVMAN_CTRL_CPU, 
 *       DEVMAN_APP_CPU_x, DEVMAN_ATP_CARD
 */
ccl_err_t devman_brd_misc_ram_usage_info_get(devman_dev_t host, devman_ram_usage_info_t *usage);

/**
 * @brief Get CPU info from the specific host
 * @param[in] host - CPU device
 * @param[out] cpuinfo - CPU info
 * @return CCL_OK on success, error code on failure 
 * @note valid devices are: DEVMAN_MAIN_CPU, DEVMAN_CTRL_CPU, 
 *       DEVMAN_APP_CPU_x, DEVMAN_ATP_CARD
 */
ccl_err_t devman_brd_misc_cpu_info_get(devman_dev_t host, devman_cpu_info_t *cpuinfo);

/**
 * @brief Get sysmon test result
 * @param[in] dev - device under test
 * @return CCL_TRUE - test passed, CCL_FALSE - otherwise
 * @note valid devices are: DEVMAN_FPGA, DEVMAN_CPLD
 */
b_bool devman_brd_misc_sysmon_test_result_get(devman_dev_t dev);

/**
 * @brief Run sysmon test 
 * @param[in] dev - device under test
 * @return CCL_OK on success, error code on failure 
 * @note valid devices are: DEVMAN_FPGA, DEVMAN_CPLD
 */
ccl_err_t devman_brd_misc_sysmon_test_run(devman_dev_t dev);



#endif /*_DEVMAN_BRD_MISC_H*/
