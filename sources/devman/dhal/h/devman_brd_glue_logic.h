/********************************************************************************/
/**
 * @file devman_brd_glue_logic.h
 * @brief HAL Glue Logic interface
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#ifndef _DEVMAN_BRD_GLUE_LOGIC_H
#define _DEVMAN_BRD_GLUE_LOGIC_H

/**
 * @brief Init board Glue Logic device
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_gl_init(void);

/**
 * @brief Write value to specific register of Glue Logic device 
 * @param[in] reg - register address
 * @param[in] val - written value 
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_gl_write(int reg, int val);

/**
 * @brief Read value from specific register of Glue Logic device
 * @param[in] reg - register address
 * @param[out] pval - read value 
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_gl_read(int reg, int *pval);

#endif /*_DEVMAN_BRD_GLUE_LOGIC_H*/
