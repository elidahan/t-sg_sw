/********************************************************************************/
/**
 * @file devman_brd_spi_mux.h
 * @brief HAL SPI MUX interface
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#ifndef _DEVMAN_BRD_SPI_MUX_H
#define _DEVMAN_BRD_SPI_MUX_H

#include <ccl_types.h>


/** SPI MUX serial flashes enumeration */
typedef enum {
    DEVMAN_SPI_MUX_MAIN_CPU = 1,
    DEVMAN_SPI_MUX_CTRL_CPU,
    DEVMAN_SPI_MUX_APP_CPU_1,
    DEVMAN_SPI_MUX_APP_CPU_2,
    DEVMAN_SPI_MUX_APP_CPU_3,
    DEVMAN_SPI_MUX_APP_CPU_4,
    DEVMAN_SPI_MUX_CPLD_FLASH_1,
    DEVMAN_SPI_MUX_CPLD_FLASH_2,
} devman_spi_mux_dest_t;


/**
 * @brief Set SPI MUX destination
 * @param[in] dest - spi mux destination
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_spi_mux_dest_set(devman_spi_mux_dest_t dest);

#endif /*_DEVMAN_BRD_SPI_MUX_H*/
