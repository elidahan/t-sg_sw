/********************************************************************************/
/**
 * @file devman_brd_uart.h
 * @brief HAL UART interface
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#ifndef _DEVMAN_BRD_UART_H
#define _DEVMAN_BRD_UART_H

#include <ccl_types.h>

typedef enum {
    DEVMAN_UART_CLI_PORT,
    DEVMAN_UART_PORT_MAX
} devman_uart_port_t;

/** UART properties enumeration */
typedef enum {
    DEVMAN_UART_BAUD_RATE = 1,
    DEVMAN_UART_PARITY,
    DEVMAN_UART_STOP_BIT,
    DEVMAN_UART_DATA_LEN
} devman_uart_property_t;


/**
 * @brief Set UART property 
 * @param[in] port - serial port
 * @param[in] property - serial port property
 * @param[in] val - property value
 * @return CCL_OK on success, error code on failure 
 * @note currently only CLI port is supported 
 */
ccl_err_t devman_brd_uart_property_set(devman_uart_port_t port, devman_uart_property_t property, b_u32 val);

/**
 * @brief Get UART property
 * @param[in] port - serial port
 * @param[in] property - uart property
 * @param[out] pval - property value
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_uart_property_get(devman_uart_port_t port, devman_uart_property_t property, b_u32 *pval);

#endif /*_DEVMAN_BRD_UART_H*/
