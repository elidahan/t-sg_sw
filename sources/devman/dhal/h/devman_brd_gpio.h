/********************************************************************************/
/**
 * @file devman_brd_gpio.h
 * @brief HAL GPIO interface
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#ifndef _DEVMAN_BRD_GPIO_H
#define _DEVMAN_BRD_GPIO_H

#include <ccl_types.h>

/**
 * @brief Init board default GPIO values and directions
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_gpio_init(void);

/**
 * @brief Write value to GPIO pin of specific device 
 * @param[in] iodev - device 
 * @param[in] iopin - pin number 
 * @param[in] val - written value 
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_gpio_set(int iodev, int iopin, int val);

/**
 * @brief Read value from GPIO pin of specific device 
 * @param[in] iodev - device 
 * @param[in] iopin - pin number 
 * @param[out] pval - read value 
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_gpio_get(int iodev, int iopin, int *pval);

#endif /*_DEVMAN_BRD_GPIO_H*/
