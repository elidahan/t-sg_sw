/********************************************************************************/
/**
 * @file devman_brd_uart_mux.h
 * @brief HAL UART MUX interface
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#ifndef _DEVMAN_BRD_UART_MUX_H
#define _DEVMAN_BRD_UART_MUX_H

#include <ccl_types.h>


/** UART MUX destinations enumeration */
typedef enum {
    DEVMAN_UART_MUX_ATP_CARD = 1,
    DEVMAN_UART_MUX_APP_CPU_1,
    DEVMAN_UART_MUX_APP_CPU_2,
    DEVMAN_UART_MUX_APP_CPU_3,
    DEVMAN_UART_MUX_APP_CPU_4,
} devman_uart_mux_dest_t;


/**
 * @brief Set UART MUX destination
 * @param[in] dest - uart mux destination
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_uart_mux_dest_set(devman_uart_mux_dest_t dest);

#endif /*_DEVMAN_BRD_UART_MUX_H*/
