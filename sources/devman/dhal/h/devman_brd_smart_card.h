/********************************************************************************/
/**
 * @file devman_brd_smart_card.h
 * @brief HAL Smart Card interface
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#ifndef _DEVMAN_BRD_SMART_CARD_H
#define _DEVMAN_BRD_SMART_CARD_H

#include <ccl_types.h>

#define DEVMAN_SMART_CARD_FIELD_MAX_LEN 32

typedef struct {
    char *name;
    char contents[DEVMAN_SMART_CARD_FIELD_MAX_LEN];
    b_u8 len;
} devman_smart_cart_field_t;

/**
 * @brief Check whether Smart Card is present 
 * @return CCL_TRUE - smart card is present, CCL_FALSE otherwise
 * @note currently only CLI port is supported 
 */
b_bool devman_brd_smart_card_is_present(void);

/**
 * @brief Get Smart Card field contents 
 * param[in,out] field - smart card field 
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_smart_card_field_get(devman_smart_cart_field_t *field);

#endif /*_DEVMAN_BRD_SMART_CARD_H*/
