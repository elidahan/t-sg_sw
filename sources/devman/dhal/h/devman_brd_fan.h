/********************************************************************************/
/**
 * @file devman_brd_fan.h
 * @brief HAL fans interface
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/

#ifndef _DEVMAN_BRD_FAN_H
#define _DEVMAN_BRD_FAN_H

/** fan status enumeration */
typedef enum {
    FAN_STAT_OK,
    FAN_STAT_ABSENT,
    FAN_STAT_OFF,
    FAN_STAT_FAIL
} devman_fan_status_t;

/** fan control modes enumeration */
typedef enum {
    FAN_CONTROL_DIRECT, /**< Fan speed override by SW */
    FAN_CONTROL_AUTO,   /**< Fan speed controlled by HW */
    FAN_CONTROL_MODES_NUM   /**< Must be last */
} devman_fan_control_t;


/**
 * @brief Get number of fan trays
 * @param[out]  ptrays - number of trays
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_fan_number_of_trays_get(int *ptrays);

/**
 * @brief Get number of fans in specified tray
 * @param[in]  tray - tray index 
 * @param[out] pfans - number of fans 
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_fan_number_of_fans_in_tray_get(int tray,  int *pfans);

/**
 * @brief Get operational status of specified tray
 * @param[in]  tray - tray index 
 * @param[out] pstatus - retrieved status
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_fan_tray_status_get(int tray,  devman_fan_status_t *pstatus);

/**
 * @brief Get operational status of the fan in specified tray
 * @param[in] tray - tray index 
 * @param[in] fan - fan index 
 * @param[out] pstatus - retrieved status
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_fan_tray_fan_status_get(int tray,  int fan,  devman_fan_status_t *pstatus);

/**
 * @brief Set mode of fans speed control per device 
 *        (auto/direct)
 * @param[in] mode - speed control mode
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_fan_control_mode_set( devman_fan_control_t mode);

/**
 * @brief Get mode of fans speed control per device 
 *        (auto/direct)
 * @param[out] pmode - speed control mode
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_fan_control_mode_get(devman_fan_control_t *pmode);

/**
 * @brief Set speed of fans in tray in direct control mode in percents 
 * @param[in] tray - tray index 
 * @param[in] percent - speed in percents
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_fan_tray_speed_set(int tray,  int percent);

/**
 * @brief Get speed of fans in specific tray
 * @param[in] tray - tray index 
 * @param[out] ppercent - speed in percents
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_fan_tray_speed_get(int tray,  int *ppercent);

#endif /*_DEVMAN_BRD_FAN_H*/
