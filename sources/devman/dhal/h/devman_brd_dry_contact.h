/********************************************************************************/
/**
 * @file devman_brd_dry_contact.h
 * @brief HAL dry contact interface
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#ifndef _DEVMAN_BRD_DRY_CONTACT_H
#define _DEVMAN_BRD_DRY_CONTACT_H

#include <ccl_types.h>


/** dry contact values enumeration */
typedef enum {
    DEVMAN_DRY_CONTACT_RESET = 1,
    DEVMAN_DRY_CONTACT_CLEAR,
    DEVMAN_DRY_CONTACT_FACTORY_DEFAULT,
    DEVMAN_DRY_CONTACT_INVALID
} devman_dry_contact_val_t;


/**
 * @brief Read value of input dry contacts
 * @param[out] pdcontact - dry contact value
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_dry_contact_get(devman_dry_contact_val_t *pdcontact);

#endif /*_DEVMAN_BRD_DRY_CONTACT_H*/
