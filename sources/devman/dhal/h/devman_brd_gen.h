/********************************************************************************/
/**
 * @file devman_brd_gen.h
 * @brief HAL generic definitions
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#ifndef _DEVMAN_BRD_GEN_H
#define _DEVMAN_BRD_GEN_H


/** board devices enumeration */
typedef enum {
    DEVMAN_MAIN_CPU,
    DEVMAN_CTRL_CPU,
    DEVMAN_APP_CPU_1,
    DEVMAN_APP_CPU_2,
    DEVMAN_APP_CPU_3,
    DEVMAN_APP_CPU_4,
    DEVMAN_CPLD,
    DEVMAN_FPGA,
    DEVMAN_ATP_CARD,
    DEVMAN_POWER_SEQ,
    DEVMAN_DEV_MAX_NUM
} devman_dev_t;

/** board flashes enumeration */
typedef enum {
    DEVMAN_BRD_SPI_FLASH_MAIN_CPU = 1,
    DEVMAN_BRD_MMC_FLASH_MAIN_CPU,
    DEVMAN_BRD_SPI_FLASH_CTRL_CPU,
    DEVMAN_BRD_MMC_FLASH_CTRL_CPU,
    DEVMAN_BRD_SPI_FLASH_APP_CPU_1,
    DEVMAN_BRD_SPI_FLASH_APP_CPU_2,
    DEVMAN_BRD_SPI_FLASH_APP_CPU_3,
    DEVMAN_BRD_SPI_FLASH_APP_CPU_4,
} devman_brd_flash_t;


/**  Network interfaces MIB counters */
typedef enum
{
   DEVMAN_IF_MIB_BYTEREC = 0,
   DEVMAN_IF_MIB_BYTSENT,
   DEVMAN_IF_MIB_FRAREC,
   DEVMAN_IF_MIB_FRASENT,
   DEVMAN_IF_MIB_TOTALBYTEREC,
   DEVMAN_IF_MIB_TOTALFRAMEREC,
   DEVMAN_IF_MIB_BRDFRAREC,
   DEVMAN_IF_MIB_MULFRAREC,
   DEVMAN_IF_MIB_CRCERR,
   DEVMAN_IF_MIB_FRAOVERSIZE,
   DEVMAN_IF_MIB_FRAFRAGMENTS,
   DEVMAN_IF_MIB_JABBER,
   DEVMAN_IF_MIB_COLL,
   DEVMAN_IF_MIB_LATECOL,
   DEVMAN_IF_MIB_FRA64,
   DEVMAN_IF_MIB_FRA65T127,
   DEVMAN_IF_MIB_FRA128T255,
   DEVMAN_IF_MIB_FRA256T511,
   DEVMAN_IF_MIB_FRA512T1023,
   DEVMAN_IF_MIB_FRA1024T1522,
   DEVMAN_IF_MIB_MACRXERR,
   DEVMAN_IF_MIB_DROPPED,
   DEVMAN_IF_MIB_INUCASTPKT,
   DEVMAN_IF_MIB_INNOUCASTPKT,
   DEVMAN_IF_MIB_INERRORS,
   DEVMAN_IF_MIB_OUTERRORS,
   DEVMAN_IF_MIB_OUTNOUCASTPKT,
   DEVMAN_IF_MIB_OUTUCASTPKT,
   DEVMAN_IF_MIB_MULTIOUT,
   DEVMAN_IF_MIB_BROADOUT,
   DEVMAN_IF_MIB_UNDERSIZ,
   DEVMAN_IF_MIB_INUNKNOWNPROTOS,
   DEVMAN_IF_MIB_ALIGN_ERR,
   DEVMAN_IF_MIB_FCS_ERR,
   DEVMAN_IF_MIB_SQETEST_ERR,
   DEVMAN_IF_MIB_CSE_ERR,
   DEVMAN_IF_MIB_SYMBOL_ERR,
   DEVMAN_IF_MIB_MACTX_ERR,
   DEVMAN_IF_MIB_MACRX_ERR,
   DEVMAN_IF_MIB_TOOLONGFRA,
   DEVMAN_IF_MIB_SNGL_COLLISION,
   DEVMAN_IF_MIB_MULT_COLLISION,
   DEVMAN_IF_MIB_LATE_COLLISION,
   DEVMAN_IF_MIB_EXCESS_COLLISION,
   DEVMAN_IF_MIB_INUNKNOWNOPCODE,
   DEVMAN_IF_MIB_DEFERREDTX,
   DEVMAN_IF_MIB_INPAUSE_FRAMES,
   DEVMAN_IF_MIB_OUTPAUSE_FRAMES,
   DEVMAN_IF_MIB_INFRAOVERSIZE,
   DEVMAN_IF_MIB_OUTFRAOVERSIZE,
   DEVMAN_IF_MIB_INFRAFRAGMENTS,
   DEVMAN_IF_MIB_OUTFRAFRAGMENTS,
   DEVMAN_IF_MIB_INJABBERS,
   DEVMAN_IF_MIB_OUTJABBERS,
   DEVMAN_IF_MIB_MAX
} devman_if_counter_t;

#define DEVMAN_IF_COUNTERS_STR \
{ \
    "rx_bytes", \
    "tx_bytes", \
    "rx_packets", \
    "tx_packets", \
    "rx_total_bytes", \
    "rx_total_packets", \
    "rx_bcast", \
    "rx_mcast", \
    "crc_errors", \
    "oversize", \
    "fragments", \
    "jabber", \
    "collisions", \
    "late_collisions", \
    "tx_64", \
    "tx_65_127", \
    "tx_128_255", \
    "tx_256_511", \
    "tx_512_1023", \
    "tx_1024_1522", \
    "rx_macerrors", \
    "dropped", \
    "rx_ucast", \
    "rx_noucast", \
    "rx_errors", \
    "tx_errors", \
    "tx_noucast", \
    "tx_ucast", \
    "tx_mcast", \
    "tx_bcast", \
    "undersize", \
    "rx_unknown_protos", \
    "align_errors", \
    "fcs_errors", \
    "sqetest_errors", \
    "cse_errors", \
    "symbol_errors", \
    "mac_txerrors", \
    "mac_rxerrors", \
    "too_long_frame", \
    "single_collisions", \
    "multi_collisions", \
    "late_collisions", \
    "excess_collisions", \
    "rx_unknown_opcode", \
    "deferred_tx", \
    "rx_pause", \
    "tx_pause", \
    "rx_oversize", \
    "tx_oversize", \
    "rx_fragments", \
    "tx_fragments", \
    "rx_jabbers", \
    "rx_jabbers" \
};

typedef enum {
	DEVMAN_IF_NAME = 0,
	DEVMAN_IF_MTU,
	DEVMAN_IF_SPEED_MAX,
	DEVMAN_IF_SPEED,
	DEVMAN_IF_DUPLEX,
	DEVMAN_IF_AUTONEG,
	DEVMAN_IF_ENCAP,
	DEVMAN_IF_MEDIUM,
	DEVMAN_IF_INTERFACE_TYPE,
	DEVMAN_IF_MDIX,
	DEVMAN_IF_MDIX_STATUS,
	DEVMAN_IF_LEARN,
    DEVMAN_IF_IFILTER,
    DEVMAN_IF_SELF_EFILTER,
    DEVMAN_IF_LOCK,
    DEVMAN_IF_DROP_ON_LOCK,
    DEVMAN_IF_FWD_UNK,
	DEVMAN_IF_BCAST_LIMIT,
	DEVMAN_IF_ABILITY,
	DEVMAN_IF_FLOWCONTROL,
	DEVMAN_IF_ENABLE,
	DEVMAN_IF_SFP_PRESENT,
	DEVMAN_IF_SFP_TX_STATE,
	DEVMAN_IF_SFP_RX_STATE,
    DEVMAN_IF_DEFAULT_VLAN,
    DEVMAN_IF_PRIORITY,
    DEVMAN_IF_LED_STATE,
	DEVMAN_IF_MEDIA_PARAMETERS,
	DEVMAN_IF_MEDIA_DETAILS,
	DEVMAN_IF_LOCAL_ADVERT,
    DEVMAN_IF_MEDIA_TYPE,
    DEVMAN_IF_SFP_PORT_TYPE,
    DEVMAN_IF_MEDIA_CONFIG_TYPE,
    DEVMAN_IF_MEDIA_ERROR_CONFIG_TYPE,
    DEVMAN_IF_MEDIA_SGMII_FIBER,
	DEVMAN_IF_TX_ENABLE,
	DEVMAN_IF_SFP_PORT_NUMBERS,
	DEVMAN_IF_AUTONEG_DUPLEX,
	DEVMAN_IF_AUTONEG_SPEED ,
    DEVMAN_IF_DDM_PARAMETERS, /**< Digital diagnostic monitoring parameters */
    DEVMAN_IF_DDM_STATUS,     /**< Digital diagnostic monitoring status     */
    DEVMAN_IF_DDM_SUPPORTED,  /**< Digital diagnostic monitoring Supported ?    */ 
    DEVMAN_IF_HWADDRESS_GET,
    DEVMAN_IF_ACTIVE_LINK,    /**< is Link Active*/
    DEVMAN_PROP_MAX
} devman_if_property_t;

#define DEVMAN_IF_PROPERTIES_STR \
{ \
    "if_name", \
    "if_mtu", \
    "if_speed_max", \
    "if_speed", \
    "if_duplex", \
    "if_autoneg", \
    "if_encap", \
    "if_medium", \
    "if_interface_type", \
    "if_mdix", \
    "if_mdix_status", \
    "if_learn", \
    "if_ifilter", \
    "if_self_efilter", \
    "if_lock", \
    "if_drop_on_lock", \
    "if_fwd_unk", \
    "if_bcast_limit", \
    "if_ability", \
    "if_flow_control", \
    "if_enable", \
    "if_sfp_present", \
    "if_sfp_tx_state", \
    "if_sfp_rx_state", \
    "if_default_vlan", \
    "if_priority", \
    "if_led_state", \
    "if_media_parameters", \
    "if_media_details", \
    "if_local_advert", \
    "if_media_type", \
    "if_sfp_port_type", \
    "if_media_config_type", \
    "if_media_error_config_type", \
    "if_media_sgmii_fiber", \
    "if_tx_enable", \
    "if_sfp_port_numbers", \
    "if_autoneg_duplex", \
    "if_autoneg_speed", \
    "if_ddm_parameters", \
    "if_ddm_status", \
    "if_ddm_supported", \
    "if_hwaddress_get", \
    "if_active_link" \
};

#endif /*_DEVMAN_BRD_GEN_H*/
