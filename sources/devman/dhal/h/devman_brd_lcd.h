/********************************************************************************/
/**
 * @file devman_brd_lcd.h
 * @brief HAL LCD interface
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#ifndef _DEVMAN_BRD_LCD_H
#define _DEVMAN_BRD_LCD_H

#include <ccl_types.h>

#define DEVMAN_LCD_MAX_STRING_LEN 80
#define DEVMAN_LCD_MAX_ROWS 4
#define DEVMAN_LCD_MAX_COLUMNS 20

/** LCD components enumeration */
typedef enum {
    DEVMAN_LCD_DISPLAY,
    DEVMAN_LCD_KEYPAD
} devman_lcd_component_t;

/** backlight settings enumeration */
typedef enum {
    DEVMAN_LCD_BACKLIGHT_OFF,
    DEVMAN_LCD_BACKLIGHT_ON,
    DEVMAN_LCD_BACKLIGHT_AUTO
} devman_lcd_backlight_t;

/** cursor direction enumeration */
typedef enum {
    DEVMAN_LCD_CURSOR_UP,
    DEVMAN_LCD_CURSOR_DOWN,
    DEVMAN_LCD_CURSOR_LEFT,
    DEVMAN_LCD_CURSOR_RIGHT
} devman_lcd_cursor_direction_t;

/** available leds enumeration */
typedef enum {
    DEVMAN_LCD_LED_1 = 1,
    DEVMAN_LCD_LED_2,
    DEVMAN_LCD_LED_3,
} devman_lcd_led_t;

/** available led colors enumeration */
typedef enum {
    DEVMAN_LCD_LED_GREEN,
    DEVMAN_LCD_LED_RED
} devman_lcd_led_color_t;

/**
 * @brief Clear LCD screen
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_lcd_screen_clear(void);

/**
 * @brief Display the string on the screen starting from current
 *        cursor position
 * @param[in] str - string to display
 * @return CCL_OK on success, error code on failure 
 * @note maximum supported string length is 
 *       DEVMAN_LCD_MAX_STRING_LEN
 */
ccl_err_t devman_brd_lcd_string_display(char *str);

/**
 * @brief Move the cursor one position to specific direction
 * @param[in] dir - direction to move
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_brd_lcd_cursor_move(devman_lcd_cursor_direction_t dir);

/**
 * @brief Set the cursor to a specific cursor position where the
 *        next character is printed
 * @param[in] row - destination row 
 * @param[in] column - destination column
 * @return CCL_OK on success, error code on failure 
 * @note maximum supported row is DEVMAN_LCD_MAX_ROWS, maximum 
 *       supported column is DEVMAN_LCD_MAX_COLUMNS
 */
ccl_err_t devman_brd_lcd_cursor_set(b_u32 row, b_u32 column);

/**
 * @brief Display the startup message on the screen starting 
 *        from upper-left position
 * @param[in] msg - message to display
 * @return CCL_OK on success, error code on failure 
 * @note maximum supported message length is 
 *       DEVMAN_LCD_MAX_STRING_LEN
 */
ccl_err_t devman_brd_lcd_startup_message_display(char *msg);

/**
 * @brief Set the colors for specific led
 * @param[in] led - led to set the color for
 * @param[in] color1 - first color 
 * @param[in] state1 - first color on/off 
 * @param[in] color2 - second color 
 * @param[in] state2 - second color on/off 
 * @return CCL_OK on success, error code on failure 
 * @note generated led's color is comprised from 2 separate 
 *       colors
 */
ccl_err_t devman_brd_lcd_led_color_set(devman_lcd_led_color_t led,
                                    devman_lcd_led_color_t color1, b_bool state1, 
                                    devman_lcd_led_color_t color2, b_bool state2);

/**
 * @brief Control backlight of LCD component
 * @param[in] comp - LCD component
 * @param[in] mode - backlight mode of the component
 * @return CCL_OK on success, error code on failure 
 * @note generated led's color is comprised from 2 separate 
 *       colors
 */
ccl_err_t devman_brd_lcd_backlight_set(devman_lcd_component_t comp, devman_lcd_backlight_t mode);

/**
 * @brief Control brightness of LCD component
 * @param[in] comp - LCD component
 * @param[in] brightness - brightness of the component
 * @return CCL_OK on success, error code on failure 
 * @note brightness level from 0(Dim) to 255(Bright) 
 */
ccl_err_t devman_brd_lcd_brightness_set(devman_lcd_component_t comp, b_u8 brightness);

/**
 * @brief Check if the key is pressed and if yes, provide
 *        pressed key value
 * @param[out] pkey - key value
 * @return CCL_TRUE if the key is pressed, CCL_FALSE otherwise
 */
b_bool devman_brd_lcd_key_is_pressed(b_u8 *key);

#endif /*_DEVMAN_BRD_LCD_H*/
