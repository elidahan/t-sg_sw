/********************************************************************************/
/**
 * @file devman_brd_dry_contact.c
 * @brief HAL dry contact interface implementation
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "devman_brd_dry_contact.h"

ccl_err_t devman_brd_dry_contact_get(devman_dry_contact_val_t *pdcontact)
{
    return CCL_OK;
}




