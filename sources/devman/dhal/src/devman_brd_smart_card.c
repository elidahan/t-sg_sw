/********************************************************************************/
/**
 * @file devman_brd_smart_card.c
 * @brief HAL Smart Card interface implementation
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "devman_brd_smart_card.h"

b_bool devman_brd_smart_card_is_present(void)
{
    return CCL_OK;
}

ccl_err_t devman_brd_smart_card_field_get(devman_smart_cart_field_t *field)
{
    return CCL_OK;
}


