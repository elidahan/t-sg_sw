/********************************************************************************/
/**
 * @file devman_brd_uart.c
 * @brief HAL UART interface implementation
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "devman_brd_uart.h"

ccl_err_t devman_brd_uart_property_set(devman_uart_port_t port, devman_uart_property_t property, b_u32 val)
{
    return CCL_OK;
}

ccl_err_t devman_brd_uart_property_get(devman_uart_port_t port, devman_uart_property_t property, b_u32 *pval)
{
    return CCL_OK;
}


