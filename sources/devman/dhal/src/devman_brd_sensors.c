/********************************************************************************/
/**
 * @file devman_brd_sensors.c
 * @brief HAL sensors interface implementation
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "devman_brd_sensors.h"

ccl_err_t devman_brd_sens_number_of_sensors_get(devman_sensor_type_t type,  int *pnum)
{
    return CCL_OK;
}

ccl_err_t devman_brd_sens_voltage_status_get(int sensor,  devman_sensor_status_t pstatus)
{
    return CCL_OK;
}

ccl_err_t devman_brd_sens_temperature_get(int sensor,  b_i32 *ptemp)
{
    return CCL_OK;
}

ccl_err_t devman_brd_sens_power_get(int sensor,  b_i32 *ppower)
{
    return CCL_OK;
}

ccl_err_t devman_brd_sens_op_state_get(int sensor, devman_sensor_op_state_t *pstate)
{
    return CCL_OK;
}



