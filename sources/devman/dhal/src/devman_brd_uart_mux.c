/********************************************************************************/
/**
 * @file devman_brd_uart_mux.c
 * @brief HAL UART MUX interface implementation
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "devman_brd_uart_mux.h"

ccl_err_t devman_brd_uart_mux_dest_set(devman_uart_mux_dest_t dest)
{
    return CCL_OK;
}



