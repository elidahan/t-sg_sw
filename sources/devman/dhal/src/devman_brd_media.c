/********************************************************************************/
/**
 * @file media.c
 * @brief Media EEPROM helper functions
 * @copyright 2020 CELARE. All rights reserved.
 */
/********************************************************************************/

#include "ccl_types.h"
#include "ccl_logger.h"
#include "devman_brd_media.h"

#define MEDIA_DEBUG
/* printing/error-returning macros */
#ifdef MEDIA_DEBUG
    #define PDEBUG(fmt, args...) \
         ccl_syslog_err("MEDIA: %s[%d]: " fmt, __FUNCTION__, __LINE__, ## args)
#else
    #define PDEBUG(fmt, args...)
#endif
#define MEDIA_ERROR_RETURN(err) do { \
   ccl_syslog_err("Error! %s[%d], error=%i\n", __FUNCTION__, __LINE__, (err)); \
   return err; \
} while (0)

static ccl_err_t _ret;

static devman_media_data_block_t sfp_block_def_table[SFP_TRANSLATION_PAGES] = {
    {0x50, 0x00, 0x60, 1}, 
    {0x51, 0x00, 0x60, 1}, 
    {0x51, 0x60, 0x18, 1}, 
    {0x56, 0x00, 0x40, 2}  
};

static devman_media_data_block_t qsfp28_block_def_table[QSFP28_TRANSLATION_PAGES] = {
    {0x50, 0x00, 0x60, 1}, 
    {0x51, 0x00, 0x60, 1}, 
    {0x51, 0x60, 0x18, 1}, 
    {0x56, 0x00, 0x40, 2}  
};

static b_bool sfp_ddm_media_diag_map[MEDIA_DDM_DIAG_MAP_LAST] = {
    CCL_TRUE,   //B6 on 92 Supported
    CCL_TRUE,   //B5 on 92 Internal calibration
    CCL_TRUE,   //B4 on 92 External calibration
    CCL_TRUE,   //B3 on 92 Avg power mesarement
    CCL_TRUE,   //B2 on 92 Avg change required
    CCL_FALSE   //B4 on 92 BER supported
};

static b_bool qsfp28_ddm_media_diag_map[MEDIA_DDM_DIAG_MAP_LAST] = {
    CCL_TRUE,   //B6 on 92 Supported
    CCL_TRUE,   //B5 on 92 Internal calibration
    CCL_TRUE,   //B4 on 92 External calibration
    CCL_TRUE,   //B3 on 92 Avg power mesarement
    CCL_TRUE,   //B2 on 92 Avg change required
    CCL_FALSE   //B4 on 92 BER supported
};

/* Detail alinement line location code */
typedef enum {
    MEDIA_DETAILS_LOCAL_REGULAR,      /* Regular */
    MEDIA_DETAILS_LOCAL_REGULAR_CONT, /* Regular, list continued */
    MEDIA_DETAILS_LOCAL_LINE_LAST,    /* Last in line */
    MEDIA_DETAILS_LOCAL_LAST          /* Last detail */
} line_location_t;

typedef struct {
    b_u32     index;
    char      *p_descr;
} item_description_t;

/* Connector type description */
static item_description_t connector_type_descr[MEDIA_CONNECTOR_LAST + 1] = {
    {MEDIA_CONNECTOR_UNKNOWN, "NA"},
    {MEDIA_CONNECTOR_SC, "SC"},
    {MEDIA_CONNECTOR_FCS1_COPPER, "Fibre Channel I copper"},
    {MEDIA_CONNECTOR_FCS2_COPPER, "Fibre Channel II copper"},
    {MEDIA_CONNECTOR_BNC_TNC, "BNC/TNC"},
    {MEDIA_CONNECTOR_FC_COAX, "Fibre Channel coaxial"},
    {MEDIA_CONNECTOR_FIBER_JACK, "FiberJack"},
    {MEDIA_CONNECTOR_LC, "LC"},
    {MEDIA_CONNECTOR_MT_RJ, "MT-RJ"},
    {MEDIA_CONNECTOR_MU, "MU"},
    {MEDIA_CONNECTOR_SG, "SG"},
    {MEDIA_CONNECTOR_OPTICAL_PIGTAIL, "Optical pigtail"},
    {MEDIA_CONNECTOR_MPO_PARALLEL_OPTIC, "MPO Parallel Optic"},
    {MEDIA_CONNECTOR_HSSDC, "HSSDC II"},
    {MEDIA_CONNECTOR_COPPER_PIGTAIL, "Copper Pigtail"},
    {MEDIA_CONNECTOR_RJ45, "RJ45"},
    {MEDIA_CONNECTOR_DETAIL_LIST_END, ""}   // Must be in the end
};

b_i32 sfp_ddm_thresholds_map[MEDIA_DDM_THRESHOLD_MAP_LAST] = {
    0,  //Temperature
    8,  //Voltage
    16, //Bias
    24, //Tx Power
    32, //Rx Power
    -1, //Aux1
    -1, //Aux2
    56  //Calibration
};

b_i32 qsfp28_ddm_thresholds_map[MEDIA_DDM_THRESHOLD_MAP_LAST] = {
    0,  //Temperature
    16, //Voltage
    56, //Bias
    -1, //Tx Power
    48, //Rx Power
    -1, //Aux1
    -1, //Aux2
    -1  //Calibration                                                   };
};

typedef struct {
    b_bool    relevance;
    b_i32     offset[4];
} ddm_status_map_type_t;

ddm_status_map_type_t sfp_ddm_status_map[MEDIA_DDM_STATUS_MAP_LAST] = {  
    {CCL_TRUE,  {0, 0, 0, 0}}, //Temperature
    {CCL_TRUE,  {2, 0, 0, 0}}, //Voltage
    {CCL_TRUE,  {4, 0, 0, 0}}, //Bias
    {CCL_TRUE,  {6, 0, 0, 0}}, //Tx power
    {CCL_TRUE,  {8, 0, 0, 0}}, //Rx power
    {CCL_FALSE, {0, 0, 0, 0}}, //Aux1
    {CCL_FALSE, {0, 0, 0, 0}}, //Aux2
    {CCL_TRUE,  {14, 0, 0, 0}},//Stat/Ctrl1
    {CCL_FALSE, {0, 0, 0, 0}} //Stat/Ctrl2
};
ddm_status_map_type_t qsfp28_ddm_status_map[MEDIA_DDM_STATUS_MAP_LAST] = { 
    {CCL_TRUE,  {0, 0, 0, 0}}, //Temperature
    {CCL_TRUE,  {4, 4, 4, 4}}, //Voltage
    // Channel-specific
    {CCL_TRUE,  {20, 22, 24, 26}}, //Bias
    {CCL_FALSE, {0, 0, 0, 0}}, //Tx power
    {CCL_TRUE,  {12, 14, 16, 18}}, //Rx power
    {CCL_FALSE, {0, 0, 0, 0}},//Aux1
    {CCL_FALSE, {0, 0, 0, 0}},//Aux2
    {CCL_FALSE, {0, 0, 0, 0}},//Stat/Ctrl1
    {CCL_FALSE, {0, 0, 0, 0}} //Stat/Ctrl2
};


static ccl_err_t _media_manufacturing_date_parse (b_u8 *buf, devman_media_mfg_date_t *pmfg_date)
{
    b_u8    fbuf[3];
    b_u8    *fptr;

    fptr = buf;
    memcpy (fbuf, fptr, 2);
    fbuf[2] = 0;
    pmfg_date->year = atoi (fbuf);

    fptr = buf + 2;
    memcpy (fbuf, fptr, 2);
    fbuf[2] = 0;
    pmfg_date->month = atoi (fbuf);

    fptr = buf + 4;
    memcpy (fbuf, fptr, 2);
    fbuf[2] = 0;
    pmfg_date->day = atoi (fbuf);

    fptr = buf + 6;
    memcpy (fbuf, fptr, 2);
    fbuf[2] = 0;
    pmfg_date->lot = atoi (fbuf);

    return CCL_OK;
}

static ccl_err_t _media_ddm_parameters_parse(b_u8 media_id, b_u8 buf, 
                                             devman_media_diag_type_t *pddm)
{
    b_bool *diag_map = NULL;

    switch (media_id) {
    case MEDIA_ID_SFP:
        *diag_map = sfp_ddm_media_diag_map;
        break;
    case MEDIA_ID_QSFP28:
        diag_map = qsfp28_ddm_media_diag_map;
        break;
    default:
        return CCL_NOT_DEFINED;
    }

    /*DdmSupported;        B6 on 92*/
    if (diag_map[MEDIA_DDM_DIAG_MAP_SUPPORTED] == CCL_TRUE) {
        pddm->ddm_supported.relevance = CCL_TRUE;

        if (buf & 0x40)
            pddm->ddm_supported.status = CCL_TRUE;
        else
            pddm->ddm_supported.status = CCL_FALSE;
    } else {
        pddm->ddm_supported.relevance = CCL_FALSE;
        pddm->ddm_supported.status = CCL_TRUE;     //Not defined, so assume that is default
    }

    /*InternCalibr;        B5 on 92*/
    if (diag_map[MEDIA_DDM_DIAG_MAP_INTERN_CLBR] == CCL_TRUE) {
        pddm->intern_calibr.relevance = CCL_TRUE;

        if (buf & 0x20)
            pddm->intern_calibr.status = CCL_TRUE;
        else
            pddm->intern_calibr.status = CCL_FALSE;
    } else {
        pddm->intern_calibr.relevance = CCL_FALSE;
        pddm->intern_calibr.status = CCL_FALSE;     //Not defined, so assume that is default
    }

    /*ExternCalibr;        B4 on 92*/
    if (diag_map[MEDIA_DDM_DIAG_MAP_EXTERN_CLBR] == CCL_TRUE) {
        pddm->extern_calibr.relevance = CCL_TRUE;

        if (buf & 0x10)
            pddm->extern_calibr.status = CCL_TRUE;
        else
            pddm->extern_calibr.status = CCL_FALSE;
    } else {
        pddm->extern_calibr.relevance = CCL_FALSE;
        pddm->extern_calibr.status = CCL_FALSE;     //Not defined, so assume that is default
    }

    /*AvgPowerMeasure;     B3 on 92*/
    if (diag_map[MEDIA_DDM_DIAG_MAP_AVG_PWR_MSRMT] == CCL_TRUE) {
        pddm->avg_power_measure.relevance = CCL_TRUE;

        if (buf & 0x08)
            pddm->avg_power_measure.status = CCL_TRUE;
        else
            pddm->avg_power_measure.status = CCL_FALSE;
    } else {
        pddm->avg_power_measure.relevance = CCL_FALSE;
        pddm->avg_power_measure.status = CCL_FALSE;     //Not defined, so assume that is default
    }

    /*AdrChngRequired;     B2 on 92*/
    if (diag_map[MEDIA_DDM_DIAG_MAP_AVG_CHNG_REQ] == CCL_TRUE) {
        pddm->adr_chng_required.relevance = CCL_TRUE;

        if (buf & 0x04)
            pddm->adr_chng_required.status = CCL_TRUE;
        else
            pddm->adr_chng_required.status = CCL_FALSE;
    } else {
        pddm->adr_chng_required.relevance = CCL_FALSE;
        pddm->adr_chng_required.status = CCL_FALSE;     //Not defined, so assume that is default
    }

    /*BerSupport;     B4 on 92*/
    if (diag_map[MEDIA_DDM_DIAG_MAP_BER_SUPPORT] == CCL_TRUE) {
        pddm->ber_support.relevance = CCL_TRUE;

        if (buf & 0x10)
            pddm->ber_support.status = CCL_TRUE;
        else
            pddm->ber_support.status = CCL_FALSE;
    } else {
        pddm->ber_support.relevance = CCL_FALSE;
        pddm->ber_support.status = CCL_FALSE;     //Not defined, so assume that is default
    }

    return CCL_OK;
}

ccl_err_t media_ddm_parameters_parse(b_u8 media_id, b_u8 buf, 
                                     devman_media_diag_type_t *pddm)
{
    _ret = _media_ddm_parameters_parse(media_id, buf, pddm);
    if (_ret) {
        PDEBUG("Error! media_ddm_parameters_parse\n");
        MEDIA_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

ccl_err_t media_data_layout_get(b_u8 media_id, 
                                devman_media_data_block_t **pmedia_data_layout)
{
    switch (media_id) {
    case MEDIA_ID_SFP:
        *pmedia_data_layout = sfp_block_def_table;
        break;
    case MEDIA_ID_QSFP28:
        *pmedia_data_layout = qsfp28_block_def_table;
        break;
    default:
        return CCL_NOT_DEFINED;
    }

    return CCL_OK;
}

ccl_err_t media_parameters_parse(b_u8 media_id, b_u8 *buf, 
                                 b_u32 start_offset, b_u32 buf_len, 
                                 devman_media_description_t *p_media_descr)
{
    b_u8    *pfield = buf;
    b_u8    field_byte = 0;
    b_u16   field_half = 0;

    if (start_offset == 0) {
        /*transceiver_type;     0-0 - Unknown, GIBIC, etc.*/
        pfield = &(buf[0]);
        switch (*pfield) {
        case 0x01:
            p_media_descr->transceiver_type = MEDIA_TRANSCEIVER_GBIC;
            break;
        case 0x02:
            p_media_descr->transceiver_type = MEDIA_TRANSCEIVER_SOLDERED;
            break;
        case 0x03:
            p_media_descr->transceiver_type = MEDIA_TRANSCEIVER_SFP;
            break;
        case 0x04:
            p_media_descr->transceiver_type = MEDIA_TRANSCEIVER_XBI;
            break;
        case 0x05:
            p_media_descr->transceiver_type = MEDIA_TRANSCEIVER_XENPAK;
            break;
        case 0x06:
            p_media_descr->transceiver_type = MEDIA_TRANSCEIVER_XFP;
            break;
        case 0x07:
            p_media_descr->transceiver_type = MEDIA_TRANSCEIVER_XFF;
            break;
        case 0x08:
            p_media_descr->transceiver_type = MEDIA_TRANSCEIVER_XFP_E;
            break;
        case 0x09:
            p_media_descr->transceiver_type = MEDIA_TRANSCEIVER_XPAK;
            break;
        case 0x0A:
            p_media_descr->transceiver_type = MEDIA_TRANSCEIVER_X2;
            break;
        case 0x0B:
            p_media_descr->transceiver_type = MEDIA_TRANSCEIVER_DWDM_SFP;
            break;
        case 0x0C:
            p_media_descr->transceiver_type = MEDIA_TRANSCEIVER_QSFP;
            break;
        default:
            p_media_descr->transceiver_type = MEDIA_TRANSCEIVER_UNKNOWN;
            break;
        }
    }

    if (start_offset + buf_len < 2)
        return CCL_OK;

    if (start_offset <= 1) {
        /*extended_type;     1-1 */
        pfield = &(buf[1 - start_offset]);
        switch (*pfield) {
        case 0x01:
            p_media_descr->extended_type = MEDIA_TRANSCEIVER_EX_MOD_DEF_1;
            break;
        case 0x02:
            p_media_descr->extended_type = MEDIA_TRANSCEIVER_EX_MOD_DEF_2;
            break;
        case 0x03:
            p_media_descr->extended_type = MEDIA_TRANSCEIVER_EX_MOD_DEF_3;
            break;
        case 0x04:
            p_media_descr->extended_type = MEDIA_TRANSCEIVER_EX_MOD_TWO_WIRE_ID;
            break;
        case 0x05:
            p_media_descr->extended_type = MEDIA_TRANSCEIVER_EX_MOD_DEF_5;
            break;
        case 0x06:
            p_media_descr->extended_type = MEDIA_TRANSCEIVER_EX_MOD_DEF_6;
            break;
        case 0x07:
            p_media_descr->extended_type = MEDIA_TRANSCEIVER_EX_MOD_DEF_7;
            break;
        default:
            p_media_descr->extended_type = MEDIA_TRANSCEIVER_EX_UNKNOWN;
            break;
        }
    }

    if (start_offset + buf_len < 3)
        return   CCL_OK;

    if (start_offset <= 2) {
        /*connector_type;	     2-2 - FiberJack, LC, MT-RJ, etc */
        pfield = &(buf[2 - start_offset]);
        switch (*pfield) {
        case 0x01:
            p_media_descr->connector_type = MEDIA_CONNECTOR_SC;
            break;
        case 0x02:
            p_media_descr->connector_type = MEDIA_CONNECTOR_FCS1_COPPER;
            break;
        case 0x03:
            p_media_descr->connector_type = MEDIA_CONNECTOR_FCS2_COPPER;
            break;
        case 0x04:
            p_media_descr->connector_type = MEDIA_CONNECTOR_BNC_TNC;
            break;
        case 0x05:
            p_media_descr->connector_type = MEDIA_CONNECTOR_FC_COAX;
            break;
        case 0x06:
            p_media_descr->connector_type = MEDIA_CONNECTOR_FIBER_JACK;
            break;
        case 0x07:
            p_media_descr->connector_type = MEDIA_CONNECTOR_LC;
            break;
        case 0x08:
            p_media_descr->connector_type = MEDIA_CONNECTOR_MT_RJ;
            break;
        case 0x09:
            p_media_descr->connector_type = MEDIA_CONNECTOR_MU;
            break;
        case 0x0A:
            p_media_descr->connector_type = MEDIA_CONNECTOR_SG;
            break;
        case 0x0B:
            p_media_descr->connector_type = MEDIA_CONNECTOR_OPTICAL_PIGTAIL;
            break;
        case 0x0C:
            p_media_descr->connector_type = MEDIA_CONNECTOR_MPO_PARALLEL_OPTIC;
            break;
        case 0x20:
            p_media_descr->connector_type = MEDIA_CONNECTOR_HSSDC;
            break;
        case 0x21:
            p_media_descr->connector_type = MEDIA_CONNECTOR_COPPER_PIGTAIL;
            break;
        case 0x22:
            p_media_descr->connector_type = MEDIA_CONNECTOR_RJ45;
            break;
        default:
            p_media_descr->connector_type = MEDIA_CONNECTOR_UNKNOWN;
            break;
        }
    }


    if (start_offset + buf_len < 4)
        return   CCL_OK;

    if (start_offset <= 3) {
        /*transceiver;      3-10 - Electric or optical compability */
        /*g10_compl;   3-3 B4-7 10GBASE-SR, 10GBASE-LR, 10GBASE-LRM*/
        pfield = &(buf[3 - start_offset]);
        field_byte = (*pfield & 0xF0) >> 4;
        switch (field_byte) {
        case 0x01:
            p_media_descr->transceiver.g10_compliance = MEDIA_TRNSVR_10G_SR;
            break;
        case 0x02:
            p_media_descr->transceiver.g10_compliance = MEDIA_TRNSVR_10G_LR;
            break;
        case 0x04:
            p_media_descr->transceiver.g10_compliance = MEDIA_TRNSVR_10G_LRM;
            break;
        default:
            p_media_descr->transceiver.g10_compliance = MEDIA_TRNSVR_10G_UNKNOWN;
            break;
        }

        /*inf_compl;  3-3 B0-B3 */
        pfield = &(buf[3 - start_offset]);
        field_byte = *pfield & 0x0F;
        switch (field_byte) {
        case 0x01:
            p_media_descr->transceiver.infiniband_compliance = MEDIA_TRNSVR_INFINIBAND_1X_COPPER_PASSIVE;
            break;
        case 0x02:
            p_media_descr->transceiver.infiniband_compliance = MEDIA_TRNSVR_INFINIBAND_1X_COPPER_ACTIVE;
            break;
        case 0x04:
            p_media_descr->transceiver.infiniband_compliance = MEDIA_TRNSVR_INFINIBAND_1X_LX;
            break;
        case 0x08:
            p_media_descr->transceiver.infiniband_compliance = MEDIA_TRNSVR_INFINIBAND_1X_SX;
            break;
        default:
            p_media_descr->transceiver.infiniband_compliance = MEDIA_TRNSVR_INFINIBAND_UNKNOWN;
            break;
        }
    }

    if (start_offset + buf_len < 5)
        return   CCL_OK;

    if (start_offset <= 4) {
        /*escon_compl;  4-4 B6-B7 */
        pfield = &(buf[4 - start_offset]);
        field_byte = (*pfield & 0xC0) >> 6;
        switch (field_byte) {
        case 0x01:
            p_media_descr->transceiver.escon_compliance = MEDIA_TRNSVR_ESCON_SMF;
            break;
        case 0x02:
            p_media_descr->transceiver.escon_compliance = MEDIA_TRNSVR_ESCON_MMF;
            break;
        default:
            p_media_descr->transceiver.escon_compliance = MEDIA_TRNSVR_ESCON_UNKNOWN;
            break;
        }

        if (buf_len < 6)
            return   CCL_OK;

        /*sonet_compl;  4(B0-B5)-5 */
        pfield = &(buf[5 - start_offset]);
        field_half = (b_u16)(*pfield);
        pfield = &(buf[4 - start_offset]);
        field_half |= ((b_u16)(*pfield & 0x3F)) << 8;
        switch (field_half) {
        case 0x0001:
            p_media_descr->transceiver.sonet_compliance = MEDIA_TRNSVR_SONET_OC3_SHORT;
            break;
        case 0x0002:
            p_media_descr->transceiver.sonet_compliance = MEDIA_TRNSVR_SONET_OC3_SM_INTER;
            break;
        case 0x0004:
            p_media_descr->transceiver.sonet_compliance = MEDIA_TRNSVR_SONET_OC3_SM_LONG;
            break;
        case 0x0010:
            p_media_descr->transceiver.sonet_compliance = MEDIA_TRNSVR_SONET_OC12_SHORT;
            break;
        case 0x0020:
            p_media_descr->transceiver.sonet_compliance = MEDIA_TRNSVR_SONET_OC12_SM_INTER;
            break;
        case 0x0040:
            p_media_descr->transceiver.sonet_compliance = MEDIA_TRNSVR_SONET_OC12_SM_LONG;
            break;
        case 0x0100:
            p_media_descr->transceiver.sonet_compliance = MEDIA_TRNSVR_SONET_OC48_SHORT;
            break;
        case 0x0200:
            p_media_descr->transceiver.sonet_compliance = MEDIA_TRNSVR_SONET_OC48_SM_INTER;
            break;
        case 0x0400:
            p_media_descr->transceiver.sonet_compliance = MEDIA_TRNSVR_SONET_OC48_SM_LONG;
            break;
        case 0x0800:
            p_media_descr->transceiver.sonet_compliance = MEDIA_TRNSVR_SONET_SPEC_BIT1;
            break;
        case 0x1000:
            p_media_descr->transceiver.sonet_compliance = MEDIA_TRNSVR_SONET_SPEC_BIT2;
            break;
        case 0x2000:
            p_media_descr->transceiver.sonet_compliance = MEDIA_TRNSVR_SONET_192_SHORT;
            break;
        case 0x1002:
            p_media_descr->transceiver.sonet_compliance = MEDIA_TRNSVR_SONET_OC3_INTER_SR1_IR1_LR1;
            break;
        default:
            p_media_descr->transceiver.sonet_compliance = MEDIA_TRNSVR_SONET_UNKNOWN;
            break;
        }
    }

    if (start_offset + buf_len < 7)
        return   CCL_OK;

    if (start_offset <= 6) {
        /*eth_compl;      6-6 - 1000BASE-T, 1000BASE-CX, etc. */
        pfield = &(buf[6 - start_offset]);
        switch (*pfield) {
        case 0x01:
            p_media_descr->transceiver.eth_compliance = MEDIA_TRNSVR_ETH_1000SX;
            break;
        case 0x02:
            p_media_descr->transceiver.eth_compliance = MEDIA_TRNSVR_ETH_1000LX;
            break;
        case 0x04:
            p_media_descr->transceiver.eth_compliance = MEDIA_TRNSVR_ETH_1000CX;
            break;
        case 0x08:
            p_media_descr->transceiver.eth_compliance = MEDIA_TRNSVR_ETH_1000T;
            break;
        case 0x10:
            p_media_descr->transceiver.eth_compliance = MEDIA_TRNSVR_ETH_100LX;
            break;
        case 0x20:
            p_media_descr->transceiver.eth_compliance = MEDIA_TRNSVR_ETH_100FX;
            break;
        case 0x40:
            p_media_descr->transceiver.eth_compliance = MEDIA_TRNSVR_ETH_BX10;
            break;
        case 0x80:
            p_media_descr->transceiver.eth_compliance = MEDIA_TRNSVR_ETH_PX;
            break;
        default:
            /*Unknown: is later configured according to length-speed*/
            p_media_descr->transceiver.eth_compliance = MEDIA_TRNSVR_ETH_UNKNOWN;
            break;
        }
    }

    if (start_offset + buf_len < 8)
        return   CCL_OK;

    if (start_offset <= 7) {
        /*fiber_channel_len;   7(B3-B7) */
        pfield = &(buf[7 - start_offset]);
        field_byte = (*pfield & 0xF8) >> 3;
        switch (field_byte) {
        case 0x01:
            p_media_descr->transceiver.fbr_chnl_len = MEDIA_TRNSVR_FCL_MEDIUM;
            break;
        case 0x02:
            p_media_descr->transceiver.fbr_chnl_len = MEDIA_TRNSVR_FCL_LONG;
            break;
        case 0x04:
            p_media_descr->transceiver.fbr_chnl_len = MEDIA_TRNSVR_FCL_INTERMEDIATE;
            break;
        case 0x08:
            p_media_descr->transceiver.fbr_chnl_len = MEDIA_TRNSVR_FCL_SHORT;
            break;
        case 0x10:
            p_media_descr->transceiver.fbr_chnl_len = MEDIA_TRNSVR_FCL_VERY_LONG;
            break;
        default:
            p_media_descr->transceiver.fbr_chnl_len = MEDIA_TRNSVR_FCL_UNKNOWN;
            break;
        }

        if (start_offset + buf_len < 9)
            return   CCL_OK;

        /*fiber_channel_tech; 7(B0-B2)-8 */
        pfield = &(buf[8 - start_offset]);
        field_half = (b_u16)(*pfield);
        pfield = &(buf[7 - start_offset]);
        field_half |= ((b_u16)(*pfield & 0x07)) << 8;
        switch (field_half) {
        case 0x0002:
            p_media_descr->transceiver.fbr_chnl_technology = MEDIA_TRNSVR_FCT_COPPER_FC_BASE_T;
            break;
        case 0x0004:
            p_media_descr->transceiver.fbr_chnl_technology = MEDIA_TRNSVR_FCT_COPPER_PASSIVE;
            break;
        case 0x0008:
            p_media_descr->transceiver.fbr_chnl_technology = MEDIA_TRNSVR_FCT_COPPER_ACTIVE;
            break;
        case 0x0010:
            p_media_descr->transceiver.fbr_chnl_technology = MEDIA_TRNSVR_FCT_LONGWAVE_LL;
            break;
        case 0x0020:
            p_media_descr->transceiver.fbr_chnl_technology = MEDIA_TRNSVR_FCT_SHORTWAVE_W_OFC_SL;
            break;
        case 0x0040:
            p_media_descr->transceiver.fbr_chnl_technology = MEDIA_TRNSVR_FCT_SHORTWAVE_WO_OFC_SN;
            break;
        case 0x0080:
            p_media_descr->transceiver.fbr_chnl_technology = MEDIA_TRNSVR_FCT_ELECTRIC_INTRA_ENCL_EL;
            break;
        case 0x0100:
            p_media_descr->transceiver.fbr_chnl_technology = MEDIA_TRNSVR_FCT_ELECTRIC_INTER_ENCL_EL;
            break;
        case 0x0200:
            p_media_descr->transceiver.fbr_chnl_technology = MEDIA_TRNSVR_FCT_LONGWAVE_LC;
            break;
        case 0x0400:
            p_media_descr->transceiver.fbr_chnl_technology = MEDIA_TRNSVR_FCT_SHORTWAVE_LINEAR_RX_SA;
            break;
        default:
            p_media_descr->transceiver.fbr_chnl_technology = MEDIA_TRNSVR_FCT_UNKNOWN;
            break;
        }
    }

    if (start_offset + buf_len < 10)
        return   CCL_OK;

    if (start_offset <= 9) {
        /*fiber_channel_media  ;  9 */
        pfield = &(buf[9 - start_offset]);
        switch (*pfield) {
        case 0x01:
            p_media_descr->transceiver.fbr_chnl_media = MEDIA_TRNSVR_FCM_SINGLE_SM;
            break;
        case 0x04:
            p_media_descr->transceiver.fbr_chnl_media = MEDIA_TRNSVR_FCM_MULTI_50UM_M5;
            break;
        case 0x08:
            p_media_descr->transceiver.fbr_chnl_media = MEDIA_TRNSVR_FCM_MULTI_62_5UM_M6;
            break;
        case 0x10:
            p_media_descr->transceiver.fbr_chnl_media = MEDIA_TRNSVR_FCM_VIDEO_COAX_TV;
            break;
        case 0x20:
            p_media_descr->transceiver.fbr_chnl_media = MEDIA_TRNSVR_FCM_MINI_COAX_MI;
            break;
        case 0x40:
            p_media_descr->transceiver.fbr_chnl_media = MEDIA_TRNSVR_FCM_TWIST_PAIR_TP;
            break;
        case 0x80:
            p_media_descr->transceiver.fbr_chnl_media = MEDIA_TRNSVR_FCM_TWIN_AX_PAIR_TW;
            break;
        default:
            p_media_descr->transceiver.fbr_chnl_media = MEDIA_TRNSVR_FCM_UNKNOWN;
            break;
        }
    }

    if (start_offset + buf_len < 11)
        return   CCL_OK;

    if (start_offset <= 10) {
        /*fiber_channel_speed;    10-10 */
        pfield = &(buf[10 - start_offset]);
        switch (*pfield) {
        case 0x01:
            p_media_descr->transceiver.fbr_chnl_speed = MEDIA_TRNSVR_FCS_100;
            break;
        case 0x04:
            p_media_descr->transceiver.fbr_chnl_speed = MEDIA_TRNSVR_FCS_200;
            break;
        case 0x10:
            p_media_descr->transceiver.fbr_chnl_speed = MEDIA_TRNSVR_FCS_400;
            break;
        case 0x20:
            p_media_descr->transceiver.fbr_chnl_speed = MEDIA_TRNSVR_FCS_1600;
            break;
        case 0x40:
            p_media_descr->transceiver.fbr_chnl_speed = MEDIA_TRNSVR_FCS_800;
            break;
        case 0x80:
            p_media_descr->transceiver.fbr_chnl_speed = MEDIA_TRNSVR_FCS_1200;
            break;
        default:
            p_media_descr->transceiver.fbr_chnl_speed = MEDIA_TRNSVR_FCS_UNKNOWN;
            break;
        }
    }


    if (start_offset + buf_len < 12)
        return   CCL_OK;

    if (start_offset <= 11) {
        /*Encoding;           11-11 */
        pfield = &(buf[11 - start_offset]);
        switch (*pfield) {
        case 0x01:
            p_media_descr->encoding = MEDIA_ENCODING_8B_10B;
            break;
        case 0x02:
            p_media_descr->encoding = MEDIA_ENCODING_4B_5B;
            break;
        case 0x03:
            p_media_descr->encoding = MEDIA_ENCODING_NRZ;
            break;
        case 0x04:
            p_media_descr->encoding = MEDIA_ENCODING_MANCHESTER;
            break;
        case 0x05:
            p_media_descr->encoding = MEDIA_ENCODING_SONET_SCRAMBLED;
            break;
        case 0x06:
            p_media_descr->encoding = MEDIA_ENCODING_64B_66B;
            break;
        default:
            p_media_descr->encoding = MEDIA_ENCODING_UNKNOWN;
            break;
        }
    }

    if (start_offset + buf_len < 13)
        return   CCL_OK;

    if (start_offset <= 12) {
        /*bit_rate_nom;          12-12 - Nominal signalling rate */
        pfield = &(buf[12 - start_offset]);
        p_media_descr->bit_rate_nom = *pfield * 100;  /* In MBd */
    }


    if (start_offset + buf_len < 14)
        return   CCL_OK;

    if (start_offset <= 13) {
        /*rate_id;             13-13 */
        pfield = &(buf[13 - start_offset]);
        switch (*pfield) {
        case 0x01:
            p_media_descr->rate_id = MEDIA_RATE_ID_4_2_1G__AS0_AS1;
            break;
        case 0x02:
            p_media_descr->rate_id = MEDIA_RATE_ID_8_4_2G_RX;
            break;
        case 0x03:
            p_media_descr->rate_id = MEDIA_RATE_ID_8_4_2G_RX_TX;
            break;
        case 0x04:
            p_media_descr->rate_id = MEDIA_RATE_ID_8_4_2G_TX;
            break;
        default:
            p_media_descr->rate_id = MEDIA_RATE_ID_UNKNOWN;
            break;
        }
    }

    if (start_offset + buf_len < 16)
        return   CCL_OK;

    if (start_offset <= 14) {
        /*LengthSmf;           14-15 Length for single mode fiber */
        pfield = &(buf[14 - start_offset]);
        p_media_descr->length_smf = *pfield * 1000;  /* kM-s in M-s */
        if (p_media_descr->length_smf == 0) {  // If Km-s specified, ignore duplicating 100m-s
            pfield = &(buf[15 - start_offset]);
            p_media_descr->length_smf = *pfield * 100;  /* Add 100M-s in M-s */
        }
    }


    if (start_offset + buf_len < 17)
        return   CCL_OK;

    if (start_offset <= 16) {
        /*Length50um;          16-16 Length for 50 */
        pfield = &(buf[16 - start_offset]);
        p_media_descr->length50um = *pfield * 10;  /* 10M-s in M-s */
    }


    if (start_offset + buf_len < 18)
        return   CCL_OK;

    if (start_offset <= 17) {
        /*Length62_5um;        17-17 Length for 62.5 um */
        pfield = &(buf[17 - start_offset]);
        p_media_descr->length62_5um = *pfield * 10;  /* 10M-s in M-s */
    }

    if (start_offset + buf_len < 19)
        return   CCL_OK;

    if (start_offset <= 18) {
        /*LengthCopper;        18-18 Length for copper */
        pfield = &(buf[18 - start_offset]);
        p_media_descr->length_copper = *pfield;  /* In M-s */
    }

    if (start_offset + buf_len < 20)
        return   CCL_OK;

    if (start_offset <= 19) {
        /*LengthOM3;           19-19 Length for 50 um OM3 fiber */
        pfield = &(buf[19 - start_offset]);
        p_media_descr->length_om3 = *pfield * 10;  /* 10M-s in M-s */
    }


    if (start_offset + buf_len < 36)
        return   CCL_OK;

    if (start_offset <= 20) {
        /*VendorName	     20-35 SFP vendor name (ASCII) */
        pfield = &(buf[20 - start_offset]);
        memcpy (p_media_descr->vendor_name, pfield, 16);
        p_media_descr->vendor_name[16] = 0;
    }


    if (start_offset + buf_len < 40)
        return   CCL_OK;

    if (start_offset <= 37) {
        /*VendorOui		 37-39 SFP vendor IEEE company ID */
        pfield = &(buf[37 - start_offset]);
        memcpy (p_media_descr->vendor_oui, pfield, 3);
        p_media_descr->vendor_oui[3] = 0;
    }


    if (start_offset + buf_len < 56)
        return   CCL_OK;

    if (start_offset <= 40) {
        /*VendorPn		 40-55 Part number provided by SFP vendor (ASCII) */
        pfield = &(buf[40 - start_offset]);
        memcpy (p_media_descr->vendor_pn, pfield, 16);
        p_media_descr->vendor_pn[16] = 0;
    }

    if (start_offset + buf_len < 60)
        return   CCL_OK;

    if (start_offset <= 56) {
        /*VendorRev		 56-59 Revision level for part number provided by vendor (ASCII) */
        pfield = &(buf[56 - start_offset]);
        memcpy (p_media_descr->vendor_rev, pfield, 4);
        p_media_descr->vendor_rev[4] = 0;
    }


    if (start_offset + buf_len < 67)
        return   CCL_OK;

    if (start_offset <= 66) {
        /*BitRateMax;          12 & 66-66 - Maximal signalling rate */
        pfield = &(buf[66 - start_offset]);
        p_media_descr->bit_rate_max = 
        (p_media_descr->bit_rate_nom / 100) * (*pfield);  /* In MBd from % */
    }


    if (start_offset + buf_len < 68)
        return   CCL_OK;

    if (start_offset <= 67) {
        /*BitRateMin;          12 & 67-67 - Minimal signalling rate */
        pfield = &(buf[67 - start_offset]);
        p_media_descr->bit_rate_min = 
        (p_media_descr->bit_rate_nom / 100) * (*pfield);  /* In MBd from % */
    }


    if (start_offset + buf_len < 84)
        return   CCL_OK;

    if (start_offset <= 68) {
        /*VendorSn		 68-83 Serial number provided by vendor (ASCII) */
        pfield = &(buf[68 - start_offset]);
        memcpy (p_media_descr->vendor_sn, pfield, 16);
        p_media_descr->vendor_sn[16] = 0;
    }

    if (start_offset + buf_len < 92)
        return   CCL_OK;

    if (start_offset <= 84) {
        /*ManDate;		     84-91 Vendor's manufactuping date */
        pfield = &(buf[84 - start_offset]);
        _media_manufacturing_date_parse(pfield, &(p_media_descr->mfg_date));
    }

    if (start_offset + buf_len < 93)
        return   CCL_OK;

    if (start_offset <= 92) {
        /*Diagnostic;		     92-92 Diagnostic parameters */
        pfield = &(buf[92 - start_offset]);
        _ret = _media_ddm_parameters_parse(media_id, *pfield,
                                           &(p_media_descr->diagnostic));
        if (_ret) {
            PDEBUG("Error! media_ddm_parameters_parse\n");
            MEDIA_ERROR_RETURN(CCL_FAIL);
        }
    }

    return CCL_OK;
}

static ccl_err_t _str_find_detail(item_description_t *items_list, b_i32 detail_idx,  
                                  char *detail_name, b_i32 name_len)
{
    b_i32 i;
    b_u8    *p_descr;

    strncpy (detail_name, "N/A", 4);

    for (i=0; ;i++) {
        /* List end */
        if (items_list[i].index == MEDIA_CONNECTOR_DETAIL_LIST_END)
            break;

        /* Item found */
        if (items_list[i].index == detail_idx) {
            p_descr = items_list[i].p_descr;
            strncpy(detail_name, p_descr, name_len);
            break;
        }
    }

    return CCL_OK;
}


static ccl_err_t _str_write_detail(b_i32 detail_value,  b_i8 *detail_name, b_i32 name_len,
                                   b_i8 *head_text, b_i8 *tail_text,
                                   b_i32 low_dflt_value, b_i8 *low_dflt_text, 
                                   b_i32 high_dflt_val, b_i8 *high_dflt_text)
{
    b_i32     remain_name_len = name_len;
    b_u8    *p_curr_name = detail_name;
    memset (detail_name, 0, name_len);

    /* Write header */
    if (head_text != NULL) {
        strncpy (p_curr_name, head_text, remain_name_len);
        remain_name_len -= strlen (head_text);
        p_curr_name += strlen (head_text);
    }

    if (low_dflt_text != NULL) {
        /* Write low default text */
        if (detail_value <= low_dflt_value) {
            strncpy (p_curr_name, low_dflt_text, remain_name_len);
            return CCL_OK;
        }
    }

    if (high_dflt_text != NULL) {
        /* Write high default text */
        if (detail_value >= high_dflt_val) {
            strncpy (p_curr_name, high_dflt_text, remain_name_len);
            return CCL_OK;
        }
    }

    /* write value */
    snprintf (p_curr_name, strlen(detail_name), "%d", detail_value);
    remain_name_len = name_len - strlen(detail_name);
    p_curr_name = detail_name + strlen(detail_name);

    if (tail_text != NULL)
        /*Write tail*/
        strncpy (p_curr_name, tail_text, remain_name_len);

    return CCL_OK;
}

static ccl_err_t _str_add_detail( b_u8 **pmedia_detail_buf, b_u8 *media_detail_str, 
                                  b_u8 *detail_title, line_location_t location_code)
{
    b_i16 last_char_index = 0; /* Index of the last character of the new media detail */

    /* Arguments validation */
    if (!pmedia_detail_buf || !(*pmedia_detail_buf) || 
        !media_detail_str || !detail_title) {
        PDEBUG("Error! Invalid input parameters\n");
        MEDIA_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* Remove the tailed spaces */
    last_char_index = strlen(media_detail_str) - 1; /* Index of the last chacter */
    for (; last_char_index >= 0; --last_char_index) {
        if (' ' != media_detail_str[last_char_index]) {
            break;
        }
        media_detail_str[last_char_index] = 0;
    }
    /* Add the datail title, data string and delimiter */
    switch (location_code) {
    case MEDIA_DETAILS_LOCAL_REGULAR: /* Regular location */
        sprintf(*pmedia_detail_buf, "%s %s ", detail_title, media_detail_str);
        break;
    case MEDIA_DETAILS_LOCAL_REGULAR_CONT: /* Regular location */
        sprintf(*pmedia_detail_buf, "%s %s, ", detail_title, media_detail_str);
        break;
    case MEDIA_DETAILS_LOCAL_LINE_LAST: /* Last in line location */
        sprintf(*pmedia_detail_buf, "%s %s,%s", detail_title, media_detail_str, MEDIA_DETAILS_NEW_LINE);
        break;
    case MEDIA_DETAILS_LOCAL_LAST: /* Last location */
        sprintf(*pmedia_detail_buf, "%s %s", detail_title, media_detail_str);
        break;
    default: /* Invalid lication code parameter */
        PDEBUG("Error! Invalid location code: %d\n", location_code);
        MEDIA_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* Update media detail buffer pob_i32er */
    *pmedia_detail_buf += strlen(*pmedia_detail_buf);

    return CCL_OK;
}

ccl_err_t media_params_description_build(devman_media_param_t *pmedia_param, 
                                         b_u8 *pmedia_descr, b_u32 size)
{
    b_u8    single_detail_buf[SINGLE_DETAIL_BUF_SIZE]; /* Buffer for one detail */
    b_bool  len_defined = CCL_FALSE;

    /* Arguments validation */
    if (!pmedia_param || !pmedia_descr) {
        PDEBUG("Error! Invalid input parameters\n");
        MEDIA_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* Variable initialization */
    memset(single_detail_buf, 0, SINGLE_DETAIL_BUF_SIZE);

    /* Get connector type string */
    _str_find_detail(connector_type_descr, 
                     pmedia_param->description.connector_type, 
                     single_detail_buf, SINGLE_DETAIL_BUF_SIZE);
    _ret = _str_add_detail(&pmedia_descr, single_detail_buf, 
                           MEDIA_DETAILS_TITLE_CON_TYPE, 
                           MEDIA_DETAILS_LOCAL_LINE_LAST);
    if (_ret) {
        PDEBUG("Error! _str_add_detail\n");
        MEDIA_ERROR_RETURN(CCL_FAIL);
    }

    /* Get vendor name */
    strncpy(single_detail_buf, pmedia_param->description.vendor_name, 
            SINGLE_DETAIL_BUF_SIZE);
    _ret = _str_add_detail(&pmedia_descr, single_detail_buf, 
                           MEDIA_DETAILS_TITLE_VEND_NAME, 
                           MEDIA_DETAILS_LOCAL_LINE_LAST);
    if (_ret) {
        PDEBUG("Error! _str_add_detail\n");
        MEDIA_ERROR_RETURN(CCL_FAIL);
    }

    /* Get vendor part number, new line */
    strncpy(single_detail_buf, pmedia_param->description.vendor_pn, 
            SINGLE_DETAIL_BUF_SIZE);
    _ret = _str_add_detail(&pmedia_descr, single_detail_buf, 
                           MEDIA_DETAILS_TITLE_PRT_NUM, 
                           MEDIA_DETAILS_LOCAL_REGULAR_CONT);
    if (_ret) {
        PDEBUG("Error! _str_add_detail\n");
        MEDIA_ERROR_RETURN(CCL_FAIL);
    }

    /* Get vendor revision */
    strncpy(single_detail_buf, pmedia_param->description.vendor_rev, 
            SINGLE_DETAIL_BUF_SIZE);
    _ret = _str_add_detail(&pmedia_descr, single_detail_buf, 
                           MEDIA_DETAILS_TITLE_VEND_REV, 
                           MEDIA_DETAILS_LOCAL_LINE_LAST);
    if (_ret) {
        PDEBUG("Error! _str_add_detail\n");
        MEDIA_ERROR_RETURN(CCL_FAIL);
    }

    /* Get link length */
    _ret = _str_add_detail(&pmedia_descr, "", 
                           "Transmitting Length:", 
                           MEDIA_DETAILS_LOCAL_REGULAR);
    if (_ret) {
        PDEBUG("Error! _str_add_detail\n");
        MEDIA_ERROR_RETURN(CCL_FAIL);
    }

    /* Get link length, single-mode 9 micron, Km units */
    if (pmedia_param->description.length_smf != 0) {
        _str_write_detail(pmedia_param->description.length_smf / LENGTH_KM_RATE, 
                          single_detail_buf, SINGLE_DETAIL_BUF_SIZE, 
                          NULL, LENGTH_KM_UNIT_STR,
                          LENGTH_NOT_SUPPORTED, FIELD_NOT_SUPPORTED, 
                          LENGTH_LONG * LENGTH_KM_RATE, LENGTH_KM_LONG_STR);
        if (len_defined != CCL_FALSE)
            _ret = _str_add_detail(&pmedia_descr, "", 
                                   "                    ", 
                                   MEDIA_DETAILS_LOCAL_REGULAR);
        _ret = _str_add_detail(&pmedia_descr, single_detail_buf, 
                               MEDIA_DETAILS_TITLE_LENG_9K, 
                               MEDIA_DETAILS_LOCAL_LINE_LAST);
        if (_ret) {
            PDEBUG("Error! _str_add_detail\n");
            MEDIA_ERROR_RETURN(CCL_FAIL);
        }
        len_defined = CCL_TRUE;
    }

    /* Get link length, multi-mode, extended bandwidth, 50 micron, meter units */
    if (pmedia_param->description.length_om3 != 0) {
        _str_write_detail(pmedia_param->description.length_om3, single_detail_buf, 
                          SINGLE_DETAIL_BUF_SIZE, NULL, LENGTH_M_UNIT_STR,
                          LENGTH_NOT_SUPPORTED, FIELD_NOT_SUPPORTED, 
                          LENGTH_LONG, LENGTH_10M_LONG_STR);
        if (len_defined != CCL_FALSE)
            _ret = _str_add_detail(&pmedia_descr, "", 
                                   "                    ", 
                                   MEDIA_DETAILS_LOCAL_REGULAR);
        _ret = _str_add_detail(&pmedia_descr, single_detail_buf, 
                               MEDIA_DETAILS_TITLE_LENG_EB_50M, 
                               MEDIA_DETAILS_LOCAL_LINE_LAST);
        if (_ret) {
            PDEBUG("Error! _str_add_detail\n");
            MEDIA_ERROR_RETURN(CCL_FAIL);
        }
        len_defined = CCL_TRUE;
    }

    /* Get link length, multi-mode 50 micron, meter units */
    if (pmedia_param->description.length50um != 0) {
        _str_write_detail(pmedia_param->description.length50um, single_detail_buf, 
                          SINGLE_DETAIL_BUF_SIZE, NULL, LENGTH_M_UNIT_STR,
                          LENGTH_NOT_SUPPORTED, FIELD_NOT_SUPPORTED, 
                          LENGTH_LONG * LENGTH_10M_RATE, LENGTH_10M_LONG_STR);
        if (len_defined != CCL_FALSE)
            _ret = _str_add_detail(&pmedia_descr, "", 
                                   "                    ", 
                                   MEDIA_DETAILS_LOCAL_REGULAR);
        _ret = _str_add_detail(&pmedia_descr, single_detail_buf, 
                               MEDIA_DETAILS_TITLE_LENG_50M, 
                               MEDIA_DETAILS_LOCAL_LINE_LAST);
        if (_ret) {
            PDEBUG("Error! _str_add_detail\n");
            MEDIA_ERROR_RETURN(CCL_FAIL);
        }
        len_defined = CCL_TRUE;
    }

    /* Get link length, multi-mode 62.5 micron, meter units */
    if (pmedia_param->description.length62_5um != 0) {
        _str_write_detail(pmedia_param->description.length62_5um, single_detail_buf, 
                          SINGLE_DETAIL_BUF_SIZE, NULL, LENGTH_M_UNIT_STR,
                          LENGTH_NOT_SUPPORTED, FIELD_NOT_SUPPORTED, 
                          LENGTH_LONG * LENGTH_10M_RATE, LENGTH_10M_LONG_STR);
        if (len_defined != CCL_FALSE)
            _ret = _str_add_detail(&pmedia_descr, "", 
                                   "                    ", 
                                   MEDIA_DETAILS_LOCAL_REGULAR);
        _ret = _str_add_detail(&pmedia_descr, single_detail_buf, 
                               MEDIA_DETAILS_TITLE_LENG_625M, 
                               MEDIA_DETAILS_LOCAL_LAST);
        if (_ret) {
            PDEBUG("Error! _str_add_detail\n");
            MEDIA_ERROR_RETURN(CCL_FAIL);
        }
        len_defined = CCL_TRUE;
    }

    /* Get link length, copper, meter units */
    if (pmedia_param->description.length_copper != 0) {
        _str_write_detail(pmedia_param->description.length_copper, single_detail_buf, 
                          SINGLE_DETAIL_BUF_SIZE, NULL, LENGTH_M_UNIT_STR,
                          LENGTH_NOT_SUPPORTED, FIELD_NOT_SUPPORTED, 
                          LENGTH_LONG, LENGTH_10M_LONG_STR);
        if (len_defined != CCL_FALSE)
            _ret = _str_add_detail(&pmedia_descr, "", 
                                   "                    ", 
                                   MEDIA_DETAILS_LOCAL_REGULAR);
        _ret = _str_add_detail(&pmedia_descr, single_detail_buf, 
                               MEDIA_DETAILS_TITLE_LENG_COPPER, 
                               MEDIA_DETAILS_LOCAL_LAST);
        if (_ret) {
            PDEBUG("Error! _str_add_detail\n");
            MEDIA_ERROR_RETURN(CCL_FAIL);
        }
        len_defined = CCL_TRUE;
    }

    if (len_defined == CCL_FALSE) {
        _ret = _str_add_detail(&pmedia_descr, "", 
                               "    Not defined", 
                               MEDIA_DETAILS_LOCAL_LINE_LAST);
        if (_ret) {
            PDEBUG("Error! _str_add_detail\n");
            MEDIA_ERROR_RETURN(CCL_FAIL);
        }
    }

    return CCL_OK;
}

/* Parse raw buffer with threshold of LMM diagnostic parameters b_i32o structure */
static ccl_err_t _media_dd_threshold_params_parse(b_u8 *buf, 
                                                  devman_media_diag_tresh_unit_t *pddm_thr)
{
    pddm_thr->applicable = CCL_TRUE;

    pddm_thr->high_alarm.modulo = buf[0];
    pddm_thr->high_alarm.fraction = buf[1];

    pddm_thr->low_alarm.modulo = buf[2];
    pddm_thr->low_alarm.fraction = buf[3];

    pddm_thr->high_warn.modulo = buf[4];
    pddm_thr->high_warn.fraction = buf[5];

    pddm_thr->low_warn.modulo = buf[6];
    pddm_thr->low_warn.fraction = buf[7];

    return CCL_OK;
}

/* Parse raw buffer with thresholds of LMM diagnostic parameters b_i32o structure */
static ccl_err_t _media_dd_thresholds_parse (b_u32 media_id, b_u8 *buf, 
                                             devman_media_diag_treshold_t *pddm_thr)
{
    ccl_err_t status = CCL_OK;
    b_i32     *ddm_thr_map = NULL;

    switch (media_id) {
    case MEDIA_PORT_TYPE_SFP:
        ddm_thr_map = sfp_ddm_thresholds_map;
    case MEDIA_PORT_TYPE_QSFP28:
        ddm_thr_map = qsfp28_ddm_thresholds_map;
        break;
    default:
        return CCL_NOT_DEFINED;
    }

    if (ddm_thr_map[MEDIA_DDM_THRESHOLD_MAP_TEMPERATURE] >= 0) {
        _ret = _media_dd_threshold_params_parse(&(buf[ddm_thr_map[MEDIA_DDM_THRESHOLD_MAP_TEMPERATURE]]),
                                                &(pddm_thr->temperature));
        if (status == CCL_OK)
            status = _ret;
    } else {
        memset (&(pddm_thr->temperature), 0, sizeof(devman_media_diag_tresh_unit_t));
        pddm_thr->temperature.applicable = CCL_FALSE;
    }

    if (ddm_thr_map[MEDIA_DDM_THRESHOLD_MAP_VOLTAGE] >= 0) {
        _ret = _media_dd_threshold_params_parse(&(buf[ddm_thr_map[MEDIA_DDM_THRESHOLD_MAP_VOLTAGE]]),
                                                &(pddm_thr->voltage));
        if (status == CCL_OK)
            status = _ret;
    } else {
        memset (&(pddm_thr->voltage), 0, sizeof(devman_media_diag_tresh_unit_t));
        pddm_thr->voltage.applicable = CCL_FALSE;
    }

    if (ddm_thr_map[MEDIA_DDM_THRESHOLD_MAP_BIAS] >= 0) {
        _ret = _media_dd_threshold_params_parse(&(buf[ddm_thr_map[MEDIA_DDM_THRESHOLD_MAP_BIAS]]),
                                                &(pddm_thr->bias));
        if (status == CCL_OK)
            status = _ret;
    } else {
        memset (&(pddm_thr->bias), 0, sizeof(devman_media_diag_tresh_unit_t));
        pddm_thr->bias.applicable = CCL_FALSE;
    }

    if (ddm_thr_map[MEDIA_DDM_THRESHOLD_MAP_TX_PWR] >= 0) {
        _ret = _media_dd_threshold_params_parse(&(buf[ddm_thr_map[MEDIA_DDM_THRESHOLD_MAP_TX_PWR]]),
                                                &(pddm_thr->tx_power));
        if (status == CCL_OK)
            _ret = status;
    } else {
        memset (&(pddm_thr->tx_power), 0, sizeof(devman_media_diag_tresh_unit_t));
        pddm_thr->tx_power.applicable = CCL_FALSE;
    }

    if (ddm_thr_map[MEDIA_DDM_THRESHOLD_MAP_RX_PWR] >= 0) {
        _ret = _media_dd_threshold_params_parse(&(buf[ddm_thr_map[MEDIA_DDM_THRESHOLD_MAP_RX_PWR]]),
                                                &(pddm_thr->rx_power));
        if (status == CCL_OK)
            status = _ret;
    } else {
        memset (&(pddm_thr->rx_power), 0, sizeof(devman_media_diag_tresh_unit_t));
        pddm_thr->rx_power.applicable = CCL_FALSE;
    }

    return status;
}

/* Parse raw buffer with array of doubles from calibration constants of LMM diagnostic parameters into structure */
static ccl_err_t _media_dd_calibration_array_parse(b_u8 *buf, double *arr)
{
    double     *pval = NULL;

    pval = (double *)&(buf[0]);
    arr[4] = *pval;

    pval = (double *)&(buf[4]);
    arr[3] = *pval;

    pval = (double *)&(buf[8]);
    arr[2] = *pval;

    pval = (double *)&(buf[12]);
    arr[1] = *pval;

    pval = (double *)&(buf[16]);
    arr[0] = *pval;

    return CCL_OK;
}

/* Parse raw buffer with parameter item from calibration constants of LMM diagnostic parameters into structure */
static ccl_err_t _media_dd_calibration_item_parse (b_u8 *buf, devman_media_diag_calib_item_t *pitem)
{
    pitem->slope.modulo = buf[0];
    pitem->slope.fraction = buf[1];

    pitem->offset.modulo = buf[2];
    pitem->offset.fraction = buf[3];

    return CCL_OK;
}

/* Parse raw buffer with calibration constants of LMM diagnostic parameters into structure */
static ccl_err_t _media_dd_calibration_parse (b_u32 media_id, b_u8 *buf,
                                              devman_media_diag_calibration_t *pcalib)
{
    ccl_err_t status = CCL_OK;
    b_i32     *ddm_thr_map = NULL;
    char      *block_buf = NULL;

    switch (media_id) {
    case MEDIA_PORT_TYPE_SFP:
        ddm_thr_map = sfp_ddm_thresholds_map;
    case MEDIA_PORT_TYPE_QSFP28:
        ddm_thr_map = qsfp28_ddm_thresholds_map;
        break;
    default:
        return CCL_NOT_DEFINED;
    }

    if (ddm_thr_map[MEDIA_DDM_THRESHOLD_MAP_CALIBRATION] >= 0) {
        pcalib->applicable = CCL_TRUE;
        block_buf = buf + ddm_thr_map[MEDIA_DDM_THRESHOLD_MAP_CALIBRATION];

        _ret = _media_dd_calibration_array_parse (&(block_buf[0]), pcalib->rx_power);
        if (status == CCL_OK)
            status = _ret;

        _ret = _media_dd_calibration_item_parse (&(block_buf[20]), &(pcalib->txi));
        if (status == CCL_OK)
            status = _ret;

        _ret = _media_dd_calibration_item_parse (&(block_buf[24]), &(pcalib->tx_power));
        if (status == CCL_OK)
            status = _ret;

        _ret = _media_dd_calibration_item_parse (&(block_buf[28]), &(pcalib->t));
        if (status == CCL_OK)
            status = _ret;

        _ret = _media_dd_calibration_item_parse (&(block_buf[32]), &(pcalib->v));
        if (status == CCL_OK)
            status = _ret;
    } else {
        memset (pcalib, 0, sizeof(devman_media_diag_calibration_t));
        pcalib->applicable = CCL_FALSE;
    }

    return status;
}

/* Parse raw buffer with LMM diagnostic parameters into structure */
ccl_err_t media_digital_diagnostic_params_parse (b_u8 media_id, IN char *buf, 
                                                 devman_media_diag_param_t *pddm)
{
    ccl_err_t status = CCL_OK;

    _ret = _media_dd_thresholds_parse(media_id, &(buf[0]), &(pddm->threshold));
    if (status == CCL_OK)
        status = _ret;

    _ret = _media_dd_calibration_parse(media_id, &(buf[0]), &(pddm->calibration));
    if (status == CCL_OK)
        status = _ret;

    return status;
}

/* Parse raw buffer with measurements of LMM diagnostic parameters into structure */
ccl_err_t media_digital_diagnostic_status(b_u8 media_id, b_u32 channel_id, b_u8 *buf, 
                                          devman_media_diag_status_t *pdiag_status)
{
    ddm_status_map_type_t *ddm_status_map = NULL;

    switch (media_id) {
    case MEDIA_PORT_TYPE_SFP:
        ddm_status_map = sfp_ddm_status_map;
    case MEDIA_PORT_TYPE_QSFP28:
        ddm_status_map = qsfp28_ddm_status_map;
        break;
    default:
        return CCL_NOT_DEFINED;
    }

    if (ddm_status_map[MEDIA_DDM_STATUS_MAP_TEMPERATURE].relevance == CCL_TRUE) {
        pdiag_status->temperature.modulo = 
        buf[ddm_status_map[MEDIA_DDM_STATUS_MAP_TEMPERATURE].offset[channel_id]];
        pdiag_status->temperature.fraction = 
        buf[ddm_status_map[MEDIA_DDM_STATUS_MAP_TEMPERATURE].offset[channel_id] + 1];
    }

    if (ddm_status_map[MEDIA_DDM_STATUS_MAP_VOLTAGE].relevance == CCL_TRUE) {
        pdiag_status->vcc.modulo = 
        buf[ddm_status_map[MEDIA_DDM_STATUS_MAP_VOLTAGE].offset[channel_id]];
        pdiag_status->vcc.fraction = 
        buf[ddm_status_map[MEDIA_DDM_STATUS_MAP_VOLTAGE].offset[channel_id] + 1];
    }

    if (ddm_status_map[MEDIA_DDM_STATUS_MAP_BIAS].relevance == CCL_TRUE) {
        pdiag_status->tx_bias.modulo = 
        buf[ddm_status_map[MEDIA_DDM_STATUS_MAP_BIAS].offset[channel_id]];
        pdiag_status->tx_bias.fraction = 
        buf[ddm_status_map[MEDIA_DDM_STATUS_MAP_BIAS].offset[channel_id] + 1];
    }

    if (ddm_status_map[MEDIA_DDM_STATUS_MAP_TX_PWR].relevance == CCL_TRUE) {
        pdiag_status->tx_power.modulo = 
        buf[ddm_status_map[MEDIA_DDM_STATUS_MAP_TX_PWR].offset[channel_id]];
        pdiag_status->tx_power.fraction = 
        buf[ddm_status_map[MEDIA_DDM_STATUS_MAP_TX_PWR].offset[channel_id] + 1];
    }

    if (ddm_status_map[MEDIA_DDM_STATUS_MAP_RX_PWR].relevance == CCL_TRUE) {
        pdiag_status->rx_power.modulo = 
        buf[ddm_status_map[MEDIA_DDM_STATUS_MAP_RX_PWR].offset[channel_id]];
        pdiag_status->rx_power.fraction = 
        buf[ddm_status_map[MEDIA_DDM_STATUS_MAP_RX_PWR].offset[channel_id] + 1];
    }

    if (ddm_status_map[MEDIA_DDM_STATUS_MAP_AUX1].relevance == CCL_TRUE) {
        pdiag_status->aux1.modulo = 
        buf[ddm_status_map[MEDIA_DDM_STATUS_MAP_AUX1].offset[channel_id]];
        pdiag_status->aux1.fraction = 
        buf[ddm_status_map[MEDIA_DDM_STATUS_MAP_AUX1].offset[channel_id] + 1];
    }

    if (ddm_status_map[MEDIA_DDM_STATUS_MAP_AUX2].relevance == CCL_TRUE) {
        pdiag_status->aux2.modulo = 
        buf[ddm_status_map[MEDIA_DDM_STATUS_MAP_AUX2].offset[channel_id]];
        pdiag_status->aux2.fraction = 
        buf[ddm_status_map[MEDIA_DDM_STATUS_MAP_AUX2].offset[channel_id] + 1];
    }

    if (ddm_status_map[MEDIA_DDM_STATUS_MAP_STAT_CTL1].relevance == CCL_TRUE) {
        if (buf[ddm_status_map[MEDIA_DDM_STATUS_MAP_STAT_CTL1].offset[channel_id]] & 0x1)
            pdiag_status->data_ready_bar = CCL_TRUE;
        else
            pdiag_status->data_ready_bar = CCL_FALSE;

        if (buf[ddm_status_map[MEDIA_DDM_STATUS_MAP_STAT_CTL1].offset[channel_id]] & 0x2)
            pdiag_status->los = CCL_TRUE;
        else
            pdiag_status->los = CCL_FALSE;

        if (buf[ddm_status_map[MEDIA_DDM_STATUS_MAP_STAT_CTL1].offset[channel_id]] & 0x4)
            pdiag_status->tx_fault = CCL_TRUE;
        else
            pdiag_status->tx_fault = CCL_FALSE;

        if (buf[ddm_status_map[MEDIA_DDM_STATUS_MAP_STAT_CTL1].offset[channel_id]] & 0x8)
            pdiag_status->soft_rate_select = CCL_TRUE;
        else
            pdiag_status->soft_rate_select = CCL_FALSE;

        if (buf[ddm_status_map[MEDIA_DDM_STATUS_MAP_STAT_CTL1].offset[channel_id]] & 0x10)
            pdiag_status->rate_select = CCL_TRUE;
        else
            pdiag_status->rate_select = CCL_FALSE;

        if (buf[ddm_status_map[MEDIA_DDM_STATUS_MAP_STAT_CTL1].offset[channel_id]] & 0x40)
            pdiag_status->soft_tx_disable = CCL_TRUE;
        else
            pdiag_status->soft_tx_disable = CCL_FALSE;

        if (buf[ddm_status_map[MEDIA_DDM_STATUS_MAP_STAT_CTL1].offset[channel_id]] & 0x80)
            pdiag_status->tx_disable = CCL_TRUE;
        else
            pdiag_status->tx_disable = CCL_FALSE;
    }

    return CCL_OK;
}

ccl_err_t media_link_type_get(devman_media_description_t *pdescr, 
                              devman_media_link_type_t *plink_type)
{
    b_i32                     category = -1;             
    b_i32                     iPorts = 0;                     
    b_bool                    singlemode = CCL_TRUE;     
    b_i32                     g10_compl = 0;            
    b_i32                     inf_compl = 0;                   
    b_i32                     escon_compl = 0;            
    b_i32                     sonet_compl = 0;            
    b_i32                     eth_compl = 0;         
    b_i32                     fiber_channel_len = 0;    
    b_i32                     fiber_channel_tech = 0;     
    b_i32                     fiber_channel_media = 0; 
    b_i32                     fiber_channel_speed = 0; 
    devman_media_link_type_t  linktype = MEDIA_IF_TYPE_UNKNOWN;
    b_i32                     len;


    singlemode  = !(pdescr->length50um | pdescr->length62_5um);

    len = pdescr->length_smf / 1000;
    if (singlemode) {
        if ((len  >   0) &&  (len <= 10))
            category = MEDIA_XFP_TYPE_CATEGORY_10GBASE_LX;
        else if ((len  >  10) &&  (len <= 40))
            category = MEDIA_XFP_TYPE_CATEGORY_10GBASE_XD;
        else if ((len  >  40) &&  (len <= 80))
            category = MEDIA_XFP_TYPE_CATEGORY_10GBASE_ZX;
        else if ((len  >  80) &&  (len <=120))
            category = MEDIA_XFP_TYPE_CATEGORY_10GBASE_EX;
        else if ((len  >  120) && (len <=180))
            category = MEDIA_XFP_TYPE_CATEGORY_10GBASE_XWDM;
        else
            category = MEDIA_XFP_TYPE_CATEGORY_10GBASE_UNKNOWN; 
    } else
        category = MEDIA_XFP_TYPE_CATEGORY_10GBASE_SX;


    g10_compl = (b_i32)pdescr->transceiver.g10_compliance;                     /* 3-3 B4-7 10GBASE-SR, 10GBASE-LR, 10GBASE-LRM*/
    inf_compl = (b_i32)pdescr->transceiver.infiniband_compliance;              /* 3-3 B0-B3 */
    escon_compl = (b_i32)pdescr->transceiver.escon_compliance;                 /* 4-4 B6-B7 */
    sonet_compl = (b_i32)pdescr->transceiver.sonet_compliance;                 /* 4(B0-B5)-5 */
    eth_compl = (b_i32)pdescr->transceiver.eth_compliance;                     /* 6-6 - 1000BASE-T, 1000BASE-CX, etc. */
    fiber_channel_len = (b_i32)pdescr->transceiver.fbr_chnl_len;               /* 7(B3-B7) */
    fiber_channel_tech = (b_i32)pdescr->transceiver.fbr_chnl_technology;       /* 7(B0-B2)-8 */
    fiber_channel_media = (b_i32)pdescr->transceiver.fbr_chnl_media;           /* 9 */
    fiber_channel_speed = (b_i32)pdescr->transceiver.fbr_chnl_speed;           /* 10-10 */

    switch ((b_i32)pdescr->transceiver_type) {
    case MEDIA_TRANSCEIVER_SFP:
        {
            switch ((b_i32)eth_compl) {
            case MEDIA_TRNSVR_ETH_BX10:
                if (pdescr->bit_rate_nom < 1000) {
                    if (singlemode) 
                        linktype = MEDIA_IF_TYPE_100BASE_BX_SM_SFP;
                    else 
                        linktype = MEDIA_IF_TYPE_100BASE_BX_MM_SFP;
                } else {
                    if (singlemode)
                        linktype = MEDIA_IF_TYPE_1000BASE_BX_SM_SFP;
                    else 
                        linktype = MEDIA_IF_TYPE_1000BASE_BX_MM_SFP;
                }
                break;
            case MEDIA_TRNSVR_ETH_PX:
                if (pdescr->bit_rate_nom < 1000) {
                    if (singlemode) 
                        linktype = MEDIA_IF_TYPE_100BASE_PX_SM_SFP;
                    else 
                        linktype = MEDIA_IF_TYPE_100BASE_PX_MM_SFP;
                } else {
                    if (singlemode) 
                        linktype = MEDIA_IF_TYPE_1000BASE_PX_SM_SFP;
                    else 
                        linktype = MEDIA_IF_TYPE_1000BASE_PX_MM_SFP;
                }
                break;
            case MEDIA_TRNSVR_ETH_1000CX:
                if (singlemode) {
                    if ((pdescr->bit_rate_nom >= 10000) && 
                        (pdescr->transceiver.infiniband_compliance == 
                                  MEDIA_TRNSVR_INFINIBAND_1X_COPPER_PASSIVE)) 
                        /*case this is GPP=PC192-3001C*/
                        linktype = MEDIA_IF_TYPE_10G_BASE_SX_SM_SFP_PLUS;
                    else 
                        linktype = MEDIA_IF_TYPE_1000BASE_CX_SM_SFP;
                } else 
                    linktype = MEDIA_IF_TYPE_1000BASE_CX_MM_SFP;
                break;
            case MEDIA_TRNSVR_ETH_1000SX:
                /* EEPROM does not contain complete data about SFP  */
                /* The link type detected on the Single/Multi Mode and Length category */
                if ( pdescr->bit_rate_nom >= 10000) {
                    switch (category) {
                    /*SFP Plus temp solution (should be recognized as SFP plus before)*/ 
                    case MEDIA_XFP_TYPE_CATEGORY_10GBASE_SX: 
                        linktype = (singlemode) ? MEDIA_IF_TYPE_10G_BASE_SX_SM_SFP_PLUS :
                                                  MEDIA_IF_TYPE_10G_BASE_SX_MM_SFP_PLUS; 
                        break;
                    case MEDIA_XFP_TYPE_CATEGORY_10GBASE_LX: 
                        linktype = (singlemode) ? MEDIA_IF_TYPE_10G_BASE_LX_SM_SFP_PLUS : 
                                                  MEDIA_IF_TYPE_10G_BASE_LX_SM_SFP_PLUS; 
                        break;
                    case MEDIA_XFP_TYPE_CATEGORY_10GBASE_ZX: 
                        linktype = (singlemode) ? MEDIA_IF_TYPE_10G_BASE_ZX_SM_SFP_PLUS : 
                                                  MEDIA_IF_TYPE_10G_BASE_ZX_MM_SFP_PLUS; 
                        break;
                    case MEDIA_XFP_TYPE_CATEGORY_10GBASE_EX: 
                        linktype = (singlemode) ? MEDIA_IF_TYPE_10G_BASE_EX_SM_XFP : 
                                                  MEDIA_IF_TYPE_10G_BASE_EX_MM_XFP; 
                        break;
                    case MEDIA_XFP_TYPE_CATEGORY_10GBASE_XD: 
                        linktype = (singlemode) ? MEDIA_IF_TYPE_10G_BASE_XD_SM_SFP_PLUS : 
                                                  MEDIA_IF_TYPE_10G_BASE_XD_MM_SFP_PLUS; 
                        break;
                    case MEDIA_XFP_TYPE_CATEGORY_10GBASE_XWDM: 
                        linktype = (singlemode) ? MEDIA_IF_TYPE_1000BASE_XWDM_SM_SFP : 
                                                  MEDIA_IF_TYPE_1000BASE_XWDM_MM_SFP; 
                        break;
                    default:
                        PDEBUG("link type %d can not be detected ", linktype);
                        linktype = MEDIA_IF_TYPE_UNKNOWN;
                    }
                    break;
                }
                if (singlemode) 
                    linktype = MEDIA_IF_TYPE_1000BASE_SX_SM_SFP;
                else 
                    linktype = MEDIA_IF_TYPE_1000BASE_SX_MM_SFP;
                break;
            case MEDIA_TRNSVR_ETH_1000LX:
                if (pdescr->length_smf < 40000) {    // Short distance
                    if (singlemode) 
                        linktype = MEDIA_IF_TYPE_1000BASE_LX_SM_SFP;
                    else 
                        linktype = MEDIA_IF_TYPE_1000BASE_LX_MM_SFP;
                } else if (pdescr->length_smf < 80000) {    // Mid distance
                    if (singlemode) 
                        linktype = MEDIA_IF_TYPE_1000BASE_EX_SM_SFP;
                    else 
                        linktype = MEDIA_IF_TYPE_1000BASE_EX_MM_SFP;
                } else {    // Long distance
                    if (singlemode) 
                        linktype = MEDIA_IF_TYPE_1000BASE_ZX_SM_SFP;
                    else 
                        linktype = MEDIA_IF_TYPE_1000BASE_ZX_MM_SFP;
                }
                break;
            case MEDIA_TRNSVR_ETH_100LX:
                if (singlemode) 
                    linktype = MEDIA_IF_TYPE_100BASE_LX_SM_SFP;
                else 
                    linktype = MEDIA_IF_TYPE_100BASE_LX_MM_SFP;
                break;
            case MEDIA_TRNSVR_ETH_100FX:
                if (singlemode) 
                    linktype = MEDIA_IF_TYPE_100BASE_FX_SM_SFP;
                else 
                    linktype = MEDIA_IF_TYPE_100BASE_FX_MM_SFP;
                break;
            case MEDIA_TRNSVR_ETH_1000T:
                //TODO: add support for sfp copper
                *plink_type = MEDIA_IF_TYPE_UNKNOWN;
                PDEBUG("sfp copper is not supported ");
                MEDIA_ERROR_RETURN(CCL_NOT_SUPPORT);
            default:
                /* EEPROM does not contain complete data about SFP  */
                /* The link type detected on the Single/Multi Mode and Length category */
                if ( pdescr->bit_rate_nom >= 10000) {
                    switch (category) {  /*10Gig*/
                    /*SFP Plus temp solution (should be recognized as SFP plus before)*/ 
                    case MEDIA_XFP_TYPE_CATEGORY_10GBASE_SX: 
                        linktype = (singlemode) ? MEDIA_IF_TYPE_10G_BASE_SX_SM_SFP_PLUS : 
                                                  MEDIA_IF_TYPE_10G_BASE_SX_MM_SFP_PLUS; 
                        break;
                    case MEDIA_XFP_TYPE_CATEGORY_10GBASE_LX: 
                        linktype = (singlemode) ? MEDIA_IF_TYPE_10G_BASE_LX_SM_SFP_PLUS : 
                                                  MEDIA_IF_TYPE_10G_BASE_LX_SM_SFP_PLUS; 
                        break;
                    case MEDIA_XFP_TYPE_CATEGORY_10GBASE_ZX: 
                        linktype = (singlemode) ? MEDIA_IF_TYPE_10G_BASE_ZX_SM_SFP_PLUS : 
                                                  MEDIA_IF_TYPE_10G_BASE_ZX_MM_SFP_PLUS; 
                        break;
                    case MEDIA_XFP_TYPE_CATEGORY_10GBASE_EX: 
                        linktype = (singlemode) ? MEDIA_IF_TYPE_10G_BASE_EX_SM_XFP : 
                                                  MEDIA_IF_TYPE_10G_BASE_EX_MM_XFP; 
                        break;
                    case MEDIA_XFP_TYPE_CATEGORY_10GBASE_XD: 
                        linktype = (singlemode) ? MEDIA_IF_TYPE_10G_BASE_XD_SM_SFP_PLUS : 
                                                  MEDIA_IF_TYPE_10G_BASE_XD_MM_SFP_PLUS; 
                        break;
                    default:
                        PDEBUG("link type %d can not be detected ", linktype);
                        linktype = MEDIA_IF_TYPE_UNKNOWN;
                    }
                } else if (pdescr->bit_rate_nom >= 1000) {   /*1 Gig*/
                    switch (category) {
                    /*SFP Plus temp solution (should be recognized as SFP plus before)*/ 
                    case MEDIA_XFP_TYPE_CATEGORY_10GBASE_SX: 
                        linktype = (singlemode) ? MEDIA_IF_TYPE_1000BASE_SX_SM_SFP : 
                                                  MEDIA_IF_TYPE_1000BASE_SX_MM_SFP; 
                        break;
                    case MEDIA_XFP_TYPE_CATEGORY_10GBASE_LX: 
                        linktype = (singlemode) ? MEDIA_IF_TYPE_1000BASE_LX_SM_SFP : 
                                                  MEDIA_IF_TYPE_1000BASE_LX_SM_SFP; 
                        break;
                    case MEDIA_XFP_TYPE_CATEGORY_10GBASE_XD: 
                        linktype = (singlemode) ? MEDIA_IF_TYPE_1000BASE_XD_SM_SFP : 
                                                  MEDIA_IF_TYPE_1000BASE_XD_MM_SFP; 
                        break;
                    case MEDIA_XFP_TYPE_CATEGORY_10GBASE_ZX: 
                        linktype = (singlemode) ? MEDIA_IF_TYPE_1000BASE_ZX_SM_SFP : 
                                                  MEDIA_IF_TYPE_1000BASE_ZX_MM_SFP; 
                        break;
                    case MEDIA_XFP_TYPE_CATEGORY_10GBASE_EX: 
                        linktype = (singlemode) ? MEDIA_IF_TYPE_1000BASE_EX_SM_SFP : 
                                                  MEDIA_IF_TYPE_1000BASE_EX_MM_SFP; 
                        break;
                    case MEDIA_XFP_TYPE_CATEGORY_10GBASE_XWDM: 
                        linktype = (singlemode) ? MEDIA_IF_TYPE_1000BASE_XWDM_SM_SFP : 
                                                  MEDIA_IF_TYPE_1000BASE_XWDM_MM_SFP; 
                        break;
                    default:
                        PDEBUG("link type %d can not be detected ", linktype);
                        linktype = MEDIA_IF_TYPE_UNKNOWN;
                    }
                } else {
                    switch (category) {
                    case MEDIA_XFP_TYPE_CATEGORY_10GBASE_SX: 
                        linktype = (singlemode) ? MEDIA_IF_TYPE_100BASE_SX_SM_SFP : 
                                                  MEDIA_IF_TYPE_100BASE_SX_MM_SFP; 
                        break;
                    case MEDIA_XFP_TYPE_CATEGORY_10GBASE_LX: 
                        linktype = (singlemode) ? MEDIA_IF_TYPE_100BASE_LX_SM_SFP : 
                                                  MEDIA_IF_TYPE_100BASE_LX_MM_SFP; 
                        break;
                    case MEDIA_XFP_TYPE_CATEGORY_10GBASE_XD: 
                        linktype = (singlemode) ? MEDIA_IF_TYPE_100BASE_XD_SM_SFP : 
                                                  MEDIA_IF_TYPE_100BASE_XD_MM_SFP; 
                        break;
                    case MEDIA_XFP_TYPE_CATEGORY_10GBASE_ZX: 
                        linktype = (singlemode) ? MEDIA_IF_TYPE_100BASE_ZX_SM_SFP : 
                                                  MEDIA_IF_TYPE_100BASE_ZX_MM_SFP; 
                        break;
                    case MEDIA_XFP_TYPE_CATEGORY_10GBASE_EX: 
                        linktype = (singlemode) ? MEDIA_IF_TYPE_100BASE_EX_SM_SFP : 
                                                  MEDIA_IF_TYPE_100BASE_EX_MM_SFP; 
                        break;
                    case MEDIA_XFP_TYPE_CATEGORY_10GBASE_XWDM: 
                        linktype = (singlemode) ? MEDIA_IF_TYPE_100BASE_XWDM_SM_SFP : 
                                                  MEDIA_IF_TYPE_100BASE_XWDM_MM_SFP; 
                        break;
                    default:
                        PDEBUG("link type %d can not be detected ", linktype);
                        linktype = MEDIA_IF_TYPE_UNKNOWN;
                    }
                }
                PDEBUG("\n\rSFP: eth_compl=%d g10_compl=%d linktype=%d",eth_compl,g10_compl,linktype);
                break;
            }
            break;
        } 

    case MEDIA_TRANSCEIVER_DWDM_SFP:
        if (pdescr->bit_rate_nom >= 10000) {
            if (singlemode) 
                linktype = MEDIA_IF_TYPE_10G_BASE_DWDM_SM_SFP_PLUS;
            else 
                linktype = MEDIA_IF_TYPE_10G_BASE_DWDM_MM_SFP_PLUS;
        } else {
            if (singlemode) 
                linktype = MEDIA_IF_TYPE_1000BASE_DWDM_SM_SFP;
            else 
                linktype = MEDIA_IF_TYPE_1000BASE_DWDM_MM_SFP;
        }
        break;
    case MEDIA_TRANSCEIVER_XFP:
            switch ((b_i32)g10_compl) {
            case MEDIA_TRNSVR_10G_SR:
                if (singlemode) 
                    linktype = MEDIA_IF_TYPE_10G_BASE_SR_SM_XFP;
                else 
                    linktype = MEDIA_IF_TYPE_10G_BASE_SR_MM_XFP;
                break;
            case MEDIA_TRNSVR_10G_LR:
                if (singlemode) 
                    linktype = MEDIA_IF_TYPE_10G_BASE_LR_SM_XFP;
                else 
                    linktype = MEDIA_IF_TYPE_10G_BASE_LR_MM_XFP;
                break;
            case MEDIA_TRNSVR_10G_LRM:
                if (singlemode) 
                    linktype = MEDIA_IF_TYPE_10G_BASE_LRM_SM_XFP;
                else 
                    linktype = MEDIA_IF_TYPE_10G_BASE_LRM_MM_XFP;
                break;
            default:
                /* EEPROM does not contain complete data about XFP  */
                /* The link type detected on the Single/Multi Mode and Length category */
                switch (category) {
                case MEDIA_XFP_TYPE_CATEGORY_10GBASE_SX: 
                    linktype = (singlemode) ? MEDIA_IF_TYPE_10G_BASE_SX_SM_XFP : 
                                              MEDIA_IF_TYPE_10G_BASE_SX_MM_XFP; 
                    break;
                case MEDIA_XFP_TYPE_CATEGORY_10GBASE_LX: 
                    linktype = (singlemode) ? MEDIA_IF_TYPE_10G_BASE_LX_SM_XFP: 
                                              MEDIA_IF_TYPE_10G_BASE_LX_MM_XFP; 
                    break;
                case MEDIA_XFP_TYPE_CATEGORY_10GBASE_XD: 
                    linktype = (singlemode) ? MEDIA_IF_TYPE_10G_BASE_XD_SM_XFP : 
                                              MEDIA_IF_TYPE_10G_BASE_XD_MM_XFP; 
                    break;
                case MEDIA_XFP_TYPE_CATEGORY_10GBASE_ZX: 
                    linktype = (singlemode) ? MEDIA_IF_TYPE_10G_BASE_ZX_SM_XFP : 
                                              MEDIA_IF_TYPE_10G_BASE_ZX_MM_XFP; 
                    break;
                case MEDIA_XFP_TYPE_CATEGORY_10GBASE_EX: 
                    linktype = (singlemode) ? MEDIA_IF_TYPE_10G_BASE_EX_SM_XFP : 
                        MEDIA_IF_TYPE_10G_BASE_EX_MM_XFP; 
                    break;
                case MEDIA_XFP_TYPE_CATEGORY_10GBASE_XWDM: 
                    linktype = (singlemode) ? MEDIA_IF_TYPE_10G_BASE_XWDM_SM_XFP : 
                                              MEDIA_IF_TYPE_10G_BASE_XWDM_MM_XFP; 
                    break;
                default:
                    PDEBUG("link type %d can not be detected ", linktype);
                    linktype = MEDIA_IF_TYPE_UNKNOWN;
                }
                break;
            }
            break;
    default:
        linktype = MEDIA_IF_TYPE_UNKNOWN;
    }

    *plink_type = linktype;

    return CCL_OK;
}


