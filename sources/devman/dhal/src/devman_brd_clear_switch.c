/********************************************************************************/
/**
 * @file devman_brd_clear_switch.c
 * @brief HAL clear switch interface implementation
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "devman_brd_clear_switch.h"

ccl_err_t devman_brd_clear_switch_get(devman_clear_switch_t cswitch, b_u32 *pval)
{
    return CCL_OK;
}

ccl_err_t devman_brd_clear_switch_led_set(devman_clear_switch_t cswitch, b_bool ledstate)
{
    return CCL_OK;
}





