/********************************************************************************/
/**
 * @file devman_brd_trans.c
 * @brief HAL transceivers interface implementation
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "devman_brd_media.h"
#include "devman_brd_gen.h"
#include "devman_brd_trans.h"

b_bool devman_brd_trans_is_present(devman_dev_t dev, int port)
{
    return CCL_OK;
}

ccl_err_t devman_brd_trans_data_get(devman_dev_t dev, int port, devman_trans_param_t *pparam)
{
    return CCL_OK;
}

ccl_err_t devman_brd_trans_data_set(devman_dev_t dev, int port, devman_trans_param_t *pparam)
{
    return CCL_OK;
}



