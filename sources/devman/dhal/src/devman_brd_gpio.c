/********************************************************************************/
/**
 * @file devman_brd_gpio.c
 * @brief HAL GPIO interface implementation
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "devman_brd_gpio.h"

ccl_err_t devman_brd_gpio_init(void)
{
    return CCL_OK;
}

ccl_err_t devman_brd_gpio_set(int iodev,  int iopin,  int val)
{
    return CCL_OK;
}

ccl_err_t devman_brd_gpio_get(int iodev,  int iopin,  int *pval)
{
    return CCL_OK;
}


