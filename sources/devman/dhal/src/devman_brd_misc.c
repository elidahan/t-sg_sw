/********************************************************************************/
/**
 * @file devman_brd_misc.c
 * @brief HAL Miscelaneous Devices interface implementation
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "devman_brd_gen.h"
#include "devman_brd_misc.h"

ccl_err_t devman_brd_misc_device_reset(devman_dev_t dev)
{
    return CCL_OK;
}

ccl_err_t devman_brd_misc_device_unreset(devman_dev_t dev)
{
    return CCL_OK;
}

b_bool devman_brd_misc_device_is_present(devman_dev_t dev)
{
    return CCL_TRUE;
}

ccl_err_t devman_brd_misc_device_status_get(devman_dev_t dev, devman_dev_state_t state)
{
    return CCL_OK;
}

ccl_err_t devman_brd_misc_fpga_boot_select_set(devman_fpga_boot_dev_t bootdev)
{
    return CCL_OK;
}

ccl_err_t devman_brd_misc_fpga_program(void)
{
    return CCL_OK;
}

ccl_err_t devman_brd_misc_fpga_flash_program(void)
{
    return CCL_OK;
}

ccl_err_t devman_brd_misc_flash_program(devman_spi_boot_flash_t bootflash)
{
    return CCL_OK;
}

ccl_err_t devman_brd_misc_firmware_version_get(devman_dev_t dev, char *buf, int len)
{
    return CCL_OK;
}

ccl_err_t devman_brd_misc_sw_version_get(devman_dev_t host, char *buf, int len)
{
    return CCL_OK;
}

ccl_err_t devman_brd_misc_flash_info_get(devman_brd_flash_t flash, devman_flash_info_t *info)
{
    return CCL_OK;
}

ccl_err_t devman_brd_misc_ram_usage_info_get(devman_dev_t host, devman_ram_usage_info_t *usage)
{
    return CCL_OK;
}

ccl_err_t devman_brd_misc_cpu_info_get(devman_dev_t host, devman_cpu_info_t *cpuinfo)
{
    return CCL_OK;
}

b_bool devman_brd_misc_sysmon_test_result_get(devman_dev_t dev)
{
    return CCL_OK;
}

ccl_err_t devman_brd_misc_sysmon_test_run(devman_dev_t dev)
{
    return CCL_OK;
}


