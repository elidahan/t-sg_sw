/********************************************************************************/
/**
 * @file devman_brd_port.c
 * @brief HAL ports interface implementation
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "devman_port.h"
#include "devman_if_stat.h"
#include "devman_brd_port.h"

static ccl_err_t _ret;

ccl_err_t devman_brd_port_init(b_u32 logport)
{
    devo_t   portmap_devo;
    b_i32    discr = 0;

    _ret = devman_device_object_identify("portmap", discr, &portmap_devo);
    if ( _ret || !portmap_devo) {
        ccl_syslog_err("Error! Can't identify \"portmap\", %d discr object\n", 
                       discr);
        return CCL_FAIL;
    }

    _ret = devman_attr_array_set_word(portmap_devo, "init", logport, 1);
    if (_ret) {
        ccl_syslog_err("init port %d failed\n", logport);
        return CCL_FAIL;
    }

    return CCL_OK;
}

ccl_err_t devman_brd_port_number_of_ports_get(b_u32 *pports)
{
    devo_t   portmap_devo;
    b_i32    discr = 0;

    _ret = devman_device_object_identify("portmap", discr, &portmap_devo);
    if ( _ret || !portmap_devo) {
        ccl_syslog_err("Error! Can't identify \"portmap\", %d discr object\n", 
                       discr);
        return CCL_FAIL;
    }

    _ret = devman_attr_array_get_word(portmap_devo, "maxnum", discr, pports);
    if (_ret) {
        ccl_syslog_err("devman_attr_array_get_word failed\n");
        return CCL_FAIL;
    }
    return CCL_OK;
}

ccl_err_t devman_brd_port_counter_get(b_u32 logport, devman_if_counter_t dcounter, b_u64 *pcounter)
{
    devo_t              portmap_devo;
    b_i32               discr = 0;
    static const char   *dcounter_names[] = DEVMAN_IF_COUNTERS_STR;

    _ret = devman_device_object_identify("portmap", discr, &portmap_devo);
    if ( _ret || !portmap_devo) {
        ccl_syslog_err("Error! Can't identify \"portmap\", %d discr object\n", 
                       discr);
        return CCL_FAIL;
    }

    if (dcounter == DEVMAN_IF_MIB_MAX) {
        ccl_syslog_err("incorrect counter %d for port %d\n", 
                       dcounter, logport);
        return CCL_FAIL;
    }

    _ret = devman_attr_array_get_double(portmap_devo, dcounter_names[dcounter], 
                                        logport, pcounter);
    if (_ret) {
        ccl_syslog_err("devman_attr_get_double failed for port %d, counter: %s (%d)\n", 
                       logport, dcounter_names[dcounter], dcounter);
        return CCL_FAIL;
    }

    return CCL_OK;
}

ccl_err_t devman_brd_port_counter_clear(b_u32 logport, devman_if_counter_t dcounter)
{
    return CCL_OK;
}

ccl_err_t devman_brd_port_tx_state_set(b_u32 logport, b_bool state)
{
    devo_t   portmap_devo;
    b_i32    discr = 0;

    _ret = devman_device_object_identify("portmap", discr, &portmap_devo);
    if ( _ret || !portmap_devo) {
        ccl_syslog_err("Error! Can't identify \"portmap\", %d discr object\n", 
                       discr);
        return CCL_FAIL;
    }

    _ret = devman_attr_array_set_bool(portmap_devo, "enable", logport, state);
    if (_ret) {
        ccl_syslog_err("set port %d tx state %s failed\n", 
                       logport, state ? "enable" : "disable");
        return CCL_FAIL;
    }
    return CCL_OK;
}

ccl_err_t devman_brd_port_tx_state_get(b_u32 logport, b_bool *pstate)
{
    devo_t   portmap_devo;
    b_i32    discr = 0;

    _ret = devman_device_object_identify("portmap", discr, &portmap_devo);
    if ( _ret || !portmap_devo) {
        ccl_syslog_err("Error! Can't identify \"portmap\", %d discr object\n", 
                       discr);
        return CCL_FAIL;
    }

    _ret = devman_attr_array_get_bool(portmap_devo, "enable", logport, pstate);
    if (_ret) {
        ccl_syslog_err("get port %d tx state failed\n", logport);
        return CCL_FAIL;
    }
    return CCL_OK;
}

ccl_err_t devman_brd_port_link_status_get(b_u32 logport, devman_if_link_status_t *pstatus)
{
    b_u8 link = 0;

    _ret = devman_brd_port_property_get(logport, DEVMAN_IF_ACTIVE_LINK, &link, sizeof(link));
    if (_ret) {
        ccl_syslog_err("get port %d link state failed\n", logport);
        return CCL_FAIL;
    }

    *pstatus = link ? DEVMAN_IF_LINK_UP : DEVMAN_IF_LINK_DOWN;

    return CCL_OK;
}

ccl_err_t devman_brd_port_if_type_get(b_u32 logport, devman_if_type_t *piftype)
{
    *piftype = DEVMAN_IF_TYPE_ETHERNET_CSMACD;

    return CCL_OK;
}

ccl_err_t devman_brd_port_led_state_set(b_u32 logport,  b_bool state)
{
    devo_t   portmap_devo;
    b_i32    discr = 0;

    _ret = devman_device_object_identify("portmap", discr, &portmap_devo);
    if ( _ret || !portmap_devo) {
        ccl_syslog_err("Error! Can't identify \"portmap\", %d discr object\n", 
                       discr);
        return CCL_FAIL;
    }

    _ret = devman_attr_set_bool(portmap_devo, "led", state);
    if (_ret) {
        ccl_syslog_err("turn led %s of port %d failed\n", 
                       state ? "on" : "off", logport);
        return CCL_FAIL;
    }
    return CCL_OK;
}

ccl_err_t devman_brd_port_property_set(b_u32 logport, devman_if_property_t dprop, 
                                    void *pdata, b_u32 size)
{
    devo_t               portmap_devo;
    b_i32                discr = 0;
    static const char    *dprop_names[] = DEVMAN_IF_PROPERTIES_STR;

    if (dprop >= DEVMAN_PROP_MAX) {
        ccl_syslog_err("incorrect property %d for port %d\n", 
                       dprop, logport);
        return CCL_FAIL;
    }

    _ret = devman_device_object_identify("portmap", discr, &portmap_devo);
    if ( _ret || !portmap_devo) {
        ccl_syslog_err("Error! Can't identify \"portmap\", %d discr object\n", 
                       discr);
        return CCL_FAIL;
    }

    _ret = devman_attr_array_set_buff(portmap_devo, dprop_names[dprop], 
                                      logport, (b_u8 *)pdata, size);
    if (_ret) {
        ccl_syslog_err("devman_attr_array_set_buff failed for port %d, property: %d, size: %d\n", 
                       logport, dprop, size);
        return CCL_FAIL;
    }

    return CCL_OK;
}

ccl_err_t devman_brd_port_property_get(b_u32 logport, devman_if_property_t dprop, 
                                    void *pdata, b_u32 size)
{
    devo_t               portmap_devo;
    b_i32                discr = 0;
    static const char    *dprop_names[] = DEVMAN_IF_PROPERTIES_STR;

    if (dprop >= DEVMAN_PROP_MAX) {
        ccl_syslog_err("incorrect property %d for port %d\n", 
                       dprop, logport);
        return CCL_FAIL;
    }

    _ret = devman_device_object_identify("portmap", discr, &portmap_devo);
    if ( _ret || !portmap_devo) {
        ccl_syslog_err("Error! Can't identify \"portmap\", %d discr object\n", 
                       discr);
        return CCL_FAIL;
    }

    _ret = devman_attr_array_get_buff(portmap_devo, dprop_names[dprop], 
                                      logport, (b_u8 *)pdata, size);
    if (_ret) {
        ccl_syslog_err("devman_attr_array_get_buff failed for port %d, property: %d, size: %d\n", 
                       logport, dprop, size);
        return CCL_FAIL;
    }

    return CCL_OK;
}

