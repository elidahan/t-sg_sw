/********************************************************************************/
/**
 * @file devman_brd_power_supply.c
 * @brief HAL power supply interface implementation
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "devman_brd_power_supply.h"

ccl_err_t devman_brd_ps_number_of_psu_get(int *pnum)
{
    return CCL_OK;
}

ccl_err_t devman_brd_ps_status_get(devman_ps_type_t psu,  devman_ps_status_t *pstatus)
{
    return CCL_OK;
}

ccl_err_t devman_brd_ps_vout_get(devman_ps_type_t psu, b_i32 *vout)
{
    return CCL_OK;
}

ccl_err_t devman_brd_ps_iout_get(devman_ps_type_t psu, b_i32 *iout)
{
    return CCL_OK;
}

ccl_err_t devman_brd_ps_pout_get(devman_ps_type_t psu, b_i32 *pout)
{
    return CCL_OK;
}

ccl_err_t devman_brd_ps_pin_get(devman_ps_type_t psu, b_i32 *pin)
{
    return CCL_OK;
}

ccl_err_t devman_brd_ps_temperature_get(devman_ps_type_t psu, b_i32 *temp)
{
    return CCL_OK;
}

ccl_err_t devman_brd_ps_fan_speed_get(devman_ps_type_t psu, b_i32 fan, b_i32 *speed)
{
    return CCL_OK;
}




