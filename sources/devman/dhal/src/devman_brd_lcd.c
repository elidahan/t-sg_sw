/********************************************************************************/
/**
 * @file devman_brd_lcd.c
 * @brief HAL LCD interface implementation
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "devman_brd_lcd.h"

ccl_err_t devman_brd_lcd_screen_clear(void)
{
    return CCL_OK;
}

ccl_err_t devman_brd_lcd_string_display(char *str)
{
    return CCL_OK;
}

ccl_err_t devman_brd_lcd_cursor_move(devman_lcd_cursor_direction_t dir)
{
    return CCL_OK;
}

ccl_err_t devman_brd_lcd_cursor_set(b_u32 row, b_u32 column)
{
    return CCL_OK;
}

ccl_err_t devman_brd_lcd_startup_message_display(char *msg)
{
    return CCL_OK;
}

ccl_err_t devman_brd_lcd_led_color_set(devman_lcd_led_color_t led,
                                    devman_lcd_led_color_t color1, b_bool state1, 
                                    devman_lcd_led_color_t color2, b_bool state2)
{
    return CCL_OK;
}

ccl_err_t devman_brd_lcd_backlight_set(devman_lcd_component_t comp, devman_lcd_backlight_t mode)
{
    return CCL_OK;
}

ccl_err_t devman_brd_lcd_brightness_set(devman_lcd_component_t comp, b_u8 brightness)
{
    return CCL_OK;
}

b_bool devman_brd_lcd_key_is_pressed(b_u8 *key)
{
    return CCL_TRUE;
}


