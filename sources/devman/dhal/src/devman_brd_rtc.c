/********************************************************************************/
/**
 * @file devman_brd_rtc.c
 * @brief HAL RTC interface implementation
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "devman_brd_rtc.h"

ccl_err_t devman_brd_rtc_clock_set(devman_rtc_device_t dev, devman_rtc_time_t time)
{
    return CCL_OK;
}

ccl_err_t devman_brd_rtc_clock_get(devman_rtc_device_t dev, devman_rtc_time_t *ptime)
{
    return CCL_OK;
}

ccl_err_t devman_brd_rtc_mem_write(devman_rtc_device_t dev, b_u8 addr, b_u8 *buf, b_u16 len)
{
    return CCL_OK;
}

ccl_err_t devman_brd_rtc_mem_read(devman_rtc_device_t dev, b_u8 addr, b_u8 *buf, b_u16 len)
{
    return CCL_OK;
}

ccl_err_t devman_brd_rtc_mem_clear(devman_rtc_device_t dev)
{
    return CCL_OK;
}

ccl_err_t devman_brd_rtc_battery_status_get(devman_rtc_device_t dev, devman_rtc_battery_status_t *pstatus)
{
    return CCL_OK;
}





