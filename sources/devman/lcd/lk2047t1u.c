/********************************************************************************/
/**
 * @file lk2047t1u.c
 * @brief LK204-T7-1U LCD Display device manager driver 
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <semaphore.h>
#include <linux/limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "i2ccore.h"       
#include "lk2047t1u.h"

#define LK204T71U_DEBUG
/* printing/error-returning macros */
#ifdef LK204T71U_DEBUG
    #define PDEBUG(fmt, args...) ccl_syslog_err("LK204T71U_DEBUG: %s[%d]: " fmt, __FUNCTION__, __LINE__, ## args)
#else
    #define PDEBUG(fmt, args...)
#endif
#define LK204T71U_ERROR_RETURN(err) do { \
   ccl_syslog_err("Error! %s[%d], error=%i\n", __FUNCTION__, __LINE__, (err)); \
   return err; \
} while (0)

/* lk2047t1u commands */
#define LK204T71U_I2C_SLAVE_ADDR                    0xFE33
#define LK204T71U_TRANS_PROTO                       0xFEA0
#define LK204T71U_CLEAR_SCREEN                      0xFE58
#define LK204T71U_CHANGE_STARTUP_SCREEN             0xFE40
#define LK204T71U_AUTO_SCROLL_ON                    0xFE51
#define LK204T71U_AUTO_SCROLL_OFF                   0xFE52
#define LK204T71U_AUTO_LINE_WRAP_ON                 0xFE43
#define LK204T71U_AUTO_LINE_WRAP_OFF                0xFE44
#define LK204T71U_SET_CURSOR_POSITION               0xFE47 
#define LK204T71U_GO_HOME                           0xFE48
#define LK204T71U_MOVE_CURSOR_BACK                  0xFE4C
#define LK204T71U_MOVE_CURSOR_FWRD                  0xFE4D
#define LK204T71U_UNDERLINE_CURSOR_ON               0xFE4A
#define LK204T71U_UNDERLINE_CURSOR_OFF              0xFE4B
#define LK204T71U_BLINKING_BLOCK_CURSOR_ON          0xFE53
#define LK204T71U_BLINKING_BLOCK_CURSOR_OFF         0xFE54
#define LK204T71U_CREATE_CUSTOM_CHARACTER           0xFE4E
#define LK204T71U_SAVE_CUSTOM_CHARACTERS            0xFEC1
#define LK204T71U_LOAD_CUSTOM_CHARACTERS            0xFEC0
#define LK204T71U_SAVE_STARTUP_SCREEN_CUST_CHARS    0xFEC2
#define LK204T71U_INIT_MEDIUM_NUMBERS               0xFE6D
#define LK204T71U_PLACE_MEDIUM_NUMBERS              0xFE6F
#define LK204T71U_INIT_LARGE_NUMBERS                0xFE6E
#define LK204T71U_PLACE_LARGE_NUMBERS               0xFE23
#define LK204T71U_INIT_HORIZONTAL_BAR               0xFE68
#define LK204T71U_PLACE_HORIZONTAL_BAR_GRAPH        0xFE7C
#define LK204T71U_INIT_NARROW_VERT_BAR              0xFE73
#define LK204T71U_INIT_WIDE_VERT_BAR                0xFE76
#define LK204T71U_PLACE_VERT_BAR                    0xFE3D
#define LK204T71U_GEN_PURPOSE_OUTPUT_ON             0xFE57
#define LK204T71U_GEN_PURPOSE_OUTPUT_OFF            0xFE56
#define LK204T71U_SET_STARTUP_GPO_STATE             0xFEC3
#define LK204T71U_AUTO_TRANSMIT_KEY_PRESSES_ON      0xFE41
#define LK204T71U_AUTO_TRANSMIT_KEY_PRESSES_OFF     0xFE4F
#define LK204T71U_POLL_KEY_PRESS                    0xFE26
#define LK204T71U_CLEAR_KEY_BUFFER                  0xFE45
#define LK204T71U_SET_DEBOUNCE_TIME                 0xFE55
#define LK204T71U_SET_AUTO_REPEAT_MODE              0xFE7E
#define LK204T71U_AUTO_REPEAT_MODE_OFF              0xFE60
#define LK204T71U_ASSIGN_KEYPAD_CODES               0xFED5
#define LK204T71U_KEYPAD_BACKLIGHT_OFF              0xFE9B
#define LK204T71U_SET_KEYPAD_BRIGHTNESS             0xFE9C
#define LK204T71U_SET_AUTO_BACKLIGHT                0xFE9D
#define LK204T71U_BACKLIGHT_ON                      0xFE42
#define LK204T71U_BACKLIGHT_OFF                     0xFE46
#define LK204T71U_SET_BRIGHTNESS                    0xFE99
#define LK204T71U_SET_AND_SAVE_BRIGHTNESS           0xFE98
#define LK204T71U_SET_BACKLIGHT_COLOUR              0xFE82
#define LK204T71U_SET_CONTRAST                      0xFE50
#define LK204T71U_SET_AND_SAVE_CONTRAST             0xFE91
#define LK204T71U_SET_REMEMBER                      0xFE93
#define LK204T71U_SET_DATA_LOCK                     0xFECAF5A0
#define LK204T71U_SET_AND_SAVE_DATA_LOCK            0xFECBF5A0
#define LK204T71U_WRITE_CUSTOMER_DATA               0xFE34
#define LK204T71U_READ_CUSTOMER_DATA                0xFE35
#define LK204T71U_READ_VERSION_NUMBER               0xFE36
#define LK204T71U_READ_MODULE_TYPE                  0xFE37

#define LK204T71U_MAX_OFFSET LK204T71U_SET_AND_SAVE_DATA_LOCK


/* the lk2047t1u internal control structure */      
typedef struct lk2047t1u_ctx_t {
    void                  *devo;
    gen_i2c_dev_brdspec_t brdspec;
} lk2047t1u_ctx_t;

static ccl_err_t _ret;                               

/* lk2047t1u register read */
static ccl_err_t _lk2047t1u_reg_read(void *p_priv, b_u32 offset, 
                                     b_u32 offset_sz, b_u32 idx, 
                                     b_u8 *p_value, b_u32 size)
{
    lk2047t1u_ctx_t   *p_lk2047t1u;
    i2c_route_t      i2c_route;

    if ( !p_priv || offset > LK204T71U_MAX_OFFSET || 
         !p_value || idx || size != sizeof(b_u8) ) {
        PDEBUG("Error! Invalid input parameters\n");
        LK204T71U_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_lk2047t1u = (lk2047t1u_ctx_t *)p_priv;     
    memcpy(&i2c_route, &p_lk2047t1u->brdspec.i2c.bus_id, sizeof(i2c_route_t));

    _ret = devi2ccore_read_buff(&i2c_route, 0, offset, offset_sz, p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to write over spi bus\n");
        LK204T71U_ERROR_RETURN(_ret);
    }

    return _ret;
}

/* lk2047t1u register write */
static ccl_err_t _lk2047t1u_reg_write(void *p_priv, b_u32 offset, 
                                      b_u32 offset_sz, b_u32 idx, 
                                      b_u8 *p_value, b_u32 size)
{
    lk2047t1u_ctx_t   *p_lk2047t1u;
    i2c_route_t      i2c_route;

    if ( !p_priv || offset > LK204T71U_MAX_OFFSET || idx ) {
        PDEBUG("Error! Invalid input parameters\n");
        LK204T71U_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_lk2047t1u = (lk2047t1u_ctx_t *)p_priv;     
    memcpy(&i2c_route, &p_lk2047t1u->brdspec.i2c.bus_id, sizeof(i2c_route_t));

    _ret = devi2ccore_write_buff(&i2c_route, 0, offset, offset_sz, p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to write over spi bus\n");
        LK204T71U_ERROR_RETURN(_ret);
    }

    return _ret;
}

/* lk2047t1u string write */
static ccl_err_t _lk2047t1u_string_write(void *p_priv, b_u32 idx, 
                                         b_u8 *p_value, b_u32 size)
{
    lk2047t1u_ctx_t   *p_lk2047t1u;
    i2c_route_t       i2c_route;

    if ( !p_priv || idx || !p_value || !size) {
        PDEBUG("Error! Invalid input parameters\n");
        LK204T71U_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    PDEBUG("str=%s\n", p_value);

    p_lk2047t1u = (lk2047t1u_ctx_t *)p_priv;     
    memcpy(&i2c_route, &p_lk2047t1u->brdspec.i2c.bus_id, sizeof(i2c_route_t));

    _ret = devi2ccore_write_buff(&i2c_route, 0, 0, 0, p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to write over spi bus\n");
        LK204T71U_ERROR_RETURN(_ret);
    }

    return _ret;
}

static ccl_err_t _lk2047t1u_init(void *p_priv)
{
    lk2047t1u_ctx_t   *p_lk2047t1u;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        LK204T71U_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_lk2047t1u = (lk2047t1u_ctx_t *)p_priv;

    return CCL_OK;
}

static ccl_err_t _lk2047t1u_configure_test(void *p_priv)
{
    lk2047t1u_ctx_t   *p_lk2047t1u;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        LK204T71U_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_lk2047t1u = (lk2047t1u_ctx_t *)p_priv;

    return CCL_OK;
}

static ccl_err_t _lk2047t1u_run_test(void *p_priv, 
                                     __attribute__((unused)) b_u8 *buf, 
                                     __attribute__((unused)) b_u32 size)
{
    lk2047t1u_ctx_t   *p_lk2047t1u;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        LK204T71U_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_lk2047t1u = (lk2047t1u_ctx_t *)p_priv;

    return CCL_OK;
}

ccl_err_t lk2047t1u_attr_read(void *p_priv, b_u32 attrid, 
                              b_u32 offset, b_u32 offset_sz, 
                              b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || offset > LK204T71U_MAX_OFFSET || 
         !p_value || idx || size != sizeof(b_u32) ) {
        PDEBUG("Error! Invalid input parameters\n");
        LK204T71U_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch (attrid) {
    case eLK204T71U_I2C_SLAVE_ADDR:
    case eLK204T71U_TRANS_PROTO:
    case eLK204T71U_READ_CUSTOMER_DATA:
    case eLK204T71U_READ_VERSION_NUMBER:
    case eLK204T71U_READ_MODULE_TYPE:
        _ret = _lk2047t1u_reg_read(p_priv, offset, offset_sz, idx, p_value, size);
        break;
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK) {
        PDEBUG("Failure in attribute id: %d\n", attrid);
    }

    return _ret;               
}

ccl_err_t lk2047t1u_attr_write(void *p_priv, b_u32 attrid, 
                               b_u32 offset, b_u32 offset_sz, 
                               b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || offset > LK204T71U_MAX_OFFSET || idx ) {
        PDEBUG("Error! Invalid input parameters\n");
        LK204T71U_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch (attrid) {
    case eLK204T71U_I2C_SLAVE_ADDR:
    case eLK204T71U_TRANS_PROTO:
    case eLK204T71U_CLEAR_SCREEN:
    case eLK204T71U_CHANGE_STARTUP_SCREEN:
    case eLK204T71U_AUTO_SCROLL_ON:
    case eLK204T71U_AUTO_SCROLL_OFF:
    case eLK204T71U_AUTO_LINE_WRAP_ON:
    case eLK204T71U_AUTO_LINE_WRAP_OFF:
    case eLK204T71U_SET_CURSOR_POSITION:
    case eLK204T71U_GO_HOME:
    case eLK204T71U_MOVE_CURSOR_BACK:
    case eLK204T71U_MOVE_CURSOR_FWRD:
    case eLK204T71U_UNDERLINE_CURSOR_ON:
    case eLK204T71U_UNDERLINE_CURSOR_OFF:
    case eLK204T71U_BLINKING_BLOCK_CURSOR_ON:
    case eLK204T71U_BLINKING_BLOCK_CURSOR_OFF:
    case eLK204T71U_CREATE_CUSTOM_CHARACTER:
    case eLK204T71U_SAVE_CUSTOM_CHARACTERS:
    case eLK204T71U_LOAD_CUSTOM_CHARACTERS:
    case eLK204T71U_SAVE_STARTUP_SCREEN_CUST_CHARS:
    case eLK204T71U_INIT_MEDIUM_NUMBERS:
    case eLK204T71U_PLACE_MEDIUM_NUMBERS:
    case eLK204T71U_INIT_LARGE_NUMBERS:
    case eLK204T71U_PLACE_LARGE_NUMBERS:
    case eLK204T71U_INIT_HORIZONTAL_BAR:
    case eLK204T71U_PLACE_HORIZONTAL_BAR_GRAPH:
    case eLK204T71U_INIT_NARROW_VERT_BAR:
    case eLK204T71U_INIT_WIDE_VERT_BAR:
    case eLK204T71U_PLACE_VERT_BAR:
    case eLK204T71U_GEN_PURPOSE_OUTPUT_ON:
    case eLK204T71U_GEN_PURPOSE_OUTPUT_OFF:
    case eLK204T71U_SET_STARTUP_GPO_STATE:
    case eLK204T71U_AUTO_TRANSMIT_KEY_PRESSES_ON:
    case eLK204T71U_AUTO_TRANSMIT_KEY_PRESSES_OFF:
    case eLK204T71U_POLL_KEY_PRESS:
    case eLK204T71U_CLEAR_KEY_BUFFER:
    case eLK204T71U_SET_DEBOUNCE_TIME:
    case eLK204T71U_SET_AUTO_REPEAT_MODE:
    case eLK204T71U_AUTO_REPEAT_MODE_OFF:
    case eLK204T71U_ASSIGN_KEYPAD_CODES:
    case eLK204T71U_KEYPAD_BACKLIGHT_OFF:
    case eLK204T71U_SET_KEYPAD_BRIGHTNESS:
    case eLK204T71U_SET_AUTO_BACKLIGHT:
    case eLK204T71U_BACKLIGHT_ON:
    case eLK204T71U_BACKLIGHT_OFF:
    case eLK204T71U_SET_BRIGHTNESS:
    case eLK204T71U_SET_AND_SAVE_BRIGHTNESS:
    case eLK204T71U_SET_BACKLIGHT_COLOUR:
    case eLK204T71U_SET_CONTRAST:
    case eLK204T71U_SET_AND_SAVE_CONTRAST:
    case eLK204T71U_SET_REMEMBER:
    case eLK204T71U_SET_DATA_LOCK:
    case eLK204T71U_SET_AND_SAVE_DATA_LOCK:
    case eLK204T71U_WRITE_CUSTOMER_DATA:
    case eLK204T71U_READ_CUSTOMER_DATA:
    case eLK204T71U_READ_VERSION_NUMBER:
    case eLK204T71U_READ_MODULE_TYPE:
        _ret = _lk2047t1u_reg_write(p_priv, offset, offset_sz, idx, p_value, size);
        break;
    case eLK204T71U_WRITE_TEXT:
        _ret = _lk2047t1u_string_write(p_priv, idx, p_value, size);
        break;
    case eLK204T71U_INIT:
        _ret = _lk2047t1u_init(p_priv);
        break;
    case eLK204T71U_CONFIGURE_TEST:
        _ret = _lk2047t1u_configure_test(p_priv);
        break;
    case eLK204T71U_RUN_TEST:
        _ret = _lk2047t1u_run_test(p_priv, p_value, size);
        break;
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK) {
        PDEBUG("Failure in attribute id: %d\n", attrid);
    }

    return _ret;               
}

/* lk2047t1u attributes definition */
static device_attr_t _attrs[] = {
    {
        .name = "i2c_bus", 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset = offsetof(struct lk2047t1u_ctx_t, brdspec.i2c.bus_id),
        .format = "%d"
    },
    {
        .name = "i2c_addr", 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset = offsetof(struct lk2047t1u_ctx_t, brdspec.i2c.dev_i2ca),
        .format = "0x%03x"
    },
    {
        .name = "slave_addr", .id = eLK204T71U_I2C_SLAVE_ADDR, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = LK204T71U_I2C_SLAVE_ADDR
    },
    {
        .name = "trans_proto", .id = eLK204T71U_TRANS_PROTO, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_TRANS_PROTO
    },
    {
        .name = "clear_screen", .id = eLK204T71U_CLEAR_SCREEN, 
        .type = eDEVMAN_ATTR_CMD, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_CLEAR_SCREEN,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "change_startup_screen", .id = eLK204T71U_CHANGE_STARTUP_SCREEN, 
        .type = eDEVMAN_ATTR_STRING, .size = 80, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_CHANGE_STARTUP_SCREEN,
        .format = "%s",
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "auto_scroll_on", .id = eLK204T71U_AUTO_SCROLL_ON, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_AUTO_SCROLL_ON,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "auto_scroll_off", .id = eLK204T71U_AUTO_SCROLL_OFF, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_AUTO_SCROLL_OFF,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "auto_line_wrap_on", .id = eLK204T71U_AUTO_LINE_WRAP_ON, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_AUTO_LINE_WRAP_ON,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "auto_line_wrap_off", .id = eLK204T71U_AUTO_LINE_WRAP_OFF, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_AUTO_LINE_WRAP_OFF,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "set_cursor_pos", .id = eLK204T71U_SET_CURSOR_POSITION, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_SET_CURSOR_POSITION,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "go_home", .id = eLK204T71U_GO_HOME, 
        .type = eDEVMAN_ATTR_CMD, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_GO_HOME,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "move_cursor_back", .id = eLK204T71U_MOVE_CURSOR_BACK, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_MOVE_CURSOR_BACK,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "move_cursor_fwrd", .id = eLK204T71U_MOVE_CURSOR_FWRD, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_MOVE_CURSOR_FWRD,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "underline_cursor_on", .id = eLK204T71U_UNDERLINE_CURSOR_ON, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_UNDERLINE_CURSOR_ON,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "underline_cursor_off", .id = eLK204T71U_UNDERLINE_CURSOR_OFF, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_UNDERLINE_CURSOR_OFF,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "blinking_block", .id = eLK204T71U_BLINKING_BLOCK_CURSOR_ON, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_BLINKING_BLOCK_CURSOR_ON,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "blinking_block_cursor_on", .id = eLK204T71U_BLINKING_BLOCK_CURSOR_OFF, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_BLINKING_BLOCK_CURSOR_OFF,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "create_custom_char", .id = eLK204T71U_CREATE_CUSTOM_CHARACTER, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_CREATE_CUSTOM_CHARACTER,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "save_custom_char", .id = eLK204T71U_SAVE_CUSTOM_CHARACTERS, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_SAVE_CUSTOM_CHARACTERS,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "load_custom_char", .id = eLK204T71U_LOAD_CUSTOM_CHARACTERS, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_LOAD_CUSTOM_CHARACTERS,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "save_startup_scr", .id = eLK204T71U_SAVE_STARTUP_SCREEN_CUST_CHARS, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_SAVE_STARTUP_SCREEN_CUST_CHARS,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "init_med_numbers", .id = eLK204T71U_INIT_MEDIUM_NUMBERS, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_INIT_MEDIUM_NUMBERS,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "place_med_numbers", .id = eLK204T71U_PLACE_MEDIUM_NUMBERS, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_PLACE_MEDIUM_NUMBERS,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "init_large_numbers", .id = eLK204T71U_INIT_LARGE_NUMBERS, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_INIT_LARGE_NUMBERS,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "place_large_numbers", .id = eLK204T71U_PLACE_LARGE_NUMBERS, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_PLACE_LARGE_NUMBERS,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "int_horiz_bar", .id = eLK204T71U_INIT_HORIZONTAL_BAR, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_INIT_HORIZONTAL_BAR,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "place_horiz_bar_graph", .id = eLK204T71U_PLACE_HORIZONTAL_BAR_GRAPH, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_PLACE_HORIZONTAL_BAR_GRAPH,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "init_narrow_vert_bar", .id = eLK204T71U_INIT_NARROW_VERT_BAR, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_INIT_NARROW_VERT_BAR,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "init_wide_vert_bar", .id = eLK204T71U_INIT_WIDE_VERT_BAR, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_INIT_WIDE_VERT_BAR,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "place_vert_bar", .id = eLK204T71U_PLACE_VERT_BAR, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_PLACE_VERT_BAR,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "gpo_on", .id = eLK204T71U_GEN_PURPOSE_OUTPUT_ON, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_GEN_PURPOSE_OUTPUT_ON,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "gpo_off", .id = eLK204T71U_GEN_PURPOSE_OUTPUT_OFF, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_GEN_PURPOSE_OUTPUT_OFF,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "set_startup_gpo_state", .id = eLK204T71U_SET_STARTUP_GPO_STATE, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_SET_STARTUP_GPO_STATE,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "auto_transmit_key_press_on", .id = eLK204T71U_AUTO_TRANSMIT_KEY_PRESSES_ON, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_AUTO_TRANSMIT_KEY_PRESSES_ON,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "auto_transmit_key_press_off", .id = eLK204T71U_AUTO_TRANSMIT_KEY_PRESSES_OFF, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_AUTO_TRANSMIT_KEY_PRESSES_OFF,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "poll_key_press", .id = eLK204T71U_POLL_KEY_PRESS, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_POLL_KEY_PRESS,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "clear_key_buffer", .id = eLK204T71U_CLEAR_KEY_BUFFER, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_CLEAR_KEY_BUFFER,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "set_debounce_time", .id = eLK204T71U_SET_DEBOUNCE_TIME, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_SET_DEBOUNCE_TIME,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "set_auto_repeat_mode", .id = eLK204T71U_SET_AUTO_REPEAT_MODE, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_SET_AUTO_REPEAT_MODE,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "auto_repeat_mode_off", .id = eLK204T71U_AUTO_REPEAT_MODE_OFF, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_AUTO_REPEAT_MODE_OFF,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "assign_keypad_codes", .id = eLK204T71U_ASSIGN_KEYPAD_CODES, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_ASSIGN_KEYPAD_CODES,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "keypad_backlight_off", .id = eLK204T71U_KEYPAD_BACKLIGHT_OFF, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_KEYPAD_BACKLIGHT_OFF,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "set_keypad_brightness", .id = eLK204T71U_SET_KEYPAD_BRIGHTNESS, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_SET_KEYPAD_BRIGHTNESS,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "set_auto_backlight", .id = eLK204T71U_SET_AUTO_BACKLIGHT, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_SET_AUTO_BACKLIGHT,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "backlight_on", .id = eLK204T71U_BACKLIGHT_ON, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_BACKLIGHT_ON,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "backlight_off", .id = eLK204T71U_BACKLIGHT_OFF, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_BACKLIGHT_OFF,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "set_brightness", .id = eLK204T71U_SET_BRIGHTNESS, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_SET_BRIGHTNESS,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "set_and_save_brightness", .id = eLK204T71U_SET_AND_SAVE_BRIGHTNESS, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_SET_AND_SAVE_BRIGHTNESS,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "set_brightness_color", .id = eLK204T71U_SET_BACKLIGHT_COLOUR, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_SET_BACKLIGHT_COLOUR,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "set_contrast", .id = eLK204T71U_SET_CONTRAST, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_SET_CONTRAST,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "set_and_save_contrast", .id = eLK204T71U_SET_AND_SAVE_CONTRAST, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_SET_AND_SAVE_CONTRAST,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "set_remember", .id = eLK204T71U_SET_REMEMBER, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_SET_REMEMBER,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "set_data_lock", .id = eLK204T71U_SET_DATA_LOCK, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = LK204T71U_SET_DATA_LOCK,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "set_and_save_data_lock", .id = eLK204T71U_SET_AND_SAVE_DATA_LOCK, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = LK204T71U_SET_AND_SAVE_DATA_LOCK,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "write_cust_data", .id = eLK204T71U_WRITE_CUSTOMER_DATA, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_WRITE_CUSTOMER_DATA,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "read_cust_data", .id = eLK204T71U_READ_CUSTOMER_DATA, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_READ_CUSTOMER_DATA
    },
    {
        .name = "read_version_num", .id = eLK204T71U_READ_VERSION_NUMBER, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_READ_VERSION_NUMBER
    },
    {
        .name = "read_module_type", .id = eLK204T71U_READ_MODULE_TYPE, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = LK204T71U_READ_MODULE_TYPE
    },
    {
        .name = "write_text", .id = eLK204T71U_WRITE_TEXT, 
        .type = eDEVMAN_ATTR_STRING, .size = 80, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), 
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "init", .id = eLK204T71U_INIT, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32),
        .format = "%s",
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "config_test", .id = eLK204T71U_CONFIGURE_TEST, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "run_test", .id = eLK204T71U_RUN_TEST, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, 
        .size = sizeof(board_dev_stats_t), 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
};

static ccl_err_t _ftemac_cmd_startup_screen(devo_t devo, 
                                            __attribute__((unused)) device_cmd_parm_t parms[], 
                                            __attribute__((unused)) b_u16 parms_num)
{
    void *p_priv = devman_device_private(devo);
    char str[] = "                      CELARE cGate-100                                         ";

    _ret = _lk2047t1u_reg_write(p_priv, LK204T71U_CHANGE_STARTUP_SCREEN, 
                                sizeof(b_u16), 0, (b_u8 *)str, strlen(str));
    if ( _ret ) {
        PDEBUG("_lk2047t1u_reg_write error\n");
        LK204T71U_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

/* device commands */
static device_cmd_t _cmds[] = 
{
    { 
        .name = "startup_screen", .f_cmd = _ftemac_cmd_startup_screen, .parms_num = 0,
        .help = "Set startup screen" 
    }
};

/* Device instance hw-specific initializations 
 */
static ccl_err_t _flk2047t1u_add(void *devo, void *brdspec)
{
    lk2047t1u_ctx_t           *p_lk2047t1u;
    gen_i2c_dev_brdspec_t     *p_brdspec;

    if ( !devo || !brdspec ) {
        PDEBUG("Error! Invalid input parameters\n");
        LK204T71U_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_lk2047t1u = devman_device_private(devo);
    if ( !p_lk2047t1u ) {
        PDEBUG("NULL context area\n");
        LK204T71U_ERROR_RETURN(CCL_FAIL);
    }
    p_brdspec = (gen_i2c_dev_brdspec_t *)brdspec;

    PDEBUG("123$=> p_brdspec->i2c.bus_id=%d, dev_i2ca=0x%x\n", 
           p_brdspec->i2c.bus_id, p_brdspec->i2c.dev_i2ca);

    p_lk2047t1u->devo = devo;    
    memcpy(&p_lk2047t1u->brdspec, p_brdspec, sizeof(gen_i2c_dev_brdspec_t));

    return CCL_OK;
}


/* Device instance hw-specific deinitializations 
 */
static ccl_err_t _flk2047t1u_cut(void *devo)
{
    if ( !devo ) {
        PDEBUG("Error! Invalid input parameters\n");
        LK204T71U_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    return CCL_OK;
}

/* device type description structure */
static device_driver_t _lk2047t1u_driver = {
    .name = "lcd",
    .f_add = _flk2047t1u_add,
    .f_cut = _flk2047t1u_cut,
    .attrs = _attrs,
    .attrs_num = sizeof(_attrs)/sizeof(_attrs[0]),
    .cmds = _cmds,
    .cmds_num = sizeof(_cmds)/sizeof(_cmds[0]),
    .priv_size = sizeof(lk2047t1u_ctx_t)
}; 

/* Initialize LK204T71U device type
 */
ccl_err_t devlk2047t1u_init(void)
{
    /* register device type */
    _ret = devman_driver_register(&_lk2047t1u_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to register driver\n");
        LK204T71U_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

/* Deinitialize LK204T71U device type
 */
ccl_err_t devlk2047t1u_fini(void)
{
    /* unregister device type */
    _ret = devman_driver_unregister(&_lk2047t1u_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to unregister driver\n");
        LK204T71U_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}


