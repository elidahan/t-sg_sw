/********************************************************************************/
/**
 * @file trans.c
 * @brief TRANS (transceiver) device manager driver
 * @copyright 2019 CELARE. All rights reserved.
 */
/********************************************************************************/
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <semaphore.h>
#include <linux/limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "i2ccore.h"       
#include "trans.h"

//#define TRANS_DEBUG
/* printing/error-returning macros */
#ifdef TRANS_DEBUG
    #define PDEBUG(fmt, args...) \
            ccl_syslog_err("TRANS: %s[%d]: " fmt, __FUNCTION__, __LINE__, ## args)
#else
    #define PDEBUG(fmt, args...)
#endif
#define TRANS_ERROR_RETURN(err) do { \
   ccl_syslog_err("Error! %s[%d], error=%i\n", __FUNCTION__, __LINE__, (err)); \
   return err; \
} while (0)

/* transceiver offsets*/

#define TRANS_IS_PRESENT              0x0
/* qsfp lower space if any */
#define TRANS_LOW_ID                  0x0
#define TRANS_LOW_STATUS              0x1
/* qsfp lower interrupt flags */
#define TRANS_LOW_INTR_FLAG_LOS       0x03
#define TRANS_LOW_INTR_FLAG_FLT       0x04
#define TRANS_LOW_INTR_FLAG_TMP       0x06 
#define TRANS_LOW_INTR_FLAG_VCC       0x07 
#define TRANS_LOW_INTR_FLAG_RX12_PWR  0x09 
#define TRANS_LOW_INTR_FLAG_RX34_PWR  0x0a 
#define TRANS_LOW_INTR_FLAG_TX12_BIAS 0x0b 
#define TRANS_LOW_INTR_FLAG_TX34_BIAS 0x0c
/* qsfp lower control */
#define TRANS_LOW_CTRL_TX_DIS         0x56
#define TRANS_LOW_CTRL_RX_RS          0x57 
#define TRANS_LOW_CTRL_TX_RS          0x58
/* serial id attributes(defined by sfp msa) */
#define TRANS_MSA_RAW                 0x0  
#define TRANS_MSA_ID                  0x0 
#define TRANS_MSA_EXTENDED            0x01 
#define TRANS_MSA_CONNECTOR           0x02 
#define TRANS_MSA_TRANSCODE           0x03
#define TRANS_MSA_ENCODING            0x0b
#define TRANS_MSA_BR_NOMINAL          0x0c 
#define TRANS_MSA_LEN_9UM_KM          0x0e
#define TRANS_MSA_LEN_9UM             0x0f
#define TRANS_MSA_LEN_50UM            0x10
#define TRANS_MSA_LEN_625UM           0x11
#define TRANS_MSA_LEN_COPPER          0x12 
#define TRANS_MSA_VENDOR_NAME         0x14
#define TRANS_MSA_VENDOR_OUI          0x25
#define TRANS_MSA_VENDOR_PN           0x28
#define TRANS_MSA_VENDOR_REV          0x38
#define TRANS_MSA_WAVELENGTH          0x3c
#define TRANS_MSA_CC_BASE             0x3f
#define TRANS_MSA_OPTIONS             0x40
#define TRANS_MSA_BR_MAX              0x42
#define TRANS_MSA_BR_MIN              0x43
#define TRANS_MSA_VENDOR_SN           0x44
#define TRANS_MSA_DATE_CODE           0x54
#define TRANS_MSA_DDM_TYPE            0x5c
#define TRANS_MSA_ENHANCED_OPS        0x5d
#define TRANS_MSA_SFF_8472            0x5e
#define TRANS_MSA_CC_EXT              0x5f
#define TRANS_MSA_VENDOR_SPEC         0x60 
/* digital diagnostics attributes; alarm and warning threshold */
#define TRANS_DD_RAW                  0x0
#define TRANS_DD_TEMP_HIGH_ALARM      0x0
#define TRANS_DD_TEMP_LOW_ALARM       0x02
#define TRANS_DD_TEMP_HIGH_WARN       0x04
#define TRANS_DD_TEMP_LOW_WARN        0x06
#define TRANS_DD_VCC_HIGH_ALARM       0x08 
#define TRANS_DD_VCC_LOW_ALARM        0x0a
#define TRANS_DD_VCC_HIGH_WARN        0x0c
#define TRANS_DD_VCC_LOW_WARN         0x0e
#define TRANS_DD_BIAS_HIGH_ALARM      0x10
#define TRANS_DD_BIAS_LOW_ALARM       0x12
#define TRANS_DD_BIAS_HIGH_WARN       0x14
#define TRANS_DD_BIAS_LOW_WARN        0x16 
#define TRANS_DD_TX_PWR_HIGH_ALARM    0x18 
#define TRANS_DD_TX_PWR_LOW_ALARM     0x1a
#define TRANS_DD_TX_PWR_HIGH_WARN     0x1c
#define TRANS_DD_TX_PWR_LOW_WARN      0x1e
#define TRANS_DD_RX_PWR_HIGH_ALARM    0x20
#define TRANS_DD_RX_PWR_LOW_ALARM     0x22 
#define TRANS_DD_RX_PWR_HIGH_WARN     0x24 
#define TRANS_DD_RX_PWR_LOW_WARN      0x26
/* digital diagnostics attributes; calibration constants */
#define TRANS_DD_CAL_RX_PWR_4         0x38 
#define TRANS_DD_CAL_RX_PWR_3         0x3c 
#define TRANS_DD_CAL_RX_PWR_2         0x40 
#define TRANS_DD_CAL_RX_PWR_1         0x44 
#define TRANS_DD_CAL_RX_PWR_0         0x48 
#define TRANS_DD_CAL_TX_I_SLOPE       0x4c 
#define TRANS_DD_CAL_TX_I_OFFSET      0x4e 
#define TRANS_DD_CAL_TX_PWR_SLOPE     0x50 
#define TRANS_DD_CAL_TX_PWR_OFFSET    0x52 
#define TRANS_DD_CAL_T_SLOPE          0x54
#define TRANS_DD_CAL_T_OFFSET         0x56
#define TRANS_DD_CAL_V_SLOPE          0x58 
#define TRANS_DD_CAL_V_OFFSET         0x5a 
#define TRANS_DD_CAL_CHECKSUM         0x5f 
/* digital diagnostics attributes; a/d values and status */    
#define TRANS_DD_AD_TEMP              0x60 
#define TRANS_DD_AD_VCC               0x62 
#define TRANS_DD_AD_TX_BIAS           0x64
#define TRANS_DD_AD_TX_PWR            0x66
#define TRANS_DD_AD_RX_PWR            0x68
#define TRANS_DD_AD_STATUS_110        0x6e
#define TRANS_DD_AD_STATUS_112        0x70
#define TRANS_DD_AD_STATUS_113        0x71
#define TRANS_DD_AD_STATUS_116        0x74
#define TRANS_DD_AD_STATUS_117        0x75
#define TRANS_DD_AD_VENDOR_SPEC       0x78
#define TRANS_DD_USER_WRITABLE        0x80

#define TRANS_MAX_OFFSET TRANS_DD_USER_WRITABLE      

#define TRANS_PAGE_SIZE               128

#define MSA_VENDOR_NAME_OFFSET 20
#define MSA_VENDOR_PN_OFFSET   40
#define MSA_VENDOR_SN_OFFSET   68
#define MSA_VENDOR_NAME_LEN    16

/* the transceiver internal control structure */
typedef struct trans_ctx_t {
    void                *devo;
    trans_brdspec_t     brdspec;
    b_bool              is_present;
    b_bool              is_qsfp;
    b_u8                msa_offset;
    b_u8                dd_temperature_offset;
    b_u8                dd_vcc_offset;
    b_u8                dd_txbias_offset;
    b_u8                dd_txpower_offset;
    b_u8                dd_rxpower_offset;
    b_u32               channel_num;
} trans_ctx_t;

static ccl_err_t _ret;

/* trans register read */
static ccl_err_t _trans_reg_read(void *p_priv, b_u32 offset, 
                                 b_u32 offset_sz, b_u32 idx, 
                                 b_u8 *p_value, b_u32 size)
{
    trans_ctx_t   *p_trans;
    i2c_route_t      i2c_route;

    if ( !p_priv || offset > TRANS_MAX_OFFSET || 
         !p_value || idx || size != sizeof(b_u8) ) {
        PDEBUG("Error! Invalid input parameters\n");
        TRANS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    PDEBUGG("123$=> offset=0x%03x, size=%d\n", offset, size);

    p_trans = (trans_ctx_t *)p_priv;     
    memcpy(&i2c_route, &p_trans->brdspec.i2c.bus_id, sizeof(i2c_route_t));

    _ret = devi2ccore_read_buff(&i2c_route, p_trans->brdspec.extra_mux_pin, 
                                offset, offset_sz, p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to write over spi bus\n");
        TRANS_ERROR_RETURN(_ret);
    }

    return _ret;
}

/* trans register write */
static ccl_err_t _trans_reg_write(void *p_priv, b_u32 offset, 
                                  b_u32 offset_sz, b_u32 idx, 
                                  b_u8 *p_value, b_u32 size)
{
    trans_ctx_t   *p_trans;
    i2c_route_t   i2c_route;

    if ( !p_priv || offset > TRANS_MAX_OFFSET || 
         !p_value || idx || size != sizeof(b_u8) ) {
        PDEBUG("Error! Invalid input parameters\n");
        TRANS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    PDEBUG("123$=> offset=0x%03x, value=0x%02x\n", offset, *p_value);

    p_trans = (trans_ctx_t *)p_priv;     
    memcpy(&i2c_route, &p_trans->brdspec.i2c.bus_id, sizeof(i2c_route_t));

    _ret = devi2ccore_write_buff(&i2c_route, p_trans->brdspec.extra_mux_pin, 
                                 offset, offset_sz, p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to write over spi bus\n");
        TRANS_ERROR_RETURN(_ret);
    }

    return _ret;
}

/* trans presence read */
static ccl_err_t _trans_is_present_read(void *p_priv, b_u32 offset, 
                                        b_u32 offset_sz, b_u32 idx, 
                                        b_u8 *p_value, b_u32 size)
{
    trans_ctx_t   *p_trans;
    b_u8          regval = 0xff;
    i2c_route_t   i2c_route;

    if ( !p_priv || !p_value || idx || size != sizeof(b_u8) ) {
        PDEBUG("Error! Invalid input parameters\n");
        TRANS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    PDEBUGG("123$=> offset=0x%03x, size=%d\n", offset, size);

    p_trans = (trans_ctx_t *)p_priv;     
    memcpy(&i2c_route, &p_trans->brdspec.i2c.bus_id, sizeof(i2c_route_t));

    /* read over i2c bus */
    _ret = devi2ccore_read_buff(&i2c_route,
                                p_trans->brdspec.extra_mux_pin, 
                                offset, offset_sz, 
                                &regval, size);
    if ( _ret ) {
        *p_value = 0;
        PDEBUG("Error! Failed to read over spi bus\n");
        TRANS_ERROR_RETURN(_ret);
    }
    /* store the current presence state */
    *p_value = p_trans->is_present = ( regval == 3 ||    /*SFP*/
                                       regval == 0x0c || /*QSFP*/
                                       regval == 0x0d ); /*QSFP*/
    /* store the qsfp type flag */
    p_trans->is_qsfp = ( regval == 0x0c || regval == 0x0d );
    return CCL_OK;
}

/* trans 1000base-t type check */
static ccl_err_t _trans_is_1000base_t(trans_ctx_t *p_trans, int *is_1000base_t)
{
    b_u8          value = 0;
    i2c_route_t   i2c_route;

    if ( !p_trans || !is_1000base_t ) {
        PDEBUG("Error! Invalid input parameters\n");
        TRANS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    memcpy(&i2c_route, &p_trans->brdspec.i2c.bus_id, sizeof(i2c_route_t));
    /* read over i2c bus */
    _ret = devi2ccore_read_buff(&i2c_route, 
                                p_trans->brdspec.extra_mux_pin, 
                                0x6, sizeof(b_u8), 
                                &value, sizeof(b_u8));
    if ( _ret ) {
        PDEBUG("Error! Failed to read over i2c bus\n");
        TRANS_ERROR_RETURN(CCL_FAIL);
    }

    *is_1000base_t = (value == 0x08);
    PDEBUGG("123$=> value=0x%x,p_trans->devo=%p\n",value,p_trans->devo);   
    return CCL_OK;
}

/* trans digital diagnostics monitoring type read */
static ccl_err_t _trans_ddm_type_read(trans_ctx_t *p_trans, b_u8 *p_value)
{
    i2c_route_t   i2c_route;

    if ( !p_trans || !p_value ) {
        PDEBUG("Error! Invalid input parameters\n");
        TRANS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    memcpy(&i2c_route, &p_trans->brdspec.i2c.bus_id, sizeof(i2c_route_t));

    /* read over i2c bus */
    _ret = devi2ccore_read_buff(&i2c_route, 
                                p_trans->brdspec.extra_mux_pin, 
                                0x5c, sizeof(b_u8), 
                                p_value, sizeof(b_u8));
    if ( _ret ) {
        PDEBUG("Error! Failed to read over i2c bus\n");
        TRANS_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

/* qsfp low area buffer read */
static ccl_err_t _trans_low_buf_read(void *p_priv, b_u32 offset, 
                                     b_u32 offset_sz, b_u32 idx, 
                                     b_u8 *p_value, b_u32 size)
{
    trans_ctx_t   *p_trans;
    i2c_route_t   i2c_route;

    if ( !p_priv || !p_value || idx ) {
        PDEBUG("Error! Invalid input parameters\n");
        TRANS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    PDEBUG("123$=> offset=0x%08x, size=%d\n", offset, size);

    p_trans = (trans_ctx_t *)p_priv;  
    memcpy(&i2c_route, &p_trans->brdspec.i2c.bus_id, sizeof(i2c_route_t));

    /* read buffer over i2c bus */
    _ret = devi2ccore_read_buff(&i2c_route, 
                                p_trans->brdspec.extra_mux_pin, 
                                offset, offset_sz, 
                                p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to read over i2c bus\n");
        TRANS_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

/* qsfp low area buffer write */
static ccl_err_t _trans_low_buf_write(void *p_priv, b_u32 offset, 
                                      b_u32 offset_sz, b_u32 idx, 
                                      b_u8 *p_value, b_u32 size)
{
    trans_ctx_t   *p_trans;
    i2c_route_t   i2c_route;

    if ( !p_priv || !p_value || idx ) {
        PDEBUG("Error! Invalid input parameters\n");
        TRANS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    PDEBUG("123$=> offset=0x%08x, size=%d\n", offset, size);
    /*123$; don't give the eprom tx_dis access */
    if ( offset == 0x56 ) {
        return CCL_OK;
    }

    p_trans = (trans_ctx_t *)p_priv;  
    memcpy(&i2c_route, &p_trans->brdspec.i2c.bus_id, sizeof(i2c_route_t));

    /* write buffer over i2c bus */
    _ret = devi2ccore_write_buff(&i2c_route, 
                                 p_trans->brdspec.extra_mux_pin, 
                                 offset, offset_sz, 
                                 p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to read over i2c bus\n");
        TRANS_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

/* trans buffer read */
static ccl_err_t _trans_msa_buf_read(void *p_priv, b_u32 offset, 
                                     b_u32 offset_sz, b_u32 idx, 
                                     b_u8 *p_value, b_u32 size)
{
    trans_ctx_t   *p_trans;
    i2c_route_t   i2c_route;

    if ( !p_priv || !p_value || idx ) {
        PDEBUG("Error! Invalid input parameters\n");
        TRANS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    PDEBUGG("123$=> offset=0x%08x, size=%d\n", offset, size);

    p_trans = (trans_ctx_t *)p_priv;  
    memcpy(&i2c_route, &p_trans->brdspec.i2c.bus_id, sizeof(i2c_route_t));

    offset += p_trans->msa_offset;
    /* read buffer over i2c bus */
    _ret = devi2ccore_read_buff(&i2c_route, 
                                p_trans->brdspec.extra_mux_pin, 
                                offset, offset_sz, 
                                p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to read over i2c bus\n");
        TRANS_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

/* trans string read */
static ccl_err_t _trans_msa_str_read(void *p_priv, b_u32 offset, 
                                     b_u32 offset_sz, b_u32 idx, 
                                     b_u8 *p_value, b_u32 size)
{
    trans_ctx_t   *p_trans;
    i2c_route_t   i2c_route;

    if ( !p_priv || !p_value || idx ) {
        PDEBUG("Error! Invalid input parameters\n");
        TRANS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    PDEBUGG("123$=> offset=0x%03x, size=%d\n", offset, size);

    p_trans = (trans_ctx_t *)p_priv;  
    memcpy(&i2c_route, &p_trans->brdspec.i2c.bus_id, sizeof(i2c_route_t));

    /* read buffer over i2c bus */
    _ret = devi2ccore_read_buff(&i2c_route, 
                                p_trans->brdspec.extra_mux_pin, 
                                offset, offset_sz, 
                                p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to read over i2c bus\n");
        TRANS_ERROR_RETURN(CCL_FAIL);
    }

    p_value[size] = 0;

    return CCL_OK;
}

/* trans buffer read */
static ccl_err_t _trans_dd_buf_read(void *p_priv, b_u32 offset, 
                                    b_u32 offset_sz, b_u32 idx, 
                                    b_u8 *p_value, b_u32 size)
{
    trans_ctx_t   *p_trans;
    i2c_route_t    i2c_route;
    b_u8           ddm_type = 0;

    if ( !p_priv || !p_value || idx ) {
        PDEBUG("Error! Invalid input parameters\n");
        TRANS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    PDEBUGG("123$=> offset=0x%08x, size=%d\n", offset, size);

    p_trans = (trans_ctx_t *)p_priv;  

    if (!p_trans->is_qsfp) {
        /* check if the digital diagnostics are implemented for the transciever */
        _ret = _trans_ddm_type_read(p_trans, &ddm_type);
        if ( _ret ) {
            PDEBUG("Error! Failed to read ddm_type over i2c bus\n");
            TRANS_ERROR_RETURN(CCL_FAIL);
        }
        if ( !ddm_type ) {
            PDEBUG("Warning! Digital Diagnosticas aren't implemented\n");
            return CCL_NOT_FOUND;
        }
    }

    /* use extra i2c address instead of primary */
    memcpy(&i2c_route, &p_trans->brdspec.i2c.bus_id, sizeof(i2c_route_t));
    i2c_route.dev_i2ca = p_trans->brdspec.extra_i2ca;

    /* read buffer over i2c bus */
    _ret = devi2ccore_read_buff(&i2c_route, 
                                p_trans->brdspec.extra_mux_pin, 
                                offset, offset_sz, 
                                p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to read dd register over i2c bus\n");
        TRANS_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

/* trans dd little endian 2bytes read */
static ccl_err_t _trans_dd_lsb_2b_read(void *p_priv, b_u32 offset, 
                                       b_u32 offset_sz, b_u32 idx, 
                                       b_u8 *p_value, b_u32 size)
{
    trans_ctx_t   *p_trans;
    i2c_route_t    i2c_route;
    b_u8           ddm_type = 0;    
    b_u16          val;

    if ( !p_priv || !p_value || idx ) {
        PDEBUG("Error! Invalid input parameters\n");
        TRANS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    PDEBUGG("123$=> offset=0x%08x, size=%d\n", offset, size);

    p_trans = (trans_ctx_t *)p_priv;  

    if (!p_trans->is_qsfp) {
        /* check if the digital diagnostics are implemented for the transciever */
        _ret = _trans_ddm_type_read(p_trans, &ddm_type);
        if ( _ret ) {
            PDEBUG("Error! Failed to read ddm_type over i2c bus\n");
            TRANS_ERROR_RETURN(CCL_FAIL);
        }
        if ( !ddm_type ) {
            PDEBUG("Warning! Digital Diagnosticas aren't implemented\n");
            return CCL_NOT_FOUND;
        }
    }

    /* use extra i2c adress instead of primary */
    memcpy(&i2c_route, &p_trans->brdspec.i2c.bus_id, sizeof(i2c_route_t));
    i2c_route.dev_i2ca = p_trans->brdspec.extra_i2ca;

    /* read buffer over i2c bus */
    _ret = devi2ccore_read_buff(&i2c_route, 
                                p_trans->brdspec.extra_mux_pin, 
                                offset, offset_sz, 
                                (b_u8 *)&val, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to read over i2c bus\n");
        TRANS_ERROR_RETURN(CCL_FAIL);
    }
    val = (val >> 8)|(val << 8);
    memcpy(p_value, &val, sizeof(b_u16));
    return CCL_OK;
}
/* trans dd big endian 2 bytes read */
static ccl_err_t _trans_dd_msb_2b_read(void *p_priv, b_u32 offset, 
                                       b_u32 offset_sz, b_u32 idx, 
                                       b_u8 *p_value, b_u32 size)
{
    trans_ctx_t   *p_trans;
    i2c_route_t    i2c_route;
    b_u8           ddm_type = 0;
    b_u16          val;

    if ( !p_priv || !p_value || idx ) {
        PDEBUG("Error! Invalid input parameters\n");
        TRANS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    PDEBUGG("123$=> offset=0x%08x, size=%d\n", offset, size);

    p_trans = (trans_ctx_t *)p_priv;  

    if (!p_trans->is_qsfp) {
        /* check if the digital diagnostics are implemented for the transciever */
        _ret = _trans_ddm_type_read(p_trans, &ddm_type);
        if ( _ret ) {
            PDEBUG("Error! Failed to read ddm_type over i2c bus\n");
            TRANS_ERROR_RETURN(CCL_FAIL);
        }
        if ( !ddm_type ) {
            PDEBUG("Warning! Digital Diagnosticas aren't implemented\n");
            return CCL_NOT_FOUND;
        }
    }

    /* use extra i2c adress instead of primary */
    memcpy(&i2c_route, &p_trans->brdspec.i2c.bus_id, sizeof(i2c_route_t));
    i2c_route.dev_i2ca = p_trans->brdspec.extra_i2ca;

    /* read buffer over i2c bus */
    _ret = devi2ccore_read_buff(&i2c_route, 
                                p_trans->brdspec.extra_mux_pin, 
                                offset, offset_sz, 
                                (b_u8 *)&val, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to read over i2c bus\n");
        TRANS_ERROR_RETURN(CCL_FAIL);
    }
    memcpy(p_value, &val, sizeof(b_u16));
    return CCL_OK;
}
/* trans dd big endian 4 bytes read */
static ccl_err_t _trans_dd_msb_4b_read(void *p_priv, b_u32 offset, 
                                       b_u32 offset_sz, b_u32 idx, 
                                       b_u8 *p_value, b_u32 size)
{
    trans_ctx_t   *p_trans;
    i2c_route_t    i2c_route;
    b_u8           ddm_type = 0;
    b_u32          val;

    if ( !p_priv || !p_value || idx ) {
        PDEBUG("Error! Invalid input parameters\n");
        TRANS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    PDEBUGG("123$=> offset=0x%08x, size=%d\n", offset, size);

    p_trans = (trans_ctx_t *)p_priv;  

    if (!p_trans->is_qsfp) {
        /* check if the digital diagnostics are implemented for the transciever */
        _ret = _trans_ddm_type_read(p_trans, &ddm_type);
        if ( _ret ) {
            PDEBUG("Error! Failed to read ddm_type over i2c bus\n");
            TRANS_ERROR_RETURN(CCL_FAIL);
        }
        if ( !ddm_type ) {
            PDEBUG("Warning! Digital Diagnosticas aren't implemented\n");
            return CCL_NOT_FOUND;
        }
    }

    /* use extra i2c adress instead of primary */
    memcpy(&i2c_route, &p_trans->brdspec.i2c.bus_id, sizeof(i2c_route_t));
    i2c_route.dev_i2ca = p_trans->brdspec.extra_i2ca;

    /* read buffer over i2c bus */
    _ret = devi2ccore_read_buff(&i2c_route, 
                                p_trans->brdspec.extra_mux_pin, 
                                offset, offset_sz, 
                                (b_u8 *)&val, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to read over i2c bus\n");
        TRANS_ERROR_RETURN(CCL_FAIL);
    }
    memcpy(p_value, &val, sizeof(b_u32));
    return CCL_OK;
}

static ccl_err_t _trans_media_decode(void *p_priv, b_u32 offset, 
                                     b_u32 offset_sz, b_u32 idx, 
                                     b_u8 *p_value, b_u32 size)
{
    trans_ctx_t      *p_trans;
    b_u8             buf[TRANS_PAGE_SIZE];
    media_decoder_t  *media_info;
    b_u16            temp;
    b_u16            vcc;
    b_u16            tx_bias[CHANNELS_PER_PORT_MAXNUM];
    b_u16            tx_pwr[CHANNELS_PER_PORT_MAXNUM];
    b_u16            rx_pwr[CHANNELS_PER_PORT_MAXNUM];
    b_i32            i;

    if ( !p_priv || !p_value || idx ||
         size != sizeof(media_decoder_t) ) {
        PDEBUG("Error! Invalid input parameters\n");
        TRANS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_trans = (trans_ctx_t *)p_priv;  
    media_info = (media_decoder_t *)p_value;

    _ret = _trans_msa_buf_read(p_priv, offset, offset_sz, 
                               idx, buf, TRANS_PAGE_SIZE);       
    if ( _ret ) {
        PDEBUG("Error! _trans_msa_buf_read\n");
        TRANS_ERROR_RETURN(CCL_FAIL);
    }
    strncpy(media_info->vendor_name, &buf[MSA_VENDOR_NAME_OFFSET], MSA_VENDOR_NAME_LEN);
    strncpy(media_info->vendor_pn, &buf[MSA_VENDOR_PN_OFFSET], MSA_VENDOR_NAME_LEN);
    strncpy(media_info->vendor_sn, &buf[MSA_VENDOR_SN_OFFSET], MSA_VENDOR_NAME_LEN);
    _ret = _trans_dd_buf_read(p_priv, offset, offset_sz, 
                              idx, buf, TRANS_PAGE_SIZE);       
    if ( _ret ) {
        PDEBUG("Error! _trans_dd_buf_read\n");
        TRANS_ERROR_RETURN(CCL_FAIL);
    }
    media_info->channel_num = p_trans->channel_num;
    temp = buf[p_trans->dd_temperature_offset] << 8 | buf[p_trans->dd_temperature_offset+1];
    media_info->temperature = (b_double64)ccl_2s_compl_to_decimal((b_u32)temp, 16) / 256;
    vcc = buf[p_trans->dd_vcc_offset] << 8 | buf[p_trans->dd_vcc_offset+1];
    media_info->vcc = (b_double64)vcc/1000000;
    for (i = 0; i < p_trans->channel_num; i++) {
        tx_bias[i] = buf[p_trans->dd_txbias_offset+i*2] << 8 | 
                  buf[p_trans->dd_txbias_offset+i*2+1];
        tx_pwr[i] = buf[p_trans->dd_txpower_offset+i*2] << 8 | 
                 buf[p_trans->dd_txpower_offset+i*2+1];
        rx_pwr[i] = buf[p_trans->dd_rxpower_offset+i*2] << 8 | 
                 buf[p_trans->dd_rxpower_offset+i*2+1];
        media_info->tx_bias[i] = (b_double64)tx_bias[i]/1000000;
        media_info->tx_power[i] = (b_double64)tx_pwr[i]/1000000;
        media_info->rx_power[i] = (b_double64)rx_pwr[i]/1000000;
    }


    return CCL_OK;
}

static ccl_err_t _trans_init(void *p_priv)
{
    trans_ctx_t   *p_trans;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        TRANS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_trans = (trans_ctx_t *)p_priv;

    return CCL_OK;
}

static ccl_err_t _trans_configure_test(void *p_priv)
{
    trans_ctx_t   *p_trans;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        TRANS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_trans = (trans_ctx_t *)p_priv;

    return CCL_OK;
}

static ccl_err_t _trans_run_test(void *p_priv, 
                                 __attribute__((unused)) b_u8 *buf, 
                                 __attribute__((unused)) b_u32 size)
{
    trans_ctx_t   *p_trans;
    b_u8          buffer[128];
    b_u32         i;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        TRANS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_trans = (trans_ctx_t *)p_priv;

    _ret = _trans_msa_buf_read(p_trans, TRANS_MSA_RAW, 1, 
                               0, buffer, 128);                       
    if (_ret) 
        TRANS_ERROR_RETURN(_ret);

    return CCL_OK;
}

ccl_err_t trans_attr_read(void *p_priv, b_u32 attrid, 
                          b_u32 offset, b_u32 offset_sz, 
                          b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || offset > TRANS_MAX_OFFSET || 
         !p_value || idx ) {
        PDEBUG("Error! Invalid input parameters\n");
        TRANS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch (attrid) {
    case eTRANS_IS_PRESENT:
        _ret = _trans_is_present_read(p_priv, offset, offset_sz, 
                                      idx, p_value, size);
        break;                  
    case eTRANS_LOW_ID:    
    case eTRANS_LOW_STATUS:              
    case eTRANS_LOW_INTR_FLAG_LOS:       
    case eTRANS_LOW_INTR_FLAG_FLT:       
    case eTRANS_LOW_INTR_FLAG_TMP:       
    case eTRANS_LOW_INTR_FLAG_VCC:       
    case eTRANS_LOW_INTR_FLAG_RX12_PWR:  
    case eTRANS_LOW_INTR_FLAG_RX34_PWR:  
    case eTRANS_LOW_INTR_FLAG_TX12_BIAS: 
    case eTRANS_LOW_INTR_FLAG_TX34_BIAS: 
    case eTRANS_LOW_CTRL_TX_DIS:         
    case eTRANS_LOW_CTRL_RX_RS:          
    case eTRANS_LOW_CTRL_TX_RS:          
        _ret = _trans_low_buf_read(p_priv, offset, offset_sz, 
                                   idx, p_value, size);              
        break;
    case eTRANS_MSA_RAW:                 
    case eTRANS_MSA_ID:                  
    case eTRANS_MSA_EXTENDED:            
    case eTRANS_MSA_CONNECTOR:           
    case eTRANS_MSA_TRANSCODE:           
    case eTRANS_MSA_ENCODING:            
    case eTRANS_MSA_BR_NOMINAL:          
    case eTRANS_MSA_LEN_9UM_KM:          
    case eTRANS_MSA_LEN_9UM:             
    case eTRANS_MSA_LEN_50UM:            
    case eTRANS_MSA_LEN_625UM:           
    case eTRANS_MSA_LEN_COPPER: 
    case eTRANS_MSA_VENDOR_OUI:          
    case eTRANS_MSA_WAVELENGTH:          
    case eTRANS_MSA_CC_BASE:             
    case eTRANS_MSA_OPTIONS:             
    case eTRANS_MSA_BR_MAX:              
    case eTRANS_MSA_BR_MIN:              
    case eTRANS_MSA_DDM_TYPE:            
    case eTRANS_MSA_ENHANCED_OPS:        
    case eTRANS_MSA_SFF_8472:            
    case eTRANS_MSA_CC_EXT:              
    case eTRANS_MSA_VENDOR_SPEC:         
        _ret = _trans_msa_buf_read(p_priv, offset, offset_sz, 
                                   idx, p_value, size);                       
        break;
    case eTRANS_MSA_VENDOR_NAME:         
    case eTRANS_MSA_VENDOR_PN:           
    case eTRANS_MSA_VENDOR_REV:          
    case eTRANS_MSA_VENDOR_SN:           
    case eTRANS_MSA_DATE_CODE:           
        _ret = _trans_msa_str_read(p_priv, offset, offset_sz, 
                                   idx, p_value, size);                       
        break;
    case eTRANS_DD_RAW:                  
    case eTRANS_DD_CAL_CHECKSUM:         
    case eTRANS_DD_AD_STATUS_110:        
    case eTRANS_DD_AD_STATUS_112:        
    case eTRANS_DD_AD_STATUS_113:        
    case eTRANS_DD_AD_STATUS_116:        
    case eTRANS_DD_AD_STATUS_117:        
    case eTRANS_DD_AD_VENDOR_SPEC:       
    case eTRANS_DD_USER_WRITABLE:          
        _ret = _trans_dd_buf_read(p_priv, offset, offset_sz, 
                                  idx, p_value, size);                       
        break;
    case eTRANS_DD_TEMP_HIGH_ALARM:      
    case eTRANS_DD_TEMP_LOW_ALARM:       
    case eTRANS_DD_TEMP_HIGH_WARN:       
    case eTRANS_DD_TEMP_LOW_WARN:        
    case eTRANS_DD_VCC_HIGH_ALARM:       
    case eTRANS_DD_VCC_LOW_ALARM:        
    case eTRANS_DD_VCC_HIGH_WARN:        
    case eTRANS_DD_VCC_LOW_WARN:         
    case eTRANS_DD_BIAS_HIGH_ALARM:      
    case eTRANS_DD_BIAS_LOW_ALARM:       
    case eTRANS_DD_BIAS_HIGH_WARN:       
    case eTRANS_DD_BIAS_LOW_WARN:        
    case eTRANS_DD_TX_PWR_HIGH_ALARM:    
    case eTRANS_DD_TX_PWR_LOW_ALARM:     
    case eTRANS_DD_TX_PWR_HIGH_WARN:     
    case eTRANS_DD_TX_PWR_LOW_WARN:      
    case eTRANS_DD_RX_PWR_HIGH_ALARM:    
    case eTRANS_DD_RX_PWR_LOW_ALARM:     
    case eTRANS_DD_RX_PWR_HIGH_WARN:     
    case eTRANS_DD_RX_PWR_LOW_WARN:      
    case eTRANS_DD_CAL_TX_I_SLOPE:       
    case eTRANS_DD_CAL_TX_I_OFFSET:      
    case eTRANS_DD_CAL_TX_PWR_SLOPE:     
    case eTRANS_DD_CAL_TX_PWR_OFFSET:    
    case eTRANS_DD_CAL_T_SLOPE:          
    case eTRANS_DD_CAL_T_OFFSET:         
    case eTRANS_DD_CAL_V_SLOPE:          
    case eTRANS_DD_CAL_V_OFFSET:         
    case eTRANS_DD_AD_TEMP:              
    case eTRANS_DD_AD_VCC:               
    case eTRANS_DD_AD_TX_BIAS:           
    case eTRANS_DD_AD_TX_PWR:            
    case eTRANS_DD_AD_RX_PWR:            
        _ret = _trans_dd_msb_2b_read(p_priv, offset, offset_sz, 
                                     idx, p_value, size);                       
        break;
    case eTRANS_DD_CAL_RX_PWR_4:         
    case eTRANS_DD_CAL_RX_PWR_3:         
    case eTRANS_DD_CAL_RX_PWR_2:         
    case eTRANS_DD_CAL_RX_PWR_1:         
    case eTRANS_DD_CAL_RX_PWR_0:         
        _ret = _trans_dd_msb_4b_read(p_priv, offset, offset_sz, 
                                     idx, p_value, size);                       
        break;
    case eTRANS_MEDIA_DECODE:
        _ret = _trans_media_decode(p_priv, offset, offset_sz, 
                                   idx, p_value, size);
        break;
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK) {
        PDEBUG("Failure in attribute id: %d\n", attrid);
    }

    return _ret;               
}

ccl_err_t trans_attr_write(void *p_priv, b_u32 attrid, 
                           b_u32 offset, b_u32 offset_sz, 
                           b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || offset > TRANS_MAX_OFFSET || 
         !p_value || idx || size != sizeof(b_u32) ) {
        PDEBUG("Error! Invalid input parameters\n");
        TRANS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch (attrid) {
    case eTRANS_LOW_CTRL_TX_DIS:         
    case eTRANS_LOW_CTRL_RX_RS:          
    case eTRANS_LOW_CTRL_TX_RS:          
        _ret = _trans_low_buf_write(p_priv, offset, offset_sz, 
                                    idx, p_value, size);              
        break;
    case eTRANS_INIT:
        _ret = _trans_init(p_priv);
        break;
    case eTRANS_CONFIGURE_TEST:
        _ret = _trans_configure_test(p_priv);
        break;
    case eTRANS_RUN_TEST:
        _ret = _trans_run_test(p_priv, p_value, size);
        break;
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK) {
        PDEBUG("Failure in attribute id: %d\n", attrid);
    }

    return _ret;               
}

/* transciever attributes definition */
static device_attr_t _attrs[] = {
    {
        .name = "i2c_bus", 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = offsetof(struct trans_ctx_t, brdspec.i2c.bus_id),
        .format = "%d"
    },
    {
        .name = "i2c_addr", 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = offsetof(struct trans_ctx_t, brdspec.i2c.dev_i2ca),
        .format = "0x%02x"
    },
    {
        .name = "i2c_mux_addr", 
        .type = eDEVMAN_ATTR_HALF, .maxnum = DEVBRD_I2C_MUXES_IN_CHAIN_MAXNUM, 
        .offset_sz = sizeof(b_u8), .offset = offsetof(struct trans_ctx_t, brdspec.i2c.mux_i2ca), 
        .format = "0x%02x"
    },
    {
        .name = "i2c_mux_pin", 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = DEVBRD_I2C_MUXES_IN_CHAIN_MAXNUM, 
        .offset_sz = sizeof(b_u8), .offset = offsetof(struct trans_ctx_t, brdspec.i2c.mux_pin), 
        .format = "%d"
    },
    {
        .name = "is_present", .id = eTRANS_IS_PRESENT,
        .type = eDEVMAN_ATTR_BOOL, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_IS_PRESENT
    },
    /* qsfp lower space if any */
    {
        .name = "low_id", .id = eTRANS_LOW_ID, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8) , .offset = TRANS_LOW_ID
    },
    {
        .name = "low_status", .id = eTRANS_LOW_STATUS, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = TRANS_LOW_STATUS
    },
    /* qsfp lower interrupt flags */
    {
        .name = "low_int_flag_los", .id = eTRANS_LOW_INTR_FLAG_LOS,
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_LOW_INTR_FLAG_LOS
    },
    {
        .name = "low_int_flag_flt", .id = eTRANS_LOW_INTR_FLAG_FLT, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_LOW_INTR_FLAG_FLT
    },
    {
        .name = "low_int_flag_temp", .id = eTRANS_LOW_INTR_FLAG_TMP, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_LOW_INTR_FLAG_TMP
    },
    {
        .name = "low_int_flag_vcc", .id = eTRANS_LOW_INTR_FLAG_VCC, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_LOW_INTR_FLAG_VCC
    },
    {
        .name = "low_int_flag_rx12_pwr", .id = eTRANS_LOW_INTR_FLAG_RX12_PWR, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_LOW_INTR_FLAG_RX12_PWR
    },
    {
        .name = "low_int_flag_rx34_pwr", .id = eTRANS_LOW_INTR_FLAG_RX34_PWR, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_LOW_INTR_FLAG_RX34_PWR
    },
    {
        .name = "low_int_flag_tx12_bias", .id = eTRANS_LOW_INTR_FLAG_TX12_BIAS, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_LOW_INTR_FLAG_TX12_BIAS
    },
    {
        .name = "low_int_flag_tx34_bias", .id = eTRANS_LOW_INTR_FLAG_TX34_BIAS, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_LOW_INTR_FLAG_TX34_BIAS
    },
    /* qsfp lower control */
    {
        .name = "low_ctrl_tx_dis", .id = eTRANS_LOW_CTRL_TX_DIS, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_LOW_CTRL_TX_DIS
    },
    {
        .name = "low_ctrl_rx_rs", .id = eTRANS_LOW_CTRL_RX_RS, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_LOW_CTRL_RX_RS
    },
    {
        .name = "low_ctrl_tx_rs", .id = eTRANS_LOW_CTRL_TX_RS, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_LOW_CTRL_TX_RS
    },
    /* serial id attributes(defined by sfp msa) */
    {
        .name = "msa_raw", .id = eTRANS_MSA_RAW, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = 128, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_MSA_RAW,
        .flags = (eDEVMAN_ATTRFLAG_HIDDEN|eDEVMAN_ATTRFLAG_RO)
    },
    {
        .name = "msa_identifier", .id = eTRANS_MSA_ID, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_MSA_ID,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "msa_extended", .id = eTRANS_MSA_EXTENDED, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_MSA_EXTENDED,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "msa_connector", .id = eTRANS_MSA_CONNECTOR, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_MSA_CONNECTOR,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "msa_transcode", .id = eTRANS_MSA_TRANSCODE, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = 8, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_MSA_TRANSCODE,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "msa_encoding", .id = eTRANS_MSA_ENCODING, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_MSA_ENCODING,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "msa_br_nominal", .id = eTRANS_MSA_BR_NOMINAL, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_MSA_BR_NOMINAL,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "msa_len_9um_km", .id = eTRANS_MSA_LEN_9UM_KM, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_MSA_LEN_9UM_KM,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "msa_len_9um", .id = eTRANS_MSA_LEN_9UM, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_MSA_LEN_9UM,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "msa_len_50um", .id = eTRANS_MSA_LEN_50UM, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_MSA_LEN_50UM,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "msa_len_625um", .id = eTRANS_MSA_LEN_625UM, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_MSA_LEN_625UM,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "msa_len_copper", .id = eTRANS_MSA_LEN_COPPER, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_MSA_LEN_COPPER,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "msa_vendor_name", .id = eTRANS_MSA_VENDOR_NAME, 
        .type = eDEVMAN_ATTR_STRING, .size = 16, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_MSA_VENDOR_NAME,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "msa_vendor_oui", .id = eTRANS_MSA_VENDOR_OUI, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = 3, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_MSA_VENDOR_OUI,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "msa_vendor_pn", .id = eTRANS_MSA_VENDOR_PN, 
        .type = eDEVMAN_ATTR_STRING, .size = 16, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_MSA_VENDOR_PN,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "msa_vendor_rev", .id = eTRANS_MSA_VENDOR_REV, 
        .type = eDEVMAN_ATTR_STRING, .size = 4, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_MSA_VENDOR_REV,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "msa_wavelength", .id = eTRANS_MSA_WAVELENGTH, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_MSA_WAVELENGTH,
        .flags = eDEVMAN_ATTRFLAG_RO,
        .format = "%d"
    },
    {
        .name = "msa_cc_base", .id = eTRANS_MSA_CC_BASE, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_MSA_CC_BASE,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "msa_options", .id = eTRANS_MSA_OPTIONS, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = TRANS_MSA_OPTIONS,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "msa_br_max", .id = eTRANS_MSA_BR_MAX, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_MSA_BR_MAX,
        .flags = eDEVMAN_ATTRFLAG_RO,
        .format = "%d"
    },
    {
        .name = "msa_br_min", .id = eTRANS_MSA_BR_MIN, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_MSA_BR_MIN,
        .flags = eDEVMAN_ATTRFLAG_RO,
        .format = "%d"
    },
    {
        .name = "msa_vendor_sn", .id = eTRANS_MSA_VENDOR_SN, 
        .type = eDEVMAN_ATTR_STRING, .size = 16, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_MSA_VENDOR_SN,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "msa_date_code", .id = eTRANS_MSA_DATE_CODE, 
        .type = eDEVMAN_ATTR_STRING, .maxnum = 1, .size = 8, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_MSA_DATE_CODE,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "msa_ddm_type", .id = eTRANS_MSA_DDM_TYPE, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_MSA_DDM_TYPE,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "msa_enhanced_ops", .id = eTRANS_MSA_ENHANCED_OPS, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_MSA_ENHANCED_OPS,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "msa_sff_8472", .id = eTRANS_MSA_SFF_8472, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_MSA_SFF_8472,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "msa_cc_ext", .id = eTRANS_MSA_CC_EXT, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_MSA_CC_EXT,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "msa_vendor_spec", .id = eTRANS_MSA_VENDOR_SPEC, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = 32, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_MSA_VENDOR_SPEC,
        .flags = eDEVMAN_ATTRFLAG_RO
    },

    /* digital diagnostics attributes; alarm and warning threshold */
    {
        .name = "dd_raw", .id = eTRANS_DD_RAW, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = 128, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_DD_RAW,
        .flags = (eDEVMAN_ATTRFLAG_HIDDEN|eDEVMAN_ATTRFLAG_RO)
    },
    {
        .name = "dd_temp_hi_alarm", .id = eTRANS_DD_TEMP_HIGH_ALARM, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = TRANS_DD_TEMP_HIGH_ALARM,
        .flags = eDEVMAN_ATTRFLAG_RO
    },    
    {
        .name = "dd_temp_lo_alarm", .id = eTRANS_DD_TEMP_LOW_ALARM, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = TRANS_DD_TEMP_LOW_ALARM,
        .flags = eDEVMAN_ATTRFLAG_RO
    },    
    {
        .name = "dd_temp_hi_warn", .id = eTRANS_DD_TEMP_HIGH_WARN, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = TRANS_DD_TEMP_HIGH_WARN,
        .flags = eDEVMAN_ATTRFLAG_RO
    },    
    {
        .name = "dd_temp_lo_warn", .id = eTRANS_DD_TEMP_LOW_WARN, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = TRANS_DD_TEMP_LOW_WARN,
        .flags = eDEVMAN_ATTRFLAG_RO
    },    
    {
        .name = "dd_vcc_hi_alarm", .id = eTRANS_DD_VCC_HIGH_ALARM, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = TRANS_DD_VCC_HIGH_ALARM,
        .flags = eDEVMAN_ATTRFLAG_RO
    },    
    {
        .name = "dd_vcc_lo_alarm", .id = eTRANS_DD_VCC_LOW_ALARM, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = TRANS_DD_VCC_LOW_ALARM,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "dd_vcc_hi_warn", .id = eTRANS_DD_VCC_HIGH_WARN, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = TRANS_DD_VCC_HIGH_WARN,
        .flags = eDEVMAN_ATTRFLAG_RO
    },    
    {
        .name = "dd_vcc_lo_warn", .id = eTRANS_DD_VCC_LOW_WARN, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = TRANS_DD_VCC_LOW_WARN,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "dd_bias_hi_alarm", .id = eTRANS_DD_BIAS_HIGH_ALARM, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = TRANS_DD_BIAS_HIGH_ALARM,
        .flags = eDEVMAN_ATTRFLAG_RO
    },    
    {
        .name = "dd_bias_lo_alarm", .id = eTRANS_DD_BIAS_LOW_ALARM, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = TRANS_DD_BIAS_LOW_ALARM,
        .flags = eDEVMAN_ATTRFLAG_RO
    },    
    {
        .name = "dd_bias_hi_warn", .id = eTRANS_DD_BIAS_HIGH_WARN, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = TRANS_DD_BIAS_HIGH_WARN,
        .flags = eDEVMAN_ATTRFLAG_RO
    },    
    {
        .name = "dd_bias_lo_warn", .id = eTRANS_DD_BIAS_LOW_WARN, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = TRANS_DD_BIAS_LOW_WARN,
        .flags = eDEVMAN_ATTRFLAG_RO
    },    
    {
        .name = "dd_tx_pwr_hi_alarm", .id = eTRANS_DD_TX_PWR_HIGH_ALARM, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = TRANS_DD_TX_PWR_HIGH_ALARM,
        .flags = eDEVMAN_ATTRFLAG_RO
    },    
    {
        .name = "dd_tx_pwr_lo_alarm", .id = eTRANS_DD_TX_PWR_LOW_ALARM, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = TRANS_DD_TX_PWR_LOW_ALARM,
        .flags = eDEVMAN_ATTRFLAG_RO
    },    
    {
        .name = "dd_tx_pwr_hi_warn", .id = eTRANS_DD_TX_PWR_HIGH_WARN, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = TRANS_DD_TX_PWR_HIGH_WARN,
        .flags = eDEVMAN_ATTRFLAG_RO
    },    
    {
        .name = "dd_tx_pwr_lo_warn", .id = eTRANS_DD_TX_PWR_LOW_WARN, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = TRANS_DD_TX_PWR_LOW_WARN,
        .flags = eDEVMAN_ATTRFLAG_RO
    },    
    {
        .name = "dd_rx_pwr_hi_alarm", .id = eTRANS_DD_RX_PWR_HIGH_ALARM, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = TRANS_DD_RX_PWR_HIGH_ALARM,
        .flags = eDEVMAN_ATTRFLAG_RO
    },    
    {
        .name = "dd_rx_pwr_lo_alarm", .id = eTRANS_DD_RX_PWR_LOW_ALARM, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = TRANS_DD_RX_PWR_LOW_ALARM,
        .flags = eDEVMAN_ATTRFLAG_RO
    },    
    {
        .name = "dd_rx_pwr_hi_warn", .id = eTRANS_DD_RX_PWR_HIGH_WARN, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_DD_RX_PWR_HIGH_WARN,
        .flags = eDEVMAN_ATTRFLAG_RO
    },    
    {
        .name = "dd_rx_pwr_lo_warn", .id = eTRANS_DD_RX_PWR_LOW_WARN, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = TRANS_DD_RX_PWR_LOW_WARN,
        .flags = eDEVMAN_ATTRFLAG_RO
    },    

    /* digital diagnostics attributes; calibration constants */
    {
        .name = "dd_cal_rx_pwr_4", .id = eTRANS_DD_CAL_RX_PWR_4, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TRANS_DD_CAL_RX_PWR_4,
        .flags = eDEVMAN_ATTRFLAG_RO
    },    
    {
        .name = "dd_cal_rx_pwr_3", .id = eTRANS_DD_CAL_RX_PWR_3, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TRANS_DD_CAL_RX_PWR_3,
        .flags = eDEVMAN_ATTRFLAG_RO
    },    
    {
        .name = "dd_cal_rx_pwr_2", .id = eTRANS_DD_CAL_RX_PWR_2, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TRANS_DD_CAL_RX_PWR_2,
        .flags = eDEVMAN_ATTRFLAG_RO
    },    
    {
        .name = "dd_cal_rx_pwr_1", .id = eTRANS_DD_CAL_RX_PWR_1, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TRANS_DD_CAL_RX_PWR_1,
        .flags = eDEVMAN_ATTRFLAG_RO
    },    
    {
        .name = "dd_cal_rx_pwr_0", .id = eTRANS_DD_CAL_RX_PWR_0, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = TRANS_DD_CAL_RX_PWR_0,
        .flags = eDEVMAN_ATTRFLAG_RO
    },    
    {
        .name = "dd_cal_tx_i_slope", .id = eTRANS_DD_CAL_TX_I_SLOPE, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = TRANS_DD_CAL_TX_I_SLOPE,
        .flags = eDEVMAN_ATTRFLAG_RO
    },    
    {
        .name = "dd_cal_tx_i_offset", .id = eTRANS_DD_CAL_TX_I_OFFSET, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = TRANS_DD_CAL_TX_I_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },    
    {
        .name = "dd_cal_tx_pwr_slope", .id = eTRANS_DD_CAL_TX_PWR_SLOPE, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = TRANS_DD_CAL_TX_PWR_SLOPE,
        .flags = eDEVMAN_ATTRFLAG_RO
    },    
    {
        .name = "dd_cal_tx_pwr_offset", .id = eTRANS_DD_CAL_TX_PWR_OFFSET, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = TRANS_DD_CAL_TX_PWR_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },    
    {
        .name = "dd_cal_t_slope", .id = eTRANS_DD_CAL_T_SLOPE, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = TRANS_DD_CAL_T_SLOPE,
        .flags = eDEVMAN_ATTRFLAG_RO
    },    
    {
        .name = "dd_cal_t_offset", .id = eTRANS_DD_CAL_T_OFFSET, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = TRANS_DD_CAL_T_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },    
    {
        .name = "dd_cal_v_slope", .id = eTRANS_DD_CAL_V_SLOPE, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = TRANS_DD_CAL_V_SLOPE,
        .flags = eDEVMAN_ATTRFLAG_RO
    },    
    {
        .name = "dd_cal_v_offset", .id = eTRANS_DD_CAL_V_OFFSET, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = TRANS_DD_CAL_V_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },    
    {
        .name = "dd_cal_checksum", .id = eTRANS_DD_CAL_CHECKSUM, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_DD_CAL_CHECKSUM,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    
    /* digital diagnostics attributes; a/d values and status */    
    {
        .name = "dd_ad_temp", .id = eTRANS_DD_AD_TEMP, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = TRANS_DD_AD_TEMP,
        .flags = eDEVMAN_ATTRFLAG_RO
    },    
    {
        .name = "dd_ad_vcc", .id = eTRANS_DD_AD_VCC, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = TRANS_DD_AD_VCC,
        .flags = eDEVMAN_ATTRFLAG_RO
    },    
    {
        .name = "dd_ad_tx_bias", .id = eTRANS_DD_AD_TX_BIAS, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = TRANS_DD_AD_TX_BIAS,
        .flags = eDEVMAN_ATTRFLAG_RO
    },    
    {
        .name = "dd_ad_tx_pwr", .id = eTRANS_DD_AD_TX_PWR, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = TRANS_DD_AD_TX_PWR,
        .flags = eDEVMAN_ATTRFLAG_RO
    },    
    {
        .name = "dd_ad_rx_pwr", .id = eTRANS_DD_AD_RX_PWR, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset_sz = sizeof(b_u16), .offset = TRANS_DD_AD_RX_PWR,
        .flags = eDEVMAN_ATTRFLAG_RO
    },    
    {
        .name = "dd_ad_status_110", .id = eTRANS_DD_AD_STATUS_110, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_DD_AD_STATUS_110,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "dd_ad_status_112", .id = eTRANS_DD_AD_STATUS_112, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_DD_AD_STATUS_112,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "dd_ad_status_113", .id = eTRANS_DD_AD_STATUS_113, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_DD_AD_STATUS_113,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "dd_ad_status_116", .id = eTRANS_DD_AD_STATUS_116, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_DD_AD_STATUS_116,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "dd_ad_status_117", .id = eTRANS_DD_AD_STATUS_117, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_DD_AD_STATUS_117,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "dd_ad_vendor_spec", .id = eTRANS_DD_AD_VENDOR_SPEC, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = 8, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_DD_AD_VENDOR_SPEC,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "dd_user_writable", .id = eTRANS_DD_USER_WRITABLE, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = 120, 
        .offset_sz = sizeof(b_u8), .offset = TRANS_DD_USER_WRITABLE,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "media_decode", .id = eTRANS_MEDIA_DECODE, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = sizeof(media_decoder_t), 
        .offset_sz = sizeof(b_u8), .offset = TRANS_MSA_RAW,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "init", .id = eTRANS_INIT, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "config_test", .id = eTRANS_CONFIGURE_TEST, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "run_test", .id = eTRANS_RUN_TEST, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, 
        .size = sizeof(board_dev_stats_t), 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
};

/* Device instance hw-specific initializations 
 */
static ccl_err_t _trans_add(void *devo, void *brdspec)
{
    trans_ctx_t        *p_trans;
    trans_brdspec_t    *p_brdspec;
    b_u32              i2c_muxnum, i;
    
    if ( !devo || !brdspec ) {
        PDEBUG("Error! Invalid input parameters\n");
        TRANS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_trans = devman_device_private(devo);
    memset(p_trans, 0, sizeof(trans_ctx_t));    
    p_brdspec = (trans_brdspec_t *)brdspec;
    
    PDEBUG("123$=> p_brdspec->bus_id=%d, dev_i2ca=0x%x\n", 
           p_brdspec->i2c.bus_id, p_brdspec->i2c.dev_i2ca);
     
    p_trans->devo = devo;    
    memcpy(&p_trans->brdspec, p_brdspec, sizeof(trans_brdspec_t));

    /* calculate the i2c muxes real number */
    for ( i2c_muxnum = 0; i2c_muxnum < DEVBRD_I2C_MUXES_IN_CHAIN_MAXNUM; i2c_muxnum++ ) {
        if ( !p_trans->brdspec.i2c.mux_i2ca[i2c_muxnum] )
            break;
    }
    /* set i2c muxes attributes array real elements number */
    for ( i = 0; i < sizeof(_attrs)/sizeof(_attrs[0]); i++ ) {
        if ( !strcmp(_attrs[i].name, "i2c_mux_addr") )
            _attrs[i].maxnum = i2c_muxnum;
        if ( !strcmp(_attrs[i].name, "i2c_mux_pin") )
            _attrs[i].maxnum = i2c_muxnum;
    }
    if (p_brdspec->is_qsfp) {
        p_trans->channel_num = 4;
        p_trans->msa_offset = 128;
        p_trans->dd_temperature_offset = 22;
        p_trans->dd_vcc_offset = 26;
        p_trans->dd_txbias_offset = 42;
        p_trans->dd_txpower_offset = 50;
        p_trans->dd_rxpower_offset = 34;
    } else {
        p_trans->channel_num = 1;
        p_trans->dd_temperature_offset = 96;
        p_trans->dd_vcc_offset = 98;
        p_trans->dd_txbias_offset = 100;
        p_trans->dd_txpower_offset = 102;
        p_trans->dd_rxpower_offset = 104;
    }
    p_trans->is_qsfp = p_brdspec->is_qsfp;


    return CCL_OK;
}


/* Device instance hw-specific deinitializations 
 */
static ccl_err_t _trans_cut(void *devo)
{
    trans_ctx_t  *p_trans;
    if ( !devo ) {
        PDEBUG("Error! Invalid input parameters\n");
        TRANS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_trans = devman_device_private(devo);
    memset(p_trans, 0, sizeof(trans_ctx_t));    
    return CCL_OK;
}

/* device type description structure */
static device_driver_t _trans_driver = 
{
    .name = "trans",
    .f_add = _trans_add,
    .f_cut = _trans_cut,
    .attrs_num = sizeof(_attrs)/sizeof(_attrs[0]),
                 .attrs = _attrs,
    .priv_size = sizeof(_trans_driver)
}; 

/* Initialize TRANS device type
 */
ccl_err_t devtrans_init(void)
{
    /* register device type */
    _ret = devman_driver_register(&_trans_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to register Transceiver driver\n");
        TRANS_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

/* Deinitialize TRANS device type
 */
ccl_err_t devtrans_fini(void)
{
    /* unregister device type */
    _ret = devman_driver_unregister(&_trans_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to unregister Transceiver driver\n");
        TRANS_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

