/********************************************************************************/
/**
 * @file tsg.c
 * @brief Implementation of the board specific functionalities
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <semaphore.h>
#include <linux/limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "devman_port.h"
#include "pmbus.h"
#include "tsg.h"


#define DEVBOARD_DEBUG
/* printing/error-returning macros */
#ifdef DEVBOARD_DEBUG
    #define PDEBUG(fmt, args...) \
         ccl_syslog_err("DEVBOARD: %s[%d]: " fmt, __FUNCTION__, __LINE__, ## args)
#else
    #define PDEBUG(fmt, args...)
#endif
#define DEVBOARD_ERROR_RETURN(err) do { \
   ccl_syslog_err("Error! %s[%d], error=%i\n", __FUNCTION__, __LINE__, (err)); \
   return err; \
} while (0)

static ccl_err_t    _ret;

/* logical port number validation */
#define DEVBRD_IS_LOGPORT_VALID(_lp)  \
        ( (((_lp) >= 0) && ((_lp) < DEVBRD_LOGPORT_MAXNUM))? 1: 0 )

/* logical port number to hwsfp index converter
 */
static ccl_err_t _fboard_logport_to_hwsfp(int logport, int *p_hwsfp)
{

    /* check input parameters */
    if ( !DEVBRD_IS_LOGPORT_VALID(logport) || !p_hwsfp ) {
        PDEBUG("Error! Bad input parameters logport=%d, p_hwsfp=0x%x\n", 
               logport, p_hwsfp);
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    *p_hwsfp = _logport_hwspec_sfp_index[logport];      

    return CCL_OK;
} 

static board_device_descr_t _i2cbus_brd_descr[] = {
    {
        .type_name = "i2cbus", .sig = eDEVBRD_I2CBUS_SIG, 
        .spec.i2cbus = {
            .bus_id = eDEVBRD_I2C_BUS_3, 
            .slaves_num = eDEVBRD_I2C3_NUMBER,
            .slave_name = {
                "rtc"
            },
            .slave_i2ca = {
                eDEVBRD_I2C3_RTC
            }
        }
    },        
};

static board_device_descr_t _i2ccore_brd_descr[] = {
    {
        //TODO - update HSID/HW spec with correct devices/addresses
        .type_name = "i2ccore", .sig = eDEVBRD_I2CCORE_0_SIG, 
        .spec.i2ccore = {
            .devname = "fpga0",
            .dev_id = eDEVBRD_I2C_BUS_0,
            .slaves_num = {
                1, 1, 1, 1, 1, 1 ,2
            },
            .slave_name = {
                {
                    "atp_lb1"
                }, 
                {
                    "atp_lb2"
                }, 
                {
                    "atp_rtc1"
                },
                {
                    "atp_rtc2"
                }, 
                {
                    "atp_eeprom1"
                }, 
                {
                    "atp_eeprom2"
                },
                {
                    "atp_i2cio1", "atp_i2cio2"
                }
            },
            .slave_i2ca = {
                { 
                    eDEVBRD_I2CA_ATP_LB1
                }, 
                {   
                    eDEVBRD_I2CA_ATP_LB2
                }, 
                {
                    eDEVBRD_I2CA_ATP_RTC1
                }, 
                {
                    eDEVBRD_I2CA_ATP_RTC2
                }, 
                {
                    eDEVBRD_I2CA_ATP_EEPROM1
                }, 
                {
                    eDEVBRD_I2CA_ATP_EEPROM2
                },
                {
                    eDEVBRD_I2CA_ATP_I2CIO1,
                    eDEVBRD_I2CA_ATP_I2CIO2
                }
            },
            .op_mode = eDEVBRD_OP_MODE_POLL,
            .irq = I2CCORE_ATP_IRQ,
            .offset = I2CCORE_ATP_OFFSET,
            .name = "I2CCORE_ATP"
        }
    },
    {
        //TODO - update HSID/HW spec with correct devices/addresses
        .type_name = "i2ccore", .sig = eDEVBRD_I2CCORE_1_SIG, 
        .spec.i2ccore = {
            .devname = "fpga0",
            .dev_id = eDEVBRD_I2C_BUS_1,
            .slaves_num = {
                13, 11, 0, 2
            },
            .slave_name = {
                {
                    "lcd", "main_eeprom", "comx0_eeprom", 
                    "comx1_eeprom", "comx2_eeprom", "comx3_eeprom",
                    "comx0_currmon", "comx1_currmon", "comx2_currmon",
                    "comx3_currmon", "smarc_main_currmon", "smarc_ctrl_currmon",
                    "rtc"
                },
                { 
                    "vccint_0.85v", "vccaux_1.8v", "vcco_1.2v", 
                    "vcco_3.3v", "vcco_2.5v",
                    "mgtavcc_0.9v", "1.0v", "mgtavtt_1.2v", 
                    "5v", "main_pwr_supply1", "main_pwr_supply2"
                }, 
                {
                    
                },
                {
                    "vcco_red_3.3v", "fan_red_ctrl"
                }
            },
            .slave_i2ca = {
                { 
                    eDEVBRD_I2CA_LCD, eDEVBRD_I2CA_MAIN_EEPROM, eDEVBRD_I2CA_COMX0_EEPROM, 
                    eDEVBRD_I2CA_COMX1_EEPROM, eDEVBRD_I2CA_COMX2_EEPROM, eDEVBRD_I2CA_COMX3_EEPROM,
                    eDEVBRD_I2CA_COMX0_CURRMON, eDEVBRD_I2CA_COMX1_CURRMON, 
                    eDEVBRD_I2CA_COMX2_CURRMON, eDEVBRD_I2CA_COMX3_CURRMON, 
                    eDEVBRD_I2CA_SMARC_MAIN_CURRMON, eDEVBRD_I2CA_SMARC_CTRL_CURRMON, eDEVBRD_I2CA_MAIN_RTC
                },
                {
                    eDEVBRD_I2CA_VCCINT_O_85V, eDEVBRD_I2CA_VCCAUX_1_8V, eDEVBRD_I2CA_VCCO_1_2V, 
                    eDEVBRD_I2CA_VCCO_3_3V, eDEVBRD_I2CA_VCCO_2_5V,
                    eDEVBRD_I2CA_MGTAVCC_0_9V, eDEVBRD_I2CA_1_0V, eDEVBRD_I2CA_MGTAVTT_1_2V, 
                    eDEVBRD_I2CA_5V, eDEVBRD_I2CA_MAIN_PS_0, eDEVBRD_I2CA_MAIN_PS_1
                },
                {
                    
                }, 
                {
                    eDEVBRD_I2CA_VCCO_CLIENT_3_3V, eDEVBRD_I2CA_FAN_CTL
                }
            },
            .op_mode = eDEVBRD_OP_MODE_POLL,
            .irq = I2CCORE_SYSTEM_IRQ,
            .offset = I2CCORE_SYSTEM_OFFSET,
            .name = "I2CCORE_SYSTEM"
        }
    },
    {
        .type_name = "i2ccore", .sig = eDEVBRD_I2CCORE_2_SIG, 
        .spec.i2ccore = {
            .devname = "fpga1",
            .dev_id = eDEVBRD_I2C_BUS_2,
            .slaves_num = {
                1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1
            },
            .slave_name = {
                { "red_sfp0" }, 
                { "red_sfp1" }, 
                { "red_sfp2" }, 
                { "red_sfp3" },
                { "red_sfp4" }, 
                { "red_sfp5" }, 
                { "red_sfp6" }, 
                { "red_sfp7" },
                { "red_sfp8" }, 
                { "red_sfp9" }, 
                { "red_sfp10" }, 
                { "red_sfp11" },
                { "red_qsfp0" }, 
                { "red_qsfp1" },
                { "red_sfp_gpio0" },
                { "red_sfp_gpio1" },
                { "red_sfp_gpio2" },
                { "red_qsfp_gpio0" },
            },
            .slave_i2ca = {
                { eDEVBRD_I2CA_TRANS },
                { eDEVBRD_I2CA_TRANS },
                { eDEVBRD_I2CA_TRANS },
                { eDEVBRD_I2CA_TRANS },
                { eDEVBRD_I2CA_TRANS },
                { eDEVBRD_I2CA_TRANS },
                { eDEVBRD_I2CA_TRANS },
                { eDEVBRD_I2CA_TRANS },
                { eDEVBRD_I2CA_TRANS },
                { eDEVBRD_I2CA_TRANS },
                { eDEVBRD_I2CA_TRANS },
                { eDEVBRD_I2CA_TRANS },
                { eDEVBRD_I2CA_TRANS },
                { eDEVBRD_I2CA_TRANS },
                { eDEVBRD_I2CA_I2CIO_0 },
                { eDEVBRD_I2CA_I2CIO_0 },
                { eDEVBRD_I2CA_I2CIO_0 },
                { eDEVBRD_I2CA_I2CIO_0 }
            },
            .op_mode = eDEVBRD_OP_MODE_POLL,
            .irq = I2CCORE_TRANS_CLIENT_IRQ,
            .offset = I2CCORE_TRANS_CLIENT_OFFSET,
            .name = "I2CCORE_TRANS_CLIENT"
        }
    },
    {
        .type_name = "i2ccore", .sig = eDEVBRD_I2CCORE_3_SIG, 
        .spec.i2ccore = {
            .devname = "fpga1",
            .dev_id = eDEVBRD_I2C_BUS_3,
            .slaves_num = {
                1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1
            },
            .slave_name = {
                { "black_sfp0" }, 
                { "black_sfp1" }, 
                { "black_sfp2" }, 
                { "black_sfp3" },
                { "black_sfp4" }, 
                { "black_sfp5" }, 
                { "black_sfp6" }, 
                { "black_sfp7" },
                { "black_sfp8" }, 
                { "black_sfp9" }, 
                { "black_sfp10" }, 
                { "black_sfp11" },
                { "black_qsfp0" }, 
                { "black_qsfp1" },
                { "black_sfp_gpio0" },
                { "black_sfp_gpio1" },
                { "black_sfp_gpio2" },
                { "black_qsfp_gpio0" },
            },
            .slave_i2ca = {
                { eDEVBRD_I2CA_TRANS },
                { eDEVBRD_I2CA_TRANS },
                { eDEVBRD_I2CA_TRANS },
                { eDEVBRD_I2CA_TRANS },
                { eDEVBRD_I2CA_TRANS },
                { eDEVBRD_I2CA_TRANS },
                { eDEVBRD_I2CA_TRANS },
                { eDEVBRD_I2CA_TRANS },
                { eDEVBRD_I2CA_TRANS },
                { eDEVBRD_I2CA_TRANS },
                { eDEVBRD_I2CA_TRANS },
                { eDEVBRD_I2CA_TRANS },
                { eDEVBRD_I2CA_TRANS },
                { eDEVBRD_I2CA_TRANS },
                { eDEVBRD_I2CA_I2CIO_0 },
                { eDEVBRD_I2CA_I2CIO_0 },
                { eDEVBRD_I2CA_I2CIO_0 },
                { eDEVBRD_I2CA_I2CIO_0 }
            },
            .op_mode = eDEVBRD_OP_MODE_POLL,
            .irq = I2CCORE_TRANS_NETWORK_IRQ,
            .offset = I2CCORE_TRANS_NETWORK_OFFSET,
            .name = "I2CCORE_TRANS_NETWORK"
        }
    },
    {
        .type_name = "i2ccore", .sig = eDEVBRD_I2CCORE_4_SIG, 
        .spec.i2ccore = {
            .devname = "fpga1",
            .dev_id = eDEVBRD_I2C_BUS_4,
            .slaves_num = {
                3
            },
            .slave_name = {
                {"sodimm_eeprom1", "sodimm_eeprom2", "sodimm_eeprom3"}
            },
            .slave_i2ca = {
                {
                    eDEVBRD_I2CA_SODIMM_EEPROM1,
                    eDEVBRD_I2CA_SODIMM_EEPROM2,
                    eDEVBRD_I2CA_SODIMM_EEPROM3
                }
            },
            .irq = I2CCORE_TRANS_NETWORK_IRQ,
            .offset = I2CCORE_SODIMM_OFFSET,
            .name = "I2CCORE_SODIMM"
        }
    }
};

static board_device_descr_t _axigpio_brd_descr[] = {
    {
        .type_name = "axigpio", .sig = eDEVBRD_AXIGPIO_0_SIG,
        .spec.axigpio = {
            .devname = "fpga0",
            .pins_num = 6,
            .direction = {
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_BIDI, eDEVBRD_AXIGPIO_DIR_BIDI,
                eDEVBRD_AXIGPIO_DIR_BIDI, eDEVBRD_AXIGPIO_DIR_BIDI
            },
            .irq = AXIGPIO_CPU_CTRL_IRQ,
            .offset = AXIGPIO_CPU_CTRL_OFFSET,
            .name = "AXIGPIO_CPU_CTRL"
        }
    },
    {
        .type_name = "axigpio", .sig = eDEVBRD_AXIGPIO_1_SIG,
        .spec.axigpio = {
            .devname = "fpga0",
            .pins_num = 14,
            .direction = {
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_INPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_NOT_VALID, eDEVBRD_AXIGPIO_DIR_NOT_VALID,
                eDEVBRD_AXIGPIO_DIR_BIDI, eDEVBRD_AXIGPIO_DIR_BIDI,
                eDEVBRD_AXIGPIO_DIR_BIDI, eDEVBRD_AXIGPIO_DIR_BIDI,
                eDEVBRD_AXIGPIO_DIR_BIDI, eDEVBRD_AXIGPIO_DIR_BIDI,
            },
            .irq = AXIGPIO_VIRTEX_CTRL_IRQ,
            .offset = AXIGPIO_VIRTEX_CTRL_OFFSET,
            .name = "AXIGPIO_VIRTEX_CTRL"
        }
    },
    {
        .type_name = "axigpio", .sig = eDEVBRD_AXIGPIO_2_SIG,
        .spec.axigpio = {
            .devname = "fpga0",
            .pins_num = 3,
            .direction = {
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_INPUT
            },
            .irq = AXIGPIO_SMART_CARD_CTRL_IRQ,
            .offset = AXIGPIO_SMART_CARD_CTRL_OFFSET,
            .name = "AXIGPIO_SMART_CARD_CTRL"
        }
    },
    {
        .type_name = "axigpio", .sig = eDEVBRD_AXIGPIO_3_SIG,
        .spec.axigpio = {
            .devname = "fpga0",
            .pins_num = 4,
            .direction = {
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT
            },
            .irq = AXIGPIO_FLASH_MUX_CTRL_IRQ,
            .offset = AXIGPIO_FLASH_MUX_CTRL_OFFSET,
            .name = "AXIGPIO_FLASH_MUX_CTRL"
        }
    },
    {
        .type_name = "axigpio", .sig = eDEVBRD_AXIGPIO_4_SIG,
        .spec.axigpio = {
            .devname = "fpga0",
            .pins_num = 6,
            .direction = {
                eDEVBRD_AXIGPIO_DIR_NOT_VALID, eDEVBRD_AXIGPIO_DIR_BIDI,
                eDEVBRD_AXIGPIO_DIR_BIDI, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_INPUT, eDEVBRD_AXIGPIO_DIR_INPUT
            },
            .irq = AXIGPIO_ATP_CTRL_IRQ,
            .offset = AXIGPIO_ATP_CTRL_OFFSET,
            .name = "AXIGPIO_ATP_CTRL"
        }
    },
    {
        .type_name = "axigpio", .sig = eDEVBRD_AXIGPIO_5_SIG,
        .spec.axigpio = {
            .devname = "fpga0",
            .pins_num = 17,
            .direction = {
                eDEVBRD_AXIGPIO_DIR_INPUT, eDEVBRD_AXIGPIO_DIR_INPUT,
                eDEVBRD_AXIGPIO_DIR_INPUT, eDEVBRD_AXIGPIO_DIR_INPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_INPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT,
            },
            .irq = AXIGPIO_POWER_RESET_CTRL_IRQ,
            .offset = AXIGPIO_POWER_RESET_CTRL_OFFSET,
            .name = "AXIGPIO_POWER_RESET_CTRL"
        }
    },
    {
        .type_name = "axigpio", .sig = eDEVBRD_AXIGPIO_6_SIG,
        .spec.axigpio = {
            .devname = "fpga0",
            .pins_num = 24,
            .direction = {
                eDEVBRD_AXIGPIO_DIR_NOT_VALID, eDEVBRD_AXIGPIO_DIR_NOT_VALID,
                eDEVBRD_AXIGPIO_DIR_NOT_VALID, eDEVBRD_AXIGPIO_DIR_NOT_VALID,
                eDEVBRD_AXIGPIO_DIR_NOT_VALID, eDEVBRD_AXIGPIO_DIR_NOT_VALID,
                eDEVBRD_AXIGPIO_DIR_NOT_VALID, eDEVBRD_AXIGPIO_DIR_NOT_VALID,
                eDEVBRD_AXIGPIO_DIR_INPUT, eDEVBRD_AXIGPIO_DIR_INPUT,
                eDEVBRD_AXIGPIO_DIR_INPUT, eDEVBRD_AXIGPIO_DIR_INPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_INPUT, eDEVBRD_AXIGPIO_DIR_INPUT,
                eDEVBRD_AXIGPIO_DIR_INPUT, eDEVBRD_AXIGPIO_DIR_INPUT
            },
            .irq = AXIGPIO_COMX_CTRL_IRQ,
            .offset = AXIGPIO_COMX_CTRL_OFFSET,
            .name = "AXIGPIO_COMX_CTRL"
        }
    },
    {
        .type_name = "axigpio", .sig = eDEVBRD_AXIGPIO_7_SIG,
        .spec.axigpio = {
            .devname = "fpga0",
            .pins_num = 12,
            .direction = {
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_INPUT,
                eDEVBRD_AXIGPIO_DIR_NOT_VALID, eDEVBRD_AXIGPIO_DIR_INPUT,
                eDEVBRD_AXIGPIO_DIR_INPUT, eDEVBRD_AXIGPIO_DIR_INPUT,
                eDEVBRD_AXIGPIO_DIR_INPUT, eDEVBRD_AXIGPIO_DIR_INPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_INPUT
            },
            .irq = AXIGPIO_PANEL_CTRL_IRQ,
            .offset = AXIGPIO_PANEL_CTRL_OFFSET,
            .name = "AXIGPIO_PANEL_CTRL"
        }
    },
    {
        .type_name = "axigpio", .sig = eDEVBRD_AXIGPIO_8_SIG,
        .spec.axigpio = {
            .devname = "fpga0",
            .pins_num = 16,
            .direction = {
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_INPUT, eDEVBRD_AXIGPIO_DIR_INPUT,
                eDEVBRD_AXIGPIO_DIR_INPUT, eDEVBRD_AXIGPIO_DIR_INPUT,
                eDEVBRD_AXIGPIO_DIR_INPUT, eDEVBRD_AXIGPIO_DIR_INPUT,
                eDEVBRD_AXIGPIO_DIR_INPUT, eDEVBRD_AXIGPIO_DIR_INPUT
            },
            .irq = AXIGPIO_GENERAL_CTRL_IRQ,
            .offset = AXIGPIO_ARTIX_GENERAL_CTRL_OFFSET,
            .name = "AXIGPIO_ARTIX_GENERAL_CTRL"
        }
    },
    {
        .type_name = "axigpio", .sig = eDEVBRD_AXIGPIO_9_SIG,
        .spec.axigpio = {
            .devname = "fpga1",
            .pins_num = 24,
            .direction = {
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
            },
            .offset = AXIGPIO_TRAFFIC_GEN_1G_OFFSET,
            .name = "AXIGPIO_TRAFFIC_GEN_1G"
        }
    },
    {
        .type_name = "axigpio", .sig = eDEVBRD_AXIGPIO_10_SIG,
        .spec.axigpio = {
            .devname = "fpga1",
            .pins_num = 31,
            .direction = {
                eDEVBRD_AXIGPIO_DIR_INPUT, eDEVBRD_AXIGPIO_DIR_INPUT,
                eDEVBRD_AXIGPIO_DIR_INPUT, eDEVBRD_AXIGPIO_DIR_INPUT,
                eDEVBRD_AXIGPIO_DIR_INPUT, eDEVBRD_AXIGPIO_DIR_INPUT,
                eDEVBRD_AXIGPIO_DIR_INPUT, eDEVBRD_AXIGPIO_DIR_INPUT,
                eDEVBRD_AXIGPIO_DIR_INPUT, eDEVBRD_AXIGPIO_DIR_INPUT,
                eDEVBRD_AXIGPIO_DIR_INPUT, eDEVBRD_AXIGPIO_DIR_INPUT,
                eDEVBRD_AXIGPIO_DIR_INPUT, eDEVBRD_AXIGPIO_DIR_INPUT,
                eDEVBRD_AXIGPIO_DIR_INPUT, eDEVBRD_AXIGPIO_DIR_INPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_INPUT
            },
            .offset = AXIGPIO_TRANS_CTRL_CLIENT_OFFSET,
            .name = "AXIGPIO_TRANS_CTRL_CLIENT"
        }
    },
    {
        .type_name = "axigpio", .sig = eDEVBRD_AXIGPIO_11_SIG,
        .spec.axigpio = {
            .devname = "fpga1",
            .pins_num = 46,
            .direction = {
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT
            },
            .offset = AXIGPIO_TRAFFIC_GEN_CLIENT_OFFSET,
            .name = "AXIGPIO_TRAFFIC_GEN_CLIENT"
        }
    },
    {
        .type_name = "axigpio", .sig = eDEVBRD_AXIGPIO_12_SIG,
        .spec.axigpio = {
            .devname = "fpga1",
            .pins_num = 31,
            .direction = {
                eDEVBRD_AXIGPIO_DIR_INPUT, eDEVBRD_AXIGPIO_DIR_INPUT,
                eDEVBRD_AXIGPIO_DIR_INPUT, eDEVBRD_AXIGPIO_DIR_INPUT,
                eDEVBRD_AXIGPIO_DIR_INPUT, eDEVBRD_AXIGPIO_DIR_INPUT,
                eDEVBRD_AXIGPIO_DIR_INPUT, eDEVBRD_AXIGPIO_DIR_INPUT,
                eDEVBRD_AXIGPIO_DIR_INPUT, eDEVBRD_AXIGPIO_DIR_INPUT,
                eDEVBRD_AXIGPIO_DIR_INPUT, eDEVBRD_AXIGPIO_DIR_INPUT,
                eDEVBRD_AXIGPIO_DIR_INPUT, eDEVBRD_AXIGPIO_DIR_INPUT,
                eDEVBRD_AXIGPIO_DIR_INPUT, eDEVBRD_AXIGPIO_DIR_INPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_INPUT
            },
            .offset = AXIGPIO_TRANS_CTRL_NETWORK_OFFSET,
            .name = "AXIGPIO_TRANS_CTRL_NETWORK"
        }
    },
    {
        .type_name = "axigpio", .sig = eDEVBRD_AXIGPIO_13_SIG,
        .spec.axigpio = {
            .devname = "fpga1",
            .pins_num = 46,
            .direction = {
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT
            },
            .offset = AXIGPIO_TRAFFIC_GEN_NETWORK_OFFSET,
            .name = "AXIGPIO_TRAFFIC_GEN_NETWORK"
        }
    },
    {
        .type_name = "axigpio", .sig = eDEVBRD_AXIGPIO_14_SIG,
        .spec.axigpio = {
            .devname = "fpga1",
            .pins_num = 14,
            .direction = {
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_BIDI, eDEVBRD_AXIGPIO_DIR_BIDI,
                eDEVBRD_AXIGPIO_DIR_BIDI, eDEVBRD_AXIGPIO_DIR_BIDI,
                eDEVBRD_AXIGPIO_DIR_BIDI, eDEVBRD_AXIGPIO_DIR_BIDI
            },
            .offset = AXIGPIO_VIRTEX_GENERAL_CTRL_OFFSET,
            .name = "AXIGPIO_VIRTEX_GENERAL_CTRL"
        }
    },
    {
        .type_name = "axigpio", .sig = eDEVBRD_AXIGPIO_15_SIG,
        .spec.axigpio = {
            .devname = "fpga1",
            .pins_num = 36,
            .direction = {
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT,
                eDEVBRD_AXIGPIO_DIR_OUTPUT, eDEVBRD_AXIGPIO_DIR_OUTPUT
            },
            .offset = AXIGPIO_DDR4_USER_TEST_CTRL_OFFSET,
            .name = "AXIGPIO_DDR4_USER_TEST_CTRL"
        }
    },
};

static board_device_descr_t _axispi_brd_descr[] = {
    {
        .type_name = "axispi", .sig = eDEVBRD_AXISPI_0_SIG,
        .spec.axispi = {
            .devname = "fpga0",
            .dev_id = eDEVBRD_AXISPI_0,
            .slaves_num = 1,
            .slave_name = {
                "virtex_load" 
            },
            .slave_cs = {
                0
            },
            .op_mode = eDEVBRD_OP_MODE_POLL,
            .irq = AXISPI_VIRTEX_LOAD_IRQ,
            .offset = AXISPI_VIRTEX_LOAD_OFFSET,
            .name = "AXISPI_VIRTEX_LOAD"
        }
    },
    {
        .type_name = "axispi", .sig = eDEVBRD_AXISPI_1_SIG,
        .spec.axispi = {
            .devname = "fpga0",
            .dev_id = eDEVBRD_AXISPI_1,
            .slaves_num = 2,
            .slave_name = {
                "atp1", "atp2" 
            },
            .slave_cs = {
                0, 1
            },
            .op_mode = eDEVBRD_OP_MODE_POLL,
            .irq = AXISPI_ATP_IRQ,
            .offset = AXISPI_ATP_OFFSET,
            .name = "AXISPI_ATP"
        }
    },
    {
        .type_name = "axispi", .sig = eDEVBRD_AXISPI_2_SIG,
        .spec.axispi = {
            .devname = "fpga0",
            .dev_id = eDEVBRD_AXISPI_2,
            .slaves_num = 0,
            .op_mode = eDEVBRD_OP_MODE_POLL,
            .irq = AXISPI_VIRTEX_IRQ,
            .offset = AXISPI_VIRTEX_OFFSET,
            .name = "AXISPI_VIRTEX"
        }
    },
    {
        .type_name = "axispi", .sig = eDEVBRD_AXISPI_3_SIG,
        .spec.axispi = {
            .devname = "fpga0",
            .dev_id = eDEVBRD_AXISPI_3,
            .slaves_num = 8,
            .slave_name = {
                "gen_flash1", "gen_flash2", "main_cpu", "ctrl_cpu",
                "app_cpu0", "app_cpu1", "app_cpu2", "app_cpu3"  
            },
            .slave_cs = {
                0, 1, 2, 3, 4, 5, 6, 7
            },
            .op_mode = eDEVBRD_OP_MODE_POLL,
            .irq = AXISPI_FLASH_IRQ,
            .offset = AXISPI_FLASH_OFFSET,
            .name = "AXISPI_FLASH"
        }
    },
};

static board_device_descr_t _axiuart_brd_descr[] = {
    {
        .type_name = "axiuart", .sig = eDEVBRD_AXIUART_0_SIG, 
        .spec.axiuart = {
            .devname = "fpga0",
            .stop_bits = eDEVBRD_AXIUART_1_STOP_BITS,
            .data_bits = eDEVBRD_AXIUART_8_DATA_BITS,
            .parity = eDEVBRD_AXIUART_NO_PARITY,
            .input_clk = 125000000,
            .baudrate = 115200,
            .dev_id = eDEVBRD_AXIUART_0,
            .op_mode = eDEVBRD_OP_MODE_POLL,
            .irq = AXIUART_COMX0_IRQ,
            .offset = AXIUART_COMX0_OFFSET,
            .name = "AXIUART_COMX0"
        },
    },
    {
        .type_name = "axiuart", .sig = eDEVBRD_AXIUART_1_SIG, 
        .spec.axiuart = {
            .devname = "fpga0",
            .stop_bits = eDEVBRD_AXIUART_1_STOP_BITS,
            .data_bits = eDEVBRD_AXIUART_8_DATA_BITS,
            .parity = eDEVBRD_AXIUART_NO_PARITY,
            .input_clk = 125000000,
            .baudrate = 115200,
            .dev_id = eDEVBRD_AXIUART_1,
            .op_mode = eDEVBRD_OP_MODE_POLL,
            .irq = AXIUART_COMX1_IRQ,
            .offset = AXIUART_COMX1_OFFSET,
            .name = "AXIUART_COMX1"
        },
    },
    {
        .type_name = "axiuart", .sig = eDEVBRD_AXIUART_2_SIG, 
        .spec.axiuart = {
            .devname = "fpga0",
            .stop_bits = eDEVBRD_AXIUART_1_STOP_BITS,
            .data_bits = eDEVBRD_AXIUART_8_DATA_BITS,
            .parity = eDEVBRD_AXIUART_NO_PARITY,
            .input_clk = 125000000,
            .baudrate = 115200,
            .dev_id = eDEVBRD_AXIUART_2,
            .op_mode = eDEVBRD_OP_MODE_POLL,
            .irq = AXIUART_COMX2_IRQ,
            .offset = AXIUART_COMX2_OFFSET,
            .name = "AXIUART_COMX2"
        },
    },
    {
        .type_name = "axiuart", .sig = eDEVBRD_AXIUART_3_SIG, 
        .spec.axiuart = {
            .devname = "fpga0",
            .stop_bits = eDEVBRD_AXIUART_1_STOP_BITS,
            .data_bits = eDEVBRD_AXIUART_8_DATA_BITS,
            .parity = eDEVBRD_AXIUART_NO_PARITY,
            .input_clk = 125000000,
            .baudrate = 115200,
            .dev_id = eDEVBRD_AXIUART_3,
            .op_mode = eDEVBRD_OP_MODE_POLL,
            .irq = AXIUART_COMX3_IRQ,
            .offset = AXIUART_COMX3_OFFSET,
            .name = "AXIUART_COMX3"
        },
    },
    {
        .type_name = "axiuart", .sig = eDEVBRD_AXIUART_4_SIG, 
        .spec.axiuart = {
            .devname = "fpga0",
            .stop_bits = eDEVBRD_AXIUART_1_STOP_BITS,
            .data_bits = eDEVBRD_AXIUART_8_DATA_BITS,
            .parity = eDEVBRD_AXIUART_NO_PARITY,
            .input_clk = 125000000,
            .baudrate = 115200,
            .dev_id = eDEVBRD_AXIUART_4,
            .op_mode = eDEVBRD_OP_MODE_POLL,
            .irq = AXIUART_ATP_IRQ,
            .offset = AXIUART_ATP_OFFSET,
            .name = "AXIUART_ATP"
        },
    },
    {
        .type_name = "axiuart", .sig = eDEVBRD_AXIUART_5_SIG, 
        .spec.axiuart = {
            .devname = "fpga0",
            .stop_bits = eDEVBRD_AXIUART_1_STOP_BITS,
            .data_bits = eDEVBRD_AXIUART_8_DATA_BITS,
            .parity = eDEVBRD_AXIUART_NO_PARITY,
            .input_clk = 125000000,
            .baudrate = 115200,
            .dev_id = eDEVBRD_AXIUART_5,
            .op_mode = eDEVBRD_OP_MODE_POLL,
            .irq = AXIUART_SMARC_MAIN_IRQ,
            .istest = CCL_TRUE,
            .offset = AXIUART_SMARC_MAIN_OFFSET,
            .name = "AXIUART_SMARC_MAIN"
        },
    },
    {
        .type_name = "axiuart", .sig = eDEVBRD_AXIUART_6_SIG, 
        .spec.axiuart = {
            .devname = "fpga0",
            .stop_bits = eDEVBRD_AXIUART_1_STOP_BITS,
            .data_bits = eDEVBRD_AXIUART_8_DATA_BITS,
            .parity = eDEVBRD_AXIUART_NO_PARITY,
            .input_clk = 125000000,
            .baudrate = 115200,
            .dev_id = eDEVBRD_AXIUART_6,
            .op_mode = eDEVBRD_OP_MODE_POLL,
            .irq = AXIUART_SMARC_CTRL_IRQ,
            .istest = CCL_FALSE,
            .offset = AXIUART_SMARC_CTRL_OFFSET,
            .name = "AXIUART_SMARC_CTRL"
        },
    },
    {
        .type_name = "axiuart", .sig = eDEVBRD_AXIUART_7_SIG, 
        .spec.axiuart = {
            .devname = "fpga0",
            .stop_bits = eDEVBRD_AXIUART_1_STOP_BITS,
            .data_bits = eDEVBRD_AXIUART_8_DATA_BITS,
            .parity = eDEVBRD_AXIUART_NO_PARITY,
            .input_clk = 125000000,
            .baudrate = 115200,
            .dev_id = eDEVBRD_AXIUART_7,
            .op_mode = eDEVBRD_OP_MODE_POLL,
            .irq = AXIUART_CLI_IRQ,
            .offset = AXIUART_CLI_OFFSET,
            .name = "AXIUART_CLI"
        },
    },
    {
        .type_name = "axiuart", .sig = eDEVBRD_AXIUART_8_SIG, 
        .spec.axiuart = {
            .devname = "fpga0",
            .stop_bits = eDEVBRD_AXIUART_1_STOP_BITS,
            .data_bits = eDEVBRD_AXIUART_8_DATA_BITS,
            .parity = eDEVBRD_AXIUART_PARITY_EVEN,
            .input_clk = 125000000,
            .baudrate = 9600,
            .dev_id = eDEVBRD_AXIUART_8,
            .op_mode = eDEVBRD_OP_MODE_POLL,
            .irq = AXIUART_SMART_CARD_IRQ,
            .offset = AXIUART_SMART_CARD_OFFSET,
            .name = "AXIUART_SMART_CARD"
        },
    }
};

static board_device_descr_t _intc_brd_descr[] = {
    {
        .type_name = "intc", .sig = eDEVBRD_INTC_0_SIG, 
        .spec.intc = {
            .devname = "fpga0",
            .intc_mode = eDEVBRD_INTC_MODE_PRIMARY,
            .op_mode = INTC_REAL_MODE,
            .dev_id = eDEVBRD_INTC_DEV_ARTIX_0,
            .offset = INTC_ARTIX_0_OFFSET,
            .name = "INTC_ARTIX_0"
        }
    },
    {
        .type_name = "intc", .sig = eDEVBRD_INTC_1_SIG, 
        .spec.intc = {
            .devname = "fpga0",
            .intc_mode = eDEVBRD_INTC_MODE_SECONDARY,
            .op_mode = INTC_REAL_MODE,
            .dev_id = eDEVBRD_INTC_DEV_ARTIX_1,
            .irq = INTC_ARTIX_1_IRQ,
            .offset = INTC_ARTIX_1_OFFSET,
            .name = "INTC_ARTIX_1"
        }
    },
    {
        .type_name = "intc", .sig = eDEVBRD_INTC_1_SIG, 
        .spec.intc = {
            .devname = "fpga1",
            .intc_mode = eDEVBRD_INTC_MODE_SECONDARY,
            .op_mode = INTC_REAL_MODE,
            .dev_id = eDEVBRD_INTC_DEV_VIRTEX,
            .irq = INTC_VIRTEX_IRQ,
            .offset = INTC_VIRTEX_OFFSET,
            .name = "INTC_VIRTEX"
        }
    },
};

static board_device_descr_t _xadc_brd_descr[] = {
    {
        .type_name = "xadc", .sig = eDEVBRD_XADC_0_SIG, 
        .spec.xadc = {
            .devname = "fpga0",
            .irq = XADC_IRQ,
            .offset = XADC_OFFSET,
            .name = "XADC_ARTIX"
        }
    }
};

static board_device_descr_t _sysmon_brd_descr[] = {
    {
        .type_name = "sysmon", .sig = eDEVBRD_SYSMON_0_SIG,
        .spec.xadc = {
            .devname = "fpga1",
            .irq = SYSMON_VIRTEX_IRQ,
            .offset = SYSMON_VIRTEX_OFFSET,
            .name = "SYSMON_VIRTEX"
        }
    }
};

static board_device_descr_t _eswitch_brd_descr[] = {
    {
        .type_name = "eswitch", .sig = eDEVBRD_ESWITCH_0_SIG, 
        .spec.eswitch = {
            .devname = "fpga0",
            .irq = ESWITCH_IRQ,
            .offset = ESWITCH_OFFSET,
            .port_num = ETH_SWITCH_PORT_NUM,
            .port_name = {
                "PORT_1G_0_ARTIX_VIRTEX",
                "PORT_1G_1_ARTIX_VIRTEX",
                "PORT_1G_0_ARTIX_SMARC_CTRL",
                "PORT_1G_1_ARTIX_SMARC_CTRL",
                "PORT_1G_0_ARTIX_SMARC_MAIN",
                "PORT_1G_1_ARTIX_SMARC_MAIN",
                "PORT_1G_ARTIX_COMX0",
                "PORT_1G_LOAD",
                "PORT_1G_OOB",
                "PORT_1G_ARTIX_COMX1",
                "PORT_1G_ARTIX_COMX2",
                "PORT_1G_ARTIX_COMX3"
            },
            .name = "ESWITCH_ARTIX"
        }
    }
};

/* mailbox board device description */
static board_device_descr_t _mbox_brd_descr[] = {
    {
        .type_name = "mbox", .sig = eDEVBRD_MBOX_0_SIG, 
        .spec.mbox = {
            .devname = "fpga0",
            .i2c.bus_id = eDEVBRD_I2C_SYS_BUS_0,
            .i2c.dev_i2ca = eDEVBRD_I2C_MBOX_0,
            .dev_id = 0,
            .offset = 0x0
        }
    },
    {
        .type_name = "mbox", .sig = eDEVBRD_MBOX_1_SIG, 
        .spec.mbox = {
            .devname = "fpga0",
            .i2c.bus_id = eDEVBRD_I2C_SYS_BUS_0,
            .i2c.dev_i2ca = eDEVBRD_I2C_MBOX_0,
            .dev_id = 1,
            .offset = 0x40
        }
    },
    {
        .type_name = "mbox", .sig = eDEVBRD_MBOX_2_SIG, 
        .spec.mbox = {
            .devname = "fpga0",
            .i2c.bus_id = eDEVBRD_I2C_SYS_BUS_0,
            .i2c.dev_i2ca = eDEVBRD_I2C_MBOX_0,
            .dev_id = 2,
            .offset = 0x80
        }
    },
    {
        .type_name = "mbox", .sig = eDEVBRD_MBOX_3_SIG, 
        .spec.mbox = {
            .devname = "fpga0",
            .i2c.bus_id = eDEVBRD_I2C_SYS_BUS_0,
            .i2c.dev_i2ca = eDEVBRD_I2C_MBOX_0,
            .dev_id = 3,
            .offset = 0xc0
        }
    },
    {
        .type_name = "mbox", .sig = eDEVBRD_MBOX_4_SIG, 
        .spec.mbox = {
            .devname = "fpga0",
            .i2c.bus_id = eDEVBRD_I2C_SYS_BUS_0,
            .i2c.dev_i2ca = eDEVBRD_I2C_MBOX_0,
            .dev_id = 4,
            .offset = 0x100
        }
    },
    {
        .type_name = "mbox", .sig = eDEVBRD_MBOX_5_SIG, 
        .spec.mbox = {
            .devname = "fpga0",
            .i2c.bus_id = eDEVBRD_I2C_SYS_BUS_0,
            .i2c.dev_i2ca = eDEVBRD_I2C_MBOX_0,
            .dev_id = 5,
            .offset = 0x140
        }
    },
    {
        .type_name = "mbox", .sig = eDEVBRD_MBOX_6_SIG, 
        .spec.mbox = {
            .devname = "fpga0",
            .i2c.bus_id = eDEVBRD_I2C_SYS_BUS_0,
            .i2c.dev_i2ca = eDEVBRD_I2C_MBOX_0,
            .dev_id = 6,
            .offset = 0x180
        }
    }
};

static board_device_descr_t _temac_brd_descr[] = {
    {
        .type_name = "temac", .sig = eDEVBRD_1G_MAC_0_SIG,
        .spec.temac = {
            .devname = "fpga0",
            .phy_addr = 0,
            .portid = LOGPORT_1G_0,
            .irq = PORT_1G_1_ARTIX_SMARC_CTRL_IRQ,
            .offset = PORT_1G_0_ARTIX_VIRTEX_OFFSET,
            .name = PORT_1G_0_ARTIX_VIRTEX_NAME
        }
    },
    {
        .type_name = "temac", .sig = eDEVBRD_1G_MAC_1_SIG,
        .spec.temac = {
            .devname = "fpga0",
            .phy_addr = 0,
            .portid = LOGPORT_1G_1,
            .irq = PORT_1G_1_ARTIX_VIRTEX_IRQ,
            .offset = PORT_1G_1_ARTIX_VIRTEX_OFFSET,
            .name = PORT_1G_1_ARTIX_VIRTEX_NAME
        }
    },
    {
        .type_name = "temac", .sig = eDEVBRD_1G_MAC_2_SIG,
        .spec.temac = {
            .devname = "fpga0",
            .phy_addr = 1,
            .portid = LOGPORT_1G_2,
            .irq = PORT_1G_0_ARTIX_SMARC_MAIN_IRQ,
            .offset = PORT_1G_0_ARTIX_SMARC_MAIN_OFFSET,
            .name = PORT_1G_0_ARTIX_SMARC_MAIN_NAME
        }
    },
    {
        .type_name = "temac", .sig = eDEVBRD_1G_MAC_3_SIG,
        .spec.temac = {
            .devname = "fpga0",
            .phy_addr = 1,
            .portid = LOGPORT_1G_3,
            .irq = PORT_1G_1_ARTIX_SMARC_MAIN_IRQ,
            .offset = PORT_1G_1_ARTIX_SMARC_MAIN_OFFSET,
            .name = PORT_1G_1_ARTIX_SMARC_MAIN_NAME
        }
    },
    {
        .type_name = "temac", .sig = eDEVBRD_1G_MAC_4_SIG,
        .spec.temac = {
            .devname = "fpga0",
            .phy_addr = 1,
            .portid = LOGPORT_1G_4,
            .irq = PORT_1G_0_ARTIX_SMARC_CTRL_IRQ,
            .offset = PORT_1G_0_ARTIX_SMARC_CTRL_OFFSET,
            .name = PORT_1G_0_ARTIX_SMARC_CTRL_NAME
        }
    },
    {
        .type_name = "temac", .sig = eDEVBRD_1G_MAC_5_SIG,
        .spec.temac = {
            .devname = "fpga0",
            .phy_addr = 1,
            .portid = LOGPORT_1G_5,
            .irq = PORT_1G_1_ARTIX_SMARC_CTRL_IRQ,
            .offset = PORT_1G_1_ARTIX_SMARC_CTRL_OFFSET,
            .name = PORT_1G_1_ARTIX_SMARC_CTRL_NAME
        }
    },
    {
        .type_name = "temac", .sig = eDEVBRD_1G_MAC_6_SIG,
        .spec.temac = {
            .devname = "fpga0",
            .phy_addr = 1,
            .iftype = eDEVBRD_IFTYPE_SGMII,
            .portid = LOGPORT_1G_6,
            .irq = PORT_1G_ARTIX_COMX0_IRQ,
            .offset = PORT_1G_ARTIX_COMX0_OFFSET,
            .name = PORT_1G_ARTIX_COMX0_NAME
        }
    },
    {
        .type_name = "temac", .sig = eDEVBRD_1G_MAC_7_SIG,
        .spec.temac = {
            .devname = "fpga0",
            .phy_addr = 1,
            .iftype = eDEVBRD_IFTYPE_SGMII,
            .portid = LOGPORT_1G_7,
            .irq = PORT_1G_ARTIX_COMX1_IRQ,
            .offset = PORT_1G_ARTIX_COMX1_OFFSET,
            .name = PORT_1G_ARTIX_COMX1_NAME
        }
    },
    {
        .type_name = "temac", .sig = eDEVBRD_1G_MAC_8_SIG,
        .spec.temac = {
            .devname = "fpga0",
            .phy_addr = 1,
            .iftype = eDEVBRD_IFTYPE_SGMII,
            .portid = LOGPORT_1G_8,
            .irq = PORT_1G_ARTIX_COMX2_IRQ,
            .offset = PORT_1G_ARTIX_COMX2_OFFSET,
            .name = PORT_1G_ARTIX_COMX2_NAME
        }
    },
    {
        .type_name = "temac", .sig = eDEVBRD_1G_MAC_9_SIG,
        .spec.temac = {
            .devname = "fpga0",
            .phy_addr = 1,
            .iftype = eDEVBRD_IFTYPE_SGMII,
            .portid = LOGPORT_1G_9,
            .irq = PORT_1G_ARTIX_COMX3_IRQ,
            .offset = PORT_1G_ARTIX_COMX3_OFFSET,
            .name = PORT_1G_ARTIX_COMX3_NAME
        }
    },
    {
        .type_name = "temac", .sig = eDEVBRD_1G_MAC_10_SIG,
        .spec.temac = {
            .devname = "fpga0",
            .phy_addr = 1,
            .iftype = eDEVBRD_IFTYPE_SGMII,
            .portid = LOGPORT_1G_10,
            .irq = PORT_1G_ARTIX_ATP_IRQ,
            .offset = PORT_1G_ARTIX_ATP_OFFSET,
            .name = PORT_1G_ARTIX_ATP_NAME
        }
    },
    {
        .type_name = "temac", .sig = eDEVBRD_1G_MAC_11_SIG,
        .spec.temac = {
            .devname = "fpga0",
            .phy_addr = 1,
            .iftype = eDEVBRD_IFTYPE_SGMII,
            .portid = LOGPORT_1G_11,
            .irq = PORT_1G_ARTIX_FRONT_PANEL_IRQ,
            .offset = PORT_1G_ARTIX_FRONT_PANEL_OFFSET,
            .name = PORT_1G_ARTIX_FRONT_PANEL_NAME
        }
    },
    {
        .type_name = "temac", .sig = eDEVBRD_1G_MAC_12_SIG,
        .spec.temac = {
            .devname = "fpga1",
            .phy_addr = 0,
            .portid = LOGPORT_1G_V_A_0,
            .irq = PORT_1G_0_VIRTEX_ARTIX_IRQ,
            .offset = PORT_1G_0_VIRTEX_ARTIX_OFFSET,
            .name = PORT_1G_0_VIRTEX_ARTIX_NAME
        }
    },
    {
        .type_name = "temac", .sig = eDEVBRD_1G_MAC_13_SIG,
        .spec.temac = {
            .devname = "fpga1",
            .phy_addr = 0,
            .portid = LOGPORT_1G_V_A_1,
            .irq = PORT_1G_1_VIRTEX_ARTIX_IRQ,
            .offset = PORT_1G_1_VIRTEX_ARTIX_OFFSET,
            .name = PORT_1G_1_VIRTEX_ARTIX_NAME
        }
    },
    {
        .type_name = "temac", .sig = eDEVBRD_1G_MAC_14_SIG,
        .spec.temac = {
            .devname = "fpga1",
            .phy_addr = 0,
            .portid = LOGPORT_1G_V_A_1,
            .irq = PORT_1G_1_VIRTEX_ARTIX_IRQ,
            .offset = PORT_1G_2_VIRTEX_ARTIX_OFFSET
        }
    },
};

static board_device_descr_t _hses10g_brd_descr[] = {
    {
        /* J38A*/
        .type_name = "hses10g", .sig = eDEVBRD_10G_MAC_0_SIG,
        .spec.hses10g = {
            .devname = "fpga1",
            .portid = LOGPORT_10G_12,
            .offset = PORT_10G_0_NETWORK_OFFSET,
            .name = PORT_10G_0_NETWORK_NAME,
            .iftype = eDEVBRD_IFTYPE_R,
            .calib_ctl = {
                .enable = CCL_TRUE,
                .channel_num = 1,
                .channel_calib_params = {
                    {
                        .calib = {
                            {
                                .name = "txdiffswing",
                                .offset = PORT_10G_0_NETWORK_USER_CTL_OFFSET + TXDIFFCTRL_OFFSET,
                                .value = 0, 
                                .encoded_value = 0xb
                            },
                            {
                                .name = "txpost",
                                .offset = PORT_10G_0_NETWORK_USER_CTL_OFFSET + TXPOSTCURSOR_OFFSET,
                                .value = 0, /* dB */
                                .encoded_value = 0x8
                            },
                            {
                                .name = "txpre",
                                .offset = PORT_10G_0_NETWORK_USER_CTL_OFFSET + TXPRECURSOR_OFFSET,
                                .value = 0.0, /* dB */
                                .encoded_value = 0
                            },
                        }
                    }
                }
            },
            .counter_ctl = {
                .enable = CCL_TRUE,
                .width = 40,
                .ctl_offset = PORT_10G_0_NETWORK_USER_CTL_OFFSET + COUNTER_CONTROL_OFFSET,
                .ctl = {
                    .tx_pkt_rst_mask = COUNTER_CTL_TX_PACKET_CNT_RST_MASK,
                    .rx_pkt_rst_mask = COUNTER_CTL_RX_PACKET_CNT_RST_MASK,     
                    .rx_bad_pkt_rst_mask = COUNTER_CTL_RX_BAD_PACKET_CNT_RST_MASK, 
                    .freeze_counters_mask = COUNTER_CTL_FREEZE_COUNTERS_MASK,
                    .rollover_mask = COUNTER_CTL_ROLLOVER_MASK       

                },
                .counter = {
                    {
                        .name = "tx_packets_lsb",
                        .offset = PORT_10G_0_NETWORK_USER_CTL_OFFSET + TX_PKT_CNT_LSB_OFFSET
                    },
                    {
                        .name = "tx_packets_msb",
                        .offset = PORT_10G_0_NETWORK_USER_CTL_OFFSET + TX_PKT_CNT_MSB_OFFSET
                    },
                    {
                        .name = "rx_packets_lsb",
                        .offset = PORT_10G_0_NETWORK_USER_CTL_OFFSET + RX_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_packets_msb",
                        .offset = PORT_10G_0_NETWORK_USER_CTL_OFFSET + RX_PKT_CNT_MSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_lsb",
                        .offset = PORT_10G_0_NETWORK_USER_CTL_OFFSET + RX_BAD_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_msb",
                        .offset = PORT_10G_0_NETWORK_USER_CTL_OFFSET + RX_BAD_PKT_CNT_MSB_OFFSET

                    },
                }
            }
        }
    },
    {
        /* J38B*/
        .type_name = "hses10g", .sig = eDEVBRD_10G_MAC_1_SIG,
        .spec.hses10g = {
            .devname = "fpga1",
            .portid = LOGPORT_10G_13,
            .offset = PORT_10G_1_NETWORK_OFFSET,
            .name = PORT_10G_1_NETWORK_NAME,
            .iftype = eDEVBRD_IFTYPE_R,
            .calib_ctl = {
                .enable = CCL_TRUE,
                .channel_num = 1,
                .channel_calib_params = {
                    {
                        .calib = {
                            {
                                .name = "txdiffswing",
                                .offset = PORT_10G_1_NETWORK_USER_CTL_OFFSET + TXDIFFCTRL_OFFSET,
                                .value = 0, 
                                .encoded_value = 0x12
                            },
                            {
                                .name = "txpost",
                                .offset = PORT_10G_1_NETWORK_USER_CTL_OFFSET + TXPOSTCURSOR_OFFSET,
                                .value = 0, /* dB */
                                .encoded_value = 0x8
                            },
                            {
                                .name = "txpre",
                                .offset = PORT_10G_1_NETWORK_USER_CTL_OFFSET + TXPRECURSOR_OFFSET,
                                .value = 0.0, /* dB */
                                .encoded_value = 0
                            },
                        }
                    }
                }
            },
            .counter_ctl = {
                .enable = CCL_TRUE,
                .width = 40,
                .ctl_offset = PORT_10G_1_NETWORK_USER_CTL_OFFSET + COUNTER_CONTROL_OFFSET,
                .ctl = {
                    .tx_pkt_rst_mask = COUNTER_CTL_TX_PACKET_CNT_RST_MASK,
                    .rx_pkt_rst_mask = COUNTER_CTL_RX_PACKET_CNT_RST_MASK,     
                    .rx_bad_pkt_rst_mask = COUNTER_CTL_RX_BAD_PACKET_CNT_RST_MASK, 
                    .freeze_counters_mask = COUNTER_CTL_FREEZE_COUNTERS_MASK,
                    .rollover_mask = COUNTER_CTL_ROLLOVER_MASK       

                },
                .counter = {
                    {
                        .name = "tx_packets_lsb",
                        .offset = PORT_10G_1_NETWORK_USER_CTL_OFFSET + TX_PKT_CNT_LSB_OFFSET
                    },
                    {
                        .name = "tx_packets_msb",
                        .offset = PORT_10G_1_NETWORK_USER_CTL_OFFSET + TX_PKT_CNT_MSB_OFFSET
                    },
                    {
                        .name = "rx_packets_lsb",
                        .offset = PORT_10G_1_NETWORK_USER_CTL_OFFSET + RX_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_packets_msb",
                        .offset = PORT_10G_1_NETWORK_USER_CTL_OFFSET + RX_PKT_CNT_MSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_lsb",
                        .offset = PORT_10G_1_NETWORK_USER_CTL_OFFSET + RX_BAD_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_msb",
                        .offset = PORT_10G_1_NETWORK_USER_CTL_OFFSET + RX_BAD_PKT_CNT_MSB_OFFSET

                    }
                }
            }
        }
    },
    {
        /* J38C*/
        .type_name = "hses10g", .sig = eDEVBRD_10G_MAC_2_SIG,
        .spec.hses10g = {
            .devname = "fpga1",
            .portid = LOGPORT_10G_14,
            .offset = PORT_10G_2_NETWORK_OFFSET,
            .name = PORT_10G_2_NETWORK_NAME,
            .iftype = eDEVBRD_IFTYPE_R,
            .calib_ctl = {
                .enable = CCL_TRUE,
                .channel_num = 1,
                .channel_calib_params = {
                    {
                        .calib = {
                            {
                                .name = "txdiffswing",
                                .offset = PORT_10G_2_NETWORK_USER_CTL_OFFSET + TXDIFFCTRL_OFFSET,
                                .value = 0, 
                                .encoded_value = 0xf
                            },
                            {
                                .name = "txpost",
                                .offset = PORT_10G_2_NETWORK_USER_CTL_OFFSET + TXPOSTCURSOR_OFFSET,
                                .value = 0, /* dB */
                                .encoded_value = 0x6
                            },
                            {
                                .name = "txpre",
                                .offset = PORT_10G_2_NETWORK_USER_CTL_OFFSET + TXPRECURSOR_OFFSET,
                                .value = 0.0, /* dB */
                                .encoded_value = 0x2
                            },
                        }
                    }
                }
            },
            .counter_ctl = {
                .enable = CCL_TRUE,
                .width = 40,
                .ctl_offset = PORT_10G_2_NETWORK_USER_CTL_OFFSET + COUNTER_CONTROL_OFFSET,
                .ctl = {
                    .tx_pkt_rst_mask = COUNTER_CTL_TX_PACKET_CNT_RST_MASK,
                    .rx_pkt_rst_mask = COUNTER_CTL_RX_PACKET_CNT_RST_MASK,     
                    .rx_bad_pkt_rst_mask = COUNTER_CTL_RX_BAD_PACKET_CNT_RST_MASK, 
                    .freeze_counters_mask = COUNTER_CTL_FREEZE_COUNTERS_MASK,
                    .rollover_mask = COUNTER_CTL_ROLLOVER_MASK       

                },
                .counter = {
                    {
                        .name = "tx_packets_lsb",
                        .offset = PORT_10G_2_NETWORK_USER_CTL_OFFSET + TX_PKT_CNT_LSB_OFFSET
                    },
                    {
                        .name = "tx_packets_msb",
                        .offset = PORT_10G_2_NETWORK_USER_CTL_OFFSET + TX_PKT_CNT_MSB_OFFSET
                    },
                    {
                        .name = "rx_packets_lsb",
                        .offset = PORT_10G_2_NETWORK_USER_CTL_OFFSET + RX_PKT_CNT_LSB_OFFSET
                        
                    },
                    {
                        .name = "rx_packets_msb",
                        .offset = PORT_10G_2_NETWORK_USER_CTL_OFFSET + RX_PKT_CNT_MSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_lsb",
                        .offset = PORT_10G_2_NETWORK_USER_CTL_OFFSET + RX_BAD_PKT_CNT_LSB_OFFSET
                        
                    },
                    {
                        .name = "rx_bad_packets_msb",
                        .offset = PORT_10G_2_NETWORK_USER_CTL_OFFSET + RX_BAD_PKT_CNT_MSB_OFFSET

                    },
                }
            }
        }
    },
    {
        /* J38D*/
        .type_name = "hses10g", .sig = eDEVBRD_10G_MAC_3_SIG,
        .spec.hses10g = {
            .devname = "fpga1",
            .portid = LOGPORT_10G_15,
            .offset = PORT_10G_3_NETWORK_OFFSET,
            .name = PORT_10G_3_NETWORK_NAME,
            .iftype = eDEVBRD_IFTYPE_R,
            .calib_ctl = {
                .enable = CCL_TRUE,
                .channel_num = 1,
                .channel_calib_params = {
                    {
                        .calib = {
                            {
                                .name = "txdiffswing",
                                .offset = PORT_10G_3_NETWORK_USER_CTL_OFFSET + TXDIFFCTRL_OFFSET,
                                .value = 0, 
                                .encoded_value = 0x14
                            },
                            {
                                .name = "txpost",
                                .offset = PORT_10G_3_NETWORK_USER_CTL_OFFSET + TXPOSTCURSOR_OFFSET,
                                .value = 0, /* dB */
                                .encoded_value = 0x9
                            },
                            {
                                .name = "txpre",
                                .offset = PORT_10G_3_NETWORK_USER_CTL_OFFSET + TXPRECURSOR_OFFSET,
                                .value = 0.0, /* dB */
                                .encoded_value = 0x3
                            },
                        }
                    }
                }
            },
            .counter_ctl = {
                .enable = CCL_TRUE,
                .width = 40,
                .ctl_offset = PORT_10G_3_NETWORK_USER_CTL_OFFSET + COUNTER_CONTROL_OFFSET,
                .ctl = {
                    .tx_pkt_rst_mask = COUNTER_CTL_TX_PACKET_CNT_RST_MASK,
                    .rx_pkt_rst_mask = COUNTER_CTL_RX_PACKET_CNT_RST_MASK,     
                    .rx_bad_pkt_rst_mask = COUNTER_CTL_RX_BAD_PACKET_CNT_RST_MASK, 
                    .freeze_counters_mask = COUNTER_CTL_FREEZE_COUNTERS_MASK,
                    .rollover_mask = COUNTER_CTL_ROLLOVER_MASK       

                },
                .counter = {
                    {
                        .name = "tx_packets_lsb",
                        .offset = PORT_10G_3_NETWORK_USER_CTL_OFFSET + TX_PKT_CNT_LSB_OFFSET
                    },
                    {
                        .name = "tx_packets_msb",
                        .offset = PORT_10G_3_NETWORK_USER_CTL_OFFSET + TX_PKT_CNT_MSB_OFFSET
                    },
                    {
                        .name = "rx_packets_lsb",
                        .offset = PORT_10G_3_NETWORK_USER_CTL_OFFSET + RX_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_packets_msb",
                        .offset = PORT_10G_3_NETWORK_USER_CTL_OFFSET + RX_PKT_CNT_MSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_lsb",
                        .offset = PORT_10G_3_NETWORK_USER_CTL_OFFSET + RX_BAD_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_msb",
                        .offset = PORT_10G_3_NETWORK_USER_CTL_OFFSET + RX_BAD_PKT_CNT_MSB_OFFSET

                    },
                }
            }
        }
    },
    {
        /* J38E*/
        .type_name = "hses10g", .sig = eDEVBRD_10G_MAC_4_SIG,
        .spec.hses10g = {
            .devname = "fpga1",
            .portid = LOGPORT_10G_16,
            .offset = PORT_10G_4_NETWORK_OFFSET,
            .name = PORT_10G_4_NETWORK_NAME,
            .iftype = eDEVBRD_IFTYPE_R,
            .calib_ctl = {
                .enable = CCL_TRUE,
                .channel_num = 1,
                .channel_calib_params = {
                    {
                        .calib = {
                            {
                                .name = "txdiffswing",
                                .offset = PORT_10G_4_NETWORK_USER_CTL_OFFSET + TXDIFFCTRL_OFFSET,
                                .value = 0, 
                                .encoded_value = 0x14
                            },
                            {
                                .name = "txpost",
                                .offset = PORT_10G_4_NETWORK_USER_CTL_OFFSET + TXPOSTCURSOR_OFFSET,
                                .value = 0, /* dB */
                                .encoded_value = 0x6
                            },
                            {
                                .name = "txpre",
                                .offset = PORT_10G_4_NETWORK_USER_CTL_OFFSET + TXPRECURSOR_OFFSET,
                                .value = 0.0, /* dB */
                                .encoded_value = 0x3
                            },
                        }
                    }
                }
            },
            .counter_ctl = {
                .enable = CCL_TRUE,
                .width = 40,
                .ctl_offset = PORT_10G_4_NETWORK_USER_CTL_OFFSET + COUNTER_CONTROL_OFFSET,
                .ctl = {
                    .tx_pkt_rst_mask = COUNTER_CTL_TX_PACKET_CNT_RST_MASK,
                    .rx_pkt_rst_mask = COUNTER_CTL_RX_PACKET_CNT_RST_MASK,     
                    .rx_bad_pkt_rst_mask = COUNTER_CTL_RX_BAD_PACKET_CNT_RST_MASK, 
                    .freeze_counters_mask = COUNTER_CTL_FREEZE_COUNTERS_MASK,
                    .rollover_mask = COUNTER_CTL_ROLLOVER_MASK       

                },
                .counter = {
                    {
                        .name = "tx_packets_lsb",
                        .offset = PORT_10G_4_NETWORK_USER_CTL_OFFSET + TX_PKT_CNT_LSB_OFFSET
                    },
                    {
                        .name = "tx_packets_msb",
                        .offset = PORT_10G_4_NETWORK_USER_CTL_OFFSET + TX_PKT_CNT_MSB_OFFSET
                    },
                    {
                        .name = "rx_packets_lsb",
                        .offset = PORT_10G_4_NETWORK_USER_CTL_OFFSET + RX_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_packets_msb",
                        .offset = PORT_10G_4_NETWORK_USER_CTL_OFFSET + RX_PKT_CNT_MSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_lsb",
                        .offset = PORT_10G_4_NETWORK_USER_CTL_OFFSET + RX_BAD_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_msb",
                        .offset = PORT_10G_4_NETWORK_USER_CTL_OFFSET + RX_BAD_PKT_CNT_MSB_OFFSET

                    },
                }
            }
        }
    },
    {
        /* J38F*/
        .type_name = "hses10g", .sig = eDEVBRD_10G_MAC_5_SIG,
        .spec.hses10g = {
            .devname = "fpga1",
            .portid = LOGPORT_10G_17,
            .offset = PORT_10G_5_NETWORK_OFFSET,
            .name = PORT_10G_5_NETWORK_NAME,
            .iftype = eDEVBRD_IFTYPE_R,
            .calib_ctl = {
                .enable = CCL_TRUE,
                .channel_num = 1,
                .channel_calib_params = {
                    {
                        .calib = {
                            {
                                .name = "txdiffswing",
                                .offset = PORT_10G_5_NETWORK_USER_CTL_OFFSET + TXDIFFCTRL_OFFSET,
                                .value = 0, 
                                .encoded_value = 0x16
                            },
                            {
                                .name = "txpost",
                                .offset = PORT_10G_5_NETWORK_USER_CTL_OFFSET + TXPOSTCURSOR_OFFSET,
                                .value = 0, /* dB */
                                .encoded_value = 0x7
                            },
                            {
                                .name = "txpre",
                                .offset = PORT_10G_5_NETWORK_USER_CTL_OFFSET + TXPRECURSOR_OFFSET,
                                .value = 0.0, /* dB */
                                .encoded_value = 0x4
                            },
                        }
                    }
                }
            },
            .counter_ctl = {
                .enable = CCL_TRUE,
                .width = 40,
                .ctl_offset = PORT_10G_5_NETWORK_USER_CTL_OFFSET + COUNTER_CONTROL_OFFSET,
                .ctl = {
                    .tx_pkt_rst_mask = COUNTER_CTL_TX_PACKET_CNT_RST_MASK,
                    .rx_pkt_rst_mask = COUNTER_CTL_RX_PACKET_CNT_RST_MASK,     
                    .rx_bad_pkt_rst_mask = COUNTER_CTL_RX_BAD_PACKET_CNT_RST_MASK, 
                    .freeze_counters_mask = COUNTER_CTL_FREEZE_COUNTERS_MASK,
                    .rollover_mask = COUNTER_CTL_ROLLOVER_MASK       

                },
                .counter = {
                    {
                        .name = "tx_packets_lsb",
                        .offset = PORT_10G_5_NETWORK_USER_CTL_OFFSET + TX_PKT_CNT_LSB_OFFSET
                    },
                    {
                        .name = "tx_packets_msb",
                        .offset = PORT_10G_5_NETWORK_USER_CTL_OFFSET + TX_PKT_CNT_MSB_OFFSET
                    },
                    {
                        .name = "rx_packets_lsb",
                        .offset = PORT_10G_5_NETWORK_USER_CTL_OFFSET + RX_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_packets_msb",
                        .offset = PORT_10G_5_NETWORK_USER_CTL_OFFSET + RX_PKT_CNT_MSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_lsb",
                        .offset = PORT_10G_5_NETWORK_USER_CTL_OFFSET + RX_BAD_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_msb",
                        .offset = PORT_10G_5_NETWORK_USER_CTL_OFFSET + RX_BAD_PKT_CNT_MSB_OFFSET

                    },
                }
            }
        }
    },
    {
        /* J38G*/
        .type_name = "hses10g", .sig = eDEVBRD_10G_MAC_6_SIG,
        .spec.hses10g = {
            .devname = "fpga1",
            .portid = LOGPORT_10G_18,
            .offset = PORT_10G_6_NETWORK_OFFSET,
            .name = PORT_10G_6_NETWORK_NAME,
            .iftype = eDEVBRD_IFTYPE_R,
            .calib_ctl = {
                .enable = CCL_TRUE,
                .channel_num = 1,
                .channel_calib_params = {
                    {
                        .calib = {
                            {
                                .name = "txdiffswing",
                                .offset = PORT_10G_6_NETWORK_USER_CTL_OFFSET + TXDIFFCTRL_OFFSET,
                                .value = 0, 
                                .encoded_value = 0x19
                            },
                            {
                                .name = "txpost",
                                .offset = PORT_10G_6_NETWORK_USER_CTL_OFFSET + TXPOSTCURSOR_OFFSET,
                                .value = 0, /* dB */
                                .encoded_value = 0x10
                            },
                            {
                                .name = "txpre",
                                .offset = PORT_10G_6_NETWORK_USER_CTL_OFFSET + TXPRECURSOR_OFFSET,
                                .value = 0.0, /* dB */
                                .encoded_value = 0x1
                            },
                        }
                    }
                }
            },
            .counter_ctl = {
                .enable = CCL_TRUE,
                .width = 40,
                .ctl_offset = PORT_10G_6_NETWORK_USER_CTL_OFFSET + COUNTER_CONTROL_OFFSET,
                .ctl = {
                    .tx_pkt_rst_mask = COUNTER_CTL_TX_PACKET_CNT_RST_MASK,
                    .rx_pkt_rst_mask = COUNTER_CTL_RX_PACKET_CNT_RST_MASK,     
                    .rx_bad_pkt_rst_mask = COUNTER_CTL_RX_BAD_PACKET_CNT_RST_MASK, 
                    .freeze_counters_mask = COUNTER_CTL_FREEZE_COUNTERS_MASK,
                    .rollover_mask = COUNTER_CTL_ROLLOVER_MASK       

                },
                .counter = {
                    {
                        .name = "tx_packets_lsb",
                        .offset = PORT_10G_6_NETWORK_USER_CTL_OFFSET + TX_PKT_CNT_LSB_OFFSET
                    },
                    {
                        .name = "tx_packets_msb",
                        .offset = PORT_10G_6_NETWORK_USER_CTL_OFFSET + TX_PKT_CNT_MSB_OFFSET
                    },
                    {
                        .name = "rx_packets_lsb",
                        .offset = PORT_10G_6_NETWORK_USER_CTL_OFFSET + RX_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_packets_msb",
                        .offset = PORT_10G_6_NETWORK_USER_CTL_OFFSET + RX_PKT_CNT_MSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_lsb",
                        .offset = PORT_10G_6_NETWORK_USER_CTL_OFFSET + RX_BAD_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_msb",
                        .offset = PORT_10G_6_NETWORK_USER_CTL_OFFSET + RX_BAD_PKT_CNT_MSB_OFFSET

                    },
                }
            }
        }
    },
    {
        /* J38H*/
        .type_name = "hses10g", .sig = eDEVBRD_10G_MAC_7_SIG,
        .spec.hses10g = {
            .devname = "fpga1",
            .portid = LOGPORT_10G_19,
            .offset = PORT_10G_7_NETWORK_OFFSET,
            .name = PORT_10G_7_NETWORK_NAME,
            .iftype = eDEVBRD_IFTYPE_R,
            .calib_ctl = {
                .enable = CCL_TRUE,
                .channel_num = 1,
                .channel_calib_params = {
                    {
                        .calib = {
                            {
                                .name = "txdiffswing",
                                .offset = PORT_10G_7_NETWORK_USER_CTL_OFFSET + TXDIFFCTRL_OFFSET,
                                .value = 0, 
                                .encoded_value = 0x1b
                            },
                            {
                                .name = "txpost",
                                .offset = PORT_10G_7_NETWORK_USER_CTL_OFFSET + TXPOSTCURSOR_OFFSET,
                                .value = 0, /* dB */
                                .encoded_value = 0x12
                            },
                            {
                                .name = "txpre",
                                .offset = PORT_10G_7_NETWORK_USER_CTL_OFFSET + TXPRECURSOR_OFFSET,
                                .value = 0.0, /* dB */
                                .encoded_value = 0x4
                            },
                        }
                    }
                }
            },
            .counter_ctl = {
                .enable = CCL_TRUE,
                .width = 40,
                .ctl_offset = PORT_10G_7_NETWORK_USER_CTL_OFFSET + COUNTER_CONTROL_OFFSET,
                .ctl = {
                    .tx_pkt_rst_mask = COUNTER_CTL_TX_PACKET_CNT_RST_MASK,
                    .rx_pkt_rst_mask = COUNTER_CTL_RX_PACKET_CNT_RST_MASK,     
                    .rx_bad_pkt_rst_mask = COUNTER_CTL_RX_BAD_PACKET_CNT_RST_MASK, 
                    .freeze_counters_mask = COUNTER_CTL_FREEZE_COUNTERS_MASK,
                    .rollover_mask = COUNTER_CTL_ROLLOVER_MASK       

                },
                .counter = {
                    {
                        .name = "tx_packets_lsb",
                        .offset = PORT_10G_7_NETWORK_USER_CTL_OFFSET + TX_PKT_CNT_LSB_OFFSET
                    },
                    {
                        .name = "tx_packets_msb",
                        .offset = PORT_10G_7_NETWORK_USER_CTL_OFFSET + TX_PKT_CNT_MSB_OFFSET
                    },
                    {
                        .name = "rx_packets_lsb",
                        .offset = PORT_10G_7_NETWORK_USER_CTL_OFFSET + RX_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_packets_msb",
                        .offset = PORT_10G_7_NETWORK_USER_CTL_OFFSET + RX_PKT_CNT_MSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_lsb",
                        .offset = PORT_10G_7_NETWORK_USER_CTL_OFFSET + RX_BAD_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_msb",
                        .offset = PORT_10G_7_NETWORK_USER_CTL_OFFSET + RX_BAD_PKT_CNT_MSB_OFFSET

                    },
                }
            }
        }
    },
    {
        /* J18A*/
        .type_name = "hses10g", .sig = eDEVBRD_10G_MAC_8_SIG,
        .spec.hses10g = {
            .devname = "fpga1",
            .portid = LOGPORT_10G_20,
            .offset = PORT_10G_8_NETWORK_OFFSET,
            .name = PORT_10G_8_NETWORK_NAME,
            .iftype = eDEVBRD_IFTYPE_R,
            .calib_ctl = {
                .enable = CCL_TRUE,
                .channel_num = 1,
                .channel_calib_params = {
                    {
                        .calib = {
                            {
                                .name = "txdiffswing",
                                .offset = PORT_10G_8_NETWORK_USER_CTL_OFFSET + TXDIFFCTRL_OFFSET,
                                .value = 0, 
                                .encoded_value = 0x1a
                            },
                            {
                                .name = "txpost",
                                .offset = PORT_10G_8_NETWORK_USER_CTL_OFFSET + TXPOSTCURSOR_OFFSET,
                                .value = 0, /* dB */
                                .encoded_value = 0xf
                            },
                            {
                                .name = "txpre",
                                .offset = PORT_10G_8_NETWORK_USER_CTL_OFFSET + TXPRECURSOR_OFFSET,
                                .value = 0.0, /* dB */
                                .encoded_value = 0x2
                            },
                        }
                    }
                }
            },
            .counter_ctl = {
                .enable = CCL_TRUE,
                .width = 40,
                .ctl_offset = PORT_10G_8_NETWORK_USER_CTL_OFFSET + COUNTER_CONTROL_OFFSET,
                .ctl = {
                    .tx_pkt_rst_mask = COUNTER_CTL_TX_PACKET_CNT_RST_MASK,
                    .rx_pkt_rst_mask = COUNTER_CTL_RX_PACKET_CNT_RST_MASK,     
                    .rx_bad_pkt_rst_mask = COUNTER_CTL_RX_BAD_PACKET_CNT_RST_MASK, 
                    .freeze_counters_mask = COUNTER_CTL_FREEZE_COUNTERS_MASK,
                    .rollover_mask = COUNTER_CTL_ROLLOVER_MASK       

                },
                .counter = {
                    {
                        .name = "tx_packets_lsb",
                        .offset = PORT_10G_8_NETWORK_USER_CTL_OFFSET + TX_PKT_CNT_LSB_OFFSET
                    },
                    {
                        .name = "tx_packets_msb",
                        .offset = PORT_10G_8_NETWORK_USER_CTL_OFFSET + TX_PKT_CNT_MSB_OFFSET
                    },
                    {
                        .name = "rx_packets_lsb",
                        .offset = PORT_10G_8_NETWORK_USER_CTL_OFFSET + RX_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_packets_msb",
                        .offset = PORT_10G_8_NETWORK_USER_CTL_OFFSET + RX_PKT_CNT_MSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_lsb",
                        .offset = PORT_10G_8_NETWORK_USER_CTL_OFFSET + RX_BAD_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_msb",
                        .offset = PORT_10G_8_NETWORK_USER_CTL_OFFSET + RX_BAD_PKT_CNT_MSB_OFFSET

                    },
                }
            }
        }
    },
    {
        /* J18B*/
        .type_name = "hses10g", .sig = eDEVBRD_10G_MAC_9_SIG,
        .spec.hses10g = {
            .devname = "fpga1",
            .portid = LOGPORT_10G_21,
            .offset = PORT_10G_9_NETWORK_OFFSET,
            .name = PORT_10G_9_NETWORK_NAME,
            .iftype = eDEVBRD_IFTYPE_R,
            .calib_ctl = {
                .enable = CCL_TRUE,
                .channel_num = 1,
                .channel_calib_params = {
                    {
                        .calib = {
                            {
                                .name = "txdiffswing",
                                .offset = PORT_10G_9_NETWORK_USER_CTL_OFFSET + TXDIFFCTRL_OFFSET,
                                .value = 0, 
                                .encoded_value = 0x19
                            },
                            {
                                .name = "txpost",
                                .offset = PORT_10G_9_NETWORK_USER_CTL_OFFSET + TXPOSTCURSOR_OFFSET,
                                .value = 0, /* dB */
                                .encoded_value = 0x14
                            },
                            {
                                .name = "txpre",
                                .offset = PORT_10G_9_NETWORK_USER_CTL_OFFSET + TXPRECURSOR_OFFSET,
                                .value = 0.0, /* dB */
                                .encoded_value = 0x3
                            },
                        }
                    }
                }
            },
            .counter_ctl = {
                .enable = CCL_TRUE,
                .width = 40,
                .ctl_offset = PORT_10G_9_NETWORK_USER_CTL_OFFSET + COUNTER_CONTROL_OFFSET,
                .ctl = {
                    .tx_pkt_rst_mask = COUNTER_CTL_TX_PACKET_CNT_RST_MASK,
                    .rx_pkt_rst_mask = COUNTER_CTL_RX_PACKET_CNT_RST_MASK,     
                    .rx_bad_pkt_rst_mask = COUNTER_CTL_RX_BAD_PACKET_CNT_RST_MASK, 
                    .freeze_counters_mask = COUNTER_CTL_FREEZE_COUNTERS_MASK,
                    .rollover_mask = COUNTER_CTL_ROLLOVER_MASK       

                },
                .counter = {
                    {
                        .name = "tx_packets_lsb",
                        .offset = PORT_10G_9_NETWORK_USER_CTL_OFFSET + TX_PKT_CNT_LSB_OFFSET
                    },
                    {
                        .name = "tx_packets_msb",
                        .offset = PORT_10G_9_NETWORK_USER_CTL_OFFSET + TX_PKT_CNT_MSB_OFFSET
                    },
                    {
                        .name = "rx_packets_lsb",
                        .offset = PORT_10G_9_NETWORK_USER_CTL_OFFSET + RX_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_packets_msb",
                        .offset = PORT_10G_9_NETWORK_USER_CTL_OFFSET + RX_PKT_CNT_MSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_lsb",
                        .offset = PORT_10G_9_NETWORK_USER_CTL_OFFSET + RX_BAD_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_msb",
                        .offset = PORT_10G_9_NETWORK_USER_CTL_OFFSET + RX_BAD_PKT_CNT_MSB_OFFSET

                    },
                }
            }
        }
    },
    {
        /* J18C*/
        .type_name = "hses10g", .sig = eDEVBRD_10G_MAC_10_SIG,
        .spec.hses10g = {
            .devname = "fpga1",
            .portid = LOGPORT_10G_22,
            .offset = PORT_10G_10_NETWORK_OFFSET,
            .name = PORT_10G_10_NETWORK_NAME,
            .iftype = eDEVBRD_IFTYPE_R,
            .calib_ctl = {
                .enable = CCL_TRUE,
                .channel_num = 1,
                .channel_calib_params = {
                    {
                        .calib = {
                            {
                                .name = "txdiffswing",
                                .offset = PORT_10G_10_NETWORK_USER_CTL_OFFSET + TXDIFFCTRL_OFFSET,
                                .value = 0, 
                                .encoded_value = 0x1b
                            },
                            {
                                .name = "txpost",
                                .offset = PORT_10G_10_NETWORK_USER_CTL_OFFSET + TXPOSTCURSOR_OFFSET,
                                .value = 0, /* dB */
                                .encoded_value = 0x11
                            },
                            {
                                .name = "txpre",
                                .offset = PORT_10G_10_NETWORK_USER_CTL_OFFSET + TXPRECURSOR_OFFSET,
                                .value = 0.0, /* dB */
                                .encoded_value = 0x1
                            },
                        }
                    }
                }
            },
            .counter_ctl = {
                .enable = CCL_TRUE,
                .width = 40,
                .ctl_offset = PORT_10G_10_NETWORK_USER_CTL_OFFSET + COUNTER_CONTROL_OFFSET,
                .ctl = {
                    .tx_pkt_rst_mask = COUNTER_CTL_TX_PACKET_CNT_RST_MASK,
                    .rx_pkt_rst_mask = COUNTER_CTL_RX_PACKET_CNT_RST_MASK,     
                    .rx_bad_pkt_rst_mask = COUNTER_CTL_RX_BAD_PACKET_CNT_RST_MASK, 
                    .freeze_counters_mask = COUNTER_CTL_FREEZE_COUNTERS_MASK,
                    .rollover_mask = COUNTER_CTL_ROLLOVER_MASK       

                },
                .counter = {
                    {
                        .name = "tx_packets_lsb",
                        .offset = PORT_10G_10_NETWORK_USER_CTL_OFFSET + TX_PKT_CNT_LSB_OFFSET
                    },
                    {
                        .name = "tx_packets_msb",
                        .offset = PORT_10G_10_NETWORK_USER_CTL_OFFSET + TX_PKT_CNT_MSB_OFFSET
                    },
                    {
                        .name = "rx_packets_lsb",
                        .offset = PORT_10G_10_NETWORK_USER_CTL_OFFSET + RX_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_packets_msb",
                        .offset = PORT_10G_10_NETWORK_USER_CTL_OFFSET + RX_PKT_CNT_MSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_lsb",
                        .offset = PORT_10G_10_NETWORK_USER_CTL_OFFSET + RX_BAD_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_msb",
                        .offset = PORT_10G_10_NETWORK_USER_CTL_OFFSET + RX_BAD_PKT_CNT_MSB_OFFSET

                    },
                }
            }
        }
    },
    {
        /* J18D*/
        .type_name = "hses10g", .sig = eDEVBRD_10G_MAC_11_SIG,
        .spec.hses10g = {
            .devname = "fpga1",
            .portid = LOGPORT_10G_23,
            .offset = PORT_10G_11_NETWORK_OFFSET,
            .name = PORT_10G_11_NETWORK_NAME,
            .iftype = eDEVBRD_IFTYPE_R,
            .calib_ctl = {
                .enable = CCL_TRUE,
                .channel_num = 1,
                .channel_calib_params = {
                    {
                        .calib = {
                            {
                                .name = "txdiffswing",
                                .offset = PORT_10G_11_NETWORK_USER_CTL_OFFSET + TXDIFFCTRL_OFFSET,
                                .value = 0, 
                                .encoded_value = 0x1a
                            },
                            {
                                .name = "txpost",
                                .offset = PORT_10G_11_NETWORK_USER_CTL_OFFSET + TXPOSTCURSOR_OFFSET,
                                .value = 1.94, /* dB */
                                .encoded_value = 0x13
                            },
                            {
                                .name = "txpre",
                                .offset = PORT_10G_11_NETWORK_USER_CTL_OFFSET + TXPRECURSOR_OFFSET,
                                .value = 0.0, /* dB */
                                .encoded_value = 0x5
                            },
                        }
                    }
                }
            },
            .counter_ctl = {
                .enable = CCL_TRUE,
                .width = 40,
                .ctl_offset = PORT_10G_11_NETWORK_USER_CTL_OFFSET + COUNTER_CONTROL_OFFSET,
                .ctl = {
                    .tx_pkt_rst_mask = COUNTER_CTL_TX_PACKET_CNT_RST_MASK,
                    .rx_pkt_rst_mask = COUNTER_CTL_RX_PACKET_CNT_RST_MASK,     
                    .rx_bad_pkt_rst_mask = COUNTER_CTL_RX_BAD_PACKET_CNT_RST_MASK, 
                    .freeze_counters_mask = COUNTER_CTL_FREEZE_COUNTERS_MASK,
                    .rollover_mask = COUNTER_CTL_ROLLOVER_MASK       

                },
                .counter = {
                    {
                        .name = "tx_packets_lsb",
                        .offset = PORT_10G_11_NETWORK_USER_CTL_OFFSET + TX_PKT_CNT_LSB_OFFSET
                    },
                    {
                        .name = "tx_packets_msb",
                        .offset = PORT_10G_11_NETWORK_USER_CTL_OFFSET + TX_PKT_CNT_MSB_OFFSET
                    },
                    {
                        .name = "rx_packets_lsb",
                        .offset = PORT_10G_11_NETWORK_USER_CTL_OFFSET + RX_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_packets_msb",
                        .offset = PORT_10G_11_NETWORK_USER_CTL_OFFSET + RX_PKT_CNT_MSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_lsb",
                        .offset = PORT_10G_11_NETWORK_USER_CTL_OFFSET + RX_BAD_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_msb",
                        .offset = PORT_10G_11_NETWORK_USER_CTL_OFFSET + RX_BAD_PKT_CNT_MSB_OFFSET

                    },
                }
            }
        }
    },
    {
        /* J17A*/
        .type_name = "hses10g", .sig = eDEVBRD_10G_MAC_12_SIG,
        .spec.hses10g = {
            .devname = "fpga1",
            .portid = LOGPORT_10G_0,
            .offset = PORT_10G_0_CLIENT_OFFSET,
            .name = PORT_10G_0_CLIENT_NAME,
            .iftype = eDEVBRD_IFTYPE_R,
            .calib_ctl = {
                .enable = CCL_TRUE,
                .channel_num = 1,
                .channel_calib_params = {
                    {
                        .calib = {
                            {
                                .name = "txdiffswing",
                                .offset = PORT_10G_0_CLIENT_USER_CTL_OFFSET + TXDIFFCTRL_OFFSET,
                                .value = 0, 
                                .encoded_value = 0x1b
                            },
                            {
                                .name = "txpost",
                                .offset = PORT_10G_0_CLIENT_USER_CTL_OFFSET + TXPOSTCURSOR_OFFSET,
                                .value = 0, /* dB */
                                .encoded_value = 0x11
                            },
                            {
                                .name = "txpre",
                                .offset = PORT_10G_0_CLIENT_USER_CTL_OFFSET + TXPRECURSOR_OFFSET,
                                .value = 0.0, /* dB */
                                .encoded_value = 0x1
                            },
                        }
                    }
                }
            },
            .counter_ctl = {
                .enable = CCL_TRUE,
                .width = 40,
                .ctl_offset = PORT_10G_0_CLIENT_USER_CTL_OFFSET + COUNTER_CONTROL_OFFSET,
                .ctl = {
                    .tx_pkt_rst_mask = COUNTER_CTL_TX_PACKET_CNT_RST_MASK,
                    .rx_pkt_rst_mask = COUNTER_CTL_RX_PACKET_CNT_RST_MASK,     
                    .rx_bad_pkt_rst_mask = COUNTER_CTL_RX_BAD_PACKET_CNT_RST_MASK, 
                    .freeze_counters_mask = COUNTER_CTL_FREEZE_COUNTERS_MASK,
                    .rollover_mask = COUNTER_CTL_ROLLOVER_MASK       

                },
                .counter = {
                    {
                        .name = "tx_packets_lsb",
                        .offset = PORT_10G_0_CLIENT_USER_CTL_OFFSET + TX_PKT_CNT_LSB_OFFSET
                    },
                    {
                        .name = "tx_packets_msb",
                        .offset = PORT_10G_0_CLIENT_USER_CTL_OFFSET + TX_PKT_CNT_MSB_OFFSET
                    },
                    {
                        .name = "rx_packets_lsb",
                        .offset = PORT_10G_0_CLIENT_USER_CTL_OFFSET + RX_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_packets_msb",
                        .offset = PORT_10G_0_CLIENT_USER_CTL_OFFSET + RX_PKT_CNT_MSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_lsb",
                        .offset = PORT_10G_0_CLIENT_USER_CTL_OFFSET + RX_BAD_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_msb",
                        .offset = PORT_10G_0_CLIENT_USER_CTL_OFFSET + RX_BAD_PKT_CNT_MSB_OFFSET

                    },
                }
            }
        }
    },
    {
        /* J17B*/
        .type_name = "hses10g", .sig = eDEVBRD_10G_MAC_13_SIG,
        .spec.hses10g = {
            .devname = "fpga1",
            .portid = LOGPORT_10G_1,
            .offset = PORT_10G_1_CLIENT_OFFSET,
            .name = PORT_10G_1_CLIENT_NAME,
            .iftype = eDEVBRD_IFTYPE_R,
            .calib_ctl = {
                .enable = CCL_TRUE,
                .channel_num = 1,
                .channel_calib_params = {
                    {
                        .calib = {
                            {
                                .name = "txdiffswing",
                                .offset = PORT_10G_1_CLIENT_USER_CTL_OFFSET + TXDIFFCTRL_OFFSET,
                                .value = 0, 
                                .encoded_value = 0x1f
                            },
                            {
                                .name = "txpost",
                                .offset = PORT_10G_1_CLIENT_USER_CTL_OFFSET + TXPOSTCURSOR_OFFSET,
                                .value = 0, /* dB */
                                .encoded_value = 0x15
                            },
                            {
                                .name = "txpre",
                                .offset = PORT_10G_1_CLIENT_USER_CTL_OFFSET + TXPRECURSOR_OFFSET,
                                .value = 0.0, /* dB */
                                .encoded_value = 0
                            },
                        }
                    }
                }
            },
            .counter_ctl = {
                .enable = CCL_TRUE,
                .width = 40,
                .ctl_offset = PORT_10G_1_CLIENT_USER_CTL_OFFSET + COUNTER_CONTROL_OFFSET,
                .ctl = {
                    .tx_pkt_rst_mask = COUNTER_CTL_TX_PACKET_CNT_RST_MASK,
                    .rx_pkt_rst_mask = COUNTER_CTL_RX_PACKET_CNT_RST_MASK,     
                    .rx_bad_pkt_rst_mask = COUNTER_CTL_RX_BAD_PACKET_CNT_RST_MASK, 
                    .freeze_counters_mask = COUNTER_CTL_FREEZE_COUNTERS_MASK,
                    .rollover_mask = COUNTER_CTL_ROLLOVER_MASK       

                },
                .counter = {
                    {
                        .name = "tx_packets_lsb",
                        .offset = PORT_10G_1_CLIENT_USER_CTL_OFFSET + TX_PKT_CNT_LSB_OFFSET
                    },
                    {
                        .name = "tx_packets_msb",
                        .offset = PORT_10G_1_CLIENT_USER_CTL_OFFSET + TX_PKT_CNT_MSB_OFFSET
                    },
                    {
                        .name = "rx_packets_lsb",
                        .offset = PORT_10G_1_CLIENT_USER_CTL_OFFSET + RX_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_packets_msb",
                        .offset = PORT_10G_1_CLIENT_USER_CTL_OFFSET + RX_PKT_CNT_MSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_lsb",
                        .offset = PORT_10G_1_CLIENT_USER_CTL_OFFSET + RX_BAD_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_msb",
                        .offset = PORT_10G_1_CLIENT_USER_CTL_OFFSET + RX_BAD_PKT_CNT_MSB_OFFSET

                    },
                }
            }
        }
    },
    {
        /* J17C*/
        .type_name = "hses10g", .sig = eDEVBRD_10G_MAC_14_SIG,
        .spec.hses10g = {
            .devname = "fpga1",
            .portid = LOGPORT_10G_2,
            .offset = PORT_10G_2_CLIENT_OFFSET,
            .name = PORT_10G_2_CLIENT_NAME,
            .iftype = eDEVBRD_IFTYPE_R,
            .calib_ctl = {
                .enable = CCL_TRUE,
                .channel_num = 1,
                .channel_calib_params = {
                    {
                        .calib = {
                            {
                                .name = "txdiffswing",
                                .offset = PORT_10G_2_CLIENT_USER_CTL_OFFSET + TXDIFFCTRL_OFFSET,
                                .value = 0, 
                                .encoded_value = 0xf
                            },
                            {
                                .name = "txpost",
                                .offset = PORT_10G_2_CLIENT_USER_CTL_OFFSET + TXPOSTCURSOR_OFFSET,
                                .value = 0, /* dB */
                                .encoded_value = 0xc
                            },
                            {
                                .name = "txpre",
                                .offset = PORT_10G_2_CLIENT_USER_CTL_OFFSET + TXPRECURSOR_OFFSET,
                                .value = 0.0, /* dB */
                                .encoded_value = 0
                            },
                        }
                    }
                }
            },
            .counter_ctl = {
                .enable = CCL_TRUE,
                .width = 40,
                .ctl_offset = PORT_10G_2_CLIENT_USER_CTL_OFFSET + COUNTER_CONTROL_OFFSET,
                .ctl = {
                    .tx_pkt_rst_mask = COUNTER_CTL_TX_PACKET_CNT_RST_MASK,
                    .rx_pkt_rst_mask = COUNTER_CTL_RX_PACKET_CNT_RST_MASK,     
                    .rx_bad_pkt_rst_mask = COUNTER_CTL_RX_BAD_PACKET_CNT_RST_MASK, 
                    .freeze_counters_mask = COUNTER_CTL_FREEZE_COUNTERS_MASK,
                    .rollover_mask = COUNTER_CTL_ROLLOVER_MASK       

                },
                .counter = {
                    {
                        .name = "tx_packets_lsb",
                        .offset = PORT_10G_2_CLIENT_USER_CTL_OFFSET + TX_PKT_CNT_LSB_OFFSET
                    },
                    {
                        .name = "tx_packets_msb",
                        .offset = PORT_10G_2_CLIENT_USER_CTL_OFFSET + TX_PKT_CNT_MSB_OFFSET
                    },
                    {
                        .name = "rx_packets_lsb",
                        .offset = PORT_10G_2_CLIENT_USER_CTL_OFFSET + RX_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_packets_msb",
                        .offset = PORT_10G_2_CLIENT_USER_CTL_OFFSET + RX_PKT_CNT_MSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_lsb",
                        .offset = PORT_10G_2_CLIENT_USER_CTL_OFFSET + RX_BAD_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_msb",
                        .offset = PORT_10G_2_CLIENT_USER_CTL_OFFSET + RX_BAD_PKT_CNT_MSB_OFFSET

                    },
                }
            }
        }
    },
    {
        /* J17D*/
        .type_name = "hses10g", .sig = eDEVBRD_10G_MAC_15_SIG,
        .spec.hses10g = {
            .devname = "fpga1",
            .portid = LOGPORT_10G_3,
            .offset = PORT_10G_3_CLIENT_OFFSET,
            .name = PORT_10G_3_CLIENT_NAME,
            .iftype = eDEVBRD_IFTYPE_R,
            .calib_ctl = {
                .enable = CCL_TRUE,
                .channel_num = 1,
                .channel_calib_params = {
                    {
                        .calib = {
                            {
                                .name = "txdiffswing",
                                .offset = PORT_10G_3_CLIENT_USER_CTL_OFFSET + TXDIFFCTRL_OFFSET,
                                .value = 0, 
                                .encoded_value = 0x1e
                            },
                            {
                                .name = "txpost",
                                .offset = PORT_10G_3_CLIENT_USER_CTL_OFFSET + TXPOSTCURSOR_OFFSET,
                                .value = 0, /* dB */
                                .encoded_value = 0x12
                            },
                            {
                                .name = "txpre",
                                .offset = PORT_10G_3_CLIENT_USER_CTL_OFFSET + TXPRECURSOR_OFFSET,
                                .value = 0.0, /* dB */
                                .encoded_value = 0
                            },
                        }
                    }
                }
            },
            .counter_ctl = {
                .enable = CCL_TRUE,
                .width = 40,
                .ctl_offset = PORT_10G_3_CLIENT_USER_CTL_OFFSET + COUNTER_CONTROL_OFFSET,
                .ctl = {
                    .tx_pkt_rst_mask = COUNTER_CTL_TX_PACKET_CNT_RST_MASK,
                    .rx_pkt_rst_mask = COUNTER_CTL_RX_PACKET_CNT_RST_MASK,     
                    .rx_bad_pkt_rst_mask = COUNTER_CTL_RX_BAD_PACKET_CNT_RST_MASK, 
                    .freeze_counters_mask = COUNTER_CTL_FREEZE_COUNTERS_MASK,
                    .rollover_mask = COUNTER_CTL_ROLLOVER_MASK       

                },
                .counter = {
                    {
                        .name = "tx_packets_lsb",
                        .offset = PORT_10G_3_CLIENT_USER_CTL_OFFSET + TX_PKT_CNT_LSB_OFFSET
                    },
                    {
                        .name = "tx_packets_msb",
                        .offset = PORT_10G_3_CLIENT_USER_CTL_OFFSET + TX_PKT_CNT_MSB_OFFSET
                    },
                    {
                        .name = "rx_packets_lsb",
                        .offset = PORT_10G_3_CLIENT_USER_CTL_OFFSET + RX_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_packets_msb",
                        .offset = PORT_10G_3_CLIENT_USER_CTL_OFFSET + RX_PKT_CNT_MSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_lsb",
                        .offset = PORT_10G_3_CLIENT_USER_CTL_OFFSET + RX_BAD_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_msb",
                        .offset = PORT_10G_3_CLIENT_USER_CTL_OFFSET + RX_BAD_PKT_CNT_MSB_OFFSET

                    },
                }
            }
        }
    },
    {
        /* J37A*/
        .type_name = "hses10g", .sig = eDEVBRD_10G_MAC_16_SIG,
        .spec.hses10g = {
            .devname = "fpga1",
            .portid = LOGPORT_10G_4,
            .offset = PORT_10G_4_CLIENT_OFFSET,
            .name = PORT_10G_4_CLIENT_NAME,
            .iftype = eDEVBRD_IFTYPE_R,
            .calib_ctl = {
                .enable = CCL_TRUE,
                .channel_num = 1,
                .channel_calib_params = {
                    {
                        .calib = {
                            {
                                .name = "txdiffswing",
                                .offset = PORT_10G_4_CLIENT_USER_CTL_OFFSET + TXDIFFCTRL_OFFSET,
                                .value = 0, 
                                .encoded_value = 0x1a
                            },
                            {
                                .name = "txpost",
                                .offset = PORT_10G_4_CLIENT_USER_CTL_OFFSET + TXPOSTCURSOR_OFFSET,
                                .value = 0, /* dB */
                                .encoded_value = 0x14
                            },
                            {
                                .name = "txpre",
                                .offset = PORT_10G_4_CLIENT_USER_CTL_OFFSET + TXPRECURSOR_OFFSET,
                                .value = 0.0, /* dB */
                                .encoded_value = 0
                            },
                        }
                    }
                }
            },
            .counter_ctl = {
                .enable = CCL_TRUE,
                .width = 40,
                .ctl_offset = PORT_10G_4_CLIENT_USER_CTL_OFFSET + COUNTER_CONTROL_OFFSET,
                .ctl = {
                    .tx_pkt_rst_mask = COUNTER_CTL_TX_PACKET_CNT_RST_MASK,
                    .rx_pkt_rst_mask = COUNTER_CTL_RX_PACKET_CNT_RST_MASK,     
                    .rx_bad_pkt_rst_mask = COUNTER_CTL_RX_BAD_PACKET_CNT_RST_MASK, 
                    .freeze_counters_mask = COUNTER_CTL_FREEZE_COUNTERS_MASK,
                    .rollover_mask = COUNTER_CTL_ROLLOVER_MASK       

                },
                .counter = {
                    {
                        .name = "tx_packets_lsb",
                        .offset = PORT_10G_4_CLIENT_USER_CTL_OFFSET + TX_PKT_CNT_LSB_OFFSET
                    },
                    {
                        .name = "tx_packets_msb",
                        .offset = PORT_10G_4_CLIENT_USER_CTL_OFFSET + TX_PKT_CNT_MSB_OFFSET
                    },
                    {
                        .name = "rx_packets_lsb",
                        .offset = PORT_10G_4_CLIENT_USER_CTL_OFFSET + RX_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_packets_msb",
                        .offset = PORT_10G_4_CLIENT_USER_CTL_OFFSET + RX_PKT_CNT_MSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_lsb",
                        .offset = PORT_10G_4_CLIENT_USER_CTL_OFFSET + RX_BAD_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_msb",
                        .offset = PORT_10G_4_CLIENT_USER_CTL_OFFSET + RX_BAD_PKT_CNT_MSB_OFFSET

                    },
                }
            }
        }
    },
    {
        /* J37B*/
        .type_name = "hses10g", .sig = eDEVBRD_10G_MAC_17_SIG,
        .spec.hses10g = {
            .devname = "fpga1",
            .portid = LOGPORT_10G_5,
            .offset = PORT_10G_5_CLIENT_OFFSET,
            .name = PORT_10G_5_CLIENT_NAME,
            .iftype = eDEVBRD_IFTYPE_R,
            .calib_ctl = {
                .enable = CCL_TRUE,
                .channel_num = 1,
                .channel_calib_params = {
                    {
                        .calib = {
                            {
                                .name = "txdiffswing",
                                .offset = PORT_10G_5_CLIENT_USER_CTL_OFFSET + TXDIFFCTRL_OFFSET,
                                .value = 0, 
                                .encoded_value = 0xc
                            },
                            {
                                .name = "txpost",
                                .offset = PORT_10G_5_CLIENT_USER_CTL_OFFSET + TXPOSTCURSOR_OFFSET,
                                .value = 0, /* dB */
                                .encoded_value = 0x12
                            },
                            {
                                .name = "txpre",
                                .offset = PORT_10G_5_CLIENT_USER_CTL_OFFSET + TXPRECURSOR_OFFSET,
                                .value = 0.0, /* dB */
                                .encoded_value = 0x1
                            },
                        }
                    }
                }
            },
            .counter_ctl = {
                .enable = CCL_TRUE,
                .width = 40,
                .ctl_offset = PORT_10G_5_CLIENT_USER_CTL_OFFSET + COUNTER_CONTROL_OFFSET,
                .ctl = {
                    .tx_pkt_rst_mask = COUNTER_CTL_TX_PACKET_CNT_RST_MASK,
                    .rx_pkt_rst_mask = COUNTER_CTL_RX_PACKET_CNT_RST_MASK,     
                    .rx_bad_pkt_rst_mask = COUNTER_CTL_RX_BAD_PACKET_CNT_RST_MASK, 
                    .freeze_counters_mask = COUNTER_CTL_FREEZE_COUNTERS_MASK,
                    .rollover_mask = COUNTER_CTL_ROLLOVER_MASK       

                },
                .counter = {
                    {
                        .name = "tx_packets_lsb",
                        .offset = PORT_10G_5_CLIENT_USER_CTL_OFFSET + TX_PKT_CNT_LSB_OFFSET
                    },
                    {
                        .name = "tx_packets_msb",
                        .offset = PORT_10G_5_CLIENT_USER_CTL_OFFSET + TX_PKT_CNT_MSB_OFFSET
                    },
                    {
                        .name = "rx_packets_lsb",
                        .offset = PORT_10G_5_CLIENT_USER_CTL_OFFSET + RX_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_packets_msb",
                        .offset = PORT_10G_5_CLIENT_USER_CTL_OFFSET + RX_PKT_CNT_MSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_lsb",
                        .offset = PORT_10G_5_CLIENT_USER_CTL_OFFSET + RX_BAD_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_msb",
                        .offset = PORT_10G_5_CLIENT_USER_CTL_OFFSET + RX_BAD_PKT_CNT_MSB_OFFSET

                    },
                }
            }
        }
    },
    {
        /* J37C*/
        .type_name = "hses10g", .sig = eDEVBRD_10G_MAC_18_SIG,
        .spec.hses10g = {
            .devname = "fpga1",
            .portid = LOGPORT_10G_6,
            .offset = PORT_10G_6_CLIENT_OFFSET,
            .name = PORT_10G_6_CLIENT_NAME,
            .iftype = eDEVBRD_IFTYPE_R,
            .calib_ctl = {
                .enable = CCL_TRUE,
                .channel_num = 1,
                .channel_calib_params = {
                    {
                        .calib = {
                            {
                                .name = "txdiffswing",
                                .offset = PORT_10G_6_CLIENT_USER_CTL_OFFSET + TXDIFFCTRL_OFFSET,
                                .value = 0, 
                                .encoded_value = 0x11
                            },
                            {
                                .name = "txpost",
                                .offset = PORT_10G_6_CLIENT_USER_CTL_OFFSET + TXPOSTCURSOR_OFFSET,
                                .value = 0, /* dB */
                                .encoded_value = 0x8
                            },
                            {
                                .name = "txpre",
                                .offset = PORT_10G_6_CLIENT_USER_CTL_OFFSET + TXPRECURSOR_OFFSET,
                                .value = 0.0, /* dB */
                                .encoded_value = 0x3
                            },
                        }
                    }
                }
            },
            .counter_ctl = {
                .enable = CCL_TRUE,
                .width = 40,
                .ctl_offset = PORT_10G_6_CLIENT_USER_CTL_OFFSET + COUNTER_CONTROL_OFFSET,
                .ctl = {
                    .tx_pkt_rst_mask = COUNTER_CTL_TX_PACKET_CNT_RST_MASK,
                    .rx_pkt_rst_mask = COUNTER_CTL_RX_PACKET_CNT_RST_MASK,     
                    .rx_bad_pkt_rst_mask = COUNTER_CTL_RX_BAD_PACKET_CNT_RST_MASK, 
                    .freeze_counters_mask = COUNTER_CTL_FREEZE_COUNTERS_MASK,
                    .rollover_mask = COUNTER_CTL_ROLLOVER_MASK       

                },
                .counter = {
                    {
                        .name = "tx_packets_lsb",
                        .offset = PORT_10G_6_CLIENT_USER_CTL_OFFSET + TX_PKT_CNT_LSB_OFFSET
                    },
                    {
                        .name = "tx_packets_msb",
                        .offset = PORT_10G_6_CLIENT_USER_CTL_OFFSET + TX_PKT_CNT_MSB_OFFSET
                    },
                    {
                        .name = "rx_packets_lsb",
                        .offset = PORT_10G_6_CLIENT_USER_CTL_OFFSET + RX_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_packets_msb",
                        .offset = PORT_10G_6_CLIENT_USER_CTL_OFFSET + RX_PKT_CNT_MSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_lsb",
                        .offset = PORT_10G_6_CLIENT_USER_CTL_OFFSET + RX_BAD_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_msb",
                        .offset = PORT_10G_6_CLIENT_USER_CTL_OFFSET + RX_BAD_PKT_CNT_MSB_OFFSET

                    },
                }
            }
        }
    },
    {
        /* J37D*/
        .type_name = "hses10g", .sig = eDEVBRD_10G_MAC_19_SIG,
        .spec.hses10g = {
            .devname = "fpga1",
            .portid = LOGPORT_10G_7,
            .offset = PORT_10G_7_CLIENT_OFFSET,
            .name = PORT_10G_7_CLIENT_NAME,
            .iftype = eDEVBRD_IFTYPE_R,
            .calib_ctl = {
                .enable = CCL_TRUE,
                .channel_num = 1,
                .channel_calib_params = {
                    {
                        .calib = {
                            {
                                .name = "txdiffswing",
                                .offset = PORT_10G_7_CLIENT_USER_CTL_OFFSET + TXDIFFCTRL_OFFSET,
                                .value = 0, 
                                .encoded_value = 0x14
                            },
                            {
                                .name = "txpost",
                                .offset = PORT_10G_7_CLIENT_USER_CTL_OFFSET + TXPOSTCURSOR_OFFSET,
                                .value = 0, /* dB */
                                .encoded_value = 0xb
                            },
                            {
                                .name = "txpre",
                                .offset = PORT_10G_7_CLIENT_USER_CTL_OFFSET + TXPRECURSOR_OFFSET,
                                .value = 0.0, /* dB */
                                .encoded_value = 0
                            },
                        }
                    }
                }
            },
            .counter_ctl = {
                .enable = CCL_TRUE,
                .width = 40,
                .ctl_offset = PORT_10G_7_CLIENT_USER_CTL_OFFSET + COUNTER_CONTROL_OFFSET,
                .ctl = {
                    .tx_pkt_rst_mask = COUNTER_CTL_TX_PACKET_CNT_RST_MASK,
                    .rx_pkt_rst_mask = COUNTER_CTL_RX_PACKET_CNT_RST_MASK,     
                    .rx_bad_pkt_rst_mask = COUNTER_CTL_RX_BAD_PACKET_CNT_RST_MASK, 
                    .freeze_counters_mask = COUNTER_CTL_FREEZE_COUNTERS_MASK,
                    .rollover_mask = COUNTER_CTL_ROLLOVER_MASK       

                },
                .counter = {
                    {
                        .name = "tx_packets_lsb",
                        .offset = PORT_10G_7_CLIENT_USER_CTL_OFFSET + TX_PKT_CNT_LSB_OFFSET
                    },
                    {
                        .name = "tx_packets_msb",
                        .offset = PORT_10G_7_CLIENT_USER_CTL_OFFSET + TX_PKT_CNT_MSB_OFFSET
                    },
                    {
                        .name = "rx_packets_lsb",
                        .offset = PORT_10G_7_CLIENT_USER_CTL_OFFSET + RX_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_packets_msb",
                        .offset = PORT_10G_7_CLIENT_USER_CTL_OFFSET + RX_PKT_CNT_MSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_lsb",
                        .offset = PORT_10G_7_CLIENT_USER_CTL_OFFSET + RX_BAD_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_msb",
                        .offset = PORT_10G_7_CLIENT_USER_CTL_OFFSET + RX_BAD_PKT_CNT_MSB_OFFSET

                    },
                }
            }
        }
    },
    {
        /* J37E*/
        .type_name = "hses10g", .sig = eDEVBRD_10G_MAC_20_SIG,
        .spec.hses10g = {
            .devname = "fpga1",
            .portid = LOGPORT_10G_8,
            .offset = PORT_10G_8_CLIENT_OFFSET,
            .name = PORT_10G_8_CLIENT_NAME,
            .iftype = eDEVBRD_IFTYPE_R,
            .calib_ctl = {
                .enable = CCL_TRUE,
                .channel_num = 1,
                .channel_calib_params = {
                    {
                        .calib = {
                            {
                                .name = "txdiffswing",
                                .offset = PORT_10G_8_CLIENT_USER_CTL_OFFSET + TXDIFFCTRL_OFFSET,
                                .value = 0, 
                                .encoded_value = 0x13
                            },
                            {
                                .name = "txpost",
                                .offset = PORT_10G_8_CLIENT_USER_CTL_OFFSET + TXPOSTCURSOR_OFFSET,
                                .value = 0, /* dB */
                                .encoded_value = 0x7
                            },
                            {
                                .name = "txpre",
                                .offset = PORT_10G_8_CLIENT_USER_CTL_OFFSET + TXPRECURSOR_OFFSET,
                                .value = 0.0, /* dB */
                                .encoded_value = 0x4
                            },
                        }
                    }
                }
            },
            .counter_ctl = {
                .enable = CCL_TRUE,
                .width = 40,
                .ctl_offset = PORT_10G_8_CLIENT_USER_CTL_OFFSET + COUNTER_CONTROL_OFFSET,
                .ctl = {
                    .tx_pkt_rst_mask = COUNTER_CTL_TX_PACKET_CNT_RST_MASK,
                    .rx_pkt_rst_mask = COUNTER_CTL_RX_PACKET_CNT_RST_MASK,     
                    .rx_bad_pkt_rst_mask = COUNTER_CTL_RX_BAD_PACKET_CNT_RST_MASK, 
                    .freeze_counters_mask = COUNTER_CTL_FREEZE_COUNTERS_MASK,
                    .rollover_mask = COUNTER_CTL_ROLLOVER_MASK       

                },
                .counter = {
                    {
                        .name = "tx_packets_lsb",
                        .offset = PORT_10G_8_CLIENT_USER_CTL_OFFSET + TX_PKT_CNT_LSB_OFFSET
                    },
                    {
                        .name = "tx_packets_msb",
                        .offset = PORT_10G_8_CLIENT_USER_CTL_OFFSET + TX_PKT_CNT_MSB_OFFSET
                    },
                    {
                        .name = "rx_packets_lsb",
                        .offset = PORT_10G_8_CLIENT_USER_CTL_OFFSET + RX_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_packets_msb",
                        .offset = PORT_10G_8_CLIENT_USER_CTL_OFFSET + RX_PKT_CNT_MSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_lsb",
                        .offset = PORT_10G_8_CLIENT_USER_CTL_OFFSET + RX_BAD_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_msb",
                        .offset = PORT_10G_8_CLIENT_USER_CTL_OFFSET + RX_BAD_PKT_CNT_MSB_OFFSET

                    },
                }
            }
        }
    },
    {
        /* J37F*/
        .type_name = "hses10g", .sig = eDEVBRD_10G_MAC_21_SIG,
        .spec.hses10g = {
            .devname = "fpga1",
            .portid = LOGPORT_10G_9,
            .offset = PORT_10G_9_CLIENT_OFFSET,
            .name = PORT_10G_9_CLIENT_NAME,
            .iftype = eDEVBRD_IFTYPE_R,
            .calib_ctl = {
                .enable = CCL_TRUE,
                .channel_num = 1,
                .channel_calib_params = {
                    {
                        .calib = {
                            {
                                .name = "txdiffswing",
                                .offset = PORT_10G_9_CLIENT_USER_CTL_OFFSET + TXDIFFCTRL_OFFSET,
                                .value = 0, 
                                .encoded_value = 0x9
                            },
                            {
                                .name = "txpost",
                                .offset = PORT_10G_9_CLIENT_USER_CTL_OFFSET + TXPOSTCURSOR_OFFSET,
                                .value = 0, /* dB */
                                .encoded_value = 0xb
                            },
                            {
                                .name = "txpre",
                                .offset = PORT_10G_9_CLIENT_USER_CTL_OFFSET + TXPRECURSOR_OFFSET,
                                .value = 0.0, /* dB */
                                .encoded_value = 0x2
                            },
                        }
                    }
                }
            },
            .counter_ctl = {
                .enable = CCL_TRUE,
                .width = 40,
                .ctl_offset = PORT_10G_9_CLIENT_USER_CTL_OFFSET + COUNTER_CONTROL_OFFSET,
                .ctl = {
                    .tx_pkt_rst_mask = COUNTER_CTL_TX_PACKET_CNT_RST_MASK,
                    .rx_pkt_rst_mask = COUNTER_CTL_RX_PACKET_CNT_RST_MASK,     
                    .rx_bad_pkt_rst_mask = COUNTER_CTL_RX_BAD_PACKET_CNT_RST_MASK, 
                    .freeze_counters_mask = COUNTER_CTL_FREEZE_COUNTERS_MASK,
                    .rollover_mask = COUNTER_CTL_ROLLOVER_MASK       

                },
                .counter = {
                    {
                        .name = "tx_packets_lsb",
                        .offset = PORT_10G_9_CLIENT_USER_CTL_OFFSET + TX_PKT_CNT_LSB_OFFSET
                    },
                    {
                        .name = "tx_packets_msb",
                        .offset = PORT_10G_9_CLIENT_USER_CTL_OFFSET + TX_PKT_CNT_MSB_OFFSET
                    },
                    {
                        .name = "rx_packets_lsb",
                        .offset = PORT_10G_9_CLIENT_USER_CTL_OFFSET + RX_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_packets_msb",
                        .offset = PORT_10G_9_CLIENT_USER_CTL_OFFSET + RX_PKT_CNT_MSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_lsb",
                        .offset = PORT_10G_9_CLIENT_USER_CTL_OFFSET + RX_BAD_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_msb",
                        .offset = PORT_10G_9_CLIENT_USER_CTL_OFFSET + RX_BAD_PKT_CNT_MSB_OFFSET

                    },
                }
            }
        }
    },
    {
        /* J37G*/
        .type_name = "hses10g", .sig = eDEVBRD_10G_MAC_22_SIG,
        .spec.hses10g = {
            .devname = "fpga1",
            .portid = LOGPORT_10G_10,
            .offset = PORT_10G_10_CLIENT_OFFSET,
            .name = PORT_10G_10_CLIENT_NAME,
            .iftype = eDEVBRD_IFTYPE_R,
            .calib_ctl = {
                .enable = CCL_TRUE,
                .channel_num = 1,
                .channel_calib_params = {
                    {
                        .calib = {
                            {
                                .name = "txdiffswing",
                                .offset = PORT_10G_10_CLIENT_USER_CTL_OFFSET + TXDIFFCTRL_OFFSET,
                                .value = 0, 
                                .encoded_value = 0x16
                            },
                            {
                                .name = "txpost",
                                .offset = PORT_10G_10_CLIENT_USER_CTL_OFFSET + TXPOSTCURSOR_OFFSET,
                                .value = 0, /* dB */
                                .encoded_value = 0x1
                            },
                            {
                                .name = "txpre",
                                .offset = PORT_10G_10_CLIENT_USER_CTL_OFFSET + TXPRECURSOR_OFFSET,
                                .value = 0.0, /* dB */
                                .encoded_value = 0x1
                            },
                        }
                    }
                }
            },
            .counter_ctl = {
                .enable = CCL_TRUE,
                .width = 40,
                .ctl_offset = PORT_10G_10_CLIENT_USER_CTL_OFFSET + COUNTER_CONTROL_OFFSET,
                .ctl = {
                    .tx_pkt_rst_mask = COUNTER_CTL_TX_PACKET_CNT_RST_MASK,
                    .rx_pkt_rst_mask = COUNTER_CTL_RX_PACKET_CNT_RST_MASK,     
                    .rx_bad_pkt_rst_mask = COUNTER_CTL_RX_BAD_PACKET_CNT_RST_MASK, 
                    .freeze_counters_mask = COUNTER_CTL_FREEZE_COUNTERS_MASK,
                    .rollover_mask = COUNTER_CTL_ROLLOVER_MASK       

                },
                .counter = {
                    {
                        .name = "tx_packets_lsb",
                        .offset = PORT_10G_10_CLIENT_USER_CTL_OFFSET + TX_PKT_CNT_LSB_OFFSET
                    },
                    {
                        .name = "tx_packets_msb",
                        .offset = PORT_10G_10_CLIENT_USER_CTL_OFFSET + TX_PKT_CNT_MSB_OFFSET
                    },
                    {
                        .name = "rx_packets_lsb",
                        .offset = PORT_10G_10_CLIENT_USER_CTL_OFFSET + RX_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_packets_msb",
                        .offset = PORT_10G_10_CLIENT_USER_CTL_OFFSET + RX_PKT_CNT_MSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_lsb",
                        .offset = PORT_10G_10_CLIENT_USER_CTL_OFFSET + RX_BAD_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_msb",
                        .offset = PORT_10G_10_CLIENT_USER_CTL_OFFSET + RX_BAD_PKT_CNT_MSB_OFFSET

                    },
                }
            }
        }
    },
    {
        /* J37H*/
        .type_name = "hses10g", .sig = eDEVBRD_10G_MAC_23_SIG,
        .spec.hses10g = {
            .devname = "fpga1",
            .portid = LOGPORT_10G_11,
            .offset = PORT_10G_11_CLIENT_OFFSET,
            .name = PORT_10G_11_CLIENT_NAME,
            .iftype = eDEVBRD_IFTYPE_R,
            .calib_ctl = {
                .enable = CCL_TRUE,
                .channel_num = 1,
                .channel_calib_params = {
                    {
                        .calib = {
                            {
                                .name = "txdiffswing",
                                .offset = PORT_10G_11_CLIENT_USER_CTL_OFFSET + TXDIFFCTRL_OFFSET,
                                .value = 0, 
                                .encoded_value = 0x13
                            },
                            {
                                .name = "txpost",
                                .offset = PORT_10G_11_CLIENT_USER_CTL_OFFSET + TXPOSTCURSOR_OFFSET,
                                .value = 0, /* dB */
                                .encoded_value = 0x1
                            },
                            {
                                .name = "txpre",
                                .offset = PORT_10G_11_CLIENT_USER_CTL_OFFSET + TXPRECURSOR_OFFSET,
                                .value = 0.0, /* dB */
                                .encoded_value = 0x2
                            },
                        }
                    }
                }
            },
            .counter_ctl = {
                .enable = CCL_TRUE,
                .width = 40,
                .ctl_offset = PORT_10G_11_CLIENT_USER_CTL_OFFSET + COUNTER_CONTROL_OFFSET,
                .ctl = {
                    .tx_pkt_rst_mask = COUNTER_CTL_TX_PACKET_CNT_RST_MASK,
                    .rx_pkt_rst_mask = COUNTER_CTL_RX_PACKET_CNT_RST_MASK,     
                    .rx_bad_pkt_rst_mask = COUNTER_CTL_RX_BAD_PACKET_CNT_RST_MASK, 
                    .freeze_counters_mask = COUNTER_CTL_FREEZE_COUNTERS_MASK,
                    .rollover_mask = COUNTER_CTL_ROLLOVER_MASK       

                },
                .counter = {
                    {
                        .name = "tx_packets_lsb",
                        .offset = PORT_10G_11_CLIENT_USER_CTL_OFFSET + TX_PKT_CNT_LSB_OFFSET
                    },
                    {
                        .name = "tx_packets_msb",
                        .offset = PORT_10G_11_CLIENT_USER_CTL_OFFSET + TX_PKT_CNT_MSB_OFFSET
                    },
                    {
                        .name = "rx_packets_lsb",
                        .offset = PORT_10G_11_CLIENT_USER_CTL_OFFSET + RX_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_packets_msb",
                        .offset = PORT_10G_11_CLIENT_USER_CTL_OFFSET + RX_PKT_CNT_MSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_lsb",
                        .offset = PORT_10G_11_CLIENT_USER_CTL_OFFSET + RX_BAD_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_msb",
                        .offset = PORT_10G_11_CLIENT_USER_CTL_OFFSET + RX_BAD_PKT_CNT_MSB_OFFSET

                    },
                }
            }
        }
    },
    {
        .type_name = "hses10g", .sig = eDEVBRD_10G_MAC_24_SIG,
        .spec.hses10g = {
            .devname = "fpga1",
            .portid = LOGPORT_KR_0,
            .offset = PORT_KR_COMX0_0_OFFSET,
            .name = PORT_KR_COMX0_0_NAME,
            .iftype = eDEVBRD_IFTYPE_KR,
            .calib_ctl = {
                .enable = CCL_FALSE,
            },
            .autoneg_ctl = {
                .enable = CCL_TRUE,
                .autoneg = {
                    {
                        .name = "autoneg",
                        .offset = PORT_KR_COMX0_0_USER_CTL_OFFSET + AUTONEG_OFFSET,
                        .value = 0
                    }
                }
            },
            .counter_ctl = {
                .enable = CCL_TRUE,
                .width = 40,
                .ctl_offset = PORT_KR_COMX0_0_USER_CTL_OFFSET + COUNTER_CONTROL_OFFSET,
                .ctl = {
                    .tx_pkt_rst_mask = COUNTER_CTL_TX_PACKET_CNT_RST_MASK,
                    .rx_pkt_rst_mask = COUNTER_CTL_RX_PACKET_CNT_RST_MASK,     
                    .rx_bad_pkt_rst_mask = COUNTER_CTL_RX_BAD_PACKET_CNT_RST_MASK, 
                    .freeze_counters_mask = COUNTER_CTL_FREEZE_COUNTERS_MASK,
                    .rollover_mask = COUNTER_CTL_ROLLOVER_MASK       

                },
                .counter = {
                    {
                        .name = "tx_packets_lsb",
                        .offset = PORT_KR_COMX0_0_USER_CTL_OFFSET + TX_PKT_CNT_LSB_OFFSET
                    },
                    {
                        .name = "tx_packets_msb",
                        .offset = PORT_KR_COMX0_0_USER_CTL_OFFSET + TX_PKT_CNT_MSB_OFFSET
                    },
                    {
                        .name = "rx_packets_lsb",
                        .offset = PORT_KR_COMX0_0_USER_CTL_OFFSET + RX_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_packets_msb",
                        .offset = PORT_KR_COMX0_0_USER_CTL_OFFSET + RX_PKT_CNT_MSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_lsb",
                        .offset = PORT_KR_COMX0_0_USER_CTL_OFFSET + RX_BAD_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_msb",
                        .offset = PORT_KR_COMX0_0_USER_CTL_OFFSET + RX_BAD_PKT_CNT_MSB_OFFSET

                    },
                }
            }
        }
    },
    {
        .type_name = "hses10g", .sig = eDEVBRD_10G_MAC_25_SIG,
        .spec.hses10g = {
            .devname = "fpga1",
            .portid = LOGPORT_KR_1,
            .offset = PORT_KR_COMX0_1_OFFSET,
            .name = PORT_KR_COMX0_1_NAME,
            .iftype = eDEVBRD_IFTYPE_KR,
            .calib_ctl = {
                .enable = CCL_FALSE,
            },
            .autoneg_ctl = {
                .enable = CCL_TRUE,
                .autoneg = {
                    {
                        .name = "autoneg",
                        .offset = PORT_KR_COMX0_1_USER_CTL_OFFSET + AUTONEG_OFFSET,
                        .value = 0
                    }
                }
            },
            .counter_ctl = {
                .enable = CCL_TRUE,
                .width = 40,
                .ctl_offset = PORT_KR_COMX0_1_USER_CTL_OFFSET + COUNTER_CONTROL_OFFSET,
                .ctl = {
                    .tx_pkt_rst_mask = COUNTER_CTL_TX_PACKET_CNT_RST_MASK,
                    .rx_pkt_rst_mask = COUNTER_CTL_RX_PACKET_CNT_RST_MASK,     
                    .rx_bad_pkt_rst_mask = COUNTER_CTL_RX_BAD_PACKET_CNT_RST_MASK, 
                    .freeze_counters_mask = COUNTER_CTL_FREEZE_COUNTERS_MASK,
                    .rollover_mask = COUNTER_CTL_ROLLOVER_MASK       

                },
                .counter = {
                    {
                        .name = "tx_packets_lsb",
                        .offset = PORT_KR_COMX0_1_USER_CTL_OFFSET + TX_PKT_CNT_LSB_OFFSET
                    },
                    {
                        .name = "tx_packets_msb",
                        .offset = PORT_KR_COMX0_1_USER_CTL_OFFSET + TX_PKT_CNT_MSB_OFFSET
                    },
                    {
                        .name = "rx_packets_lsb",
                        .offset = PORT_KR_COMX0_1_USER_CTL_OFFSET + RX_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_packets_msb",
                        .offset = PORT_KR_COMX0_1_USER_CTL_OFFSET + RX_PKT_CNT_MSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_lsb",
                        .offset = PORT_KR_COMX0_1_USER_CTL_OFFSET + RX_BAD_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_msb",
                        .offset = PORT_KR_COMX0_1_USER_CTL_OFFSET + RX_BAD_PKT_CNT_MSB_OFFSET

                    },
                }
            }
        }
    },
    {
        .type_name = "hses10g", .sig = eDEVBRD_10G_MAC_26_SIG,
        .spec.hses10g = {
            .devname = "fpga1",
            .portid = LOGPORT_KR_2,
            .offset = PORT_KR_COMX0_2_OFFSET,
            .name = PORT_KR_COMX0_2_NAME,
            .iftype = eDEVBRD_IFTYPE_KR,
            .calib_ctl = {
                .enable = CCL_FALSE,
            },
            .autoneg_ctl = {
                .enable = CCL_TRUE,
                .autoneg = {
                    {
                        .name = "autoneg",
                        .offset = PORT_KR_COMX0_2_USER_CTL_OFFSET + AUTONEG_OFFSET,
                        .value = 0
                    }
                }
            },
            .counter_ctl = {
                .enable = CCL_TRUE,
                .width = 40,
                .ctl_offset = PORT_KR_COMX0_2_USER_CTL_OFFSET + COUNTER_CONTROL_OFFSET,
                .ctl = {
                    .tx_pkt_rst_mask = COUNTER_CTL_TX_PACKET_CNT_RST_MASK,
                    .rx_pkt_rst_mask = COUNTER_CTL_RX_PACKET_CNT_RST_MASK,     
                    .rx_bad_pkt_rst_mask = COUNTER_CTL_RX_BAD_PACKET_CNT_RST_MASK, 
                    .freeze_counters_mask = COUNTER_CTL_FREEZE_COUNTERS_MASK,
                    .rollover_mask = COUNTER_CTL_ROLLOVER_MASK       

                },
                .counter = {
                    {
                        .name = "tx_packets_lsb",
                        .offset = PORT_KR_COMX0_2_USER_CTL_OFFSET + TX_PKT_CNT_LSB_OFFSET
                    },
                    {
                        .name = "tx_packets_msb",
                        .offset = PORT_KR_COMX0_2_USER_CTL_OFFSET + TX_PKT_CNT_MSB_OFFSET
                    },
                    {
                        .name = "rx_packets_lsb",
                        .offset = PORT_KR_COMX0_2_USER_CTL_OFFSET + RX_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_packets_msb",
                        .offset = PORT_KR_COMX0_2_USER_CTL_OFFSET + RX_PKT_CNT_MSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_lsb",
                        .offset = PORT_KR_COMX0_2_USER_CTL_OFFSET + RX_BAD_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_msb",
                        .offset = PORT_KR_COMX0_2_USER_CTL_OFFSET + RX_BAD_PKT_CNT_MSB_OFFSET

                    },
                }
            }
        }
    },
    {
        .type_name = "hses10g", .sig = eDEVBRD_10G_MAC_27_SIG,
        .spec.hses10g = {
            .devname = "fpga1",
            .portid = LOGPORT_KR_3,
            .offset = PORT_KR_COMX0_3_OFFSET,
            .name = PORT_KR_COMX0_3_NAME,
            .iftype = eDEVBRD_IFTYPE_KR,
            .calib_ctl = {
                .enable = CCL_FALSE,
            },
            .autoneg_ctl = {
                .enable = CCL_TRUE,
                .autoneg = {
                    {
                        .name = "autoneg",
                        .offset = PORT_KR_COMX0_3_USER_CTL_OFFSET + AUTONEG_OFFSET,
                        .value = 0
                    }
                }
            },
            .counter_ctl = {
                .enable = CCL_TRUE,
                .width = 40,
                .ctl_offset = PORT_KR_COMX0_3_USER_CTL_OFFSET + COUNTER_CONTROL_OFFSET,
                .ctl = {
                    .tx_pkt_rst_mask = COUNTER_CTL_TX_PACKET_CNT_RST_MASK,
                    .rx_pkt_rst_mask = COUNTER_CTL_RX_PACKET_CNT_RST_MASK,     
                    .rx_bad_pkt_rst_mask = COUNTER_CTL_RX_BAD_PACKET_CNT_RST_MASK, 
                    .freeze_counters_mask = COUNTER_CTL_FREEZE_COUNTERS_MASK,
                    .rollover_mask = COUNTER_CTL_ROLLOVER_MASK       

                },
                .counter = {
                    {
                        .name = "tx_packets_lsb",
                        .offset = PORT_KR_COMX0_3_USER_CTL_OFFSET + TX_PKT_CNT_LSB_OFFSET
                    },
                    {
                        .name = "tx_packets_msb",
                        .offset = PORT_KR_COMX0_3_USER_CTL_OFFSET + TX_PKT_CNT_MSB_OFFSET
                    },
                    {
                        .name = "rx_packets_lsb",
                        .offset = PORT_KR_COMX0_3_USER_CTL_OFFSET + RX_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_packets_msb",
                        .offset = PORT_KR_COMX0_3_USER_CTL_OFFSET + RX_PKT_CNT_MSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_lsb",
                        .offset = PORT_KR_COMX0_3_USER_CTL_OFFSET + RX_BAD_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_msb",
                        .offset = PORT_KR_COMX0_3_USER_CTL_OFFSET + RX_BAD_PKT_CNT_MSB_OFFSET

                    },
                }
            }
        }
    },
    {
        .type_name = "hses10g", .sig = eDEVBRD_10G_MAC_28_SIG,
        .spec.hses10g = {
            .devname = "fpga1",
            .portid = LOGPORT_KR_4,
            .offset = PORT_KR_COMX1_0_OFFSET,
            .name = PORT_KR_COMX1_0_NAME,
            .iftype = eDEVBRD_IFTYPE_KR,
            .calib_ctl = {
                .enable = CCL_FALSE,
            },
            .autoneg_ctl = {
                .enable = CCL_TRUE,
                .autoneg = {
                    {
                        .name = "autoneg",
                        .offset = PORT_KR_COMX1_0_USER_CTL_OFFSET + AUTONEG_OFFSET,
                        .value = 0
                    }
                }
            },
            .counter_ctl = {
                .enable = CCL_TRUE,
                .width = 40,
                .ctl_offset = PORT_KR_COMX1_0_USER_CTL_OFFSET + COUNTER_CONTROL_OFFSET,
                .ctl = {
                    .tx_pkt_rst_mask = COUNTER_CTL_TX_PACKET_CNT_RST_MASK,
                    .rx_pkt_rst_mask = COUNTER_CTL_RX_PACKET_CNT_RST_MASK,     
                    .rx_bad_pkt_rst_mask = COUNTER_CTL_RX_BAD_PACKET_CNT_RST_MASK, 
                    .freeze_counters_mask = COUNTER_CTL_FREEZE_COUNTERS_MASK,
                    .rollover_mask = COUNTER_CTL_ROLLOVER_MASK       

                },
                .counter = {
                    {
                        .name = "tx_packets_lsb",
                        .offset = PORT_KR_COMX1_0_USER_CTL_OFFSET + TX_PKT_CNT_LSB_OFFSET
                    },
                    {
                        .name = "tx_packets_msb",
                        .offset = PORT_KR_COMX1_0_USER_CTL_OFFSET + TX_PKT_CNT_MSB_OFFSET
                    },
                    {
                        .name = "rx_packets_lsb",
                        .offset = PORT_KR_COMX1_0_USER_CTL_OFFSET + RX_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_packets_msb",
                        .offset = PORT_KR_COMX1_0_USER_CTL_OFFSET + RX_PKT_CNT_MSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_lsb",
                        .offset = PORT_KR_COMX1_0_USER_CTL_OFFSET + RX_BAD_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_msb",
                        .offset = PORT_KR_COMX1_0_USER_CTL_OFFSET + RX_BAD_PKT_CNT_MSB_OFFSET

                    },
                }
            }
        }
    },
    {
        .type_name = "hses10g", .sig = eDEVBRD_10G_MAC_29_SIG,
        .spec.hses10g = {
            .devname = "fpga1",
            .portid = LOGPORT_KR_5,
            .offset = PORT_KR_COMX1_1_OFFSET,
            .name = PORT_KR_COMX1_1_NAME,
            .iftype = eDEVBRD_IFTYPE_KR,
            .calib_ctl = {
                .enable = CCL_FALSE,
            },
            .autoneg_ctl = {
                .enable = CCL_TRUE,
                .autoneg = {
                    {
                        .name = "autoneg",
                        .offset = PORT_KR_COMX1_1_USER_CTL_OFFSET + AUTONEG_OFFSET,
                        .value = 0
                    }
                }
            },
            .counter_ctl = {
                .enable = CCL_TRUE,
                .width = 40,
                .ctl_offset = PORT_KR_COMX1_1_USER_CTL_OFFSET + COUNTER_CONTROL_OFFSET,
                .ctl = {
                    .tx_pkt_rst_mask = COUNTER_CTL_TX_PACKET_CNT_RST_MASK,
                    .rx_pkt_rst_mask = COUNTER_CTL_RX_PACKET_CNT_RST_MASK,     
                    .rx_bad_pkt_rst_mask = COUNTER_CTL_RX_BAD_PACKET_CNT_RST_MASK, 
                    .freeze_counters_mask = COUNTER_CTL_FREEZE_COUNTERS_MASK,
                    .rollover_mask = COUNTER_CTL_ROLLOVER_MASK       

                },
                .counter = {
                    {
                        .name = "tx_packets_lsb",
                        .offset = PORT_KR_COMX1_1_USER_CTL_OFFSET + TX_PKT_CNT_LSB_OFFSET
                    },
                    {
                        .name = "tx_packets_msb",
                        .offset = PORT_KR_COMX1_1_USER_CTL_OFFSET + TX_PKT_CNT_MSB_OFFSET
                    },
                    {
                        .name = "rx_packets_lsb",
                        .offset = PORT_KR_COMX1_1_USER_CTL_OFFSET + RX_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_packets_msb",
                        .offset = PORT_KR_COMX1_1_USER_CTL_OFFSET + RX_PKT_CNT_MSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_lsb",
                        .offset = PORT_KR_COMX1_1_USER_CTL_OFFSET + RX_BAD_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_msb",
                        .offset = PORT_KR_COMX1_1_USER_CTL_OFFSET + RX_BAD_PKT_CNT_MSB_OFFSET

                    },
                }
            }
        }
    },
    {
        .type_name = "hses10g", .sig = eDEVBRD_10G_MAC_30_SIG,
        .spec.hses10g = {
            .devname = "fpga1",
            .portid = LOGPORT_KR_6,
            .offset = PORT_KR_COMX1_2_OFFSET,
            .name = PORT_KR_COMX1_2_NAME,
            .iftype = eDEVBRD_IFTYPE_KR,
            .calib_ctl = {
                .enable = CCL_FALSE,
            },
            .autoneg_ctl = {
                .enable = CCL_TRUE,
                .autoneg = {
                    {
                        .name = "autoneg",
                        .offset = PORT_KR_COMX1_2_USER_CTL_OFFSET + AUTONEG_OFFSET,
                        .value = 0
                    }
                }
            },
            .counter_ctl = {
                .enable = CCL_TRUE,
                .width = 40,
                .ctl_offset = PORT_KR_COMX1_2_USER_CTL_OFFSET + COUNTER_CONTROL_OFFSET,
                .ctl = {
                    .tx_pkt_rst_mask = COUNTER_CTL_TX_PACKET_CNT_RST_MASK,
                    .rx_pkt_rst_mask = COUNTER_CTL_RX_PACKET_CNT_RST_MASK,     
                    .rx_bad_pkt_rst_mask = COUNTER_CTL_RX_BAD_PACKET_CNT_RST_MASK, 
                    .freeze_counters_mask = COUNTER_CTL_FREEZE_COUNTERS_MASK,
                    .rollover_mask = COUNTER_CTL_ROLLOVER_MASK       

                },
                .counter = {
                    {
                        .name = "tx_packets_lsb",
                        .offset = PORT_KR_COMX1_2_USER_CTL_OFFSET + TX_PKT_CNT_LSB_OFFSET
                    },
                    {
                        .name = "tx_packets_msb",
                        .offset = PORT_KR_COMX1_2_USER_CTL_OFFSET + TX_PKT_CNT_MSB_OFFSET
                    },
                    {
                        .name = "rx_packets_lsb",
                        .offset = PORT_KR_COMX1_2_USER_CTL_OFFSET + RX_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_packets_msb",
                        .offset = PORT_KR_COMX1_2_USER_CTL_OFFSET + RX_PKT_CNT_MSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_lsb",
                        .offset = PORT_KR_COMX1_2_USER_CTL_OFFSET + RX_BAD_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_msb",
                        .offset = PORT_KR_COMX1_2_USER_CTL_OFFSET + RX_BAD_PKT_CNT_MSB_OFFSET

                    },
                }
            }
        }
    },
    {
        .type_name = "hses10g", .sig = eDEVBRD_10G_MAC_31_SIG,
        .spec.hses10g = {
            .devname = "fpga1",
            .portid = LOGPORT_KR_7,
            .offset = PORT_KR_COMX1_3_OFFSET,
            .name = PORT_KR_COMX1_3_NAME,
            .iftype = eDEVBRD_IFTYPE_KR,
            .calib_ctl = {
                .enable = CCL_FALSE,
            },
            .autoneg_ctl = {
                .enable = CCL_TRUE,
                .autoneg = {
                    {
                        .name = "autoneg",
                        .offset = PORT_KR_COMX1_3_USER_CTL_OFFSET + AUTONEG_OFFSET,
                        .value = 0
                    }
                }
            },
            .counter_ctl = {
                .enable = CCL_TRUE,
                .width = 40,
                .ctl_offset = PORT_KR_COMX1_3_USER_CTL_OFFSET + COUNTER_CONTROL_OFFSET,
                .ctl = {
                    .tx_pkt_rst_mask = COUNTER_CTL_TX_PACKET_CNT_RST_MASK,
                    .rx_pkt_rst_mask = COUNTER_CTL_RX_PACKET_CNT_RST_MASK,     
                    .rx_bad_pkt_rst_mask = COUNTER_CTL_RX_BAD_PACKET_CNT_RST_MASK, 
                    .freeze_counters_mask = COUNTER_CTL_FREEZE_COUNTERS_MASK,
                    .rollover_mask = COUNTER_CTL_ROLLOVER_MASK       

                },
                .counter = {
                    {
                        .name = "tx_packets_lsb",
                        .offset = PORT_KR_COMX1_3_USER_CTL_OFFSET + TX_PKT_CNT_LSB_OFFSET
                    },
                    {
                        .name = "tx_packets_msb",
                        .offset = PORT_KR_COMX1_3_USER_CTL_OFFSET + TX_PKT_CNT_MSB_OFFSET
                    },
                    {
                        .name = "rx_packets_lsb",
                        .offset = PORT_KR_COMX1_3_USER_CTL_OFFSET + RX_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_packets_msb",
                        .offset = PORT_KR_COMX1_3_USER_CTL_OFFSET + RX_PKT_CNT_MSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_lsb",
                        .offset = PORT_KR_COMX1_3_USER_CTL_OFFSET + RX_BAD_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_msb",
                        .offset = PORT_KR_COMX1_3_USER_CTL_OFFSET + RX_BAD_PKT_CNT_MSB_OFFSET

                    },
                }
            }
        }
    },
    {
        .type_name = "hses10g", .sig = eDEVBRD_10G_MAC_32_SIG,
        .spec.hses10g = {
            .devname = "fpga1",
            .portid = LOGPORT_KR_8,
            .offset = PORT_KR_COMX2_0_OFFSET,
            .name = PORT_KR_COMX2_0_NAME,
            .iftype = eDEVBRD_IFTYPE_KR,
            .calib_ctl = {
                .enable = CCL_FALSE,
            },
            .autoneg_ctl = {
                .enable = CCL_TRUE,
                .autoneg = {
                    {
                        .name = "autoneg",
                        .offset = PORT_KR_COMX2_0_USER_CTL_OFFSET + AUTONEG_OFFSET,
                        .value = 0
                    }
                }
            },
            .counter_ctl = {
                .enable = CCL_TRUE,
                .width = 40,
                .ctl_offset = PORT_KR_COMX2_0_USER_CTL_OFFSET + COUNTER_CONTROL_OFFSET,
                .ctl = {
                    .tx_pkt_rst_mask = COUNTER_CTL_TX_PACKET_CNT_RST_MASK,
                    .rx_pkt_rst_mask = COUNTER_CTL_RX_PACKET_CNT_RST_MASK,     
                    .rx_bad_pkt_rst_mask = COUNTER_CTL_RX_BAD_PACKET_CNT_RST_MASK, 
                    .freeze_counters_mask = COUNTER_CTL_FREEZE_COUNTERS_MASK,
                    .rollover_mask = COUNTER_CTL_ROLLOVER_MASK       

                },
                .counter = {
                    {
                        .name = "tx_packets_lsb",
                        .offset = PORT_KR_COMX2_0_USER_CTL_OFFSET + TX_PKT_CNT_LSB_OFFSET
                    },
                    {
                        .name = "tx_packets_msb",
                        .offset = PORT_KR_COMX2_0_USER_CTL_OFFSET + TX_PKT_CNT_MSB_OFFSET
                    },
                    {
                        .name = "rx_packets_lsb",
                        .offset = PORT_KR_COMX2_0_USER_CTL_OFFSET + RX_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_packets_msb",
                        .offset = PORT_KR_COMX2_0_USER_CTL_OFFSET + RX_PKT_CNT_MSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_lsb",
                        .offset = PORT_KR_COMX2_0_USER_CTL_OFFSET + RX_BAD_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_msb",
                        .offset = PORT_KR_COMX2_0_USER_CTL_OFFSET + RX_BAD_PKT_CNT_MSB_OFFSET

                    },
                }
            }
        }
    },
    {
        .type_name = "hses10g", .sig = eDEVBRD_10G_MAC_33_SIG,
        .spec.hses10g = {
            .devname = "fpga1",
            .portid = LOGPORT_KR_9,
            .offset = PORT_KR_COMX2_1_OFFSET,
            .name = PORT_KR_COMX2_1_NAME,
            .iftype = eDEVBRD_IFTYPE_KR,
            .calib_ctl = {
                .enable = CCL_FALSE,
            },
            .autoneg_ctl = {
                .enable = CCL_TRUE,
                .autoneg = {
                    {
                        .name = "autoneg",
                        .offset = PORT_KR_COMX2_1_USER_CTL_OFFSET + AUTONEG_OFFSET,
                        .value = 0
                    }
                }
            },
            .counter_ctl = {
                .enable = CCL_TRUE,
                .width = 40,
                .ctl_offset = PORT_KR_COMX2_1_USER_CTL_OFFSET + COUNTER_CONTROL_OFFSET,
                .ctl = {
                    .tx_pkt_rst_mask = COUNTER_CTL_TX_PACKET_CNT_RST_MASK,
                    .rx_pkt_rst_mask = COUNTER_CTL_RX_PACKET_CNT_RST_MASK,     
                    .rx_bad_pkt_rst_mask = COUNTER_CTL_RX_BAD_PACKET_CNT_RST_MASK, 
                    .freeze_counters_mask = COUNTER_CTL_FREEZE_COUNTERS_MASK,
                    .rollover_mask = COUNTER_CTL_ROLLOVER_MASK       

                },
                .counter = {
                    {
                        .name = "tx_packets_lsb",
                        .offset = PORT_KR_COMX2_1_USER_CTL_OFFSET + TX_PKT_CNT_LSB_OFFSET
                    },
                    {
                        .name = "tx_packets_msb",
                        .offset = PORT_KR_COMX2_1_USER_CTL_OFFSET + TX_PKT_CNT_MSB_OFFSET
                    },
                    {
                        .name = "rx_packets_lsb",
                        .offset = PORT_KR_COMX2_1_USER_CTL_OFFSET + RX_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_packets_msb",
                        .offset = PORT_KR_COMX2_1_USER_CTL_OFFSET + RX_PKT_CNT_MSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_lsb",
                        .offset = PORT_KR_COMX2_1_USER_CTL_OFFSET + RX_BAD_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_msb",
                        .offset = PORT_KR_COMX2_1_USER_CTL_OFFSET + RX_BAD_PKT_CNT_MSB_OFFSET

                    },
                }
            }
        }
    },
    {
        .type_name = "hses10g", .sig = eDEVBRD_10G_MAC_34_SIG,
        .spec.hses10g = {
            .devname = "fpga1",
            .portid = LOGPORT_KR_10,
            .offset = PORT_KR_COMX2_2_OFFSET,
            .name = PORT_KR_COMX2_2_NAME,
            .iftype = eDEVBRD_IFTYPE_KR,
            .calib_ctl = {
                .enable = CCL_FALSE,
            },
            .autoneg_ctl = {
                .enable = CCL_TRUE,
                .autoneg = {
                    {
                        .name = "autoneg",
                        .offset = PORT_KR_COMX2_2_USER_CTL_OFFSET + AUTONEG_OFFSET,
                        .value = 0
                    }
                }
            },
            .counter_ctl = {
                .enable = CCL_TRUE,
                .width = 40,
                .ctl_offset = PORT_KR_COMX2_2_USER_CTL_OFFSET + COUNTER_CONTROL_OFFSET,
                .ctl = {
                    .tx_pkt_rst_mask = COUNTER_CTL_TX_PACKET_CNT_RST_MASK,
                    .rx_pkt_rst_mask = COUNTER_CTL_RX_PACKET_CNT_RST_MASK,     
                    .rx_bad_pkt_rst_mask = COUNTER_CTL_RX_BAD_PACKET_CNT_RST_MASK, 
                    .freeze_counters_mask = COUNTER_CTL_FREEZE_COUNTERS_MASK,
                    .rollover_mask = COUNTER_CTL_ROLLOVER_MASK       

                },
                .counter = {
                    {
                        .name = "tx_packets_lsb",
                        .offset = PORT_KR_COMX2_2_USER_CTL_OFFSET + TX_PKT_CNT_LSB_OFFSET
                    },
                    {
                        .name = "tx_packets_msb",
                        .offset = PORT_KR_COMX2_2_USER_CTL_OFFSET + TX_PKT_CNT_MSB_OFFSET
                    },
                    {
                        .name = "rx_packets_lsb",
                        .offset = PORT_KR_COMX2_2_USER_CTL_OFFSET + RX_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_packets_msb",
                        .offset = PORT_KR_COMX2_2_USER_CTL_OFFSET + RX_PKT_CNT_MSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_lsb",
                        .offset = PORT_KR_COMX2_2_USER_CTL_OFFSET + RX_BAD_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_msb",
                        .offset = PORT_KR_COMX2_2_USER_CTL_OFFSET + RX_BAD_PKT_CNT_MSB_OFFSET

                    },
                }
            }
        }
    },
    {
        .type_name = "hses10g", .sig = eDEVBRD_10G_MAC_35_SIG,
        .spec.hses10g = {
            .devname = "fpga1",
            .portid = LOGPORT_KR_11,
            .offset = PORT_KR_COMX2_3_OFFSET,
            .name = PORT_KR_COMX2_3_NAME,
            .iftype = eDEVBRD_IFTYPE_KR,
            .calib_ctl = {
                .enable = CCL_FALSE,
            },
            .autoneg_ctl = {
                .enable = CCL_TRUE,
                .autoneg = {
                    {
                        .name = "autoneg",
                        .offset = PORT_KR_COMX2_3_USER_CTL_OFFSET + AUTONEG_OFFSET,
                        .value = 0
                    }
                }
            },
            .counter_ctl = {
                .enable = CCL_TRUE,
                .width = 40,
                .ctl_offset = PORT_KR_COMX2_3_USER_CTL_OFFSET + COUNTER_CONTROL_OFFSET,
                .ctl = {
                    .tx_pkt_rst_mask = COUNTER_CTL_TX_PACKET_CNT_RST_MASK,
                    .rx_pkt_rst_mask = COUNTER_CTL_RX_PACKET_CNT_RST_MASK,     
                    .rx_bad_pkt_rst_mask = COUNTER_CTL_RX_BAD_PACKET_CNT_RST_MASK, 
                    .freeze_counters_mask = COUNTER_CTL_FREEZE_COUNTERS_MASK,
                    .rollover_mask = COUNTER_CTL_ROLLOVER_MASK       

                },
                .counter = {
                    {
                        .name = "tx_packets_lsb",
                        .offset = PORT_KR_COMX2_3_USER_CTL_OFFSET + TX_PKT_CNT_LSB_OFFSET
                    },
                    {
                        .name = "tx_packets_msb",
                        .offset = PORT_KR_COMX2_3_USER_CTL_OFFSET + TX_PKT_CNT_MSB_OFFSET
                    },
                    {
                        .name = "rx_packets_lsb",
                        .offset = PORT_KR_COMX2_3_USER_CTL_OFFSET + RX_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_packets_msb",
                        .offset = PORT_KR_COMX2_3_USER_CTL_OFFSET + RX_PKT_CNT_MSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_lsb",
                        .offset = PORT_KR_COMX2_3_USER_CTL_OFFSET + RX_BAD_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_msb",
                        .offset = PORT_KR_COMX2_3_USER_CTL_OFFSET + RX_BAD_PKT_CNT_MSB_OFFSET

                    },
                }
            }
        }
    },
    {
        .type_name = "hses10g", .sig = eDEVBRD_10G_MAC_36_SIG,
        .spec.hses10g = {
            .devname = "fpga1",
            .portid = LOGPORT_KR_12,
            .offset = PORT_KR_COMX3_0_OFFSET,
            .name = PORT_KR_COMX3_0_NAME,
            .iftype = eDEVBRD_IFTYPE_KR,
            .calib_ctl = {
                .enable = CCL_FALSE,
            },
            .autoneg_ctl = {
                .enable = CCL_TRUE,
                .autoneg = {
                    {
                        .name = "autoneg",
                        .offset = PORT_KR_COMX3_0_USER_CTL_OFFSET + AUTONEG_OFFSET,
                        .value = 0
                    }
                }
            },
            .counter_ctl = {
                .enable = CCL_TRUE,
                .width = 40,
                .ctl_offset = PORT_KR_COMX3_0_USER_CTL_OFFSET + COUNTER_CONTROL_OFFSET,
                .ctl = {
                    .tx_pkt_rst_mask = COUNTER_CTL_TX_PACKET_CNT_RST_MASK,
                    .rx_pkt_rst_mask = COUNTER_CTL_RX_PACKET_CNT_RST_MASK,     
                    .rx_bad_pkt_rst_mask = COUNTER_CTL_RX_BAD_PACKET_CNT_RST_MASK, 
                    .freeze_counters_mask = COUNTER_CTL_FREEZE_COUNTERS_MASK,
                    .rollover_mask = COUNTER_CTL_ROLLOVER_MASK       

                },
                .counter = {
                    {
                        .name = "tx_packets_lsb",
                        .offset = PORT_KR_COMX3_0_USER_CTL_OFFSET + TX_PKT_CNT_LSB_OFFSET
                    },
                    {
                        .name = "tx_packets_msb",
                        .offset = PORT_KR_COMX3_0_USER_CTL_OFFSET + TX_PKT_CNT_MSB_OFFSET
                    },
                    {
                        .name = "rx_packets_lsb",
                        .offset = PORT_KR_COMX3_0_USER_CTL_OFFSET + RX_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_packets_msb",
                        .offset = PORT_KR_COMX3_0_USER_CTL_OFFSET + RX_PKT_CNT_MSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_lsb",
                        .offset = PORT_KR_COMX3_0_USER_CTL_OFFSET + RX_BAD_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_msb",
                        .offset = PORT_KR_COMX3_0_USER_CTL_OFFSET + RX_BAD_PKT_CNT_MSB_OFFSET

                    },
                }
            }
        }
    },
    {
        .type_name = "hses10g", .sig = eDEVBRD_10G_MAC_37_SIG,
        .spec.hses10g = {
            .devname = "fpga1",
            .portid = LOGPORT_KR_13,
            .offset = PORT_KR_COMX3_1_OFFSET,
            .name = PORT_KR_COMX3_1_NAME,
            .iftype = eDEVBRD_IFTYPE_KR,
            .calib_ctl = {
                .enable = CCL_FALSE,
            },
            .autoneg_ctl = {
                .enable = CCL_TRUE,
                .autoneg = {
                    {
                        .name = "autoneg",
                        .offset = PORT_KR_COMX3_1_USER_CTL_OFFSET + AUTONEG_OFFSET,
                        .value = 0
                    }
                }
            },
            .counter_ctl = {
                .enable = CCL_TRUE,
                .width = 40,
                .ctl_offset = PORT_KR_COMX3_1_USER_CTL_OFFSET + COUNTER_CONTROL_OFFSET,
                .ctl = {
                    .tx_pkt_rst_mask = COUNTER_CTL_TX_PACKET_CNT_RST_MASK,
                    .rx_pkt_rst_mask = COUNTER_CTL_RX_PACKET_CNT_RST_MASK,     
                    .rx_bad_pkt_rst_mask = COUNTER_CTL_RX_BAD_PACKET_CNT_RST_MASK, 
                    .freeze_counters_mask = COUNTER_CTL_FREEZE_COUNTERS_MASK,
                    .rollover_mask = COUNTER_CTL_ROLLOVER_MASK       

                },
                .counter = {
                    {
                        .name = "tx_packets_lsb",
                        .offset = PORT_KR_COMX3_1_USER_CTL_OFFSET + TX_PKT_CNT_LSB_OFFSET
                    },
                    {
                        .name = "tx_packets_msb",
                        .offset = PORT_KR_COMX3_1_USER_CTL_OFFSET + TX_PKT_CNT_MSB_OFFSET
                    },
                    {
                        .name = "rx_packets_lsb",
                        .offset = PORT_KR_COMX3_1_USER_CTL_OFFSET + RX_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_packets_msb",
                        .offset = PORT_KR_COMX3_1_USER_CTL_OFFSET + RX_PKT_CNT_MSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_lsb",
                        .offset = PORT_KR_COMX3_1_USER_CTL_OFFSET + RX_BAD_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_msb",
                        .offset = PORT_KR_COMX3_1_USER_CTL_OFFSET + RX_BAD_PKT_CNT_MSB_OFFSET

                    },
                }
            }
        }
    },
    {
        .type_name = "hses10g", .sig = eDEVBRD_10G_MAC_38_SIG,
        .spec.hses10g = {
            .devname = "fpga1",
            .portid = LOGPORT_KR_14,
            .offset = PORT_KR_COMX3_2_OFFSET,
            .name = PORT_KR_COMX3_2_NAME,
            .iftype = eDEVBRD_IFTYPE_KR,
            .calib_ctl = {
                .enable = CCL_FALSE,
            },
            .autoneg_ctl = {
                .enable = CCL_TRUE,
                .autoneg = {
                    {
                        .name = "autoneg",
                        .offset = PORT_KR_COMX3_2_USER_CTL_OFFSET + AUTONEG_OFFSET,
                        .value = 0
                    }
                }
            },
            .counter_ctl = {
                .enable = CCL_TRUE,
                .width = 40,
                .ctl_offset = PORT_KR_COMX3_2_USER_CTL_OFFSET + COUNTER_CONTROL_OFFSET,
                .ctl = {
                    .tx_pkt_rst_mask = COUNTER_CTL_TX_PACKET_CNT_RST_MASK,
                    .rx_pkt_rst_mask = COUNTER_CTL_RX_PACKET_CNT_RST_MASK,     
                    .rx_bad_pkt_rst_mask = COUNTER_CTL_RX_BAD_PACKET_CNT_RST_MASK, 
                    .freeze_counters_mask = COUNTER_CTL_FREEZE_COUNTERS_MASK,
                    .rollover_mask = COUNTER_CTL_ROLLOVER_MASK       

                },
                .counter = {
                    {
                        .name = "tx_packets_lsb",
                        .offset = PORT_KR_COMX3_2_USER_CTL_OFFSET + TX_PKT_CNT_LSB_OFFSET
                    },
                    {
                        .name = "tx_packets_msb",
                        .offset = PORT_KR_COMX3_2_USER_CTL_OFFSET + TX_PKT_CNT_MSB_OFFSET
                    },
                    {
                        .name = "rx_packets_lsb",
                        .offset = PORT_KR_COMX3_2_USER_CTL_OFFSET + RX_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_packets_msb",
                        .offset = PORT_KR_COMX3_2_USER_CTL_OFFSET + RX_PKT_CNT_MSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_lsb",
                        .offset = PORT_KR_COMX3_2_USER_CTL_OFFSET + RX_BAD_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_msb",
                        .offset = PORT_KR_COMX3_2_USER_CTL_OFFSET + RX_BAD_PKT_CNT_MSB_OFFSET

                    },
                }
            }
        }
    },
    {
        .type_name = "hses10g", .sig = eDEVBRD_10G_MAC_39_SIG,
        .spec.hses10g = {
            .devname = "fpga1",
            .portid = LOGPORT_KR_15,
            .offset = PORT_KR_COMX3_3_OFFSET,
            .name = PORT_KR_COMX3_3_NAME,
            .iftype = eDEVBRD_IFTYPE_KR,
            .calib_ctl = {
                .enable = CCL_FALSE,
            },
            .autoneg_ctl = {
                .enable = CCL_TRUE,
                .autoneg = {
                    {
                        .name = "autoneg",
                        .offset = PORT_KR_COMX3_3_USER_CTL_OFFSET + AUTONEG_OFFSET,
                        .value = 0
                    }
                }
            },
            .counter_ctl = {
                .enable = CCL_TRUE,
                .width = 40,
                .ctl_offset = PORT_KR_COMX3_3_USER_CTL_OFFSET + COUNTER_CONTROL_OFFSET,
                .ctl = {
                    .tx_pkt_rst_mask = COUNTER_CTL_TX_PACKET_CNT_RST_MASK,
                    .rx_pkt_rst_mask = COUNTER_CTL_RX_PACKET_CNT_RST_MASK,     
                    .rx_bad_pkt_rst_mask = COUNTER_CTL_RX_BAD_PACKET_CNT_RST_MASK, 
                    .freeze_counters_mask = COUNTER_CTL_FREEZE_COUNTERS_MASK,
                    .rollover_mask = COUNTER_CTL_ROLLOVER_MASK       

                },
                .counter = {
                    {
                        .name = "tx_packets_lsb",
                        .offset = PORT_KR_COMX3_3_USER_CTL_OFFSET + TX_PKT_CNT_LSB_OFFSET
                    },
                    {
                        .name = "tx_packets_msb",
                        .offset = PORT_KR_COMX3_3_USER_CTL_OFFSET + TX_PKT_CNT_MSB_OFFSET
                    },
                    {
                        .name = "rx_packets_lsb",
                        .offset = PORT_KR_COMX3_3_USER_CTL_OFFSET + RX_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_packets_msb",
                        .offset = PORT_KR_COMX3_3_USER_CTL_OFFSET + RX_PKT_CNT_MSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_lsb",
                        .offset = PORT_KR_COMX3_3_USER_CTL_OFFSET + RX_BAD_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_msb",
                        .offset = PORT_KR_COMX3_3_USER_CTL_OFFSET + RX_BAD_PKT_CNT_MSB_OFFSET

                    },
                }
            }
        }
    },
};

static board_device_descr_t _usplus100g_brd_descr[] = {
    {
        /* J121*/
        .type_name = "usplus100g", .sig = eDEVBRD_100G_MAC_0_SIG,
        .spec.usplus100g = {
            .devname = "fpga1",
            .portid = LOGPORT_100G_0,
            .offset = PORT_100G_0_NETWORK_OFFSET,
            .name = PORT_100G_0_NETWORK_NAME,
            .calib_ctl = {
                .enable = CCL_TRUE,
                .channel_num = 4,
                .channel_calib_params = {
                    {
                        .calib = {
                            {
                                .name = "txdiffswing",
                                .offset = PORT_100G_0_NETWORK_CHANNEL_0_USER_CTL_OFFSET + TXDIFFCTRL_OFFSET,
                                .value = 0, /* dB */
                                .encoded_value = 0x16
                            },
                            {
                                .name = "txpost",
                                .offset = PORT_100G_0_NETWORK_CHANNEL_0_USER_CTL_OFFSET + TXPOSTCURSOR_OFFSET,
                                .value = 0.0, /* dB */
                                .encoded_value = 0xe
                            },
                            {
                                .name = "txpre",
                                .offset = PORT_100G_0_NETWORK_CHANNEL_0_USER_CTL_OFFSET + TXPRECURSOR_OFFSET,
                                .value = 0, 
                                .encoded_value = 0x8
                            },
                        }
                    },
                    {
                        .calib = {
                            {
                                .name = "txdiffswing",
                                .offset = PORT_100G_0_NETWORK_CHANNEL_1_USER_CTL_OFFSET + TXDIFFCTRL_OFFSET,
                                .value = 0, /* dB */
                                .encoded_value = 0x16
                            },
                            {
                                .name = "txpost",
                                .offset = PORT_100G_0_NETWORK_CHANNEL_1_USER_CTL_OFFSET + TXPOSTCURSOR_OFFSET,
                                .value = 0.0, /* dB */
                                .encoded_value = 0xe
                            },
                            {
                                .name = "txpre",
                                .offset = PORT_100G_0_NETWORK_CHANNEL_1_USER_CTL_OFFSET + TXPRECURSOR_OFFSET,
                                .value = 0, 
                                .encoded_value = 0x8
                            },
                        }
                    },
                    {
                        .calib = {
                            {
                                .name = "txdiffswing",
                                .offset = PORT_100G_0_NETWORK_CHANNEL_2_USER_CTL_OFFSET + TXDIFFCTRL_OFFSET,
                                .value = 0, /* dB */
                                .encoded_value = 0x16
                            },
                            {
                                .name = "txpost",
                                .offset = PORT_100G_0_NETWORK_CHANNEL_2_USER_CTL_OFFSET + TXPOSTCURSOR_OFFSET,
                                .value = 0.0, /* dB */
                                .encoded_value = 0xe
                            },
                            {
                                .name = "txpre",
                                .offset = PORT_100G_0_NETWORK_CHANNEL_2_USER_CTL_OFFSET + TXPRECURSOR_OFFSET,
                                .value = 0, 
                                .encoded_value = 0x8
                            },
                        }
                    },
                    {
                        .calib = {
                            {
                                .name = "txdiffswing",
                                .offset = PORT_100G_0_NETWORK_CHANNEL_3_USER_CTL_OFFSET + TXDIFFCTRL_OFFSET,
                                .value = 0, /* dB */
                                .encoded_value = 0x16
                            },
                            {
                                .name = "txpost",
                                .offset = PORT_100G_0_NETWORK_CHANNEL_3_USER_CTL_OFFSET + TXPOSTCURSOR_OFFSET,
                                .value = 0.0, /* dB */
                                .encoded_value = 0xe
                            },
                            {
                                .name = "txdiffswing",
                                .offset = PORT_100G_0_NETWORK_CHANNEL_3_USER_CTL_OFFSET + TXPRECURSOR_OFFSET,
                                .value = 0, 
                                .encoded_value = 0x8
                            },
                        }
                    }
                }
            },
            .counter_ctl = {
                .enable = CCL_TRUE,
                .width = 40,
                .ctl_offset = PORT_100G_0_NETWORK_CHANNEL_0_USER_CTL_OFFSET + COUNTER_CONTROL_OFFSET,
                .ctl = {
                    .tx_pkt_rst_mask = COUNTER_CTL_TX_PACKET_CNT_RST_MASK,
                    .rx_pkt_rst_mask = COUNTER_CTL_RX_PACKET_CNT_RST_MASK,     
                    .rx_bad_pkt_rst_mask = COUNTER_CTL_RX_BAD_PACKET_CNT_RST_MASK, 
                    .freeze_counters_mask = COUNTER_CTL_FREEZE_COUNTERS_MASK,
                    .rollover_mask = COUNTER_CTL_ROLLOVER_MASK       

                },
                .counter = {
                    {
                        .name = "tx_packets_lsb",
                        .offset = PORT_100G_0_NETWORK_CHANNEL_0_USER_CTL_OFFSET + TX_PKT_CNT_LSB_OFFSET
                    },
                    {
                        .name = "tx_packets_msb",
                        .offset = PORT_100G_0_NETWORK_CHANNEL_0_USER_CTL_OFFSET + TX_PKT_CNT_MSB_OFFSET
                    },
                    {
                        .name = "rx_packets_lsb",
                        .offset = PORT_100G_0_NETWORK_CHANNEL_0_USER_CTL_OFFSET + RX_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_packets_msb",
                        .offset = PORT_100G_0_NETWORK_CHANNEL_0_USER_CTL_OFFSET + RX_PKT_CNT_MSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_lsb",
                        .offset = PORT_100G_0_NETWORK_CHANNEL_0_USER_CTL_OFFSET + RX_BAD_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_msb",
                        .offset = PORT_100G_0_NETWORK_CHANNEL_0_USER_CTL_OFFSET + RX_BAD_PKT_CNT_MSB_OFFSET

                    },
                }
            }
        }
    },
    {
        /* J124*/
        .type_name = "usplus100g", .sig = eDEVBRD_100G_MAC_1_SIG,
        .spec.usplus100g = {
            .devname = "fpga1",
            .portid = LOGPORT_100G_1,
            .offset = PORT_100G_1_NETWORK_OFFSET,
            .name = PORT_100G_1_NETWORK_NAME,
            .calib_ctl = {
                .enable = CCL_TRUE,
                .channel_num = 4,
                .channel_calib_params = {
                    {
                        .calib = {
                            {
                                .name = "txdiffswing",
                                .offset = PORT_100G_1_NETWORK_CHANNEL_0_USER_CTL_OFFSET + TXDIFFCTRL_OFFSET,
                                .value = 0, /* dB */
                                .encoded_value = 0x16
                            },
                            {
                                .name = "txpost",
                                .offset = PORT_100G_1_NETWORK_CHANNEL_0_USER_CTL_OFFSET + TXPOSTCURSOR_OFFSET,
                                .value = 0.0, /* dB */
                                .encoded_value = 0xe
                            },
                            {
                                .name = "txpre",
                                .offset = PORT_100G_1_NETWORK_CHANNEL_0_USER_CTL_OFFSET + TXPRECURSOR_OFFSET,
                                .value = 0, 
                                .encoded_value = 0x8
                            },
                        }
                    },
                    {
                        .calib = {
                            {
                                .name = "txdiffswing",
                                .offset = PORT_100G_1_NETWORK_CHANNEL_1_USER_CTL_OFFSET + TXDIFFCTRL_OFFSET,
                                .value = 0, /* dB */
                                .encoded_value = 0x16
                            },
                            {
                                .name = "txpost",
                                .offset = PORT_100G_1_NETWORK_CHANNEL_1_USER_CTL_OFFSET + TXPOSTCURSOR_OFFSET,
                                .value = 0.0, /* dB */
                                .encoded_value = 0xe
                            },
                            {
                                .name = "txpre",
                                .offset = PORT_100G_1_NETWORK_CHANNEL_1_USER_CTL_OFFSET + TXPRECURSOR_OFFSET,
                                .value = 0, 
                                .encoded_value = 0x8
                            },
                        }
                    },
                    {
                        .calib = {
                            {
                                .name = "txdiffswing",
                                .offset = PORT_100G_1_NETWORK_CHANNEL_2_USER_CTL_OFFSET + TXDIFFCTRL_OFFSET,
                                .value = 0, /* dB */
                                .encoded_value = 0x16
                            },
                            {
                                .name = "txpost",
                                .offset = PORT_100G_1_NETWORK_CHANNEL_2_USER_CTL_OFFSET + TXPOSTCURSOR_OFFSET,
                                .value = 0.0, /* dB */
                                .encoded_value = 0xe
                            },
                            {
                                .name = "txpre",
                                .offset = PORT_100G_1_NETWORK_CHANNEL_2_USER_CTL_OFFSET + TXPRECURSOR_OFFSET,
                                .value = 0, 
                                .encoded_value = 0x8
                            },
                        }
                    },
                    {
                        .calib = {
                            {
                                .name = "txdiffswing",
                                .offset = PORT_100G_1_NETWORK_CHANNEL_3_USER_CTL_OFFSET + TXDIFFCTRL_OFFSET,
                                .value = 0, /* dB */
                                .encoded_value = 0x16
                            },
                            {
                                .name = "txpost",
                                .offset = PORT_100G_1_NETWORK_CHANNEL_3_USER_CTL_OFFSET + TXPOSTCURSOR_OFFSET,
                                .value = 0.0, /* dB */
                                .encoded_value = 0xe
                            },
                            {
                                .name = "txpre",
                                .offset = PORT_100G_1_NETWORK_CHANNEL_3_USER_CTL_OFFSET + TXPRECURSOR_OFFSET,
                                .value = 0, 
                                .encoded_value = 0x8
                            },
                        }
                    }
                }
            },
            .counter_ctl = {
                .enable = CCL_TRUE,
                .width = 40,
                .ctl_offset = PORT_100G_1_NETWORK_CHANNEL_0_USER_CTL_OFFSET + COUNTER_CONTROL_OFFSET,
                .ctl = {
                    .tx_pkt_rst_mask = COUNTER_CTL_TX_PACKET_CNT_RST_MASK,
                    .rx_pkt_rst_mask = COUNTER_CTL_RX_PACKET_CNT_RST_MASK,     
                    .rx_bad_pkt_rst_mask = COUNTER_CTL_RX_BAD_PACKET_CNT_RST_MASK, 
                    .freeze_counters_mask = COUNTER_CTL_FREEZE_COUNTERS_MASK,
                    .rollover_mask = COUNTER_CTL_ROLLOVER_MASK       

                },
                .counter = {
                    {
                        .name = "tx_packets_lsb",
                        .offset = PORT_100G_1_NETWORK_CHANNEL_0_USER_CTL_OFFSET + TX_PKT_CNT_LSB_OFFSET
                    },
                    {
                        .name = "tx_packets_msb",
                        .offset = PORT_100G_1_NETWORK_CHANNEL_0_USER_CTL_OFFSET + TX_PKT_CNT_MSB_OFFSET
                    },
                    {
                        .name = "rx_packets_lsb",
                        .offset = PORT_100G_1_NETWORK_CHANNEL_0_USER_CTL_OFFSET + RX_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_packets_msb",
                        .offset = PORT_100G_1_NETWORK_CHANNEL_0_USER_CTL_OFFSET + RX_PKT_CNT_MSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_lsb",
                        .offset = PORT_100G_1_NETWORK_CHANNEL_0_USER_CTL_OFFSET + RX_BAD_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_msb",
                        .offset = PORT_100G_1_NETWORK_CHANNEL_0_USER_CTL_OFFSET + RX_BAD_PKT_CNT_MSB_OFFSET

                    },
                }
            }
        }
    },
    {
        /* J123*/
        .type_name = "usplus100g", .sig = eDEVBRD_100G_MAC_2_SIG,
        .spec.usplus100g = {
            .devname = "fpga1",
            .portid = LOGPORT_100G_2,
            .offset = PORT_100G_0_CLIENT_OFFSET,
            .name = PORT_100G_0_CLIENT_NAME,
            .calib_ctl = {
                .enable = CCL_TRUE,
                .channel_num = 4,
                .channel_calib_params = {
                    {
                        .calib = {
                            {
                                .name = "txdiffswing",
                                .offset = PORT_100G_0_CLIENT_CHANNEL_0_USER_CTL_OFFSET + TXDIFFCTRL_OFFSET,
                                .value = 0, /* dB */
                                .encoded_value = 0x16
                            },
                            {
                                .name = "txpost",
                                .offset = PORT_100G_0_CLIENT_CHANNEL_0_USER_CTL_OFFSET + TXPOSTCURSOR_OFFSET,
                                .value = 0.0, /* dB */
                                .encoded_value = 0xe
                            },
                            {
                                .name = "txpre",
                                .offset = PORT_100G_0_CLIENT_CHANNEL_0_USER_CTL_OFFSET + TXPRECURSOR_OFFSET,
                                .value = 0, 
                                .encoded_value = 0x8
                            },
                        }
                    },
                    {
                        .calib = {
                            {
                                .name = "txdiffswing",
                                .offset = PORT_100G_0_CLIENT_CHANNEL_1_USER_CTL_OFFSET + TXDIFFCTRL_OFFSET,
                                .value = 0, /* dB */
                                .encoded_value = 0x16
                            },
                            {
                                .name = "txpost",
                                .offset = PORT_100G_0_CLIENT_CHANNEL_1_USER_CTL_OFFSET + TXPOSTCURSOR_OFFSET,
                                .value = 0.0, /* dB */
                                .encoded_value = 0xe
                            },
                            {
                                .name = "txpre",
                                .offset = PORT_100G_0_CLIENT_CHANNEL_1_USER_CTL_OFFSET + TXPRECURSOR_OFFSET,
                                .value = 0, 
                                .encoded_value = 0x8
                            },
                        }
                    },
                    {
                        .calib = {
                            {
                                .name = "txdiffswing",
                                .offset = PORT_100G_0_CLIENT_CHANNEL_2_USER_CTL_OFFSET + TXDIFFCTRL_OFFSET,
                                .value = 0, /* dB */
                                .encoded_value = 0x16
                            },
                            {
                                .name = "txpost",
                                .offset = PORT_100G_0_CLIENT_CHANNEL_2_USER_CTL_OFFSET + TXPOSTCURSOR_OFFSET,
                                .value = 0.0, /* dB */
                                .encoded_value = 0xe
                            },
                            {
                                .name = "txpre",
                                .offset = PORT_100G_0_CLIENT_CHANNEL_2_USER_CTL_OFFSET + TXPRECURSOR_OFFSET,
                                .value = 0, 
                                .encoded_value = 0x8
                            },
                        }
                    },
                    {
                        .calib = {
                            {
                                .name = "txdiffswing",
                                .offset = PORT_100G_0_CLIENT_CHANNEL_3_USER_CTL_OFFSET + TXDIFFCTRL_OFFSET,
                                .value = 0, /* dB */
                                .encoded_value = 0x16
                            },
                            {
                                .name = "txpost",
                                .offset = PORT_100G_0_CLIENT_CHANNEL_3_USER_CTL_OFFSET + TXPOSTCURSOR_OFFSET,
                                .value = 0.0, /* dB */
                                .encoded_value = 0xe
                            },
                            {
                                .name = "txpre",
                                .offset = PORT_100G_0_CLIENT_CHANNEL_3_USER_CTL_OFFSET + TXPRECURSOR_OFFSET,
                                .value = 0, 
                                .encoded_value = 0x8
                            },
                        }
                    }
                }
            },
            .counter_ctl = {
                .enable = CCL_TRUE,
                .width = 40,
                .ctl_offset = PORT_100G_0_CLIENT_CHANNEL_0_USER_CTL_OFFSET + COUNTER_CONTROL_OFFSET,
                .ctl = {
                    .tx_pkt_rst_mask = COUNTER_CTL_TX_PACKET_CNT_RST_MASK,
                    .rx_pkt_rst_mask = COUNTER_CTL_RX_PACKET_CNT_RST_MASK,     
                    .rx_bad_pkt_rst_mask = COUNTER_CTL_RX_BAD_PACKET_CNT_RST_MASK, 
                    .freeze_counters_mask = COUNTER_CTL_FREEZE_COUNTERS_MASK,
                    .rollover_mask = COUNTER_CTL_ROLLOVER_MASK       

                },
                .counter = {
                    {
                        .name = "tx_packets_lsb",
                        .offset = PORT_100G_0_CLIENT_CHANNEL_0_USER_CTL_OFFSET + TX_PKT_CNT_LSB_OFFSET
                    },
                    {
                        .name = "tx_packets_msb",
                        .offset = PORT_100G_0_CLIENT_CHANNEL_0_USER_CTL_OFFSET + TX_PKT_CNT_MSB_OFFSET
                    },
                    {
                        .name = "rx_packets_lsb",
                        .offset = PORT_100G_0_CLIENT_CHANNEL_0_USER_CTL_OFFSET + RX_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_packets_msb",
                        .offset = PORT_100G_0_CLIENT_CHANNEL_0_USER_CTL_OFFSET + RX_PKT_CNT_MSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_lsb",
                        .offset = PORT_100G_0_CLIENT_CHANNEL_0_USER_CTL_OFFSET + RX_BAD_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_msb",
                        .offset = PORT_100G_0_CLIENT_CHANNEL_0_USER_CTL_OFFSET + RX_BAD_PKT_CNT_MSB_OFFSET

                    },
                }
            }
        }
    },
    {
        /* J126*/
        .type_name = "usplus100g", .sig = eDEVBRD_100G_MAC_3_SIG,
        .spec.usplus100g = {
            .devname = "fpga1",
            .portid = LOGPORT_100G_3,
            .offset = PORT_100G_1_CLIENT_OFFSET,
            .name = PORT_100G_1_CLIENT_NAME,
            .calib_ctl = {
                .enable = CCL_TRUE,
                .channel_num = 4,
                .channel_calib_params = {
                    {
                        .calib = {
                            {
                                .name = "txdiffswing",
                                .offset = PORT_100G_1_CLIENT_CHANNEL_0_USER_CTL_OFFSET + TXDIFFCTRL_OFFSET,
                                .value = 0, /* dB */
                                .encoded_value = 0x16
                            },
                            {
                                .name = "txpost",
                                .offset = PORT_100G_1_CLIENT_CHANNEL_0_USER_CTL_OFFSET + TXPOSTCURSOR_OFFSET,
                                .value = 0.0, /* dB */
                                .encoded_value = 0xe
                            },
                            {
                                .name = "txpre",
                                .offset = PORT_100G_1_CLIENT_CHANNEL_0_USER_CTL_OFFSET + TXPRECURSOR_OFFSET,
                                .value = 0, 
                                .encoded_value = 0x8
                            },
                        }
                    },
                    {
                        .calib = {
                            {
                                .name = "txdiffswing",
                                .offset = PORT_100G_1_CLIENT_CHANNEL_1_USER_CTL_OFFSET + TXDIFFCTRL_OFFSET,
                                .value = 0, /* dB */
                                .encoded_value = 0x16
                            },
                            {
                                .name = "txpost",
                                .offset = PORT_100G_1_CLIENT_CHANNEL_1_USER_CTL_OFFSET + TXPOSTCURSOR_OFFSET,
                                .value = 0.0, /* dB */
                                .encoded_value = 0xe
                            },
                            {
                                .name = "txpre",
                                .offset = PORT_100G_1_CLIENT_CHANNEL_1_USER_CTL_OFFSET + TXPRECURSOR_OFFSET,
                                .value = 0, 
                                .encoded_value = 0x8
                            },
                        }
                    },
                    {
                        .calib = {
                            {
                                .name = "txdiffswing",
                                .offset = PORT_100G_1_CLIENT_CHANNEL_2_USER_CTL_OFFSET + TXDIFFCTRL_OFFSET,
                                .value = 0, /* dB */
                                .encoded_value = 0x16
                            },
                            {
                                .name = "txpost",
                                .offset = PORT_100G_1_CLIENT_CHANNEL_2_USER_CTL_OFFSET + TXPOSTCURSOR_OFFSET,
                                .value = 0.0, /* dB */
                                .encoded_value = 0xe
                            },
                            {
                                .name = "txpre",
                                .offset = PORT_100G_1_CLIENT_CHANNEL_2_USER_CTL_OFFSET + TXPRECURSOR_OFFSET,
                                .value = 0, 
                                .encoded_value = 0x8
                            },
                        }
                    },
                    {
                        .calib = {
                            {
                                .name = "txdiffswing",
                                .offset = PORT_100G_1_CLIENT_CHANNEL_3_USER_CTL_OFFSET + TXDIFFCTRL_OFFSET,
                                .value = 0, /* dB */
                                .encoded_value = 0x16
                            },
                            {
                                .name = "txpost",
                                .offset = PORT_100G_1_CLIENT_CHANNEL_3_USER_CTL_OFFSET + TXPOSTCURSOR_OFFSET,
                                .value = 0.0, /* dB */
                                .encoded_value = 0xe
                            },
                            {
                                .name = "txpre",
                                .offset = PORT_100G_1_CLIENT_CHANNEL_3_USER_CTL_OFFSET + TXPRECURSOR_OFFSET,
                                .value = 0, 
                                .encoded_value = 0x8
                            },
                        }
                    }
                }
            },
            .counter_ctl = {
                .enable = CCL_TRUE,
                .width = 40,
                .ctl_offset = PORT_100G_1_CLIENT_CHANNEL_0_USER_CTL_OFFSET + COUNTER_CONTROL_OFFSET,
                .ctl = {
                    .tx_pkt_rst_mask = COUNTER_CTL_TX_PACKET_CNT_RST_MASK,
                    .rx_pkt_rst_mask = COUNTER_CTL_RX_PACKET_CNT_RST_MASK,     
                    .rx_bad_pkt_rst_mask = COUNTER_CTL_RX_BAD_PACKET_CNT_RST_MASK, 
                    .freeze_counters_mask = COUNTER_CTL_FREEZE_COUNTERS_MASK,
                    .rollover_mask = COUNTER_CTL_ROLLOVER_MASK       

                },
                .counter = {
                    {
                        .name = "tx_packets_lsb",
                        .offset = PORT_100G_1_CLIENT_CHANNEL_0_USER_CTL_OFFSET + TX_PKT_CNT_LSB_OFFSET
                    },
                    {
                        .name = "tx_packets_msb",
                        .offset = PORT_100G_1_CLIENT_CHANNEL_0_USER_CTL_OFFSET + TX_PKT_CNT_MSB_OFFSET
                    },
                    {
                        .name = "rx_packets_lsb",
                        .offset = PORT_100G_1_CLIENT_CHANNEL_0_USER_CTL_OFFSET + RX_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_packets_msb",
                        .offset = PORT_100G_1_CLIENT_CHANNEL_0_USER_CTL_OFFSET + RX_PKT_CNT_MSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_lsb",
                        .offset = PORT_100G_1_CLIENT_CHANNEL_0_USER_CTL_OFFSET + RX_BAD_PKT_CNT_LSB_OFFSET

                    },
                    {
                        .name = "rx_bad_packets_msb",
                        .offset = PORT_100G_1_CLIENT_CHANNEL_0_USER_CTL_OFFSET + RX_BAD_PKT_CNT_MSB_OFFSET

                    },
                }
            }
        }
    },
};

static board_device_descr_t _spidev_brd_descr[] = {
    {
        .type_name = "spidev", .sig = eDEVBRD_SPIDEV_0_SIG, 
        .subtype_name = "atp1",
        .spec.spidev = {
            .addr_width = 3,
            .dev_id = eDEVBRD_AXISPI_1,
            .cs = 0,
            .name = "ATP1"
        }
    },
    {
        .type_name = "spidev", .sig = eDEVBRD_SPIDEV_1_SIG, 
        .subtype_name = "atp2",
        .spec.spidev = {
            .addr_width = 3,
            .dev_id = eDEVBRD_AXISPI_1,
            .cs = 1,
            .name = "ATP2"
        }
    },
    {
        .type_name = "spidev", .sig = eDEVBRD_SPIDEV_2_SIG, 
        .subtype_name = "gen_flash1",
        .spec.spidev = {
            .addr_width = 3,
            .dev_id = eDEVBRD_AXISPI_3,
            .cs = 0,
            .name = "GEN_FLASH1"
        }
    },
    {
        .type_name = "spidev", .sig = eDEVBRD_SPIDEV_3_SIG, 
        .subtype_name = "gen_flash2",
        .spec.spidev = {
            .addr_width = 3,
            .dev_id = eDEVBRD_AXISPI_3,
            .cs = 1,
            .name = "GEN_FLASH2"
        }
    },
    {
        .type_name = "spidev", .sig = eDEVBRD_SPIDEV_4_SIG, 
        .subtype_name = "main_cpu",
        .spec.spidev = {
            .addr_width = 3,
            .page_size = 256,
            .bloff = 0x400,
            .sector_erase_cmd = 0xD8,
            .sector_erase_cmd_width = 1,
            .burst_l = 16,
            .dev_id = eDEVBRD_AXISPI_3,
            .cs = 3,
            .name = "MAIN_CPU"
        }
    },
    {
        .type_name = "spidev", .sig = eDEVBRD_SPIDEV_5_SIG, 
        .subtype_name = "ctrl_cpu",
        .spec.spidev = {
            .addr_width = 3,
            .page_size = 256,
            .sector_size = 0x10000, // 64KB
            .bloff = 0x400,
            .sector_erase_cmd = 0xD8,
            .sector_erase_cmd_width = 1,
            .burst_l = 16,
            .dev_id = eDEVBRD_AXISPI_3,
            .cs = 2,
            .name = "CTRL_CPU"
        }
    },
    {
        .type_name = "spidev", .sig = eDEVBRD_SPIDEV_6_SIG, 
        .subtype_name = "app_cpu0",
        .spec.spidev = {
            .addr_width = 3,
            .page_size = 256,
            .sector_size = 0x10000, // 64KB
            .bloff = 0x0,
            .sector_erase_cmd = 0xD8,
            .sector_erase_cmd_width = 1,
            .burst_l = 16,
            .dev_id = eDEVBRD_AXISPI_3,
            .cs = 4,
            .name = "COMX0"
        }
    },
    {
        .type_name = "spidev", .sig = eDEVBRD_SPIDEV_7_SIG, 
        .subtype_name = "app_cpu1",
        .spec.spidev = {
            .addr_width = 2,
            .dev_id = eDEVBRD_AXISPI_3,
            .cs = 5,
            .name = "COMX1"        
        }
    },
    {
        .type_name = "spidev", .sig = eDEVBRD_SPIDEV_8_SIG, 
        .subtype_name = "app_cpu2",
        .spec.spidev = {
            .addr_width = 2,
            .dev_id = eDEVBRD_AXISPI_3,
            .cs = 6,
            .name = "COMX2"
        }
    },
    {
        .type_name = "spidev", .sig = eDEVBRD_SPIDEV_9_SIG, 
        .subtype_name = "app_cpu3",
        .spec.spidev = {
            .addr_width = 2,
            .dev_id = eDEVBRD_AXISPI_3,
            .cs = 7,
            .name = "COMX3"        
        }
    },
};

static board_device_descr_t _pmbus_brd_descr[] = {
    {
        .type_name = "pmbus", .sig = eDEVBRD_PMBUS_0_SIG, 
        .subtype_name = "vccint_0.85v",
        .spec.pmbus = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_1,
                .dev_i2ca = eDEVBRD_I2CA_VCCINT_O_85V
            },
            .name = "VCCINT_O.85V",
            .exponent = -14,
            .extra_mux_pin = 1,
            .vout_max = 15,
            .iout_max = 100,
            .vin_max = 15,
            .cmd[ePMBUS_CMD_OPERATION] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_ON_OFF_CONFIG] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_CLEAR_FAULTS] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_WRITE_PROTECT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STORE_DEFAULT_ALL] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_RESTORE_DEFAULT_ALL] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_MODE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_COMMAND] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_TRIM] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_CAL_OFFSET] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_MARGIN_HIGH] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_MARGIN_LOW] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VIN_ON] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VIN_OFF] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_IOUT_CAL_GAIN] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_IOUT_CAL_OFFSET] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_OV_FAULT_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_OV_FAULT_RESPONSE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_OV_WARN_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_UV_WARN_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_UV_FAULT_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_UV_FAULT_RESPONSE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_IOUT_OC_FAULT_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_IOUT_OC_WARN_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_OT_FAULT_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_OT_FAULT_RESPONSE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_OT_WARN_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VIN_OV_FAULT_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VIN_OV_FAULT_RESPONSE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VIN_OV_WARN_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VIN_UV_WARN_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VIN_UV_FAULT_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VIN_UV_FAULT_RESPONSE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_POWER_GOOD_ON] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_POWER_GOOD_OFF] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_TON_DELAY] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_TON_RISE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_TOFF_DELAY] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_TOFF_FALL] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_BYTE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_WORD] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_VOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_IOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_INPUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_TEMPERATURE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_CML] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_VIN] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_VOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_IOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_TEMPERATURE_1] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_TEMPERATURE_2] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_FREQUENCY] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_PMBUS_REVISION] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_USER_DATA_00] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_USER_DATA_01] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_USER_DATA_02] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_USER_DATA_03] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_00] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_04] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_05] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_07] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_08] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_11] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_13] = {
                .is_supported = CCL_TRUE,
                .size = 1
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_15] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_32] = {
                .is_supported = CCL_TRUE,
                .size = 12
            }
        }
    },
    {
        .type_name = "pmbus", .sig = eDEVBRD_PMBUS_1_SIG, 
        .subtype_name = "vccaux_1.8v",
        .spec.pmbus = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_1,
                .dev_i2ca = eDEVBRD_I2CA_VCCAUX_1_8V
            },
            .name = "VCCAUX_1.8V",
            .exponent = -10,
            .extra_mux_pin = 1,
            .vout_max = 15,
            .iout_max = 100,
            .vin_max = 15,
            .cmd[ePMBUS_CMD_OPERATION] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_ON_OFF_CONFIG] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_CLEAR_FAULTS] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_WRITE_PROTECT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STORE_DEFAULT_ALL] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_RESTORE_DEFAULT_ALL] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STORE_DEFAULT_CODE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_RESTORE_DEFAULT_CODE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_MODE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_TRIM] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_MARGIN_HIGH] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_MARGIN_LOW] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_SCALE_LOOP] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VIN_ON] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VIN_OFF] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_IOUT_CAL_GAIN] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_IOUT_CAL_OFFSET] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_OV_FAULT_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_OV_FAULT_RESPONSE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_UV_FAULT_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_UV_FAULT_RESPONSE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_IOUT_OC_FAULT_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_IOUT_OC_WARN_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_POWER_GOOD_ON] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_POWER_GOOD_OFF] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_TON_RISE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_BYTE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_WORD] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_VOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_IOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_TEMPERATURE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_CML] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_VIN] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_VOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_IOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_PMBUS_REVISION] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_MFR_VIN_MIN] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_MFR_VIN_MAX] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_00] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_04] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_05] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_06] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_07] = {
                .is_supported = CCL_TRUE,
                .size = 2
            }
        }
    },
    {
        .type_name = "pmbus", .sig = eDEVBRD_PMBUS_2_SIG, 
        .subtype_name = "vcco_1.2v",
        .spec.pmbus = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_1,
                .dev_i2ca = eDEVBRD_I2CA_VCCO_1_2V
            },
            .name = "VCCO_1.2V",
            .exponent = -10,
            .extra_mux_pin = 1,
            .vout_max = 15,
            .iout_max = 100,
            .vin_max = 15,
            .cmd[ePMBUS_CMD_OPERATION] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_ON_OFF_CONFIG] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_CLEAR_FAULTS] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_WRITE_PROTECT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STORE_DEFAULT_ALL] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_RESTORE_DEFAULT_ALL] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STORE_DEFAULT_CODE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_RESTORE_DEFAULT_CODE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_MODE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_TRIM] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_MARGIN_HIGH] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_MARGIN_LOW] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_SCALE_LOOP] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VIN_ON] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VIN_OFF] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_IOUT_CAL_GAIN] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_IOUT_CAL_OFFSET] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_OV_FAULT_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_OV_FAULT_RESPONSE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_UV_FAULT_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_UV_FAULT_RESPONSE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_IOUT_OC_FAULT_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_IOUT_OC_WARN_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_POWER_GOOD_ON] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_POWER_GOOD_OFF] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_TON_RISE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_BYTE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_WORD] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_VOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_IOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_TEMPERATURE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_CML] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_VIN] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_VOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_IOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_PMBUS_REVISION] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_MFR_VIN_MIN] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_MFR_VIN_MAX] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_00] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_04] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_05] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_06] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_07] = {
                .is_supported = CCL_TRUE,
                .size = 2
            }
        }
    },
    {
        .type_name = "pmbus", .sig = eDEVBRD_PMBUS_3_SIG, 
        .subtype_name = "vcco_3.3v",
        .spec.pmbus = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_1,
                .dev_i2ca = eDEVBRD_I2CA_VCCO_3_3V
            },
            .name = "VCCO_3.3V",
            .exponent = -10,
            .extra_mux_pin = 1,
            .vout_max = 15,
            .iout_max = 100,
            .vin_max = 15,
            .cmd[ePMBUS_CMD_OPERATION] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_ON_OFF_CONFIG] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_CLEAR_FAULTS] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_WRITE_PROTECT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STORE_DEFAULT_ALL] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_RESTORE_DEFAULT_ALL] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STORE_DEFAULT_CODE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_RESTORE_DEFAULT_CODE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_MODE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_TRIM] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_MARGIN_HIGH] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_MARGIN_LOW] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_SCALE_LOOP] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VIN_ON] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VIN_OFF] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_IOUT_CAL_GAIN] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_IOUT_CAL_OFFSET] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_OV_FAULT_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_OV_FAULT_RESPONSE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_UV_FAULT_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_UV_FAULT_RESPONSE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_IOUT_OC_FAULT_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_IOUT_OC_WARN_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_POWER_GOOD_ON] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_POWER_GOOD_OFF] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_TON_RISE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_BYTE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_WORD] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_VOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_IOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_TEMPERATURE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_CML] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_VIN] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_VOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_IOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_PMBUS_REVISION] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_MFR_VIN_MIN] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_MFR_VIN_MAX] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_00] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_04] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_05] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_06] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_07] = {
                .is_supported = CCL_TRUE,
                .size = 2
            }
        }
    },
    {
        .type_name = "pmbus", .sig = eDEVBRD_PMBUS_4_SIG, 
        .subtype_name = "vcco_2.5v",
        .spec.pmbus = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_1,
                .dev_i2ca = eDEVBRD_I2CA_VCCO_2_5V
            },
            .name = "VCCO_2.5V",
            .exponent = -10,
            .extra_mux_pin = 1,
            .vout_max = 15,
            .iout_max = 100,
            .vin_max = 15,
            .cmd[ePMBUS_CMD_OPERATION] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_ON_OFF_CONFIG] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_CLEAR_FAULTS] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_WRITE_PROTECT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STORE_DEFAULT_ALL] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_RESTORE_DEFAULT_ALL] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STORE_DEFAULT_CODE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_RESTORE_DEFAULT_CODE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_MODE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_TRIM] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_MARGIN_HIGH] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_MARGIN_LOW] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_SCALE_LOOP] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VIN_ON] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VIN_OFF] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_IOUT_CAL_GAIN] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_IOUT_CAL_OFFSET] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_OV_FAULT_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_OV_FAULT_RESPONSE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_UV_FAULT_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_UV_FAULT_RESPONSE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_IOUT_OC_FAULT_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_IOUT_OC_WARN_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_POWER_GOOD_ON] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_POWER_GOOD_OFF] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_TON_RISE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_BYTE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_WORD] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_VOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_IOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_TEMPERATURE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_CML] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_VIN] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_VOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_IOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_PMBUS_REVISION] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_MFR_VIN_MIN] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_MFR_VIN_MAX] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_00] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_04] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_05] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_06] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_07] = {
                .is_supported = CCL_TRUE,
                .size = 2
            }
        }
    },
    {
        .type_name = "pmbus", .sig = eDEVBRD_PMBUS_5_SIG, 
        .subtype_name = "mgtavcc_0.9v",
        .spec.pmbus = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_1,
                .dev_i2ca = eDEVBRD_I2CA_MGTAVCC_0_9V
            },
            .name = "MGTAVCC_0.9V",
            .exponent = -10,
            .extra_mux_pin = 1,
            .vout_max = 15,
            .iout_max = 100,
            .vin_max = 15,
            .cmd[ePMBUS_CMD_OPERATION] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_ON_OFF_CONFIG] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_CLEAR_FAULTS] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_WRITE_PROTECT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STORE_DEFAULT_ALL] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_RESTORE_DEFAULT_ALL] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STORE_DEFAULT_CODE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_RESTORE_DEFAULT_CODE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_MODE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_TRIM] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_MARGIN_HIGH] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_MARGIN_LOW] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_SCALE_LOOP] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VIN_ON] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VIN_OFF] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_IOUT_CAL_GAIN] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_IOUT_CAL_OFFSET] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_OV_FAULT_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_OV_FAULT_RESPONSE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_UV_FAULT_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_UV_FAULT_RESPONSE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_IOUT_OC_FAULT_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_IOUT_OC_WARN_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_POWER_GOOD_ON] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_POWER_GOOD_OFF] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_TON_RISE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_BYTE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_WORD] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_VOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_IOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_TEMPERATURE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_CML] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_VIN] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_VOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_IOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_PMBUS_REVISION] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_MFR_VIN_MIN] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_MFR_VIN_MAX] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_00] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_04] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_05] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_06] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_07] = {
                .is_supported = CCL_TRUE,
                .size = 2
            }
        }
    },
    {
        .type_name = "pmbus", .sig = eDEVBRD_PMBUS_6_SIG, 
        .subtype_name = "1.0v",
        .spec.pmbus = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_1,
                .dev_i2ca = eDEVBRD_I2CA_1_0V
            },
            .name = "1.0V",
            .exponent = -10,
            .extra_mux_pin = 1,
            .vout_max = 15,
            .iout_max = 100,
            .vin_max = 15,
            .cmd[ePMBUS_CMD_OPERATION] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_ON_OFF_CONFIG] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_CLEAR_FAULTS] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_WRITE_PROTECT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STORE_DEFAULT_ALL] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_RESTORE_DEFAULT_ALL] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STORE_DEFAULT_CODE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_RESTORE_DEFAULT_CODE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_MODE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_TRIM] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_MARGIN_HIGH] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_MARGIN_LOW] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_SCALE_LOOP] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VIN_ON] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VIN_OFF] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_IOUT_CAL_GAIN] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_IOUT_CAL_OFFSET] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_OV_FAULT_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_OV_FAULT_RESPONSE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_UV_FAULT_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_UV_FAULT_RESPONSE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_IOUT_OC_FAULT_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_IOUT_OC_WARN_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_POWER_GOOD_ON] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_POWER_GOOD_OFF] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_TON_RISE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_BYTE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_WORD] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_VOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_IOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_TEMPERATURE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_CML] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_VIN] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_VOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_IOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_PMBUS_REVISION] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_MFR_VIN_MIN] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_MFR_VIN_MAX] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_00] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_04] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_05] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_06] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_07] = {
                .is_supported = CCL_TRUE,
                .size = 2
            }
        }
    },
    {
        .type_name = "pmbus", .sig = eDEVBRD_PMBUS_7_SIG, 
        .subtype_name = "mgtavtt_1.2v",
        .spec.pmbus = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_1,
                .dev_i2ca = eDEVBRD_I2CA_MGTAVTT_1_2V
            },
            .name = "MGTAVTT_1.2V",
            .exponent = -10,
            .extra_mux_pin = 1,
            .vout_max = 15,
            .iout_max = 100,
            .vin_max = 15,
            .cmd[ePMBUS_CMD_OPERATION] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_ON_OFF_CONFIG] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_CLEAR_FAULTS] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_WRITE_PROTECT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STORE_DEFAULT_ALL] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_RESTORE_DEFAULT_ALL] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STORE_DEFAULT_CODE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_RESTORE_DEFAULT_CODE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_MODE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_TRIM] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_MARGIN_HIGH] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_MARGIN_LOW] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_SCALE_LOOP] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VIN_ON] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VIN_OFF] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_IOUT_CAL_GAIN] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_IOUT_CAL_OFFSET] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_OV_FAULT_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_OV_FAULT_RESPONSE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_UV_FAULT_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_UV_FAULT_RESPONSE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_IOUT_OC_FAULT_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_IOUT_OC_WARN_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_POWER_GOOD_ON] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_POWER_GOOD_OFF] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_TON_RISE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_BYTE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_WORD] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_VOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_IOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_TEMPERATURE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_CML] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_VIN] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_VOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_IOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_PMBUS_REVISION] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_MFR_VIN_MIN] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_MFR_VIN_MAX] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_00] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_04] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_05] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_06] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_07] = {
                .is_supported = CCL_TRUE,
                .size = 2
            }
        }
    },
    {
        .type_name = "pmbus", .sig = eDEVBRD_PMBUS_8_SIG, 
        .subtype_name = "5v",
        .spec.pmbus = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_1,
                .dev_i2ca = eDEVBRD_I2CA_5V
            },
            .name = "5V",
            .exponent = -10,
            .extra_mux_pin = 1,
            .vout_max = 15,
            .iout_max = 100,
            .vin_max = 15,
            .cmd[ePMBUS_CMD_OPERATION] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_ON_OFF_CONFIG] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_CLEAR_FAULTS] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_WRITE_PROTECT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STORE_DEFAULT_ALL] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_RESTORE_DEFAULT_ALL] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STORE_DEFAULT_CODE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_RESTORE_DEFAULT_CODE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_MODE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_TRIM] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_MARGIN_HIGH] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_MARGIN_LOW] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_SCALE_LOOP] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VIN_ON] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VIN_OFF] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_IOUT_CAL_GAIN] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_IOUT_CAL_OFFSET] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_OV_FAULT_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_OV_FAULT_RESPONSE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_UV_FAULT_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_UV_FAULT_RESPONSE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_IOUT_OC_FAULT_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_IOUT_OC_WARN_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_POWER_GOOD_ON] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_POWER_GOOD_OFF] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_TON_RISE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_BYTE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_WORD] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_VOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_IOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_TEMPERATURE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_CML] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_VIN] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_VOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_IOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_PMBUS_REVISION] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_MFR_VIN_MIN] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_MFR_VIN_MAX] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_00] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_04] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_05] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_06] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_07] = {
                .is_supported = CCL_TRUE,
                .size = 2
            }
        }
    },
    {
        .type_name = "pmbus", .sig = eDEVBRD_PMBUS_9_SIG, 
        .subtype_name = "main_pwr_supply_1",
        .spec.pmbus = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_1,
                .dev_i2ca = eDEVBRD_I2CA_MAIN_PS_0
            },
            .name = "MAIN_PWR_SUPPLY_1",
            .exponent = -6,
            .extra_mux_pin = 1,
            .vout_max = 15,
            .iout_max = 100,
            .vin_max = 15,
            .cmd[ePMBUS_CMD_CAPABILITY] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_QUERY] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_MODE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_COEFFICIENTS] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_FAN_CONFIG_1_2] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_WORD] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_VOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_IOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_INPUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_TEMPERATURE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_FANS_1_2] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_EIN] = {
                .is_supported = CCL_TRUE,
                .size = 6
            },
            .cmd[ePMBUS_CMD_READ_EOUT] = { 
                .is_supported = CCL_TRUE,
                .size = 6
            },
            .cmd[ePMBUS_CMD_READ_VOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_IOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_TEMPERATURE_1] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_FAN_SPEED_1] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_FAN_SPEED_2] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_POUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_PIN] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_MFR_ID] = {
                .is_supported = CCL_TRUE,
                .size = 5
            },
            .cmd[ePMBUS_CMD_MFR_MODEL] = {
                .is_supported = CCL_TRUE,
                .size = 10 //TODO: check real size
            },
            .cmd[ePMBUS_CMD_MFR_IOUT_MAX] = {
                .is_supported = CCL_TRUE
            }
        }
    },
    {
        .type_name = "pmbus", .sig = eDEVBRD_PMBUS_10_SIG, 
        .subtype_name = "main_pwr_supply_2",
        .spec.pmbus = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_1,
                .dev_i2ca = eDEVBRD_I2CA_MAIN_PS_1
            },
            .name = "MAIN_PWR_SUPPLY_2",
            .exponent = -6,
            .extra_mux_pin = 1,
            .vout_max = 15,
            .iout_max = 100,
            .vin_max = 15,
            .cmd[ePMBUS_CMD_CAPABILITY] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_QUERY] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_MODE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_COEFFICIENTS] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_FAN_CONFIG_1_2] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_WORD] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_VOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_IOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_INPUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_TEMPERATURE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_FANS_1_2] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_EIN] = {
                .is_supported = CCL_TRUE,
                .size = 6
            },
            .cmd[ePMBUS_CMD_READ_EOUT] = { 
                .is_supported = CCL_TRUE,
                .size = 6
            },
            .cmd[ePMBUS_CMD_READ_VOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_IOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_TEMPERATURE_1] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_FAN_SPEED_1] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_FAN_SPEED_2] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_POUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_PIN] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_MFR_ID] = {
                .is_supported = CCL_TRUE,
                .size = 5
            },
            .cmd[ePMBUS_CMD_MFR_MODEL] = {
                .is_supported = CCL_TRUE,
                .size = 10 //TODO: check real size
            },
            .cmd[ePMBUS_CMD_MFR_IOUT_MAX] = {
                .is_supported = CCL_TRUE
            }
        }
    },
    {
        .type_name = "pmbus", .sig = eDEVBRD_PMBUS_CLIENT_0_SIG, 
        .subtype_name = "vcco_red_3.3v",
        .spec.pmbus = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_1,
                .dev_i2ca = eDEVBRD_I2CA_VCCO_CLIENT_3_3V
            },
            .name = "VCCO_CLIENT_3.3",
            .exponent = -10,
            .extra_mux_pin = 3,
            .vout_max = 15,
            .iout_max = 100,
            .vin_max = 15,
            .cmd[ePMBUS_CMD_OPERATION] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_ON_OFF_CONFIG] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_CLEAR_FAULTS] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_WRITE_PROTECT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STORE_DEFAULT_ALL] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_RESTORE_DEFAULT_ALL] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STORE_DEFAULT_CODE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_RESTORE_DEFAULT_CODE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_MODE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_TRIM] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_MARGIN_HIGH] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_MARGIN_LOW] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_SCALE_LOOP] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VIN_ON] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VIN_OFF] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_IOUT_CAL_GAIN] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_IOUT_CAL_OFFSET] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_OV_FAULT_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_OV_FAULT_RESPONSE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_UV_FAULT_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_UV_FAULT_RESPONSE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_IOUT_OC_FAULT_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_IOUT_OC_WARN_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_POWER_GOOD_ON] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_POWER_GOOD_OFF] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_TON_RISE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_BYTE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_WORD] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_VOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_IOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_TEMPERATURE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_CML] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_VIN] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_VOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_IOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_PMBUS_REVISION] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_MFR_VIN_MIN] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_MFR_VIN_MAX] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_00] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_04] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_05] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_06] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_07] = {
                .is_supported = CCL_TRUE,
                .size = 2
            }
        }
    }
};


static board_device_descr_t _eeprom_brd_descr[] = {
    {
        .type_name = "eeprom", .sig = eDEVBRD_MAIN_EEPROM_SIG, 
        .spec.eeprom = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_1,
                .dev_i2ca = eDEVBRD_I2CA_MAIN_EEPROM
            },
            .name = "MAIN_EEPROM",
            .extra_mux_pin = 0,
            .addr_width = 2,
            .size = 4096,
            .page_size = 32,
            .param = {
            	{0, 20, NULL, "Board Serial Number"},
            	{20, 8, NULL, "Assembly Number"},
            	{28, 4, NULL, "Hardware Revision"},
            	{32, 4, NULL, "Hardware Subrevision"},
            	{36, 15, NULL, "Part Number"},
            	{51, 15, NULL, "CLEI"},
            	{66, 10, NULL, "Manufacturing Date DD/MM/YYYY"},
                {76, 4, NULL, "License value"},
            	{80, 4, NULL, "Device Type ID"}
            }
        }
    },
    {
        .type_name = "eeprom", .sig = eDEVBRD_COMX0_EEPROM_SIG, 
        .spec.eeprom = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_1,
                .dev_i2ca = eDEVBRD_I2CA_COMX0_EEPROM
            },
            .name = "COMX0_EEPROM",
            .extra_mux_pin = 0,
            .addr_width = 2,
            .size = 4096,
            .page_size = 32,
            .param = {
                {0, 20, NULL, "Board Serial Number"},
                {20, 8, NULL, "Assembly Number"},
                {28, 4, NULL, "Hardware Revision"},
                {32, 4, NULL, "Hardware Subrevision"},
                {36, 15, NULL, "Part Number"},
                {51, 15, NULL, "CLEI"},
                {66, 10, NULL, "Manufacturing Date DD/MM/YYYY"},
                {76, 4, NULL, "License value"},
                {80, 4, NULL, "Device Type ID"}
            }
        }
    },
    {
        .type_name = "eeprom", .sig = eDEVBRD_COMX1_EEPROM_SIG, 
        .spec.eeprom = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_1,
                .dev_i2ca = eDEVBRD_I2CA_COMX1_EEPROM
            },
            .name = "COMX1_EEPROM",
            .extra_mux_pin = 0,
            .addr_width = 2,
            .size = 4096,
            .page_size = 32,
            .param = {
                {0, 20, NULL, "Board Serial Number"},
                {20, 8, NULL, "Assembly Number"},
                {28, 4, NULL, "Hardware Revision"},
                {32, 4, NULL, "Hardware Subrevision"},
                {36, 15, NULL, "Part Number"},
                {51, 15, NULL, "CLEI"},
                {66, 10, NULL, "Manufacturing Date DD/MM/YYYY"},
                {76, 4, NULL, "License value"},
                {80, 4, NULL, "Device Type ID"}
            }
        }
    },
    {
        .type_name = "eeprom", .sig = eDEVBRD_COMX2_EEPROM_SIG, 
        .spec.eeprom = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_1,
                .dev_i2ca = eDEVBRD_I2CA_COMX2_EEPROM
            },
            .name = "COMX2_EEPROM",
            .extra_mux_pin = 0,
            .addr_width = 2,
            .size = 4096,
            .page_size = 32,
            .param = {
                {0, 20, NULL, "Board Serial Number"},
                {20, 8, NULL, "Assembly Number"},
                {28, 4, NULL, "Hardware Revision"},
                {32, 4, NULL, "Hardware Subrevision"},
                {36, 15, NULL, "Part Number"},
                {51, 15, NULL, "CLEI"},
                {66, 10, NULL, "Manufacturing Date DD/MM/YYYY"},
                {76, 4, NULL, "License value"},
                {80, 4, NULL, "Device Type ID"}
            }
        }
    },
    {
        .type_name = "eeprom", .sig = eDEVBRD_COMX3_EEPROM_SIG, 
        .spec.eeprom = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_1,
                .dev_i2ca = eDEVBRD_I2CA_COMX3_EEPROM
            },
            .name = "COMX3_EEPROM",
            .extra_mux_pin = 0,
            .addr_width = 2,
            .size = 4096,
            .page_size = 32,
            .param = {
                {0, 20, NULL, "Board Serial Number"},
                {20, 8, NULL, "Assembly Number"},
                {28, 4, NULL, "Hardware Revision"},
                {32, 4, NULL, "Hardware Subrevision"},
                {36, 15, NULL, "Part Number"},
                {51, 15, NULL, "CLEI"},
                {66, 10, NULL, "Manufacturing Date DD/MM/YYYY"},
                {76, 4, NULL, "License value"},
                {80, 4, NULL, "Device Type ID"}
            }
        }
    },
    {
        .type_name = "eeprom", .sig = eDEVBRD_ATP_0_EEPROM_SIG, 
        .spec.eeprom = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_0,
                .dev_i2ca = eDEVBRD_I2CA_ATP_EEPROM1
            },
            .name = "ATP_EEPROM_1",
            .extra_mux_pin = 4,
            .addr_width = 2,
            .size = 4096,
            .page_size = 32,
            .param = {
                {0, 20, NULL, "Board Serial Number"},
                {20, 8, NULL, "Assembly Number"},
                {28, 4, NULL, "Hardware Revision"},
                {32, 4, NULL, "Hardware Subrevision"},
                {36, 15, NULL, "Part Number"},
                {51, 15, NULL, "CLEI"},
                {66, 10, NULL, "Manufacturing Date DD/MM/YYYY"},
                {76, 4, NULL, "License value"},
                {80, 4, NULL, "Device Type ID"}
            }
        }
    },
    {
        .type_name = "eeprom", .sig = eDEVBRD_ATP_1_EEPROM_SIG, 
        .spec.eeprom = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_0,
                .dev_i2ca = eDEVBRD_I2CA_ATP_EEPROM2
            },
            .name = "ATP_EEPROM_2",
            .extra_mux_pin = 5,
            .addr_width = 2,
            .size = 4096,
            .page_size = 32,
            .param = {
                {0, 20, NULL, "Board Serial Number"},
                {20, 8, NULL, "Assembly Number"},
                {28, 4, NULL, "Hardware Revision"},
                {32, 4, NULL, "Hardware Subrevision"},
                {36, 15, NULL, "Part Number"},
                {51, 15, NULL, "CLEI"},
                {66, 10, NULL, "Manufacturing Date DD/MM/YYYY"},
                {76, 4, NULL, "License value"},
                {80, 4, NULL, "Device Type ID"}
            }
        }
    },
    {
        .type_name = "eeprom", .sig = eDEVBRD_SODIMM_EEPROM_1_SIG, 
        .spec.eeprom = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_4,
                .dev_i2ca = eDEVBRD_I2CA_SODIMM_EEPROM1
            },
            .name = "SODIMM_EEPROM_1",
            .extra_mux_pin = 0,
            .addr_width = 1,
            .size = 512,
            .page_size = 32,
            .is_spd = CCL_TRUE,
            .spd_ctrl = {
                .sba0 = 0x6c >> 1,
                .sba1 = 0x6e >> 1,
                .rba = 0x6c >> 1,
                .bsize = 256,
                .ndummy = 2
            },
            .param = {
                {0, 20, NULL, "Board Serial Number"},
                {20, 8, NULL, "Assembly Number"},
                {28, 4, NULL, "Hardware Revision"},
                {32, 4, NULL, "Hardware Subrevision"},
                {36, 15, NULL, "Part Number"},
                {51, 15, NULL, "CLEI"},
                {66, 10, NULL, "Manufacturing Date DD/MM/YYYY"},
                {76, 4, NULL, "License value"},
                {80, 4, NULL, "Device Type ID"}
            }
        }
    },
    {
        .type_name = "eeprom", .sig = eDEVBRD_SODIMM_EEPROM_2_SIG, 
        .spec.eeprom = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_4,
                .dev_i2ca = eDEVBRD_I2CA_SODIMM_EEPROM2
            },
            .name = "SODIMM_EEPROM_2",
            .extra_mux_pin = 0,
            .addr_width = 1,
            .size = 512,
            .page_size = 32,
            .is_spd = CCL_TRUE,
            .spd_ctrl = {
                .sba0 = 0x6c >> 1,
                .sba1 = 0x6e >> 1,
                .rba = 0x6c >> 1,
                .bsize = 256,
                .ndummy = 2
            },
            .param = {
                {0, 20, NULL, "Board Serial Number"},
                {20, 8, NULL, "Assembly Number"},
                {28, 4, NULL, "Hardware Revision"},
                {32, 4, NULL, "Hardware Subrevision"},
                {36, 15, NULL, "Part Number"},
                {51, 15, NULL, "CLEI"},
                {66, 10, NULL, "Manufacturing Date DD/MM/YYYY"},
                {76, 4, NULL, "License value"},
                {80, 4, NULL, "Device Type ID"}
            }
        },
    },
    {
        .type_name = "eeprom", .sig = eDEVBRD_SODIMM_EEPROM_3_SIG, 
        .spec.eeprom = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_4,
                .dev_i2ca = eDEVBRD_I2CA_SODIMM_EEPROM3
            },
            .name = "SODIMM_EEPROM_3",
            .extra_mux_pin = 0,
            .addr_width = 1,
            .size = 512,
            .page_size = 32,
            .is_spd = CCL_TRUE,
            .spd_ctrl = {
                .sba0 = 0x6c >> 1,
                .sba1 = 0x6e >> 1,
                .rba = 0x6c >> 1,
                .bsize = 256,
                .ndummy = 2
            },
            .param = {
                {0, 20, NULL, "Board Serial Number"},
                {20, 8, NULL, "Assembly Number"},
                {28, 4, NULL, "Hardware Revision"},
                {32, 4, NULL, "Hardware Subrevision"},
                {36, 15, NULL, "Part Number"},
                {51, 15, NULL, "CLEI"},
                {66, 10, NULL, "Manufacturing Date DD/MM/YYYY"},
                {76, 4, NULL, "License value"},
                {80, 4, NULL, "Device Type ID"}
            }
        }
    }
};

/* rtc board device description */
static board_device_descr_t _rtc_brd_descr[] = {
    {
        .type_name = "rtc", .sig = eDEVBRD_MAIN_RTC_SIG, 
        .spec.rtc = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_1,
                .dev_i2ca = eDEVBRD_I2CA_MAIN_RTC
            },
            .extra_mux_pin = 0,
            .addr_width = 1,
            .name = "MAIN_RTC"
        }
    },
    {
        .type_name = "rtc", .sig = eDEVBRD_ATP_0_RTC_SIG, 
        .spec.rtc = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_0,
                .dev_i2ca = eDEVBRD_I2CA_ATP_RTC1
            },
            .addr_width = 1,
            .extra_mux_pin = 2,
            .name = "ATP_RTC_1"
        }
    },
    {
        .type_name = "rtc", .sig = eDEVBRD_ATP_1_RTC_SIG, 
        .spec.rtc = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_0,
                .dev_i2ca = eDEVBRD_I2CA_ATP_RTC2
            },
            .addr_width = 1,
            .extra_mux_pin = 3,
            .name = "ATP_RTC_2"
        }
    }

};

/* lcd board device description */
static board_device_descr_t _lcd_brd_descr[] = {
    {
        .type_name = "lcd", .sig = eDEVBRD_LCD_SIG, 
        .spec.lcd = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_1,
                .dev_i2ca = eDEVBRD_I2CA_LCD
            },
            .extra_mux_pin = 0,
        }
    },
};

static board_device_descr_t _currmon_brd_descr[] = {
    {
        .type_name = "currmon", .sig = eDEVBRD_COMX0_CURRMON_SIG, 
        .spec.currmon = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_1,
                .dev_i2ca = eDEVBRD_I2CA_COMX0_CURRMON
            },
            .current_max = 10,
            .rshunt = 0.03,
            .current_lsb = 0.001,
            .extra_mux_pin = 0,
            .name = "COMX0_CURRMON"
        }
    },
    {
        .type_name = "currmon", .sig = eDEVBRD_COMX1_CURRMON_SIG, 
        .spec.currmon = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_1,
                .dev_i2ca = eDEVBRD_I2CA_COMX1_CURRMON
            },
            .current_max = 10,
            .rshunt = 0.03,
            .current_lsb = 0.001,
            .extra_mux_pin = 0,
            .name = "COMX1_CURRMON"
        }
    },
    {
        .type_name = "currmon", .sig = eDEVBRD_COMX2_CURRMON_SIG, 
        .spec.currmon = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_1,
                .dev_i2ca = eDEVBRD_I2CA_COMX2_CURRMON
            },
            .current_max = 10,
            .rshunt = 0.03,
            .current_lsb = 0.001,
            .extra_mux_pin = 0,
            .name = "COMX2_CURRMON"
        }
    },
    {
        .type_name = "currmon", .sig = eDEVBRD_COMX3_CURRMON_SIG, 
        .spec.currmon = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_1,
                .dev_i2ca = eDEVBRD_I2CA_COMX3_CURRMON
            },
            .current_max = 10,
            .rshunt = 0.03,
            .current_lsb = 0.001,
            .extra_mux_pin = 0,
            .name = "COMX3_CURRMON"
        }
    },
    {
        .type_name = "currmon", .sig = eDEVBRD_SMARC_MAIN_CURRMON_SIG, 
        .spec.currmon = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_1,
                .dev_i2ca = eDEVBRD_I2CA_SMARC_MAIN_CURRMON
            },
            .current_max = 10,
            .rshunt = 0.03,
            .current_lsb = 0.001,
            .extra_mux_pin = 0,
            .name = "SMARC_MAIN"
        }
    },
    {
        .type_name = "currmon", .sig = eDEVBRD_SMARC_CTRL_CURRMON_SIG,
        .spec.currmon = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_1,
                .dev_i2ca = eDEVBRD_I2CA_SMARC_CTRL_CURRMON
            },
            .current_max = 10,
            .rshunt = 0.03,
            .current_lsb = 0.001,
            .extra_mux_pin = 0,
            .name = "SMARC_CTRL"
        }
    }
}; 

static board_device_descr_t _i2cio_brd_descr[] = {
    /* max7312 board devices description */
    {
        .type_name = "i2cio", .sig = eDEVBRD_I2CIO_0_SIG, 
        .spec.i2cio = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_0,
                .dev_i2ca = eDEVBRD_I2CA_ATP_I2CIO1
            },
            .addr_width = 1,
            .extra_mux_pin = 6,
            .name = "ATP_I2CIO_1"
        }
    },
    {
        .type_name = "i2cio", .sig = eDEVBRD_I2CIO_1_SIG, 
        .spec.i2cio = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_0,
                .dev_i2ca = eDEVBRD_I2CA_ATP_I2CIO2
            },
            .addr_width = 1,
            .extra_mux_pin = 6,
            .name = "ATP_I2CIO_2"
        }
    },
    {
        /* NOT Populated */
        .type_name = "i2cio", .sig = eDEVBRD_I2CIO_2_SIG, 
        .spec.i2cio = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_0,
                .dev_i2ca = eDEVBRD_I2CA_ATP_LB1
            },
            .addr_width = 1,
            .extra_mux_pin = 2,
            .name = "ATP_I2CIO_LB_1"
        }
    },
    {
        /* NOT Populated */
        .type_name = "i2cio", .sig = eDEVBRD_I2CIO_3_SIG, 
        .spec.i2cio = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_0,
                .dev_i2ca = eDEVBRD_I2CA_ATP_LB2
            },
            .addr_width = 1,
            .extra_mux_pin = 1,
            .name = "ATP_I2CIO_LB_2"
        }
    },
    {
        /* NOT Populated */
        .type_name = "i2cio", .sig = eDEVBRD_I2CIO_4_SIG, 
        .spec.i2cio = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_0,
                .dev_i2ca = eDEVBRD_I2CA_I2CIO_0
            },
            .addr_width = 1,
            .extra_mux_pin = 16,
            .name = "ATP_I2CIO_0"
        }
    },
    {
        /* NOT Populated */
        .type_name = "i2cio", .sig = eDEVBRD_I2CIO_5_SIG, 
        .spec.i2cio = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_0,
                .dev_i2ca = eDEVBRD_I2CA_I2CIO_0
            },
            .addr_width = 1,
            .extra_mux_pin = 17,
            .name = "ATP_I2CIO_1"
        }
    },
    {
        /* NOT Populated */
        .type_name = "i2cio", .sig = eDEVBRD_I2CIO_6_SIG, 
        .spec.i2cio = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_1,
                .dev_i2ca = eDEVBRD_I2CA_I2CIO_0
            },
            .addr_width = 1,
            .extra_mux_pin = 14,
            .name = "ATP_I2CIO_2"
        }
    },
    {
        /* NOT Populated */
        .type_name = "i2cio", .sig = eDEVBRD_I2CIO_7_SIG, 
        .spec.i2cio = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_1,
                .dev_i2ca = eDEVBRD_I2CA_I2CIO_0
            },
            .addr_width = 1,
            .extra_mux_pin = 15,
            .name = "ATP_I2CIO_3"
        }
    },
    {
        /* NOT Populated */
        .type_name = "i2cio", .sig = eDEVBRD_I2CIO_8_SIG, 
        .spec.i2cio = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_1,
                .dev_i2ca = eDEVBRD_I2CA_I2CIO_0
            },
            .addr_width = 1,
            .extra_mux_pin = 16,
            .name = "ATP_I2CIO_4"
        }
    },
    {
        /* NOT Populated */
        .type_name = "i2cio", .sig = eDEVBRD_I2CIO_8_SIG, 
        .spec.i2cio = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_1,
                .dev_i2ca = eDEVBRD_I2CA_I2CIO_0
            },
            .addr_width = 1,
            .extra_mux_pin = 17,
            .name = "ATP_I2CIO_5"
        }
    },
};

/* lcd board device description */
static board_device_descr_t _fan_brd_descr[] = {
    {
        .type_name = "fan", .sig = eDEVBRD_FAN_SIG, 
        .spec.fan = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_1,
                .dev_i2ca = eDEVBRD_I2CA_FAN_CTL
            },
            .pulse_num = 2,
            .extra_mux_pin = 3,
            .name = "FAN_CTRL"
        }
    },
};

/* spibus board device description */
static board_device_descr_t _spibus_brd_descr[] = {
    {
        .type_name = "spibus", .sig = eDEVBRD_CPLD_SIG, 
        .spec.spibus = {
            .spi_bus_id = 0,
            .spi_cs_id = 0
        }
    },
};

/* portmap board device description */
static board_device_descr_t _portmap_brd_descr[] = {
    {
        .type_name = "portmap", .sig = eDEVBRD_PORTMAP_SIG, 
        .spec.portmap =
        {
            .ports_num = BRDDEV_PORTS_NUM, 
            .port_types = 
            { 
                eDEVMAN_PORT_TYPE_MEDIA_SLOT, eDEVMAN_PORT_TYPE_MEDIA_SLOT,
                eDEVMAN_PORT_TYPE_MEDIA_SLOT, eDEVMAN_PORT_TYPE_MEDIA_SLOT, 
                eDEVMAN_PORT_TYPE_MEDIA_SLOT, eDEVMAN_PORT_TYPE_MEDIA_SLOT,
                eDEVMAN_PORT_TYPE_MEDIA_SLOT, eDEVMAN_PORT_TYPE_MEDIA_SLOT, 
                eDEVMAN_PORT_TYPE_MEDIA_SLOT, eDEVMAN_PORT_TYPE_MEDIA_SLOT,
                eDEVMAN_PORT_TYPE_MEDIA_SLOT, eDEVMAN_PORT_TYPE_MEDIA_SLOT,
                eDEVMAN_PORT_TYPE_MEDIA_SLOT, eDEVMAN_PORT_TYPE_MEDIA_SLOT,
                eDEVMAN_PORT_TYPE_MEDIA_SLOT, eDEVMAN_PORT_TYPE_MEDIA_SLOT,
                eDEVMAN_PORT_TYPE_MEDIA_SLOT, eDEVMAN_PORT_TYPE_MEDIA_SLOT,
                eDEVMAN_PORT_TYPE_MEDIA_SLOT, eDEVMAN_PORT_TYPE_MEDIA_SLOT,
                eDEVMAN_PORT_TYPE_MEDIA_SLOT, eDEVMAN_PORT_TYPE_MEDIA_SLOT,
                eDEVMAN_PORT_TYPE_MEDIA_SLOT, eDEVMAN_PORT_TYPE_MEDIA_SLOT,
                eDEVMAN_PORT_TYPE_MEDIA_SLOT, eDEVMAN_PORT_TYPE_MEDIA_SLOT,
                eDEVMAN_PORT_TYPE_MEDIA_SLOT, eDEVMAN_PORT_TYPE_MEDIA_SLOT,
                eDEVMAN_PORT_TYPE_INTERNAL,   eDEVMAN_PORT_TYPE_INTERNAL,                  
                eDEVMAN_PORT_TYPE_INTERNAL,   eDEVMAN_PORT_TYPE_INTERNAL,                  
                eDEVMAN_PORT_TYPE_INTERNAL,   eDEVMAN_PORT_TYPE_INTERNAL,                  
                eDEVMAN_PORT_TYPE_INTERNAL,   eDEVMAN_PORT_TYPE_INTERNAL,                  
                eDEVMAN_PORT_TYPE_INTERNAL,   eDEVMAN_PORT_TYPE_INTERNAL,                  
                eDEVMAN_PORT_TYPE_INTERNAL,   eDEVMAN_PORT_TYPE_INTERNAL,                  
                eDEVMAN_PORT_TYPE_INTERNAL,   eDEVMAN_PORT_TYPE_INTERNAL,                  
                eDEVMAN_PORT_TYPE_INTERNAL,   eDEVMAN_PORT_TYPE_INTERNAL,                  
                eDEVMAN_PORT_TYPE_INTERNAL,   eDEVMAN_PORT_TYPE_INTERNAL,                  
                eDEVMAN_PORT_TYPE_INTERNAL,   eDEVMAN_PORT_TYPE_INTERNAL,                  
                eDEVMAN_PORT_TYPE_INTERNAL,   eDEVMAN_PORT_TYPE_INTERNAL,                  
                eDEVMAN_PORT_TYPE_INTERNAL,   eDEVMAN_PORT_TYPE_INTERNAL,                  
                eDEVMAN_PORT_TYPE_INTERNAL,   eDEVMAN_PORT_TYPE_INTERNAL,                  
                eDEVMAN_PORT_TYPE_INTERNAL,   eDEVMAN_PORT_TYPE_INTERNAL,                  
                eDEVMAN_PORT_TYPE_INTERNAL,   eDEVMAN_PORT_TYPE_INTERNAL,                  
            },
            .port_id = 
            { 
                eDEVBRD_10G_NETWORK_0,        eDEVBRD_10G_NETWORK_1,                                              
                eDEVBRD_10G_NETWORK_2,        eDEVBRD_10G_NETWORK_3,                                              
                eDEVBRD_10G_NETWORK_4,        eDEVBRD_10G_NETWORK_5,                                              
                eDEVBRD_10G_NETWORK_6,        eDEVBRD_10G_NETWORK_7,                                              
                eDEVBRD_10G_NETWORK_8,        eDEVBRD_10G_NETWORK_9,                                              
                eDEVBRD_10G_NETWORK_10,       eDEVBRD_10G_NETWORK_11,                                             
                eDEVBRD_10G_CLIENT_0,         eDEVBRD_10G_CLIENT_1,                                               
                eDEVBRD_10G_CLIENT_2,         eDEVBRD_10G_CLIENT_3,                                               
                eDEVBRD_10G_CLIENT_4,         eDEVBRD_10G_CLIENT_5,                                               
                eDEVBRD_10G_CLIENT_6,         eDEVBRD_10G_CLIENT_7,                                               
                eDEVBRD_10G_CLIENT_8,         eDEVBRD_10G_CLIENT_9,                                               
                eDEVBRD_10G_CLIENT_10,        eDEVBRD_10G_CLIENT_11,                                              
                eDEVBRD_100G_NETWORK_0,       eDEVBRD_100G_NETWORK_1,                                             
                eDEVBRD_100G_CLIENT_0,        eDEVBRD_100G_CLIENT_1,                                              
                eDEVBRD_KR_COMX0_0,           eDEVBRD_KR_COMX0_1,          /* KR Virtex <-> COMX0 0-1*/           
                eDEVBRD_KR_COMX0_2,           eDEVBRD_KR_COMX0_3,          /* KR Virtex <-> COMX0 2-3*/           
                eDEVBRD_KR_COMX1_0,           eDEVBRD_KR_COMX1_1,          /* KR Virtex <-> COMX0 0-1*/           
                eDEVBRD_KR_COMX1_2,           eDEVBRD_KR_COMX1_3,          /* KR Virtex <-> COMX0 2-3*/           
                eDEVBRD_KR_COMX2_0,           eDEVBRD_KR_COMX2_1,          /* KR Virtex <-> COMX0 0-1*/           
                eDEVBRD_KR_COMX2_2,           eDEVBRD_KR_COMX2_3,          /* KR Virtex <-> COMX0 2-3*/           
                eDEVBRD_KR_COMX3_0,           eDEVBRD_KR_COMX3_1,          /* KR Virtex <-> COMX0 0-1*/           
                eDEVBRD_KR_COMX3_2,           eDEVBRD_KR_COMX3_3,          /* KR Virtex <-> COMX0 2-3*/           
                eDEVBRD_1G_AV_INTERCHIP_0,    eDEVBRD_1G_AV_INTERCHIP_1,   /* Artix -> Virtex Interchip 0,1 */    
                eDEVBRD_1G_MAIN_CPU_0,        eDEVBRD_1G_MAIN_CPU_1,       /* 1G Main CPU <-> Artix 0-1 */        
                eDEVBRD_1G_CTRL_CPU_0,        eDEVBRD_1G_CTRL_CPU_1,       /* 1G Control CPU <-> Artix 0-1 */          
                eDEVBRD_1G_COMX0,             eDEVBRD_1G_COMX1,            /* 1G COMX0,1 <-> Artix */             
                eDEVBRD_1G_COMX2,             eDEVBRD_1G_COMX3,            /* 1G COMX2,3 <-> Artix */             
                eDEVBRD_1G_ATP,               eDEVBRD_1G_OOB,              /* 1G ATP, OOB */                      
                eDEVBRD_1G_VA_INTERCHIP_0,    eDEVBRD_1G_VA_INTERCHIP_1,   /* Virtex -> Artix Interchip 0,1 */          
            },                                                                                                    
            .trans_id =                                                                                           
            {                                                                                                     
                eDEVBRD_10G_NETWORK_0,        eDEVBRD_10G_NETWORK_1,                                              
                eDEVBRD_10G_NETWORK_2,        eDEVBRD_10G_NETWORK_3,                                              
                eDEVBRD_10G_NETWORK_4,        eDEVBRD_10G_NETWORK_5,                                              
                eDEVBRD_10G_NETWORK_6,        eDEVBRD_10G_NETWORK_7,                                              
                eDEVBRD_10G_NETWORK_8,        eDEVBRD_10G_NETWORK_9,                                              
                eDEVBRD_10G_NETWORK_10,       eDEVBRD_10G_NETWORK_11,                                             
                eDEVBRD_10G_CLIENT_0,         eDEVBRD_10G_CLIENT_1,                                               
                eDEVBRD_10G_CLIENT_2,         eDEVBRD_10G_CLIENT_3,                                               
                eDEVBRD_10G_CLIENT_4,         eDEVBRD_10G_CLIENT_5,                                               
                eDEVBRD_10G_CLIENT_6,         eDEVBRD_10G_CLIENT_7,                                               
                eDEVBRD_10G_CLIENT_8,         eDEVBRD_10G_CLIENT_9,                                               
                eDEVBRD_10G_CLIENT_10,        eDEVBRD_10G_CLIENT_11,                                              
                eDEVBRD_100G_NETWORK_0,       eDEVBRD_100G_NETWORK_1,                                             
                eDEVBRD_100G_CLIENT_0,        eDEVBRD_100G_CLIENT_1,                                              
                DEVBRD_PORT_TRANS_NONE,       DEVBRD_PORT_TRANS_NONE, /* KR Virtex <-> COMX0 0-1*/                
                DEVBRD_PORT_TRANS_NONE,       DEVBRD_PORT_TRANS_NONE, /* KR Virtex <-> COMX0 2-3*/                
                DEVBRD_PORT_TRANS_NONE,       DEVBRD_PORT_TRANS_NONE, /* KR Virtex <-> COMX1 0-1*/                
                DEVBRD_PORT_TRANS_NONE,       DEVBRD_PORT_TRANS_NONE, /* KR Virtex <-> COMX1 2-3*/                
                DEVBRD_PORT_TRANS_NONE,       DEVBRD_PORT_TRANS_NONE, /* KR Virtex <-> COMX2 0-1*/                
                DEVBRD_PORT_TRANS_NONE,       DEVBRD_PORT_TRANS_NONE, /* KR Virtex <-> COMX2 2-3*/                
                DEVBRD_PORT_TRANS_NONE,       DEVBRD_PORT_TRANS_NONE, /* KR Virtex <-> COMX3 0-1*/                
                DEVBRD_PORT_TRANS_NONE,       DEVBRD_PORT_TRANS_NONE, /* KR Virtex <-> COMX3 2-3*/                
                DEVBRD_PORT_TRANS_NONE,       DEVBRD_PORT_TRANS_NONE, /* Artix -> Virtex Interchip 0,1 */         
                DEVBRD_PORT_TRANS_NONE,       DEVBRD_PORT_TRANS_NONE, /* 1G Main CPU <-> Artix 0-1 */             
                DEVBRD_PORT_TRANS_NONE,       DEVBRD_PORT_TRANS_NONE, /* 1G Control CPU <-> Artix 0-1 */          
                DEVBRD_PORT_TRANS_NONE,                               /* 1G COMX0 <-> Artix */                    
                DEVBRD_PORT_TRANS_NONE,                               /* 1G COMX1 <-> Artix */                    
                DEVBRD_PORT_TRANS_NONE,                               /* 1G COMX2 <-> Artix */                    
                DEVBRD_PORT_TRANS_NONE,                               /* 1G COMX3 <-> Artix */                    
                DEVBRD_PORT_TRANS_NONE,                               /* 1G OOB */                                
                DEVBRD_PORT_TRANS_NONE,                               /* 1G ATP */                                
                DEVBRD_PORT_TRANS_NONE,       DEVBRD_PORT_TRANS_NONE, /* Virtex -> Artix Interchip 0,1 */         
            },
            .name = 
            {
                "PORT_10G_0_NETWORK",         
                "PORT_10G_1_NETWORK",         
                "PORT_10G_2_NETWORK",         
                "PORT_10G_3_NETWORK",         
                "PORT_10G_4_NETWORK",         
                "PORT_10G_5_NETWORK",         
                "PORT_10G_6_NETWORK",         
                "PORT_10G_7_NETWORK",         
                "PORT_10G_8_NETWORK",         
                "PORT_10G_9_NETWORK",         
                "PORT_10G_10_NETWORK",        
                "PORT_10G_11_NETWORK",        
                "PORT_10G_0_CLIENT",          
                "PORT_10G_1_CLIENT",          
                "PORT_10G_2_CLIENT",          
                "PORT_10G_3_CLIENT",          
                "PORT_10G_4_CLIENT",          
                "PORT_10G_5_CLIENT",          
                "PORT_10G_6_CLIENT",          
                "PORT_10G_7_CLIENT",          
                "PORT_10G_8_CLIENT",          
                "PORT_10G_9_CLIENT",          
                "PORT_10G_10_CLIENT",         
                "PORT_10G_11_CLIENT",         
                "PORT_100G_0_NETWORK",        
                "PORT_100G_1_NETWORK",        
                "PORT_100G_0_CLIENT",         
                "PORT_100G_1_CLIENT",         
                "PORT_KR_COMX0_0",            
                "PORT_KR_COMX0_1",            
                "PORT_KR_COMX0_2",            
                "PORT_KR_COMX0_3",            
                "PORT_KR_COMX1_0",            
                "PORT_KR_COMX1_1",            
                "PORT_KR_COMX1_2",            
                "PORT_KR_COMX1_3",            
                "PORT_KR_COMX2_0",            
                "PORT_KR_COMX2_1",            
                "PORT_KR_COMX2_2",            
                "PORT_KR_COMX2_3",            
                "PORT_KR_COMX3_0",            
                "PORT_KR_COMX3_1",            
                "PORT_KR_COMX3_2",            
                "PORT_KR_COMX3_3",            
                "PORT_1G_0_ARTIX_VIRTEX",     
                "PORT_1G_1_ARTIX_VIRTEX",     
                "PORT_1G_0_ARTIX_SMARC_MAIN", 
                "PORT_1G_1_ARTIX_SMARC_MAIN", 
                "PORT_1G_0_ARTIX_SMARC_CTRL", 
                "PORT_1G_1_ARTIX_SMARC_CTRL", 
                "PORT_1G_ARTIX_COMX0",        
                "PORT_1G_ARTIX_COMX1",        
                "PORT_1G_ARTIX_COMX2",        
                "PORT_1G_ARTIX_COMX3",        
                "PORT_1G_ARTIX_ATP",          
                "PORT_1G_ARTIX_FRONT_PANEL",  
                "PORT_1G_0_VIRTEX_ARTIX",     
                "PORT_1G_1_VIRTEX_ARTIX"     
            },
            .offset = 
            {
                PORT_10G_0_NETWORK_OFFSET, 
                PORT_10G_1_NETWORK_OFFSET, 
                PORT_10G_2_NETWORK_OFFSET, 
                PORT_10G_3_NETWORK_OFFSET, 
                PORT_10G_4_NETWORK_OFFSET, 
                PORT_10G_5_NETWORK_OFFSET, 
                PORT_10G_6_NETWORK_OFFSET, 
                PORT_10G_7_NETWORK_OFFSET, 
                PORT_10G_8_NETWORK_OFFSET, 
                PORT_10G_9_NETWORK_OFFSET, 
                PORT_10G_10_NETWORK_OFFSET,
                PORT_10G_11_NETWORK_OFFSET,
                PORT_10G_0_CLIENT_OFFSET, 
                PORT_10G_1_CLIENT_OFFSET, 
                PORT_10G_2_CLIENT_OFFSET, 
                PORT_10G_3_CLIENT_OFFSET, 
                PORT_10G_4_CLIENT_OFFSET, 
                PORT_10G_5_CLIENT_OFFSET, 
                PORT_10G_6_CLIENT_OFFSET, 
                PORT_10G_7_CLIENT_OFFSET, 
                PORT_10G_8_CLIENT_OFFSET, 
                PORT_10G_9_CLIENT_OFFSET, 
                PORT_10G_10_CLIENT_OFFSET,
                PORT_10G_11_CLIENT_OFFSET,
                PORT_100G_0_NETWORK_OFFSET,
                PORT_100G_1_NETWORK_OFFSET,
                PORT_100G_0_CLIENT_OFFSET,
                PORT_100G_1_CLIENT_OFFSET,
                PORT_KR_COMX0_0_OFFSET,   
                PORT_KR_COMX0_1_OFFSET,   
                PORT_KR_COMX0_2_OFFSET,   
                PORT_KR_COMX0_3_OFFSET,   
                PORT_KR_COMX1_0_OFFSET,   
                PORT_KR_COMX1_1_OFFSET,   
                PORT_KR_COMX1_2_OFFSET,   
                PORT_KR_COMX1_3_OFFSET,   
                PORT_KR_COMX2_0_OFFSET,    
                PORT_KR_COMX2_1_OFFSET,    
                PORT_KR_COMX2_2_OFFSET,    
                PORT_KR_COMX2_3_OFFSET,    
                PORT_KR_COMX3_0_OFFSET,    
                PORT_KR_COMX3_1_OFFSET,    
                PORT_KR_COMX3_2_OFFSET,    
                PORT_KR_COMX3_3_OFFSET,    
                PORT_1G_0_ARTIX_VIRTEX_OFFSET,       
                PORT_1G_1_ARTIX_VIRTEX_OFFSET,       
                PORT_1G_0_ARTIX_SMARC_MAIN_OFFSET,   
                PORT_1G_1_ARTIX_SMARC_MAIN_OFFSET,   
                PORT_1G_0_ARTIX_SMARC_CTRL_OFFSET,   
                PORT_1G_1_ARTIX_SMARC_CTRL_OFFSET,   
                PORT_1G_ARTIX_COMX0_OFFSET,          
                PORT_1G_ARTIX_COMX1_OFFSET,          
                PORT_1G_ARTIX_COMX2_OFFSET,          
                PORT_1G_ARTIX_COMX3_OFFSET,          
                PORT_1G_ARTIX_ATP_OFFSET,            
                PORT_1G_ARTIX_FRONT_PANEL_OFFSET,    
                PORT_1G_0_VIRTEX_ARTIX_OFFSET, 
                PORT_1G_1_VIRTEX_ARTIX_OFFSET, 
            }
        }
    }
};

static board_device_descr_t _trans_brd_descr[] = {
    /* 10Gb black Ports 0-11 */
    {
        .type_name = "trans", .sig = eDEVBRD_TRANSR_0_SIG, 
        .spec.trans = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_3, 
                .dev_i2ca = eDEVBRD_I2CA_TRANS
            },
            .extra_i2ca = eDEVBRD_I2CA_TRANS_DIG,
            .extra_mux_pin = 0
        }
    },
    {
        .type_name = "trans", .sig = eDEVBRD_TRANSR_1_SIG, 
        .spec.trans = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_3, 
                .dev_i2ca = eDEVBRD_I2CA_TRANS
            },
            .extra_i2ca = eDEVBRD_I2CA_TRANS_DIG,
            .extra_mux_pin = 1
        }
    },
    {
        .type_name = "trans", .sig = eDEVBRD_TRANSR_2_SIG, 
        .spec.trans = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_3, 
                .dev_i2ca = eDEVBRD_I2CA_TRANS
            },
            .extra_i2ca = eDEVBRD_I2CA_TRANS_DIG,
            .extra_mux_pin = 2
        }
    },
    {
        .type_name = "trans", .sig = eDEVBRD_TRANSR_3_SIG, 
        .spec.trans = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_3, 
                .dev_i2ca = eDEVBRD_I2CA_TRANS
            },
            .extra_i2ca = eDEVBRD_I2CA_TRANS_DIG,
            .extra_mux_pin = 3
        }
    },
    {
        .type_name = "trans", .sig = eDEVBRD_TRANSR_4_SIG, 
        .spec.trans = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_3, 
                .dev_i2ca = eDEVBRD_I2CA_TRANS
            },
            .extra_i2ca = eDEVBRD_I2CA_TRANS_DIG,
            .extra_mux_pin = 4
        }
    },
    {
        .type_name = "trans", .sig = eDEVBRD_TRANSR_5_SIG, 
        .spec.trans = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_3, 
                .dev_i2ca = eDEVBRD_I2CA_TRANS
            },
            .extra_i2ca = eDEVBRD_I2CA_TRANS_DIG,
            .extra_mux_pin = 5
        }
    },
    {
        .type_name = "trans", .sig = eDEVBRD_TRANSR_6_SIG, 
        .spec.trans = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_3, 
                .dev_i2ca = eDEVBRD_I2CA_TRANS
            },
            .extra_i2ca = eDEVBRD_I2CA_TRANS_DIG,
            .extra_mux_pin = 6
        }
    },
    {
        .type_name = "trans", .sig = eDEVBRD_TRANSR_7_SIG, 
        .spec.trans = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_3, 
                .dev_i2ca = eDEVBRD_I2CA_TRANS
            },
            .extra_i2ca = eDEVBRD_I2CA_TRANS_DIG,
            .extra_mux_pin = 7
        }
    },
    {
        .type_name = "trans", .sig = eDEVBRD_TRANSR_8_SIG, 
        .spec.trans = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_3, 
                .dev_i2ca = eDEVBRD_I2CA_TRANS
            },
            .extra_i2ca = eDEVBRD_I2CA_TRANS_DIG,
            .extra_mux_pin = 8
        }
    },
    {
        .type_name = "trans", .sig = eDEVBRD_TRANSR_9_SIG, 
        .spec.trans = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_3, 
                .dev_i2ca = eDEVBRD_I2CA_TRANS
            },
            .extra_i2ca = eDEVBRD_I2CA_TRANS_DIG,
            .extra_mux_pin = 9
        }
    },
    {
        .type_name = "trans", .sig = eDEVBRD_TRANSR_10_SIG, 
        .spec.trans = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_3, 
                .dev_i2ca = eDEVBRD_I2CA_TRANS
            },
            .extra_i2ca = eDEVBRD_I2CA_TRANS_DIG,
            .extra_mux_pin = 10
        }
    },
    {
        .type_name = "trans", .sig = eDEVBRD_TRANSR_11_SIG, 
        .spec.trans = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_3, 
                .dev_i2ca = eDEVBRD_I2CA_TRANS
            },
            .extra_i2ca = eDEVBRD_I2CA_TRANS_DIG,
            .extra_mux_pin = 11
        }
    },
    /* 10Gb Red Ports 0-11 */
    {
        .type_name = "trans", .sig = eDEVBRD_TRANSR_12_SIG, 
        .spec.trans = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_2, 
                .dev_i2ca = eDEVBRD_I2CA_TRANS
            },
            .extra_i2ca = eDEVBRD_I2CA_TRANS_DIG,
            .extra_mux_pin = 0
        }
    },
    {
        .type_name = "trans", .sig = eDEVBRD_TRANSR_13_SIG, 
        .spec.trans = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_2, 
                .dev_i2ca = eDEVBRD_I2CA_TRANS
            },
            .extra_i2ca = eDEVBRD_I2CA_TRANS_DIG,
            .extra_mux_pin = 1
        }
    },
    {
        .type_name = "trans", .sig = eDEVBRD_TRANSR_14_SIG, 
        .spec.trans = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_2, 
                .dev_i2ca = eDEVBRD_I2CA_TRANS
            },
            .extra_i2ca = eDEVBRD_I2CA_TRANS_DIG,
            .extra_mux_pin = 2
        }
    },
    {
        .type_name = "trans", .sig = eDEVBRD_TRANSR_15_SIG, 
        .spec.trans = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_2, 
                .dev_i2ca = eDEVBRD_I2CA_TRANS
            },
            .extra_i2ca = eDEVBRD_I2CA_TRANS_DIG,
            .extra_mux_pin = 3
        }
    },
    {
        .type_name = "trans", .sig = eDEVBRD_TRANSR_16_SIG, 
        .spec.trans = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_2, 
                .dev_i2ca = eDEVBRD_I2CA_TRANS
            },
            .extra_i2ca = eDEVBRD_I2CA_TRANS_DIG,
            .extra_mux_pin = 4
        }
    },
    {
        .type_name = "trans", .sig = eDEVBRD_TRANSR_17_SIG, 
        .spec.trans = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_2, 
                .dev_i2ca = eDEVBRD_I2CA_TRANS
            },
            .extra_i2ca = eDEVBRD_I2CA_TRANS_DIG,
            .extra_mux_pin = 5
        }
    },
    {
        .type_name = "trans", .sig = eDEVBRD_TRANSR_18_SIG, 
        .spec.trans = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_2, 
                .dev_i2ca = eDEVBRD_I2CA_TRANS
            },
            .extra_i2ca = eDEVBRD_I2CA_TRANS_DIG,
            .extra_mux_pin = 6
        }
    },
    {
        .type_name = "trans", .sig = eDEVBRD_TRANSR_19_SIG, 
        .spec.trans = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_2, 
                .dev_i2ca = eDEVBRD_I2CA_TRANS
            },
            .extra_i2ca = eDEVBRD_I2CA_TRANS_DIG,
            .extra_mux_pin = 7
        }
    },
    {
        .type_name = "trans", .sig = eDEVBRD_TRANSR_20_SIG, 
        .spec.trans = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_2, 
                .dev_i2ca = eDEVBRD_I2CA_TRANS
            },
            .extra_i2ca = eDEVBRD_I2CA_TRANS_DIG,
            .extra_mux_pin = 8
        }
    },
    {
        .type_name = "trans", .sig = eDEVBRD_TRANSR_21_SIG, 
        .spec.trans = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_2, 
                .dev_i2ca = eDEVBRD_I2CA_TRANS
            },
            .extra_i2ca = eDEVBRD_I2CA_TRANS_DIG,
            .extra_mux_pin = 9
        }
    },
    {
        .type_name = "trans", .sig = eDEVBRD_TRANSR_22_SIG, 
        .spec.trans = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_2, 
                .dev_i2ca = eDEVBRD_I2CA_TRANS
            },
            .extra_i2ca = eDEVBRD_I2CA_TRANS_DIG,
            .extra_mux_pin = 10
        }
    },
    {
        .type_name = "trans", .sig = eDEVBRD_TRANSR_23_SIG, 
        .spec.trans = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_2, 
                .dev_i2ca = eDEVBRD_I2CA_TRANS
            },
            .extra_i2ca = eDEVBRD_I2CA_TRANS_DIG,
            .extra_mux_pin = 11
        }
    },
    /* 100Gb Black Ports 0-1 */
    {
        .type_name = "trans", .sig = eDEVBRD_TRANSR_24_SIG, 
        .spec.trans = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_3, 
                .dev_i2ca = eDEVBRD_I2CA_TRANS
            },
            .is_qsfp = CCL_TRUE,
            .extra_i2ca = eDEVBRD_I2CA_TRANS,
            .extra_mux_pin = 13
        }
    },
    {
        .type_name = "trans", .sig = eDEVBRD_TRANSR_25_SIG, 
        .spec.trans = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_3, 
                .dev_i2ca = eDEVBRD_I2CA_TRANS
            },
            .is_qsfp = CCL_TRUE,
            .extra_i2ca = eDEVBRD_I2CA_TRANS,
            .extra_mux_pin = 12
        }
    },
    /* 100Gb Red Ports 0-1 */
    {
        .type_name = "trans", .sig = eDEVBRD_TRANSR_26_SIG, 
        .spec.trans = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_2, 
                .dev_i2ca = eDEVBRD_I2CA_TRANS
            },
            .is_qsfp = CCL_TRUE,
            .extra_i2ca = eDEVBRD_I2CA_TRANS,
            .extra_mux_pin = 13
        }
    },
    {
        .type_name = "trans", .sig = eDEVBRD_TRANSR_27_SIG, 
        .spec.trans = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_2, 
                .dev_i2ca = eDEVBRD_I2CA_TRANS
            },
            .is_qsfp = CCL_TRUE,
            .extra_i2ca = eDEVBRD_I2CA_TRANS,
            .extra_mux_pin = 12
        }
    },
};

static board_device_descr_t _smartcard_brd_descr[] = {
    {
        .type_name = "smartcard", .sig = eDEVBRD_SMARTCARD_0_SIG, 
        .spec.smartcard = {
            .uart_id = eDEVBRD_AXIUART_8
        }
    },
};

/* cpu board device description */
static board_device_descr_t _cpu_brd_descr[] = {
    {
        .type_name = "cpu", .sig = eDEVBRD_CPU_SIG, 
        .spec.cpu = {
            .sys_sensors = {
                .voltage_sensors_num = 0,
                .temp_sensors_num = 1,
                .temp_sensors = {
                    {"temp1_temperature", "/sys/class/hwmon/hwmon0/temp1_input", 0}
                }
            }
        }
    }
};

static board_device_descr_t _dramtester_brd_descr[] = {
    {
        .type_name = "dramtester", .sig = eDEVBRD_DRAMTESTER_0_SIG, 
        .spec.dramtester = {
            .devname = "fpga1",
            .slot = 0,
            .offset = AXIGPIO_DDR4_USER_TEST_CTRL_OFFSET
        }
    },
    {
        .type_name = "dramtester", .sig = eDEVBRD_DRAMTESTER_1_SIG, 
        .spec.dramtester = {
            .devname = "fpga1",
            .slot = 1,
            .offset = AXIGPIO_DDR4_USER_TEST_CTRL_OFFSET
        }
    },
    {
        .type_name = "dramtester", .sig = eDEVBRD_DRAMTESTER_2_SIG, 
        .spec.dramtester = {
            .devname = "fpga1",
            .slot = 2,
            .offset = AXIGPIO_DDR4_USER_TEST_CTRL_OFFSET
        }
    }
};

/* atp module device description */
static board_device_descr_t _atp_brd_descr[] = {
    {
        .type_name = "atp", .sig = eDEVBRD_ATP_SIG, 
        .spec.atp = {
            .board_stats = {
                .port_num = BRDDEV_PORTS_NUM,
                .currmon_num = BRDDEV_CURRMON_NUM,
                .pmbus_num = BRDDEV_PMBUS_NUM,
                .fanctls_num = BRDDEV_FANCTLS_NUM,
                .rtc_num = BRDDEV_RTC_NUM,
                .spidev_num = BRDDEV_SPIDEV_NUM,
                .xadc_num = BRDDEV_XADC_NUM,
                .sysmon_num = BRDDEV_SYSMON_NUM,
                .dram_num = BRDDEV_DRAM_NUM,
                .eeprom_spd_num = BRDDEV_DRAM_NUM,
                .port_stats = {
                    {"PORT_10G_0_NETWORK"},
                    {"PORT_10G_1_NETWORK"},
                    {"PORT_10G_2_NETWORK"},
                    {"PORT_10G_3_NETWORK"},
                    {"PORT_10G_4_NETWORK"},
                    {"PORT_10G_5_NETWORK"},
                    {"PORT_10G_6_NETWORK"},
                    {"PORT_10G_7_NETWORK"},
                    {"PORT_10G_8_NETWORK"},
                    {"PORT_10G_9_NETWORK"},
                    {"PORT_10G_10_NETWORK"},
                    {"PORT_10G_11_NETWORK"},
                    {"PORT_10G_0_CLIENT"},
                    {"PORT_10G_1_CLIENT"},
                    {"PORT_10G_2_CLIENT"},
                    {"PORT_10G_3_CLIENT"},
                    {"PORT_10G_4_CLIENT"},
                    {"PORT_10G_5_CLIENT"},
                    {"PORT_10G_6_CLIENT"},
                    {"PORT_10G_7_CLIENT"},
                    {"PORT_10G_8_CLIENT"},
                    {"PORT_10G_9_CLIENT"},
                    {"PORT_10G_10_CLIENT"},
                    {"PORT_10G_11_CLIENT"},
                    {"PORT_100G_0_NETWORK"},
                    {"PORT_100G_1_NETWORK"},
                    {"PORT_100G_0_CLIENT"},
                    {"PORT_100G_1_CLIENT"},
                    {"PORT_KR_COMX0_0"},
                    {"PORT_KR_COMX0_1"},
                    {"PORT_KR_COMX0_2"},
                    {"PORT_KR_COMX0_3"},
                    {"PORT_KR_COMX1_0"},
                    {"PORT_KR_COMX1_1"},
                    {"PORT_KR_COMX1_2"},
                    {"PORT_KR_COMX1_3"},
                    {"PORT_KR_COMX2_0"},
                    {"PORT_KR_COMX2_1"},
                    {"PORT_KR_COMX2_2"},
                    {"PORT_KR_COMX2_3"},
                    {"PORT_KR_COMX3_0"},
                    {"PORT_KR_COMX3_1"},
                    {"PORT_KR_COMX3_2"},
                    {"PORT_KR_COMX3_3"},
                    {"PORT_1G_0_ARTIX_VIRTEX"},
                    {"PORT_1G_1_ARTIX_VIRTEX"},
                    {"PORT_1G_0_ARTIX_SMARC_MAIN"},
                    {"PORT_1G_1_ARTIX_SMARC_MAIN"},
                    {"PORT_1G_0_ARTIX_SMARC_CTRL"},
                    {"PORT_1G_1_ARTIX_SMARC_CTRL"},
                    {"PORT_1G_ARTIX_COMX0"},
                    {"PORT_1G_ARTIX_COMX1"},
                    {"PORT_1G_ARTIX_COMX2"},
                    {"PORT_1G_ARTIX_COMX3"},
                    {"PORT_1G_ARTIX_ATP"},
                    {"PORT_1G_ARTIX_OOB"},
                    {"PORT_1G_0_VIRTEX_ARTIX"},
                    {"PORT_1G_1_VIRTEX_ARTIX"}
                },
                .currmon_stats = {
                    {"CURRENT_MONITOR_COMX0"},
                    {"CURRENT_MONITOR_COMX1"},
                    {"CURRENT_MONITOR_COMX2"},
                    {"CURRENT_MONITOR_COMX3"},
                    {"CURRENT_MONITOR_MAIN_CPU"},
                    {"CURRENT_MONITOR_CONTROL_CPU"}
                },
                .pmbus_stats = {
                    {"PMBUS_VCCINT_O_85V"},
                    {"PMBUS_VCCAUX_1_8V"},
                    {"PMBUS_VCCO_1_2V"},
                    {"PMBUS_VCCO_3_3V"},
                    {"PMBUS_VCCO_2_5V"},
                    {"PMBUS_MGTAVCC_0_9V"},
                    {"PMBUS_1_0V"},
                    {"PMBUS_MGTAVTT_1_2V"},
                    {"PMBUS_5V"},
                    {"PMBUS_MAIN_PS_1"},
                    {"PMBUS_MAIN_PS_2"},
                    {"PMBUS_VCCO_CLIENT_3_3V"}
                },
                .fanctl_stats = {
                    {"FAN_CONTROLLER"}
                },
                .rtc_stats = {
                    {"RTC_MAIN_BOARD"},
                    {"RTC_ATP_1"},
                    {"RTC_ATP_2"}
                },
                .spidev_stats = {
                    {"FLASH_ATP1"},
                    {"FLASH_ATP2"},
                    {"FLASH_GENERIC1"},
                    {"FLASH_GENERIC2"},
                    {"FLASH_MAIN"},
                    {"FLASH_CTRL"},
                    {"FLASH_COMX0"},
                    {"FLASH_COMX1"},
                    {"FLASH_COMX2"},
                    {"FLASH_COMX3"}
                },
                .xadc_stats = {
                    {"XADC"}
                },
                .sysmon_stats = {
                    {"SYSMON"}
                },
                .dram_stats = {
                    {"DRAM_0"},
                    {"DRAM_1"},
                    {"DRAM_2"}
                },
                .eeprom_spd_stats = {
                    {"EEPROM_DRAM_0"},
                    {"EEPROM_DRAM_1"},
                    {"EEPROM_DRAM_2"}
                }
            }
        }
    }
};

/* atp module device description */
static board_device_descr_t _hal_brd_descr[] = {
    {
        .type_name = "hal", .sig = eDEVBRD_HAL_SIG, 
        .spec.hal = {
            .name = "HAL"
        }
    }
};

/* I2CBUS device board initialization routine
 */
static ccl_err_t _fboard_i2cbus_init(board_device_descr_t *p_ds, int instances_num)
{
    int i;

    /* check input parameters */
    if ( !p_ds ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    extern ccl_err_t devi2cbus_init(void);
    _ret = devi2cbus_init();
    if ( _ret ) {
        PDEBUG("Error! Can't initialize I2CBUS device type\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* create instances for I2CBUS device */
    for ( i=0; i < instances_num; i++ ) {
        PDEBUG("123$=> i=%d, tname=%s, sig=0x%08x\n", 
               i, p_ds->type_name, p_ds->sig);

        _ret = devman_device_create(p_ds->type_name, p_ds->sig, 
                                    (void *)&p_ds->spec.i2cbus, 
                                    &p_ds->devo);
        if ( _ret ) {
            PDEBUG("Error! Can't create I2CBUS instance %d\n", i);
            DEVBOARD_ERROR_RETURN(CCL_FAIL);
        }
        p_ds++;
    }         

    return CCL_OK;
}

/* I2CCORE device board initialization routine
 */
static ccl_err_t _fboard_i2ccore_init(board_device_descr_t *p_ds, int instances_num)
{
    int i;

    /* check input parameters */
    if ( !p_ds ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    extern ccl_err_t devi2ccore_init(void);
    _ret = devi2ccore_init();
    if ( _ret ) {
        PDEBUG("Error! Can't initialize I2CCORE device type\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* create instances for I2CBUS device */
    for ( i=0; i < instances_num; i++ ) {
        PDEBUG("123$=> i=%d, tname=%s, sig=0x%08x\n", 
               i, p_ds->type_name, p_ds->sig);

        _ret = devman_device_create(p_ds->type_name, p_ds->sig, 
                                    (void *)&p_ds->spec.i2ccore, 
                                    &p_ds->devo);
        if ( _ret ) {
            PDEBUG("Error! Can't create I2CCORE instance %d\n", i);
            DEVBOARD_ERROR_RETURN(CCL_FAIL);
        }
        p_ds++;
    }         

    return CCL_OK;
}

/* AXI GPIO device board initialization routine
 */
static ccl_err_t _fboard_axigpio_init(board_device_descr_t *p_ds, int instances_num)
{
    int i;

    /* check input parameters */
    if ( !p_ds ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    extern ccl_err_t devaxigpio_init(void);
    _ret = devaxigpio_init();
    if ( _ret ) {
        PDEBUG("Error! Can't initialize AXI GPIO device type\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* create instances for AXI GPIO device */
    for ( i=0; i < instances_num; i++ ) {
        PDEBUG("123$=> i=%d, tname=%s, sig=0x%08x\n", 
               i, p_ds->type_name, p_ds->sig);

        _ret = devman_device_create(p_ds->type_name, p_ds->sig, 
                                    (void *)&p_ds->spec.axigpio, 
                                    &p_ds->devo);
        if ( _ret ) {
            PDEBUG("Error! Can't create AXI GPIO instance %d\n", i);
            DEVBOARD_ERROR_RETURN(CCL_FAIL);
        }
        p_ds++;
    }         

    return CCL_OK;
}

/* AXI SPI device board initialization routine
 */
static ccl_err_t _fboard_axispi_init(board_device_descr_t *p_ds, int instances_num)
{
    int i;

    /* check input parameters */
    if ( !p_ds ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    extern ccl_err_t devaxispi_init(void);
    _ret = devaxispi_init();
    if ( _ret ) {
        PDEBUG("Error! Can't initialize AXI SPI device type\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* create instances for AXI SPI device */
    for ( i=0; i < instances_num; i++ ) {
        PDEBUG("123$=> i=%d, tname=%s, sig=0x%08x\n", 
               i, p_ds->type_name, p_ds->sig);

        _ret = devman_device_create(p_ds->type_name, p_ds->sig, 
                                    (void *)&p_ds->spec.axispi, 
                                    &p_ds->devo);
        if ( _ret ) {
            PDEBUG("Error! Can't create AXI SPI instance %d\n", i);
            DEVBOARD_ERROR_RETURN(CCL_FAIL);
        }
        p_ds++;
    }         

    return CCL_OK;
}

/* AXI UART device board initialization routine
 */
static ccl_err_t _fboard_axiuart_init(board_device_descr_t *p_ds, int instances_num)
{
    int i;

    /* check input parameters */
    if ( !p_ds ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    extern ccl_err_t devaxiuart_init(void);
    _ret = devaxiuart_init();
    if ( _ret ) {
        PDEBUG("Error! Can't initialize AXI UART device type\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* create instances for AXI UART device */
    for ( i=0; i < instances_num; i++ ) {
        PDEBUG("123$=> i=%d, tname=%s, sig=0x%08x\n", 
               i, p_ds->type_name, p_ds->sig);

        _ret = devman_device_create(p_ds->type_name, p_ds->sig, 
                                    (void *)&p_ds->spec.axiuart, 
                                    &p_ds->devo);
        if ( _ret ) {
            PDEBUG("Error! Can't create AXI UART instance %d\n", i);
            DEVBOARD_ERROR_RETURN(CCL_FAIL);
        }
        p_ds++;
    }         

    return CCL_OK;
}

/* AXI INTC device board initialization routine
 */
static ccl_err_t _fboard_axiintc_init(board_device_descr_t *p_ds, int instances_num)
{
    int i;

    /* check input parameters */
    if ( !p_ds ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    extern ccl_err_t devintc_init(void);
    _ret = devintc_init();
    if ( _ret ) {
        PDEBUG("Error! Can't initialize AXI INTC device type\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* create instances for AXI INTC device */
    for ( i=0; i < instances_num; i++ ) {
        PDEBUG("123$=> i=%d, tname=%s, sig=0x%08x\n", 
               i, p_ds->type_name, p_ds->sig);

        _ret = devman_device_create(p_ds->type_name, p_ds->sig, 
                                    (void *)&p_ds->spec.intc, 
                                    &p_ds->devo);
        if ( _ret ) {
            PDEBUG("Error! Can't create AXI INTC instance %d\n", i);
            DEVBOARD_ERROR_RETURN(CCL_FAIL);
        }
        p_ds++;
    }         

    return CCL_OK;
}

/* XADC device board initialization routine
 */
static ccl_err_t _fboard_xadc_init(board_device_descr_t *p_ds, int instances_num)
{
    int i;

    /* check input parameters */
    if ( !p_ds ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    extern ccl_err_t devxadc_init(void);
    _ret = devxadc_init();
    if ( _ret ) {
        PDEBUG("Error! Can't initialize XADC device type\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* create instances for XADC device */
    for ( i=0; i < instances_num; i++ ) {
        PDEBUG("123$=> i=%d, tname=%s, sig=0x%08x\n", 
               i, p_ds->type_name, p_ds->sig);

        _ret = devman_device_create(p_ds->type_name, p_ds->sig, 
                                    (void *)&p_ds->spec.xadc, 
                                    &p_ds->devo);
        if ( _ret ) {
            PDEBUG("Error! Can't create XADC instance %d\n", i);
            DEVBOARD_ERROR_RETURN(CCL_FAIL);
        }
        p_ds++;
    }         

    return CCL_OK;
}

/* Sysmon device board initialization routine
 */
static ccl_err_t _fboard_sysmon_init(board_device_descr_t *p_ds, int instances_num)
{
    int i;

    /* check input parameters */
    if ( !p_ds ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    extern ccl_err_t devsysmon_init(void);
    _ret = devsysmon_init();
    if ( _ret ) {
        PDEBUG("Error! Can't initialize Sysmon device type\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* create instances for Sysmon device */
    for ( i=0; i < instances_num; i++ ) {
        PDEBUG("123$=> i=%d, tname=%s, sig=0x%08x\n", 
               i, p_ds->type_name, p_ds->sig);

        _ret = devman_device_create(p_ds->type_name, p_ds->sig, 
                                    (void *)&p_ds->spec.sysmon, 
                                    &p_ds->devo);
        if ( _ret ) {
            PDEBUG("Error! Can't create Sysmon instance %d\n", i);
            DEVBOARD_ERROR_RETURN(CCL_FAIL);
        }
        p_ds++;
    }         

    return CCL_OK;
}

/* Ethernet Switch device board initialization routine
 */
static ccl_err_t _fboard_eswitch_init(board_device_descr_t *p_ds, int instances_num)
{
    int i;

    /* check input parameters */
    if ( !p_ds ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    extern ccl_err_t deveswitch_init(void);
    _ret = deveswitch_init();
    if ( _ret ) {
        PDEBUG("Error! Can't initialize Ethernet Switch device type\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* create instances for Ethernet Switch device */
    for ( i=0; i < instances_num; i++ ) {
        PDEBUG("123$=> i=%d, tname=%s, sig=0x%08x\n", 
               i, p_ds->type_name, p_ds->sig);

        _ret = devman_device_create(p_ds->type_name, p_ds->sig, 
                                    (void *)&p_ds->spec.eswitch, 
                                    &p_ds->devo);
        if ( _ret ) {
            PDEBUG("Error! Can't create Ethernet Switch instance %d\n", i);
            DEVBOARD_ERROR_RETURN(CCL_FAIL);
        }
        p_ds++;
    }         

    return CCL_OK;
}

/* MBOX device board initialization routine
 */
static ccl_err_t _fboard_mbox_init(board_device_descr_t *p_ds, int instances_num)
{
    int i;

    /* check input parameters */
    if ( !p_ds ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    extern ccl_err_t devmbox_init(void);
    _ret = devmbox_init();
    if ( _ret ) {
        PDEBUG("Error! Can't initialize MBOX device type\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* create instances for MBOX device */
    for ( i=0; i < instances_num; i++ ) {
        PDEBUG("123$=> i=%d, tname=%s, sig=0x%08x\n", 
               i, p_ds->type_name, p_ds->sig);

        _ret = devman_device_create(p_ds->type_name, p_ds->sig, 
                                    (void *)&p_ds->spec.mbox, 
                                    &p_ds->devo);
        if ( _ret ) {
            PDEBUG("Error! Can't create MBOX instance %d\n", i);
            DEVBOARD_ERROR_RETURN(CCL_FAIL);
        }
        p_ds++;
    }         

    return CCL_OK;
}

/* TEMAC devices board initialization routine 
 */
static ccl_err_t _fboard_temac_init(board_device_descr_t *p_ds, int instances_num) 
{
    int i;

    /* check input parameters */
    if ( !p_ds ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    extern ccl_err_t devtemac_init(void);
    _ret = devtemac_init();
    if ( _ret ) {
        PDEBUG("Error! Can't initialize 1G device type\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* create instances for 1G device */
    for ( i=0; i < instances_num; i++ ) {
        PDEBUG("123$=> i=%d, tname=%s, sig=0x%08x\n", 
               i, p_ds->type_name, p_ds->sig);

        _ret = devman_device_create(p_ds->type_name, p_ds->sig, 
                                    (void *)&p_ds->spec.temac, &p_ds->devo);
        if ( _ret ) {
            PDEBUG("Error! Can't create 1G instance %d\n", i);
            DEVBOARD_ERROR_RETURN(CCL_FAIL);
        }
        p_ds++;
    }         

    return CCL_OK;
}

/* 10G (HSES10G) devices board initialization routine 
 */
static ccl_err_t _fboard_hses10g_init(board_device_descr_t *p_ds, int instances_num) 
{
    int i;

    /* check input parameters */
    if ( !p_ds ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    extern ccl_err_t devhses10g_init(void);
    _ret = devhses10g_init();
    if ( _ret ) {
        PDEBUG("Error! Can't initialize 10G device type\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* create instances for 10G device */
    for ( i=0; i < instances_num; i++ ) {
        PDEBUG("123$=> i=%d, tname=%s, sig=0x%08x\n", 
               i, p_ds->type_name, p_ds->sig);

        _ret = devman_device_create(p_ds->type_name, p_ds->sig, 
                                    (void *)&p_ds->spec.hses10g, &p_ds->devo);
        if ( _ret ) {
            PDEBUG("Error! Can't create 10G instance %d\n", i);
            DEVBOARD_ERROR_RETURN(CCL_FAIL);
        }
        p_ds++;
    }         

    return CCL_OK;
}

/* 100G (USPLUS100G) devices board initialization routine 
 */
static ccl_err_t _fboard_usplus100g_init(board_device_descr_t *p_ds, int instances_num) 
{
    int i;

    /* check input parameters */
    if ( !p_ds ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    extern ccl_err_t devusplus100g_init(void);
    _ret = devusplus100g_init();
    if ( _ret ) {
        PDEBUG("Error! Can't initialize 100G device type\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* create instances for 100G device */
    for ( i=0; i < instances_num; i++ ) {
        PDEBUG("123$=> i=%d, tname=%s, sig=0x%08x\n", 
               i, p_ds->type_name, p_ds->sig);

        _ret = devman_device_create(p_ds->type_name, p_ds->sig, 
                                    (void *)&p_ds->spec.usplus100g, &p_ds->devo);
        if ( _ret ) {
            PDEBUG("Error! Can't create 100G instance %d\n", i);
            DEVBOARD_ERROR_RETURN(CCL_FAIL);
        }
        p_ds++;
    }         

    return CCL_OK;
}

/* SPI devices board initialization routine 
 */
static ccl_err_t _fboard_spidev_init(board_device_descr_t *p_ds, int instances_num) 
{
    int i;

    /* check input parameters */
    if ( !p_ds ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    extern ccl_err_t devspidev_init(void);
    _ret = devspidev_init();
    if ( _ret ) {
        PDEBUG("Error! Can't initialize SPI device type\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* create instances for SPI device */
    for ( i=0; i < instances_num; i++ ) {
        PDEBUG("123$=> i=%d, tname=%s, sig=0x%08x\n", 
               i, p_ds->type_name, p_ds->sig);

        _ret = devman_device_create(p_ds->type_name, p_ds->sig, 
                                    (void *)&p_ds->spec.usplus100g, &p_ds->devo);
        if ( _ret ) {
            PDEBUG("Error! Can't create SPI instance %d\n", i);
            DEVBOARD_ERROR_RETURN(CCL_FAIL);
        }
        p_ds++;
    }         

    return CCL_OK;
}

static ccl_err_t _fboard_i2c_devices_init(void)
{
    board_device_descr_t *p_ds;
    int i, instances_num;

    /* 1. PMBUS devices initialization*/
    extern ccl_err_t devpmbus_init(void);
    _ret = devpmbus_init();
    if ( _ret ) {
        PDEBUG("Error! Can't initialize PMBUS device type\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_ds = &_pmbus_brd_descr[0];
    instances_num = sizeof(_pmbus_brd_descr)/sizeof(_pmbus_brd_descr[0]);

    /* create instances for PBMUS device */
    for ( i=0; i < instances_num; i++ ) {
        PDEBUG("123$=> i=%d, tname=%s, sig=0x%08x\n", 
               i, p_ds->type_name, p_ds->sig);

        _ret = devman_device_create(p_ds->type_name, p_ds->sig, 
                                    (void *)&p_ds->spec.pmbus, &p_ds->devo);
        if ( _ret ) {
            PDEBUG("Error! Can't create PMBUS %d\n", i);
            DEVBOARD_ERROR_RETURN(CCL_FAIL);
        }
        p_ds++;
    }         

    /* 2. EEPROM devices initialization */
    extern ccl_err_t deveeprom_init(void);
    _ret = deveeprom_init();
    if ( _ret ) {
        PDEBUG("Error! Can't initialize EEPROM type\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_ds = &_eeprom_brd_descr[0];
    instances_num = sizeof(_eeprom_brd_descr)/sizeof(_eeprom_brd_descr[0]);

    /* create instances for EEPROM device */
    for ( i=0; i < instances_num; i++ ) {
        PDEBUG("123$=> i=%d, tname=%s, sig=0x%08x\n", 
               i, p_ds->type_name, p_ds->sig);

        _ret = devman_device_create(p_ds->type_name, p_ds->sig, 
                                    (void *)&p_ds->spec.eeprom, &p_ds->devo);
        if ( _ret ) {
            PDEBUG("Error! Can't create EEPROM %d\n", i);
            DEVBOARD_ERROR_RETURN(CCL_FAIL);
        }
        p_ds++;
    }         

    /* 3. RTC devices initialization */
    extern ccl_err_t devm41st87w_init(void);
    _ret = devm41st87w_init();
    if ( _ret ) {
        PDEBUG("Error! Can't initialize RTC device type\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_ds = &_rtc_brd_descr[0];
    instances_num = sizeof(_rtc_brd_descr)/sizeof(_rtc_brd_descr[0]);

    /* create instances for RTC device */
    for ( i=0; i < instances_num; i++ ) {
        PDEBUG("123$=> i=%d, tname=%s, sig=0x%08x\n", 
               i, p_ds->type_name, p_ds->sig);

        _ret = devman_device_create(p_ds->type_name, p_ds->sig, 
                                    (void *)&p_ds->spec.rtc, &p_ds->devo);
        if ( _ret ) {
            PDEBUG("Error! Can't create RTC %d\n", i);
            DEVBOARD_ERROR_RETURN(CCL_FAIL);
        }
        p_ds++;
    }         

    /* 4. LCD devices initialization */
    extern ccl_err_t devlk2047t1u_init(void);
    _ret = devlk2047t1u_init();
    if ( _ret ) {
        PDEBUG("Error! Can't initialize LCD device type\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_ds = &_lcd_brd_descr[0];
    instances_num = sizeof(_lcd_brd_descr)/sizeof(_lcd_brd_descr[0]);

    /* create instances for LCD device */
    for ( i=0; i < instances_num; i++ ) {
        PDEBUG("123$=> i=%d, tname=%s, sig=0x%08x\n", 
               i, p_ds->type_name, p_ds->sig);

        _ret = devman_device_create(p_ds->type_name, p_ds->sig, 
                                    (void *)&p_ds->spec.lcd, &p_ds->devo);
        if ( _ret ) {
            PDEBUG("Error! Can't create LCD %d\n", i);
            DEVBOARD_ERROR_RETURN(CCL_FAIL);
        }
        p_ds++;
    }

    /* 5. Current Monitor devices initialization */         
    extern ccl_err_t devina220_init(void);
    _ret = devina220_init();
    if ( _ret ) {
        PDEBUG("Error! Can't initialize Current Monitor device type\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_ds = &_currmon_brd_descr[0];
    instances_num = sizeof(_currmon_brd_descr)/sizeof(_currmon_brd_descr[0]);

    /* create instances for Current Monitor device */
    for ( i=0; i < instances_num; i++ ) {
        PDEBUG("123$=> i=%d, tname=%s, sig=0x%08x\n", 
               i, p_ds->type_name, p_ds->sig);

        _ret = devman_device_create(p_ds->type_name, p_ds->sig, 
                                    (void *)&p_ds->spec.currmon, &p_ds->devo);
        if ( _ret ) {
            PDEBUG("Error! Can't create Current Monitor %d\n", i);
            DEVBOARD_ERROR_RETURN(CCL_FAIL);
        }
        p_ds++;
    }

    /* 6. I2CIO devices initialization */         
    extern ccl_err_t devtca6416a_init(void);
    _ret = devtca6416a_init();
    if ( _ret ) {
        PDEBUG("Error! Can't initialize I2CIO device type\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_ds = &_i2cio_brd_descr[0];
    instances_num = sizeof(_i2cio_brd_descr)/sizeof(_i2cio_brd_descr[0]);

    /* create instances for I2CIO device */
    for ( i=0; i < instances_num; i++ ) {
        PDEBUG("123$=> i=%d, tname=%s, sig=0x%08x\n", 
               i, p_ds->type_name, p_ds->sig);

        _ret = devman_device_create(p_ds->type_name, p_ds->sig, 
                                    (void *)&p_ds->spec.i2cio, &p_ds->devo);
        if ( _ret ) {
            PDEBUG("Error! Can't create I2CIO %d\n", i);
            DEVBOARD_ERROR_RETURN(CCL_FAIL);
        }
        p_ds++;
    }

    /* 7. Fan Controller devices initialization */         
    extern ccl_err_t devadt7490_init(void);
    _ret = devadt7490_init();
    if ( _ret ) {
        PDEBUG("Error! Can't initialize Fan Controller device type\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_ds = &_fan_brd_descr[0];
    instances_num = sizeof(_fan_brd_descr)/sizeof(_fan_brd_descr[0]);

    /* create instances for Fan Controller device */
    for ( i=0; i < instances_num; i++ ) {
        PDEBUG("123$=> i=%d, tname=%s, sig=0x%08x\n", 
               i, p_ds->type_name, p_ds->sig);

        _ret = devman_device_create(p_ds->type_name, p_ds->sig, 
                                    (void *)&p_ds->spec.fan, &p_ds->devo);
        if ( _ret ) {
            PDEBUG("Error! Can't create Fan Controller %d\n", i);
            DEVBOARD_ERROR_RETURN(CCL_FAIL);
        }
        p_ds++;
    }

    return CCL_OK;
}

/* Transceivers board initialization routine 
 */
static ccl_err_t _fboard_trans_init(board_device_descr_t *p_ds, int instances_num) 
{
    int i;

    /* check input parameters */
    if ( !p_ds ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    extern ccl_err_t devtrans_init(void);
    _ret = devtrans_init();
    if ( _ret ) {
        PDEBUG("Error! Can't initialize transceiver device type\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* create instances for transceiver device */
    for ( i=0; i < instances_num; i++ ) {
        PDEBUG("123$=> i=%d, tname=%s, sig=0x%08x\n", 
               i, p_ds->type_name, p_ds->sig);

        _ret = devman_device_create(p_ds->type_name, p_ds->sig, 
                                    (void *)&p_ds->spec.trans, &p_ds->devo);
        if ( _ret ) {
            PDEBUG("Error! Can't create transceiver instance %d\n", i);
            DEVBOARD_ERROR_RETURN(CCL_FAIL);
        }
        p_ds++;
    }         

    return CCL_OK;
}

/* Portmap board initialization routine 
 */
static ccl_err_t _fboard_portmap_init(board_device_descr_t *p_ds, int instances_num) 
{
    int i;

    /* check input parameters */
    if ( !p_ds ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    extern ccl_err_t devportmap_init(void);
    _ret = devportmap_init();
    if ( _ret ) {
        PDEBUG("Error! Can't initialize portmap device type\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* create instances for portmap device */
    for ( i=0; i < instances_num; i++ ) {
        PDEBUG("123$=> i=%d, tname=%s, sig=0x%08x\n", 
               i, p_ds->type_name, p_ds->sig);

        _ret = devman_device_create(p_ds->type_name, p_ds->sig, 
                                    (void *)&p_ds->spec.portmap, &p_ds->devo);
        if ( _ret ) {
            PDEBUG("Error! Can't create portmap instance %d\n", i);
            DEVBOARD_ERROR_RETURN(CCL_FAIL);
        }
        p_ds++;
    }         

    return CCL_OK;
}

/* SPI bus device board initialization routine 
 */
static ccl_err_t _fboard_spibus_init(board_device_descr_t *p_ds, int instances_num) 
{
    int i;

    /* check input parameters */
    if ( !p_ds ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    extern ccl_err_t devspibus_init(void);
    _ret = devspibus_init();
    if ( _ret ) {
        PDEBUG("Error! Can't initialize SPI bus device type\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* create instances for SPI bus device */
    for ( i=0; i < instances_num; i++ ) {
        PDEBUG("123$=> i=%d, tname=%s, sig=0x%08x, bus=%d, cs=0x%x\n", 
               i, p_ds->type_name, p_ds->sig,
               p_ds->spec.spibus.spi_bus_id, 
               p_ds->spec.spibus.spi_cs_id);

        _ret = devman_device_create(p_ds->type_name, p_ds->sig, 
                                    (void *)&p_ds->spec.spibus, &p_ds->devo);
        if ( _ret ) {
            PDEBUG("Error! Can't create SPI bus instance %d\n", i);
            DEVBOARD_ERROR_RETURN(CCL_FAIL);
        }
        p_ds++;
    }         

    return CCL_OK;
}

/* smartcard module initialization routine 
 */
static ccl_err_t _fboard_smartcard_init(board_device_descr_t *p_ds, int instances_num) 
{
    int i;

    /* check input parameters */
    if ( !p_ds ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    extern ccl_err_t devsmartcard_init(void);
    _ret = devsmartcard_init();
    if ( _ret ) {
        PDEBUG("Error! Can't initialize smartcard device type\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* create instances for atp device */
    for ( i=0; i < instances_num; i++ ) {
        PDEBUG("123$=> i=%d, tname=%s, sig=0x%08x\n", 
               i, p_ds->type_name, p_ds->sig);

        _ret = devman_device_create(p_ds->type_name, p_ds->sig, 
                                    (void *)&p_ds->spec.smartcard, &p_ds->devo);
        if ( _ret ) {
            PDEBUG("Error! Can't create smartcard instance %d\n", i);
            DEVBOARD_ERROR_RETURN(CCL_FAIL);
        }
        p_ds++;
    }         

    return CCL_OK;
}

/* ATP module initialization routine 
 */
static ccl_err_t _fboard_atp_init(board_device_descr_t *p_ds, int instances_num) 
{
    int i;

    /* check input parameters */
    if ( !p_ds ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    extern ccl_err_t devatp_init(void);
    _ret = devatp_init();
    if ( _ret ) {
        PDEBUG("Error! Can't initialize atp device type\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* create instances for atp device */
    for ( i=0; i < instances_num; i++ ) {
        PDEBUG("123$=> i=%d, tname=%s, sig=0x%08x\n", 
               i, p_ds->type_name, p_ds->sig);

        _ret = devman_device_create(p_ds->type_name, p_ds->sig, 
                                    (void *)&p_ds->spec.atp, &p_ds->devo);
        if ( _ret ) {
            PDEBUG("Error! Can't create atp instance %d\n", i);
            DEVBOARD_ERROR_RETURN(CCL_FAIL);
        }
        p_ds++;
    }         

    return CCL_OK;
}

/* CPU module initialization routine 
 */
static ccl_err_t _fboard_cpu_init(board_device_descr_t *p_ds, int instances_num) 
{
    int i;

    /* check input parameters */
    if ( !p_ds ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    extern ccl_err_t devcpu_init(void);
    _ret = devcpu_init();
    if ( _ret ) {
        PDEBUG("Error! Can't initialize atp device type\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* create instances for atp device */
    for ( i=0; i < instances_num; i++ ) {
        PDEBUG("123$=> i=%d, tname=%s, sig=0x%08x\n", 
               i, p_ds->type_name, p_ds->sig);

        _ret = devman_device_create(p_ds->type_name, p_ds->sig, 
                                    (void *)&p_ds->spec.cpu, &p_ds->devo);
        if ( _ret ) {
            PDEBUG("Error! Can't create cpu instance %d\n", i);
            DEVBOARD_ERROR_RETURN(CCL_FAIL);
        }
        p_ds++;
    }         

    return CCL_OK;
}

/* DRAM TESTER module initialization routine 
 */
static ccl_err_t _fboard_dramtester_init(board_device_descr_t *p_ds, int instances_num) 
{
    int i;

    /* check input parameters */
    if ( !p_ds ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    extern ccl_err_t devdramtester_init(void);
    _ret = devdramtester_init();
    if ( _ret ) {
        PDEBUG("Error! Can't initialize dramtester device type\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* create instances for dramtester device */
    for ( i=0; i < instances_num; i++ ) {
        PDEBUG("123$=> i=%d, tname=%s, sig=0x%08x\n", 
               i, p_ds->type_name, p_ds->sig);

        _ret = devman_device_create(p_ds->type_name, p_ds->sig, 
                                    (void *)&p_ds->spec.dramtester, &p_ds->devo);
        if ( _ret ) {
            PDEBUG("Error! Can't create dramtester instance %d\n", i);
            DEVBOARD_ERROR_RETURN(CCL_FAIL);
        }
        p_ds++;
    }         

    return CCL_OK;
}

/* HAL module initialization routine 
 */
static ccl_err_t _fboard_hal_init(board_device_descr_t *p_ds, int instances_num) 
{
    int i;

    /* check input parameters */
    if ( !p_ds ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    extern ccl_err_t devhal_init(void);
    _ret = devhal_init();
    if ( _ret ) {
        PDEBUG("Error! Can't initialize hal device type\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* create instances for hal device */
    for ( i=0; i < instances_num; i++ ) {
        PDEBUG("123$=> i=%d, tname=%s, sig=0x%08x\n", 
               i, p_ds->type_name, p_ds->sig);

        _ret = devman_device_create(p_ds->type_name, p_ds->sig, 
                                    (void *)&p_ds->spec.hal, &p_ds->devo);
        if ( _ret ) {
            PDEBUG("Error! Can't create hal instance %d\n", i);
            DEVBOARD_ERROR_RETURN(CCL_FAIL);
        }
        p_ds++;
    }         

    return CCL_OK;
}

/* Get device descriptor id given port id 
 */
static ccl_err_t _fboard_portid_to_devdescr(int port, int *discr)
{
    /* check input parameters */
    if ( !discr ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    if ( port >= eDEVBRD_10G_NETWORK_0 && port <= eDEVBRD_10G_CLIENT_11 )
        *discr = port;
    else if (port >= eDEVBRD_100G_NETWORK_0 && port <= eDEVBRD_100G_CLIENT_1) 
        *discr = port - eDEVBRD_100G_NETWORK_0;
    else if (port >= eDEVBRD_KR_COMX0_0 && port <= eDEVBRD_KR_COMX3_3) 
        *discr = port - 4;
    else if (port >= eDEVBRD_1G_AV_INTERCHIP_0 && port <= eDEVBRD_1G_VA_INTERCHIP_1) 
        *discr = port - eDEVBRD_1G_AV_INTERCHIP_0;
    else 
        return CCL_NOT_SUPPORT;

    return CCL_OK;
}

/* Get appropriate device/attr/significant_mask for port "led" operating  
 */
ccl_err_t devboard_portled_to_devattr(int port, char dev_name[], 
                                      char attr_name[], b_u32 *p_sign_mask,
                                      int *p_is_active_hi)
{
    /* check input parameters */
    if ( !p_sign_mask || !p_is_active_hi ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    *p_is_active_hi = 1;
    /* get target device name/output port attribute name */
    if ( (port >= eDEVBRD_10G_NETWORK_0 && port <= eDEVBRD_10G_NETWORK_11) ||
         (port >= eDEVBRD_100G_NETWORK_0 && port <= eDEVBRD_100G_NETWORK_1) ) {
        strncpy(dev_name, "axigpio/12", DEVMAN_DEVNAME_LEN);      
        /* prepare significant attribute bits  */
        *p_sign_mask = 1 << port;
        if (port == eDEVBRD_100G_NETWORK_0) 
            *p_sign_mask = 1 << 13;
        else if (port == eDEVBRD_100G_NETWORK_1) 
            *p_sign_mask = 1 << 12;
    } else if ( (port >= eDEVBRD_10G_CLIENT_0 && port <= eDEVBRD_10G_CLIENT_11) ||
                (port >= eDEVBRD_100G_CLIENT_0 && port <= eDEVBRD_100G_CLIENT_1) ) {
        strncpy(dev_name, "axigpio/10", DEVMAN_DEVNAME_LEN);      
        /* prepare significant attribute bits  */
        *p_sign_mask = 1 << (port - eDEVBRD_10G_CLIENT_0);
        if (port == eDEVBRD_100G_CLIENT_0) 
            *p_sign_mask = 1 << 13;
        else if (port == eDEVBRD_100G_CLIENT_1) 
            *p_sign_mask = 1 << 12;
    } else
        return CCL_NOT_SUPPORT;

    strncpy(attr_name, "data2", DEVMAN_DEVNAME_LEN);      

    return CCL_OK;
}

/* Get appropriate device/attr/significant_mask for port "enable" operating 
 */
ccl_err_t devboard_portena_to_devattr(int port, char dev_name[], 
                                      char attr_name[], b_u32 *p_sign_mask, 
                                      int *p_is_active_hi)
{
    /* check input parameters */
    if ( !p_sign_mask || !p_is_active_hi ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    *p_is_active_hi = 1;
    /* get target device name/output port attribute name */
    if ( port >= eDEVBRD_10G_NETWORK_0 && port <= eDEVBRD_10G_NETWORK_11 ) {
        strncpy(dev_name, "axigpio/12", DEVMAN_DEVNAME_LEN);      
        /* prepare significant attribute bits  */
        *p_sign_mask = 1 << (port+16);

    } else if ( port >= eDEVBRD_10G_CLIENT_0 && port <= eDEVBRD_10G_CLIENT_11 ) {
        strncpy(dev_name, "axigpio/10", DEVMAN_DEVNAME_LEN);      
        /* prepare significant attribute bits  */
        *p_sign_mask = 1 << (port+4);
    } else
        return CCL_NOT_SUPPORT;

    strncpy(attr_name, "data", DEVMAN_DEVNAME_LEN);      

    return CCL_OK;
}

/* Get appropriate device/attr/significant_mask for port "reset" operating 
 */
ccl_err_t devboard_portrst_to_devattr(int port, char dev_name[], 
                                      char attr_name[], b_u32 *p_sign_mask, 
                                      int *p_is_active_hi)
{
    /* check input parameters */
    if ( !p_sign_mask || !p_is_active_hi ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    *p_is_active_hi = 1;
    /* get target device name/output port attribute name */
    if ( port >= eDEVBRD_100G_NETWORK_0 && port <= eDEVBRD_100G_NETWORK_1 ) {
        strncpy(dev_name, "axigpio/12", DEVMAN_DEVNAME_LEN);      
        /* prepare significant attribute bits  */
        if (port == eDEVBRD_100G_NETWORK_0) 
            *p_sign_mask = 1 << 29;
        else
            *p_sign_mask = 1 << 28;
    } else if ( port >= eDEVBRD_100G_CLIENT_0 && port <= eDEVBRD_100G_CLIENT_1 ) {
        strncpy(dev_name, "axigpio/10", DEVMAN_DEVNAME_LEN);      
        /* prepare significant attribute bits  */
        if (port == eDEVBRD_100G_CLIENT_0) 
            *p_sign_mask = 1 << 29;
        else
            *p_sign_mask = 1 << 28;
    } else
        return CCL_NOT_SUPPORT;

    strncpy(attr_name, "data", DEVMAN_DEVNAME_LEN);      

    return CCL_OK;
}

/* Get appropriate device/attr/significant_mask for port "media_pres" operating 
 */
ccl_err_t devboard_portprs_to_devattr(int port, char dev_name[], 
                                      char attr_name[], b_u32 *p_sign_mask, 
                                      int *p_is_active_hi)
{
    /* check input parameters */
    if ( !p_sign_mask || !p_is_active_hi ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* actual signal is module present */
    *p_is_active_hi = 1;
    /* get target device name/output port attribute name */
    if ( port >= eDEVBRD_10G_NETWORK_0 && port <= eDEVBRD_10G_NETWORK_11 || 
         port >= eDEVBRD_100G_NETWORK_0 && port <= eDEVBRD_100G_NETWORK_1 ) {
        /* NETWORK*/
        strncpy(dev_name, "axigpio/12", DEVMAN_DEVNAME_LEN);      
        /* prepare significant attribute bits  */
        if (port >= eDEVBRD_10G_NETWORK_0 && port <= eDEVBRD_10G_NETWORK_11) 
            *p_sign_mask = 1 << port;
        else if (port == eDEVBRD_100G_NETWORK_0) 
            *p_sign_mask = 1 << 13;
        else 
            *p_sign_mask = 1 << 12;
        

    } else if ( port >= eDEVBRD_10G_CLIENT_0 && port <= eDEVBRD_10G_CLIENT_11 ||
                port >= eDEVBRD_100G_CLIENT_0 && port <= eDEVBRD_100G_CLIENT_1 ) {
        /*CLIENT*/
        strncpy(dev_name, "axigpio/10", DEVMAN_DEVNAME_LEN);      
        /* prepare significant attribute bits  */
        if (port >= eDEVBRD_10G_CLIENT_0 && port <= eDEVBRD_10G_CLIENT_11) 
            *p_sign_mask = 1 << (port - 12);
        else if (port == eDEVBRD_100G_CLIENT_0) 
            *p_sign_mask = 1 << 13;
        else 
            *p_sign_mask = 1 << 12;
    } else
        return CCL_NOT_SUPPORT;

    strncpy(attr_name, "data", DEVMAN_DEVNAME_LEN);     

    return CCL_OK;
}

/* Get appropriate device/attr/significant_mask for port "tx_fault" operating  
 */
ccl_err_t devboard_porttxf_to_devattr(int port, 
                                      char dev_name[], 
                                      char attr_name[], 
                                      b_u8 *p_sign_mask)
{
    b_u32  offset = 0;

    /* check input parameters */
    if ( !p_sign_mask ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* get target i2cio device name/output port attribute name */
    if ( port >= eDEVBRD_10G_NETWORK_0 && port <= eDEVBRD_10G_NETWORK_3 ) {
        strncpy(dev_name, "i2cio/0", DEVMAN_DEVNAME_LEN);      
        /* prepare significant attribute bits offset */
        offset = 0;
    } else if ( port >= eDEVBRD_10G_NETWORK_4 && port <= eDEVBRD_10G_NETWORK_7 ) {
        strncpy(dev_name, "i2cio/1", DEVMAN_DEVNAME_LEN);      
        offset = 4;
    } else if ( port >= eDEVBRD_10G_NETWORK_8 && port <= eDEVBRD_10G_NETWORK_11 ) {
        strncpy(dev_name, "i2cio/2", DEVMAN_DEVNAME_LEN);      
        offset = 8;
    } else if ( port >= eDEVBRD_10G_CLIENT_0 && port <= eDEVBRD_10G_CLIENT_3 ) {
        strncpy(dev_name, "i2cio/4", DEVMAN_DEVNAME_LEN);      
        offset = 14;
    } else if ( port >= eDEVBRD_10G_CLIENT_4 && port <= eDEVBRD_10G_CLIENT_7 ) {
        strncpy(dev_name, "i2cio/5", DEVMAN_DEVNAME_LEN);      
        offset = 18;
    } else if ( port >= eDEVBRD_10G_CLIENT_8 && port <= eDEVBRD_10G_CLIENT_11 ) {
        strncpy(dev_name, "i2cio/6", DEVMAN_DEVNAME_LEN);      
        offset = 22;
    } else
        return CCL_NOT_SUPPORT;

    strncpy(attr_name, "oport7_0", DEVMAN_DEVNAME_LEN);      
    /* prepare significant attribute bits */
    *p_sign_mask = 1 << (1 + 2*(port-offset));   //TODO: check offsets

    return CCL_OK;
}

/* Get appropriate device/attr/significant_mask for port "rx_los" operating  
 */
ccl_err_t devboard_portrxlos_to_devattr(int port, 
                                        char dev_name[], 
                                        char attr_name[], 
                                        b_u8 *p_sign_mask)
{
    b_u32  offset = 0;

    /* check input parameters */
    if ( !p_sign_mask ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* get target i2cio device name/output port attribute name */
    if ( port >= eDEVBRD_10G_NETWORK_0 && port <= eDEVBRD_10G_NETWORK_3 ) {
        strncpy(dev_name, "i2cio/0", DEVMAN_DEVNAME_LEN);      
        /* prepare significant attribute bits offset */
        offset = 0;
    } else if ( port >= eDEVBRD_10G_NETWORK_4 && port <= eDEVBRD_10G_NETWORK_7 ) {
        strncpy(dev_name, "i2cio/1", DEVMAN_DEVNAME_LEN);      
        offset = 4;
    } else if ( port >= eDEVBRD_10G_NETWORK_8 && port <= eDEVBRD_10G_NETWORK_11 ) {
        strncpy(dev_name, "i2cio/2", DEVMAN_DEVNAME_LEN);      
        offset = 8;
    } else if ( port >= eDEVBRD_10G_CLIENT_0 && port <= eDEVBRD_10G_CLIENT_3 ) {
        strncpy(dev_name, "i2cio/4", DEVMAN_DEVNAME_LEN);      
        offset = 14;
    } else if ( port >= eDEVBRD_10G_CLIENT_4 && port <= eDEVBRD_10G_CLIENT_7 ) {
        strncpy(dev_name, "i2cio/5", DEVMAN_DEVNAME_LEN);      
        offset = 18;
    } else if ( port >= eDEVBRD_10G_CLIENT_8 && port <= eDEVBRD_10G_CLIENT_11 ) {
        strncpy(dev_name, "i2cio/6", DEVMAN_DEVNAME_LEN);      
        offset = 22;
    } else
        return CCL_NOT_SUPPORT;

    strncpy(attr_name, "oport7_0", DEVMAN_DEVNAME_LEN);      
    /* prepare significant attribute bits */
    *p_sign_mask = 1 << 2*(port-offset);     //TODO: check offsets

    return CCL_OK;
}

/* Get appropriate device/attr/significant_mask for port "rs" operating  
 */
ccl_err_t devboard_portrs_to_devattr(int port, 
                                     b_u8 rs_id,
                                     char dev_name[], 
                                     char attr_name[], 
                                     b_u8 *p_sign_mask)
{
    b_u32  offset = 0;

    /* check input parameters */
    if ( !p_sign_mask  || rs_id >= DEVBRD_TRANS_RS_ID_MAXNUM) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* get target i2cio device name/output port attribute name */
    if ( port >= eDEVBRD_10G_NETWORK_0 && port <= eDEVBRD_10G_NETWORK_3 ) {
        strncpy(dev_name, "i2cio/0", DEVMAN_DEVNAME_LEN);      
        /* prepare significant attribute bits offset */
        offset = 0;
    } else if ( port >= eDEVBRD_10G_NETWORK_4 && port <= eDEVBRD_10G_NETWORK_7 ) {
        strncpy(dev_name, "i2cio/1", DEVMAN_DEVNAME_LEN);      
        offset = 4;
    } else if ( port >= eDEVBRD_10G_NETWORK_8 && port <= eDEVBRD_10G_NETWORK_11 ) {
        strncpy(dev_name, "i2cio/2", DEVMAN_DEVNAME_LEN);      
        offset = 8;
    } else if ( port >= eDEVBRD_10G_CLIENT_0 && port <= eDEVBRD_10G_CLIENT_3 ) {
        strncpy(dev_name, "i2cio/4", DEVMAN_DEVNAME_LEN);      
        offset = 14;
    } else if ( port >= eDEVBRD_10G_CLIENT_4 && port <= eDEVBRD_10G_CLIENT_7 ) {
        strncpy(dev_name, "i2cio/5", DEVMAN_DEVNAME_LEN);      
        offset = 18;
    } else if ( port >= eDEVBRD_10G_CLIENT_8 && port <= eDEVBRD_10G_CLIENT_11 ) {
        strncpy(dev_name, "i2cio/6", DEVMAN_DEVNAME_LEN);      
        offset = 22;
    } else
        return CCL_NOT_SUPPORT;

    strncpy(attr_name, "oport15_8", DEVMAN_DEVNAME_LEN);     
    /* prepare significant attribute bits */
    *p_sign_mask = 1 << (8 + 2*(port-offset) + rs_id);    //TODO: check offsets

    return CCL_OK;
}

ccl_err_t devboard_portmodsell_to_devattr(int port, 
                                          char dev_name[], 
                                          char attr_name[], 
                                          b_u8 *p_sign_mask)
{
    b_u32  offset = 0;

    /* check input parameters */
    if ( !p_sign_mask ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* get target i2cio device name/output port attribute name */
    if ( port >= eDEVBRD_10G_NETWORK_0 && port <= eDEVBRD_10G_NETWORK_1 ) {
        strncpy(dev_name, "i2cio/3", DEVMAN_DEVNAME_LEN);      
        /* prepare significant attribute bits offset */
        offset = 12;
    } else if ( port >= eDEVBRD_10G_CLIENT_0 && port <= eDEVBRD_10G_CLIENT_1 ) {
        strncpy(dev_name, "i2cio/7", DEVMAN_DEVNAME_LEN);      
        offset = 26;
    } else
        return CCL_NOT_SUPPORT;

    strncpy(attr_name, "oport7_0", DEVMAN_DEVNAME_LEN);      
    /* prepare significant attribute bits */
    *p_sign_mask = 1 << 4*(port-offset);      //TODO: check offsets

    return CCL_OK;
}

ccl_err_t devboard_portlpmode_to_devattr(int port, 
                                         char dev_name[], 
                                         char attr_name[], 
                                         b_u8 *p_sign_mask)
{
    b_u32  offset = 0;

    /* check input parameters */
    if ( !p_sign_mask ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* get target i2cio device name/output port attribute name */
    if ( port >= eDEVBRD_10G_NETWORK_0 && port <= eDEVBRD_10G_NETWORK_1 ) {
        strncpy(dev_name, "i2cio/3", DEVMAN_DEVNAME_LEN);      
        /* prepare significant attribute bits offset */
        offset = 12;
    } else if ( port >= eDEVBRD_10G_CLIENT_0 && port <= eDEVBRD_10G_CLIENT_1 ) {
        strncpy(dev_name, "i2cio/7", DEVMAN_DEVNAME_LEN);      
        offset = 26;
    } else
        return CCL_NOT_SUPPORT;

    strncpy(attr_name, "oport7_0", DEVMAN_DEVNAME_LEN);      
    /* prepare significant attribute bits */
    *p_sign_mask = 1 << (2 + 4*(port-offset));  //TODO: check offsets

    return CCL_OK;
}

/* Get appropriate device/attr/significant_mask for port "init" operating 
 */
ccl_err_t devboard_portinit_to_devattr(int port, char dev_name[], 
                                       char attr_name[], int *discr)
{
    /* check input parameters */
    if ( port < 0 || port > BRDDEV_PORTS_NUM ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    if ( port >= eDEVBRD_10G_NETWORK_0 && port <= eDEVBRD_10G_NETWORK_11 ||         /* Black 10G */
         port >= eDEVBRD_10G_CLIENT_0 && port <= eDEVBRD_10G_CLIENT_11 ||             /* Red 10G */
         port >= eDEVBRD_KR_COMX0_0 && port <= eDEVBRD_KR_COMX3_3 )            /* Backplane 10G */
        strncpy(dev_name, "hses10g", DEVMAN_DEVNAME_LEN);      
    else if ( port >= eDEVBRD_100G_NETWORK_0 && port <= eDEVBRD_100G_NETWORK_1 || /* Black 100G */
                port >= eDEVBRD_100G_CLIENT_0 && port <= eDEVBRD_100G_CLIENT_1 )     /* Red 100G */
        strncpy(dev_name, "usplus100g", DEVMAN_DEVNAME_LEN);      
        /* prepare significant attribute bits  */
    else if (port >= eDEVBRD_1G_AV_INTERCHIP_0 && port <= eDEVBRD_1G_VA_INTERCHIP_1)   /* 1G */
        strncpy(dev_name, "temac", DEVMAN_DEVNAME_LEN);      
    else
        return CCL_NOT_SUPPORT;

    strncpy(attr_name, "init", DEVMAN_DEVNAME_LEN);      
    _ret = _fboard_portid_to_devdescr(port, discr);
    if (_ret) {
        PDEBUG("Error! _fboard_portid_to_devdescr failed\n");
        DEVBOARD_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

/* Get appropriate device/attr/significant_mask for port "read_counters" operating 
 */
ccl_err_t devboard_portcntr_to_devattr(int port, char dev_name[], 
                                       char attr_name[], int *discr)
{
    /* check input parameters */
    if ( port < 0 || port > BRDDEV_PORTS_NUM ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    if ( port >= eDEVBRD_10G_NETWORK_0 && port <= eDEVBRD_10G_NETWORK_11 ||         /* Black 10G */
         port >= eDEVBRD_10G_CLIENT_0 && port <= eDEVBRD_10G_CLIENT_11 ||           /* Red 10G */
         port >= eDEVBRD_KR_COMX0_0 && port <= eDEVBRD_KR_COMX3_3 )                 /* Backplane 10G */
        strncpy(dev_name, "hses10g", DEVMAN_DEVNAME_LEN);      
    else if ( port >= eDEVBRD_100G_NETWORK_0 && port <= eDEVBRD_100G_NETWORK_1 ||   /* Black 100G */
                port >= eDEVBRD_100G_CLIENT_0 && port <= eDEVBRD_100G_CLIENT_1 )    /* Red 100G */
        strncpy(dev_name, "usplus100g", DEVMAN_DEVNAME_LEN);      
        /* prepare significant attribute bits  */
    else if (port >= eDEVBRD_1G_AV_INTERCHIP_0 && port <= eDEVBRD_1G_VA_INTERCHIP_1) /* 1G */
        strncpy(dev_name, "temac", DEVMAN_DEVNAME_LEN);      
    else
        return CCL_NOT_SUPPORT;

    strncpy(attr_name, "read_single_counter", DEVMAN_DEVNAME_LEN);      
    _ret = _fboard_portid_to_devdescr(port, discr);
    if (_ret) {
        PDEBUG("Error! _fboard_portid_to_devdescr failed\n");
        DEVBOARD_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

/* Get appropriate device/attr/significant_mask for port "property" operating 
 */
ccl_err_t devboard_portprop_to_devattr(int port, char dev_name[], 
                                       char attr_name[], int *discr)
{
    /* check input parameters */
    if ( port < 0 || port > BRDDEV_PORTS_NUM ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    if ( port >= eDEVBRD_10G_NETWORK_0 && port <= eDEVBRD_10G_NETWORK_11 ||         /* Black 10G */
         port >= eDEVBRD_10G_CLIENT_0 && port <= eDEVBRD_10G_CLIENT_11 ||           /* Red 10G */
         port >= eDEVBRD_KR_COMX0_0 && port <= eDEVBRD_KR_COMX3_3 )                 /* Backplane 10G */
        strncpy(dev_name, "hses10g", DEVMAN_DEVNAME_LEN);      
    else if ( port >= eDEVBRD_100G_NETWORK_0 && port <= eDEVBRD_100G_NETWORK_1 ||   /* Black 100G */
                port >= eDEVBRD_100G_CLIENT_0 && port <= eDEVBRD_100G_CLIENT_1 )    /* Red 100G */
        strncpy(dev_name, "usplus100g", DEVMAN_DEVNAME_LEN);      
        /* prepare significant attribute bits  */
    else if (port >= eDEVBRD_1G_AV_INTERCHIP_0 && port <= eDEVBRD_1G_VA_INTERCHIP_1) /* 1G */
        strncpy(dev_name, "temac", DEVMAN_DEVNAME_LEN);      
    else
        return CCL_NOT_SUPPORT;

    strncpy(attr_name, "property", DEVMAN_DEVNAME_LEN);      
    _ret = _fboard_portid_to_devdescr(port, discr);
    if (_ret) {
        PDEBUG("Error! _fboard_portid_to_devdescr failed\n");
        DEVBOARD_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

/* Get appropriate device/attr/significant_mask for port "read_counters" operating 
 */
ccl_err_t devboard_portcntrs_to_devattr(int port, char dev_name[], 
                                        char attr_name[], int *discr)
{
    /* check input parameters */
    if ( port < 0 || port > BRDDEV_PORTS_NUM ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    if ( port >= eDEVBRD_10G_NETWORK_0 && port <= eDEVBRD_10G_NETWORK_11 ||         /* Black 10G */
         port >= eDEVBRD_10G_CLIENT_0 && port <= eDEVBRD_10G_CLIENT_11 ||             /* Red 10G */
         port >= eDEVBRD_KR_COMX0_0 && port <= eDEVBRD_KR_COMX3_3 )            /* Backplane 10G */
        strncpy(dev_name, "hses10g", DEVMAN_DEVNAME_LEN);      
    else if ( port >= eDEVBRD_100G_NETWORK_0 && port <= eDEVBRD_100G_NETWORK_1 || /* Black 100G */
                port >= eDEVBRD_100G_CLIENT_0 && port <= eDEVBRD_100G_CLIENT_1 )     /* Red 100G */
        strncpy(dev_name, "usplus100g", DEVMAN_DEVNAME_LEN);      
        /* prepare significant attribute bits  */
    else if (port >= eDEVBRD_1G_AV_INTERCHIP_0 && port <= eDEVBRD_1G_VA_INTERCHIP_1)   /* 1G */
        strncpy(dev_name, "temac", DEVMAN_DEVNAME_LEN);      
    else
        return CCL_NOT_SUPPORT;

    strncpy(attr_name, "read_counters", DEVMAN_DEVNAME_LEN);      
    _ret = _fboard_portid_to_devdescr(port, discr);
    if (_ret) {
        PDEBUG("Error! _fboard_portid_to_devdescr failed\n");
        DEVBOARD_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

/* Get appropriate device/attr/significant_mask for port "counters_reset" operating 
 */
ccl_err_t devboard_portcntrsrst_to_devattr(int port, char dev_name[], 
                                           char attr_name[], int *discr)
{
    /* check input parameters */
    if ( port < 0 || port > BRDDEV_PORTS_NUM ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    if ( port >= eDEVBRD_10G_NETWORK_0 && port <= eDEVBRD_10G_NETWORK_11 ||         /* Black 10G */
         port >= eDEVBRD_10G_CLIENT_0 && port <= eDEVBRD_10G_CLIENT_11 ||             /* Red 10G */
         port >= eDEVBRD_KR_COMX0_0 && port <= eDEVBRD_KR_COMX3_3 )            /* Backplane 10G */
        strncpy(dev_name, "hses10g", DEVMAN_DEVNAME_LEN);      
    else if ( port >= eDEVBRD_100G_NETWORK_0 && port <= eDEVBRD_100G_NETWORK_1 || /* Black 100G */
                port >= eDEVBRD_100G_CLIENT_0 && port <= eDEVBRD_100G_CLIENT_1 )     /* Red 100G */
        strncpy(dev_name, "usplus100g", DEVMAN_DEVNAME_LEN);      
        /* prepare significant attribute bits  */
    else
        return CCL_NOT_SUPPORT;

    strncpy(attr_name, "counters_reset", DEVMAN_DEVNAME_LEN);      
    _ret = _fboard_portid_to_devdescr(port, discr);
    if (_ret) {
        PDEBUG("Error! _fboard_portid_to_devdescr failed\n");
        DEVBOARD_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

/* Get appropriate device/attr/significant_mask for port "traffic gen" operating 
 */
ccl_err_t devboard_porttrgen_to_devattr(int port, char dev_name[], 
                                        char attr_name[])
{
    /* check input parameters */
    if ( port < 0 || port > BRDDEV_PORTS_NUM ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    if ( port >= eDEVBRD_10G_NETWORK_0 && port <= eDEVBRD_10G_NETWORK_11 ||         /* Black 10G */
         port >= eDEVBRD_100G_NETWORK_0 && port <= eDEVBRD_100G_NETWORK_1 ) {       /* Black 100G */
        strncpy(dev_name, "axigpio/14", DEVMAN_DEVNAME_LEN);      
        strncpy(attr_name, "data", DEVMAN_DEVNAME_LEN);      
    } else if ( port >= eDEVBRD_10G_CLIENT_0 && port <= eDEVBRD_10G_CLIENT_11 ||      /* Red 10G */
                port >= eDEVBRD_100G_CLIENT_0 && port <= eDEVBRD_100G_CLIENT_1 ) {    /* Red 100G */
        strncpy(dev_name, "axigpio/13", DEVMAN_DEVNAME_LEN);      
        strncpy(attr_name, "data", DEVMAN_DEVNAME_LEN);     
    } else if ( port >= eDEVBRD_KR_COMX0_0 && port <= eDEVBRD_KR_COMX1_3 ||     /* COMX0, COMX1 */
                port == eDEVBRD_1G_VA_INTERCHIP_0 ) {                           /* 1G Virtex interchip 0 */
        strncpy(dev_name, "axigpio/13", DEVMAN_DEVNAME_LEN);      
        strncpy(attr_name, "data2", DEVMAN_DEVNAME_LEN);     
    } else if ( port >= eDEVBRD_KR_COMX2_0 && port <= eDEVBRD_KR_COMX3_3 ||     /* COMX2, COMX3 */
                port == eDEVBRD_1G_VA_INTERCHIP_1 ) {                           /* 1G Virtex interchip 1 */
        strncpy(dev_name, "axigpio/14", DEVMAN_DEVNAME_LEN);      
        strncpy(attr_name, "data2", DEVMAN_DEVNAME_LEN);     
    } else if (port >= eDEVBRD_1G_AV_INTERCHIP_0 && port <= eDEVBRD_1G_OOB ) {  /* 1G Artix ports */
        strncpy(dev_name, "axigpio/15", DEVMAN_DEVNAME_LEN);      
        strncpy(attr_name, "data", DEVMAN_DEVNAME_LEN);     
    } else
        return CCL_NOT_SUPPORT;

    return CCL_OK;
}

/* Run Self Tests for all board types/devices
 */
ccl_err_t devboard_bist(void)
{
    return CCL_OK;
}

/* Initialize all board types/devices
 */
ccl_err_t devboard_init(__attribute__((unused)) b_u32 flags)
{
    /* i2cbus initialization */
    _ret = _fboard_i2cbus_init(&_i2cbus_brd_descr[0], 
                               sizeof(_i2cbus_brd_descr)/sizeof(_i2cbus_brd_descr[0]));
    if ( _ret ) {
        PDEBUG("Error! Can't initialize I2CBUS devices\n");
        DEVBOARD_ERROR_RETURN(CCL_FAIL);
    }

    /* i2ccore board initialization */
    _ret = _fboard_i2ccore_init(&_i2ccore_brd_descr[0], 
                                sizeof(_i2ccore_brd_descr)/sizeof(_i2ccore_brd_descr[0]));
    if ( _ret ) {
        PDEBUG("Error! Can't initialize I2CCORE devices\n");
        DEVBOARD_ERROR_RETURN(CCL_FAIL);
    }

    /* axi gpio board initialization */
    _ret = _fboard_axigpio_init(&_axigpio_brd_descr[0], 
                                sizeof(_axigpio_brd_descr)/sizeof(_axigpio_brd_descr[0]));
    if ( _ret ) {
        PDEBUG("Error! Can't initialize AXI GPIO devices\n");
        DEVBOARD_ERROR_RETURN(CCL_FAIL);
    }

    /* axi spi initialization */
    _ret = _fboard_axispi_init(&_axispi_brd_descr[0], 
                               sizeof(_axispi_brd_descr)/sizeof(_axispi_brd_descr[0]));
    if ( _ret ) {
        PDEBUG("Error! Can't initialize AXI SPI devices\n");
        DEVBOARD_ERROR_RETURN(CCL_FAIL);
    }

    /* axi uart initialization */
    _ret = _fboard_axiuart_init(&_axiuart_brd_descr[0], 
                                sizeof(_axiuart_brd_descr)/sizeof(_axiuart_brd_descr[0]));
    if ( _ret ) {
        PDEBUG("Error! Can't initialize AXI UART devices\n");
        DEVBOARD_ERROR_RETURN(CCL_FAIL);
    }

    /* axi intc initialization */
    _ret = _fboard_axiintc_init(&_intc_brd_descr[0], 
                                sizeof(_intc_brd_descr)/sizeof(_intc_brd_descr[0]));
    if ( _ret ) {
        PDEBUG("Error! Can't initialize AXI INTC devices\n");
        DEVBOARD_ERROR_RETURN(CCL_FAIL);
    }

    /* xadc board initialization */
    _ret = _fboard_xadc_init(&_xadc_brd_descr[0], 
                             sizeof(_xadc_brd_descr)/sizeof(_xadc_brd_descr[0]));
    if ( _ret ) {
        PDEBUG("Error! Can't initialize XADC devices\n");
        DEVBOARD_ERROR_RETURN(CCL_FAIL);
    }

    /* sysmon initialization */
    _ret = _fboard_sysmon_init(&_sysmon_brd_descr[0], 
                               sizeof(_sysmon_brd_descr)/sizeof(_sysmon_brd_descr[0]));
    if ( _ret ) {
        PDEBUG("Error! Can't initialize Sysmon devices\n");
        DEVBOARD_ERROR_RETURN(CCL_FAIL);
    }

    /* mbox initialization */
    _ret = _fboard_mbox_init(&_mbox_brd_descr[0], 
                             sizeof(_mbox_brd_descr)/sizeof(_mbox_brd_descr[0]));
    if ( _ret ) {
        PDEBUG("Error! Can't initialize Sysmon devices\n");
        DEVBOARD_ERROR_RETURN(CCL_FAIL);
    }

    /* 1G ports (temac) initialization */
    _ret = _fboard_temac_init(&_temac_brd_descr[0], 
                              sizeof(_temac_brd_descr)/sizeof(_temac_brd_descr[0]));
    if ( _ret ) {
        PDEBUG("Error! Can't initialize TEMAC devices\n");
        DEVBOARD_ERROR_RETURN(CCL_FAIL);
    }
    /* 10G ports initialization */
    _ret = _fboard_hses10g_init(&_hses10g_brd_descr[0], 
                                sizeof(_hses10g_brd_descr)/sizeof(_hses10g_brd_descr[0]));
    if ( _ret ) {
        PDEBUG("Error! Can't initialize 10G (HSES10G) devices\n");
        DEVBOARD_ERROR_RETURN(CCL_FAIL);
    }
    /* 100G ports initialization */
    _ret = _fboard_usplus100g_init(&_usplus100g_brd_descr[0], 
                                   sizeof(_usplus100g_brd_descr)/sizeof(_usplus100g_brd_descr[0]));
    if ( _ret ) {
        PDEBUG("Error! Can't initialize 100G (USPLUS100G) devices\n");
        DEVBOARD_ERROR_RETURN(CCL_FAIL);
    }

    /* spi devices initialization */
    _ret = _fboard_spidev_init(&_spidev_brd_descr[0], 
                               sizeof(_spidev_brd_descr)/sizeof(_spidev_brd_descr[0]));
    if ( _ret ) {
        PDEBUG("Error! Can't initialize SPI devices\n");
        DEVBOARD_ERROR_RETURN(CCL_FAIL);
    }

    /* i2c devices initialization */
    _ret = _fboard_i2c_devices_init();
    if ( _ret ) {
        PDEBUG("Error! Can't initialize I2C devices\n");
        DEVBOARD_ERROR_RETURN(CCL_FAIL);
    }

    /* transceivers initialization */
    _ret = _fboard_trans_init(&_trans_brd_descr[0], 
                              sizeof(_trans_brd_descr)/sizeof(_trans_brd_descr[0]));
    if ( _ret ) {
        PDEBUG("Error! Can't initialize Transceiver devices\n");
        DEVBOARD_ERROR_RETURN(CCL_FAIL);
    }

    /* portmap initialization */
    _ret = _fboard_portmap_init(&_portmap_brd_descr[0], 
                                sizeof(_portmap_brd_descr)/sizeof(_portmap_brd_descr[0]));
    if ( _ret ) {
        PDEBUG("Error! Can't initialize portmap devices\n");
        DEVBOARD_ERROR_RETURN(CCL_FAIL);
    }

    /* spibus initialization */
    _ret = _fboard_spibus_init(&_spibus_brd_descr[0], 
                             sizeof(_spibus_brd_descr)/sizeof(_spibus_brd_descr[0]));
    if ( _ret ) {
        PDEBUG("Error! Can't initialize spibus device\n");
        DEVBOARD_ERROR_RETURN(CCL_FAIL);
    }

    /* smartcard module initialization */
    _ret = _fboard_smartcard_init(&_smartcard_brd_descr[0], 
                                  sizeof(_smartcard_brd_descr)/sizeof(_smartcard_brd_descr[0]));
    if ( _ret ) {
        PDEBUG("Error! Can't initialize smartcard device\n");
        DEVBOARD_ERROR_RETURN(CCL_FAIL);
    }

    /* atp module initialization */
    _ret = _fboard_atp_init(&_atp_brd_descr[0], 
                             sizeof(_atp_brd_descr)/sizeof(_atp_brd_descr[0]));
    if ( _ret ) {
        PDEBUG("Error! Can't initialize atp device\n");
        DEVBOARD_ERROR_RETURN(CCL_FAIL);
    }

    /* cpu module initialization */
    _ret = _fboard_cpu_init(&_cpu_brd_descr[0], 
                             sizeof(_cpu_brd_descr)/sizeof(_cpu_brd_descr[0]));
    if ( _ret ) {
        PDEBUG("Error! Can't initialize cpu device\n");
        DEVBOARD_ERROR_RETURN(CCL_FAIL);
    }

    /* dram tester module initialization */
    _ret = _fboard_dramtester_init(&_dramtester_brd_descr[0], 
                                   sizeof(_dramtester_brd_descr)/sizeof(_dramtester_brd_descr[0]));
    if ( _ret ) {
        PDEBUG("Error! Can't initialize cpu device\n");
        DEVBOARD_ERROR_RETURN(CCL_FAIL);
    }

    /* eswitch board initialization */
    _ret = _fboard_eswitch_init(&_eswitch_brd_descr[0],
                              sizeof(_eswitch_brd_descr)/sizeof(_eswitch_brd_descr[0]));
    if ( _ret ) {
      PDEBUG("Error! Can't initialize Ethernet Switch device\n");
      DEVBOARD_ERROR_RETURN(CCL_FAIL);
    }

    /* hal module initialization */
    _ret = _fboard_hal_init(&_hal_brd_descr[0], 
                             sizeof(_hal_brd_descr)/sizeof(_hal_brd_descr[0]));
    if ( _ret ) {
        PDEBUG("Error! Can't initialize atp device\n");
        DEVBOARD_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

/* Deinitialize all board devices/types
 */
ccl_err_t devboard_fini(void)
{
    return CCL_OK;
}
