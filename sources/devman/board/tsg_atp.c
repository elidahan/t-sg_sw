/********************************************************************************/
/**
 * @file atp.c
 * @brief ATP device manager driver 
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include <linux/limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include <fcntl.h> 
#include <termios.h>
#include <unistd.h>
#include <math.h>

#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "ccl_stat_msg.h"
#include "ccl_stat_sender.h"
#include "http_cluster_node.h"
#include "devman.h"
#include "devman_mod.h"
#include "devboard.h"
#include "spibus.h"
#include "axispi.h"
#include "tsg.h"

extern ccl_logger_t logger;

#define ATP_DEBUG
/* printing/error-returning macros */
#ifdef ATP_DEBUG
    #define PDEBUG(fmt, args...) \
         ccl_syslog_err("ATP: %s[%d]: " fmt, __FUNCTION__, __LINE__, ## args)
#else
    #define PDEBUG(fmt, args...)
#endif
#define ATP_ERROR_RETURN(err) do { \
   ccl_syslog_err("Error! %s[%d], error=%i\n", __FUNCTION__, __LINE__, (err)); \
   return err; \
} while (0)

#ifdef ATP_APPLY_MUTEX
#define ATP_MUTEX_LOCK(mutex) do {\
    _rc = pthread_mutex_lock(&mutex); \
    if (_rc) { \
        PDEBUG("pthread_mutex_lock error\n"); \
        ATP_ERROR_RETURN(CCL_FAIL); \
    } \
} while (0)

#define ATP_MUTEX_UNLOCK(mutex) do {\
    _rc = pthread_mutex_unlock(&mutex); \
    if (_rc) { \
        PDEBUG("pthread_mutex_unlock error\n"); \
        ATP_ERROR_RETURN(CCL_FAIL); \
    } \
} while (0)
#else 
#define ATP_MUTEX_LOCK(mutex)
#define ATP_MUTEX_UNLOCK(mutex)
#endif 

#define FPGA_GPIO_BOARD_ID_BANK 4
#define FPGA_GPIO_BOARD_ID_IO   6
#define IMX_GPIO_NR(bank, nr)		(((bank) - 1) * 32 + (nr))

#define FLASH_MAX_ID                11
#define UART_MAX_ID                 9
#define EEPROM_MAX_ID               10
#define RTC_MAX_ID                  3

#define ARTIX_IMAGE_FILENAME_MAXLEN 50

#define DATA_ARRAY_LEN              16


typedef struct {
    b_bool  enable;
    b_u32   index;
    b_u8    type[DEVMAN_CMD_PARM_LEN+1];
} es_config_t;

typedef struct {
    char cpu[DEVMAN_DEVNAME_LEN];
    char artix[DEVMAN_DEVNAME_LEN];
    char virtex[DEVMAN_DEVNAME_LEN];
} build_time_t;

typedef struct kr_tester kr_tester_t;
typedef void (*kr_test_fn_t)(kr_tester_t *kr);

struct kr_tester {
    kr_test_fn_t kr_tester;
    timer_t      timer;
    b_u32        interval;
    b_u32 cpu;                 /**< 0-3 */
    b_u32 link;                /**< 0-3 */
    b_u8  rx_block_lock;       /**< STAT_RX_BLOCK_LOCK b.0 (LL) */
    b_u8  an_autoneg_complete; /**< STAT_AN_STATUS b.2 */
    b_u8  lt_training;         /**< STAT_LT_STATUS_REG2 b.0 */
    b_u8  lt_frame_lock;       /**< STAT_LT_STATUS_REG2 b.16 */
    b_u8  lt_signal_detect;    /**< STAT_LT_STATUS_REG3 b.0 */
    b_u8  lt_training_fail;    /**< STAT_LT_STATUS_REG3 b.16 */
    b_u8  rx_remote_fault;     /**< STAT_RX_STATUS_REG1 b.5 (LH) */
    b_u8  rx_local_fault;      /**< STAT_RX_STATUS_REG1 b.6 (LH) */
    b_u8  rx_int_local_fault;  /**< STAT_RX_STATUS_REG1 b.7 (LH) */
    b_u8  rx_rec_local_fault;  /**< STAT_RX_STATUS_REG1 b.8 (LH) */
};

/** @struct atp_ctx_t
 *  atp device internal control structure
 */
typedef struct {
    void                     *devo;
    atp_brdspec_t            brdspec;
    pthread_mutex_t          mutex;
    b_u8                     board_id;
    kr_tester_t              kr_tester; 
    ccl_stat_sender_t        stat_sender;
    http_cluster_node_t      node;
    http_cluster_node_conn_t node_conn;
    es_config_t              es_config;
    build_time_t             build_time;
} atp_ctx_t;

static ccl_err_t _ret;
static int       _rc;

static void _prepare_board_stat(ccl_stat_msg_t *stat_msg, void *priv);

static ccl_err_t _flash_id2sig(b_u32 id, b_u32 *discr)
{
    if ( id > FLASH_MAX_ID || !discr )
    {
        PDEBUG("Error! Invalid input parameters - id=%d,discr=%p\n", 
               id, discr);
        ATP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    switch (id) {
    case 0:
        *discr = eDEVBRD_SPIDEV_4_SIG & BRDDEV_MINOR_MASK;
        break;
    case 1:
        *discr = eDEVBRD_SPIDEV_5_SIG & BRDDEV_MINOR_MASK;
        break;
    case 2:
        *discr = eDEVBRD_SPIDEV_6_SIG & BRDDEV_MINOR_MASK;
        break;
    case 3:
        *discr = eDEVBRD_SPIDEV_7_SIG & BRDDEV_MINOR_MASK;
        break;
    case 4:
        *discr = eDEVBRD_SPIDEV_8_SIG & BRDDEV_MINOR_MASK;
        break;
    case 5:
        *discr = eDEVBRD_SPIDEV_9_SIG & BRDDEV_MINOR_MASK;
        break;
    case 6:
        *discr = eDEVBRD_SPIDEV_0_SIG & BRDDEV_MINOR_MASK;
        break;
    case 7:
        *discr = eDEVBRD_SPIDEV_1_SIG & BRDDEV_MINOR_MASK;
        break;
    case 10:
        *discr = eDEVBRD_SPIDEV_2_SIG & BRDDEV_MINOR_MASK;
        break;
    case 11:
        *discr = eDEVBRD_SPIDEV_3_SIG & BRDDEV_MINOR_MASK;
        break;
    default:
        PDEBUG("Error! unsupported id=%d\n", 
               id);
        ATP_ERROR_RETURN(CCL_NOT_SUPPORT);
    }
    return CCL_OK;
}

static ccl_err_t _uart_id2sig(b_u32 id, b_u32 *discr)
{
    if ( id > UART_MAX_ID || !discr )
    {
        PDEBUG("Error! Invalid input parameters - id=%d,discr=%p\n", 
               id, discr);
        ATP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    switch (id) {
    case 1:
        *discr = eDEVBRD_AXIUART_0_SIG & BRDDEV_MINOR_MASK;
        break;
    case 2:
        *discr = eDEVBRD_AXIUART_1_SIG & BRDDEV_MINOR_MASK;
        break;
    case 3:
        *discr = eDEVBRD_AXIUART_2_SIG & BRDDEV_MINOR_MASK;
        break;
    case 4:
        *discr = eDEVBRD_AXIUART_3_SIG & BRDDEV_MINOR_MASK;
        break;
    case 5:
        *discr = eDEVBRD_AXIUART_4_SIG & BRDDEV_MINOR_MASK;
        break;
    case 6:
        *discr = eDEVBRD_AXIUART_5_SIG & BRDDEV_MINOR_MASK;
        break;
    case 7:
        *discr = eDEVBRD_AXIUART_6_SIG & BRDDEV_MINOR_MASK;
        break;
    case 8:
        *discr = eDEVBRD_AXIUART_7_SIG & BRDDEV_MINOR_MASK;
        break;
    case 9:
        *discr = eDEVBRD_AXIUART_8_SIG & BRDDEV_MINOR_MASK;
        break;
    default:
        PDEBUG("Error! unsupported id=%d\n", 
               id);
        ATP_ERROR_RETURN(CCL_NOT_SUPPORT);
    }
    return CCL_OK;
}

static ccl_err_t _eeprom_id2sig(b_u32 id, b_u32 *discr)
{
    if ( id > EEPROM_MAX_ID || !discr )
    {
        PDEBUG("Error! Invalid input parameters - id=%d,discr=%p\n", 
               id, discr);
        ATP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    switch (id) {
    case 1:
        *discr = eDEVBRD_MAIN_EEPROM_SIG & BRDDEV_MINOR_MASK;
        break;
    case 2:
        *discr = eDEVBRD_COMX0_EEPROM_SIG & BRDDEV_MINOR_MASK;
        break;
    case 3:
        *discr = eDEVBRD_COMX1_EEPROM_SIG & BRDDEV_MINOR_MASK;
        break;
    case 4:
        *discr = eDEVBRD_COMX2_EEPROM_SIG & BRDDEV_MINOR_MASK;
        break;
    case 5:
        *discr = eDEVBRD_COMX3_EEPROM_SIG & BRDDEV_MINOR_MASK;
        break;
    case 6:
        *discr = eDEVBRD_ATP_0_EEPROM_SIG & BRDDEV_MINOR_MASK;
        break;
    case 7:
        *discr = eDEVBRD_ATP_1_EEPROM_SIG & BRDDEV_MINOR_MASK;
        break;        
    case 8:
        *discr = eDEVBRD_SODIMM_EEPROM_1_SIG & BRDDEV_MINOR_MASK;
        break;        
    case 9:
        *discr = eDEVBRD_SODIMM_EEPROM_2_SIG & BRDDEV_MINOR_MASK;
        break;        
    case 10:
        *discr = eDEVBRD_SODIMM_EEPROM_3_SIG & BRDDEV_MINOR_MASK;
        break;        
    default:
        PDEBUG("Error! unsupported id=%d\n", 
               id);
        ATP_ERROR_RETURN(CCL_NOT_SUPPORT);
    }
    return CCL_OK;
}

static ccl_err_t _rtc_id2sig(b_u32 id, b_u32 *discr)
{
    if ( id > RTC_MAX_ID || !discr )
    {
        PDEBUG("Error! Invalid input parameters - id=%d,discr=%p\n", 
               id, discr);
        ATP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    switch (id) {
    case 1:
        *discr = eDEVBRD_MAIN_RTC_SIG & BRDDEV_MINOR_MASK;
        break;
    case 2:
        *discr = eDEVBRD_ATP_0_RTC_SIG & BRDDEV_MINOR_MASK;
        break;
    case 3:
        *discr = eDEVBRD_ATP_1_RTC_SIG & BRDDEV_MINOR_MASK;
        break;
    default:
        PDEBUG("Error! unsupported id=%d\n", 
               id);
        ATP_ERROR_RETURN(CCL_NOT_SUPPORT);
    }
    return CCL_OK;
}

static int set_interface_attribs (int fd, int speed, int parity)
{
    struct termios tty;
    if (tcgetattr (fd, &tty) != 0) {
        PDEBUG ("error %d from tcgetattr", errno);
        return -1;
    }

    cfsetospeed (&tty, speed);
    cfsetispeed (&tty, speed);

    tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;     // 8-bit chars
    // disable IGNBRK for mismatched speed tests; otherwise receive break
    // as \000 chars
    tty.c_iflag &= ~IGNBRK;         // disable break processing
    tty.c_lflag = 0;                // no signaling chars, no echo,
                                    // no canonical processing
    tty.c_oflag = 0;                // no remapping, no delays
    tty.c_cc[VMIN]  = 0;            // read doesn't block
    tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

    tty.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl

    tty.c_cflag |= (CLOCAL | CREAD);// ignore modem controls,
                                    // enable reading
    tty.c_cflag &= ~(PARENB | PARODD);      // shut off parity
    tty.c_cflag |= parity;
    tty.c_cflag &= ~CSTOPB;
    tty.c_cflag &= ~CRTSCTS;

    if (tcsetattr (fd, TCSANOW, &tty) != 0) {
        PDEBUG ("error %d from tcsetattr", errno);
        return -1;
    }
    return 0;
}

static void set_blocking (int fd, int should_block)
{
    struct termios tty;
    memset (&tty, 0, sizeof tty);
    if (tcgetattr (fd, &tty) != 0) {
        PDEBUG ("error %d from tggetattr", errno);
        return;
    }

    tty.c_cc[VMIN]  = should_block ? 1 : 0;
    tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

    if (tcsetattr (fd, TCSANOW, &tty) != 0)
        PDEBUG ("error %d setting term attributes", errno);
}

/* rmap display helper */
static ccl_err_t _fatp_fpga_rmap_dump(b_u32 offset, b_u32 count, b_u32 width)
{
    b_u32   value;
    int     i;
    char    devname[DEVMAN_DEVNAME_LEN];

    /* chips */
    if ( !count || (width!=1&&width!=2&&width!=4) )
    {
        PDEBUG("Error! Invalid input parameters - offset=0x%x,count=%d,width=%d\n", 
               offset, count, width);
        ATP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    snprintf(devname, DEVMAN_DEVNAME_LEN, "fpga""%d", offset >= VIRTEX_OFFSET);
    /* display data */
    for ( i = 0; i < (count*width); i += width ) {
        if ( !(i%16) )
            PRINTL("0x%08x: ", offset+i);
        if ( width == 4 ) {
            b_u32 value = 0;
            _ret = devspibus_read_buff(devname, offset+i, 0, (b_u8 *)&value, width);
            if ( _ret ) {
                PDEBUG("devspibus_read_buff error\n");
                ATP_ERROR_RETURN(_ret);
            }
            PRINTL("0x%08x ", value);
        }
        else if ( width == 2 ) {
            b_u16 value = 0;
            _ret = devspibus_read_buff(devname, offset+i, 0, (b_u8 *)&value, width);
            if ( _ret ) {
                PDEBUG("devspibus_read_buff error\n");
                ATP_ERROR_RETURN(_ret);
            }
            PRINTL("0x%04x ", value);
        }
        else if ( width == 1 ) {
            b_u16 value = 0;
            _ret = devspibus_read_buff(devname, offset+i, 0, (b_u8 *)&value, width);
            if ( _ret ) {
                PDEBUG("devspibus_read_buff error\n");
                ATP_ERROR_RETURN(_ret);
            }
            PRINTL("0x%02x ", value);
        }
        if ( !((i+width)%16) || (i+width) >= (count*width) )
            PRINTL("\n");
    }   

    return CCL_OK;
}

static ccl_err_t _fatp_cmd_fpga_readb(devo_t atpdevo, 
                                   device_cmd_parm_t parms[], 
                                   b_u16 parms_num)
{
    b_u32   offset = 0;
    b_u32   count = 0;
    b_u32   width = 1;

    offset = (b_u32)parms[0].value;
    count = (b_u32)parms[1].value;

    _ret = _fatp_fpga_rmap_dump(offset, count, width);
    if ( _ret ) {
        PDEBUG("_fatp_fpga_rmap_dump error\n");
        ATP_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

static ccl_err_t _fatp_cmd_fpga_readh(devo_t atpdevo, 
                                   device_cmd_parm_t parms[], 
                                   b_u16 parms_num)
{
    b_u32   offset = 0;
    b_u32   count = 0;
    b_u32   width = 2;

    offset = (b_u32)parms[0].value;
    count = (b_u32)parms[1].value;

    _ret = _fatp_fpga_rmap_dump(offset, count, width);
    if ( _ret ) {
        PDEBUG("_fatp_fpga_rmap_dump error\n");
        ATP_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

static ccl_err_t _fatp_cmd_fpga_readw(devo_t atpdevo, 
                                   device_cmd_parm_t parms[], 
                                   b_u16 parms_num)
{
    b_u32   offset = 0;
    b_u32   count = 0;
    b_u32   width = 4;

    offset = (b_u32)parms[0].value;
    count = (b_u32)parms[1].value;

    _ret = _fatp_fpga_rmap_dump(offset, count, width);
    if ( _ret ) {
        PDEBUG("_fatp_fpga_rmap_dump error\n");
        ATP_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

static ccl_err_t _fatp_cmd_fpga_dump(devo_t atpdevo, 
                                   device_cmd_parm_t parms[], 
                                   b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_fpga_writeb(devo_t atpdevo, 
                                   device_cmd_parm_t parms[], 
                                   b_u16 parms_num)
{
    b_u32   offset = (b_u32)parms[0].value;
    b_u32   value = (b_u32)parms[1].value;
    char    devname[DEVMAN_DEVNAME_LEN];

    snprintf(devname, DEVMAN_DEVNAME_LEN, "fpga""%d", offset >= VIRTEX_OFFSET);

    _ret = devspibus_write_buff("devname", offset, 0, (b_u8 *)&value, 1);
    if ( _ret ) {
        PDEBUG("devspibus_write_buff error\n");
        ATP_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

static ccl_err_t _fatp_cmd_fpga_writeh(devo_t atpdevo, 
                                   device_cmd_parm_t parms[], 
                                   b_u16 parms_num)
{
    b_u32   offset = (b_u32)parms[0].value;
    b_u32   value = (b_u32)parms[1].value;
    char    devname[DEVMAN_DEVNAME_LEN];

    snprintf(devname, DEVMAN_DEVNAME_LEN, "fpga""%d", offset >= VIRTEX_OFFSET);

    _ret = devspibus_write_buff(devname, offset, 0, (b_u8 *)&value, 2);
    if ( _ret ) {
        PDEBUG("devspibus_write_buff error\n");
        ATP_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

static ccl_err_t _fatp_cmd_fpga_writew(devo_t atpdevo, 
                                   device_cmd_parm_t parms[], 
                                   b_u16 parms_num)
{
    b_u32   offset = (b_u32)parms[0].value;
    b_u32   value = (b_u32)parms[1].value;
    char    devname[DEVMAN_DEVNAME_LEN];

    snprintf(devname, DEVMAN_DEVNAME_LEN, "fpga""%d", offset >= VIRTEX_OFFSET);

    _ret = devspibus_write_buff(devname, offset, 0, (b_u8 *)&value, 4);
    if ( _ret ) {
        PDEBUG("devspibus_write_buff error\n");
        ATP_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

static ccl_err_t _fatp_cmd_fpga_bwr(devo_t atpdevo, 
                                    device_cmd_parm_t parms[], 
                                    b_u16 parms_num)
{
    b_u32         offset = 0;
    b_u8          *value;
    b_u32         count = 0;
    char          devname[DEVMAN_DEVNAME_LEN];

    offset = (b_u32)parms[0].value;
    value = parms[1].value;
    count = (b_u32)parms[1].rsize;


    PDEBUG("offset=0x%08x, length=%d\n",
           offset, count);
    DUMPBUF(value, count, 16);

    snprintf(devname, DEVMAN_DEVNAME_LEN, "fpga""%d", offset >= VIRTEX_OFFSET);
    PDEBUG("devname: %s\n ", devname);
    _ret = devspibus_write_burst(devname, offset, 0, value, count);
    if (_ret) {
        PDEBUG("devspibus_write_burst error\n");
        ATP_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

/* rmap display helper */
static ccl_err_t _fatp_flash_rmap_dump(b_u32 offset, b_u32 count, b_u32 width)
{
    b_u32   value;
    int     i;

    /* chips */
    if ( !count || (width!=1&&width!=2&&width!=4) )
    {
        PDEBUG("Error! Invalid input parameters - offset=0x%x,count=%d,width=%d\n", 
               offset, count, width);
        ATP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    
    /* display data */
    for ( i = 0; i < (count*width); i += width ) {
        if ( !(i%16) )
            PRINTL("0x%08x: ", offset+i);
        if ( width == 4 ) {
            b_u32 value = 0;
            _ret = devspibus_read_buff("flash", offset, 0, (b_u8 *)&value, width);
            if ( _ret ) {
                PDEBUG("devspibus_read_buff error\n");
                ATP_ERROR_RETURN(_ret);
            }
            PRINTL("0x%08x ", value);
        }
        else if ( width == 2 ) {
            b_u16 value = 0;
            _ret = devspibus_read_buff("flash", offset, 0, (b_u8 *)&value, width);
            if ( _ret ) {
                PDEBUG("devspibus_read_buff error\n");
                ATP_ERROR_RETURN(_ret);
            }
            PRINTL("0x%04x ", value);
        }
        else if ( width == 1 ) {
            b_u16 value = 0;
            _ret = devspibus_read_buff("flash", offset, 0, (b_u8 *)&value, width);
            if ( _ret ) {
                PDEBUG("devspibus_read_buff error\n");
                ATP_ERROR_RETURN(_ret);
            }
            PRINTL("0x%02x ", value);
        }
        if ( !((i+width)%16) || (i+width) >= (count*width) )
            PRINTL("\n");
    }   

    return CCL_OK;
}

static ccl_err_t _fatp_cmd_flash_readb(devo_t atpdevo, 
                                   device_cmd_parm_t parms[], 
                                   b_u16 parms_num)
{
    b_u32   offset = 0;
    b_u32   count = 0;
    b_u32   width = 1;

    offset = (b_u32)parms[0].value;
    count = (b_u32)parms[1].value;

    _ret = _fatp_flash_rmap_dump(offset, count, width);
    if ( _ret ) {
        PDEBUG("_fatp_fpga_rmap_dump error\n");
        ATP_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

static ccl_err_t _fatp_cmd_flash_readh(devo_t atpdevo, 
                                   device_cmd_parm_t parms[], 
                                   b_u16 parms_num)
{
    b_u32   offset = 0;
    b_u32   count = 0;
    b_u32   width = 2;

    offset = (b_u32)parms[0].value;
    count = (b_u32)parms[1].value;

    _ret = _fatp_flash_rmap_dump(offset, count, width);
    if ( _ret ) {
        PDEBUG("_fatp_fpga_rmap_dump error\n");
        ATP_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

static ccl_err_t _fatp_cmd_flash_readw(devo_t atpdevo, 
                                   device_cmd_parm_t parms[], 
                                   b_u16 parms_num)
{
    b_u32   offset = 0;
    b_u32   count = 0;
    b_u32   width = 4;

    offset = (b_u32)parms[0].value;
    count = (b_u32)parms[1].value;

    _ret = _fatp_flash_rmap_dump(offset, count, width);
    if ( _ret ) {
        PDEBUG("_fatp_fpga_rmap_dump error\n");
        ATP_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

static ccl_err_t _fatp_cmd_flash_writeb(devo_t atpdevo, 
                                   device_cmd_parm_t parms[], 
                                   b_u16 parms_num)
{
    b_u32   offset = (b_u32)parms[0].value;
    b_u32   value = (b_u32)parms[1].value;

    _ret = devspibus_write_buff("flash", offset, 0, (b_u8 *)&value, 1);
    if ( _ret ) {
        PDEBUG("devspibus_write_buff error\n");
        ATP_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

static ccl_err_t _fatp_cmd_flash_writeh(devo_t atpdevo, 
                                   device_cmd_parm_t parms[], 
                                   b_u16 parms_num)
{
    b_u32   offset = (b_u32)parms[0].value;
    b_u32   value = (b_u32)parms[1].value;

    _ret = devspibus_write_buff("flash", offset, 0, (b_u8 *)&value, 2);
    if ( _ret ) {
        PDEBUG("devspibus_write_buff error\n");
        ATP_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

static ccl_err_t _fatp_cmd_flash_writew(devo_t atpdevo, 
                                   device_cmd_parm_t parms[], 
                                   b_u16 parms_num)
{
    b_u32   offset = (b_u32)parms[0].value;
    b_u32   value = (b_u32)parms[1].value;

    _ret = devspibus_write_buff("flash", offset, 0, (b_u8 *)&value, 4);
    if ( _ret ) {
        PDEBUG("devspibus_write_buff error\n");
        ATP_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

static ccl_err_t _fatp_cmd_flashrd(devo_t atpdevo, 
                                   device_cmd_parm_t parms[], 
                                   b_u16 parms_num)
{
    return CCL_OK;
}
static ccl_err_t _fatp_cmd_flashwr(devo_t atpdevo, 
                                   device_cmd_parm_t parms[], 
                                   b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_mboxctrl(devo_t atpdevo, 
                                    device_cmd_parm_t parms[], 
                                    b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_mboxsts(devo_t atpdevo, 
                                   device_cmd_parm_t parms[], 
                                   b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_mboxrd(devo_t atpdevo, 
                                  device_cmd_parm_t parms[], 
                                  b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_mboxwr(devo_t atpdevo, 
                                  device_cmd_parm_t parms[], 
                                  b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_temac_init(devo_t atpdevo, 
                                      device_cmd_parm_t parms[], 
                                      b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_temac_link(devo_t atpdevo, 
                                      device_cmd_parm_t parms[], 
                                      b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_temac_traffic(devo_t atpdevo, 
                                         device_cmd_parm_t parms[], 
                                         b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_axispi_flashrd(devo_t atpdevo, 
                                          device_cmd_parm_t parms[], 
                                          b_u16 parms_num)
{
    b_u32         i;
    b_u32         id = 0;
    b_u32         offset = 0;
    b_u32         count = 0;
    b_u32         discr;
    devo_t        devo;
    b_u8          *buf;

    id = (b_u32)parms[0].value; 
    offset = (b_u32)parms[1].value;
    count = (b_u32)parms[2].value;

    buf = calloc(1, count);
    if (!buf) {
        PDEBUG("Can't allocate memory\n");
        return CCL_NO_MEM_ERR;
    }
    _ret = _flash_id2sig(id, &discr);
    if (_ret) {
        PDEBUG("_flash_id2sig error\n");
        ATP_ERROR_RETURN(_ret);
    }
    _ret = devman_device_object_identify("spidev", discr, &devo);
    if ( _ret || !devo) {
        PDEBUG("Error! Can't identify \"spidev\", %d discr object\n", 
               discr);
        return CCL_FAIL;
    }

    PDEBUG("devo=%p, buf=%p, offset=0x%04x, count=%d",
           devo, buf, offset, count);
    _ret = devman_attr_array_get_buff_with_offset(devo, "buffer", 0, 
                                                  buf, offset, 0, count);
    if (_ret) {
        free(buf);
        PDEBUG("devman_attr_array_get_buff_with_offset error\n");
        ATP_ERROR_RETURN(_ret);
    }
    for (i = 0; i < count; i++) {
        printf("buf[%d]=0x%02x ", i, buf[i]);
    }
    printf("\n");

    free(buf);

    return CCL_OK;
}

static ccl_err_t _fatp_cmd_axispi_flashwr(devo_t atpdevo, 
                                          device_cmd_parm_t parms[], 
                                          b_u16 parms_num)
{
    b_u32         id = 0;
    b_u32         offset = 0;
    b_u32         value = 0;
    b_u32         discr;
    devo_t        devo;


    id = (b_u32)parms[0].value; 
    offset = (b_u32)parms[1].value;
    value = (b_u32)parms[2].value;

    _ret = _flash_id2sig(id, &discr);
    if (_ret) {
        PDEBUG("_flash_id2sig error\n");
        ATP_ERROR_RETURN(_ret);
    }
    _ret = devman_device_object_identify("spidev", discr, &devo);
    if ( _ret || !devo) {
        PDEBUG("Error! Can't identify \"spidev\", %d discr object\n", 
               discr);
        return CCL_FAIL;
    }

    PDEBUG("devo=%p, buf=%p, offset=0x%04x, length=%d",
           devo, value, offset, sizeof(value));
    _ret = devman_attr_array_set_buff_with_offset(devo, "buffer", 0, 
                                                  (b_u8 *)&value, 
                                                  offset, 0, 
                                                  sizeof(value));
    if (_ret) {
        PDEBUG("devman_attr_array_get_buff_with_offset error\n");
        ATP_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

static ccl_err_t _fatp_cmd_axigpio_cpu_ctrl(devo_t atpdevo, 
                                            device_cmd_parm_t parms[], 
                                            b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_axigpio_virtex_ctrl(devo_t atpdevo, 
                                               device_cmd_parm_t parms[], 
                                               b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_axigpio_smartcard_ctrl(devo_t atpdevo, 
                                                  device_cmd_parm_t parms[], 
                                                  b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_axigpio_flashmux_ctrl(devo_t atpdevo, 
                                                 device_cmd_parm_t parms[], 
                                                 b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_axigpio_atp_ctrl(devo_t atpdevo, 
                                            device_cmd_parm_t parms[], 
                                            b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_axigpio_pwrrst_ctrl(devo_t atpdevo, 
                                               device_cmd_parm_t parms[], 
                                               b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_axigpio_xeon_ctrl(devo_t atpdevo, 
                                             device_cmd_parm_t parms[], 
                                             b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_axigpio_fp_ctrl(devo_t atpdevo, 
                                           device_cmd_parm_t parms[], 
                                           b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_axigpio_gen_artix_ctrl(devo_t atpdevo, 
                                                  device_cmd_parm_t parms[], 
                                                  b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_axigpio_gen_virtex_ctrl(devo_t atpdevo, 
                                                   device_cmd_parm_t parms[], 
                                                   b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_axigpio_sfp_presence(devo_t atpdevo, 
                                                device_cmd_parm_t parms[], 
                                                b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_axigpio_qsfp_presence(devo_t atpdevo, 
                                                 device_cmd_parm_t parms[], 
                                                 b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_axigpio_sfp_tx_disable(devo_t atpdevo, 
                                                  device_cmd_parm_t parms[], 
                                                  b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_axigpio_qsfp_reset(devo_t atpdevo, 
                                              device_cmd_parm_t parms[], 
                                              b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_axigpio_qsfp_irq(devo_t atpdevo, 
                                            device_cmd_parm_t parms[], 
                                            b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_axigpio_i2cio_irq(devo_t atpdevo, 
                                             device_cmd_parm_t parms[], 
                                             b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_axigpio_sfp_traffic_en(devo_t atpdevo, 
                                                  device_cmd_parm_t parms[], 
                                                  b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_axigpio_qsfp_traffic_en(devo_t atpdevo, 
                                                   device_cmd_parm_t parms[], 
                                                   b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_axigpio_1g_traffic_en(devo_t atpdevo, 
                                                 device_cmd_parm_t parms[], 
                                                 b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_axigpio_memory_traffic_en(devo_t atpdevo, 
                                                     device_cmd_parm_t parms[], 
                                                     b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_axigpio_is_memory_test_ok(devo_t atpdevo, 
                                                     device_cmd_parm_t parms[], 
                                                     b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_sfp_media_details(devo_t atpdevo, 
                                             device_cmd_parm_t parms[], 
                                             b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_qsfp_media_details(devo_t atpdevo, 
                                              device_cmd_parm_t parms[], 
                                              b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_sfp_rx_los(devo_t atpdevo, 
                                      device_cmd_parm_t parms[], 
                                      b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_sfp_tx_flt(devo_t atpdevo, 
                                      device_cmd_parm_t parms[], 
                                      b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_sfp_rs(devo_t atpdevo, 
                                  device_cmd_parm_t parms[], 
                                  b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_qsfp_modsell(devo_t atpdevo, 
                                        device_cmd_parm_t parms[], 
                                        b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_qsfp_lpmode(devo_t atpdevo, 
                                       device_cmd_parm_t parms[], 
                                       b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_uart_init(devo_t atpdevo, 
                                     device_cmd_parm_t parms[], 
                                     b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_uart_tx(devo_t atpdevo, 
                                   device_cmd_parm_t parms[], 
                                   b_u16 parms_num)
{
    b_u32   id = 0;
    b_u8    *str = NULL;
    b_u32   discr;
    char    *buf;
    b_u32   i;

    id = (b_u32)parms[0].value; 
    str = (b_u8 *)parms[1].value;

    _ret = _uart_id2sig(id, &discr);
    if (_ret) {
        PDEBUG("_flash_id2sig error\n");
        ATP_ERROR_RETURN(_ret);
    }

    _ret = devaxiuart_write_buff(discr, str, strlen(str));
    if (_ret) {
        PDEBUG("devaxiuart_write_buff error\n");
        ATP_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

static ccl_err_t _fatp_cmd_uart_rx(devo_t atpdevo, 
                                   device_cmd_parm_t parms[], 
                                   b_u16 parms_num)
{
    b_u32   id = 0;
    b_u32   count = 0;
    char    *str = NULL;
    b_u32   discr;
    char    *buf;
    b_u32   i;

    id = (b_u32)parms[0].value; 
    count = (b_u32)parms[1].value;

    if (parms_num > 2) {
        str = (char *)parms[2].value;
    }

    _ret = _uart_id2sig(id, &discr);
    if (_ret) {
        PDEBUG("_flash_id2sig error\n");
        ATP_ERROR_RETURN(_ret);
    }

    buf = calloc(1, count);
    if (!buf) {
        PDEBUG("Can't allocate memory\n");
        return CCL_NO_MEM_ERR;
    }

    _ret = devaxiuart_read_buff(discr, buf, count);
    if (_ret) {
        free(buf);
        PDEBUG("devaxiuart_read_buff error\n");
        ATP_ERROR_RETURN(_ret);
    }

    for (i = 0; i < count; i++) {
        printf("buf[%d]=0x%02x(%c) ", i, buf[i], buf[i]);
    }
    printf("\n");

    free(buf);

    return CCL_OK;
}

static ccl_err_t _fatp_cmd_uart_test(devo_t atpdevo, 
                                     device_cmd_parm_t parms[], 
                                     b_u16 parms_num)
{
    b_u32   id = 0;
    b_u32   count = 0;
    char    *str = NULL;
    b_u32   discr;
    char    *buf;
    b_u32   i;
    char    sdev[DEVMAN_DEVNAME_LEN];

    id = (b_u32)parms[0].value; 
    count = (b_u32)parms[1].value;

    if (parms_num > 2) {
        str = (char *)parms[2].value;
    }

    _ret = _uart_id2sig(id, &discr);
    if (_ret) {
        PDEBUG("_flash_id2sig error\n");
        ATP_ERROR_RETURN(_ret);
    }

    if (id >= 1 && id <= 4) 
        strncpy(sdev, "/dev/ttyS1", DEVMAN_DEVNAME_LEN);
    else if (id == 6 | id == 7) 
        strncpy(sdev, "/dev/ttymxc5", DEVMAN_DEVNAME_LEN);
    else {
        PDEBUG("invalid id: %d\n", id);
        ATP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    int fd = open (sdev, O_RDWR | O_NOCTTY | O_SYNC);
    if (fd < 0) {
        PDEBUG ("error %d opening %s: %s\n", 
                errno, sdev, strerror (errno));
        ATP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    set_interface_attribs (fd, B115200, 0);  // set speed to 115,200 bps, 8n1 (no parity)
    set_blocking (fd, 0);                    // set no blocking

    buf = calloc(1, count);
    if (!buf) {
        PDEBUG("Can't allocate memory\n");
        ATP_ERROR_RETURN(CCL_NO_MEM_ERR);;
    }

    /* Test CPU -> Artix direction */
    printf("CPU write:\n");
    for (i = 0; i < count; i++) {
        buf[i] = 'a' + i;
        printf("%c", buf[i]);
    }
    printf("\n");
    write (fd, buf, count);           
    sleep(1);
    /* read Artix UART Rx buffer*/
    _ret = devaxiuart_read_buff(discr, buf, count);
    if (_ret) {
        free(buf);
        PDEBUG("devaxiuart_read_buff error\n");
        ATP_ERROR_RETURN(_ret);
    }
    printf("FPGA Read:\n");
    for (i = 0; i < count; i++) {
        printf("%c", buf[i]);
    }
    printf("\n");

    /* Test Artix -> CPU direction */
    printf("FPGA write:\n");
    for (i = 0; i < count; i++) {
        buf[i] = 'a' + (count - i - 1);
        printf("%c", buf[i]);
    }
    printf("\n");

    /* write to Artix Tx buffer */
    _ret = devaxiuart_write_buff(discr, buf, count);
    if (_ret) {
        free(buf);
        PDEBUG("devaxiuart_write_buff error\n");
        ATP_ERROR_RETURN(_ret);
    }
    memset(buf, 0, count);
    int n = read(fd, buf, count);  
    if (n != count) {
        free(buf);
        PDEBUG("Error! Read %d characters instead of %d\n",
               n, count);
        ATP_ERROR_RETURN(CCL_FAIL);
    }
    printf("CPU Read:\n");
    for (i = 0; i < n; i++) {
        printf("%c", buf[i]);
    }
    printf("\n");
    free(buf);

    return CCL_OK;
}

static ccl_err_t _fatp_cmd_uart_sc_atr_test(devo_t atpdevo, 
                                            device_cmd_parm_t parms[], 
                                            b_u16 parms_num)
{
    b_u32   count;
    char    *buf;
    devo_t  devo;
    b_i32   discr = 0;
    char    devname[DEVMAN_DEVNAME_LEN];
    b_u32   offset;
    b_u32   value;
    b_u8    uart_id;
    b_u32   i;

    count = (b_u32)parms[0].value;

    buf = calloc(1, count);
    if (!buf) {
        PDEBUG("Can't allocate memory\n");
        ATP_ERROR_RETURN(CCL_NO_MEM_ERR);;
    }

    _ret = devman_device_object_identify("smartcard", discr, &devo);
    if ( _ret || !devo) {
        PDEBUG("Error! Can't identify \"smartcard\", %d discr object\n", 
               discr);
        return CCL_FAIL;
    }
    _ret = devman_attr_array_get_byte(devo, "uart_id", 0, &uart_id);
    if (_ret) {
        PDEBUG("devman_attr_array_get_byte error\n");
        ATP_ERROR_RETURN(_ret);
    }
    PDEBUG("uart_id=%d\n", uart_id);

    offset = AXIGPIO_SMART_CARD_CTRL_OFFSET;
    /* Power up*/
    #define SC_POWER_UP_MASK 0x1
    #define SC_RESET_MASK 0x2

    snprintf(devname, DEVMAN_DEVNAME_LEN, "fpga""%d", offset >= VIRTEX_OFFSET);

    _ret = devspibus_read_buff(devname, offset, 0, (b_u8 *)&value, 4);
    if ( _ret ) {
        PDEBUG("devspibus_read_buff error\n");
        ATP_ERROR_RETURN(_ret);
    }

    value |= SC_POWER_UP_MASK;
    _ret = devspibus_write_buff(devname, offset, 0, (b_u8 *)&value, 4);
    if ( _ret ) {
        PDEBUG("devspibus_write_buff error\n");
        ATP_ERROR_RETURN(_ret);
    }
    sleep(1);
    value ^= SC_RESET_MASK;
    snprintf(devname, DEVMAN_DEVNAME_LEN, "fpga""%d", offset >= VIRTEX_OFFSET);
    _ret = devspibus_write_buff(devname, offset, 0, (b_u8 *)&value, 4);
    if ( _ret ) {
        PDEBUG("devspibus_write_buff error\n");
        ATP_ERROR_RETURN(_ret);
    }
    /* read Artix UART Rx buffer*/
    _ret = devaxiuart_read_buff(uart_id, buf, count);
    if (_ret) {
        free(buf);
        PDEBUG("devaxiuart_read_buff error\n");
        ATP_ERROR_RETURN(_ret);
    }
    printf("ATR Read:\n");
    for (i = 0; i < count; i++) {
        printf("%02x ", buf[i]);
    }
    printf("\n");

    return CCL_OK;
}

static ccl_err_t _fatp_cmd_uart_sc_init(devo_t atpdevo, 
                                        device_cmd_parm_t parms[], 
                                        b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_uart_sc_presence(devo_t atpdevo, 
                                            device_cmd_parm_t parms[], 
                                            b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_uart_sc_poweron(devo_t atpdevo, 
                                           device_cmd_parm_t parms[], 
                                           b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_uart_sc_rd(devo_t atpdevo, 
                                      device_cmd_parm_t parms[], 
                                      b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_uart_sc_apdu(devo_t atpdevo, 
                                        device_cmd_parm_t parms[], 
                                        b_u16 parms_num)
{
    b_i32     discr = 0;
    char      devname[DEVMAN_DEVNAME_LEN];
    b_u8      *apdu;
    b_u32     count = 0;
    devo_t    devo;
    b_u8      uart_id;

    apdu = parms[0].value;
    count = (b_u32)parms[0].rsize;

    _ret = devman_device_object_identify("smartcard", discr, &devo);
    if ( _ret || !devo) {
        PDEBUG("Error! Can't identify \"smartcard\", %d discr object\n", 
               discr);
        return CCL_FAIL;
    }
    _ret = devman_attr_array_get_byte(devo, "uart_id", 0, &uart_id);
    if (_ret) {
        PDEBUG("devman_attr_array_get_byte error\n");
        ATP_ERROR_RETURN(_ret);
    }

    PDEBUG("uart_id=%d\n", uart_id);
    DUMPBUF(apdu, count, 16);

    _ret = devaxiuart_write_buff(uart_id, apdu, count);
    if (_ret) {
        PDEBUG("devaxiuart_write_buff error\n");
        ATP_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

static ccl_err_t _fatp_cmd_uart_sc_reset(devo_t atpdevo, 
                                         device_cmd_parm_t parms[], 
                                         b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_xadc_temperature(devo_t atpdevo, 
                                            device_cmd_parm_t parms[], 
                                            b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_xadc_voltage(devo_t atpdevo, 
                                        device_cmd_parm_t parms[], 
                                        b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_sysmon_temperature(devo_t atpdevo, 
                                              device_cmd_parm_t parms[], 
                                              b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_sysmon_voltage(devo_t atpdevo, 
                                          device_cmd_parm_t parms[], 
                                          b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_hses10g_init(devo_t atpdevo, 
                                        device_cmd_parm_t parms[], 
                                        b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_hses10g_link(devo_t atpdevo, 
                                        device_cmd_parm_t parms[], 
                                        b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_hses10g_traffic(devo_t atpdevo, 
                                           device_cmd_parm_t parms[], 
                                           b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_usplus100g_init(devo_t atpdevo, 
                                           device_cmd_parm_t parms[], 
                                           b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_usplus100g_link(devo_t atpdevo, 
                                           device_cmd_parm_t parms[], 
                                           b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_usplus100g_traffic(devo_t atpdevo, 
                                              device_cmd_parm_t parms[], 
                                              b_u16 parms_num)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_i2ccore_eepromrd(devo_t atpdevo, 
                                            device_cmd_parm_t parms[], 
                                            b_u16 parms_num)
{
    b_u32         id = 0;
    b_u32         offset = 0;
    b_u32         count = 0;
    b_u32         discr;
    devo_t        devo;
    b_u8          *buf;

    id = (b_u32)parms[0].value; 
    offset = (b_u32)parms[1].value;
    count = (b_u32)parms[2].value;

    buf = calloc(1, count);
    if (!buf) {
        PDEBUG("Can't allocate memory\n");
        return CCL_NO_MEM_ERR;
    }
    _ret = _eeprom_id2sig(id, &discr);
    if (_ret) {
        free(buf);
        PDEBUG("_eeprom_id2sig error\n");
        ATP_ERROR_RETURN(_ret);
    }
    _ret = devman_device_object_identify("eeprom", discr, &devo);
    if ( _ret || !devo) {
        free(buf);
        PDEBUG("Error! Can't identify \"eeprom\", %d discr object\n", 
               discr);
        return CCL_FAIL;
    }

    PDEBUG("devo=%p, buf=%p, offset=0x%04x, count=%d\n",
           devo, buf, offset, count);
    _ret = devman_attr_array_get_buff_with_offset(devo, "buffer", 0, 
                                                  buf, offset, 0, count);
    if (_ret) {
        free(buf);
        PDEBUG("devman_attr_array_get_buff_with_offset error\n");
        ATP_ERROR_RETURN(_ret);
    }
    DUMPBUF(buf, count, 16);

    free(buf);

    return CCL_OK;
}

static ccl_err_t _fatp_cmd_i2ccore_eepromwr(devo_t atpdevo, 
                                            device_cmd_parm_t parms[], 
                                            b_u16 parms_num)
{
    b_u32         id = 0;
    b_u32         offset = 0;
    b_u8          *value;
    b_u32         count = 0;
    b_u32         discr;
    devo_t        devo;
    b_u32         i;


    id = (b_u32)parms[0].value; 
    offset = (b_u32)parms[1].value;
    value = parms[2].value;
    count = (b_u32)parms[2].rsize;


    _ret = _eeprom_id2sig(id, &discr);
    if (_ret) {
        PDEBUG("_eeprom_id2sig error\n");
        ATP_ERROR_RETURN(_ret);
    }
    _ret = devman_device_object_identify("eeprom", discr, &devo);
    if ( _ret || !devo) {
        PDEBUG("Error! Can't identify \"eeprom\", %d discr object\n", 
               discr);
        return CCL_FAIL;
    }

    PDEBUG("devo=%p, offset=0x%04x, length=%d\n",
           devo, offset, count);
    DUMPBUF(value, count, 16);

    _ret = devman_attr_array_set_buff_with_offset(devo, "buffer", 0, 
                                                  value, 
                                                  offset, 0, 
                                                  count);
    if (_ret) {
        PDEBUG("devman_attr_array_get_buff_with_offset error\n");
        ATP_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

static ccl_err_t _fatp_cmd_i2ccore_rtcrd(devo_t atpdevo, 
                                            device_cmd_parm_t parms[], 
                                            b_u16 parms_num)
{
    b_u32         i;
    b_u32         id = 0;
    b_u32         offset = 0;
    b_u32         count = 0;
    b_u32         discr;
    devo_t        devo;
    b_u8          value;
    b_u8          *buf;

    id = (b_u32)parms[0].value; 
    offset = (b_u32)parms[1].value;
    count = (b_u32)parms[2].value;

    buf = calloc(1, count);
    if (!buf) {
        PDEBUG("Can't allocate memory\n");
        return CCL_NO_MEM_ERR;
    }

    _ret = _rtc_id2sig(id, &discr);
    if (_ret) {
        free(buf);
        PDEBUG("_rtc_id2sig error\n");
        ATP_ERROR_RETURN(_ret);
    }
    _ret = devman_device_object_identify("rtc", discr, &devo);
    if ( _ret || !devo) {
        free(buf);
        PDEBUG("Error! Can't identify \"rtc\", %d discr object\n", 
               discr);
        return CCL_FAIL;
    }

    PDEBUG("devo=%p, offset=0x%04x, count=%d\n",
           devo, offset, count);
    _ret = devman_attr_array_get_buff_with_offset(devo, "buffer", 0, 
                                                  buf, offset, 0, count);
    if (_ret) {
        free(buf);
        PDEBUG("devman_attr_array_get_buff_with_offset error\n");
        ATP_ERROR_RETURN(_ret);
    }
    for (i = 0; i < count; i++) {
        printf("buf[%d]=0x%02x ", i, buf[i]);
    }
    printf("\n");

    free(buf);    

    return CCL_OK;
}

static ccl_err_t _fatp_cmd_i2ccore_rtcwr(devo_t atpdevo, 
                                         device_cmd_parm_t parms[], 
                                         b_u16 parms_num)
{
    b_u32         i;
    b_u32         id = 0;
    b_u32         offset = 0;
    b_u32         value = 0;
    b_u32         count = 0;
    b_u32         discr;
    devo_t        devo;

    id = (b_u32)parms[0].value; 
    offset = (b_u32)parms[1].value;
    value = (b_u32)parms[2].value;
    count = (b_u32)parms[3].value;

    _ret = _rtc_id2sig(id, &discr);
    if (_ret) {
        PDEBUG("_rtc_id2sig error\n");
        ATP_ERROR_RETURN(_ret);
    }
    _ret = devman_device_object_identify("rtc", discr, &devo);
    if ( _ret || !devo) {
        PDEBUG("Error! Can't identify \"rtc\", %d discr object\n", 
               discr);
        return CCL_FAIL;
    }

    PDEBUG("devo=%p, offset=0x%04x, value=0x%02x, count=%d\n",
           devo, offset, value, count);
    _ret = devman_attr_array_set_buff_with_offset(devo, "buffer", 0, 
                                                  (b_u8 *)&value, offset, 0, count);
    if (_ret) {
        PDEBUG("devman_attr_array_get_buff_with_offset error\n");
        ATP_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

static ccl_err_t _fatp_cmd_trans_data(devo_t atpdevo, 
                                         device_cmd_parm_t parms[], 
                                         b_u16 parms_num)
{
    b_u32         discr;
    devo_t        devo;
    char          *mmap;          
    b_u8          *buf;

    discr = (b_u32)parms[0].value; 
    mmap = (b_u32)parms[1].value;

    if (strncmp(mmap, "dd_raw", 6) && strncmp(mmap, "msa_raw", 7)) {
        PDEBUG("wrong mmap: %s\n", mmap);
        ATP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    buf = calloc(1, 128);
    if (!buf) {
        PDEBUG("Can't allocate memory\n");
        return CCL_NO_MEM_ERR;
    }

    _ret = devman_device_object_identify("trans", discr, &devo);
    if ( _ret || !devo) {
        free(buf);
        PDEBUG("Error! Can't identify \"trans\", %d discr object\n", 
               discr);
        return CCL_FAIL;
    }

    _ret = devman_attr_array_get_buff(devo, mmap, 0, buf, 128);
    if (_ret) {
        free(buf);
        PDEBUG("devman_attr_array_get_buff error\n");
        ATP_ERROR_RETURN(_ret);
    }
    DUMPBUF(buf, 128, 16);

    free(buf);

    return CCL_OK;
}

static ccl_err_t _fatp_cmd_stat_period(devo_t atpdevo, 
                                       device_cmd_parm_t parms[], 
                                       b_u16 parms_num)
{
    atp_ctx_t     *p_atp = devman_device_private(atpdevo);
    b_u32         period;

    period = (b_u32)parms[0].value; 

    _ret = ccl_update_stat_sender(&p_atp->stat_sender, 
                                  period, &logger);
    if (_ret) {
        PDEBUG("ccl_update_stat_sender error\n");
        ATP_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

static ccl_err_t _fatp_get_cpu_sensors(sys_sensors_t *sys_sensors)
{
    devo_t devo;

    _ret = devman_device_object_identify("cpu", 0, &devo);
    if ( _ret || !devo) {
        PDEBUG("Error! Can't identify \"cpu\" object\n");
        ATP_ERROR_RETURN(CCL_FAIL);
    }

    _ret = devman_attr_array_get_buff(devo, "sensors", 0, 
                                      (b_u8 *)sys_sensors, 
                                      sizeof(*sys_sensors));
    if (_ret) {
        PDEBUG("devman_attr_array_get_buff error\n");
        ATP_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

static ccl_err_t _fatp_cmd_es_config(devo_t atpdevo, 
                                     device_cmd_parm_t parms[], 
                                     b_u16 parms_num)
{
    atp_ctx_t     *p_atp = devman_device_private(atpdevo);
    char          *hostname;
    b_u32         port;
    sys_sensors_t sys_sensors;

    p_atp->es_config.enable = (b_u8)parms[0].value ? CCL_TRUE : CCL_FALSE; 
    p_atp->es_config.index = parms[1].value;
    strncpy(p_atp->es_config.type, (char *)parms[2].value, DEVMAN_CMD_PARM_LEN+1);
    hostname = (char *)parms[3].value;
    port = parms[4].value;

    if (p_atp->es_config.enable) {
        init_http_cluster_node(&p_atp->node, hostname, port, 0);
        _ret = connect_to_http_cluster_node(&p_atp->node, &p_atp->node_conn, 15);
        if (_ret) {
            PDEBUG("connect_to_http_cluster_node error\n");
            ATP_ERROR_RETURN(_ret);
        }
        _ret = _fatp_get_cpu_sensors(&sys_sensors);
        if (_ret) {
            PDEBUG("Error! _fatp_get_cpu_sensors failed\n");
            ATP_ERROR_RETURN(CCL_FAIL);
        }

        _ret = ccl_init_stat_sender(&p_atp->stat_sender,
                                    p_atp->es_config.index,
                                    p_atp->es_config.type,
                                    30,
                                    _prepare_board_stat,
                                    &sys_sensors,
                                    p_atp,
                                    &logger);
        if (_ret) {
            PDEBUG("Error! ccl_init_stat_sender failed\n");
            ATP_ERROR_RETURN(CCL_FAIL);
        }
    }

    return CCL_OK;
}
/*---------------------------------------------TESTS-----------------------------*/
static ccl_err_t _i2c_eeprom_test(b_i32 discr)
{
    devo_t   devo;
    b_u8     value1[16];
    b_u8     value2[16];
    b_u32    offset = 20;
    b_i32    i;

    for (i = 0; i < 16; i++) {
        if (i%2) 
            value1[i] = 0x55;
        else
            value1[i] = 0xaa;
    }
    PRINTL("eeprom device: %d\n", discr);
    PRINTL("tested value:\n");
    for (i = 0; i < 16; i++) 
        PRINTL("%02x ", value1[i]);
    PRINTL("\n");

    _ret = devman_device_object_identify("eeprom", discr, &devo);
    if ( _ret || !devo) {
        ccl_syslog_err("Error! Can't identify \"eeprom\", %d discr object\n", 
               discr);
        ATP_ERROR_RETURN(CCL_FAIL);
    }

    _ret = devman_attr_array_set_buff_with_offset(devo, "buffer", 0, 
                                                  value1, 
                                                  offset, 0, 
                                                  16);
    if (_ret) 
        ATP_ERROR_RETURN(_ret);

    _ret = devman_attr_array_get_buff_with_offset(devo, "buffer", 0, 
                                                  value2, offset, 0, 16);
    if (_ret) 
        ATP_ERROR_RETURN(_ret);

    PRINTL("read value:\n");
    for (i = 0; i < 16; i++) 
        PRINTL("%02x ", value2[i]);
    PRINTL("\n");
    for (i = 0; i < 16; i++) {
        if (value1[i] != value2[i]) {
            PRINTL("Error: value1[%d]=0x%02x, value2[%d]=0x%02x\n",
                   i, value1[i], i, value2[i]);
            ATP_ERROR_RETURN(CCL_FAIL);
        }
    }

    return CCL_OK;
}

static ccl_err_t _i2c_pmbus_test(b_u8 discr)
{
    devo_t      devo;
    b_u16       data;
    b_i32       mantissa;
    b_i32       exponent;
    double      read_vout;

    _ret = devman_device_object_identify("pmbus", discr, &devo);
    if ( _ret || !devo) {
        ccl_syslog_err("Error! Can't identify \"pmbus\", %d discr object\n", 
               discr);
        ATP_ERROR_RETURN(CCL_FAIL);
    }

    _ret = devman_attr_array_get_half(devo, "read_vout", 0, &mantissa);
    if (_ret) {
        PDEBUG("devman_attr_array_get_word error\n");
        ATP_ERROR_RETURN(_ret);
    }

    mantissa = ccl_2s_compl_to_decimal(mantissa, 16);
    _ret = devman_attr_array_get_word(devo, "exponent", 0, &exponent);
    if (_ret) {
        PDEBUG("devman_attr_array_get_word error\n");
        ATP_ERROR_RETURN(_ret);
    }

    read_vout = mantissa * pow(2, exponent);
    PRINTL("read_vout[%d]=%f\n", discr, read_vout);


    return CCL_OK;
}

static ccl_err_t _i2c_rtc_test(b_u8 discr)
{
    devo_t   devo;
    b_u8     value1 = 0xa5;
    b_u8     value2;
    b_i32    i;

    PRINTL("rtc device: %d\n", discr);
    PRINTL("tested value: 0x%02x\n", value1);

    _ret = devman_device_object_identify("rtc", discr, &devo);
    if ( _ret || !devo) {
        ccl_syslog_err("Error! Can't identify \"rtc\", %d discr object\n", 
               discr);
        ATP_ERROR_RETURN(CCL_FAIL);
    }

    _ret = devman_attr_array_set_byte(devo, "minutes", 0, value1);
    if (_ret) 
        ATP_ERROR_RETURN(_ret);
    _ret = devman_attr_array_get_byte(devo, "minutes", 0, &value2);
    if (_ret) 
        ATP_ERROR_RETURN(_ret);


    PRINTL("read value: 0x%02x\n", value2);

    return CCL_OK;
}

static ccl_err_t _i2c_lcd_test(b_u8 discr)
{
    return CCL_OK;
}

static ccl_err_t _i2c_i2cio_test(b_u8 discr)
{
    devo_t   devo;
    b_u8     value1 = 0x5a;
    b_u8     value2;
    b_i32    i;

    PRINTL("i2cio device: %d\n", discr);
    PRINTL("tested value: 0x%02x\n", value1);

    _ret = devman_device_object_identify("i2cio", discr, &devo);
    if ( _ret || !devo) {
        ccl_syslog_err("Error! Can't identify \"rtc\", %d discr object\n", 
               discr);
        ATP_ERROR_RETURN(CCL_FAIL);
    }

    _ret = devman_attr_array_set_byte(devo, "config7_0", 0, value1);
    if (_ret) 
        ATP_ERROR_RETURN(_ret);
    _ret = devman_attr_array_get_byte(devo, "config7_0", 0, &value2);
    if (_ret) 
        ATP_ERROR_RETURN(_ret);


    PRINTL("read value: 0x%02x\n", value2);

    return CCL_OK;
}

static ccl_err_t _i2c_currmon_test(b_u8 discr)
{
    return CCL_OK;
}

static ccl_err_t _i2c_trans_test(b_u8 discr)
{
    devo_t   devo;
    b_u8     buf[128];

    PRINTL("transceiver: %d\n", discr);

    _ret = devman_device_object_identify("portmap", 0, &devo);
    if ( _ret || !devo) {
        ccl_syslog_err("Error! Can't identify \"portmap\", %d discr object\n", 
               discr);
        ATP_ERROR_RETURN(CCL_FAIL);
    }
    _ret = devman_attr_array_set_bool(devo, "reset", discr, CCL_TRUE);
    if (_ret) {
        ccl_syslog_err("resetting port %d failed\n", discr);
        ATP_ERROR_RETURN(_ret);
    }
    usleep(2000000);
    _ret = devman_attr_array_get_buff(devo, "media_msa", discr, buf, 128);
    if (_ret) {
        ccl_syslog_err("transceiver %d is not present\n", discr);
        ATP_ERROR_RETURN(_ret);
    }
    DUMPBUF(buf, 128, 16);

    return CCL_OK;
}

static ccl_err_t _uart_test(b_u8 discr)
{
    devo_t   devo;
    b_u32    count = 16;
    char     *str = NULL;
    char     *buf;
    char     sdev[DEVMAN_DEVNAME_LEN];
    b_i32    i;
    b_bool   istest;

    _ret = devman_device_object_identify("axiuart", discr, &devo);
    if ( _ret || !devo) {
        ccl_syslog_err("Error! Can't identify \"axiuart\", %d discr object\n", 
               discr);
        ATP_ERROR_RETURN(CCL_FAIL);
    }

    _ret = devman_attr_array_set_word(devo, "init", discr, 1);
    if (_ret) {
        ccl_syslog_err("Error! Can't set \"init\" attribute for %d discr object\n", discr);
        ATP_ERROR_RETURN(_ret);
    }

    _ret = devman_attr_array_get_bool(devo, "istest", discr, &istest);
    if (_ret) {
        ccl_syslog_err("Error! Can't identify \"istest\" attribute for %d discr object\n", discr);
        ATP_ERROR_RETURN(_ret);
    }
    PRINTL("discr: %d, istest: %d\n", discr, istest);
    if (!istest) 
        return CCL_OK;

    if (discr >= 0 && discr <= 4) 
        strncpy(sdev, "/dev/ttyS1", DEVMAN_DEVNAME_LEN);
    else if (discr == 5 | discr == 6) 
        strncpy(sdev, "/dev/ttymxc5", DEVMAN_DEVNAME_LEN);
    else {
        PDEBUG("invalid discr: %d\n", discr);
        ATP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    int fd = open (sdev, O_RDWR | O_NOCTTY | O_SYNC);
    if (fd < 0) {
        PDEBUG ("error %d opening %s: %s\n", 
                errno, sdev, strerror (errno));
        ATP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    set_interface_attribs (fd, B115200, 0);  // set speed to 115,200 bps, 8n1 (no parity)
    set_blocking (fd, 0);                    // set no blocking

    buf = calloc(1, count);
    if (!buf) {
        PDEBUG("Can't allocate memory\n");
        ATP_ERROR_RETURN(CCL_NO_MEM_ERR);;
    }

    /* Test CPU -> Artix direction */
    PRINTL("CPU write:\n");
    for (i = 0; i < count; i++) {
        buf[i] = 'a' + i;
        PRINTL("%c", buf[i]);
    }
    PRINTL("\n");
    write (fd, buf, count);           
    sleep(1);
    /* read Artix UART Rx buffer*/
    _ret = devaxiuart_read_buff(discr, buf, count);
    if (_ret) {
        free(buf);
        PDEBUG("devaxiuart_read_buff error\n");
        ATP_ERROR_RETURN(_ret);
    }
    PRINTL("FPGA Read:\n");
    for (i = 0; i < count; i++) {
        PRINTL("%c", buf[i]);
    }
    PRINTL("\n");

    /* Test Artix -> CPU direction */
    PRINTL("FPGA write:\n");
    for (i = 0; i < count; i++) {
        buf[i] = 'a' + (count - i - 1);
        PRINTL("%c", buf[i]);
    }
    PRINTL("\n");

    /* write to Artix Tx buffer */
    _ret = devaxiuart_write_buff(discr, buf, count);
    if (_ret) {
        free(buf);
        PDEBUG("devaxiuart_write_buff error\n");
        ATP_ERROR_RETURN(_ret);
    }
    memset(buf, 0, count);
    int n = read(fd, buf, count);  
    if (n != count) {
        free(buf);
        PDEBUG("Error! Read %d characters instead of %d\n",
               n, count);
        ATP_ERROR_RETURN(CCL_FAIL);
    }
    PRINTL("CPU Read:\n");
    for (i = 0; i < n; i++) {
        PRINTL("%c", buf[i]);
    }
    PRINTL("\n");
    free(buf);

    return CCL_OK;
}

static b_u32 _port2traffic_gen(b_u32 portid, b_u32 port_type)
{
    return 0;
}

static ccl_err_t _1g_test(b_u8 discr)
{
    return CCL_OK;
}

static ccl_err_t _10g_test(b_u8 discr)
{
    return CCL_OK;
}

static ccl_err_t _100g_test(b_u8 discr)
{
    return CCL_OK;
}

#define SKIP_STATS(stats, prev_stats) ((stats) > (prev_stats)) ? 0 : 1

static void _update_port_counters(port_stats_t *port_stats)
{
    port_stats->tx_bytes.delta = 0;
    port_stats->tx_packets.delta = 0;
    port_stats->tx_packets_64.delta = 0;
    port_stats->tx_packets_65_127.delta = 0;
    port_stats->tx_packets_128_255.delta = 0;
    port_stats->tx_packets_256_511.delta = 0;
    port_stats->tx_packets_512_1023.delta = 0;
    port_stats->tx_packets_1024_max.delta = 0;
    port_stats->tx_bcast.delta = 0;
    port_stats->tx_mcast.delta = 0;
    port_stats->tx_packets_oversize.delta = 0;
    port_stats->tx_packets_underrun_err.delta = 0;
    port_stats->rx_bytes.delta = 0;
    port_stats->rx_packets.delta = 0;
    port_stats->rx_packets_64.delta = 0;
    port_stats->rx_packets_65_127.delta = 0;
    port_stats->rx_packets_128_255.delta = 0;
    port_stats->rx_packets_256_511.delta = 0;
    port_stats->rx_packets_512_1023.delta = 0;
    port_stats->rx_packets_1024_max.delta = 0;
    port_stats->rx_bcast.delta = 0;
    port_stats->rx_mcast.delta = 0;
    port_stats->rx_packets_oversize.delta = 0;
    port_stats->rx_packets_undersize.delta = 0;
    port_stats->rx_packets_frag.delta = 0;
    port_stats->rx_packets_fcs_err.delta = 0;


    if (!SKIP_STATS(port_stats->tx_bytes.value, port_stats->tx_bytes.value_prev))
        port_stats->tx_bytes.delta = port_stats->tx_bytes.value - port_stats->tx_bytes.value_prev;
    port_stats->tx_bytes.value_prev = port_stats->tx_bytes.value;

    if (!SKIP_STATS(port_stats->tx_packets.value, port_stats->tx_packets.value_prev))
        port_stats->tx_packets.delta = port_stats->tx_packets.value - port_stats->tx_packets.value_prev;
    port_stats->tx_packets.value_prev = port_stats->tx_packets.value;

    if (!SKIP_STATS(port_stats->tx_packets_64.value, port_stats->tx_packets_64.value_prev))
        port_stats->tx_packets_64.delta = port_stats->tx_packets_64.value - port_stats->tx_packets_64.value_prev;
    port_stats->tx_packets_64.value_prev = port_stats->tx_packets_64.value;

    if (!SKIP_STATS(port_stats->tx_packets_65_127.value, port_stats->tx_packets_65_127.value_prev))
        port_stats->tx_packets_65_127.delta = port_stats->tx_packets_65_127.value - port_stats->tx_packets_65_127.value_prev;
    port_stats->tx_packets_65_127.value_prev = port_stats->tx_packets_65_127.value;

    if (!SKIP_STATS(port_stats->tx_packets_128_255.value, port_stats->tx_packets_128_255.value_prev))
        port_stats->tx_packets_128_255.delta = port_stats->tx_packets_128_255.value - port_stats->tx_packets_128_255.value_prev;
    port_stats->tx_packets_128_255.value_prev = port_stats->tx_packets_128_255.value;

    if (!SKIP_STATS(port_stats->tx_packets_256_511.value, port_stats->tx_packets_256_511.value_prev))
        port_stats->tx_packets_256_511.delta = port_stats->tx_packets_256_511.value - port_stats->tx_packets_256_511.value_prev;
    port_stats->tx_packets_256_511.value_prev = port_stats->tx_packets_256_511.value;

    if (!SKIP_STATS(port_stats->tx_packets_512_1023.value, port_stats->tx_packets_512_1023.value_prev))
        port_stats->tx_packets_512_1023.delta = port_stats->tx_packets_512_1023.value - port_stats->tx_packets_512_1023.value_prev;
    port_stats->tx_packets_512_1023.value_prev = port_stats->tx_packets_512_1023.value;

    if (!SKIP_STATS(port_stats->tx_packets_1024_max.value, port_stats->tx_packets_1024_max.value_prev))
        port_stats->tx_packets_1024_max.delta = port_stats->tx_packets_1024_max.value - port_stats->tx_packets_1024_max.value_prev;
    port_stats->tx_packets_1024_max.value_prev = port_stats->tx_packets_1024_max.value;

    if (!SKIP_STATS(port_stats->tx_bcast.value, port_stats->tx_bcast.value_prev))
        port_stats->tx_bcast.delta = port_stats->tx_bcast.value - port_stats->tx_bcast.value_prev;
    port_stats->tx_bcast.value_prev = port_stats->tx_bcast.value;

    if (!SKIP_STATS(port_stats->tx_mcast.value, port_stats->tx_mcast.value_prev))
        port_stats->tx_mcast.delta = port_stats->tx_mcast.value - port_stats->tx_mcast.value_prev;
    port_stats->tx_mcast.value_prev = port_stats->tx_mcast.value;

    if (!SKIP_STATS(port_stats->tx_packets_oversize.value, port_stats->tx_packets_oversize.value_prev))
        port_stats->tx_packets_oversize.delta = port_stats->tx_packets_oversize.value - port_stats->tx_packets_oversize.value_prev;
    port_stats->tx_packets_oversize.value_prev = port_stats->tx_packets_oversize.value;

    if (!SKIP_STATS(port_stats->tx_packets_underrun_err.value, port_stats->tx_packets_underrun_err.value_prev))
        port_stats->tx_packets_underrun_err.delta = port_stats->tx_packets_underrun_err.value - port_stats->tx_packets_underrun_err.value_prev;
    port_stats->tx_packets_underrun_err.value_prev = port_stats->tx_packets_underrun_err.value;

    if (!SKIP_STATS(port_stats->rx_bytes.value, port_stats->rx_bytes.value_prev))
        port_stats->rx_bytes.delta = port_stats->rx_bytes.value - port_stats->rx_bytes.value_prev;
    port_stats->rx_bytes.value_prev = port_stats->rx_bytes.value;

    if (!SKIP_STATS(port_stats->rx_packets.value, port_stats->rx_packets.value_prev))
        port_stats->rx_packets.delta = port_stats->rx_packets.value - port_stats->rx_packets.value_prev;
    port_stats->rx_packets.value_prev = port_stats->rx_packets.value;

    if (!SKIP_STATS(port_stats->rx_packets_64.value, port_stats->rx_packets_64.value_prev))
        port_stats->rx_packets_64.delta = port_stats->rx_packets_64.value - port_stats->rx_packets_64.value_prev;
    port_stats->rx_packets_64.value_prev = port_stats->rx_packets_64.value;

    if (!SKIP_STATS(port_stats->rx_packets_65_127.value, port_stats->rx_packets_65_127.value_prev))
        port_stats->rx_packets_65_127.delta = port_stats->rx_packets_65_127.value - port_stats->rx_packets_65_127.value_prev;
    port_stats->rx_packets_65_127.value_prev = port_stats->rx_packets_65_127.value;

    if (!SKIP_STATS(port_stats->rx_packets_128_255.value, port_stats->rx_packets_128_255.value_prev))
        port_stats->rx_packets_128_255.delta = port_stats->rx_packets_128_255.value - port_stats->rx_packets_128_255.value_prev;
    port_stats->rx_packets_128_255.value_prev = port_stats->rx_packets_128_255.value;

    if (!SKIP_STATS(port_stats->rx_packets_256_511.value, port_stats->rx_packets_256_511.value_prev))
        port_stats->rx_packets_256_511.delta = port_stats->rx_packets_256_511.value - port_stats->rx_packets_256_511.value_prev;
    port_stats->rx_packets_256_511.value_prev = port_stats->rx_packets_256_511.value;

    if (!SKIP_STATS(port_stats->rx_packets_512_1023.value, port_stats->rx_packets_512_1023.value_prev))
        port_stats->rx_packets_512_1023.delta = port_stats->rx_packets_512_1023.value - port_stats->rx_packets_512_1023.value_prev;
    port_stats->rx_packets_512_1023.value_prev = port_stats->rx_packets_512_1023.value;

    if (!SKIP_STATS(port_stats->rx_packets_1024_max.value, port_stats->rx_packets_1024_max.value_prev))
        port_stats->rx_packets_1024_max.delta = port_stats->rx_packets_1024_max.value - port_stats->rx_packets_1024_max.value_prev;
    port_stats->rx_packets_1024_max.value_prev = port_stats->rx_packets_1024_max.value;

    if (!SKIP_STATS(port_stats->rx_bcast.value, port_stats->rx_bcast.value_prev))
        port_stats->rx_bcast.delta = port_stats->rx_bcast.value - port_stats->rx_bcast.value_prev;
    port_stats->rx_bcast.value_prev = port_stats->rx_bcast.value;

    if (!SKIP_STATS(port_stats->rx_mcast.value, port_stats->rx_mcast.value_prev))
        port_stats->rx_mcast.delta = port_stats->rx_mcast.value - port_stats->rx_mcast.value_prev;
    port_stats->rx_mcast.value_prev = port_stats->rx_mcast.value;

    if (!SKIP_STATS(port_stats->rx_packets_oversize.value, port_stats->rx_packets_oversize.value_prev))
        port_stats->rx_packets_oversize.delta = port_stats->rx_packets_oversize.value - port_stats->rx_packets_oversize.value_prev;
    port_stats->rx_packets_oversize.value_prev = port_stats->rx_packets_oversize.value;

    if (!SKIP_STATS(port_stats->rx_packets_undersize.value, port_stats->rx_packets_undersize.value_prev))
        port_stats->rx_packets_undersize.delta = port_stats->rx_packets_undersize.value - port_stats->rx_packets_undersize.value_prev;
    port_stats->rx_packets_undersize.value_prev = port_stats->rx_packets_undersize.value;

    if (!SKIP_STATS(port_stats->rx_packets_frag.value, port_stats->rx_packets_frag.value_prev))
        port_stats->rx_packets_frag.delta = port_stats->rx_packets_frag.value - port_stats->rx_packets_frag.value_prev;
    port_stats->rx_packets_frag.value_prev = port_stats->rx_packets_frag.value;

    if (!SKIP_STATS(port_stats->rx_packets_fcs_err.value, port_stats->rx_packets_fcs_err.value_prev))
        port_stats->rx_packets_fcs_err.delta = port_stats->rx_packets_fcs_err.value - port_stats->rx_packets_fcs_err.value_prev;
    port_stats->rx_packets_fcs_err.value_prev = port_stats->rx_packets_fcs_err.value;
}

#define LCD_SCREEN_LEN 80
static ccl_err_t _display_results_on_lcd(atp_ctx_t *p_atp)
{
    devo_t            devo;
    static b_i32      cnt = 0;
    char              msg[LCD_SCREEN_LEN+1];
    const char        msg_frmt[] = "CELARE cGATE-100 %03dV=%02.0f A=%02.0f F=%02dL,%02dR       IV=%05.2f      %02.0f    %02.0f    %02.0f    %02.0f";
    board_dev_stats_t *board_stats = &p_atp->brdspec.board_stats;

    if (!p_atp) {
        ccl_syslog_err("Error! %s[%d] - Invalid input parameters\n", 
               __FUNCTION__, __LINE__);
        return;
    }

    snprintf(msg, LCD_SCREEN_LEN+1, msg_frmt, 
             ++cnt%1000, 
             board_stats->sysmon_stats[0].temp, board_stats->xadc_stats[0].temp,
             board_stats->fanctl_stats[0].temp_local, board_stats->fanctl_stats[0].temp_remote1,
             board_stats->pmbus_stats[0].iout, 
             board_stats->port_stats[0].trans_info.media_decoder.temperature,
             board_stats->port_stats[24].trans_info.media_decoder.temperature,
             board_stats->port_stats[26].trans_info.media_decoder.temperature,
             board_stats->port_stats[22].trans_info.media_decoder.temperature);

    PDEBUG("msg: %s\n", msg);
    _ret = devman_device_object_identify("lcd", 0, &devo);
    if ( _ret || !devo) {
        ccl_syslog_err("Error! Can't identify \"lcd\" object\n");
        return;
    }
#if 0
    /* clear screen */
    _ret = devman_attr_array_set_cmd(devo, "clear_screen", 0);
    if (_ret) {
        PDEBUG("devman_attr_array_set_cmd error\n");
        ATP_ERROR_RETURN(_ret);
    }
#endif
    /* go home */
    _ret = devman_attr_array_set_cmd(devo, "go_home", 0);
    if (_ret) {
        PDEBUG("devman_attr_array_set_cmd error\n");
        ATP_ERROR_RETURN(_ret);
    }

    _ret = devman_attr_array_set_string(devo, "write_text", 0, (b_u8 *)msg);
    if (_ret) {
        PDEBUG("devman_attr_array_set_string error\n");
        ATP_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

static void _prepare_board_stat(ccl_stat_msg_t *stat_msg, void *priv)
{
    atp_ctx_t   *p_atp;
    b_u32       i, j;

    if (!stat_msg || !priv) {
        ccl_syslog_err("Error! %s[%d] - Invalid input parameters\n", 
               __FUNCTION__, __LINE__);
        return;
    }

    p_atp = (atp_ctx_t *)priv;
    _display_results_on_lcd(p_atp);
    ATP_MUTEX_LOCK(p_atp->mutex);
    for (i = 0; i < p_atp->brdspec.board_stats.pmbus_num; i++) {
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                       p_atp->brdspec.board_stats.pmbus_stats[i].name, 
                                       "input_voltage", 
                                       p_atp->brdspec.board_stats.pmbus_stats[i].vin);
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                       p_atp->brdspec.board_stats.pmbus_stats[i].name,
                                       "output_voltage", 
                                       p_atp->brdspec.board_stats.pmbus_stats[i].vout);
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                       p_atp->brdspec.board_stats.pmbus_stats[i].name, 
                                       "output_current", 
                                       p_atp->brdspec.board_stats.pmbus_stats[i].iout);
    }

    for (i = 0; i < p_atp->brdspec.board_stats.currmon_num; i++) {
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                       p_atp->brdspec.board_stats.currmon_stats[i].name,
                                       "bus_voltage", 
                                       p_atp->brdspec.board_stats.currmon_stats[i].bus_voltage);
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                       p_atp->brdspec.board_stats.currmon_stats[i].name,
                                       "current", 
                                       p_atp->brdspec.board_stats.currmon_stats[i].current);
    }

    for (i = 0; i < p_atp->brdspec.board_stats.fanctls_num; i++) {
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                       p_atp->brdspec.board_stats.fanctl_stats[i].name,
                                       "local_temperature", 
                                       p_atp->brdspec.board_stats.fanctl_stats[i].temp_local);
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                       p_atp->brdspec.board_stats.fanctl_stats[i].name,
                                       "virtex_temperature", 
                                       p_atp->brdspec.board_stats.fanctl_stats[i].temp_remote1);
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                       p_atp->brdspec.board_stats.fanctl_stats[i].name,
                                       "remote2_temperature", 
                                       p_atp->brdspec.board_stats.fanctl_stats[i].temp_remote2);
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                       p_atp->brdspec.board_stats.fanctl_stats[i].name,
                                       "speed_rpm1", 
                                       p_atp->brdspec.board_stats.fanctl_stats[i].speed_rpm1);
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                       p_atp->brdspec.board_stats.fanctl_stats[i].name,
                                       "speed_rpm2", 
                                       p_atp->brdspec.board_stats.fanctl_stats[i].speed_rpm2);
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                       p_atp->brdspec.board_stats.fanctl_stats[i].name,
                                       "speed_rpm3", 
                                       p_atp->brdspec.board_stats.fanctl_stats[i].speed_rpm3);
    }

    for (i = 0; i < p_atp->brdspec.board_stats.port_num; i++) {
        if (p_atp->brdspec.board_stats.port_stats[i].trans_info.present) {
            ccl_stat_msg_add_string_field(stat_msg, 
                                          p_atp->brdspec.board_stats.port_stats[i].name,
                                          "media_vendor", 
                                          p_atp->brdspec.board_stats.port_stats[i].trans_info.media_decoder.vendor_name);
            ccl_stat_msg_add_string_field(stat_msg, 
                                          p_atp->brdspec.board_stats.port_stats[i].name,
                                          "media_pn", 
                                          p_atp->brdspec.board_stats.port_stats[i].trans_info.media_decoder.vendor_pn);
            ccl_stat_msg_add_string_field(stat_msg, 
                                          p_atp->brdspec.board_stats.port_stats[i].name,
                                          "media_sn", 
                                          p_atp->brdspec.board_stats.port_stats[i].trans_info.media_decoder.vendor_sn);
            ccl_stat_msg_add_numeric_field(stat_msg, 
                                           p_atp->brdspec.board_stats.port_stats[i].name,
                                           "media_temperature", 
                                           p_atp->brdspec.board_stats.port_stats[i].trans_info.media_decoder.temperature);
            ccl_stat_msg_add_numeric_field(stat_msg, 
                                           p_atp->brdspec.board_stats.port_stats[i].name,
                                           "media_vcc", 
                                           p_atp->brdspec.board_stats.port_stats[i].trans_info.media_decoder.vcc);
            for (j = 0; j < p_atp->brdspec.board_stats.port_stats[i].trans_info.media_decoder.channel_num; j++) {
                char strbuf[DEVMAN_DEVNAME_LEN];

                snprintf(strbuf, DEVMAN_DEVNAME_LEN, "media_tx_bias%d", j+1);
                ccl_stat_msg_add_numeric_field(stat_msg, 
                                               p_atp->brdspec.board_stats.port_stats[i].name,
                                               strbuf, 
                                               p_atp->brdspec.board_stats.port_stats[i].trans_info.media_decoder.tx_bias[j]);
                snprintf(strbuf, DEVMAN_DEVNAME_LEN, "media_tx_power%d", j+1);
                ccl_stat_msg_add_numeric_field(stat_msg, 
                                               p_atp->brdspec.board_stats.port_stats[i].name,
                                               strbuf, 
                                               p_atp->brdspec.board_stats.port_stats[i].trans_info.media_decoder.tx_power[j]);
                snprintf(strbuf, DEVMAN_DEVNAME_LEN, "media_rx_power%d", j+1);
                ccl_stat_msg_add_numeric_field(stat_msg, 
                                               p_atp->brdspec.board_stats.port_stats[i].name,
                                               strbuf, 
                                               p_atp->brdspec.board_stats.port_stats[i].trans_info.media_decoder.rx_power[j]);
            }
        }       
        time_t curr_time = time(NULL);
        b_u64 value_prev = p_atp->brdspec.board_stats.port_stats[i].tx_packets.value_prev;
        _update_port_counters(&p_atp->brdspec.board_stats.port_stats[i]);
        PDEBUG("time=%u, port=%d, tx packets: value=%llu, prev_value=%llu, delta=%llu\n",
               curr_time,
               i,
               p_atp->brdspec.board_stats.port_stats[i].tx_packets.value,
               value_prev,
               p_atp->brdspec.board_stats.port_stats[i].tx_packets.delta);
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                       p_atp->brdspec.board_stats.port_stats[i].name,
                                       "port_tx_bytes", 
                                       p_atp->brdspec.board_stats.port_stats[i].tx_bytes.delta);
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                       p_atp->brdspec.board_stats.port_stats[i].name,
                                       "port_tx_packets", 
                                       p_atp->brdspec.board_stats.port_stats[i].tx_packets.delta);
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                       p_atp->brdspec.board_stats.port_stats[i].name,
                                       "port_tx_packets_64", 
                                       p_atp->brdspec.board_stats.port_stats[i].tx_packets_64.delta);
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                       p_atp->brdspec.board_stats.port_stats[i].name,
                                       "port_tx_packets_65_127", 
                                       p_atp->brdspec.board_stats.port_stats[i].tx_packets_65_127.delta);
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                       p_atp->brdspec.board_stats.port_stats[i].name,
                                       "port_tx_packets_128_255", 
                                       p_atp->brdspec.board_stats.port_stats[i].tx_packets_128_255.delta);
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                       p_atp->brdspec.board_stats.port_stats[i].name,
                                       "port_tx_packets_256_511", 
                                       p_atp->brdspec.board_stats.port_stats[i].tx_packets_256_511.delta);
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                       p_atp->brdspec.board_stats.port_stats[i].name,
                                       "port_tx_packets_512_1024", 
                                       p_atp->brdspec.board_stats.port_stats[i].tx_packets_512_1023.delta);
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                       p_atp->brdspec.board_stats.port_stats[i].name,
                                       "port_tx_packets_1024_max", 
                                       p_atp->brdspec.board_stats.port_stats[i].tx_packets_1024_max.delta);
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                       p_atp->brdspec.board_stats.port_stats[i].name,
                                       "port_tx_bcast", 
                                       p_atp->brdspec.board_stats.port_stats[i].tx_bcast.delta);
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                       p_atp->brdspec.board_stats.port_stats[i].name,
                                       "port_tx_mcast", 
                                       p_atp->brdspec.board_stats.port_stats[i].tx_mcast.delta);
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                       p_atp->brdspec.board_stats.port_stats[i].name,
                                       "port_tx_packets_oversize", 
                                       p_atp->brdspec.board_stats.port_stats[i].tx_packets_oversize.delta);
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                       p_atp->brdspec.board_stats.port_stats[i].name,
                                       "port_tx_packets_underrun_err", 
                                       p_atp->brdspec.board_stats.port_stats[i].tx_packets_underrun_err.delta);
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                       p_atp->brdspec.board_stats.port_stats[i].name,
                                       "port_rx_bytes", 
                                       p_atp->brdspec.board_stats.port_stats[i].rx_bytes.delta);
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                       p_atp->brdspec.board_stats.port_stats[i].name,
                                       "port_rx_packets", 
                                       p_atp->brdspec.board_stats.port_stats[i].rx_packets.delta);
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                       p_atp->brdspec.board_stats.port_stats[i].name,
                                       "port_rx_packets_64", 
                                       p_atp->brdspec.board_stats.port_stats[i].rx_packets_64.delta);
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                       p_atp->brdspec.board_stats.port_stats[i].name,
                                       "port_rx_packets_65_127", 
                                       p_atp->brdspec.board_stats.port_stats[i].rx_packets_65_127.delta);
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                       p_atp->brdspec.board_stats.port_stats[i].name,
                                       "port_rx_packets_128_255", 
                                       p_atp->brdspec.board_stats.port_stats[i].rx_packets_128_255.delta);
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                       p_atp->brdspec.board_stats.port_stats[i].name,
                                       "port_rx_packets_256_511", 
                                       p_atp->brdspec.board_stats.port_stats[i].rx_packets_256_511.delta);
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                       p_atp->brdspec.board_stats.port_stats[i].name,
                                       "port_rx_packets_512_1024", 
                                       p_atp->brdspec.board_stats.port_stats[i].rx_packets_512_1023.delta);
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                       p_atp->brdspec.board_stats.port_stats[i].name,
                                       "port_rx_packets_1024_max", 
                                       p_atp->brdspec.board_stats.port_stats[i].rx_packets_1024_max.delta);
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                       p_atp->brdspec.board_stats.port_stats[i].name,
                                       "port_rx_bcast", 
                                       p_atp->brdspec.board_stats.port_stats[i].rx_bcast.delta);
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                       p_atp->brdspec.board_stats.port_stats[i].name,
                                       "port_rx_mcast", 
                                       p_atp->brdspec.board_stats.port_stats[i].rx_mcast.delta);
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                       p_atp->brdspec.board_stats.port_stats[i].name,
                                       "port_rx_packets_oversize", 
                                       p_atp->brdspec.board_stats.port_stats[i].rx_packets_oversize.delta);
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                       p_atp->brdspec.board_stats.port_stats[i].name,
                                       "port_rx_packets_undersize", 
                                       p_atp->brdspec.board_stats.port_stats[i].rx_packets_undersize.delta);
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                       p_atp->brdspec.board_stats.port_stats[i].name,
                                       "port_rx_packets_frag", 
                                       p_atp->brdspec.board_stats.port_stats[i].rx_packets_frag.delta);
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                       p_atp->brdspec.board_stats.port_stats[i].name,
                                       "port_rx_packets_fcs_err", 
                                       p_atp->brdspec.board_stats.port_stats[i].rx_packets_fcs_err.delta);
    }

    for (i = 0; i < p_atp->brdspec.board_stats.rtc_num; i++) {
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                  p_atp->brdspec.board_stats.rtc_stats[i].name,
                                  "hours", 
                                  p_atp->brdspec.board_stats.rtc_stats[i].hours);
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                  p_atp->brdspec.board_stats.rtc_stats[i].name,
                                  "minutes", 
                                  p_atp->brdspec.board_stats.rtc_stats[i].minutes);
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                  p_atp->brdspec.board_stats.rtc_stats[i].name,
                                  "seconds", 
                                  p_atp->brdspec.board_stats.rtc_stats[i].seconds);
    }

    for (i = 0; i < p_atp->brdspec.board_stats.spidev_num; i++) {
        char jedec_str[11];
        snprintf(jedec_str, 11, "%02x%02x%02x%02x%02x",
                 p_atp->brdspec.board_stats.spidev_stats[i].jedec[0],
                 p_atp->brdspec.board_stats.spidev_stats[i].jedec[1],
                 p_atp->brdspec.board_stats.spidev_stats[i].jedec[2],
                 p_atp->brdspec.board_stats.spidev_stats[i].jedec[3],
                 p_atp->brdspec.board_stats.spidev_stats[i].jedec[4]);
        ccl_stat_msg_add_string_field(stat_msg, 
                                      p_atp->brdspec.board_stats.spidev_stats[i].name,
                                      "jedec", 
                                      jedec_str);
    }

    for (i = 0; i < p_atp->brdspec.board_stats.xadc_num; i++) {
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                       p_atp->brdspec.board_stats.xadc_stats[i].name,
                                       "temperature", 
                                       p_atp->brdspec.board_stats.xadc_stats[i].temp);
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                       p_atp->brdspec.board_stats.xadc_stats[i].name,
                                       "vccint", 
                                       p_atp->brdspec.board_stats.xadc_stats[i].vccint);
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                       p_atp->brdspec.board_stats.xadc_stats[i].name,
                                       "vccaux", 
                                       p_atp->brdspec.board_stats.xadc_stats[i].vccaux);
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                       p_atp->brdspec.board_stats.xadc_stats[i].name,
                                       "vbram", 
                                       p_atp->brdspec.board_stats.xadc_stats[i].vbram);
    }

    for (i = 0; i < p_atp->brdspec.board_stats.sysmon_num; i++) {
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                       p_atp->brdspec.board_stats.sysmon_stats[i].name,
                                       "temperature", 
                                       p_atp->brdspec.board_stats.sysmon_stats[i].temp);
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                       p_atp->brdspec.board_stats.sysmon_stats[i].name,
                                       "vccint", 
                                       p_atp->brdspec.board_stats.sysmon_stats[i].vccint);
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                       p_atp->brdspec.board_stats.sysmon_stats[i].name,
                                       "vccaux", 
                                       p_atp->brdspec.board_stats.sysmon_stats[i].vccaux);
        ccl_stat_msg_add_numeric_field(stat_msg, 
                                       p_atp->brdspec.board_stats.sysmon_stats[i].name,
                                       "vbram", 
                                       p_atp->brdspec.board_stats.sysmon_stats[i].vbram);
    }

    for (i = 0; i < p_atp->brdspec.board_stats.dram_num; i++) {
            ccl_stat_msg_add_string_field(stat_msg, 
                                          p_atp->brdspec.board_stats.dram_stats[i].name,
                                          "boot_test", 
                                          p_atp->brdspec.board_stats.dram_stats[i].boot_test_status);
            ccl_stat_msg_add_string_field(stat_msg, 
                                          p_atp->brdspec.board_stats.dram_stats[i].name,
                                          "custom_test", 
                                          p_atp->brdspec.board_stats.dram_stats[i].custom_test_status);
            ccl_stat_msg_add_string_field(stat_msg, 
                                          p_atp->brdspec.board_stats.dram_stats[i].name,
                                          "prbs_test", 
                                          p_atp->brdspec.board_stats.dram_stats[i].prbs_test_status);
    }

    for (i = 0; i < p_atp->brdspec.board_stats.dram_num; i++) {
            ccl_stat_msg_add_string_field(stat_msg, 
                                          p_atp->brdspec.board_stats.eeprom_spd_stats[i].name,
                                          "module_sn", 
                                          p_atp->brdspec.board_stats.eeprom_spd_stats[i].eeprom_spd_info.module_sn);
            ccl_stat_msg_add_string_field(stat_msg, 
                                          p_atp->brdspec.board_stats.eeprom_spd_stats[i].name,
                                          "module_pn", 
                                          p_atp->brdspec.board_stats.eeprom_spd_stats[i].eeprom_spd_info.module_pn);
            ccl_stat_msg_add_string_field(stat_msg, 
                                          p_atp->brdspec.board_stats.eeprom_spd_stats[i].name,
                                          "manufacturer_id", 
                                          p_atp->brdspec.board_stats.eeprom_spd_stats[i].eeprom_spd_info.manufacturer_id);
    }

    char *stat_str = ccl_stat_msg_to_string(stat_msg);
    PDEBUGG("%s\n\n", stat_str);
    free(stat_str);

    if (p_atp->es_config.enable) {
        char path[DEVMAN_DEVNAME_LEN];
        snprintf(path, DEVMAN_DEVNAME_LEN, "tsg-%d-monitor/_doc", p_atp->es_config.index);
        http_cluster_node_conn_t node_conn;
        stat_str = ccl_stat_msg_to_string_unformatted(stat_msg);
        PDEBUGG("ES config: msg: %p, msg_len: %d, enable: %d, index: %d, hostname: %s, port: %d\n", 
                stat_str,
                strlen(stat_str),
                p_atp->es_config.enable,
                p_atp->es_config.index,
                p_atp->node.host_name,
                p_atp->node.port);
        PDEBUGG("%s\n", stat_str);
        if (!stat_str) 
            goto _prepare_board_stat_end;

        PRINTL("before send to es: time=%u\n",
               time(NULL));

        _ret = http_cluster_node_send_post(&p_atp->node_conn,
                                           path,
                                           stat_str,
                                           strlen(stat_str),
                                           NULL,
                                           "application/json");
        free(stat_str);
        if (_ret) {
            ccl_syslog_err("Error! http_cluster_node_send_post()\n");
            goto _prepare_board_stat_end;
        }
        PRINTL("after send to es: time=%u\n",
               time(NULL));
    }

    time_t curr_time = time(NULL);
    p_atp->stat_sender.prev_send_time = curr_time;

_prepare_board_stat_end:
    ATP_MUTEX_UNLOCK(p_atp->mutex);
    return;
}

static ccl_err_t _reset_dev(char *devname, b_i32 discr, b_u32 idx)
{
    devo_t devo;

    if (!devname) {
        PDEBUG("Error! Invalid input parameters\n");
        ATP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    _ret = devman_device_object_identify(devname, discr, &devo);
    if ( _ret || !devo) {
        ccl_syslog_err("Error! Can't identify %s/%d discr object\n", 
               devname, discr);
        ATP_ERROR_RETURN(CCL_FAIL);
    }
    _ret = devman_attr_array_set_word(devo, "reset", idx, 1);
    if (_ret) {
        ccl_syslog_err("Error! Can't set \"reset\" attribute for %s/%d discr object\n", 
               devname, discr);
        ATP_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

static ccl_err_t _init_dev(char *devname, b_i32 discr, b_u32 idx)
{
    devo_t devo;

    if (!devname) {
        PDEBUG("Error! Invalid input parameters\n");
        ATP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    _ret = devman_device_object_identify(devname, discr, &devo);
    if ( _ret || !devo) {
        ccl_syslog_err("Error! Can't identify %s/%d discr object\n", 
               devname, discr);
        ATP_ERROR_RETURN(CCL_FAIL);
    }
    _ret = devman_attr_array_set_word(devo, "init", idx, 1);
    if (_ret) {
        ccl_syslog_err("Error! Can't set \"init\" attribute for %s/%d discr object\n", 
               devname, discr);
        ATP_ERROR_RETURN(_ret);
    }
}

static ccl_err_t _init_devices(void)
{
    b_i32      i;

    for (i = 0; i < BRDDEV_AXISPI_NUM; i++) {
        PDEBUG("AXISPI %d init\n", i);
        _ret = _init_dev("axispi", i, 0);
        if ( _ret ) {
            ccl_syslog_err("Error! _init_dev \"axispi\" for %d discr failed\n", 
                   i);
//            ATP_ERROR_RETURN(CCL_FAIL);
        }
    }
    for (i = 0; i < BRDDEV_SPIDEV_NUM; i++) {
        PDEBUG("SPIDEV %d init\n", i);
        _ret = _init_dev("spidev", i, 0);
        if ( _ret ) {
            ccl_syslog_err("Error! _init_dev \"spidev\" for %d discr failed\n", 
                   i);
//            ATP_ERROR_RETURN(CCL_FAIL);
        }
    }
    for (i = 0; i < BRDDEV_PORTS_NUM; i++) {
        PDEBUG("PORT %d init\n", i);
        _ret = _init_dev("portmap", 0, i);
        if ( _ret ) {
            ccl_syslog_err("Error! _init_dev \"portmap\" for port %d failed\n", 
                   i);
//            ATP_ERROR_RETURN(CCL_FAIL);
        }
    }
    for (i = 0; i < BRDDEV_I2CCORE_NUM; i++) {
        PDEBUG("I2CCORE %d init\n", i);
        _ret = _init_dev("i2ccore", i, 0);
        if ( _ret ) {
            ccl_syslog_err("Error! _init_dev \"i2ccore\" for %d discr failed\n", 
                   i);
//            ATP_ERROR_RETURN(CCL_FAIL);
        }
    }
    for (i = 0; i < BRDDEV_PMBUS_NUM; i++) {
        PDEBUG("PMBUS %d init\n", i);
        _ret = _init_dev("pmbus", i, 0);
        if ( _ret ) {
            ccl_syslog_err("Error! _init_dev \"pmbus\" for %d discr failed\n", 
                   i);
//            ATP_ERROR_RETURN(CCL_FAIL);
        }
    }
    for (i = 0; i < BRDDEV_RTC_NUM; i++) {
        PDEBUG("RTC %d init\n", i);
        _ret = _init_dev("rtc", i, 0);
        if ( _ret ) {
            ccl_syslog_err("Error! _init_dev \"rtc\" for %d discr failed\n", 
                   i);
//            ATP_ERROR_RETURN(CCL_FAIL);
        }
    }
    for (i = 0; i < BRDDEV_CURRMON_NUM; i++) {
        PDEBUG("Current Monitor %d init\n", i);
        _ret = _init_dev("currmon", i, 0);
        if ( _ret ) {
            ccl_syslog_err("Error! _init_dev \"currmon\" for %d discr failed\n", 
                   i);
//            ATP_ERROR_RETURN(CCL_FAIL);
        }
    }
    for (i = 0; i < BRDDEV_FANCTLS_NUM; i++) {
        PDEBUG("Fan Controller %d init\n", i);
        _ret = _init_dev("fan", i, 0);
        if ( _ret ) {
            ccl_syslog_err("Error! _init_dev \"fan\" for %d discr failed\n", 
                   i);
//            ATP_ERROR_RETURN(CCL_FAIL);
        }
    }
    for (i = 0; i < BRDDEV_XADC_NUM; i++) {
        PDEBUG("XADC %d init\n", i);
        _ret = _init_dev("xadc", i, 0);
        if ( _ret ) {
            ccl_syslog_err("Error! _init_dev \"xadc\" for %d discr failed\n", 
                   i);
//            ATP_ERROR_RETURN(CCL_FAIL);
        }
    }
    for (i = 0; i < BRDDEV_SYSMON_NUM; i++) {
        PDEBUG("SYSMON %d init\n", i);
        _ret = _init_dev("sysmon", i, 0);
        if ( _ret ) {
            ccl_syslog_err("Error! _init_dev \"sysmon\" for %d discr failed\n", 
                   i);
//            ATP_ERROR_RETURN(CCL_FAIL);
        }
    }
#if 0
    for (i = 0; i < BRDDEV_AXIGPIO_NUM; i++) {
        PDEBUG("AXIGPIO %d init\n", i);
        _ret = _init_dev("axigpio", i, 0);
        if ( _ret ) {
            ccl_syslog_err("Error! _init_dev \"axigpio\" for %d discr failed\n", 
                   i);
            ATP_ERROR_RETURN(CCL_FAIL);
        }
    }
    for (i = 0; i < BRDDEV_AXIUART_NUM; i++) {
        PDEBUG("AXIUART %d init\n", i);
        _ret = _init_dev("axiuart", i, 0);
        if ( _ret ) {
            ccl_syslog_err("Error! _init_dev \"axiuart\" for %d discr failed\n", 
                   i);
            ATP_ERROR_RETURN(CCL_FAIL);
        }
    }
    for (i = 0; i < BRDDEV_EEPROM_NUM; i++) {
        PDEBUG("EEPROM %d init\n", i);
        _ret = _init_dev("eeprom", i, 0);
        if ( _ret ) {
            ccl_syslog_err("Error! _init_dev \"eeprom\" for %d discr failed\n", 
                   i);
            ATP_ERROR_RETURN(CCL_FAIL);
        }
    }
    for (i = 0; i < BRDDEV_I2CIO_NUM; i++) {
        PDEBUG("I2CIO %d init\n", i);
        _ret = _init_dev("i2cio", i, 0);
        if ( _ret ) {
            ccl_syslog_err("Error! _init_dev \"i2cio\" for %d discr failed\n", 
                   i);
            ATP_ERROR_RETURN(CCL_FAIL);
        }
    }
    for (i = 0; i < BRDDEV_SMARTCARD_NUM; i++) {
        PDEBUG("SMARTCARD %d init\n", i);
        _ret = _init_dev("smartcard", i, 0);
        if ( _ret ) {
            ccl_syslog_err("Error! _init_dev \"smartcard\" for %d discr failed\n", 
                   i);
            ATP_ERROR_RETURN(CCL_FAIL);
        }
    }
    for (i = 0; i < BRDDEV_LCD_NUM; i++) {
        PDEBUG("LCD %d init\n", i);
        _ret = _init_dev("lcd", i, 0);
        if ( _ret ) {
            ccl_syslog_err("Error! _init_dev \"lcd\" for %d discr failed\n", 
                   i);
            ATP_ERROR_RETURN(CCL_FAIL);
        }
    }
    for (i = 0; i < BRDDEV_TRANS_NUM; i++) {
        PDEBUG("TRANS %d init\n", i);
        _ret = _init_dev("trans", i, 0);
        if ( _ret ) {
            ccl_syslog_err("Error! _init_dev \"trans\" for %d discr failed\n", 
                   i);
            ATP_ERROR_RETURN(CCL_FAIL);
        }
    }
#endif
    return CCL_OK;
}

static ccl_err_t _configure_tests(void)
{
    return CCL_OK;
}

static ccl_err_t _run_test_dev(char *devname, 
                               b_i32 discr, b_u32 idx, 
                               void *p_stats, b_u32 size)
{
    devo_t devo;

    if ( !devname ) {
        PDEBUG("Error! Invalid input parameters\n");
        ATP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    _ret = devman_device_object_identify(devname, discr, &devo);
    if ( _ret || !devo) {
        ccl_syslog_err("Error! Can't identify %s/%d discr object\n", 
               devname, discr);
        ATP_ERROR_RETURN(CCL_FAIL);
    }

    PRINTL("Tested device: %s/%d idx=%d\n", devname, discr, idx);

    _ret = devman_attr_array_set_buff(devo, "run_test", idx, 
                                      (b_u8 *)p_stats, size);
    if (_ret) {
        ccl_syslog_err("Error! Can't set \"run_test\" attribute for %s/%d discr idx: %d object\n", 
               devname, discr, idx);
        ATP_ERROR_RETURN(_ret);
    }
    return CCL_OK;
}

static ccl_err_t _run_tests(atp_ctx_t *p_atp)
{
    b_i32      i;
    b_u32      dummy;
    b_u32      size = 1;

    if (!p_atp) {
        PDEBUG("Error! Invalid input parameters\n");
        ATP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    for (i = 0; i < BRDDEV_SPIDEV_NUM; i++) {
        if (i==0) 
            PDEBUG("SPIDEV test\n");
        _ret = _run_test_dev("spidev", i, 0, 
                             &p_atp->brdspec.board_stats.spidev_stats[i],
                             sizeof(spidev_stats_t));
        if ( _ret ) {
            ccl_syslog_err("Error! _run_test_dev \"spidev\" for %d discr failed\n", 
                   i);
//            ATP_ERROR_RETURN(CCL_FAIL);
        }
    }
    for (i = 0; i < BRDDEV_PORTS_NUM; i++) {
        PDEBUG("PORT %d test\n", i);
        _ret = _run_test_dev("portmap", 0, i, 
                             &p_atp->brdspec.board_stats.port_stats[i],
                             sizeof(port_stats_t));
        if ( _ret ) {
            ccl_syslog_err("Error! _run_test_dev \"portmap/0\" for %d idx failed\n", 
                   i);
//            ATP_ERROR_RETURN(CCL_FAIL);
        }
    }
    for (i = 0; i < BRDDEV_PMBUS_NUM; i++) {
        if (i == 0) 
            PDEBUG("PMBUS test\n");
        //TODO: remove this pmbus i2c problem workaround
        if (i == BRDDEV_PMBUS_NUM - 1) {
            _ret = _init_dev("i2ccore", 1, 0);
            if ( _ret ) {
                ccl_syslog_err("Error! _init_dev \"i2ccore\" for %d discr failed\n", 1);
    //            ATP_ERROR_RETURN(CCL_FAIL);
            }
        } else {
            _ret = _reset_dev("i2ccore", 1, 0);
            if ( _ret ) {
                ccl_syslog_err("Error! _reset_dev \"i2ccore\" for %d discr failed\n", 1);
    //            ATP_ERROR_RETURN(CCL_FAIL);
            }
        } 
        _ret = _run_test_dev("pmbus", i, 0, 
                             &p_atp->brdspec.board_stats.pmbus_stats[i],
                             sizeof(pmbus_stats_t));
        if ( _ret ) {
            ccl_syslog_err("Error! _run_test_dev \"pmbus\" for %d discr failed\n", 
                   i);
//            ATP_ERROR_RETURN(CCL_FAIL);
        }
    }
    for (i = 0; i < BRDDEV_RTC_NUM; i++) {
        if (i==0) 
            PDEBUG("RTC test\n");
        _ret = _run_test_dev("rtc", i, 0,
                             &p_atp->brdspec.board_stats.rtc_stats[i],
                             sizeof(rtc_stats_t));
        if ( _ret ) {
            ccl_syslog_err("Error! _run_test_dev \"rtc\" for %d discr failed\n", 
                   i);
//            ATP_ERROR_RETURN(CCL_FAIL);
        }
    }
    for (i = 0; i < BRDDEV_CURRMON_NUM; i++) {
        if (i==0) 
            PDEBUG("Current Monitor test\n");
        _ret = _run_test_dev("currmon", i, 0, 
                             &p_atp->brdspec.board_stats.currmon_stats[i], 
                             sizeof(currmon_stats_t));
        if ( _ret ) {
            ccl_syslog_err("Error! _run_test_dev \"currmon\" for %d discr failed\n", 
                   i);
//            ATP_ERROR_RETURN(CCL_FAIL);
        }
    }
    for (i = 0; i < BRDDEV_FANCTLS_NUM; i++) {
        if (i == 0) 
            PDEBUG("Fan Controller test\n");
        _ret = _run_test_dev("fan", i, 0, 
                             &p_atp->brdspec.board_stats.fanctl_stats[i], 
                             sizeof(fanctl_stats_t));
        if ( _ret ) {
            ccl_syslog_err("Error! _run_test_dev \"fan\" for %d discr failed\n", 
                   i);
//            ATP_ERROR_RETURN(CCL_FAIL);
        }
    }
    for (i = 0; i < BRDDEV_XADC_NUM; i++) {
        if (i==0) 
            PDEBUG("XADC test\n");
        _ret = _run_test_dev("xadc", i, 0, 
                             &p_atp->brdspec.board_stats.xadc_stats[i], 
                             sizeof(xadc_stats_t));
        if ( _ret ) {
            ccl_syslog_err("Error! _run_test_dev \"xadc\" for %d discr failed\n", 
                   i);
//            ATP_ERROR_RETURN(CCL_FAIL);
        }
    }
    for (i = 0; i < BRDDEV_SYSMON_NUM; i++) {
        if (i==0) 
            PDEBUG("SYSMON test\n");
        _ret = _run_test_dev("sysmon", i, 0, 
                             &p_atp->brdspec.board_stats.sysmon_stats[i], 
                             sizeof(sysmon_stats_t));
        if ( _ret ) {
            ccl_syslog_err("Error! _run_test_dev \"sysmon\" for %d discr failed\n", 
                   i);
//            ATP_ERROR_RETURN(CCL_FAIL);
        }
    }
    for (i = 0; i < BRDDEV_DRAM_NUM; i++) {
        if (i==0) 
            PDEBUG("DRAMTESTER test\n");
        _ret = _run_test_dev("dramtester", i, 0, 
                             &p_atp->brdspec.board_stats.dram_stats[i],
                             sizeof(dram_stats_t));
        if ( _ret ) {
            ccl_syslog_err("Error! _run_test_dev \"dramtester\" for %d discr failed\n", 
                   i);
//            ATP_ERROR_RETURN(CCL_FAIL);
        }
    }

#define SPD_EEPROM_FIRST_ID  7
    /* Test only SPD EEPROMS */
    for (i = SPD_EEPROM_FIRST_ID; i < BRDDEV_EEPROM_NUM; i++) {
        if (i==SPD_EEPROM_FIRST_ID) 
            PDEBUG("EEPROM test\n");
        _ret = _run_test_dev("eeprom", i, 0, 
                             &p_atp->brdspec.board_stats.eeprom_spd_stats[i-SPD_EEPROM_FIRST_ID],
                             sizeof(eeprom_spd_stats_t));
        if ( _ret ) {
            ccl_syslog_err("Error! _run_test_dev \"eeprom\" for %d discr failed\n", 
                   i);
            ATP_ERROR_RETURN(CCL_FAIL);
        }
    }
#if 0
    for (i = 0; i < BRDDEV_I2CIO_NUM; i++) {
        if (i==0) 
            PDEBUG("I2CIO test\n");
        _ret = _run_test_dev("i2cio", i, 0, &dummy, size);
        if ( _ret ) {
            ccl_syslog_err("Error! _run_test_dev \"i2cio\" for %d discr failed\n", 
                   i);
            ATP_ERROR_RETURN(CCL_FAIL);
        }
    }
    for (i = 0; i < BRDDEV_UART_NUM; i++) {
        if (i==0) 
            PDEBUG("UART test\n");
        _ret = _run_test_dev("axiuart", i, 0, &dummy, size);
        if ( _ret ) {
            ccl_syslog_err("Error! _run_test_dev \"axiuart\" for %d discr failed\n", 
                   i);
            ATP_ERROR_RETURN(CCL_FAIL);
        }
    }
    for (i = 0; i < BRDDEV_TRANS_NUM; i++) {
        if (i==0) 
            PDEBUG("TRANS test\n");
        _ret = _run_test_dev("trans", i, 0, &dummy, size);
        if ( _ret ) {
            ccl_syslog_err("Error! _run_test_dev \"trans\" for %d discr failed\n", 
                   i);
            ATP_ERROR_RETURN(CCL_FAIL);
        }
    }
#endif

    return CCL_OK;
}

static ccl_err_t _fatp_cmd_bist(devo_t atpdevo, 
                                device_cmd_parm_t parms[], 
                                b_u16 parms_num) 
{
    atp_ctx_t *p_atp = devman_device_private(atpdevo);
    b_i32 i;

    _ret = _init_devices();
    if (_ret) {
        PDEBUG("Error! _init_devices failed\n");
        ATP_ERROR_RETURN(_ret);
    }
#if 0
    _ret = _configure_tests();
    if (_ret) {
        PDEBUG("Error! _configure_tests failed\n");
        ATP_ERROR_RETURN(_ret);
    }
#endif
    while (1) {
        ATP_MUTEX_LOCK(p_atp->mutex);
        _run_tests(p_atp);
        ATP_MUTEX_UNLOCK(p_atp->mutex);
        usleep(1000000);
    }

    return CCL_OK;
}

/* FPGA Programmer GPIO definition */
#define IMX_FPGA_GPIO_INIT_B_BANK         1
#define IMX_FPGA_GPIO_INIT_B_IO           0

#define IMX_FPGA_GPIO_DONE_BANK           1
#define IMX_FPGA_GPIO_DONE_IO             1

#define IMX_FPGA_GPIO_PROGRAM_B_BANK      1
#define IMX_FPGA_GPIO_PROGRAM_B_IO        2

#define IMX_FPGA_GPIO_SERIAL_OUT_BANK     1
#define IMX_FPGA_GPIO_SERIAL_OUT_IO       3

#define IMX_FPGA_GPIO_CCLK_BANK           1
#define IMX_FPGA_GPIO_CCLK_IO             4

#define IMX_GPIO_BASE_ADDRESS             0x30210000
#define IMX_GPIO_BANK_SIZE                0x10000
#define IMX_GPIO_BLOCK_SIZE               0x10000

#define IMX_GPIO_OUTPUT                   1
#define IMX_GPIO_INPUT                    0

static inline void _shift_cclk(unsigned long *gpio_addr, b_u32 count)
{
    b_u32 i;

    ccl_gpio_clear(gpio_addr, 
               IMX_FPGA_GPIO_CCLK_BANK, 
               IMX_GPIO_BANK_SIZE,
               IMX_FPGA_GPIO_CCLK_IO);
    for (i = 0; i < count; i++) {
        ccl_gpio_set(gpio_addr, 
                 IMX_FPGA_GPIO_CCLK_BANK, 
                 IMX_GPIO_BANK_SIZE,
                 IMX_FPGA_GPIO_CCLK_IO);
        ccl_gpio_clear(gpio_addr, 
                   IMX_FPGA_GPIO_CCLK_BANK, 
                   IMX_GPIO_BANK_SIZE,
                   IMX_FPGA_GPIO_CCLK_IO);
    }
}

static inline void _shift_word_out(unsigned long *gpio_addr, b_u32 data)
{
    b_u32 i;
    b_u8 val;

    for (i = 31; i >= 0; --i) {
        val = !!(data & (1 << i));
        if (val) 
            ccl_gpio_set(gpio_addr, 
                     IMX_FPGA_GPIO_SERIAL_OUT_BANK, 
                     IMX_GPIO_BANK_SIZE,
                     IMX_FPGA_GPIO_SERIAL_OUT_IO);
        else 
            ccl_gpio_clear(gpio_addr, 
                       IMX_FPGA_GPIO_SERIAL_OUT_BANK, 
                       IMX_GPIO_BANK_SIZE,
                       IMX_FPGA_GPIO_SERIAL_OUT_IO);
        _shift_cclk(gpio_addr, 1);


    }
}

static ccl_err_t _fpga_cmd_program(devo_t atpdevo, 
                                   device_cmd_parm_t parms[], 
                                   b_u16 parms_num) 
{
    FILE    *filep = NULL;
    int      file_size, buf_len;
    char    *buf;    
    char    *devname = (char *)parms[0].value;
    char    *file_name = (char *)parms[1].value;
    b_bool artix;
    b_i32               opcode;
    devman_ioctl_parm_t ioctl_parm;

    _ret = CCL_OK;

    if (devname[0] == 'a') 
        artix = CCL_TRUE;
    else if (devname[0] = 'v') 
        artix = CCL_FALSE;
    else {
        PDEBUG("Error! devname: %s\n", devname);
        ATP_ERROR_RETURN(CCL_FAIL);
    }

    /* check the input stream(openability, readability, size) */ 
    filep = fopen(file_name, "r");
    if ( !filep ) {
        PDEBUG("Error! Can't open FPGA image file %s\n", file_name);
        ATP_ERROR_RETURN(CCL_FAIL);
    }
    /* get length of the file, check errors */
    fseek(filep, 0, SEEK_END);
    file_size = ftell(filep);	
    fseek(filep, 0, SEEK_SET);
    if ( file_size < 0 ) {
        fclose(filep);
        PDEBUG("Error! The file %s is not a seekable\n", file_name);
        ATP_ERROR_RETURN(CCL_FAIL);
    } else if ( !file_size ) {
        fclose(filep);
        PDEBUG("Error! The file %s is empty\n", file_name);
        ATP_ERROR_RETURN(CCL_FAIL);
    }

    /* buffer allocation */
    buf = malloc(file_size);
    if ( !buf ) {
        fclose(filep);
        PDEBUG("Error! Can't allocate the image temporary room\n");
        ATP_ERROR_RETURN(CCL_FAIL);
    }

    /* copy image into the memory */
    if ( fread(buf, 1, file_size, filep) < file_size ) {
        PDEBUG("Error! Can't read the file %s\n", file_name);
        _ret = CCL_FAIL;
        goto _lfpga_program_exit;
    }

    memset(&ioctl_parm, 0, sizeof(devman_ioctl_parm_t));
    opcode = DEVMAN_IOC_EXEC;
    ioctl_parm.p      = (char *)buf;
    ioctl_parm.length = file_size;

    PDEBUG("file name: %s, file size: %d\n",
           file_name, file_size);

    if (artix) {
        strcpy(ioctl_parm.name, "gpio");
        _ret = devman_ioctl(opcode, &ioctl_parm);    
        if ( _ret ) {
            PDEBUG("Error! Can't operate the kernel device! _ret=%d\n", _ret);
            ATP_ERROR_RETURN(CCL_ERRNO);
        }
    } else {
        devo_t devo;
        b_i32 discr = 1;
        b_u32 dir; //0xfc6
        #define OP_MODE_M0_MASK      0x00000008 /* output */
        #define OP_MODE_M1_MASK      0x00000010 /* output */
        #define OP_MODE_M2_MASK      0x00000020 /* output */
        #define PROG_B_MASK          0x00000001 /* output */
        #define INIT_B_MASK          0x00000002 /* input */
        #define DONE_MASK            0x00000004 /* input */       
        #define BITSTREAM_BUSY_MASK  0x00001000 /* input */
        b_u32 data;
        char buf1[DATA_ARRAY_LEN];
        b_u32 i;

        for (i = 0; i < DATA_ARRAY_LEN; i++) 
            buf1[i] = 0xff;

        _ret = devman_device_object_identify("axigpio", discr, &devo);
        if ( _ret || !devo) {
            PDEBUG("Error! Can't identify \"axigpio\", %d discr object\n", 
                   discr);
            return CCL_FAIL;
        }
        _ret = devman_attr_array_get_word(devo, "tri", 0, &dir);
        if (_ret) {
            PDEBUG("devman_attr_array_get_word error\n");
            ATP_ERROR_RETURN(_ret);
        }
        PDEBUG("direction=0x%08x\n", dir);

        dir &= ~(PROG_B_MASK |
                 OP_MODE_M0_MASK | 
                 OP_MODE_M1_MASK |
                 OP_MODE_M2_MASK);
        dir |= INIT_B_MASK | 
               DONE_MASK |
               BITSTREAM_BUSY_MASK;
        PDEBUG("direction=0x%08x\n", dir);
        _ret = devman_attr_array_set_word(devo, "tri", 0, dir);
        if (_ret) {
            PDEBUG("devman_attr_array_set_word error\n");
            ATP_ERROR_RETURN(_ret);
        }
        _ret = devman_attr_array_get_word(devo, "data", 0, &data);
        if (_ret) {
            PDEBUG("devman_attr_array_get_word error\n");
            ATP_ERROR_RETURN(_ret);
        }
        PDEBUG("data=0x%08x, direction=0x%08x\n", data, dir);
        data |= OP_MODE_M0_MASK |
                OP_MODE_M1_MASK |
                OP_MODE_M2_MASK;
        data &= ~PROG_B_MASK;
        PDEBUG("data=0x%08x, direction=0x%08x, PROG_B=%d\n", 
               data, dir, !!(data & PROG_B_MASK));
        _ret = devman_attr_array_set_word(devo, "data", 0, data);
        if (_ret) {
            PDEBUG("devman_attr_array_set_word error\n");
            ATP_ERROR_RETURN(_ret);
        }
        usleep(1000);
        /* check INIT_B */
        _ret = devman_attr_array_get_word(devo, "data", 0, &data);
        if (_ret) {
            PDEBUG("devman_attr_array_get_word error\n");
            ATP_ERROR_RETURN(_ret);
        }
        PDEBUG("INIT_B must be 0: data=0x%08x, direction=0x%08x, INIT_B=%d\n", 
               data, dir, !!(data & INIT_B_MASK));
        if (data & INIT_B_MASK) {
            PDEBUG("INIT_B must be 0\n");
            ATP_ERROR_RETURN(_ret);
        }
        /* Assert PROG_B */
        data |= PROG_B_MASK;
        PDEBUG("Assert PROG_B: data=0x%08x, direction=0x%08x, PROG_B=%d\n", 
               data, dir, !!(data & PROG_B_MASK));
        _ret = devman_attr_array_set_word(devo, "data", 0, data);
        if (_ret) {
            PDEBUG("devman_attr_array_set_word error\n");
            ATP_ERROR_RETURN(_ret);
        }
        /* check INIT_B */
        i = 0;
        _ret = devman_attr_array_get_word(devo, "data", 0, &data);
        if (_ret) {
            PDEBUG("devman_attr_array_get_word error\n");
            ATP_ERROR_RETURN(_ret);
        }
        PDEBUG("check INIT_B (must be 1:): data=0x%08x, INIT_B=%d\n", data, !!(data & INIT_B_MASK));
        while (!(data & INIT_B_MASK)) {
            if (i++ > 1000000) {
              PDEBUG("INIT_B has not gone high: i=%d\n", i);
              ATP_ERROR_RETURN(CCL_FAIL);
            }
            _ret = devman_attr_array_get_word(devo, "data", 0, &data);
            if (_ret) {
                PDEBUG("devman_attr_array_get_word error\n");
                ATP_ERROR_RETURN(_ret);
            }
        }
        PDEBUG("start sending bitstream: data=0x%08x, INIT_B=%d\n", 
               data, !!(data & INIT_B_MASK));
        /* start sending bitstream*/
#define PROGRAM_OPTIMIZATION
#ifdef PROGRAM_OPTIMIZATION
        ioctl_parm.offset = BITSTREAM_BUFFER_OFFSET;
        strcpy(ioctl_parm.name, "fpga0");
        _ret = devman_ioctl(opcode, &ioctl_parm);    
        if ( _ret ) {
            PDEBUG("Error! Can't operate the kernel device! _ret=%d\n", _ret);
            ATP_ERROR_RETURN(CCL_ERRNO);
        }
#else
        strcpy(ioctl_parm.name, "fpga1");
        _ret = devman_ioctl(opcode, &ioctl_parm);    
        if ( _ret ) {
            PDEBUG("Error! Can't operate the kernel device! _ret=%d\n", _ret);
            ATP_ERROR_RETURN(CCL_ERRNO);
        }
        _ret = devman_attr_array_get_word(devo, "data", 0, &data);
        if (_ret) {
            PDEBUG("devman_attr_array_get_word error\n");
            ATP_ERROR_RETURN(_ret);
        }
        PDEBUG("data=0x%08x, direction=0x%08x\n", data, dir);
        if (!(data & INIT_B_MASK)) {
            PDEBUG("INIT_B is low\n");
            ATP_ERROR_RETURN(CCL_FAIL);
        }
        ioctl_parm.p      = (char *)buf1;
        ioctl_parm.length = DATA_ARRAY_LEN;
        i = 0;
        while (!(data & DONE_MASK)) {
            _ret = devman_ioctl(opcode, &ioctl_parm);    
            if ( _ret ) {
                PDEBUG("Error! Can't operate the kernel device! _ret=%d\n", _ret);
                ATP_ERROR_RETURN(CCL_ERRNO);
            }
            _ret = devman_attr_array_get_word(devo, "data", 0, &data);
            if (_ret) {
                PDEBUG("devman_attr_array_get_word error\n");
                ATP_ERROR_RETURN(_ret);
            }
            if (!(i++ % 1000)) 
                PDEBUG("i=%d, data=0x%08x, direction=0x%08x\n", i, data, dir);            
        }
#endif /* PROGRAM_OPTIMIZATION */
    }
_lfpga_program_exit:    
    /* release the resources */
    free(buf);
    fclose(filep);

    return _ret;

}

static ccl_err_t _fatp_cmd_load_test(devo_t atpdevo, 
                                     device_cmd_parm_t parms[], 
                                     b_u16 parms_num)
{
    b_u32 regval;
    b_u32 pattern;
    b_u8  slr;
    b_u32 strobe_mask = 0x80000000;
    b_u32 slr_mask = 0x03000000;

    pattern = (b_u32)parms[0].value;

    _ret = devspibus_write_buff("fpga1", AXIGPIO_VIRTEX_GENERAL_CTRL_OFFSET+8, 
                                0, (b_u8 *)&pattern, sizeof(b_u32));

    for (slr = 0; slr < 3; slr++) {
        regval = ((slr << 24) & slr_mask);
        _ret |= devspibus_write_buff("fpga1", AXIGPIO_VIRTEX_GENERAL_CTRL_OFFSET, 
                                     0, (b_u8 *)&regval, sizeof(b_u32));
        regval |= strobe_mask;
        _ret |= devspibus_write_buff("fpga1", AXIGPIO_VIRTEX_GENERAL_CTRL_OFFSET, 
                                     0, (b_u8 *)&regval, sizeof(b_u32));
        regval &= ~strobe_mask;
        _ret |= devspibus_write_buff("fpga1", AXIGPIO_VIRTEX_GENERAL_CTRL_OFFSET, 
                                     0, (b_u8 *)&regval, sizeof(b_u32));
        if ( _ret ) {
            PDEBUG("devspibus_write_buff error\n");
            ATP_ERROR_RETURN(_ret);
        }
    }

    return CCL_OK;
}

static ccl_err_t _fatp_cmd_boot_virtex(devo_t atpdevo, 
                                       device_cmd_parm_t parms[], 
                                       b_u16 parms_num)
{
    b_u32 regval;

    regval = 0x1fc6;
    /* set direction pins */
    _ret = devspibus_write_buff("fpga0", AXIGPIO_VIRTEX_CTRL_OFFSET+4, 
                                0, (b_u8 *)&regval, sizeof(b_u32));
    /* strobe the PROG_B pin low */
    regval = 0x708;
    _ret |= devspibus_write_buff("fpga0", AXIGPIO_VIRTEX_CTRL_OFFSET, 
                                 0, (b_u8 *)&regval, sizeof(b_u32));
    /* strobe the PROG_B pin high */
    regval = 0x709;
    _ret |= devspibus_write_buff("fpga0", AXIGPIO_VIRTEX_CTRL_OFFSET, 
                                 0, (b_u8 *)&regval, sizeof(b_u32));
    if ( _ret ) {
        PDEBUG("devspibus_write_buff error\n");
        ATP_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

static ccl_err_t _fatp_get_board_id(b_u8 *board_id)
{
    b_i32 fd;
    char in[2] = {0, 0};
    b_i32 id; 
    char buf[DEVMAN_DEVNAME_LEN];
    int  gpio_brd = IMX_GPIO_NR(FPGA_GPIO_BOARD_ID_BANK, FPGA_GPIO_BOARD_ID_IO);

    fd = open("/sys/class/gpio/export", O_WRONLY);
    if (fd == -1) {
        PDEBUG("Error! Can't open /sys/class/gpio/export\n");
        ATP_ERROR_RETURN(CCL_FAIL);
    }

    snprintf(buf, DEVMAN_DEVNAME_LEN, "%d", gpio_brd);
    _rc = write(fd, buf, strlen(buf)); 
    if (_rc == -1) {
        PDEBUG("Error writing %d to /sys/class/gpio/export\n",
               gpio_brd);
        ATP_ERROR_RETURN(CCL_FAIL);
    }
    close(fd);

    snprintf(buf, DEVMAN_DEVNAME_LEN, 
             "/sys/class/gpio/gpio%d/value", 
             gpio_brd);
    fd = open(buf, O_RDWR | O_SYNC);
    if (fd == -1) {
        PDEBUG("Unable to open %s\n", buf);
        ATP_ERROR_RETURN(CCL_FAIL);
    }
    if (read(fd, in, 1) != 1) {
        PDEBUG("Error reading from %d\n", buf);
        ATP_ERROR_RETURN(CCL_FAIL);
    }
    close(fd);
    id=atoi(in);

    fd = open("/sys/class/gpio/unexport", O_WRONLY);
    if (fd == -1) {
        PDEBUG("Unable to open /sys/class/gpio/unexport");
        ATP_ERROR_RETURN(CCL_FAIL);
    }
    snprintf(buf, DEVMAN_DEVNAME_LEN, "%d", gpio_brd);
    _rc = write(fd, buf, strlen(buf)); 
    if (_rc == -1) {
        PDEBUG("Error writing %d to /sys/class/gpio/export\n",
               gpio_brd);
        ATP_ERROR_RETURN(CCL_FAIL);
    }
    close(fd);

    *board_id = id;

    return CCL_OK;
}

static ccl_err_t _fatp_cmd_get_board_id(devo_t atpdevo, 
                                        device_cmd_parm_t parms[], 
                                        b_u16 parms_num)
{
    b_u8 board_id;

    _ret = _fatp_get_board_id(&board_id);
    if ( _ret ) {
        PDEBUG("_fatp_get_board_id error\n");
        ATP_ERROR_RETURN(_ret);
    }

    PRINTL("board_id = %d\n",board_id);

    return CCL_OK;
}

static ccl_err_t _fatp_get_buid_time(atp_ctx_t *p_atp)
{
    b_u32      offset; 
    const char build_time[] = __DATE__ " " __TIME__;
    char       devname[DEVMAN_DEVNAME_LEN]; 
    b_u32      regval;                          
    b_u32      year, month, day;                
    b_u32      hour, minute;                    
    b_u32      i;
    char       *devptr;

    if ( !p_atp ) {
        PDEBUG("Error! Invalid input parameters\n");
        ATP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    strncpy(p_atp->build_time.cpu, build_time, DEVMAN_DEVNAME_LEN);

    for (i = 0; i < 2; i++) {
        if (i == 0) {
            offset = AXIGPIO_ARTIX_DATE_TIME_OFFSET;
            devptr = p_atp->build_time.artix;

        } else {
            offset = AXIGPIO_VIRTEX_DATE_TIME_OFFSET;
            devptr = p_atp->build_time.virtex;
        }
        snprintf(devname, DEVMAN_DEVNAME_LEN, "fpga%d", offset >= VIRTEX_OFFSET);
        _ret = devspibus_read_buff(devname, 
                                   offset,
                                   0, 
                                   (b_u8 *)&regval, 
                                   sizeof(b_u32));
        if ( _ret ) {
            PDEBUG("devspibus_read_buff error\n");
            ATP_ERROR_RETURN(_ret);
        }
        year = (regval >> 16) & 0xffff;
        month = (regval >> 8) & 0xff;
        day = regval & 0xff;

        offset += 8;

        _ret = devspibus_read_buff(devname, 
                                   offset,
                                   0, 
                                   (b_u8 *)&regval, 
                                   sizeof(b_u32));
        if ( _ret ) {
            PDEBUG("devspibus_read_buff error\n");
            ATP_ERROR_RETURN(_ret);
        }
        hour = (regval >> 8) & 0xff;
        minute = regval & 0xff;

        snprintf(devptr, DEVMAN_DEVNAME_LEN, "%02x.%02x.%04x %02x:%02x",
                 day, month, year, hour, minute);
    }

    return CCL_OK;
}

static ccl_err_t _fatp_cmd_version_time(devo_t atpdevo, 
                                        device_cmd_parm_t parms[], 
                                        b_u16 parms_num) 
{
    atp_ctx_t  *p_atp = devman_device_private(atpdevo);
    
    _ret = _fatp_get_buid_time(p_atp);
    if (_ret) {
        PDEBUG("Error! _fatp_get_buid_time\n");
        ATP_ERROR_RETURN(CCL_FAIL);
    }

    PRINTL("CPU build time: %s\n", p_atp->build_time.cpu);
    PRINTL("Artix build time: %s \n", p_atp->build_time.artix);
    PRINTL("Virtex build time: %s \n", p_atp->build_time.virtex);

    return CCL_OK;
}

static ccl_err_t _fatp_cmd_init_logger(devo_t atpdevo, 
                                       device_cmd_parm_t parms[], 
                                       b_u16 parms_num) 
{
    ccl_logger_conf_t cfg;
    char              *file_path = (char *)parms[0].value;
    char              *log_level = (char *)parms[1].value;
    char              *log_syslog_level = (char *)parms[2].value;

    if (log_level[0] == 'e' && log_level[1] == 'm') 
        cfg.log_lvl = LOG_LVL_EMERGENCY;
    else if (log_level[0] == 'a' && log_level[1] == 'l') 
        cfg.log_lvl = LOG_LVL_ALERT;
    else if (log_level[0] == 'c' && log_level[1] == 'r') 
        cfg.log_lvl = LOG_LVL_CRITICAL;
    else if (log_level[0] == 'e' && log_level[1] == 'r') 
        cfg.log_lvl = LOG_LVL_ERROR;
    else if (log_level[0] == 'w' && log_level[1] == 'a') 
        cfg.log_lvl = LOG_LVL_WARNING;
    else if (log_level[0] == 'n' && log_level[1] == 'o') 
        cfg.log_lvl = LOG_LVL_NOTICE;
    else if (log_level[0] == 'i' && log_level[1] == 'n') 
        cfg.log_lvl = LOG_LVL_INFO;
    else if (log_level[0] == 'd' && log_level[1] == 'e') 
        cfg.log_lvl = LOG_LVL_DEBUG;
    else {
        PDEBUG("Error! log level: %s\n", log_level);
        ATP_ERROR_RETURN(CCL_FAIL);
    }
    if (log_syslog_level[0] == 'e' && log_syslog_level[1] == 'm') 
        cfg.log_syslog_lvl = LOG_LVL_EMERGENCY;
    else if (log_syslog_level[0] == 'a' && log_syslog_level[1] == 'l') 
        cfg.log_syslog_lvl = LOG_LVL_ALERT;
    else if (log_syslog_level[0] == 'c' && log_syslog_level[1] == 'r') 
        cfg.log_syslog_lvl = LOG_LVL_CRITICAL;
    else if (log_syslog_level[0] == 'e' && log_syslog_level[1] == 'r') 
        cfg.log_syslog_lvl = LOG_LVL_ERROR;
    else if (log_syslog_level[0] == 'w' && log_syslog_level[1] == 'a') 
        cfg.log_syslog_lvl = LOG_LVL_WARNING;
    else if (log_syslog_level[0] == 'n' && log_syslog_level[1] == 'o') 
        cfg.log_syslog_lvl = LOG_LVL_NOTICE;
    else if (log_syslog_level[0] == 'i' && log_syslog_level[1] == 'n') 
        cfg.log_syslog_lvl = LOG_LVL_INFO;
    else if (log_syslog_level[0] == 'd' && log_syslog_level[1] == 'e') 
        cfg.log_syslog_lvl = LOG_LVL_DEBUG;
    else {
        PDEBUG("Error! log syslog level: %s\n", log_syslog_level);
        ATP_ERROR_RETURN(CCL_FAIL);
    }
    cfg.log_file = file_path;
    cfg.module_name = "ATP";

    logger = ccl_create_logger(&cfg);

    return CCL_OK;
}

static ccl_err_t _kr_test(kr_tester_t *kr)
{
    devo_t devo;
    b_u32  port;
    b_u32  rx_block_lock;
    b_u32  rx_status1;
    b_u32  an_status;
    b_u32  lt_status2;
    b_u32  lt_status3;

    port = LOGPORT_KR_0 + 4 * kr->cpu + kr->link;

    _ret = devman_device_object_identify("hses10g", port, &devo);
    if ( _ret || !devo) {
        PDEBUG("Error! Can't identify \"hses10g\", %d discr object\n", 
               port);
        ATP_ERROR_RETURN(CCL_FAIL);
    }

    /* Read twice (latched value) */
    _ret = devman_attr_get_word(devo, "status_rx_block_lock", &rx_block_lock);
    _ret |= devman_attr_get_word(devo, "status_rx_block_lock", &rx_block_lock);
    if (_ret) {
        PDEBUG("read status_rx_block_lock error\n");
        ATP_ERROR_RETURN(_ret);
    }
    /* Read twice (latched value) */
    _ret = devman_attr_get_word(devo, "rx_status1", &rx_status1);
    _ret |= devman_attr_get_word(devo, "rx_status1", &rx_status1);
    if (_ret) {
        PDEBUG("read rx_status1 error\n");
        ATP_ERROR_RETURN(_ret);
    }
    _ret = devman_attr_get_word(devo, "an_status", &an_status);
    if (_ret) {
        PDEBUG("read an_status error\n");
        ATP_ERROR_RETURN(_ret);
    }
    _ret = devman_attr_get_word(devo, "lt_status2", &lt_status2);
    if (_ret) {
        PDEBUG("read lt_status2 error\n");
        ATP_ERROR_RETURN(_ret);
    }
    _ret = devman_attr_get_word(devo, "lt_status3", &lt_status3);
    if (_ret) {
        PDEBUG("read lt_status3 error\n");
        ATP_ERROR_RETURN(_ret);
    }

    kr->rx_block_lock = rx_block_lock & 0x1;
    kr->rx_remote_fault = (rx_status1 >> 5) & 0x1;
    kr->rx_local_fault = (rx_status1 >> 6) & 0x1;
    kr->rx_int_local_fault = (rx_status1 >> 7) & 0x1;
    kr->rx_rec_local_fault = (rx_status1 >> 8) & 0x1;
    kr->an_autoneg_complete = (an_status >> 2) & 0x1;
    kr->lt_training = lt_status2 & 0x1;
    kr->lt_frame_lock = (lt_status2 >> 16) & 0x1;
    kr->lt_signal_detect = lt_status3 & 0x1;
    kr->lt_training_fail = (lt_status3 >> 16) & 0x1;

    return CCL_OK;
}

static void _kr_test_timer_cb(union sigval sigval) {
    kr_tester_t *kr = (kr_tester_t *)sigval.sival_ptr;

    _ret = _kr_test(kr);
    if (CCL_OK == _ret) {
       PRINTL("%u%5d%15d%12d%15d%15d%15d%19d%15d%15d%15d\n",
              time(NULL), 
              kr->rx_block_lock,
              kr->an_autoneg_complete,
              kr->lt_training,
              kr->lt_frame_lock,
              kr->lt_signal_detect,
              kr->lt_training_fail,
              kr->rx_remote_fault,
              kr->rx_local_fault,
              kr->rx_int_local_fault,
              kr->rx_rec_local_fault);
    } else
        PRINTL("_kr_test error: %d\n", _ret);

    return;
}

static ccl_err_t _fatp_cmd_kr_test(devo_t atpdevo, 
                                   device_cmd_parm_t parms[], 
                                   b_u16 parms_num) 
{
    b_u32       cpu = (b_u32 *)parms[0].value;
    b_u32       link = (b_u32 *)parms[1].value;
    b_u32       interval = (b_u32 *)parms[2].value;
    struct sigevent sev;
    atp_ctx_t *p_atp = devman_device_private(atpdevo);

    p_atp->kr_tester.cpu = cpu;
    p_atp->kr_tester.link = link;
    p_atp->kr_tester.interval = interval;

    memset(&sev, 0, sizeof(sev));
    sev.sigev_notify = SIGEV_THREAD;
    sev.sigev_notify_function = _kr_test_timer_cb;
    sev.sigev_value.sival_ptr = &p_atp->kr_tester;

    PRINTL("time      |rx block lock|an complete|lt training|lt frame lock|lt signal detect|lt training fail|rx remote flt|rx local flt|rx int. local flt|rx rcvd. local flt|\n");

    SYSCALL_PANIC_FAILURE_CHECK(
                timer_create(CLOCK_REALTIME, &sev, &p_atp->kr_tester.timer),
                0, &logger,
                "timer_create");

    struct itimerspec itspec;
    memset(&itspec, 0, sizeof(itspec));

    itspec.it_interval.tv_sec = itspec.it_value.tv_sec = interval;

    SYSCALL_PANIC_FAILURE_CHECK(
                timer_settime(p_atp->kr_tester.timer, 0, &itspec, NULL),
                0, &logger,
                "timer_settime");

    return CCL_OK;
}
/* device commands */
static device_cmd_t _cmds[] = 
{
    { 
        .name = "readb", .f_cmd = _fatp_cmd_fpga_readb, .parms_num = 2,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "offset", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" }, 
            { .name = "count", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%d" }, 
        }, 
        .help = "Test raw read specified Artix registers area in byte-width.\n"
                "Usage:\n"
                "  readb <offset> <count>\n"
    },
    { 
        .name = "readh", .f_cmd = _fatp_cmd_fpga_readh, .parms_num = 2,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "offset", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" }, 
            { .name = "count", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%d" }, 
        }, 
        .help = "Test raw read specified Artix registers area in half-width.\n"
                "Usage:\n"
                "  readh <offset> <count>\n"
    },
    { 
        .name = "readw", .f_cmd = _fatp_cmd_fpga_readw, .parms_num = 2,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "offset", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" }, 
            { .name = "count", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%d" }, 
        }, 
        .help = "Test raw read specified Artix registers area in word-width.\n"
                "Usage:\n"
                "  readw <offset> <count>\n"
    },
    { 
        .name = "dump", .f_cmd = _fatp_cmd_fpga_dump, .parms_num = 2,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "offset", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" }, 
            { .name = "count", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%d" }, 
        }, 
        .help = "Dump specified Artix memory area.\n"
                "Usage:\n"
                "  dump <offset> <count>\n"
    },
    { 
        .name = "writeb", .f_cmd = _fatp_cmd_fpga_writeb, .parms_num = 2,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "offset", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" }, 
            { .name = "value", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" }, 
        }, 
        .help = "Test raw write specified Artix registers area in byte-width.\n"
                "Usage:\n"
                "  writeb <offset> <value>\n"
    },
    { 
        .name = "writeh", .f_cmd = _fatp_cmd_fpga_writeh, .parms_num = 2,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "offset", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" }, 
            { .name = "value", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" }, 
        }, 
        .help = "Test raw write specified Artix registers area in half-width.\n"
                "Usage:\n"
                "  writeh <offset> <value>\n"
    },
    { 
        .name = "writew", .f_cmd = _fatp_cmd_fpga_writew, .parms_num = 2,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "offset", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" }, 
            { .name = "value", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" }, 
        }, 
        .help = "Test raw write specified Artix registers area in word-width.\n"
                "Usage:\n"
                "  writew <offset> <value>\n"
    },
    { 
        .name = "bwr", .f_cmd = _fatp_cmd_fpga_bwr, .parms_num = 2,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "offset", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" }, 
            { .name = "value", .type = eDEVMAN_ATTR_BUFF, .size = 128 }, 
        }, 
        .help = "Test raw write specified Artix registers area in burst mode.\n"
    },
    { 
        .name = "flash_readb", .f_cmd = _fatp_cmd_flash_readb, .parms_num = 2,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "offset", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" }, 
            { .name = "count", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%d" }, 
        }, 
        .help = "Test raw read CPUs' boot flashes in byte-width.\n"
                "Usage:\n"
                "  flash_readb <offset> <count>\n"
    },
    { 
        .name = "flash_readh", .f_cmd = _fatp_cmd_flash_readh, .parms_num = 2,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "offset", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" }, 
            { .name = "count", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%d" }, 
        }, 
        .help = "Test raw read CPUs' boot flashes in half-width.\n"
                "Usage:\n"
                "  flash_readh <offset> <count>\n"
    },
    { 
        .name = "flash_readw", .f_cmd = _fatp_cmd_flash_readw, .parms_num = 2,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "offset", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" }, 
            { .name = "count", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%d" }, 
        }, 
        .help = "Test raw read CPUs' boot flashes in word-width.\n"
                "Usage:\n"
                "  flash_readw <offset> <count>\n"
    },
    { 
        .name = "flash_writeb", .f_cmd = _fatp_cmd_flash_writeb, .parms_num = 2,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "offset", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" }, 
            { .name = "value", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" }, 
        }, 
        .help = "Test raw read CPUs' boot flashes in byte-width.\n"
                "Usage:\n"
                "  flash_writeb <offset> <value>\n"
    },
    { 
        .name = "flash_writeh", .f_cmd = _fatp_cmd_flash_writeh, .parms_num = 2,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "offset", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" }, 
            { .name = "value", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" }, 
        }, 
        .help = "Test raw read CPUs' boot flashes in half-width.\n"
                "Usage:\n"
                "  flash_writeh <offset> <value>\n"
    },
    { 
        .name = "flash_writew", .f_cmd = _fatp_cmd_flash_writew, .parms_num = 2,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "offset", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" }, 
            { .name = "value", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" }, 
        }, 
        .help = "Test raw read CPUs' boot flashes in word-width.\n"
                "Usage:\n"
                "  flash_writew <offset> <value>\n"
    },
    { 
        .name = "flashrd", .f_cmd = _fatp_cmd_flashrd, .parms_num = 3,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "id", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
            { .name = "offset", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" }, 
            { .name = "count", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%d" }, 
        }, 
        .help = "Test spi read access to CPUs' boot flashes.\n"
                "Usage:\n"
                "  flashrd <id> <offset> <count>\n"
                "  flash id can be one of the following:\n"
                "    0 - Main boot SPI flash\n"
                "    1 - Control boot SPI flash\n"
                "    2 - ATP Xeon1 boot SPI flash\n"
                "    3 - ATP Xeon2 boot SPI flash\n"
                "    4 - ATP Xeon3 boot SPI flash\n"
                "    5 - ATP Xeon4 boot SPI flash\n"
                "    6 - Carrier Xeon1 boot SPI flash\n"
                "    7 - Carrier Xeon2 boot SPI flash\n"
                "    8 - Carrier Xeon3 boot SPI flash\n"
                "    9 - Carrier Xeon4 boot SPI flash\n"
    },
    { 
        .name = "flashwr", .f_cmd = _fatp_cmd_flashwr, .parms_num = 3,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "id", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
            { .name = "offset", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" }, 
            { .name = "value", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" }, 
        }, 
        .help = "Test spi write access to CPUs' boot flashes.\n"
                "Usage:\n"
                "  flashwr <id> <offset> <value>\n"
                "  flash id can be one of the following:\n"
                "    0 - Main boot SPI flash\n"
                "    1 - Control boot SPI flash\n"
                "    2 - ATP Xeon1 boot SPI flash\n"
                "    3 - ATP Xeon2 boot SPI flash\n"
                "    4 - ATP Xeon3 boot SPI flash\n"
                "    5 - ATP Xeon4 boot SPI flash\n"
                "    6 - Carrier Xeon1 boot SPI flash\n"
                "    7 - Carrier Xeon2 boot SPI flash\n"
                "    8 - Carrier Xeon3 boot SPI flash\n"
                "    9 - Carrier Xeon4 boot SPI flash\n"
    },
    { 
        .name = "mboxctrl", .f_cmd = _fatp_cmd_mboxctrl, .parms_num = 1,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "value", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" }, 
        }, 
        .help = "Modify Mailbox control register.\n"
                "Usage:\n"
                "  mboxctrl <value>\n"
    },
    { 
        .name = "mboxsts", .f_cmd = _fatp_cmd_mboxsts, .parms_num = 0,
        .help = "Read Mailbox status register.\n"
                "Usage:\n"
                "  mboxsts\n"
    },
    { 
        .name = "mboxrd", .f_cmd = _fatp_cmd_mboxrd, .parms_num = 2,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "id", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
            { .name = "count", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" }, 
        }, 
        .help = "Read number of bytes from the Mailbox.\n"
                "Usage:\n"
                "  mboxrd <id> <count>\n"
                "  Mailbox id can be one of the following:\n"
                "    0 - Main CPU mailbox\n"
                "    1 - Main CPU mailbox\n"
                "    2 - Xeon1 mailbox\n"
                "    3 - Xeon2 mailbox\n"
                "    4 - Xeon3 mailbox\n"
                "    5 - Xeon4 mailbox\n"
                "    6 - ATP mailbox\n"
    },
    { 
        .name = "mboxwr", .f_cmd = _fatp_cmd_mboxwr, .parms_num = 2,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "id", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
            { .name = "value", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" }, 
        }, 
        .help = "Write single byte to the Mailbox.\n"
                "Usage:\n"
                "  mboxwr <id> <value>\n"
                "  Mailbox id can be one of the following:\n"
                "    0 - Main CPU mailbox\n"
                "    1 - Control CPU mailbox\n"
                "    2 - Xeon1 mailbox\n"
                "    3 - Xeon2 mailbox\n"
                "    4 - Xeon3 mailbox\n"
                "    5 - Xeon4 mailbox\n"
                "    6 - ATP mailbox\n"
    },
    { 
        .name = "temac_init", .f_cmd = _fatp_cmd_temac_init, .parms_num = 1,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "port", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
        }, 
        .help = "Artix Temac IP Core init (default configuration).\n"
                "Usage:\n"
                "  temac_init <port>\n"
    },
    { 
        .name = "temac_link", .f_cmd = _fatp_cmd_temac_link, .parms_num = 1,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "port", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
        }, 
        .help = "Read link status for specific port.\n"
                "Usage:\n"
                "  temac_link <port>\n"
    },
    { 
        .name = "temac_traffic", .f_cmd = _fatp_cmd_temac_traffic, .parms_num = 2,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "port", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
            { .name = "mode", .type = eDEVMAN_ATTR_STRING, .size = DEVMAN_CMD_PARM_LEN, .format = "%s" },
        }, 
        .help = "Verify traffic for specific port in specific test mode.\n"
                "Usage:\n"
                "  temac_traffic <port> <mode>\n"
                "  test mode can be one of the following:\n"
                "    \"loopback\"\n"
                "    \"traffic_gen\"\n"
    },
    { 
        .name = "axispi_flashrd", .f_cmd = _fatp_cmd_axispi_flashrd, .parms_num = 3,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "id", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
            { .name = "offset", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" }, 
            { .name = "count", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%d" }, 
        }, 
        .help = "Test AXISPI IP core read access to various flashes.\n"
                "Usage:\n"
                "  axispi_flashrd <id> <offset> <count>\n"
                "  flash id can be one of the following:\n"
                "    0 - Main boot SPI flash\n"
                "    1 - Control boot SPI flash\n"
                "    2 - ATP Xeon1 boot SPI flash\n"
                "    3 - ATP Xeon2 boot SPI flash\n"
                "    4 - ATP Xeon3 boot SPI flash\n"
                "    5 - ATP Xeon4 boot SPI flash\n"
                "    6 - Carrier Xeon1 boot SPI flash\n"
                "    7 - Carrier Xeon2 boot SPI flash\n"
                "    8 - Carrier Xeon3 boot SPI flash\n"
                "    9 - Carrier Xeon4 boot SPI flash\n"
                "    10 - Generic flash 1\n"
                "    11 - Generic flash 2\n"
    },
    { 
        .name = "axispi_flashwr", .f_cmd = _fatp_cmd_axispi_flashwr, .parms_num = 3,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "id", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
            { .name = "offset", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" }, 
            { .name = "value", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" }, 
        }, 
        .help = "Test AXISPI IP core write access to various flashes.\n"
                "Usage:\n"
                "  axispi_flashwr <id> <offset> <value>\n"
                "  flash id can be one of the following:\n"
                "    0 - Main boot SPI flash\n"
                "    1 - Control boot SPI flash\n"
                "    2 - ATP Xeon1 boot SPI flash\n"
                "    3 - ATP Xeon2 boot SPI flash\n"
                "    4 - ATP Xeon3 boot SPI flash\n"
                "    5 - ATP Xeon4 boot SPI flash\n"
                "    6 - Carrier Xeon1 boot SPI flash\n"
                "    7 - Carrier Xeon2 boot SPI flash\n"
                "    8 - Carrier Xeon3 boot SPI flash\n"
                "    9 - Carrier Xeon4 boot SPI flash\n"
                "    10 - Generic flash 1\n"
                "    11 - Generic flash 2\n"
    },
    { 
        .name = "axigpio_cpu_ctrl", .f_cmd = _fatp_cmd_axigpio_cpu_ctrl, .parms_num = 1,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "id", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
        }, 
        .help = "Test CPU control GPIOs\n"
                "Usage:\n"
                "  axigpio_cpu_ctrl <id> \n"
                "  gpio id can be one of the following:\n"
                "    0 - Main CPU IRQ (out)\n"
                "    1 - Control CPU IRQ (out)\n"
                "    2 - Main CPU GPIO 1 (bidi)\n"
                "    3 - Main CPU GPIO 2 (bidi)\n"
                "    4 - Control CPU GPIO 1 (bidi)\n"
                "    5 - Control CPU GPIO 2 (bidi)\n"
    },
    { 
        .name = "axigpio_virtex_ctrl", .f_cmd = _fatp_cmd_axigpio_virtex_ctrl, .parms_num = 1,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "id", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
        }, 
        .help = "Test Virtex control GPIOs\n"
                "Usage:\n"
                "  axigpio_virtex_ctrl <id> \n"
                "  gpio id can be one of the following:\n"
                "    0    - PROG_B (out)\n"
                "    1    - INIT_B (out)\n"
                "    2    - DONE (in)\n"
                "    3-5  - MODE (out)\n"
                "    8-13 - GPIO (bidi)\n"
    },
    { 
        .name = "axigpio_smartcard_ctrl", .f_cmd = _fatp_cmd_axigpio_smartcard_ctrl, .parms_num = 1,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "id", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
        }, 
        .help = "Smart Card control GPIOs\n"
                "Usage:\n"
                "  axigpio_smartcard_ctrl <id> \n"
                "  gpio id can be one of the following:\n"
                "    0 - POWER_ON (out)\n"
                "    1 - RESET (out)\n"
                "    2 - PRESENT (in)\n"
    },
    { 
        .name = "axigpio_flashmux_ctrl", .f_cmd = _fatp_cmd_axigpio_flashmux_ctrl, .parms_num = 1,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "id", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
        }, 
        .help = "Flash Mux control GPIOs\n"
                "Usage:\n"
                "  axigpio_flashmux_ctrl <id> \n"
                "  gpio id can be one of the following:\n"
                "    0 - COMX Flash to Xeon (out)\n"
                "    1 - COMX Flash to Artix (out)\n"
                "    2 - SMARC Flash to CPU (out)\n"
                "    3 - SMARC Flash to Artix (out)\n"
    },
    { 
        .name = "axigpio_atp_ctrl", .f_cmd = _fatp_cmd_axigpio_atp_ctrl, .parms_num = 1,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "id", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
        }, 
        .help = "ATP control GPIOs\n"
                "Usage:\n"
                "  axigpio_atp_ctrl <id> \n"
                "  gpio id can be one of the following:\n"
                "    0 - ATP_GPIO0 (bidi)\n"
                "    1 - ATP_GPIO1 (bidi)\n"
                "    2 - ATP_IRQ (out)\n"
                "    3 - ATP_LOAD_PHY_IRQ (in)\n"
                "    4 - ATP_PRESENT (in)\n"
    },
    { 
        .name = "axigpio_pwrrst_ctrl", .f_cmd = _fatp_cmd_axigpio_pwrrst_ctrl, .parms_num = 1,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "id", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
        }, 
        .help = "Power & Reset control GPIOs\n"
                "Usage:\n"
                "  axigpio_pwrrst_ctrl <id> \n"
                "  gpio id can be one of the following:\n"
                "    0 - PS0 OK (in)\n"
                "    1 - PS1 OK (in)\n"
                "    2 - PMBUS Alert (in)\n"
                "    3 - PMBUS CLIENT Alert (in)\n"
                "    4 - Power Off (out)\n"
                "    5 - BATFAIL (in)\n"
                "    6 - XEON1 Power On (out)\n"
                "    7 - XEON2 Power On (out)\n"
                "    8 - XEON3 Power On (out)\n"
                "    9 - XEON4 Power On (out)\n"
                "    10 - XEON1 Reset (out)\n"
                "    11 - XEON2 Reset (out)\n"
                "    12 - XEON3 Reset (out)\n"
                "    13 - XEON4 Reset (out)\n"
                "    14 - OOB PHY Reset (out)\n"
                "    15 - Virtex Reset (out)\n"
                "    16 - ATP Reset (out)\n"
    },
    { 
        .name = "axigpio_xeon_ctrl", .f_cmd = _fatp_cmd_axigpio_xeon_ctrl, .parms_num = 1,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "id", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
        }, 
        .help = "XEONs (Application CPUs) control GPIOs\n"
                "Usage:\n"
                "  axigpio_xeon_ctrl <id> \n"
                "  gpio id can be one of the following:\n"
                "    8  - XEON1 Present (in)\n"
                "    9  - XEON2 Present (in)\n"
                "    10 - XEON3 Present (in)\n"
                "    11 - XEON4 Present (in)\n"
                "    12 - XEON1 IRQ (out)\n"
                "    13 - XEON2 IRQ (out)\n"
                "    14 - XEON3 IRQ (out)\n"
                "    15 - XEON4 IRQ (out)\n"
                "    16 - XEON1 GPI (out)\n"
                "    17 - XEON2 GPI (out)\n"
                "    18 - XEON3 GPI (out)\n"
                "    19 - XEON4 GPI (out)\n"
                "    20 - XEON1 GPO (in)\n"
                "    21 - XEON2 GPO (in)\n"
                "    22 - XEON3 GPO (in)\n"
                "    23 - XEON4 GPO (in)\n"
    },
    { 
        .name = "axigpio_fp_ctrl", .f_cmd = _fatp_cmd_axigpio_fp_ctrl, .parms_num = 1,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "id", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
        }, 
        .help = "Front Panel control GPIOs\n"
                "Usage:\n"
                "  axigpio_fp_ctrl <id> \n"
                "  gpio id can be one of the following:\n"
                "    0  - CLI Red LED (out)\n"
                "    1  - CLI Green LED (out)\n"
                "    2  - Clear LED (out)\n"
                "    3  - OOB PHY IRQ (in)\n"
                "    5  - Dry Contact 0 (in)\n"
                "    6  - Dry Contact 1 (in)\n"
                "    7  - Dry Contact 2 (in)\n"
                "    8  - Clear Button (in)\n"
                "    9  - Emergency Button (in)\n"
                "    10 - USB Power (out)\n"
                "    11 - USB Present (in)\n"
    },
    { 
        .name = "axigpio_gen_artix_ctrl", .f_cmd = _fatp_cmd_axigpio_gen_artix_ctrl, .parms_num = 1,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "id", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
        }, 
        .help = "General Control (Artix) control GPIOs\n"
                "Usage:\n"
                "  axigpio_gen_artix_ctrl <id> \n"
                "  gpio id can be one of the following:\n"
                "    0-7  - Debug LED (out)\n"
                "    8-15 - DIP Switch (in)\n"
    },
    { 
        .name = "axigpio_gen_virtex_ctrl", .f_cmd = _fatp_cmd_axigpio_gen_virtex_ctrl, .parms_num = 1,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "id", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
        }, 
        .help = "General Control (Virtex) control GPIOs\n"
                "Usage:\n"
                "  axigpio_gen_artix_ctrl <id>\n"
                "  gpio id can be one of the following:\n"
                "    0-7  - Debug LED (out)\n"
                "    8-15 - DIP Switch (in)\n"
    },
    { 
        .name = "axigpio_sfp_presence", .f_cmd = _fatp_cmd_axigpio_sfp_presence, .parms_num = 2,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "side", .type = eDEVMAN_ATTR_STRING, .size = DEVMAN_CMD_PARM_LEN, .format = "%s" },
            { .name = "port", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
        }, 
        .help = "Verify SFP presence\n"
                "Usage:\n"
                "  axigpio_sfp_presence <side> <port>\n"
                "  side can be one of the following:\n"
                "    \"client\"\n"
                "    \"network\"\n"
                "    port can be one of the following:\n"
                "    0-11 - SFP port\n"
    },
    { 
        .name = "axigpio_qsfp_presence", .f_cmd = _fatp_cmd_axigpio_qsfp_presence, .parms_num = 2,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "side", .type = eDEVMAN_ATTR_STRING, .size = DEVMAN_CMD_PARM_LEN, .format = "%s" },
            { .name = "port", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
        }, 
        .help = "Verify QSFP presence\n"
                "Usage:\n"
                "  axigpio_qsfp_presence <side> <port> \n"
                "  side can be one of the following:\n"
                "    \"client\"\n"
                "    \"network\"\n"
                "    port can be one of the following:\n"
                "    0-1 - QSFP port\n"
    },
    { 
        .name = "axigpio_sfp_tx_disable", .f_cmd = _fatp_cmd_axigpio_sfp_tx_disable, .parms_num = 2,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "side", .type = eDEVMAN_ATTR_STRING, .size = DEVMAN_CMD_PARM_LEN, .format = "%s" },
            { .name = "port", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
        }, 
        .help = "Set SFP TX disable\n"
                "Usage:\n"
                "  axigpio_sfp_tx_disable <side> <port> \n"
                "  side can be one of the following:\n"
                "    \"client\"\n"
                "    \"network\"\n"
                "    port can be one of the following:\n"
                "    0-11 - SFP port\n"
    },
    { 
        .name = "axigpio_qsfp_reset", .f_cmd = _fatp_cmd_axigpio_qsfp_reset, .parms_num = 2,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "side", .type = eDEVMAN_ATTR_STRING, .size = DEVMAN_CMD_PARM_LEN, .format = "%s" },
            { .name = "port", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
        }, 
        .help = "Reset QSFP module\n"
                "Usage:\n"
                "  axigpio_sfp_tx_disable <side> <port> \n"
                "  side can be one of the following:\n"
                "    \"client\"\n"
                "    \"network\"\n"
                "    port can be one of the following:\n"
                "    0-1 - QSFP port\n"
    },
    { 
        .name = "axigpio_qsfp_irq", .f_cmd = _fatp_cmd_axigpio_qsfp_irq, .parms_num = 2,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "side", .type = eDEVMAN_ATTR_STRING, .size = DEVMAN_CMD_PARM_LEN, .format = "%s" },
            { .name = "port", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
        }, 
        .help = "Generate QSFP interrupt\n"
                "Usage:\n"
                "  axigpio_sfp_tx_disable <side> <port> \n"
                "  side can be one of the following:\n"
                "    \"client\"\n"
                "    \"network\"\n"
                "    port can be one of the following:\n"
                "    0-1 - QSFP port\n"
    },
    { 
        .name = "axigpio_i2cio_irq", .f_cmd = _fatp_cmd_axigpio_i2cio_irq, .parms_num = 1,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "side", .type = eDEVMAN_ATTR_STRING, .size = DEVMAN_CMD_PARM_LEN, .format = "%s" },
        }, 
        .help = "Generate IO Expander interrupt\n"
                "Usage:\n"
                "  axigpio_sfp_tx_disable <side> <port> \n"
                "  side can be one of the following:\n"
                "    \"client\"\n"
                "    \"network\"\n"
    },
    { 
        .name = "axigpio_sfp_traffic_en", .f_cmd = _fatp_cmd_axigpio_sfp_traffic_en, .parms_num = 2,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "side", .type = eDEVMAN_ATTR_STRING, .size = DEVMAN_CMD_PARM_LEN, .format = "%s" },
            { .name = "port", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
        }, 
        .help = "SFP traffic enable\n"
                "Usage:\n"
                "  axigpio_sfp_traffic_en <side> <port> \n"
                "  side can be one of the following:\n"
                "    \"client\"\n"
                "    \"network\"\n"
                "    port can be one of the following:\n"
                "    0-11 - SFP port\n"
    },
    { 
        .name = "axigpio_qsfp_traffic_en", .f_cmd = _fatp_cmd_axigpio_qsfp_traffic_en, .parms_num = 2,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "side", .type = eDEVMAN_ATTR_STRING, .size = DEVMAN_CMD_PARM_LEN, .format = "%s" },
            { .name = "port", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
        }, 
        .help = "QSFP traffic enable\n"
                "Usage:\n"
                "  axigpio_qsfp_traffic_en <side> <port> \n"
                "  side can be one of the following:\n"
                "    \"client\"\n"
                "    \"network\"\n"
                "    port can be one of the following:\n"
                "    0-1 - QSFP port\n"
    },
    { 
        .name = "axigpio_1g_traffic_en", .f_cmd = _fatp_cmd_axigpio_1g_traffic_en, .parms_num = 2,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "side", .type = eDEVMAN_ATTR_STRING, .size = DEVMAN_CMD_PARM_LEN, .format = "%s" },
            { .name = "port", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
        }, 
        .help = "Internal 1G port traffic enable\n"
                "Usage:\n"
                "  axigpio_1g_traffic_en <side> <port> \n"
                "  side can be one of the following:\n"
                "    \"client\"\n"
                "    \"network\"\n"
                "    port can be one of the following:\n"
                "    0-1 - internal 1G port\n"
    },
    { 
        .name = "axigpio_memory_traffic_en", .f_cmd = _fatp_cmd_axigpio_memory_traffic_en, .parms_num = 0,
        .help = "Memory traffic enable\n"
                "Usage:\n"
                "  axigpio_memory_traffic_en\n"
    },
    { 
        .name = "axigpio_is_memory_test_ok", .f_cmd = _fatp_cmd_axigpio_is_memory_test_ok, .parms_num = 0,
        .help = "Test whether the memory test passes\n"
                "Usage:\n"
                "  axigpio_is_memory_test_ok\n"
    },
    { 
        .name = "sfp_rx_los", .f_cmd = _fatp_cmd_sfp_rx_los, .parms_num = 2,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "side", .type = eDEVMAN_ATTR_STRING, .size = DEVMAN_CMD_PARM_LEN, .format = "%s" },
            { .name = "port", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
        }, 
        .help = "Read SFP RX LOS status\n"
                "Usage:\n"
                "  sfp_rx_los <side> <port> \n"
                "  side can be one of the following:\n"
                "    \"client\"\n"
                "    \"network\"\n"
                "    port can be one of the following:\n"
                "    0-11 - SFP port\n"
    },
    { 
        .name = "sfp_tx_flt", .f_cmd = _fatp_cmd_sfp_tx_flt, .parms_num = 2,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "side", .type = eDEVMAN_ATTR_STRING, .size = DEVMAN_CMD_PARM_LEN, .format = "%s" },
            { .name = "port", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
        }, 
        .help = "Read SFP TX FLT status\n"
                "Usage:\n"
                "  sfp_tx_flt <side> <port> \n"
                "  side can be one of the following:\n"
                "    \"client\"\n"
                "    \"network\"\n"
                "    port can be one of the following:\n"
                "    0-11 - SFP port\n"
    },
    { 
        .name = "sfp_rs", .f_cmd = _fatp_cmd_sfp_rs, .parms_num = 3,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "side", .type = eDEVMAN_ATTR_STRING, .size = DEVMAN_CMD_PARM_LEN, .format = "%s" },
            { .name = "port", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
            { .name = "rate", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
        }, 
        .help = "Set SFP rate\n"
                "Usage:\n"
                "  sfp_rs <side> <port> <rate>\n"
                "  side can be one of the following:\n"
                "    \"client\"\n"
                "    \"network\"\n"
                "    port can be one of the following:\n"
                "    0-11 - SFP port\n"
                "    rate can be one of the following:\n"
                "    1 - 1 Gbps\n"
                "    10 - 10 Gbps\n"
    },
    { 
        .name = "qsfp_modsell", .f_cmd = _fatp_cmd_qsfp_modsell, .parms_num = 3,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "side", .type = eDEVMAN_ATTR_STRING, .size = DEVMAN_CMD_PARM_LEN, .format = "%s" },
            { .name = "port", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
            { .name = "modsell", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
        }, 
        .help = "Test QSFP module select\n"
                "Usage:\n"
                "  qsfp_modsell <side> <port> <rate>\n"
                "  side can be one of the following:\n"
                "    \"client\"\n"
                "    \"network\"\n"
                "    port can be one of the following:\n"
                "    0-1 - QSFP port\n"
                "    modsell can be one of the following:\n"
                "    0 - QSFP module responds to 2-wire serial communication commands (i2c interface)\n"
                "    1 - QSFP module doesn't respond to 2-wire serial communication commands\n"
    },
    { 
        .name = "qsfp_lpmode", .f_cmd = _fatp_cmd_qsfp_lpmode, .parms_num = 3,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "side", .type = eDEVMAN_ATTR_STRING, .size = DEVMAN_CMD_PARM_LEN, .format = "%s" },
            { .name = "port", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
            { .name = "lpmode", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
        }, 
        .help = "Test QSFP LP (Low Power) mode\n"
                "Usage:\n"
                "  qsfp_modsell <side> <port> <rate>\n"
                "  side can be one of the following:\n"
                "    \"client\"\n"
                "    \"network\"\n"
                "    port can be one of the following:\n"
                "    0-1 - QSFP port\n"
                "    lpmode can be one of the following:\n"
                "    0 - //TODO: add explanation\n"
                "    1 - //TODO: add explanation\n"
    },
    { 
        .name = "uart_init", .f_cmd = _fatp_cmd_uart_init, .parms_num = 1,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "device", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
        }, 
        .help = "UART init\n"
                "Usage:\n"
                "  uart_init <device>\n"
                "  device can be one of the following:\n"
                "    1 - XEON1 UART\n"
                "    2 - XEON2 UART\n"
                "    3 - XEON3 UART\n"
                "    4 - XEON4 UART\n"
                "    5 - ATP UART\n"
                "    6 - Main CPU UART\n"
                "    7 - Control CPU UART\n"
                "    8 - CLI UART\n"
                "    9 - Smart Card UART\n"
    },
    { 
        .name = "uart_tx", .f_cmd = _fatp_cmd_uart_tx, .parms_num = 2,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "device", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
            { .name = "test_string", .type = eDEVMAN_ATTR_STRING, .size = DEVMAN_CMD_PARM_LEN, .format = "%s" }, 
        }, 
        .help = "Test UART TX\n"
                "Usage:\n"
                "  uart_tx <device> <test string>\n"
                "  device can be one of the following:\n"
                "    1 - XEON1 UART\n"
                "    2 - XEON2 UART\n"
                "    3 - XEON3 UART\n"
                "    4 - XEON4 UART\n"
                "    5 - ATP UART\n"
                "    6 - Main CPU UART\n"
                "    7 - Control CPU UART\n"
                "    8 - CLI UART\n"
                "    9 - Smart Card UART\n"
    },
    { 
        .name = "uart_rx", .f_cmd = _fatp_cmd_uart_rx, .parms_num = 3,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "device", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
            { .name = "count", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%d" }, 
            { .name = "test_string", .type = eDEVMAN_ATTR_STRING, .size = DEVMAN_CMD_PARM_LEN, .format = "%s", .flags = eDEVMAN_ATTRFLAG_OPTIONAL }, 
        }, 
        .help = "TEST UART RX\n"
                "Usage:\n"
                "  uart_rx <device> <count> [test_string]\n"
                "  device can be one of the following:\n"
                "    1 - XEON1 UART\n"
                "    2 - XEON2 UART\n"
                "    3 - XEON3 UART\n"
                "    4 - XEON4 UART\n"
                "    5 - ATP UART\n"
                "    6 - Main CPU UART\n"
                "    7 - Control CPU UART\n"
                "    8 - CLI UART\n"
                "    9 - Smart Card UART\n"
    },
    { 
        .name = "uart_test", .f_cmd = _fatp_cmd_uart_test, .parms_num = 2,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "device", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
            { .name = "count", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%d" }, 
        }, 
        .help = "TEST UART TX/RX\n"
                "Usage:\n"
                "  uart_test <device> <count> \n"
                "  device can be one of the following:\n"
                "    1 - XEON1 UART\n"
                "    2 - XEON2 UART\n"
                "    3 - XEON3 UART\n"
                "    4 - XEON4 UART\n"
                "    6 - Main CPU UART\n"
                "    7 - Control CPU UART\n"
    },
    { 
        .name = "uart_sc_atr_test", .f_cmd = _fatp_cmd_uart_sc_atr_test, .parms_num = 1,
            .parms = (device_cmd_parm_t[])
            { 
                { .name = "count", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%d" }, 
            }, 
        .help = "Smard Card UART Answer To Reset test\n"
                "Usage:\n"
                "  uart_sc_atr_test <count>\n"
    },
    { 
        .name = "uart_sc_init", .f_cmd = _fatp_cmd_uart_sc_init, .parms_num = 0,
        .help = "Smard Card UART init\n"
                "Usage:\n"
                "  uart_sc_init\n"
    },
    { 
        .name = "uart_sc_presence", .f_cmd = _fatp_cmd_uart_sc_presence, .parms_num = 0,
        .help = "Test Smard Card presence\n"
                "Usage:\n"
                "  uart_sc_presence\n"
    },
    { 
        .name = "uart_sc_poweron", .f_cmd = _fatp_cmd_uart_sc_poweron, .parms_num = 0,
        .help = "Power On Smard Card\n"
                "Usage:\n"
                "  uart_sc_poweron\n"
    },
    { 
        .name = "uart_sc_rd", .f_cmd = _fatp_cmd_uart_sc_rd, .parms_num = 2,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "offset", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" }, 
            { .name = "count", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" }, 
        }, 
        .help = "Test uart read access to smart card\n"
                "Usage:\n"
                "  uart_sc_rd <offset> <count>\n"
    },
    { 
        .name = "uart_sc_apdu", .f_cmd = _fatp_cmd_uart_sc_apdu, .parms_num = 1,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "apdu", .type = eDEVMAN_ATTR_BUFF, .size = 128 }, 
        }, 
        .help = "Test uart write access to smart card\n"
                "Usage:\n"
                "  uart_sc_wr <apdu>\n"
    },
    { 
        .name = "uart_sc_reset", .f_cmd = _fatp_cmd_uart_sc_reset, .parms_num = 0,
        .help = "Reset Smard Card\n"
                "Usage:\n"
                "  uart_sc_reset\n"
    },
    { 
        .name = "xadc_temperature", .f_cmd = _fatp_cmd_xadc_temperature, .parms_num = 0,
        .help = "Read XADC temperature\n"
                "Usage:\n"
                "  xadc_temperature\n"
    },
    { 
        .name = "xadc_voltage", .f_cmd = _fatp_cmd_xadc_voltage, .parms_num = 0,
        .help = "Read XADC voltage\n"
                "Usage:\n"
                "  xadc_voltage\n"
    },
    { 
        .name = "sysmon_temperature", .f_cmd = _fatp_cmd_sysmon_temperature, .parms_num = 0,
        .help = "Read SysMon temperature\n"
                "Usage:\n"
                "  sysmon_temperature\n"
    },
    { 
        .name = "sysmon_voltage", .f_cmd = _fatp_cmd_sysmon_voltage, .parms_num = 0,
        .help = "Read SysMon voltage\n"
                "Usage:\n"
                "  sysmon_voltage\n"
    },
    { 
        .name = "hses10g_init", .f_cmd = _fatp_cmd_hses10g_init, .parms_num = 2,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "side", .type = eDEVMAN_ATTR_STRING, .size = DEVMAN_CMD_PARM_LEN, .format = "%s" },
            { .name = "port", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
        }, 
        .help = "Virtex 10G/25G High Speed Ethernet Subsystem (HSES10G) IP Core init (default configuration).\n"
                "Usage:\n"
                "  hses10g_init <side> <port>\n"
                "  side can be one of the following:\n"
                "    \"client\"\n"
                "    \"network\"\n"
                "    \"internal\"\n"
                "  port can be one of the following:\n"
                "  In case of \"client\"/\"network\"\n" 
                "    0-11 - Front Panel\n"
                "  In case of \"internal\"\n"
                "    0-3 - Xeon1 10BaseKR\n"
                "    4-7 - Xeon2 10BaseKR\n"
                "    8-11 - Xeon3 10BaseKR\n"
                "    12-15 - Xeon4 10BaseKR\n"
    },
    { 
        .name = "hses10g_link", .f_cmd = _fatp_cmd_hses10g_link, .parms_num = 2,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "side", .type = eDEVMAN_ATTR_STRING, .size = DEVMAN_CMD_PARM_LEN, .format = "%s" },
            { .name = "port", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
        }, 
        .help = "Read link status for specific port.\n"
                "Usage:\n"
                "  hses10g_link <side> <port>\n"
                "  side can be one of the following:\n"
                "    \"client\"\n"
                "    \"network\"\n"
                "    \"internal\"\n"
                "  port can be one of the following:\n"
                "  In case of \"client\"/\"network\"\n"
                "    0-11 - Front Panel\n"
                "  In case of \"internal\"\n"
                "    0-3 - Xeon1 10BaseKR\n"
                "    4-7 - Xeon2 10BaseKR\n"
                "    8-11 - Xeon3 10BaseKR\n"
                "    12-15 - Xeon4 10BaseKR\n"
    },
    { 
        .name = "hses10g_traffic", .f_cmd = _fatp_cmd_hses10g_traffic, .parms_num = 3,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "side", .type = eDEVMAN_ATTR_STRING, .size = DEVMAN_CMD_PARM_LEN, .format = "%s" },
            { .name = "port", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
            { .name = "mode", .type = eDEVMAN_ATTR_STRING, .size = DEVMAN_CMD_PARM_LEN, .format = "%s" },
        }, 
        .help = "Verify traffic for specific port in specific test mode.\n"
                "Usage:\n"
                "  hses10g_traffic <side> <port> <mode>\n"
                "  side can be one of the following:\n"
                "    \"client\"\n"
                "    \"network\"\n"
                "    \"internal\"\n"
                "  port can be one of the following:\n"
                "  In case of \"client\"/\"network\"\n" 
                "    0-11 - Front Panel\n"
                "  In case of \"internal\"\n"
                "    0-3 - Xeon1 10BaseKR\n"
                "    4-7 - Xeon2 10BaseKR\n"
                "    8-11 - Xeon3 10BaseKR\n"
                "    12-15 - Xeon4 10BaseKR\n"
                "  test mode can be one of the following:\n"
                "    \"loopback\"\n"
                "    \"traffic_gen\"\n"
    },
    { 
        .name = "usplus100g_init", .f_cmd = _fatp_cmd_usplus100g_init, .parms_num = 2,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "side", .type = eDEVMAN_ATTR_STRING, .size = DEVMAN_CMD_PARM_LEN, .format = "%s" },
            { .name = "port", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
        }, 
        .help = "Virtex 10G/25G High Speed Ethernet Subsystem (HSES10G) IP Core init (default configuration).\n"
                "Usage:\n"
                "  usplus100g_init <side> <port>\n"
                "  side can be one of the following:\n"
                "    \"client\"\n"
                "    \"network\"\n"
                "  port can be one of the following:\n"
                "    0-1 - Front Panel\n"
    },
    { 
        .name = "usplus100g_link", .f_cmd = _fatp_cmd_usplus100g_link, .parms_num = 2,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "side", .type = eDEVMAN_ATTR_STRING, .size = DEVMAN_CMD_PARM_LEN, .format = "%s" },
            { .name = "port", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
        }, 
        .help = "Read link status for specific port.\n"
                "Usage:\n"
                "  usplus100g_link <side> <port>\n"
                "  side can be one of the following:\n"
                "    \"client\"\n"
                "    \"network\"\n"
                "  port can be one of the following:\n"
                "    0-1 - Front Panel\n"
    },
    { 
        .name = "usplus100g_traffic", .f_cmd = _fatp_cmd_usplus100g_traffic, .parms_num = 3,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "side", .type = eDEVMAN_ATTR_STRING, .size = DEVMAN_CMD_PARM_LEN, .format = "%s" },
            { .name = "port", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
            { .name = "mode", .type = eDEVMAN_ATTR_STRING, .size = DEVMAN_CMD_PARM_LEN, .format = "%s" },
        }, 
        .help = "Verify traffic for specific port in specific test mode.\n"
                "Usage:\n"
                "  usplus100g_traffic <side> <port> <mode>\n"
                "  side can be one of the following:\n"
                "    \"client\"\n"
                "    \"network\"\n"
                "  port can be one of the following:\n"
                "    0-1 - Front Panel\n"
                "  test mode can be one of the following:\n"
                "    \"loopback\"\n"
                "    \"traffic_gen\"\n"
    },  
    { 
        .name = "eepromrd", .f_cmd = _fatp_cmd_i2ccore_eepromrd, .parms_num = 3,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "id", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
            { .name = "offset", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" }, 
            { .name = "count", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%d" }, 
        }, 
        .help = "Test i2c read access to board's eeproms.\n"
                "Usage:\n"
                "  eepromrd <id> <offset> <count>\n"
                "  eeprom id can be one of the following:\n"
                "    1  - Main Board eeprom\n"
                "    2  - COMX0 eeprom\n"
                "    3  - COMX1 eeprom\n"
                "    4  - COMX2 eeprom\n"
                "    5  - COMX3 eeprom\n"
                "    6  - ATP eeprom 1\n"
                "    7  - ATP eeprom 2\n"
                "    8  - SODIMM eeprom 1\n"
                "    9  - SODIMM eeprom 2\n"
                "    10 - SODIMM eeprom 3\n"
    },
    { 
        .name = "eepromwr", .f_cmd = _fatp_cmd_i2ccore_eepromwr, .parms_num = 3,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "id", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
            { .name = "offset", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" }, 
            { .name = "value", .type = eDEVMAN_ATTR_BUFF, .size = 128 }, 
        }, 
        .help = "Test i2c write access to board's eeproms.\n"
                "Usage:\n"
                "    1  - Main Board eeprom\n"
                "    2  - COMX0 eeprom\n"
                "    3  - COMX1 eeprom\n"
                "    4  - COMX2 eeprom\n"
                "    5  - COMX3 eeprom\n"
                "    6  - ATP eeprom 1\n"
                "    7  - ATP eeprom 2\n"
                "    8  - SODIMM eeprom 1\n"
                "    9  - SODIMM eeprom 2\n"
                "    10 - SODIMM eeprom 3\n"
    },
    { 
        .name = "rtcrd", .f_cmd = _fatp_cmd_i2ccore_rtcrd, .parms_num = 3,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "id", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
            { .name = "offset", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" }, 
            { .name = "count", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%d" }, 
        }, 
        .help = "Test i2c read access to board's RTCs.\n"
                "Usage:\n"
                "  rtcrd <id> <offset> <count>\n"
                "  RTC id can be one of the following:\n"
                "    1 - Main Board RTC\n"
                "    2 - ATP RTC 1\n"
                "    3 - ATP RTC 2\n"
    },
    { 
        .name = "rtcwr", .f_cmd = _fatp_cmd_i2ccore_rtcwr, .parms_num = 4,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "id", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
            { .name = "offset", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" }, 
            { .name = "value", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" }, 
            { .name = "count", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%d" }, 
        }, 
        .help = "Test i2c write access to board's RTCs.\n"
                "Usage:\n"
                "  rtcwr <id> <offset> <value> <count>\n"
                "  RTC id can be one of the following:\n"
                "    1 - Main Board RTC\n"
                "    2 - ATP RTC 1\n"
                "    3 - ATP RTC 2\n"
    },
    { 
        .name = "trans_data", .f_cmd = _fatp_cmd_trans_data, .parms_num = 2,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "id", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
            { .name = "mmap", .type = eDEVMAN_ATTR_STRING, .size = DEVMAN_CMD_PARM_LEN, .format = "%s" },
        }, 
        .help = "Get transceiver raw data from msa/dd memory map.\n"
    },
    { 
        .name = "stat_period", .f_cmd = _fatp_cmd_stat_period, .parms_num = 1,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "id", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%d" },
        }, 
        .help = "Set statistics time period.\n"
    },
    { 
        .name = "es_config", .f_cmd = _fatp_cmd_es_config, .parms_num = 5,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "enable", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
            { .name = "index", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%d" },
            { .name = "type", .type = eDEVMAN_ATTR_STRING, .size = DEVMAN_CMD_PARM_LEN, .format = "%s" },
            { .name = "hostname", .type = eDEVMAN_ATTR_STRING, .size = DEVMAN_CMD_PARM_LEN, .format = "%s" },
            { .name = "port", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%d" },
        }, 
        .help = "ElasticSearch configuration.\n"
    },
    { 
        .name = "bist", .f_cmd = _fatp_cmd_bist, .parms_num = 0,
        .help = "Run BiST on the board \n"
    },
    { 
        .name = "program", .f_cmd = _fpga_cmd_program, .parms_num = 2,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "device", .type = eDEVMAN_ATTR_STRING, .size = DEVMAN_CMD_PARM_LEN, .format = "%s" },
            { .name = "file_pathname", .type = eDEVMAN_ATTR_STRING, .size = ARTIX_IMAGE_FILENAME_MAXLEN, .format = "%s" }, 
        }, 
        .help = "Program Artix FPGA \n" 
    },
    { 
        .name = "load_test", .f_cmd = _fatp_cmd_load_test, .parms_num = 1,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "pattern", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" }, 
        }, 
        .help = "run virtex load test with specific test pattern.\n"
                "Usage:\n"
                "load_test <pattern>\n"
    },
    { 
        .name = "bootv", .f_cmd = _fatp_cmd_boot_virtex, .parms_num = 0,
        .help = "Boot Virtex.\n"
    },
    { 
        .name = "board_id", .f_cmd = _fatp_cmd_get_board_id, .parms_num = 0,
        .help = "Get board ID.\n"
    },
    { 
        .name = "version", .f_cmd = _fatp_cmd_version_time, .parms_num = 0,
        .help = "Get CPU/Artix/Virtex version build time\n" 
    },
    { 
        .name = "init_logger", .f_cmd = _fatp_cmd_init_logger, .parms_num = 3,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "path", .type = eDEVMAN_ATTR_STRING, .size = DEVMAN_CMD_PARM_LEN, .format = "%s" },
            { .name = "log_level", .type = eDEVMAN_ATTR_STRING, .size = DEVMAN_CMD_PARM_LEN, .format = "%s" },
            { .name = "syslog_level", .type = eDEVMAN_ATTR_STRING, .size = DEVMAN_CMD_PARM_LEN, .format = "%s" },
        }, 
        .help = "Init system logger. \n" 
                "Usage:\n"
                "init_logger <log file path>\n"
                             "<log level emgncy|alrt|crit|err|warn|notice|info|debug>\n"
                             "<syslog level emgncy|alrt|crit|err|warn|notice|info|debug>\n"
    },
    { 
        .name = "kr_test", .f_cmd = _fatp_cmd_kr_test, .parms_num = 3,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "cpu", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%d" },
            { .name = "link", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%d" },
            { .name = "interval", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%d" },
        }, 
        .help = "KR link monitor.\n" 
                "Usage:\n"
                "kr_test <cpu 0-3> <link 0-3> <sampling interval msec>"               
    }
};


/** atp attributes definition */
static device_attr_t _attrs[] = {
};

/** device specific initialization */
static ccl_err_t _fatp_add(void *devo, void *brdspec)
{
    atp_ctx_t       *p_atp;
    atp_brdspec_t   *p_brdspec;
    b_u32               i;

    if ( !devo || !brdspec ) {
        PDEBUG("Error! Invalid input parameters\n");
        ATP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_atp = (atp_ctx_t *)devman_device_private(devo);
    p_brdspec = (atp_brdspec_t *)brdspec;

    p_atp->devo = devo;    
    /* save the board specific info */
    memcpy(&p_atp->brdspec, p_brdspec, sizeof(atp_brdspec_t));

    _ret = _fatp_get_board_id(&p_atp->board_id);
    if (_ret) {
        PDEBUG("_fatp_get_board_id error\n");
        ATP_ERROR_RETURN(CCL_FAIL);
    }
    _rc = pthread_mutex_init(&p_atp->mutex, NULL);
    if ( _rc ) {
        PDEBUG("pthread_mutex_init error\n");
        ATP_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}

/** device specific deinitialization */
static ccl_err_t _fatp_cut(void *devo)
{
    atp_ctx_t       *p_atp;

    if ( !devo ) {
        PDEBUG("Error! Invalid input parameters\n");
        ATP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    p_atp = devman_device_private(devo);

    _rc = pthread_mutex_destroy(&p_atp->mutex);
    if ( _rc ) {
        PDEBUG("pthread_mutex_destroy error\n");
        ATP_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}

/** atp device type */
static device_driver_t _atp_driver = {
    .name = "atp",
    .f_add = _fatp_add,
    .f_cut = _fatp_cut,
    .attrs_num = sizeof(_attrs)/sizeof(_attrs[0]),                 
    .attrs = _attrs,
    .cmds_num = sizeof(_cmds)/sizeof(_cmds[0]), 
    .cmds = _cmds,
    .priv_size = sizeof(atp_ctx_t)
}; 

/** 
 *  Initialize ATP device type
 */

ccl_err_t devatp_init(void)
{
    /* register device type */
    _ret = devman_driver_register(&_atp_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to register driver\n");
        ATP_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

/** 
 *  Deinitialize ATP device type
 */
ccl_err_t devatp_fini(void)
{
    /* unregister device type */
    _ret = devman_driver_unregister(&_atp_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to unregister driver\n");
        ATP_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

