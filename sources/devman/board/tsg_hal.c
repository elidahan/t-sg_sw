/********************************************************************************/
/**
 * @file tsg_hal.c
 * @brief Test module for HAL layer 
 * @copyright 2020 CELARE. All rights reserved.
 */
/********************************************************************************/
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include <linux/limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include <fcntl.h> 
#include <termios.h>
#include <unistd.h>
#include <math.h>

#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "devman_brd_media.h"
#include "devman_brd_port.h"
#include "tsg.h"

extern const char *media_mapping_type_to_str(b_u32 type, 
                                             devman_media_mapping_entry_str_t mapping_array[]);
extern ccl_logger_t logger;

#define HAL_DEBUG
/* printing/error-returning macros */
#ifdef HAL_DEBUG
    #define PDEBUG(fmt, args...) \
         ccl_syslog_err("HAL: %s[%d]: " fmt, __FUNCTION__, __LINE__, ## args)
#else
    #define PDEBUG(fmt, args...)
#endif
#define HAL_ERROR_RETURN(err) do { \
   ccl_syslog_err("Error! %s[%d], error=%i\n", __FUNCTION__, __LINE__, (err)); \
   return err; \
} while (0)

#ifdef HAL_APPLY_MUTEX
#define HAL_MUTEX_LOCK(mutex) do {\
    _rc = pthread_mutex_lock(&mutex); \
    if (_rc) { \
        PDEBUG("pthread_mutex_lock error\n"); \
        HAL_ERROR_RETURN(CCL_FAIL); \
    } \
} while (0)

#define HAL_MUTEX_UNLOCK(mutex) do {\
    _rc = pthread_mutex_unlock(&mutex); \
    if (_rc) { \
        PDEBUG("pthread_mutex_unlock error\n"); \
        HAL_ERROR_RETURN(CCL_FAIL); \
    } \
} while (0)
#else 
#define HAL_MUTEX_LOCK(mutex)
#define HAL_MUTEX_UNLOCK(mutex)
#endif 

/** @struct hal_ctx_t
 *  hal device internal control structure
 */
typedef struct {
    void                     *devo;
    hal_brdspec_t            brdspec;
    pthread_mutex_t          mutex;
} hal_ctx_t;

static ccl_err_t _ret;
static int       _rc;

static devman_if_counter_t _hal_if_counter_str_to_enum(char *counter_str)
{
    static const char *cnt_names[] = DEVMAN_IF_COUNTERS_STR;
    b_i32 i;

    for (i = 0; i < DEVMAN_IF_MIB_MAX; i++) {
        PDEBUG("counter_str:%s cnt_names[%d]: %s\n", 
               counter_str, i, cnt_names[i]);
        if (!strncmp(counter_str, cnt_names[i], DEVMAN_DEVNAME_LEN)) 
            return i;
    }
    return DEVMAN_IF_MIB_MAX;
}

static devman_if_property_t _hal_if_property_str_to_enum(char *property_str)
{
    static const char *prop_names[] = DEVMAN_IF_PROPERTIES_STR;
    b_u32 i;


    for (i = 0; i < DEVMAN_PROP_MAX; i++) {
        if (!strncmp(property_str, prop_names[i], DEVMAN_DEVNAME_LEN)) 
            return i;
    }
    return DEVMAN_PROP_MAX;
}

static ccl_err_t _fhal_cmd_if_counter_get(devo_t atpdevo, 
                                          device_cmd_parm_t parms[], 
                                          b_u16 parms_num)
{
    b_u32                port_id;
    char                 *counter_str;
    devman_if_counter_t  counter_type;
    b_u64                val = 0;

    port_id = (b_u32)parms[0].value; 
    counter_str = (b_u32)parms[1].value;

    if ((counter_type = _hal_if_counter_str_to_enum(counter_str)) == DEVMAN_IF_MIB_MAX) {
        PDEBUG("wrong counter: %s\n", counter_str);
        HAL_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    _ret = hal_brd_port_counter_get(port_id, counter_type, &val);
    if (_ret) {
        PDEBUG("Error! hal_brd_port_counter_get() failed; port: %d, counter: %s (%d)\n", 
               port_id, counter_str, counter_type);
        HAL_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    PRINTL("port: %d counter %s=%llu\n", port_id, counter_str, val);

    return CCL_OK;
}

static ccl_err_t _hal_if_property_media_params_print(b_u32 port_id, b_u8 *pdata)
{
    devman_media_param_t *pmedia_param;

    if ( !pdata ) {
        PDEBUG("Error! Invalid input parameters\n");
        HAL_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    pmedia_param = (devman_media_param_t *)pdata;

    PRINTL("   Port = %d\n\r", port_id);
    PRINTL("   Media Port Type = %d %s \n\r", 
           pmedia_param->media_type, 
           media_mapping_type_to_str(pmedia_param->media_type, 
                                     media_type_str));
    PRINTL("   Link Type  = %d %s \n\r",      
           pmedia_param->link_type,  
           media_mapping_type_to_str(pmedia_param->link_type,
                                     media_link_type_str));

    PRINTL("   Transiever Type = %d\n\r", 
           pmedia_param->description.transceiver_type);
    PRINTL("   Extended   Type  = %d\n\r", 
           pmedia_param->description.extended_type);
    PRINTL("   Connector  Type  = %d\n\r", 
           pmedia_param->description.connector_type);

    PRINTL("   Transceiver parameters:\n\r");
    PRINTL("       10G Compliance                = %d %s \n\r", 
           pmedia_param->description.transceiver.g10_compliance       ,
           media_mapping_type_to_str(pmedia_param->description.transceiver.g10_compliance,
                                     media_trnsvr_10g_compliance_type_str));
    PRINTL("       Infiniband Compliance         = %d %s \n\r", 
           pmedia_param->description.transceiver.infiniband_compliance, 
           media_mapping_type_to_str(pmedia_param->description.transceiver.infiniband_compliance,
                                     media_trnsvr_infiniband_compliance_type_str));
    PRINTL("       Ecson Compliance              = %d %s \n\r", 
           pmedia_param->description.transceiver.escon_compliance, 
           media_mapping_type_to_str(pmedia_param->description.transceiver.escon_compliance,
                                     media_trnsvr_escon_compliance_type_str));
    PRINTL("       Sonet Compliance              = %d %s \n\r", 
           pmedia_param->description.transceiver.sonet_compliance, 
           media_mapping_type_to_str(pmedia_param->description.transceiver.sonet_compliance,
                                     media_trnsvr_sonet_compliance_type_str));
    PRINTL("       Ethernet Compliance           = %d %s \n\r", 
           pmedia_param->description.transceiver.eth_compliance, 
           media_mapping_type_to_str(pmedia_param->description.transceiver.eth_compliance,
                                     media_trnsvr_eth_compliance_type_str));
    PRINTL("       Fiber Channel Length          = %d %s \n\r", 
           pmedia_param->description.transceiver.fbr_chnl_len, 
           media_mapping_type_to_str(pmedia_param->description.transceiver.fbr_chnl_len,
                                     media_trnsvr_fbr_chnl_length_type_str));
    PRINTL("       Fiber Channel Technology      = %d %s \n\r", 
           pmedia_param->description.transceiver.fbr_chnl_technology, 
           media_mapping_type_to_str(pmedia_param->description.transceiver.fbr_chnl_technology,
                                     media_trnsvr_fbr_chnl_technology_type_str));
    PRINTL("       Fiber Channel Media           = %d %s \n\r", 
           pmedia_param->description.transceiver.fbr_chnl_media, 
           media_mapping_type_to_str(pmedia_param->description.transceiver.fbr_chnl_media,
                                     media_trnsvr_fbr_chnl_media_type_str));
    PRINTL("       Fiber Channel Speed           = %d %s \n\r", 
           pmedia_param->description.transceiver.fbr_chnl_speed, 
           media_mapping_type_to_str(pmedia_param->description.transceiver.fbr_chnl_speed,
                                     media_trnsvr_fbr_chnl_speed_type_str));

    PRINTL("   Encoding = %d\n\r", 
           pmedia_param->description.encoding);
    PRINTL("   Bitrate nominal = %d\n\r", 
           pmedia_param->description.bit_rate_nom);
    PRINTL("   Bitrate maximal = %d\n\r", 
           pmedia_param->description.bit_rate_max);
    PRINTL("   Bitrate minimal = %d\n\r", 
           pmedia_param->description.bit_rate_min);
    PRINTL("   Rate ID = %d\n\r", 
           pmedia_param->description.rate_id);
    PRINTL("   Length SM fiber = %d\n\r", 
           pmedia_param->description.length_smf);
    PRINTL("   Length 50um = %d\n\r", 
           pmedia_param->description.length50um);
    PRINTL("   Length 625um = %d\n\r", 
           pmedia_param->description.length62_5um);
    PRINTL("   Length copper = %d\n\r", 
           pmedia_param->description.length_copper);
    PRINTL("   Length OM3 = %d\n\r", 
           pmedia_param->description.length_om3);
    PRINTL("   Vendor name = \"%s\"\n\r", 
           pmedia_param->description.vendor_name);
    PRINTL("   Vendor OUI = \"%s\"\n\r", 
           pmedia_param->description.vendor_oui);
    PRINTL("   Vendor PN = \"%s\"\n\r", 
           pmedia_param->description.vendor_pn);
    PRINTL("   Vendor revision = \"%s\"\n\r", 
           pmedia_param->description.vendor_rev);
    PRINTL("   Vendor SN = \"%s\"\n\r", 
           pmedia_param->description.vendor_sn);
    PRINTL("   Manufacturing date = %d/%d/%d lot %d\n\r", 
           pmedia_param->description.mfg_date.year, 
           pmedia_param->description.mfg_date.month, 
           pmedia_param->description.mfg_date.day, 
           pmedia_param->description.mfg_date.lot);
    PRINTL("   diagnostic parameters:\n\r");
    PRINTL("       DDM supported            = %d (relevance %d)\n\r", 
           pmedia_param->description.diagnostic.ddm_supported.status, 
           pmedia_param->description.diagnostic.ddm_supported.relevance);
    PRINTL("       Internal calibration     = %d (relevance %d)\n\r", 
           pmedia_param->description.diagnostic.intern_calibr.status, 
           pmedia_param->description.diagnostic.intern_calibr.relevance);
    PRINTL("       External calibration     = %d (relevance %d)\n\r", 
           pmedia_param->description.diagnostic.extern_calibr.status, 
           pmedia_param->description.diagnostic.extern_calibr.relevance);
    PRINTL("       Average Power Masurement = %d (relevance %d)\n\r", 
           pmedia_param->description.diagnostic.avg_power_measure.status, 
           pmedia_param->description.diagnostic.avg_power_measure.relevance);
    PRINTL("       Address change required  = %d (relevance %d)\n\r", 
           pmedia_param->description.diagnostic.adr_chng_required.status, 
           pmedia_param->description.diagnostic.adr_chng_required.relevance);

    return CCL_OK;
}

static ccl_err_t _hal_if_property_media_details_print(b_u32 port_id, b_u8 *pdata)
{
    return CCL_OK;
}

static ccl_err_t _hal_if_property_ddm_params_print(b_u32 port_id, b_u8 *pdata)
{
    devman_media_diag_param_t        *pddm_param;
    devman_media_diag_tresh_unit_t   *pthresh_unit;
    devman_media_diag_calib_item_t   *pcalib_unit;
    double                           *pfloat_unit;

    if ( !pdata ) {
        PDEBUG("Error! Invalid input parameters\n");
        HAL_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    pddm_param = (devman_media_diag_param_t *)pdata;


    PRINTL("   Port = %d\n\r", port_id);
    PRINTL("\n\rTresholds:\tHiAlarm\tLoAlarm\tHiWarn\tLoWarn\n\r");
    pthresh_unit = &(pddm_param->threshold.temperature);
    PRINTL("  temper.-\t%d.%d\t%d.%d\t%d.%d\t%d.%d\n\r", 
           pthresh_unit->high_alarm.modulo, 
           pthresh_unit->high_alarm.fraction,
           pthresh_unit->low_alarm.modulo, 
           pthresh_unit->low_alarm.fraction,
           pthresh_unit->high_warn.modulo, 
           pthresh_unit->high_warn.fraction,
           pthresh_unit->low_warn.modulo, 
           pthresh_unit->low_warn.fraction);

    pthresh_unit = &(pddm_param->threshold.tx_power);
    PRINTL("  Tx.Pwr.-\t%d.%d\t%d.%d\t%d.%d\t%d.%d\n\r", 
           pthresh_unit->high_alarm.modulo, 
           pthresh_unit->high_alarm.fraction,
           pthresh_unit->low_alarm.modulo, 
           pthresh_unit->low_alarm.fraction,
           pthresh_unit->high_warn.modulo, 
           pthresh_unit->high_warn.fraction,
           pthresh_unit->low_warn.modulo, 
           pthresh_unit->low_warn.fraction);
    pthresh_unit = &(pddm_param->threshold.rx_power);
    PRINTL("  Rx.Pwr.-\t%d.%d\t%d.%d\t%d.%d\t%d.%d\n\r", 
           pthresh_unit->high_alarm.modulo, 
           pthresh_unit->high_alarm.fraction,
           pthresh_unit->low_alarm.modulo, 
           pthresh_unit->low_alarm.fraction,
           pthresh_unit->high_warn.modulo, 
           pthresh_unit->high_warn.fraction,
           pthresh_unit->low_warn.modulo, 
           pthresh_unit->low_warn.fraction);

    PRINTL("\n\rCalibration:\tSlope\tOffset\n\r");
    pcalib_unit = &(pddm_param->calibration.t);
    PRINTL("  temper.-\t%d.%d\t%d.%d\n\r", 
           pcalib_unit->slope.modulo, 
           pcalib_unit->slope.fraction,
           pcalib_unit->offset.modulo, 
           pcalib_unit->offset.fraction);

    pcalib_unit = &(pddm_param->calibration.v);
    PRINTL("  voltage-\t%d.%d\t%d.%d\n\r", 
           pcalib_unit->slope.modulo, 
           pcalib_unit->slope.fraction,
           pcalib_unit->offset.modulo, 
           pcalib_unit->offset.fraction);

    pcalib_unit = &(pddm_param->calibration.txi);
    PRINTL("  TxI    -\t%d.%d\t%d.%d\n\r", 
           pcalib_unit->slope.modulo, 
           pcalib_unit->slope.fraction,
           pcalib_unit->offset.modulo, 
           pcalib_unit->offset.fraction);

    pcalib_unit = &(pddm_param->calibration.tx_power);
    PRINTL("  TxPwr  -\t%d.%d\t%d.%d\n\r", 
           pcalib_unit->slope.modulo, 
           pcalib_unit->slope.fraction,
           pcalib_unit->offset.modulo, 
           pcalib_unit->offset.fraction);

    pfloat_unit = pddm_param->calibration.rx_power;
    PRINTL("  RxPwr  -\t%f\t%f\t%f\t%f\t%f\n\r", 
           pfloat_unit[0], 
           pfloat_unit[1], 
           pfloat_unit[2], 
           pfloat_unit[3], 
           pfloat_unit[4]);

    return CCL_OK;
}

static ccl_err_t _hal_if_property_ddm_status_print(b_u32 port_id, b_u8 *pdata)
{
    devman_media_diag_status_t       *pddm_status;
    devman_media_diag_tresh_value_t  *pthresh_val;

    if ( !pdata ) {
        PDEBUG("Error! Invalid input parameters\n");
        HAL_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    pddm_status = (devman_media_diag_status_t *)pdata;

    PRINTL("\n\rMeasurements:\n\r");

    pthresh_val = &(pddm_status->temperature);
    PRINTL("  temper.-\t%d.%d\n\r", 
           pthresh_val->modulo, pthresh_val->fraction);

    pthresh_val = &(pddm_status->vcc);
    PRINTL("  Vcc    -\t%d.%d\n\r", 
           pthresh_val->modulo, pthresh_val->fraction);

    pthresh_val = &(pddm_status->tx_bias);
    PRINTL("  TxBias -\t%d.%d\n\r", 
           pthresh_val->modulo, pthresh_val->fraction);

    pthresh_val = &(pddm_status->tx_power);
    PRINTL("  TxPower-\t%d.%d\n\r", 
           pthresh_val->modulo, pthresh_val->fraction);

    pthresh_val = &(pddm_status->rx_power);
    PRINTL("  RxPower-\t%d.%d\n\r", 
           pthresh_val->modulo, pthresh_val->fraction);

    PRINTL("  TxDisable   -\t%d\n\r", pddm_status->tx_disable);
    PRINTL("  SoftTxDsbl  -\t%d\n\r", pddm_status->soft_tx_disable);
    PRINTL("  RateSelect  -\t%d\n\r", pddm_status->rate_select);
    PRINTL("  SoftRateSlct-\t%d\n\r", pddm_status->soft_rate_select);
    PRINTL("  TxFault     -\t%d\n\r", pddm_status->tx_fault);
    PRINTL("  LOS         -\t%d\n\r", pddm_status->los);
    PRINTL("  DataReadyBar-\t%d\n\r", pddm_status->data_ready_bar);

    return CCL_OK;
}

static ccl_err_t _fhal_cmd_if_property_get(devo_t atpdevo, 
                                           device_cmd_parm_t parms[], 
                                           b_u16 parms_num)
{
    b_u32             port_id;
    char              *property_str;
    devman_if_property_t property_id;
    b_u8              data[1024];

    port_id = (b_u32)parms[0].value; 
    property_str = (b_u32)parms[1].value;

    if (property_id = _hal_if_property_str_to_enum(property_str) == DEVMAN_PROP_MAX) {
        PDEBUG("wrong property: %s\n", property_str);
        HAL_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    _ret = hal_brd_port_property_get(port_id, property_id, data, sizeof(data));
    if (_ret) {
        PDEBUG("Error! hal_brd_port_property_get() failed; port: %d, property: %s (%d)\n", 
               port_id, property_str, property_id);
        HAL_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch (property_id) {
    case DEVMAN_IF_MEDIA_PARAMETERS:
        _hal_if_property_media_params_print(port_id, data);
        break;
    case DEVMAN_IF_MEDIA_DETAILS:
        _hal_if_property_media_details_print(port_id, data);
        break;
    case DEVMAN_IF_DDM_PARAMETERS:
        _hal_if_property_ddm_params_print(port_id, data);
        break;
    case DEVMAN_IF_DDM_STATUS:
        _hal_if_property_ddm_status_print(port_id, data);
        break;
    default:
        PDEBUG("wrong property: %s\n", property_str);
        HAL_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    return CCL_OK;
}

static ccl_err_t _fhal_cmd_if_tx_state_set(devo_t atpdevo, 
                                           device_cmd_parm_t parms[], 
                                           b_u16 parms_num)
{
    b_u32             port_id;
    char              *tx_state;
    b_bool            val; 

    port_id = (b_u32)parms[0].value; 
    tx_state = (b_u32)parms[1].value;

    if (!strcmp(tx_state, "enable")) 
        val = CCL_TRUE;
    else if (!strcmp(tx_state, "disable")) 
        val = CCL_FALSE;
    else {
        PDEBUG("wrong port tx state value: %s\n", tx_state);
        HAL_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    _ret = hal_brd_port_tx_state_set(port_id, val);
    if (_ret) {
        PDEBUG("Error! hal_brd_port_tx_state_set() failed; port: %d, val: %d\n", 
               port_id, val);
        HAL_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    return CCL_OK;
}

static ccl_err_t _fhal_cmd_if_tx_state_get(devo_t atpdevo, 
                                           device_cmd_parm_t parms[], 
                                           b_u16 parms_num)
{
    b_u32             port_id;
    b_bool            state;

    port_id = (b_u32)parms[0].value; 

    _ret = hal_brd_port_tx_state_get(port_id, &state);
    if (_ret) {
        PDEBUG("Error! hal_brd_port_tx_state_get() failed; port: %d\n", 
               port_id);
        HAL_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    PRINTL("port: %d tx_state: %s\n", port_id, 
           (state == CCL_TRUE) ? "enable" : "disable");

    return CCL_OK;
}

static ccl_err_t _fhal_cmd_if_led_state_set(devo_t atpdevo, 
                                            device_cmd_parm_t parms[], 
                                            b_u16 parms_num)
{
    b_u32             port_id;
    b_u32             val; 
    b_bool            state;

    port_id = (b_u32)parms[0].value; 
    val = (b_u32)parms[1].value;

    state = !val ? CCL_FALSE : CCL_TRUE; 

    _ret = hal_brd_port_led_state_set(port_id, state);
    if (_ret) {
        PDEBUG("Error! hal_brd_port_led_state_set() failed; port: %d, state: %d\n", 
               port_id, state);
        HAL_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    return CCL_OK;
}

static ccl_err_t _fhal_cmd_if_portnum(devo_t atpdevo, 
                                      device_cmd_parm_t parms[], 
                                      b_u16 parms_num)
{
    b_u32     portnum;

    _ret = hal_brd_port_number_of_ports_get(&portnum);
    if (_ret) {
        PDEBUG("Error! hal_brd_port_number_of_ports_get() failed\n");
        HAL_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    PRINTL("number of ports: %d\n", portnum);

    return CCL_OK;
}

static ccl_err_t _fhal_cmd_if_link_status(devo_t atpdevo, 
                                          device_cmd_parm_t parms[], 
                                          b_u16 parms_num)
{
    b_u32                 port_id;
    devman_if_link_status_t  status;

    port_id = (b_u32)parms[0].value; 

    _ret = hal_brd_port_link_status_get(port_id, &status);
    if (_ret) {
        PDEBUG("Error! hal_brd_port_link_status_get() failed; port: %d\n", 
               port_id);
        HAL_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    PRINTL("port: %d link status: %s\n", port_id, 
          (status == DEVMAN_IF_LINK_UP) ? "up" : "down");

    return CCL_OK;
}

/* device commands */
static device_cmd_t _cmds[] = {
    { 
        .name = "if_counter_get", .f_cmd = _fhal_cmd_if_counter_get, .parms_num = 2,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "port", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%d" },
            { .name = "counter", .type = eDEVMAN_ATTR_STRING, .size = DEVMAN_CMD_PARM_LEN, .format = "%s" },
        }, 
        .help = "Read port counter\n"
                "Usage:\n"
                "port <0-57>\n"
                "counter:\n"
                "rx_bytes\n"            
                "tx_bytes\n"            
                "rx_packets\n"             
                "tx_packets\n"             
                "rx_total_bytes\n"      
                "rx_total_packets\n"       
                "rx_bcast\n"            
                "rx_mcast\n"            
                "crc_errors\n"          
                "oversize\n"            
                "fragments\n"           
                "jabber\n"              
                "collisions\n"          
                "late_collisions\n"     
                "tx_64\n"               
                "tx_65_127\n"           
                "tx_128_255\n"          
                "tx_256_511\n"          
                "tx_512_1023\n"         
                "tx_1024_1522\n"        
                "rx_macerrors\n"        
                "dropped\n"             
                "rx_ucast\n"            
                "rx_noucast\n"          
                "rx_errors\n"           
                "tx_errors\n"           
                "tx_noucast\n"          
                "tx_ucast\n"            
                "tx_mcast\n"            
                "tx_bcast\n"            
                "undersize\n"           
                "rx_unknown_protos\n"   
                "align_errors\n"        
                "fcs_errors\n"          
                "sqetest_errors\n"      
                "cse_errors\n"          
                "symbol_errors\n"       
                "mac_txerrors\n"        
                "mac_rxerrors\n"        
                "too_long_frame\n"      
                "single_collisions\n"   
                "multi_collisions\n"    
                "late_collisions\n"     
                "excess_collisions\n"   
                "rx_unknown_opcode\n"   
                "deferred_tx\n"         
                "rx_pause\n"            
                "tx_pause\n"            
                "rx_oversize\n"         
                "tx_oversize\n"         
                "rx_fragments\n"        
                "tx_fragments\n"        
                "rx_jabbers\n"          
                "rx_jabbers\n"          
    },
    { 
        .name = "if_property_get", .f_cmd = _fhal_cmd_if_property_get, .parms_num = 2,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "port", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%d" },
            { .name = "property", .type = eDEVMAN_ATTR_STRING, .size = DEVMAN_CMD_PARM_LEN, .format = "%s" },
        }, 
        .help = "Get port property\n"
                "Usage:\n"
                "port <0-57>\n"
                "property:\n"
                "if_name\n"
                "if_mtu\n"
                "if_speed_max\n"
                "if_speed\n"
                "if_duplex\n"
                "if_autoneg\n"
                "if_encap\n"
                "if_medium\n"
                "if_interface_type\n"
                "if_mdix\n"
                "if_mdix_status\n"
                "if_learn\n"
                "if_ifilter\n"
                "if_self_efilter\n"
                "if_lock\n"
                "if_drop_on_lock\n"
                "if_fwd_unk\n"
                "if_bcast_limit\n"
                "if_ability\n"
                "if_flow_control\n"
                "if_enable\n"
                "if_sfp_present\n"
                "if_sfp_tx_state\n"
                "if_sfp_rx_state\n"
                "if_default_vlan\n"
                "if_priority\n"
                "if_led_state\n"
                "if_media_parameters\n"
                "if_media_details\n"
                "if_local_advert\n"
                "if_media_type\n"
                "if_sfp_port_type\n"
                "if_media_config_type\n"
                "if_media_error_config_type\n"
                "if_media_sgmii_fiber\n"
                "if_tx_enable\n"
                "if_sfp_port_numbers\n"
                "if_autoneg_duplex\n"
                "if_autoneg_speed\n"
                "if_ddm_parameters\n"
                "if_ddm_status\n"
                "if_ddm_supported\n"
                "if_hwaddress_get\n"
                "if_active_link\n" 
    },
    { 
        .name = "if_tx_state_set", .f_cmd = _fhal_cmd_if_tx_state_set, .parms_num = 2,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "port", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%d" },
            { .name = "state", .type = eDEVMAN_ATTR_STRING, .size = DEVMAN_CMD_PARM_LEN, .format = "%s" },
        }, 
        .help = "Set port tx state\n"
                "Usage:\n"
                "port <0-57>\n"
                "state <enable|disable>\n"
    },
    { 
        .name = "if_tx_state_get", .f_cmd = _fhal_cmd_if_tx_state_get, .parms_num = 1,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "port", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%d" },
        }, 
        .help = "Get port tx state\n"
                "Usage:\n"
                "port <0-57>\n"
    },
    { 
        .name = "if_led_state", .f_cmd = _fhal_cmd_if_led_state_set, .parms_num = 2,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "port", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%d" },
            { .name = "state", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
        }, 
        .help = "Set/Unset port led\n"
                "Usage:\n"
                "port <0-57>\n"
                "state <0|1>\n"
    },
    { 
        .name = "if_portnum", .f_cmd = _fhal_cmd_if_portnum, .parms_num = 0,
        .help = "Get number of ports\n"
    },
    { 
        .name = "if_link_status", .f_cmd = _fhal_cmd_if_link_status, .parms_num = 1,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "port", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%d" },
        }, 
        .help = "Get port link status\n"
                "Usage:\n"
                "port <0-57>\n"
    },
};


/** hal attributes definition */
static device_attr_t _attrs[] = {
};

/** device specific initialization */
static ccl_err_t _fhal_add(void *devo, void *brdspec)
{
    hal_ctx_t       *p_hal;
    hal_brdspec_t   *p_brdspec;
    b_u32               i;

    if ( !devo || !brdspec ) {
        PDEBUG("Error! Invalid input parameters\n");
        HAL_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_hal = (hal_ctx_t *)devman_device_private(devo);
    p_brdspec = (hal_brdspec_t *)brdspec;

    p_hal->devo = devo;    
    /* save the board specific info */
    memcpy(&p_hal->brdspec, p_brdspec, sizeof(hal_brdspec_t));

    _rc = pthread_mutex_init(&p_hal->mutex, NULL);
    if ( _rc ) {
        PDEBUG("pthread_mutex_init error\n");
        HAL_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}

/** device specific deinitialization */
static ccl_err_t _fhal_cut(void *devo)
{
    hal_ctx_t       *p_hal;

    if ( !devo ) {
        PDEBUG("Error! Invalid input parameters\n");
        HAL_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    p_hal = devman_device_private(devo);

    _rc = pthread_mutex_destroy(&p_hal->mutex);
    if ( _rc ) {
        PDEBUG("pthread_mutex_destroy error\n");
        HAL_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}

/** hal device type */
static device_driver_t _hal_driver = {
    .name = "hal",
    .f_add = _fhal_add,
    .f_cut = _fhal_cut,
    .attrs_num = sizeof(_attrs)/sizeof(_attrs[0]),                 
    .attrs = _attrs,
    .cmds_num = sizeof(_cmds)/sizeof(_cmds[0]), 
    .cmds = _cmds,
    .priv_size = sizeof(hal_ctx_t)
}; 

/** 
 *  Initialize HAL device type
 */

ccl_err_t devhal_init(void)
{
    /* register device type */
    _ret = devman_driver_register(&_hal_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to register driver\n");
        HAL_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

/** 
 *  Deinitialize HAL device type
 */
ccl_err_t devhal_fini(void)
{
    /* unregister device type */
    _ret = devman_driver_unregister(&_hal_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to unregister driver\n");
        HAL_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

