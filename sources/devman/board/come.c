/********************************************************************************/
/**
 * @file come.c
 * @brief Implementation of the board specific functionalities
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <semaphore.h>
#include <linux/limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "devman_port.h"
#include "pmbus.h"
#include "come.h"


#define DEVBOARD_DEBUG
/* printing/error-returning macros */
#ifdef DEVBOARD_DEBUG
    #define PDEBUG(fmt, args...) \
         ccl_syslog_err("DEVBOARD: %s[%d]: " fmt, __FUNCTION__, __LINE__, ## args)
#else
    #define PDEBUG(fmt, args...)
#endif
#define DEVBOARD_ERROR_RETURN(err) do { \
   ccl_syslog_err("Error! %s[%d], error=%i\n", __FUNCTION__, __LINE__, (err)); \
   return err; \
} while (0)

static ccl_err_t    _ret;

/* logical port number validation */
#define DEVBRD_IS_LOGPORT_VALID(_lp)  \
        ( (((_lp) >= 0) && ((_lp) < DEVBRD_LOGPORT_MAXNUM))? 1: 0 )

static ccl_err_t _fboard_logport_to_hwsfp(int logport, int *p_hwsfp)
{

    /* check input parameters */
    if ( !DEVBRD_IS_LOGPORT_VALID(logport) || !p_hwsfp ) {
        PDEBUG("Error! Bad input parameters logport=%d, p_hwsfp=0x%x\n", 
               logport, p_hwsfp);
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    *p_hwsfp = _logport_hwspec_sfp_index[logport];      

    return CCL_OK;
} 

static board_device_descr_t _i2cbus_brd_descr[] = {
    {
        .type_name = "i2cbus", .sig = eDEVBRD_I2CBUS_SIG, 
        .spec.i2cbus = {
            .bus_id = eDEVBRD_I2C_BUS_0, 
            .slaves_num = 1,
            .slave_name = {
                "vcco_3.3v"
            },
            .slave_i2ca = {
                eDEVBRD_I2CA_VCCO_3_3V
            }
        }
    },        
};

static board_device_descr_t _pmbus_brd_descr[] = {
    {
        .type_name = "pmbus", .sig = eDEVBRD_PMBUS_0_SIG, 
        .subtype_name = "vcco_3.3v",
        .spec.pmbus = {
            .i2c = {
                .bus_id = eDEVBRD_I2C_BUS_0,
                .dev_i2ca = eDEVBRD_I2CA_VCCO_3_3V
            },
            .cmd[ePMBUS_CMD_OPERATION] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_ON_OFF_CONFIG] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_CLEAR_FAULTS] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_WRITE_PROTECT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STORE_DEFAULT_ALL] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_RESTORE_DEFAULT_ALL] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STORE_DEFAULT_CODE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_RESTORE_DEFAULT_CODE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_MODE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_TRIM] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_MARGIN_HIGH] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_MARGIN_LOW] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_SCALE_LOOP] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VIN_ON] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VIN_OFF] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_IOUT_CAL_GAIN] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_IOUT_CAL_OFFSET] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_OV_FAULT_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_OV_FAULT_RESPONSE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_UV_FAULT_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_VOUT_UV_FAULT_RESPONSE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_IOUT_OC_FAULT_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_IOUT_OC_WARN_LIMIT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_POWER_GOOD_ON] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_POWER_GOOD_OFF] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_TON_RISE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_BYTE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_WORD] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_VOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_IOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_TEMPERATURE] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_STATUS_CML] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_VIN] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_VOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_READ_IOUT] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_PMBUS_REVISION] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_MFR_VIN_MIN] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_MFR_VIN_MAX] = {
                .is_supported = CCL_TRUE
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_00] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_04] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_05] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_06] = {
                .is_supported = CCL_TRUE,
                .size = 2
            },
            .cmd[ePMBUS_CMD_MFR_SPECIFIC_07] = {
                .is_supported = CCL_TRUE,
                .size = 2
            }
        }
    }
};

/* cpu board device description */
static board_device_descr_t _cpu_brd_descr[] = {
    {
        .type_name = "cpu", .sig = eDEVBRD_CPU_SIG, 
        .spec.cpu = {
            .sys_sensors = {
                .voltage_sensors_num = 4,
                .temp_sensors_num = 3,
                .voltage_sensors = {
                    {"in0_voltage", "/sys/class/hwmon/hwmon1/in0_input", 0},
                    {"in1_voltage", "/sys/class/hwmon/hwmon1/in1_input", 0},
                    {"in3_voltage", "/sys/class/hwmon/hwmon1/in3_input", 0},
                    {"in4_voltage", "/sys/class/hwmon/hwmon1/in4_input", 0}
                },
                .temp_sensors = {
                    {"temp1_temperature", "/sys/class/hwmon/hwmon1/temp1_input", 0},
                    {"temp4_temperature", "/sys/class/hwmon/hwmon1/temp4_input", 0},
                    {"temp5_temperature", "/sys/class/hwmon/hwmon1/temp5_input", 0}
                }
            }
        }
    }
};

/* atp module device description */
static board_device_descr_t _atp_brd_descr[] = {
    {
        .type_name = "atp", .sig = eDEVBRD_ATP_SIG, 
        .spec.atp = {
            .board_stats = {
            }
        }
    }
};

/* I2CBUS device board initialization routine
 */
static ccl_err_t _fboard_i2cbus_init(board_device_descr_t *p_ds, int instances_num)
{
    int i;

    /* check input parameters */
    if ( !p_ds ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    extern ccl_err_t devi2cbus_init(void);
    _ret = devi2cbus_init();
    if ( _ret ) {
        PDEBUG("Error! Can't initialize I2CBUS device type\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* create instances for I2CBUS device */
    for ( i=0; i < instances_num; i++ ) {
        PDEBUG("123$=> i=%d, tname=%s, sig=0x%08x\n", 
               i, p_ds->type_name, p_ds->sig);

        _ret = devman_device_create(p_ds->type_name, p_ds->sig, 
                                    (void *)&p_ds->spec.i2cbus, 
                                    &p_ds->devo);
        if ( _ret ) {
            PDEBUG("Error! Can't create I2CBUS instance %d\n", i);
            DEVBOARD_ERROR_RETURN(CCL_FAIL);
        }
        p_ds++;
    }         

    return CCL_OK;
}

static ccl_err_t _fboard_i2c_devices_init(void)
{
    board_device_descr_t *p_ds;
    int i, instances_num;

    /* 1. PMBUS devices initialization*/
    extern ccl_err_t devpmbus_init(void);
    _ret = devpmbus_init();
    if ( _ret ) {
        PDEBUG("Error! Can't initialize PMBUS device type\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_ds = &_pmbus_brd_descr[0];
    instances_num = sizeof(_pmbus_brd_descr)/sizeof(_pmbus_brd_descr[0]);

    /* create instances for PBMUS device */
    for ( i=0; i < instances_num; i++ ) {
        PDEBUG("123$=> i=%d, tname=%s, sig=0x%08x\n", 
               i, p_ds->type_name, p_ds->sig);

        _ret = devman_device_create(p_ds->type_name, p_ds->sig, 
                                    (void *)&p_ds->spec.pmbus, &p_ds->devo);
        if ( _ret ) {
            PDEBUG("Error! Can't create PMBUS %d\n", i);
            DEVBOARD_ERROR_RETURN(CCL_FAIL);
        }
        p_ds++;
    }         

    return CCL_OK;
}

/* ATP module initialization routine 
 */
static ccl_err_t _fboard_atp_init(board_device_descr_t *p_ds, int instances_num) 
{
    int i;

    /* check input parameters */
    if ( !p_ds ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    extern ccl_err_t devatp_init(void);
    _ret = devatp_init();
    if ( _ret ) {
        PDEBUG("Error! Can't initialize atp device type\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* create instances for atp device */
    for ( i=0; i < instances_num; i++ ) {
        PDEBUG("123$=> i=%d, tname=%s, sig=0x%08x\n", 
               i, p_ds->type_name, p_ds->sig);

        _ret = devman_device_create(p_ds->type_name, p_ds->sig, 
                                    (void *)&p_ds->spec.atp, &p_ds->devo);
        if ( _ret ) {
            PDEBUG("Error! Can't create atp instance %d\n", i);
            DEVBOARD_ERROR_RETURN(CCL_FAIL);
        }
        p_ds++;
    }         

    return CCL_OK;
}

/* CPU module initialization routine 
 */
static ccl_err_t _fboard_cpu_init(board_device_descr_t *p_ds, int instances_num) 
{
    int i;

    /* check input parameters */
    if ( !p_ds ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    extern ccl_err_t devcpu_init(void);
    _ret = devcpu_init();
    if ( _ret ) {
        PDEBUG("Error! Can't initialize atp device type\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* create instances for atp device */
    for ( i=0; i < instances_num; i++ ) {
        PDEBUG("123$=> i=%d, tname=%s, sig=0x%08x\n", 
               i, p_ds->type_name, p_ds->sig);

        _ret = devman_device_create(p_ds->type_name, p_ds->sig, 
                                    (void *)&p_ds->spec.cpu, &p_ds->devo);
        if ( _ret ) {
            PDEBUG("Error! Can't create cpu instance %d\n", i);
            DEVBOARD_ERROR_RETURN(CCL_FAIL);
        }
        p_ds++;
    }         

    return CCL_OK;
}

/* Get appropriate device/attr/significant_mask for port "led" operating  
 */
ccl_err_t devboard_portled_to_devattr(__attribute__((unused)) int logport, 
                                      __attribute__((unused)) char dev_name[], 
                                      __attribute__((unused)) char attr_name[], 
                                      __attribute__((unused)) b_u32 *p_sign_mask,
                                      __attribute__((unused)) int *p_is_active_hi)
{
    PDEBUG("Error! There aren't leds on this board\n");
    DEVBOARD_ERROR_RETURN(CCL_NOT_SUPPORT);
}

/* Get appropriate device/attr/significant_mask for port "enable" operating 
 */
ccl_err_t devboard_portena_to_devattr(int logport, char dev_name[], 
                                      char attr_name[], b_u32 *p_sign_mask, 
                                      int *p_is_active_hi)
{
    int     physfp;

    /* check input parameters */
    if ( !p_sign_mask || !p_is_active_hi ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* get physfp index */
    if ( _fboard_logport_to_hwsfp(logport, &physfp) ) {
        PDEBUG("Error! Can't get hwsfp number for the logport=%d\n", logport);
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    *p_is_active_hi = 0;
    /* get target device name/output port attribute name */
    if ( physfp >= 0 && physfp < 12 ) {
        strncpy(dev_name, "axigpio/9", DEVMAN_DEVNAME_LEN);      
        strncpy(attr_name, "data", DEVMAN_DEVNAME_LEN);      
        /* prepare significant attribute bits  */
        *p_sign_mask = 1 << (physfp+16);

    } else if ( physfp >= 14 && physfp < 26 ) {
        strncpy(dev_name, "axigpio/10", DEVMAN_DEVNAME_LEN);      
        strncpy(attr_name, "data", DEVMAN_DEVNAME_LEN);     
        /* prepare significant attribute bits  */
        *p_sign_mask = 1 << (physfp+2);
    } else
        return CCL_BAD_PARAM_ERR;
    return CCL_OK;
}

/* Get appropriate device/attr/significant_mask for port "reset" operating 
 */
ccl_err_t devboard_portrst_to_devattr(int logport, char dev_name[], 
                                      char attr_name[], b_u32 *p_sign_mask, 
                                      int *p_is_active_hi)
{
    int     physfp;

    /* check input parameters */
    if ( !p_sign_mask || !p_is_active_hi ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* get physfp index */
    if ( _fboard_logport_to_hwsfp(logport, &physfp) ) {
        PDEBUG("Error! Can't get hwsfp number for the logport=%d\n", logport);
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    *p_is_active_hi = 1;
    /* get target device name/output port attribute name */
    if ( physfp >= 12 && physfp < 14 ) {
        strncpy(dev_name, "axigpio/9", DEVMAN_DEVNAME_LEN);      
        strncpy(attr_name, "data", DEVMAN_DEVNAME_LEN);      
        /* prepare significant attribute bits  */
        *p_sign_mask = 1 << (physfp+16);

    } else if ( physfp >= 26 && physfp < 28 ) {
        strncpy(dev_name, "axigpio/10", DEVMAN_DEVNAME_LEN);      
        strncpy(attr_name, "data", DEVMAN_DEVNAME_LEN);     
        /* prepare significant attribute bits  */
        *p_sign_mask = 1 << (physfp+2);
    } else
        return CCL_BAD_PARAM_ERR;

    return CCL_OK;
}

/* Get appropriate device/attr/significant_mask for port "media_pres" operating 
 */
ccl_err_t devboard_portprs_to_devattr(int logport, char dev_name[], 
                                      char attr_name[], b_u32 *p_sign_mask, 
                                      int *p_is_active_hi)
{
    int     physfp;

    /* check input parameters */
    if ( !p_sign_mask || !p_is_active_hi ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* get physfp index */
    if ( _fboard_logport_to_hwsfp(logport, &physfp) ) {
        PDEBUG("Error! Can't get hwsfp number for the logport=%d\n", logport);
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* actual signal is module absent */
    *p_is_active_hi = 0;
    /* get target device name/output port attribute name */
    if ( physfp >= 0 && physfp < 14 ) {
        strncpy(dev_name, "axigpio/9", DEVMAN_DEVNAME_LEN);      
        strncpy(attr_name, "data", DEVMAN_DEVNAME_LEN);      
        /* prepare significant attribute bits  */
        *p_sign_mask = 1 << physfp;

    } else if ( physfp >= 14 && physfp < 28 ) {
        strncpy(dev_name, "axigpio/10", DEVMAN_DEVNAME_LEN);      
        strncpy(attr_name, "data", DEVMAN_DEVNAME_LEN);     
        /* prepare significant attribute bits  */
        *p_sign_mask = 1 << (physfp-14);
    } else
        return CCL_BAD_PARAM_ERR;
    return CCL_OK;
}

/* Get appropriate device/attr/significant_mask for port "tx_fault" operating  
 */
ccl_err_t devboard_porttxf_to_devattr(int logport, 
                                      char dev_name[], 
                                      char attr_name[], 
                                      b_u8 *p_sign_mask)
{
    int    physfp;
    b_u32  offset = 0;

    /* check input parameters */
    if ( !p_sign_mask ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* get physfp index */
    if ( _fboard_logport_to_hwsfp(logport, &physfp) || 
         !p_sign_mask ) {
        PDEBUG("Error! Can't get hwsfp number for the logport=%d\n", logport);
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* get target i2cio device name/output port attribute name */
    if ( physfp >= 0 && physfp < 4 ) {
        strncpy(dev_name, "i2cio/0", DEVMAN_DEVNAME_LEN);      
        strncpy(attr_name, "oport7_0", DEVMAN_DEVNAME_LEN);      
        /* prepare significant attribute bits offset */
        offset = 0;
    } else if ( physfp >= 4 && physfp < 8 ) {
        strncpy(dev_name, "i2cio/1", DEVMAN_DEVNAME_LEN);      
        strncpy(attr_name, "oport7_0", DEVMAN_DEVNAME_LEN);     
        offset = 4;
    } else if ( physfp >= 8 && physfp < 12 ) {
        strncpy(dev_name, "i2cio/2", DEVMAN_DEVNAME_LEN);      
        strncpy(attr_name, "oport7_0", DEVMAN_DEVNAME_LEN);      
        offset = 8;
    } else if ( physfp >= 14 && physfp < 18 ) {
        strncpy(dev_name, "i2cio/4", DEVMAN_DEVNAME_LEN);      
        strncpy(attr_name, "oport7_0", DEVMAN_DEVNAME_LEN);     
        offset = 14;
    } else if ( physfp >= 18 && physfp < 22 ) {
        strncpy(dev_name, "i2cio/5", DEVMAN_DEVNAME_LEN);      
        strncpy(attr_name, "oport7_0", DEVMAN_DEVNAME_LEN);      
        offset = 18;
    } else if ( physfp >= 22 && physfp < 26 ) {
        strncpy(dev_name, "i2cio/6", DEVMAN_DEVNAME_LEN);      
        strncpy(attr_name, "oport7_0", DEVMAN_DEVNAME_LEN);     
        offset = 22;
    } else
        return CCL_BAD_PARAM_ERR;

    /* prepare significant attribute bits */
    *p_sign_mask = 1 << (1 + 2*(physfp-offset));

    return CCL_OK;
}

/* Get appropriate device/attr/significant_mask for port "rx_los" operating  
 */
ccl_err_t devboard_portrxlos_to_devattr(int logport, 
                                        char dev_name[], 
                                        char attr_name[], 
                                        b_u8 *p_sign_mask)
{
    int    physfp;
    b_u32  offset = 0;

    /* check input parameters */
    if ( !p_sign_mask ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* get physfp index */
    if ( _fboard_logport_to_hwsfp(logport, &physfp) || 
         !p_sign_mask ) {
        PDEBUG("Error! Can't get hwsfp number for the logport=%d\n", logport);
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* get target i2cio device name/output port attribute name */
    if ( physfp >= 0 && physfp < 4 ) {
        strncpy(dev_name, "i2cio/0", DEVMAN_DEVNAME_LEN);      
        strncpy(attr_name, "oport7_0", DEVMAN_DEVNAME_LEN);      
        /* prepare significant attribute bits offset */
        offset = 0;
    } else if ( physfp >= 4 && physfp < 8 ) {
        strncpy(dev_name, "i2cio/1", DEVMAN_DEVNAME_LEN);      
        strncpy(attr_name, "oport7_0", DEVMAN_DEVNAME_LEN);     
        offset = 4;
    } else if ( physfp >= 8 && physfp < 12 ) {
        strncpy(dev_name, "i2cio/2", DEVMAN_DEVNAME_LEN);      
        strncpy(attr_name, "oport7_0", DEVMAN_DEVNAME_LEN);      
        offset = 8;
    } else if ( physfp >= 14 && physfp < 18 ) {
        strncpy(dev_name, "i2cio/4", DEVMAN_DEVNAME_LEN);      
        strncpy(attr_name, "oport7_0", DEVMAN_DEVNAME_LEN);     
        offset = 14;
    } else if ( physfp >= 18 && physfp < 22 ) {
        strncpy(dev_name, "i2cio/5", DEVMAN_DEVNAME_LEN);      
        strncpy(attr_name, "oport7_0", DEVMAN_DEVNAME_LEN);      
        offset = 18;
    } else if ( physfp >= 22 && physfp < 26 ) {
        strncpy(dev_name, "i2cio/6", DEVMAN_DEVNAME_LEN);      
        strncpy(attr_name, "oport7_0", DEVMAN_DEVNAME_LEN);     
        offset = 22;
    } else
        return CCL_BAD_PARAM_ERR;
    /* prepare significant attribute bits */
    *p_sign_mask = 1 << 2*(physfp-offset) ;

    return CCL_OK;
}

/* Get appropriate device/attr/significant_mask for port "rs" operating  
 */
ccl_err_t devboard_portrs_to_devattr(int logport, 
                                     b_u8 rs_id,
                                     char dev_name[], 
                                     char attr_name[], 
                                     b_u8 *p_sign_mask)
{
    int    physfp;
    b_u32  offset = 0;

    /* check input parameters */
    if ( !p_sign_mask  || rs_id >= DEVBRD_TRANS_RS_ID_MAXNUM) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* get physfp index */
    if ( _fboard_logport_to_hwsfp(logport, &physfp) || 
         !p_sign_mask ) {
        PDEBUG("Error! Can't get hwsfp number for the logport=%d\n", logport);
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* get target i2cio device name/output port attribute name */
    if ( physfp >= 0 && physfp < 4 ) {
        strncpy(dev_name, "i2cio/0", DEVMAN_DEVNAME_LEN);      
        strncpy(attr_name, "oport15_8", DEVMAN_DEVNAME_LEN);      
        /* prepare significant attribute bits offset */
        offset = 0;
    } else if ( physfp >= 4 && physfp < 8 ) {
        strncpy(dev_name, "i2cio/1", DEVMAN_DEVNAME_LEN);      
        strncpy(attr_name, "oport15_8", DEVMAN_DEVNAME_LEN);     
        offset = 4;
    } else if ( physfp >= 8 && physfp < 12 ) {
        strncpy(dev_name, "i2cio/2", DEVMAN_DEVNAME_LEN);      
        strncpy(attr_name, "oport15_8", DEVMAN_DEVNAME_LEN);      
        offset = 8;
    } else if ( physfp >= 14 && physfp < 18 ) {
        strncpy(dev_name, "i2cio/4", DEVMAN_DEVNAME_LEN);      
        strncpy(attr_name, "oport15_8", DEVMAN_DEVNAME_LEN);     
        offset = 14;
    } else if ( physfp >= 18 && physfp < 22 ) {
        strncpy(dev_name, "i2cio/5", DEVMAN_DEVNAME_LEN);      
        strncpy(attr_name, "oport15_8", DEVMAN_DEVNAME_LEN);      
        offset = 18;
    } else if ( physfp >= 22 && physfp < 26 ) {
        strncpy(dev_name, "i2cio/6", DEVMAN_DEVNAME_LEN);      
        strncpy(attr_name, "oport15_8", DEVMAN_DEVNAME_LEN);     
        offset = 22;
    } else
        return CCL_BAD_PARAM_ERR;
    /* prepare significant attribute bits */
    *p_sign_mask = 1 << (8 + 2*(physfp-offset) + rs_id);

    return CCL_OK;
}

ccl_err_t devboard_portmodsell_to_devattr(int logport, 
                                          char dev_name[], 
                                          char attr_name[], 
                                          b_u8 *p_sign_mask)
{
    int    physfp;
    b_u32  offset = 0;

    /* check input parameters */
    if ( !p_sign_mask ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* get physfp index */
    if ( _fboard_logport_to_hwsfp(logport, &physfp) || 
         !p_sign_mask ) {
        PDEBUG("Error! Can't get hwsfp number for the logport=%d\n", logport);
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* get target i2cio device name/output port attribute name */
    if ( physfp >= 12 && physfp < 14 ) {
        strncpy(dev_name, "i2cio/3", DEVMAN_DEVNAME_LEN);      
        strncpy(attr_name, "oport7_0", DEVMAN_DEVNAME_LEN);      
        /* prepare significant attribute bits offset */
        offset = 12;
    } else if ( physfp >= 26 && physfp < 28 ) {
        strncpy(dev_name, "i2cio/7", DEVMAN_DEVNAME_LEN);      
        strncpy(attr_name, "oport7_0", DEVMAN_DEVNAME_LEN);     
        offset = 26;
    } else
        return CCL_BAD_PARAM_ERR;
    /* prepare significant attribute bits */
    *p_sign_mask = 1 << 4*(physfp-offset);

    return CCL_OK;
}

ccl_err_t devboard_portlpmode_to_devattr(int logport, 
                                         char dev_name[], 
                                         char attr_name[], 
                                         b_u8 *p_sign_mask)
{
    int    physfp;
    b_u32  offset = 0;

    /* check input parameters */
    if ( !p_sign_mask ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* get physfp index */
    if ( _fboard_logport_to_hwsfp(logport, &physfp) || 
         !p_sign_mask ) {
        PDEBUG("Error! Can't get hwsfp number for the logport=%d\n", logport);
        DEVBOARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* get target i2cio device name/output port attribute name */
    if ( physfp >= 12 && physfp < 14 ) {
        strncpy(dev_name, "i2cio/3", DEVMAN_DEVNAME_LEN);      
        strncpy(attr_name, "oport7_0", DEVMAN_DEVNAME_LEN);      
        /* prepare significant attribute bits offset */
        offset = 12;
    } else if ( physfp >= 26 && physfp < 28 ) {
        strncpy(dev_name, "i2cio/7", DEVMAN_DEVNAME_LEN);      
        strncpy(attr_name, "oport7_0", DEVMAN_DEVNAME_LEN);     
        offset = 26;
    } else
        return CCL_BAD_PARAM_ERR;

    /* prepare significant attribute bits */
    *p_sign_mask = 1 << (2 + 4*(physfp-offset));

    return CCL_OK;
}

ccl_err_t devboard_portinit_to_devattr(__attribute__((unused)) int port, 
                                       __attribute__((unused)) char dev_name[], 
                                       __attribute__((unused)) char attr_name[], 
                                       __attribute__((unused)) int *discr)
{
    return CCL_OK;
}

ccl_err_t devboard_portcntrs_to_devattr(__attribute__((unused)) int port, 
                                        __attribute__((unused)) char dev_name[], 
                                        __attribute__((unused)) char attr_name[], 
                                        __attribute__((unused)) int *discr)
{
    return CCL_OK;
}

ccl_err_t devboard_porttrgen_to_devattr(__attribute__((unused))  int port, 
                                        __attribute__((unused)) char dev_name[], 
                                        __attribute__((unused)) char attr_name[])
{
    return CCL_OK;
}

ccl_err_t devboard_portcntrsrst_to_devattr(__attribute__((unused)) int port, 
                                           __attribute__((unused)) char dev_name[], 
                                           __attribute__((unused)) char attr_name[], 
                                           __attribute__((unused)) int *discr)
{
    return CCL_OK;
}

/* Initialize all board types/devices
 */
ccl_err_t devboard_init(__attribute__((unused)) b_u32 flags)
{
    /* i2cbus initialization */
    _ret = _fboard_i2cbus_init(&_i2cbus_brd_descr[0], 
                               sizeof(_i2cbus_brd_descr)/sizeof(_i2cbus_brd_descr[0]));
    if ( _ret ) {
        PDEBUG("Error! Can't initialize I2CBUS devices\n");
        DEVBOARD_ERROR_RETURN(CCL_FAIL);
    }

    /* i2c devices initialization */
    _ret = _fboard_i2c_devices_init();
    if ( _ret ) {
        PDEBUG("Error! Can't initialize I2C devices\n");
        DEVBOARD_ERROR_RETURN(CCL_FAIL);
    }

    /* atp module initialization */
    _ret = _fboard_atp_init(&_atp_brd_descr[0], 
                             sizeof(_atp_brd_descr)/sizeof(_atp_brd_descr[0]));
    if ( _ret ) {
        PDEBUG("Error! Can't initialize atp device\n");
        DEVBOARD_ERROR_RETURN(CCL_FAIL);
    }

    /* atp module initialization */
    _ret = _fboard_cpu_init(&_cpu_brd_descr[0], 
                             sizeof(_cpu_brd_descr)/sizeof(_cpu_brd_descr[0]));
    if ( _ret ) {
        PDEBUG("Error! Can't initialize cpu device\n");
        DEVBOARD_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

/* Deinitialize all board devices/types
 */
ccl_err_t devboard_fini(void)
{
    return CCL_OK;
}
