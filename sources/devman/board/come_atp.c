/********************************************************************************/
/**
 * @file atp.c
 * @brief ATP device manager driver 
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include <linux/limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include <fcntl.h> 
#include <termios.h>
#include <unistd.h>
#include <math.h>

#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "ccl_stat_msg.h"
#include "ccl_stat_sender.h"
#include "http_cluster_node.h"
#include "devman.h"
#include "devman_mod.h"
#include "devboard.h"
#include "spibus.h"
#include "axispi.h"
#include "come.h"

extern ccl_logger_t logger;

#define ATP_DEBUG
/* printing/error-returning macros */
#ifdef ATP_DEBUG
    #define PDEBUG(fmt, args...) \
         ccl_syslog_err("ATP: %s[%d]: " fmt, __FUNCTION__, __LINE__, ## args)
#else
    #define PDEBUG(fmt, args...)
#endif
#define ATP_ERROR_RETURN(err) do { \
   ccl_syslog_err("Error! %s[%d], error=%i\n", __FUNCTION__, __LINE__, (err)); \
   return err; \
} while (0)


#define FLASH_MAX_ID                11
#define UART_MAX_ID                 9
#define EEPROM_MAX_ID               10
#define RTC_MAX_ID                  3

#define ARTIX_IMAGE_FILENAME_MAXLEN 50

#define DATA_ARRAY_LEN              16


typedef struct {
    b_bool  enable;
    b_u32   index;
    b_u8    type[DEVMAN_CMD_PARM_LEN+1];
} es_config_t;


/** @struct atp_ctx_t
 *  atp device internal control structure
 */
typedef struct atp_ctx_t {
    void                     *devo;
    atp_brdspec_t            brdspec;
    ccl_stat_sender_t        stat_sender;
    http_cluster_node_t      node;
    http_cluster_node_conn_t node_conn;
    es_config_t              es_config;
} atp_ctx_t;

static ccl_err_t _ret;
static int       _rc;

static void _prepare_board_stat(ccl_stat_msg_t *stat_msg, void *priv);

static ccl_err_t _fatp_get_board_id(b_u8 *board_id)
{
    b_i32 fd;
    char in[2] = {0, 0};
    b_i32 idl; 
    b_i32 idm;
    char buf[DEVMAN_DEVNAME_LEN];

    fd = open("/sys/class/gpio/export", O_WRONLY);
    if (fd == -1) {
        PDEBUG("Error! Can't open /sys/class/gpio/export\n");
        ATP_ERROR_RETURN(CCL_FAIL);
    }

    snprintf(buf, DEVMAN_DEVNAME_LEN, "%d", GPIO_BOARD_ID_LSB);
    _rc = write(fd, buf, strlen(buf)); 
    if (_rc == -1) {
        PDEBUG("Error writing %d to /sys/class/gpio/export\n",
               GPIO_BOARD_ID_LSB);
        ATP_ERROR_RETURN(CCL_FAIL);
    }

    snprintf(buf, DEVMAN_DEVNAME_LEN, "%d", GPIO_BOARD_ID_MSB);
    _rc = write(fd, buf, strlen(buf)); 
    if (_rc == -1) {
        PDEBUG("Error writing %d to /sys/class/gpio/export\n",
               GPIO_BOARD_ID_MSB);
        ATP_ERROR_RETURN(CCL_FAIL);
    }
    close(fd);

    snprintf(buf, DEVMAN_DEVNAME_LEN, 
             "/sys/class/gpio/gpio%d/value", 
             GPIO_BOARD_ID_LSB);
    fd = open(buf, O_RDWR | O_SYNC);
    if (fd == -1) {
        PDEBUG("Unable to open %s\n", buf);
        ATP_ERROR_RETURN(CCL_FAIL);
    }
    if (read(fd, in, 1) != 1) {
        PDEBUG("Error reading from %d\n", buf);
        ATP_ERROR_RETURN(CCL_FAIL);
    }
    close(fd);
    idl=atoi(in);

    snprintf(buf, DEVMAN_DEVNAME_LEN, 
             "/sys/class/gpio/gpio%d/value", 
             GPIO_BOARD_ID_MSB);
    fd = open(buf, O_RDWR | O_SYNC);
    if (fd == -1) {
        PDEBUG("Unable to open %s\n", buf);
        ATP_ERROR_RETURN(CCL_FAIL);
    }
    if (read(fd, in, 1) != 1) {
        PDEBUG("Error reading from %d\n", buf);
        ATP_ERROR_RETURN(CCL_FAIL);
    }
    close(fd);
    idm=atoi(in);

    fd = open("/sys/class/gpio/unexport", O_WRONLY);
    if (fd == -1) {
        PDEBUG("Unable to open /sys/class/gpio/unexport");
        ATP_ERROR_RETURN(CCL_FAIL);
    }
    snprintf(buf, DEVMAN_DEVNAME_LEN, "%d", GPIO_BOARD_ID_LSB);
    _rc = write(fd, buf, strlen(buf)); 
    if (_rc == -1) {
        PDEBUG("Error writing %d to /sys/class/gpio/export\n",
               GPIO_BOARD_ID_LSB);
        ATP_ERROR_RETURN(CCL_FAIL);
    }
    snprintf(buf, DEVMAN_DEVNAME_LEN, "%d", GPIO_BOARD_ID_MSB);
    _rc = write(fd, buf, strlen(buf)); 
    if (_rc == -1) {
        PDEBUG("Error writing %d to /sys/class/gpio/export\n",
               GPIO_BOARD_ID_MSB);
        ATP_ERROR_RETURN(CCL_FAIL);
    }
    close(fd);

    *board_id = (idm << 1) | idl;

    return CCL_OK;
}

static ccl_err_t _fatp_cmd_get_board_id(devo_t atpdevo, 
                                        device_cmd_parm_t parms[], 
                                        b_u16 parms_num)
{
    b_u8 board_id;

    _ret = _fatp_get_board_id(&board_id);
    if ( _ret ) {
        PDEBUG("_fatp_get_board_id error\n");
        ATP_ERROR_RETURN(_ret);
    }

    PRINTL("board_id = %d\n",board_id);

    return CCL_OK;
}

static ccl_err_t _fatp_cmd_stat_period(devo_t atpdevo, 
                                       device_cmd_parm_t parms[], 
                                       b_u16 parms_num)
{
    atp_ctx_t     *p_atp = devman_device_private(atpdevo);
    b_u32         period;

    period = (b_u32)parms[0].value; 

    _ret = ccl_update_stat_sender(&p_atp->stat_sender, 
                                  period, &logger);
    if (_ret) {
        PDEBUG("ccl_update_stat_sender error\n");
        ATP_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

static ccl_err_t _fatp_get_cpu_sensors(sys_sensors_t *sys_sensors)
{
    devo_t devo;

    _ret = devman_device_object_identify("cpu", 0, &devo);
    if ( _ret || !devo) {
        PDEBUG("Error! Can't identify \"cpu\" object\n");
        ATP_ERROR_RETURN(CCL_FAIL);
    }

    _ret = devman_attr_array_get_buff(devo, "sensors", 0, 
                                      (b_u8 *)sys_sensors, 
                                      sizeof(*sys_sensors));
    if (_ret) {
        PDEBUG("devman_attr_array_get_buff error\n");
        ATP_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

static ccl_err_t _fatp_cmd_es_config(devo_t atpdevo, 
                                     device_cmd_parm_t parms[], 
                                     b_u16 parms_num)
{
    atp_ctx_t     *p_atp = devman_device_private(atpdevo);
    char          *hostname;
    b_u32         port;
    b_u8          board_id;
    sys_sensors_t sys_sensors;

    _ret = _fatp_get_board_id(&board_id);
    if ( _ret ) {
        PDEBUG("_fatp_get_board_id error\n");
        ATP_ERROR_RETURN(_ret);
    }

    p_atp->es_config.enable = (b_u8)parms[0].value ? CCL_TRUE : CCL_FALSE; 
    p_atp->es_config.index = parms[1].value;
    snprintf(p_atp->es_config.type, DEVMAN_CMD_PARM_LEN+1, "%s_%u", parms[2].buf, board_id);
    hostname = parms[3].buf;
    port = parms[4].value;

    if (p_atp->es_config.enable) {
        init_http_cluster_node(&p_atp->node, hostname, port, 0);
        _ret = connect_to_http_cluster_node(&p_atp->node, &p_atp->node_conn, 15);
        if (_ret) {
            PDEBUG("connect_to_http_cluster_node error\n");
            ATP_ERROR_RETURN(_ret);
        }
        _ret = _fatp_get_cpu_sensors(&sys_sensors);
        if (_ret) {
            PDEBUG("Error! _fatp_get_cpu_sensors failed\n");
            ATP_ERROR_RETURN(CCL_FAIL);
        }

        _ret = ccl_init_stat_sender(&p_atp->stat_sender,
                                    p_atp->es_config.index,
                                    p_atp->es_config.type,
                                    30,
                                    _prepare_board_stat,
                                    &sys_sensors,
                                    p_atp,
                                    &logger);
        if (_ret) {
            PDEBUG("Error! ccl_init_stat_sender failed\n");
            ATP_ERROR_RETURN(CCL_FAIL);
        }
    }

    return CCL_OK;
}

static void _prepare_board_stat(ccl_stat_msg_t *stat_msg, void *priv)
{
    atp_ctx_t   *p_atp;
    b_u32       i, j;

    if (!stat_msg || !priv) {
        ccl_syslog_err("Error! %s[%d] - Invalid input parameters\n", 
               __FUNCTION__, __LINE__);
        return;
    }

    p_atp = (atp_ctx_t *)priv;


    char *stat_str = ccl_stat_msg_to_string(stat_msg);
    PDEBUGG("%s\n\n", stat_str);
    free(stat_str);

    if (p_atp->es_config.enable) {
        char path[DEVMAN_DEVNAME_LEN];
        snprintf(path, DEVMAN_DEVNAME_LEN, "tsg-%d-monitor/_doc", p_atp->es_config.index);
        http_cluster_node_conn_t node_conn;
        stat_str = ccl_stat_msg_to_string_unformatted(stat_msg);
        PDEBUGG("ES config: msg: %p, msg_len: %d, enable: %d, index: %d, hostname: %s, port: %d\n", 
                stat_str,
                strlen(stat_str),
                p_atp->es_config.enable,
                p_atp->es_config.index,
                p_atp->node.host_name,
                p_atp->node.port);
        PDEBUGG("%s\n", stat_str);
        if (!stat_str) 
            goto _prepare_board_stat_end;
        _ret = http_cluster_node_send_post(&p_atp->node_conn,
                                           path,
                                           stat_str,
                                           strlen(stat_str),
                                           NULL,
                                           "application/json");
        free(stat_str);
        if (_ret) {
            ccl_syslog_err("Error! http_cluster_node_send_post()\n");
            goto _prepare_board_stat_end;
        }
    }

    time_t curr_time = time(NULL);
    p_atp->stat_sender.prev_send_time = curr_time;

_prepare_board_stat_end:
    return;
}

static ccl_err_t _init_dev(char *devname, b_i32 discr, b_u32 idx)
{
    devo_t devo;

    if (!devname) {
        PDEBUG("Error! Invalid input parameters\n");
        ATP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    _ret = devman_device_object_identify(devname, discr, &devo);
    if ( _ret || !devo) {
        ccl_syslog_err("Error! Can't identify %s/%d discr object\n", 
               devname, discr);
        ATP_ERROR_RETURN(CCL_FAIL);
    }
    _ret = devman_attr_array_set_word(devo, "init", idx, 1);
    if (_ret) {
        ccl_syslog_err("Error! Can't set \"init\" attribute for %s/%d discr object\n", 
               devname, discr);
        ATP_ERROR_RETURN(_ret);
    }
}

static ccl_err_t _init_devices(void)
{
    return CCL_OK;
}

static ccl_err_t _configure_tests(void)
{
    return CCL_OK;
}

static ccl_err_t _run_test_dev(char *devname, 
                               b_i32 discr, b_u32 idx, 
                               void *p_stats, b_u32 size)
{
    devo_t devo;

    if ( !devname ) {
        PDEBUG("Error! Invalid input parameters\n");
        ATP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    _ret = devman_device_object_identify(devname, discr, &devo);
    if ( _ret || !devo) {
        ccl_syslog_err("Error! Can't identify %s/%d discr object\n", 
               devname, discr);
        ATP_ERROR_RETURN(CCL_FAIL);
    }

    PRINTL("Tested device: %s/%d idx=%d\n", devname, discr, idx);

    _ret = devman_attr_array_set_buff(devo, "run_test", idx, 
                                      (b_u8 *)p_stats, size);
    if (_ret) {
        ccl_syslog_err("Error! Can't set \"run_test\" attribute for %s/%d discr idx: %d object\n", 
               devname, discr, idx);
        ATP_ERROR_RETURN(_ret);
    }
    return CCL_OK;
}

static ccl_err_t _run_tests(atp_ctx_t *p_atp)
{
    return CCL_OK;
}

static ccl_err_t _fatp_cmd_bist(devo_t atpdevo, 
                                device_cmd_parm_t parms[], 
                                b_u16 parms_num) 
{
    atp_ctx_t *p_atp = devman_device_private(atpdevo);
    b_i32 i;

    _ret = _init_devices();
    if (_ret) {
        PDEBUG("Error! _init_devices failed\n");
        ATP_ERROR_RETURN(_ret);
    }
#if 0
    _ret = _configure_tests();
    if (_ret) {
        PDEBUG("Error! _configure_tests failed\n");
        ATP_ERROR_RETURN(_ret);
    }
#endif
    while (1) {
        _ret = _run_tests(p_atp);
        if (_ret) {
            PDEBUG("Error! _run_tests failed\n");
            ATP_ERROR_RETURN(_ret);
        }
        usleep(1000000);
    }

    return CCL_OK;
}

/* device commands */
static device_cmd_t _cmds[] = 
{
    { 
        .name = "get_board_id", .f_cmd = _fatp_cmd_get_board_id, .parms_num = 0,
        .help = "Get board ID.\n"
    },
    { 
        .name = "stat_period", .f_cmd = _fatp_cmd_stat_period, .parms_num = 1,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "id", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%d" },
        }, 
        .help = "Set statistics time period.\n"
    },
    { 
        .name = "es_config", .f_cmd = _fatp_cmd_es_config, .parms_num = 5,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "enable", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
            { .name = "index", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%d" },
            { .name = "type", .type = eDEVMAN_ATTR_STRING, .size = DEVMAN_CMD_PARM_LEN, .format = "%s" },
            { .name = "hostname", .type = eDEVMAN_ATTR_STRING, .size = DEVMAN_CMD_PARM_LEN, .format = "%s" },
            { .name = "port", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%d" },
        }, 
        .help = "ElasticSearch configuration.\n"
    },
    { 
        .name = "bist", .f_cmd = _fatp_cmd_bist, .parms_num = 1,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "id", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
        }, 
        .help = "Run BiST on the board referenced by <id>.\n"
    },
};


/** atp attributes definition */
static device_attr_t _attrs[] = {
};

/** device specific initialization */
static ccl_err_t _fatp_add(void *devo, void *brdspec)
{
    atp_ctx_t       *p_atp;
    atp_brdspec_t   *p_brdspec;
    b_u32           i;

    if ( !devo || !brdspec ) {
        PDEBUG("Error! Invalid input parameters\n");
        ATP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_atp = (atp_ctx_t *)devman_device_private(devo);
    p_brdspec = (atp_brdspec_t *)brdspec;

    p_atp->devo = devo;    
    /* save the board specific info */
    memcpy(&p_atp->brdspec, p_brdspec, sizeof(atp_brdspec_t));

    return CCL_OK;
}

/** device specific deinitialization */
static ccl_err_t _fatp_cut(void *devo)
{
    if ( !devo ) {
        PDEBUG("Error! Invalid input parameters\n");
        ATP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    return CCL_OK;
}

/** atp device type */
static device_driver_t _atp_driver = {
    .name = "atp",
    .f_add = _fatp_add,
    .f_cut = _fatp_cut,
    .attrs_num = sizeof(_attrs)/sizeof(_attrs[0]),                 
    .attrs = _attrs,
    .cmds_num = sizeof(_cmds)/sizeof(_cmds[0]), 
    .cmds = _cmds,
    .priv_size = sizeof(atp_ctx_t)
}; 

/** 
 *  Initialize ATP device type
 */

ccl_err_t devatp_init(void)
{
    /* register device type */
    _ret = devman_driver_register(&_atp_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to register driver\n");
        ATP_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

/** 
 *  Deinitialize ATP device type
 */
ccl_err_t devatp_fini(void)
{
    /* unregister device type */
    _ret = devman_driver_unregister(&_atp_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to unregister driver\n");
        ATP_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

