
CMAKE_MINIMUM_REQUIRED(VERSION 2.8.12)

project(tsg_axispi)

include(${PROJECT_SOURCE_DIR}/../../../CMakeCommon.txt)

execute_process(
  COMMAND git rev-parse HEAD
  COMMAND tr -d '\n'
  WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
  OUTPUT_VARIABLE COMMIT_HASH
  OUTPUT_STRIP_TRAILING_WHITESPACE
)


set(INCLUDE_DIR
    ../../common/h
    ../h
)

set(SOURCE_FILES
    axispi.c
)

find_package(PkgConfig REQUIRED)
pkg_search_module(GLIB REQUIRED glib-2.0)

link_directories(
    ${PROJECT_SOURCE_DIR}/../../lib/
)

add_library(${PROJECT_NAME} SHARED ${SOURCE_FILES} ${HEADER_FILES})
target_link_libraries(${PROJECT_NAME} ${GLIB_LIBRARIES})
target_include_directories(${PROJECT_NAME} PUBLIC ${INCLUDE_DIR} ${GLIB_INCLUDE_DIRS})

include(CPack)




