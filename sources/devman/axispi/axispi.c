/********************************************************************************/
/**
 * @file axispi.c
 * @brief AXI Serial Peripheral Interface (SPI) device manager driver
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/

#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <semaphore.h>
#include <linux/limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "spibus.h"
#include "axispi.h"

//#define AXISPI_DEBUG
/* printing/error-returning macros */
#ifdef AXISPI_DEBUG
    #define PDEBUG(fmt, args...) \
            ccl_syslog_err("AXISPI_DEBUG: %s[%d]: " fmt, __FUNCTION__, __LINE__, ## args)
#else
    #define PDEBUG(fmt, args...)
#endif
#define AXISPI_ERROR_RETURN(err) do { \
   ccl_syslog_err("Error! %s[%d], error=%i\n", __FUNCTION__, __LINE__, (err)); \
   return err; \
} while (0)
#define AXISPI_REG_READ(context, offset, offset_sz, idx, pvalue, size) do { \
    _ret = _axispi_reg_read((context), \
                            (offset), \
                            (offset_sz), (idx), \
                            (pvalue), (size)); \
    if ( _ret ) { \
        PDEBUG("_axispi_reg_read error\n"); \
        AXISPI_ERROR_RETURN(_ret); \
    } \
} while (0)

#define AXISPI_REG_WRITE(context, offset, offset_sz, idx, pvalue, size) do { \
    _ret = _axispi_reg_write((context), \
                             (offset), \
                             (offset_sz), (idx), \
                             (pvalue), (size)); \
    if ( _ret ) { \
        PDEBUG("_axispi_reg_write error\n"); \
        AXISPI_ERROR_RETURN(_ret); \
    } \
} while (0)

/* AXISPI registers offsets */
#define AXISPI_IISR		0x20	/**< Interrupt status register */
#define AXISPI_IIER		0x28	/**< Interrupt Enable register */
#define AXISPI_SRR	 	0x40	/**< Software Reset register */
#define AXISPI_CR		0x60	/**< Control register */
#define AXISPI_SR		0x64	/**< Status register */
#define AXISPI_DTR		0x68	/**< Data Transmit register*/
#define AXISPI_DRR		0x6C	/**< Data Receive regoster */
#define AXISPI_SSR		0x70	/**< 32-bit Slave Select register */
#define AXISPI_TFO		0x74	/**< Tx FIFO Occupancy register */
#define AXISPI_RFO		0x78	/**< Rx FIFO Occupancy register */
#define AXISPI_DGIER	0x1C	/**< Global Interrupt Enable register */

#define AXISPI_MAX_OFFSET AXISPI_RFO  

#define AXISPI_SR_RESET_STATE	0x5	   /* Default to Tx/Rx reg empty */
#define AXISPI_CR_RESET_STATE	0x180

/**
 * @name Global Interrupt Enable Register (GIER) mask(s)
 * @{
 */
#define AXISPI_GINTR_ENABLE_MASK	0x80000000	/**< Global interrupt enable */

/* @} */


/** @name SPI Device Interrupt Status/Enable Registers
 *
 * <b> Interrupt Status Register (IPISR) </b>
 *
 * This register holds the interrupt status flags for the Spi device.
 *
 * <b> Interrupt Enable Register (IPIER) </b>
 *
 * This register is used to enable interrupt sources for the Spi device.
 * Writing a '1' to a bit in this register enables the corresponding Interrupt.
 * Writing a '0' to a bit in this register disables the corresponding Interrupt.
 *
 * ISR/IER registers have the same bit definitions and are only defined once.
 * @{
 */
#define AXISPI_INTR_MODE_FAULT_MASK	       0x00000001 /**< Mode fault error */
#define AXISPI_INTR_SLAVE_MODE_FAULT_MASK  0x00000002 /**< Selected as slave while disabled */
#define AXISPI_INTR_TX_EMPTY_MASK		   0x00000004 /**< DTR/TxFIFO is empty */
#define AXISPI_INTR_TX_UNDERRUN_MASK	   0x00000008 /**< DTR/TxFIFO underrun */
#define AXISPI_INTR_RX_FULL_MASK		   0x00000010 /**< DRR/RxFIFO is full */
#define AXISPI_INTR_RX_OVERRUN_MASK	       0x00000020 /**< DRR/RxFIFO overrun */
#define AXISPI_INTR_TX_HALF_EMPTY_MASK	   0x00000040 /**< TxFIFO is half empty */
#define AXISPI_INTR_SLAVE_MODE_MASK	       0x00000080 /**< Slave select mode */
#define AXISPI_INTR_RX_NOT_EMPTY_MASK	   0x00000100 /**< RxFIFO not empty */

/**
 * The following bits are available only in axi_qspi Interrupt Status and
 * Interrupt Enable registers.
 */
#define AXISPI_INTR_CPOL_CPHA_ERR_MASK	0x00000200 /**< CPOL/CPHA error */
#define AXISPI_INTR_SLAVE_MODE_ERR_MASK	0x00000400 /**< Slave mode error */
#define AXISPI_INTR_MSB_ERR_MASK		0x00000800 /**< MSB Error */
#define AXISPI_INTR_LOOP_BACK_ERR_MASK	0x00001000 /**< Loop back error */
#define AXISPI_INTR_CMD_ERR_MASK		0x00002000 /**< 'Invalid cmd' error */

/**
 * Mask for all the interrupts in the IP Interrupt Registers.
 */
#define AXISPI_INTR_ALL	(AXISPI_INTR_MODE_FAULT_MASK      | \
            			AXISPI_INTR_SLAVE_MODE_FAULT_MASK | \
				        AXISPI_INTR_TX_EMPTY_MASK         | \
				        AXISPI_INTR_TX_UNDERRUN_MASK      | \
				        AXISPI_INTR_RX_FULL_MASK          | \
				        AXISPI_INTR_TX_HALF_EMPTY_MASK    | \
				        AXISPI_INTR_RX_OVERRUN_MASK       | \
				        AXISPI_INTR_SLAVE_MODE_MASK       | \
				        AXISPI_INTR_RX_NOT_EMPTY_MASK     | \
				        AXISPI_INTR_CMD_ERR_MASK          | \
				        AXISPI_INTR_LOOP_BACK_ERR_MASK    | \
				        AXISPI_INTR_MSB_ERR_MASK          | \
				        AXISPI_INTR_SLAVE_MODE_ERR_MASK   | \
				        AXISPI_INTR_CPOL_CPHA_ERR_MASK)

/**
 * The interrupts we want at startup. We add the TX_EMPTY interrupt in later
 * when we're getting ready to transfer data.  The others we don't care
 * about for now.
 */
#define AXISPI_INTR_DFT_MASK (AXISPI_INTR_MODE_FAULT_MASK      | \
				             AXISPI_INTR_TX_UNDERRUN_MASK      | \
				             AXISPI_INTR_RX_OVERRUN_MASK       | \
				             AXISPI_INTR_SLAVE_MODE_FAULT_MASK | \
				             AXISPI_INTR_CMD_ERR_MASK)
/* @} */

/**
 * SPI Software Reset Register (SRR) mask.
 */
#define AXISPI_SRR_RESET_MASK		0x0000000A


/** @name SPI Control Register (CR) masks
 *
 * @{
 */
#define AXISPI_CR_LOOPBACK_MASK	     0x00000001 /**< Local loopback mode */
#define AXISPI_CR_ENABLE_MASK	     0x00000002 /**< System enable */
#define AXISPI_CR_MASTER_MODE_MASK	 0x00000004 /**< Enable master mode */
#define AXISPI_CR_CLK_POLARITY_MASK  0x00000008 /**< Clock polarity high or low */
#define AXISPI_CR_CLK_PHASE_MASK	 0x00000010 /**< Clock phase 0 or 1 */
#define AXISPI_CR_TXFIFO_RESET_MASK  0x00000020 /**< Reset transmit FIFO */
#define AXISPI_CR_RXFIFO_RESET_MASK  0x00000040 /**< Reset receive FIFO */
#define AXISPI_CR_MANUAL_SS_MASK	 0x00000080 /**< Manual slave select assert */
#define AXISPI_CR_TRANS_INHIBIT_MASK 0x00000100 /**< Master transaction inhibit */

/**
 * LSB/MSB first data format select. The default data format is MSB first.
 * The LSB first data format is not available in all versions of the Xilinx Spi
 * Device whereas the MSB first data format is supported by all the versions of
 * the Xilinx Spi Devices. Please check the HW specification to see if this
 * feature is supported or not.
 */
#define AXISPI_CR_LSB_MSB_FIRST_MASK	0x00000200

/* @} */

/** @name SPI Control Register (CR) masks for XIP Mode
 *
 * @{
 */
#define AXISPI_CR_XIP_CLK_PHASE_MASK	0x00000001 /**< Clock phase 0 or 1 */
#define AXISPI_CR_XIP_CLK_POLARITY_MASK	0x00000002 /**< Clock polarity high or low */

/* @} */




/** @name Status Register (SR) masks
 *
 * @{
 */
#define AXISPI_SR_RX_EMPTY_MASK	   0x00000001 /**< Receive Reg/FIFO is empty */
#define AXISPI_SR_RX_FULL_MASK	   0x00000002 /**< Receive Reg/FIFO is full */
#define AXISPI_SR_TX_EMPTY_MASK	   0x00000004 /**< Transmit Reg/FIFO is empty */
#define AXISPI_SR_TX_FULL_MASK	   0x00000008 /**< Transmit Reg/FIFO is full */
#define AXISPI_SR_MODE_FAULT_MASK  0x00000010 /**< Mode fault error */
#define AXISPI_SR_SLAVE_MODE_MASK  0x00000020 /**< Slave mode select */

/*
 * The following bits are available only in axi_qspi Status register.
 */
#define AXISPI_SR_CPOL_CPHA_ERR_MASK  0x00000040 /**< CPOL/CPHA error */
#define AXISPI_SR_SLAVE_MODE_ERR_MASK 0x00000080 /**< Slave mode error */
#define AXISPI_SR_MSB_ERR_MASK	      0x00000100 /**< MSB Error */
#define AXISPI_SR_LOOP_BACK_ERR_MASK  0x00000200 /**< Loop back error */
#define AXISPI_SR_CMD_ERR_MASK	      0x00000400 /**< 'Invalid cmd' error */

/* @} */

/** @name Status Register (SR) masks for XIP Mode
 *
 * @{
 */
#define AXISPI_SR_XIP_RX_EMPTY_MASK	    0x00000001 /**< Receive Reg/FIFO is empty */
#define AXISPI_SR_XIP_RX_FULL_MASK		0x00000002 /**< Receive Reg/FIFO is full */
#define AXISPI_SR_XIP_MASTER_MODF_MASK	0x00000004 /**< Receive Reg/FIFO is full */
#define AXISPI_SR_XIP_CPHPL_ERROR_MASK	0x00000008 /**< Clock Phase,Clock Polarity Error */
#define AXISPI_SR_XIP_AXI_ERROR_MASK	0x00000010 /**< AXI Transaction Error */

/* @} */


/** @name SPI Transmit FIFO Occupancy (TFO) mask
 *
 * @{
 */
/* The binary value plus one yields the occupancy.*/
#define AXISPI_TFO_MASK		0x0000001F

/* @} */

/** @name SPI Receive FIFO Occupancy (RFO) mask
 *
 * @{
 */
/* The binary value plus one yields the occupancy.*/
#define AXISPI_RFO_MASK		0x0000001F

/* @} */

/** @name data Width Definitions
 *
 * @{
 */
#define AXISPI_DATAWIDTH_BYTE	    8   /**< Tx/Rx Reg is Byte Wide */
#define AXISPI_DATAWIDTH_HALF_WORD	16  /**< Tx/Rx Reg is Half Word (16 bit) Wide */
#define AXISPI_DATAWIDTH_WORD	    32  /**< Tx/Rx Reg is Word (32 bit)  Wide */

/* @} */

/** @name SPI Modes
 *
 * The following constants define the modes in which qxi_qspi operates.
 *
 * @{
 */
#define AXISPI_STANDARD_MODE	0
#define AXISPI_DUAL_MODE		1
#define AXISPI_QUAD_MODE		2

/**
 * The Master option configures the SPI device as a master. By default, the
 * device is a slave.
 *
 * The Active Low Clock option configures the device's clock polarity. Setting
 * this option means the clock is active low and the SCK signal idles high. By
 * default, the clock is active high and SCK idles low.
 *
 * The Clock Phase option configures the SPI device for one of two transfer
 * formats.  A clock phase of 0, the default, means data if valid on the first
 * SCK edge (rising or falling) after the slave select (SS) signal has been
 * asserted. A clock phase of 1 means data is valid on the second SCK edge
 * (rising or falling) after SS has been asserted.
 *
 * The Loopback option configures the SPI device for loopback mode.  data is
 * looped back from the transmitter to the receiver.
 *
 * The Manual Slave Select option, which is default, causes the device not
 * to automatically drive the slave select.  The driver selects the device
 * at the start of a transfer and deselects it at the end of a transfer.
 * If this option is off, then the device automatically toggles the slave
 * select signal between bytes in a transfer.
 */
#define AXISPI_MASTER_OPTION		    0x1
#define AXISPI_CLK_ACTIVE_LOW_OPTION	0x2
#define AXISPI_CLK_PHASE_1_OPTION		0x4
#define AXISPI_LOOPBACK_OPTION		    0x8
#define AXISPI_MANUAL_SSELECT_OPTION	0x10


/** @struct axispi_ctx_t
 *  axispi context structure
 */
typedef struct axispi_ctx_t {
    void                     *devo;
    axispi_brdspec_t         brdspec;
} axispi_ctx_t;

static ccl_err_t       _ret;
static axispi_ctx_t    *_p_axispis[eDEVBRD_AXISPIS_NUM];

static ccl_err_t _axispi_init(void *priv);

/* axispi register read */
static ccl_err_t _axispi_reg_read(void *p_priv, b_u32 offset, 
                                  __attribute__((unused)) b_u32 offset_sz, 
                                  b_u32 idx, b_u8 *p_value, b_u32 size)
{
    axispi_ctx_t *p_axispi; 

    if ( offset > AXISPI_MAX_OFFSET || 
         !p_value || idx || size != sizeof(b_u32) ) {
        PDEBUG("Error! Invalid input parameters\n"
               "offset=0x%04x, p_value=%p, idx=%d, size=%d",
               offset, p_value, idx, size);
        AXISPI_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_axispi = (axispi_ctx_t *)p_priv;     
    offset += p_axispi->brdspec.offset;
    _ret = devspibus_read_buff(p_axispi->brdspec.devname, offset, idx, p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to read over spi bus\n");
        AXISPI_ERROR_RETURN(_ret);
    }
    PDEBUG("p_axispi->brdspec.offset=0x%x, offset=0x%03x, value=0x%08x\n", 
           p_axispi->brdspec.offset, offset, *(b_u32 *)p_value);

    return _ret;
}

/* axispi register write */
static ccl_err_t _axispi_reg_write(void *p_priv, b_u32 offset, 
                                   __attribute__((unused)) b_u32 offset_sz, 
                                   b_u32 idx, b_u8 *p_value, b_u32 size)
{
    axispi_ctx_t *p_axispi; 

    if ( offset > AXISPI_MAX_OFFSET || 
         !p_value || idx || size != sizeof(b_u32) ) {
        PDEBUG("Error! Invalid input parameters\n"
               "offset=0x%04x, p_value=%p, idx=%d, size=%d\n",
               offset, p_value, idx, size);
        AXISPI_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_axispi = (axispi_ctx_t *)p_priv;     
    offset += p_axispi->brdspec.offset;
    PDEBUG("p_axispi->brdspec.offset=0x%x, offset=0x%03x, value=0x%08x\n", 
           p_axispi->brdspec.offset, offset, *(b_u32 *)p_value);

    _ret = devspibus_write_buff(p_axispi->brdspec.devname, offset, idx, p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to write over spi bus\n");
        AXISPI_ERROR_RETURN(_ret);
    }

    return _ret;
}

static ccl_err_t _axispi_configure_test(void *p_priv)
{
    axispi_ctx_t *p_axispi; 

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXISPI_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_axispi = (axispi_ctx_t *)p_priv;

    return CCL_OK;
}

static ccl_err_t _axispi_run_test(void *p_priv, 
                                  __attribute__((unused)) b_u8 *buf, 
                                  __attribute__((unused)) b_u32 size)
{
    axispi_ctx_t *p_axispi; 

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXISPI_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_axispi = (axispi_ctx_t *)p_priv;

    return CCL_OK;
}

ccl_err_t axispi_attr_read(void *p_priv, b_u32 attrid, 
                           b_u32 offset, b_u32 offset_sz, 
                           b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || offset > AXISPI_MAX_OFFSET || 
         !p_value || idx || size != sizeof(b_u32) ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXISPI_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch ( attrid ) {
    case eAXISPI_IISR:
    case eAXISPI_IIER:
    case eAXISPI_CR:
    case eAXISPI_SR:
    case eAXISPI_DRR:
    case eAXISPI_SSR:
    case eAXISPI_TFO:
    case eAXISPI_RFO:
    case eAXISPI_DGIER:
        AXISPI_REG_READ(p_priv, offset, offset_sz, idx, p_value, size);
        break;
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK)
        PDEBUG("Failure in attribute id: %d\n", attrid);

    return _ret;               
}

ccl_err_t axispi_attr_write(void *p_priv, b_u32 attrid, 
                            b_u32 offset, b_u32 offset_sz, 
                            b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || offset > AXISPI_MAX_OFFSET || 
         !p_value || idx || size != sizeof(b_u32) ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXISPI_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch ( attrid ) {
    case eAXISPI_IISR:
    case eAXISPI_IIER:
    case eAXISPI_SRR:
    case eAXISPI_CR:
    case eAXISPI_DTR:
    case eAXISPI_SSR:
    case eAXISPI_DGIER:
        AXISPI_REG_WRITE(p_priv, offset, offset_sz, idx, p_value, size);
        break;
    case eAXISPI_INIT:
        _ret = _axispi_init(p_priv);
        break;
    case eAXISPI_CONFIGURE_TEST:
        _ret = _axispi_configure_test(p_priv);
        break;
    case eAXISPI_RUN_TEST:
        _ret = _axispi_run_test(p_priv, p_value, size);
        break;
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK)
        PDEBUG("Failure in attribute id: %d\n", attrid);

    return _ret;               
}

/* axispi device attributes definition */
static device_attr_t _attrs[] = {
    {
        .name = "name", 
        .type = eDEVMAN_ATTR_STRING, .maxnum = 1, 
        .offset = offsetof(struct axispi_ctx_t, brdspec.name),
        .size = DEVMAN_DEVNAME_LEN,
        .format = "%s"
    },
    {
        .name = "offset", 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset = offsetof(struct axispi_ctx_t, brdspec.offset),
        .format = "0x%08x"
    },
    {
        .name = "slaves_num", 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset = offsetof(struct axispi_ctx_t, brdspec.slaves_num),
        .format = "%d"
    },
    {
        .name = "slave_name", 
        .type = eDEVMAN_ATTR_STRING, .maxnum = DEVBRD_SPIDEVS_MAXNUM, 
        .offset = offsetof(struct axispi_ctx_t, brdspec.slave_name),
        .size = DEVBRD_SPIDEVS_NAMELEN,
    },
    {
        .name = "slave_cs", 
        .type = eDEVMAN_ATTR_WORD, .maxnum = DEVBRD_SPIDEVS_MAXNUM, 
        .offset = offsetof(struct axispi_ctx_t, brdspec.slave_cs),
        .format = "%d"
    },
    {
        .name = "isr", .id = eAXISPI_IISR, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXISPI_IISR
    },
    {
        .name = "ier", .id = eAXISPI_IIER, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXISPI_IIER
    },
    {
        .name = "srr", .id = eAXISPI_SRR, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXISPI_SRR,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "cr", .id = eAXISPI_CR, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXISPI_CR
    },
    {
        .name = "sr", .id = eAXISPI_SR, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXISPI_SR,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "dtr", .id = eAXISPI_DTR, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXISPI_DTR,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "drr", .id = eAXISPI_DRR, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXISPI_DRR,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "ssr", .id = eAXISPI_SSR, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXISPI_SSR
    },
    {
        .name = "tfo", .id = eAXISPI_TFO, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXISPI_TFO,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rfo", .id = eAXISPI_RFO, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXISPI_RFO,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "dgier", .id = eAXISPI_DGIER, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXISPI_DGIER
    },
    {
        .name = "init", .id = eAXISPI_INIT, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "config_test", .id = eAXISPI_CONFIGURE_TEST, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "run_test", .id = eAXISPI_RUN_TEST, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, 
        .size = sizeof(board_dev_stats_t), 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
};

/**
 * @brief Resets the SPI device by writing to the Software Reset
 *         register
 *
 * @param[in] p_axispi	is a pointer to the axispi_ctx_t 
 *       instance to be worked on
 *
 * @return	error code
 */
static ccl_err_t _axispi_reset(axispi_ctx_t *p_axispi)
{
    b_u32 regval;

    if ( !p_axispi ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXISPI_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* Reset the device. */
    regval = AXISPI_SRR_RESET_MASK;
    AXISPI_REG_WRITE(p_axispi, AXISPI_SRR, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));

    AXISPI_REG_READ(p_axispi, AXISPI_CR, 
                    sizeof(b_u32), 0, 
                    (b_u8 *)&regval, sizeof(b_u32));
    if (regval != AXISPI_CR_RESET_STATE) {
        PDEBUG("CR register value: 0x%x. It must be in reset state (0x%x)\n",
               regval, AXISPI_CR_RESET_STATE);
        AXISPI_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

/**
 *
 * @brief Set the contents of the Interrupt Enable
 *       Register.
 *
 * @param[in] p_axispi	is a pointer to the axispi_ctx_t 
 *       instance to be worked on
 * @param[in] mask is the bitmask of the interrupts to 
 *      be enabled
 *
 * @return 	CCL_OK on success, CCL_BAD_PARAM_ERR on failure
 */
static inline ccl_err_t _axispi_intr_enable(axispi_ctx_t *p_axispi, b_u32 mask)
{
    b_u32 regval;

    if ( !p_axispi ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXISPI_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    AXISPI_REG_READ(p_axispi, AXISPI_IIER, 
                    sizeof(b_u32), 0, 
                    (b_u8 *)&regval, sizeof(b_u32));
    regval |= (mask | AXISPI_INTR_ALL);
    AXISPI_REG_WRITE(p_axispi, AXISPI_IIER, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));       

    return CCL_OK;
}

/*****************************************************************************/
/**
*
* This function clears the specified interrupts in the Interrupt status
* Register. The interrupt is cleared by writing to this register with the bits
* to be cleared set to a one and all others bits to zero. Setting a bit which
* is zero within this register causes an interrupt to be generated.
*
* This function writes only the specified value to the register such that
* some status bits may be set and others cleared.  It is the caller's
* responsibility to get the value of the register prior to setting the value
* to prevent an destructive behavior.
*
* @param	InstancePtr is a pointer to the XSpi instance to be worked on.
* @param	ClearMask is the Bitmask for interrupts to be cleared.
*		Bit positions of "1" clears the interrupt. Bit positions of 0
*		will keep the previous setting. This mask is formed by OR'ing
*		XSP_INTR_* bits defined in xspi_l.h.
*
* @return	None.
*
* @note		C-Style signature:
*		void XSpi_IntrClear(XSpi *InstancePtr, u32 ClearMask);
*
******************************************************************************/
static inline ccl_err_t _axispi_intr_clear(axispi_ctx_t *p_axispi, b_u32 mask)
{
    b_u32 regval;

    if ( !p_axispi ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXISPI_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    AXISPI_REG_READ(p_axispi, AXISPI_IISR, 
                    sizeof(b_u32), 0, 
                    (b_u8 *)&regval, sizeof(b_u32));
    regval |= mask;
    AXISPI_REG_WRITE(p_axispi, AXISPI_IISR, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/**
* @brief Transfer the specified data on the SPI bus. If the SPI
* device is configured to be a master, this function initiates 
* bus communication and sends/receives the data to/from the 
* selected SPI slave. If the SPI device is configured to be a 
* slave, this function prepares the data to be sent/received 
* when selected by a master. For every byte sent, a byte is 
* received. 
*
* This function/driver operates in interrupt mode and polled mode.
*  - In interrupt mode this function is non-blocking and the transfer is
*    initiated by this function and completed by the interrupt service routine.
*  - In polled mode this function is blocking and the control exits this
*    function only after all the requested data is transferred.
*
* The caller has the option of providing two different buffers for send and
* receive, or one buffer for both send and receive, or no buffer for receive.
* The receive buffer must be at least as big as the send buffer to prevent
* unwanted memory writes. This implies that the byte count passed in as an
* argument must be the smaller of the two buffers if they differ in size.
* Here are some sample usages:
* <pre>
*   _axispi_transfer(sendbuf, recvbuf, length)
*	The caller wishes to send and receive, and provides two different
*	buffers for send and receive.
*
*	_axispi_transfer(sendbuf, recvbuf, length)
*	The caller wishes only to send and does not care about the received
*	data. The driver ignores the received data in this case.
*
*	_axispi_transfer(sendbuf, recvbuf, length)
*	The caller wishes to send and receive, but provides the same buffer
*	for doing both. The driver sends the data and overwrites the send
*	buffer with received data as it transfers the data.
*void
*	_axispi_transfer(sendbuf, recvbuf, length)
*	The caller wishes to only receive and does not care about sending
*	data.  In this case, the caller must still provide a send buffer, but
*	it can be the same as the receive buffer if the caller does not care
*	what it sends. The device must send N bytes of data if it wishes to
*	receive N bytes of data.
* </pre>
* In interrupt mode, though this function takes a buffer as an argument, the
* driver can only transfer a limited number of bytes at time. It transfers only
* one byte at a time if there are no FIFOs, or it can transfer the number of
* bytes up to the size of the FIFO if FIFOs exist.
*  - In interrupt mode a call to this function only starts the transfer, the
*    subsequent transfer of the data is performed by the interrupt service
*    routine until the entire buffer has been transferred.The status callback
*    function is called when the entire buffer has been sent/received.
*  - In polled mode this function is blocking and the control exits this
*    function only after all the requested data is transferred.
*
* As a master, the SetSlaveSelect function must be called prior to this
* function.
*  
* @param[in] p_spi_point is the slave device index on the spi 
*       bus
* @param[in] sendbuf is a pointer to a buffer of data which is 
*       to be sent. This buffer must not be NULL.
* @param[in] recvbuf is a pointer to a buffer which will be 
*       filled with received data. This argument can be NULL if
*       the caller does not wish to receive data.
* @param[in] length contains the number of bytes to 
*       send/receive. The number of bytes received always equals
*       the number of bytes sent.
*
* @return CCL_OK on success, error code on failure
*/
static ccl_err_t _axispi_transfer(spi_route_t *p_spi_point,
                                  b_u8 *sendbuf, b_u8 *recvbuf, 
                                  b_u32 length)
{
    static axispi_ctx_t  *p_axispi;
    b_u32 regval = 0, status = 0;
    b_bool globalint;
    b_u32 data = 0;
    b_u8  data_width = AXISPI_DATAWIDTH_BYTE;
    b_u8 *p_sendbuf = sendbuf;
    b_u8 *p_recvbuf = recvbuf;
    b_u32 rem_bytes = length;
    b_u32 trans_bytes;

    /*
     * The recvbuf argument can be NULL.
     */
    if ( !p_spi_point || !sendbuf || !length ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXISPI_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* Get the current AXI SPI context */
    p_axispi = _p_axispis[p_spi_point->dev_id];

    /*
     * Save the Global Interrupt Enable Register.
     */
    AXISPI_REG_READ(p_axispi, AXISPI_DGIER, 
                    sizeof(b_u32), 0, 
                    (b_u8 *)&regval, sizeof(b_u32));
    globalint = (regval == AXISPI_GINTR_ENABLE_MASK) ? CCL_TRUE : CCL_FALSE;

    /*
     * Enter a critical section from here to the end of the function since
     * state is modified, an interrupt is enabled, and the control register
     * is modified (r/m/w).
     */
    regval = 0;
    AXISPI_REG_WRITE(p_axispi, AXISPI_DGIER, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));

    /*
     * Fill the DTR/FIFO with as many bytes as it will take (or as many as
     * we have to send). We use the tx full status bit to know if the device
     * can take more data. By doing this, the driver does not need to know
     * the size of the FIFO or that there even is a FIFO. The downside is
     * that the status register must be read each loop iteration.
     */
    AXISPI_REG_READ(p_axispi, AXISPI_SR, 
                    sizeof(b_u32), 0, 
                    (b_u8 *)&status, sizeof(b_u32));

    while (((status & AXISPI_SR_TX_FULL_MASK) == 0) &&
           (rem_bytes > 0)) {
        if (data_width == AXISPI_DATAWIDTH_BYTE) {
            /*
             * data Transfer Width is Byte (8 bit).
             */
            data = *p_sendbuf;
        } else if (data_width == AXISPI_DATAWIDTH_HALF_WORD) {
            /*
             * data Transfer Width is Half Word (16 bit).
             */
            data = *(b_u16 *)p_sendbuf;
        } else if (data_width == AXISPI_DATAWIDTH_WORD) {
            /*
             * data Transfer Width is Word (32 bit).
             */
            data = *(b_u32 *)p_sendbuf;
        }

        AXISPI_REG_WRITE(p_axispi, AXISPI_DTR, 
                         sizeof(b_u32), 0, 
                         (b_u8 *)&data, sizeof(b_u32));
        p_sendbuf += (data_width >> 3);
        rem_bytes -= (data_width >> 3);
        AXISPI_REG_READ(p_axispi, AXISPI_SR, 
                        sizeof(b_u32), 0, 
                        (b_u8 *)&status, sizeof(b_u32));
    }

    /*
     * Set the slave select register to select the device on the SPI before
     * starting the transfer of data.
     */
    regval = ~(1UL << p_spi_point->slave_id);
    AXISPI_REG_WRITE(p_axispi, AXISPI_SSR, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));

    /*
     * Start the transfer by no longer inhibiting the transmitter and
     * enabling the device. For a master, this will in fact start the
     * transfer, but for a slave it only prepares the device for a transfer
     * that must be initiated by a master.
     */
    AXISPI_REG_READ(p_axispi, AXISPI_CR, 
                    sizeof(b_u32), 0, 
                    (b_u8 *)&regval, sizeof(b_u32));
    regval &= ~AXISPI_CR_TRANS_INHIBIT_MASK;
    AXISPI_REG_WRITE(p_axispi, AXISPI_CR, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));

    /*
     * If the interrupts are enabled as indicated by Global Interrupt
     * Enable Register, then enable the transmit empty interrupt to operate
     * in Interrupt mode of operation.
     */
    if (globalint == CCL_TRUE) { /* Interrupt Mode of operation */
        /*
         * Enable the transmit empty interrupt, which we use to
         * determine progress on the transmission.
         */
        _ret = _axispi_intr_enable(p_axispi, AXISPI_INTR_TX_EMPTY_MASK);
        if (_ret) {
            PDEBUG("_axispi_intr_enable error\n");
            AXISPI_ERROR_RETURN(_ret);
        }

        /*
         * End critical section.
         */
        regval = AXISPI_GINTR_ENABLE_MASK;
        AXISPI_REG_WRITE(p_axispi, AXISPI_DGIER, 
                         sizeof(b_u32), 0, 
                         (b_u8 *)&regval, sizeof(b_u32));

    } else { /* Polled mode of operation */
        /*
         * If interrupts are not enabled, poll the status register to
         * Transmit/Receive SPI data.
         */
        while (length > 0) {
            /*
             * Wait for the transfer to be done by polling the
             * Transmit empty status bit
             */
            do {
                AXISPI_REG_READ(p_axispi, AXISPI_SR, 
                                sizeof(b_u32), 0, 
                                (b_u8 *)&status, sizeof(b_u32));
            } while ((status & AXISPI_INTR_TX_EMPTY_MASK) == 0);

            _ret = _axispi_intr_clear(p_axispi, AXISPI_INTR_TX_EMPTY_MASK);
            if (_ret) {
                PDEBUG("_axispi_intr_clear error\n");
                AXISPI_ERROR_RETURN(_ret);
            }

            /*
             * A transmit has just completed. Process received data
             * and check for more data to transmit. Always inhibit
             * the transmitter while the transmit register/FIFO is
             * being filled, or make sure it is stopped if we're
             * done.
             */
            AXISPI_REG_READ(p_axispi, AXISPI_CR, 
                            sizeof(b_u32), 0, 
                            (b_u8 *)&regval, sizeof(b_u32));
            regval |= AXISPI_CR_TRANS_INHIBIT_MASK;
            AXISPI_REG_WRITE(p_axispi, AXISPI_CR, 
                             sizeof(b_u32), 0, 
                             (b_u8 *)&regval, sizeof(b_u32));

            /*
             * First get the data received as a result of the
             * transmit that just completed. We get all the data
             * available by reading the status register to determine
             * when the Receive register/FIFO is empty. Always get
             * the received data, but only fill the receive
             * buffer if it points to something (the upper layer
             * software may not care to receive data).
             */
            AXISPI_REG_READ(p_axispi, AXISPI_SR, 
                            sizeof(b_u32), 0, 
                            (b_u8 *)&status, sizeof(b_u32));

            while ((status & AXISPI_SR_RX_EMPTY_MASK) == 0) {
                AXISPI_REG_READ(p_axispi, AXISPI_DRR, 
                                sizeof(b_u32), 0, 
                                (b_u8 *)&data, sizeof(b_u32));
                if (data_width == AXISPI_DATAWIDTH_BYTE) {
                    /*
                     * data Transfer Width is Byte (8 bit).
                     */
                    if (p_recvbuf != NULL)
                        *p_recvbuf++ = (b_u8)data;
                } else if (data_width == AXISPI_DATAWIDTH_HALF_WORD) {
                    /*
                     * data Transfer Width is Half Word
                     * (16 bit).
                     */
                    if (p_recvbuf != NULL) {
                        *(b_u16 *)p_recvbuf = (b_u16)data;
                        p_recvbuf += 2;
                    }
                } else if (data_width == AXISPI_DATAWIDTH_WORD) {
                    /*
                     * data Transfer Width is Word (32 bit).
                     */
                    if (p_recvbuf != NULL) {
                        *(b_u32 *)p_recvbuf = data;
                        p_recvbuf += 4;
                    }
                }
                trans_bytes += (data_width >> 3);
                length -= (data_width >> 3);
                AXISPI_REG_READ(p_axispi, AXISPI_SR, 
                                sizeof(b_u32), 0, 
                                (b_u8 *)&status, sizeof(b_u32));
            }
            if (rem_bytes > 0) {
                /*
                 * Fill the DTR/FIFO with as many bytes as it
                 * will take (or as many as we have to send).
                 * We use the Tx full status bit to know if the
                 * device can take more data.
                 * By doing this, the driver does not need to
                 * know the size of the FIFO or that there even
                 * is a FIFO.
                 * The downside is that the status must be read
                 * each loop iteration.
                 */
                AXISPI_REG_READ(p_axispi, AXISPI_SR, 
                                sizeof(b_u32), 0, 
                                (b_u8 *)&status, sizeof(b_u32));

                while (((status & AXISPI_SR_TX_FULL_MASK) == 0) &&
                       (rem_bytes > 0)) {
                    if (data_width == AXISPI_DATAWIDTH_BYTE) {
                        /*
                         * data Transfer Width is Byte
                         * (8 bit).
                         */
                        data = *p_sendbuf;

                    } else if (data_width == AXISPI_DATAWIDTH_HALF_WORD) {

                        /*
                         * data Transfer Width is Half
                         * Word (16 bit).
                         */
                        data = *(b_u16 *)p_sendbuf;
                    } else if (data_width == AXISPI_DATAWIDTH_WORD) {
                        /*
                         * data Transfer Width is Word
                         * (32 bit).
                         */
                        data = *(b_u32 *)p_sendbuf;
                    }
                    AXISPI_REG_WRITE(p_axispi, AXISPI_DTR, 
                                     sizeof(b_u32), 0, 
                                     (b_u8 *)&data, sizeof(b_u32));
                    p_sendbuf += (data_width >> 3);
                    rem_bytes -= (data_width >> 3);
                    AXISPI_REG_READ(p_axispi, AXISPI_SR, 
                                    sizeof(b_u32), 0, 
                                    (b_u8 *)&status, sizeof(b_u32));
                }

                /*
                 * Start the transfer by not inhibiting the
                 * transmitter any longer.
                 */
                AXISPI_REG_READ(p_axispi, AXISPI_CR, 
                                sizeof(b_u32), 0, 
                                (b_u8 *)&regval, sizeof(b_u32));
                regval &= ~AXISPI_CR_TRANS_INHIBIT_MASK;
                AXISPI_REG_WRITE(p_axispi, AXISPI_CR, 
                                 sizeof(b_u32), 0, 
                                 (b_u8 *)&regval, sizeof(b_u32));
            }
        }

        /*
         * Stop the transfer (hold off automatic sending) by inhibiting
         * the transmitter.
         */
        AXISPI_REG_READ(p_axispi, AXISPI_CR, 
                        sizeof(b_u32), 0, 
                        (b_u8 *)&regval, sizeof(b_u32));
        regval |= AXISPI_CR_TRANS_INHIBIT_MASK;
        AXISPI_REG_WRITE(p_axispi, AXISPI_CR, 
                         sizeof(b_u32), 0, 
                         (b_u8 *)&regval, sizeof(b_u32));

        /*
         * Deassert slave select register;
         */
        regval = 0xffffffff;
        AXISPI_REG_WRITE(p_axispi, AXISPI_SSR, 
                         sizeof(b_u32), 0, 
                         (b_u8 *)&regval, sizeof(b_u32));
    }

    return _ret;
}

static ccl_err_t _axispi_transfer_burst(spi_route_t *p_spi_point,
                                        b_u8 *sendbuf, b_u8 *recvbuf, 
                                        b_u32 burst_l, b_u32 length)
{
    static axispi_ctx_t  *p_axispi;
    b_u32 regval = 0, status = 0;
    b_bool globalint;
    b_u32 data = 0;
    b_u8  data_width = AXISPI_DATAWIDTH_BYTE;
    b_u8 *p_sendbuf = sendbuf;
    b_u8 *p_recvbuf = recvbuf;
    b_u32 rem_bytes = length;
    b_u32 trans_bytes;

    /*
     * The recvbuf argument can be NULL.
     */
    if ( !p_spi_point || !sendbuf || !length ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXISPI_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* Get the current AXI SPI context */
    p_axispi = _p_axispis[p_spi_point->dev_id];

    /*
     * Save the Global Interrupt Enable Register.
     */
    AXISPI_REG_READ(p_axispi, AXISPI_DGIER, 
                    sizeof(b_u32), 0, 
                    (b_u8 *)&regval, sizeof(b_u32));
    globalint = (regval == AXISPI_GINTR_ENABLE_MASK) ? CCL_TRUE : CCL_FALSE;

    /*
     * Enter a critical section from here to the end of the function since
     * state is modified, an interrupt is enabled, and the control register
     * is modified (r/m/w).
     */
    regval = 0;
    AXISPI_REG_WRITE(p_axispi, AXISPI_DGIER, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));

    /*
     * Fill the DTR/FIFO with as many bytes as it will take (or as many as
     * we have to send). We use the tx full status bit to know if the device
     * can take more data. By doing this, the driver does not need to know
     * the size of the FIFO or that there even is a FIFO. The downside is
     * that the status register must be read each loop iteration.
     */
    AXISPI_REG_READ(p_axispi, AXISPI_SR, 
                    sizeof(b_u32), 0, 
                    (b_u8 *)&status, sizeof(b_u32));

    while (((status & AXISPI_SR_TX_FULL_MASK) == 0) &&
           (rem_bytes > 0)) {
        if (data_width == AXISPI_DATAWIDTH_BYTE) {
            /*
             * data Transfer Width is Byte (8 bit).
             */
            data = *p_sendbuf;
        } else if (data_width == AXISPI_DATAWIDTH_HALF_WORD) {
            /*
             * data Transfer Width is Half Word (16 bit).
             */
            data = *(b_u16 *)p_sendbuf;
        } else if (data_width == AXISPI_DATAWIDTH_WORD) {
            /*
             * data Transfer Width is Word (32 bit).
             */
            data = *(b_u32 *)p_sendbuf;
        }

        AXISPI_REG_WRITE(p_axispi, AXISPI_DTR, 
                         sizeof(b_u32), 0, 
                         (b_u8 *)&data, sizeof(b_u32));
        p_sendbuf += (data_width >> 3);
        rem_bytes -= (data_width >> 3);
        AXISPI_REG_READ(p_axispi, AXISPI_SR, 
                        sizeof(b_u32), 0, 
                        (b_u8 *)&status, sizeof(b_u32));
    }

    /*
     * Set the slave select register to select the device on the SPI before
     * starting the transfer of data.
     */
    regval = ~(1UL << p_spi_point->slave_id);
    AXISPI_REG_WRITE(p_axispi, AXISPI_SSR, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));

    /*
     * Start the transfer by no longer inhibiting the transmitter and
     * enabling the device. For a master, this will in fact start the
     * transfer, but for a slave it only prepares the device for a transfer
     * that must be initiated by a master.
     */
    AXISPI_REG_READ(p_axispi, AXISPI_CR, 
                    sizeof(b_u32), 0, 
                    (b_u8 *)&regval, sizeof(b_u32));
    regval &= ~AXISPI_CR_TRANS_INHIBIT_MASK;
    AXISPI_REG_WRITE(p_axispi, AXISPI_CR, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));

    /*
     * If the interrupts are enabled as indicated by Global Interrupt
     * Enable Register, then enable the transmit empty interrupt to operate
     * in Interrupt mode of operation.
     */
    if (globalint == CCL_TRUE) { /* Interrupt Mode of operation */
        /*
         * Enable the transmit empty interrupt, which we use to
         * determine progress on the transmission.
         */
        _ret = _axispi_intr_enable(p_axispi, AXISPI_INTR_TX_EMPTY_MASK);
        if (_ret) {
            PDEBUG("_axispi_intr_enable error\n");
            AXISPI_ERROR_RETURN(_ret);
        }

        /*
         * End critical section.
         */
        regval = AXISPI_GINTR_ENABLE_MASK;
        AXISPI_REG_WRITE(p_axispi, AXISPI_DGIER, 
                         sizeof(b_u32), 0, 
                         (b_u8 *)&regval, sizeof(b_u32));

    } else { /* Polled mode of operation */
        /*
         * If interrupts are not enabled, poll the status register to
         * Transmit/Receive SPI data.
         */
        while (length > 0) {
            /*
             * Wait for the transfer to be done by polling the
             * Transmit empty status bit
             */
            do {
                AXISPI_REG_READ(p_axispi, AXISPI_SR, 
                                sizeof(b_u32), 0, 
                                (b_u8 *)&status, sizeof(b_u32));
            } while ((status & AXISPI_INTR_TX_EMPTY_MASK) == 0);

            _ret = _axispi_intr_clear(p_axispi, AXISPI_INTR_TX_EMPTY_MASK);
            if (_ret) {
                PDEBUG("_axispi_intr_clear error\n");
                AXISPI_ERROR_RETURN(_ret);
            }

            /*
             * A transmit has just completed. Process received data
             * and check for more data to transmit. Always inhibit
             * the transmitter while the transmit register/FIFO is
             * being filled, or make sure it is stopped if we're
             * done.
             */
            AXISPI_REG_READ(p_axispi, AXISPI_CR, 
                            sizeof(b_u32), 0, 
                            (b_u8 *)&regval, sizeof(b_u32));
            regval |= AXISPI_CR_TRANS_INHIBIT_MASK;
            AXISPI_REG_WRITE(p_axispi, AXISPI_CR, 
                             sizeof(b_u32), 0, 
                             (b_u8 *)&regval, sizeof(b_u32));

            /*
             * First get the data received as a result of the
             * transmit that just completed. We get all the data
             * available by reading the status register to determine
             * when the Receive register/FIFO is empty. Always get
             * the received data, but only fill the receive
             * buffer if it points to something (the upper layer
             * software may not care to receive data).
             */
            AXISPI_REG_READ(p_axispi, AXISPI_SR, 
                            sizeof(b_u32), 0, 
                            (b_u8 *)&status, sizeof(b_u32));

            while ((status & AXISPI_SR_RX_EMPTY_MASK) == 0) {
                AXISPI_REG_READ(p_axispi, AXISPI_DRR, 
                                sizeof(b_u32), 0, 
                                (b_u8 *)&data, sizeof(b_u32));
                if (data_width == AXISPI_DATAWIDTH_BYTE) {
                    /*
                     * data Transfer Width is Byte (8 bit).
                     */
                    if (p_recvbuf != NULL)
                        *p_recvbuf++ = (b_u8)data;
                } else if (data_width == AXISPI_DATAWIDTH_HALF_WORD) {
                    /*
                     * data Transfer Width is Half Word
                     * (16 bit).
                     */
                    if (p_recvbuf != NULL) {
                        *(b_u16 *)p_recvbuf = (b_u16)data;
                        p_recvbuf += 2;
                    }
                } else if (data_width == AXISPI_DATAWIDTH_WORD) {
                    /*
                     * data Transfer Width is Word (32 bit).
                     */
                    if (p_recvbuf != NULL) {
                        *(b_u32 *)p_recvbuf = data;
                        p_recvbuf += 4;
                    }
                }
                trans_bytes += (data_width >> 3);
                length -= (data_width >> 3);
                AXISPI_REG_READ(p_axispi, AXISPI_SR, 
                                sizeof(b_u32), 0, 
                                (b_u8 *)&status, sizeof(b_u32));
            }
            if (rem_bytes > 0) {
                /*
                 * Fill the DTR/FIFO with as many bytes as it
                 * will take (or as many as we have to send).
                 * We use the Tx full status bit to know if the
                 * device can take more data.
                 * By doing this, the driver does not need to
                 * know the size of the FIFO or that there even
                 * is a FIFO.
                 * The downside is that the status must be read
                 * each loop iteration.
                 */
                AXISPI_REG_READ(p_axispi, AXISPI_SR, 
                                sizeof(b_u32), 0, 
                                (b_u8 *)&status, sizeof(b_u32));

                while (((status & AXISPI_SR_TX_FULL_MASK) == 0) &&
                       (rem_bytes > 0)) {
                    if (data_width == AXISPI_DATAWIDTH_BYTE) {
                        /*
                         * data Transfer Width is Byte
                         * (8 bit).
                         */
                        data = *p_sendbuf;

                    } else if (data_width == AXISPI_DATAWIDTH_HALF_WORD) {

                        /*
                         * data Transfer Width is Half
                         * Word (16 bit).
                         */
                        data = *(b_u16 *)p_sendbuf;
                    } else if (data_width == AXISPI_DATAWIDTH_WORD) {
                        /*
                         * data Transfer Width is Word
                         * (32 bit).
                         */
                        data = *(b_u32 *)p_sendbuf;
                    }
                    AXISPI_REG_WRITE(p_axispi, AXISPI_DTR, 
                                     sizeof(b_u32), 0, 
                                     (b_u8 *)&data, sizeof(b_u32));
                    p_sendbuf += (data_width >> 3);
                    rem_bytes -= (data_width >> 3);
                    AXISPI_REG_READ(p_axispi, AXISPI_SR, 
                                    sizeof(b_u32), 0, 
                                    (b_u8 *)&status, sizeof(b_u32));
                }

                /*
                 * Start the transfer by not inhibiting the
                 * transmitter any longer.
                 */
                AXISPI_REG_READ(p_axispi, AXISPI_CR, 
                                sizeof(b_u32), 0, 
                                (b_u8 *)&regval, sizeof(b_u32));
                regval &= ~AXISPI_CR_TRANS_INHIBIT_MASK;
                AXISPI_REG_WRITE(p_axispi, AXISPI_CR, 
                                 sizeof(b_u32), 0, 
                                 (b_u8 *)&regval, sizeof(b_u32));
            }
        }

        /*
         * Stop the transfer (hold off automatic sending) by inhibiting
         * the transmitter.
         */
        AXISPI_REG_READ(p_axispi, AXISPI_CR, 
                        sizeof(b_u32), 0, 
                        (b_u8 *)&regval, sizeof(b_u32));
        regval |= AXISPI_CR_TRANS_INHIBIT_MASK;
        AXISPI_REG_WRITE(p_axispi, AXISPI_CR, 
                         sizeof(b_u32), 0, 
                         (b_u8 *)&regval, sizeof(b_u32));

        /*
         * Deassert slave select register;
         */
        regval = 0xffffffff;
        AXISPI_REG_WRITE(p_axispi, AXISPI_SSR, 
                         sizeof(b_u32), 0, 
                         (b_u8 *)&regval, sizeof(b_u32));
    }

    return _ret;
}

ccl_err_t devaxispi_read_buff(spi_route_t *p_spi_point, b_u8 *sendbuf, 
                              b_u8 *recvbuf, b_u32 length)
{
    if ( !p_spi_point || !sendbuf || !recvbuf) {
        PDEBUG("Error! Invalid input parameters\n");
        AXISPI_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* write buffer down */
    _ret = _axispi_transfer(p_spi_point, sendbuf, recvbuf, length);
    if ( _ret ) {
        PDEBUG("Error! Can't write word\n");
        AXISPI_ERROR_RETURN(_ret);
    }

    return _ret;
}

ccl_err_t devaxispi_write_buff(spi_route_t *p_spi_point, b_u8 *sendbuf, 
                               b_u8 *recvbuf, b_u32 length)
{
    if ( !p_spi_point || !sendbuf ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXISPI_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* write buffer down */
    _ret = _axispi_transfer(p_spi_point, sendbuf, NULL, length);
    if ( _ret ) {
        PDEBUG("Error! Can't write word\n");
        AXISPI_ERROR_RETURN(_ret);
    }

    return _ret;
}

ccl_err_t devaxispi_write_burst(spi_route_t *p_spi_point, b_u8 *sendbuf, 
                                b_u8 *recvbuf, b_u32 burst_l, b_u32 length)
{
    if ( !p_spi_point || !sendbuf ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXISPI_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* write buffer down */
    _ret = _axispi_transfer_burst(p_spi_point, sendbuf, NULL, burst_l, length);
    if ( _ret ) {
        PDEBUG("Error! Can't write word\n");
        AXISPI_ERROR_RETURN(_ret);
    }

    return _ret;
}

/**
 * @brief Enable interrupts for the SPI device. If the Spi driver is used
 * in interrupt mode, it is up to the user to connect the SPI interrupt handler
 * to the interrupt controller before this function is called. If the Spi driver
 * is used in polled mode the user has to disable the Global Interrupts after
 * this function is called. If the device is configured with FIFOs, the FIFOs are
 * reset at this time.
 *
 * @param[in] p_axispi is a pointer to the axispi_ctx_t instance
 *       to be worked on
 *
 * @return 	CCL_OK on success, CCL_BAD_PARAM_ERR on failure
 */
static ccl_err_t _axispi_start(axispi_ctx_t *p_axispi)
{
    b_u32 regval;

    if ( !p_axispi ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXISPI_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Enable the interrupts.
     */
    _ret = _axispi_intr_enable(p_axispi, AXISPI_INTR_DFT_MASK);
    if (_ret) {
        PDEBUG("_axispi_intr_enable error\n");
        AXISPI_ERROR_RETURN(_ret);
    }

    /*
     * Reset the transmit and receive FIFOs if present. There is a critical
     * section here since this register is also modified during interrupt
     * context. So we wait until after the r/m/w of the control register to
     * enable the Global Interrupt Enable.
     */
    AXISPI_REG_READ(p_axispi, AXISPI_CR, 
                    sizeof(b_u32), 0, 
                    (b_u8 *)&regval, sizeof(b_u32));
    regval |= AXISPI_CR_MASTER_MODE_MASK | 
              AXISPI_CR_TXFIFO_RESET_MASK | 
              AXISPI_CR_RXFIFO_RESET_MASK | 
              AXISPI_CR_ENABLE_MASK;
    AXISPI_REG_WRITE(p_axispi, AXISPI_CR, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));

    /*
     * Update the Global Interrupt Enable just after we start.
     */
    if ( p_axispi->brdspec.op_mode == eDEVBRD_OP_MODE_POLL ) 
        regval = 0;
    else 
        regval = AXISPI_GINTR_ENABLE_MASK;

    AXISPI_REG_WRITE(p_axispi, AXISPI_DGIER, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

static ccl_err_t _axispi_init(void *p_priv)
{
    axispi_ctx_t *p_axispi;
    b_u32 regval;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXISPI_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_axispi = (axispi_ctx_t *)p_priv;

    _ret = _axispi_reset(p_axispi);
    if (_ret) {
        PDEBUG("_axispi_reset error\n");
        AXISPI_ERROR_RETURN(_ret);
    }
#if 0
    /*
     * Set the Spi device as a master
     */
    AXISPI_REG_READ(p_axispi, AXISPI_CR, 
                    sizeof(b_u32), 0, 
                    (b_u8 *)&regval, sizeof(b_u32));
    regval |= AXISPI_MASTER_OPTION;
    AXISPI_REG_WRITE(p_axispi, AXISPI_CR, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));
#endif
    _ret = _axispi_start(p_axispi);
    if (_ret) {
        PDEBUG("_axispi_start error\n");
        AXISPI_ERROR_RETURN(_ret);
    }

    /*
     * Disable Global interrupt to use polled mode operation
     */
    regval = 0;
    AXISPI_REG_WRITE(p_axispi, AXISPI_DGIER, 
                     sizeof(b_u32), 0, 
                     (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

static ccl_err_t _faxispi_cmd_reset(devo_t devo, 
                                    __attribute__((unused)) device_cmd_parm_t parms[], 
                                    __attribute__((unused)) b_u16 parms_num)
{
    b_u32 regval;
    axispi_ctx_t *p_axispi = devman_device_private(devo);
PDEBUG("p_axispi->brdspec.offset=0x%x\n", p_axispi->brdspec.offset);
    /* reset the device */
    _ret = _axispi_reset(p_axispi);
    if ( _ret ) {
        PDEBUG("_axispi_reset error\n");
        AXISPI_ERROR_RETURN(_ret);
    }

    PDEBUG("line: %d\n", __LINE__);
    return CCL_OK;
}

static ccl_err_t _faxispi_cmd_start(devo_t devo, 
                                    __attribute__((unused)) device_cmd_parm_t parms[], 
                                    __attribute__((unused)) b_u16 parms_num)
{
    axispi_ctx_t *p_axispi = devman_device_private(devo);

    /* start the device */
    _ret = _axispi_start(p_axispi);
    if ( _ret ) {
        PDEBUG("_axispi_start error\n");
        AXISPI_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

static ccl_err_t _faxispi_cmd_init(devo_t devo, 
                                   __attribute__((unused)) device_cmd_parm_t parms[], 
                                   __attribute__((unused)) b_u16 parms_num)
{
    void *p_priv = devman_device_private(devo);

    /* init the device */
    _ret = _axispi_init(p_priv);
    if ( _ret ) {
        PDEBUG("_axispi_init error\n");
        AXISPI_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

/* device commands */
static device_cmd_t _cmds[] = 
{
    { 
        .name = "reset", .f_cmd = _faxispi_cmd_reset, .parms_num = 0,
        .help = "Reset AXI SPI device" 
    },
    { 
        .name = "start", .f_cmd = _faxispi_cmd_start, .parms_num = 0,
        .help = "Start AXI SPI device" 
    },
    { 
        .name = "init", .f_cmd = _faxispi_cmd_init, .parms_num = 0,
        .help = "Init AXI SPI device" 
    },
};

/* Device instance hw-specific initializations 
 */
static ccl_err_t _faxispi_add(void *devo, void *brdspec) 
{
    axispi_ctx_t       *p_axispi;
    axispi_brdspec_t   *p_brdspec;
    b_u32               i;

    if ( !devo || !brdspec ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXISPI_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_axispi = devman_device_private(devo);
    if ( !p_axispi ) {
        PDEBUG("NULL context area\n");
        AXISPI_ERROR_RETURN(CCL_FAIL);
    }

    p_brdspec = (axispi_brdspec_t *)brdspec;
    /* back reference the device manager object handle */    
    p_axispi->devo = devo;    
    /* save the board specific info */
    memcpy(&p_axispi->brdspec, p_brdspec, sizeof(axispi_brdspec_t));

    /* store the context */
    _p_axispis[p_brdspec->dev_id] = p_axispi;

    return CCL_OK;
}

/* Device instance hw-specific deinitializations 
 */
static ccl_err_t _faxispi_cut(void *devo)
{
    axispi_ctx_t  *p_axispi;
    if ( !devo ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXISPI_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_axispi = devman_device_private(devo);
    if ( !p_axispi ) {
        PDEBUG("NULL context area\n");
        AXISPI_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

/* device type description structure */
static device_driver_t _axispi_driver = {
    .name = "axispi",
    .f_add = _faxispi_add,
    .f_cut = _faxispi_cut,
    .attrs = _attrs,
    .attrs_num = sizeof(_attrs)/sizeof(_attrs[0]),
    .cmds = _cmds,
    .cmds_num = sizeof(_cmds)/sizeof(_cmds[0]),
    .priv_size = sizeof(axispi_ctx_t)
}; 

/* Initialize AXISPI device type
 */
ccl_err_t devaxispi_init(void)
{
    /* register device type */
    _ret = devman_driver_register(&_axispi_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to register driver\n");
        AXISPI_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}

/* Deinitialize AXISPI device type
 */
ccl_err_t devaxispi_fini(void)
{
    /* unregister device type */
    _ret = devman_driver_unregister(&_axispi_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to unregister driver\n");
        AXISPI_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}
