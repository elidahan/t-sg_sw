#ifndef _DEV_EEPROM_H_
#define _DEV_EEPROM_H_

/**
 * @name Enumerator to specify I2C IO Expander EEPROM device 
 *       attributes
 */
enum eeprom_attr_e {
	eEEPROM_BOARD_SN,         /**< Board Serial Number */
	eEEPROM_ASSEMBLY_NUM,     /**< Assembly Number */
	eEEPROM_HW_REV,           /**< Hardware Revision */
	eEEPROM_HW_SUB_REV,       /**< Hardware Subrevision */
	eEEPROM_PART_NUM,         /**< Part Number */
	eEEPROM_CLEI,             /**< CLEI */
	eEEPROM_MFG_DATE,         /**< Manufacturing Date */
    eEEPROM_LICENSE,          /**< License value */
    eEEPROM_DEV_TYPE_ID,      /**< Device type ID */
    eEEPROM_BUFFER,           /**< Raw buffer */
    eEEPROM_INIT,
    eEEPROM_CONFIGURE_TEST,
    eEEPROM_RUN_TEST,
};

/* APIs */
/**
* @brief Read EEPROM EEPROM device attribute
*
* @param[in] p_priv points to the private context of the device
* @param[in] attrid id of the attribute
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] idx attribute's index in case the attribute id corresponds to array
* @param[out] p_value pointer to the buffer to hold returned attribute's value
* @param[in] size indicates size of the attribute
*
* @return	error code
*/
ccl_err_t eeprom_attr_read(void *p_priv, b_u32 attrid, 
                             b_u32 offset, b_u32 offset_sz, 
                             b_u32 idx, b_u8 *p_value, b_u32 size);

/**
* @brief Write EEPROM EEPROM device attribute
*
* @param[in] p_priv points to the private context of the device
* @param[in] attrid id of the attribute
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] idx attribute's index in case the attribute id corresponds to array
* @param[in] p_value pointer to the buffer which holds written 
*            data value
* @param[in] size indicates size of the attribute
*
* @return	error code
*/
ccl_err_t eeprom_attr_write(void *p_priv, b_u32 attrid, 
                              b_u32 offset, b_u32 offset_sz, 
                              b_u32 idx, b_u8 *p_value, b_u32 size);

#endif/* _DEV_EEPROM_H_ */
