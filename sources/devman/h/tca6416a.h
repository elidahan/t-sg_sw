#ifndef _DEV_TCA6416A_H_
#define _DEV_TCA6416A_H_

/**
 * @name Enumerator to specify I2C IO Expander TCA6416A device 
 *       attributes
 */
enum tca6416a_attr_e {
    eTCA6416A_INPUT_PORT_0,
    eTCA6416A_INPUT_PORT_1,
    eTCA6416A_OUTPUT_PORT_0,
    eTCA6416A_OUTPUT_PORT_1,
    eTCA6416A_POLARITY_INV_PORT_0,
    eTCA6416A_POLARITY_INV_PORT_1,
    eTCA6416A_CONFIG_0,
    eTCA6416A_CONFIG_1,
    eTCA6416A_INIT,
    eTCA6416A_CONFIGURE_TEST,
    eTCA6416A_RUN_TEST,
};

/* APIs */
/**
* @brief Read I2C IO Expander TCA6416A device attribute
*
* @param[in] p_priv points to the private context of the device
* @param[in] attrid id of the attribute
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] idx attribute's index in case the attribute id corresponds to array
* @param[out] p_value pointer to the buffer to hold returned attribute's value
* @param[in] size indicates size of the attribute
*
* @return	error code
*/
ccl_err_t tca6416a_attr_read(void *p_priv, b_u32 attrid, 
                             b_u32 offset, b_u32 offset_sz, 
                             b_u32 idx, b_u8 *p_value, b_u32 size);

/**
* @brief Write I2C IO Expander TCA6416A device attribute
*
* @param[in] p_priv points to the private context of the device
* @param[in] attrid id of the attribute
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] idx attribute's index in case the attribute id corresponds to array
* @param[in] p_value pointer to the buffer which holds written 
*            data value
* @param[in] size indicates size of the attribute
*
* @return	error code
*/
ccl_err_t tca6416a_attr_write(void *p_priv, b_u32 attrid, 
                              b_u32 offset, b_u32 offset_sz, 
                              b_u32 idx, b_u8 *p_value, b_u32 size);

#endif/* _DEV_TCA6416A_H_ */
