#ifndef _TSG_H_
#define _TSG_H_

/* System modules offsets definitions */

/* ARTIX */
#define INTC_ARTIX_0_OFFSET                    0x00000000
#define ETHERNET_SWITCH_OFFSET                 0x00002000
#define XADC_OFFSET                            0x00004000
#define I2CCORE_ATP_OFFSET                     0x00006000
#define I2CCORE_SYSTEM_OFFSET                  0x00008000
#define AXISPI_VIRTEX_LOAD_OFFSET              0x0000A000
#define AXISPI_ATP_OFFSET                      0x0000C000
#define AXISPI_VIRTEX_OFFSET                   0x0000E000
#define AXISPI_FLASH_OFFSET                    0x00010000
#define AXIUART_COMX0_OFFSET                   0x00012000
#define AXIUART_COMX1_OFFSET                   0x00014000
#define AXIUART_COMX2_OFFSET                   0x00016000
#define AXIUART_COMX3_OFFSET                   0x00018000
#define AXIUART_ATP_OFFSET                     0x0001A000
#define AXIUART_SMARC_MAIN_OFFSET              0x0001C000
#define AXIUART_SMARC_CTRL_OFFSET              0x0001E000
#define AXIUART_CLI_OFFSET                     0x00020000
#define AXIUART_SMART_CARD_OFFSET              0x00022000
#define AXIGPIO_CPU_CTRL_OFFSET                0x00024000
#define AXIGPIO_VIRTEX_CTRL_OFFSET             0x00026000
#define AXIGPIO_SMART_CARD_CTRL_OFFSET         0x00028000
#define AXIGPIO_FLASH_MUX_CTRL_OFFSET          0x0002A000
#define AXIGPIO_ATP_CTRL_OFFSET                0x0002C000
#define AXIGPIO_POWER_RESET_CTRL_OFFSET        0x0002E000
#define AXIGPIO_COMX_CTRL_OFFSET               0x00030000
#define AXIGPIO_PANEL_CTRL_OFFSET              0x00032000
#define AXIGPIO_ARTIX_GENERAL_CTRL_OFFSET      0x00034000
#define PORT_1G_0_ARTIX_VIRTEX_OFFSET          0x00036000
#define PORT_1G_1_ARTIX_VIRTEX_OFFSET          0x00038000
#define PORT_1G_0_ARTIX_SMARC_MAIN_OFFSET      0x0003A000
#define PORT_1G_1_ARTIX_SMARC_MAIN_OFFSET      0x0003C000
#define PORT_1G_0_ARTIX_SMARC_CTRL_OFFSET      0x0003E000
#define PORT_1G_1_ARTIX_SMARC_CTRL_OFFSET      0x00040000
#define PORT_1G_ARTIX_COMX0_OFFSET             0x00042000
#define PORT_1G_ARTIX_COMX1_OFFSET             0x00044000
#define PORT_1G_ARTIX_COMX2_OFFSET             0x00046000
#define PORT_1G_ARTIX_COMX3_OFFSET             0x00048000
#define PORT_1G_ARTIX_ATP_OFFSET               0x0004A000
#define PORT_1G_ARTIX_FRONT_PANEL_OFFSET       0x0004C000
#define INTC_ARTIX_1_OFFSET                    0x0004E000
#define AXIGPIO_TRAFFIC_GEN_1G_OFFSET          0x00050000
#define AXIGPIO_ARTIX_DATE_TIME_OFFSET         0x00056000
#define BITSTREAM_BUFFER_OFFSET                0x00060000
#define ESWITCH_OFFSET                         0x00070100 
/* VIRTEX */
#define PORT_10G_0_CLIENT_OFFSET               0x00400000     
#define PORT_10G_1_CLIENT_OFFSET               0x00410000    
#define PORT_10G_2_CLIENT_OFFSET               0x00420000    
#define PORT_10G_3_CLIENT_OFFSET               0x00430000    
#define PORT_10G_4_CLIENT_OFFSET               0x00440000    
#define PORT_10G_5_CLIENT_OFFSET               0x00450000    
#define PORT_10G_6_CLIENT_OFFSET               0x00460000    
#define PORT_10G_7_CLIENT_OFFSET               0x00470000    
#define PORT_10G_8_CLIENT_OFFSET               0x00480000    
#define PORT_10G_9_CLIENT_OFFSET               0x00490000    
#define PORT_10G_10_CLIENT_OFFSET              0x004A0000   
#define PORT_10G_11_CLIENT_OFFSET              0x004B0000   
#define PORT_KR_COMX0_0_OFFSET                 0x004C0000
#define PORT_KR_COMX0_1_OFFSET                 0x004D0000
#define PORT_KR_COMX0_2_OFFSET                 0x004E0000
#define PORT_KR_COMX0_3_OFFSET                 0x004F0000
#define PORT_KR_COMX1_0_OFFSET                 0x00500000
#define PORT_KR_COMX1_1_OFFSET                 0x00510000
#define PORT_KR_COMX1_2_OFFSET                 0x00520000
#define PORT_KR_COMX1_3_OFFSET                 0x00530000
#define PORT_100G_0_CLIENT_OFFSET              0x00550000   
#define PORT_100G_1_CLIENT_OFFSET              0x00540000   
#define I2CCORE_TRANS_CLIENT_OFFSET            0x00570000   
#define AXIGPIO_TRANS_CTRL_CLIENT_OFFSET       0x00572000   
#define AXIGPIO_TRAFFIC_GEN_CLIENT_OFFSET      0x00574000   
#define PORT_10G_0_NETWORK_OFFSET              0x00580000   
#define PORT_10G_1_NETWORK_OFFSET              0x00590000   
#define PORT_10G_2_NETWORK_OFFSET              0x005A0000   
#define PORT_10G_3_NETWORK_OFFSET              0x005B0000   
#define PORT_10G_4_NETWORK_OFFSET              0x005C0000   
#define PORT_10G_5_NETWORK_OFFSET              0x005D0000   
#define PORT_10G_6_NETWORK_OFFSET              0x005E0000   
#define PORT_10G_7_NETWORK_OFFSET              0x005F0000   
#define PORT_10G_8_NETWORK_OFFSET              0x00600000   
#define PORT_10G_9_NETWORK_OFFSET              0x00610000   
#define PORT_10G_10_NETWORK_OFFSET             0x00620000   
#define PORT_10G_11_NETWORK_OFFSET             0x00630000   
#define PORT_KR_COMX2_0_OFFSET                 0x00640000
#define PORT_KR_COMX2_1_OFFSET                 0x00650000
#define PORT_KR_COMX2_2_OFFSET                 0x00660000
#define PORT_KR_COMX2_3_OFFSET                 0x00670000
#define PORT_KR_COMX3_0_OFFSET                 0x00680000
#define PORT_KR_COMX3_1_OFFSET                 0x00690000
#define PORT_KR_COMX3_2_OFFSET                 0x006A0000
#define PORT_KR_COMX3_3_OFFSET                 0x006B0000
#define PORT_100G_0_NETWORK_OFFSET             0x006D0000   
#define PORT_100G_1_NETWORK_OFFSET             0x006C0000   
#define I2CCORE_TRANS_NETWORK_OFFSET           0x006F0000   
#define AXIGPIO_TRANS_CTRL_NETWORK_OFFSET      0x006F2000   
#define AXIGPIO_TRAFFIC_GEN_NETWORK_OFFSET     0x006F4000   
#define AXIGPIO_VIRTEX_GENERAL_CTRL_OFFSET     0x00700000
#define I2CCORE_SODIMM_OFFSET                  0x00702000
#define AXIGPIO_DDR4_USER_TEST_CTRL_OFFSET     0x00704000
#define DDR4_BANK_A_OFFSET                     0x00500000 
#define DDR4_BANK_B_OFFSET                     0x00520000
#define DDR4_BANK_C_OFFSET                     0x00540000
#define PORT_1G_0_VIRTEX_ARTIX_OFFSET          0x00560000
#define PORT_1G_1_VIRTEX_ARTIX_OFFSET          0x006E0000
#define PORT_1G_2_VIRTEX_ARTIX_OFFSET          0x00420000
#define INTC_VIRTEX_OFFSET                     0x00714000
#define SYSMON_VIRTEX_OFFSET                   0x00715000
#define AXIGPIO_VIRTEX_DATE_TIME_OFFSET        0x00717000




#define PORT_10G_0_NETWORK_USER_CTL_OFFSET        0x0070A200
#define PORT_10G_1_NETWORK_USER_CTL_OFFSET        0x0070A300
#define PORT_10G_2_NETWORK_USER_CTL_OFFSET        0x0070B000
#define PORT_10G_3_NETWORK_USER_CTL_OFFSET        0x0070B100
#define PORT_10G_4_NETWORK_USER_CTL_OFFSET        0x0070B200
#define PORT_10G_5_NETWORK_USER_CTL_OFFSET        0x0070B300
#define PORT_10G_6_NETWORK_USER_CTL_OFFSET        0x00707200
#define PORT_10G_7_NETWORK_USER_CTL_OFFSET        0x00707300
#define PORT_10G_8_NETWORK_USER_CTL_OFFSET        0x00706000
#define PORT_10G_9_NETWORK_USER_CTL_OFFSET        0x00706100
#define PORT_10G_10_NETWORK_USER_CTL_OFFSET       0x00706200
#define PORT_10G_11_NETWORK_USER_CTL_OFFSET       0x00706300
#define PORT_10G_0_CLIENT_USER_CTL_OFFSET         0x00708000
#define PORT_10G_1_CLIENT_USER_CTL_OFFSET         0x00708100
#define PORT_10G_2_CLIENT_USER_CTL_OFFSET         0x00708200
#define PORT_10G_3_CLIENT_USER_CTL_OFFSET         0x00708300
#define PORT_10G_4_CLIENT_USER_CTL_OFFSET         0x00707000
#define PORT_10G_5_CLIENT_USER_CTL_OFFSET         0x00707100
#define PORT_10G_6_CLIENT_USER_CTL_OFFSET         0x00709000
#define PORT_10G_7_CLIENT_USER_CTL_OFFSET         0x00709100
#define PORT_10G_8_CLIENT_USER_CTL_OFFSET         0x00709200
#define PORT_10G_9_CLIENT_USER_CTL_OFFSET         0x00709300
#define PORT_10G_10_CLIENT_USER_CTL_OFFSET        0x0070A000
#define PORT_10G_11_CLIENT_USER_CTL_OFFSET        0x0070A100

#define PORT_100G_0_NETWORK_CHANNEL_0_USER_CTL_OFFSET     0x70D000  
#define PORT_100G_0_NETWORK_CHANNEL_1_USER_CTL_OFFSET     0x70D100  
#define PORT_100G_0_NETWORK_CHANNEL_2_USER_CTL_OFFSET     0x70D200  
#define PORT_100G_0_NETWORK_CHANNEL_3_USER_CTL_OFFSET     0x70D300  
#define PORT_100G_1_NETWORK_CHANNEL_0_USER_CTL_OFFSET     0x70C000  
#define PORT_100G_1_NETWORK_CHANNEL_1_USER_CTL_OFFSET     0x70C100  
#define PORT_100G_1_NETWORK_CHANNEL_2_USER_CTL_OFFSET     0x70C200  
#define PORT_100G_1_NETWORK_CHANNEL_3_USER_CTL_OFFSET     0x70C300  
#define PORT_100G_0_CLIENT_CHANNEL_0_USER_CTL_OFFSET      0x70F000
#define PORT_100G_0_CLIENT_CHANNEL_1_USER_CTL_OFFSET      0x70F100
#define PORT_100G_0_CLIENT_CHANNEL_2_USER_CTL_OFFSET      0x70F200
#define PORT_100G_0_CLIENT_CHANNEL_3_USER_CTL_OFFSET      0x70F300
#define PORT_100G_1_CLIENT_CHANNEL_0_USER_CTL_OFFSET      0x70E000
#define PORT_100G_1_CLIENT_CHANNEL_1_USER_CTL_OFFSET      0x70E100
#define PORT_100G_1_CLIENT_CHANNEL_2_USER_CTL_OFFSET      0x70E200
#define PORT_100G_1_CLIENT_CHANNEL_3_USER_CTL_OFFSET      0x70E300

#define PORT_KR_COMX0_0_USER_CTL_OFFSET                   0x710000
#define PORT_KR_COMX0_1_USER_CTL_OFFSET                   0x710100
#define PORT_KR_COMX0_2_USER_CTL_OFFSET                   0x710200
#define PORT_KR_COMX0_3_USER_CTL_OFFSET                   0x710300
#define PORT_KR_COMX1_0_USER_CTL_OFFSET                   0x711000
#define PORT_KR_COMX1_1_USER_CTL_OFFSET                   0x711100
#define PORT_KR_COMX1_2_USER_CTL_OFFSET                   0x711200
#define PORT_KR_COMX1_3_USER_CTL_OFFSET                   0x711300
#define PORT_KR_COMX2_0_USER_CTL_OFFSET                   0x712000
#define PORT_KR_COMX2_1_USER_CTL_OFFSET                   0x712100
#define PORT_KR_COMX2_2_USER_CTL_OFFSET                   0x712200
#define PORT_KR_COMX2_3_USER_CTL_OFFSET                   0x712300
#define PORT_KR_COMX3_0_USER_CTL_OFFSET                   0x713000
#define PORT_KR_COMX3_1_USER_CTL_OFFSET                   0x713100
#define PORT_KR_COMX3_2_USER_CTL_OFFSET                   0x713200
#define PORT_KR_COMX3_3_USER_CTL_OFFSET                   0x713300



#define VIRTEX_OFFSET                          0x00400000

/* User Control offsets*/
#define TX_PKT_CNT_LSB_OFFSET                  0x0        
#define TX_PKT_CNT_MSB_OFFSET                  0x04       
#define RX_PKT_CNT_LSB_OFFSET                  0x08       
#define RX_PKT_CNT_MSB_OFFSET                  0x0C       
#define RX_BAD_PKT_CNT_LSB_OFFSET              0x10       
#define RX_BAD_PKT_CNT_MSB_OFFSET              0x14       
#define COUNTER_CONTROL_OFFSET                 0x40       
#define TXDIFFCTRL_OFFSET                      0x44
#define TXPOSTCURSOR_OFFSET                    0x48
#define TXPRECURSOR_OFFSET                     0x4C
#define AUTONEG_OFFSET                         0x50

#define COUNTER_CTL_TX_PACKET_CNT_RST_MASK     0x00000001
#define COUNTER_CTL_RX_PACKET_CNT_RST_MASK     0x00000002
#define COUNTER_CTL_RX_BAD_PACKET_CNT_RST_MASK 0x00000004
#define COUNTER_CTL_FREEZE_COUNTERS_MASK       0x00000100
#define COUNTER_CTL_ROLLOVER_MASK              0x00003000

#define PORT_10G_0_NETWORK_NAME                "PORT_10G_0_NETWORK"  
#define PORT_10G_1_NETWORK_NAME                "PORT_10G_1_NETWORK"  
#define PORT_10G_2_NETWORK_NAME                "PORT_10G_2_NETWORK"  
#define PORT_10G_3_NETWORK_NAME                "PORT_10G_3_NETWORK" 
#define PORT_10G_4_NETWORK_NAME                "PORT_10G_4_NETWORK"  
#define PORT_10G_5_NETWORK_NAME                "PORT_10G_5_NETWORK"  
#define PORT_10G_6_NETWORK_NAME                "PORT_10G_6_NETWORK"  
#define PORT_10G_7_NETWORK_NAME                "PORT_10G_7_NETWORK"  
#define PORT_10G_8_NETWORK_NAME                "PORT_10G_8_NETWORK"  
#define PORT_10G_9_NETWORK_NAME                "PORT_10G_9_NETWORK"  
#define PORT_10G_10_NETWORK_NAME               "PORT_10G_10_NETWORK"  
#define PORT_10G_11_NETWORK_NAME               "PORT_10G_11_NETWORK"  
#define PORT_10G_0_CLIENT_NAME                 "PORT_10G_0_CLIENT"   
#define PORT_10G_1_CLIENT_NAME                 "PORT_10G_1_CLIENT"   
#define PORT_10G_2_CLIENT_NAME                 "PORT_10G_2_CLIENT"   
#define PORT_10G_3_CLIENT_NAME                 "PORT_10G_3_CLIENT"   
#define PORT_10G_4_CLIENT_NAME                 "PORT_10G_4_CLIENT"   
#define PORT_10G_5_CLIENT_NAME                 "PORT_10G_5_CLIENT"   
#define PORT_10G_6_CLIENT_NAME                 "PORT_10G_6_CLIENT"   
#define PORT_10G_7_CLIENT_NAME                 "PORT_10G_7_CLIENT"   
#define PORT_10G_8_CLIENT_NAME                 "PORT_10G_8_CLIENT"   
#define PORT_10G_9_CLIENT_NAME                 "PORT_10G_9_CLIENT"   
#define PORT_10G_10_CLIENT_NAME                "PORT_10G_10_CLIENT"  
#define PORT_10G_11_CLIENT_NAME                "PORT_10G_11_CLIENT"  
#define PORT_100G_0_NETWORK_NAME               "PORT_100G_0_NETWORK"  
#define PORT_100G_1_NETWORK_NAME               "PORT_100G_1_NETWORK"  
#define PORT_100G_0_CLIENT_NAME                "PORT_100G_0_CLIENT"  
#define PORT_100G_1_CLIENT_NAME                "PORT_100G_1_CLIENT"  
#define PORT_KR_COMX0_0_NAME                   "PORT_KR_COMX0_0"  
#define PORT_KR_COMX0_1_NAME                   "PORT_KR_COMX0_1"  
#define PORT_KR_COMX0_2_NAME                   "PORT_KR_COMX0_2"  
#define PORT_KR_COMX0_3_NAME                   "PORT_KR_COMX0_3"  
#define PORT_KR_COMX1_0_NAME                   "PORT_KR_COMX1_0"  
#define PORT_KR_COMX1_1_NAME                   "PORT_KR_COMX1_1"  
#define PORT_KR_COMX1_2_NAME                   "PORT_KR_COMX1_2"  
#define PORT_KR_COMX1_3_NAME                   "PORT_KR_COMX1_3"  
#define PORT_KR_COMX2_0_NAME                   "PORT_KR_COMX2_0"  
#define PORT_KR_COMX2_1_NAME                   "PORT_KR_COMX2_1"  
#define PORT_KR_COMX2_2_NAME                   "PORT_KR_COMX2_2"  
#define PORT_KR_COMX2_3_NAME                   "PORT_KR_COMX2_3"  
#define PORT_KR_COMX3_0_NAME                   "PORT_KR_COMX3_0"  
#define PORT_KR_COMX3_1_NAME                   "PORT_KR_COMX3_1"  
#define PORT_KR_COMX3_2_NAME                   "PORT_KR_COMX3_2"  
#define PORT_KR_COMX3_3_NAME                   "PORT_KR_COMX3_3"  
#define PORT_1G_0_ARTIX_VIRTEX_NAME            "PORT_1G_0_ARTIX_VIRTEX"        
#define PORT_1G_1_ARTIX_VIRTEX_NAME            "PORT_1G_1_ARTIX_VIRTEX"        
#define PORT_1G_0_ARTIX_SMARC_MAIN_NAME        "PORT_1G_0_ARTIX_SMARC_MAIN"    
#define PORT_1G_1_ARTIX_SMARC_MAIN_NAME        "PORT_1G_1_ARTIX_SMARC_MAIN"    
#define PORT_1G_0_ARTIX_SMARC_CTRL_NAME        "PORT_1G_0_ARTIX_SMARC_CTRL"    
#define PORT_1G_1_ARTIX_SMARC_CTRL_NAME        "PORT_1G_1_ARTIX_SMARC_CTRL"    
#define PORT_1G_ARTIX_COMX0_NAME               "PORT_1G_ARTIX_COMX0"           
#define PORT_1G_ARTIX_COMX1_NAME               "PORT_1G_ARTIX_COMX1"           
#define PORT_1G_ARTIX_COMX2_NAME               "PORT_1G_ARTIX_COMX2"           
#define PORT_1G_ARTIX_COMX3_NAME               "PORT_1G_ARTIX_COMX3"           
#define PORT_1G_ARTIX_ATP_NAME                 "PORT_1G_ARTIX_ATP"             
#define PORT_1G_ARTIX_FRONT_PANEL_NAME         "PORT_1G_ARTIX_FRONT_PANEL"     
#define PORT_1G_0_VIRTEX_ARTIX_NAME            "PORT_1G_0_VIRTEX_ARTIX"        
#define PORT_1G_1_VIRTEX_ARTIX_NAME            "PORT_1G_1_VIRTEX_ARTIX"        




/* System modules IRQs definitions */
/* IRQs of first ARTIX Interrupt Controller) */
#define ETHERNET_SWITCH_IRQ                    0
#define XADC_IRQ                               1
#define I2CCORE_ATP_IRQ                        2
#define I2CCORE_SYSTEM_IRQ                     3
#define AXISPI_VIRTEX_LOAD_IRQ                 4
#define AXISPI_ATP_IRQ                         5
#define AXISPI_VIRTEX_IRQ                      6
#define AXISPI_FLASH_IRQ                       7
#define AXIUART_COMX0_IRQ                      8
#define AXIUART_COMX1_IRQ                      9
#define AXIUART_COMX2_IRQ                      10
#define AXIUART_COMX3_IRQ                      11
#define AXIUART_ATP_IRQ                        12
#define AXIUART_SMARC_MAIN_IRQ                 13
#define AXIUART_SMARC_CTRL_IRQ                 14
#define AXIUART_CLI_IRQ                        15
#define AXIUART_SMART_CARD_IRQ                 16
#define AXIGPIO_CPU_CTRL_IRQ                   17
#define AXIGPIO_VIRTEX_CTRL_IRQ                18
#define AXIGPIO_SMART_CARD_CTRL_IRQ            19
#define AXIGPIO_FLASH_MUX_CTRL_IRQ             20
#define AXIGPIO_ATP_CTRL_IRQ                   21
#define AXIGPIO_POWER_RESET_CTRL_IRQ           22
#define AXIGPIO_COMX_CTRL_IRQ                  23
#define AXIGPIO_PANEL_CTRL_IRQ                 24
#define AXIGPIO_GENERAL_CTRL_IRQ               25
#define PORT_1G_0_ARTIX_VIRTEX_IRQ             26
#define PORT_1G_1_ARTIX_VIRTEX_IRQ             27
#define PORT_1G_0_ARTIX_SMARC_MAIN_IRQ         28
#define PORT_1G_1_ARTIX_SMARC_MAIN_IRQ         29
#define PORT_1G_0_ARTIX_SMARC_CTRL_IRQ         30
#define INTC_ARTIX_1_IRQ                       31 
/* IRQs of second ARTIX Interrupt Controller */
#define PORT_1G_1_ARTIX_SMARC_CTRL_IRQ         32 
#define PORT_1G_ARTIX_COMX0_IRQ                33 
#define PORT_1G_ARTIX_COMX1_IRQ                34 
#define PORT_1G_ARTIX_COMX2_IRQ                35 
#define PORT_1G_ARTIX_COMX3_IRQ                36 
#define PORT_1G_ARTIX_ATP_IRQ                  37 
#define PORT_1G_ARTIX_FRONT_PANEL_IRQ          38 
#define INTC_VIRTEX_IRQ                        63
#define ESWITCH_IRQ                            99  //TODO: use correct IRQ - it is not yet defined as of 25.11.20
/* IRQs of Virtex Interrupt Controller */
#define I2CCORE_TRANS_CLIENT_IRQ               64
#define AXIGPIO_TRANS_CTRL_CLIENT_IRQ          65
#define I2CCORE_TRANS_NETWORK_IRQ              66
#define AXIGPIO_TRANS_CTRL_NETWORK_IRQ         67
#define SYSMON_VIRTEX_IRQ                      68
#define I2CCORE_SODIMM_IRQ                     69
#define AXIGPIO_VIRTEX_GEN_CTRL_IRQ            70
#define AXIGPIO_VIRTEX_ATP_CTRL_IRQ            71
#define PORT_1G_0_VIRTEX_ARTIX_IRQ             72
#define PORT_1G_1_VIRTEX_ARTIX_IRQ             73

#define BRDDEV_I2CCORE_NUM                     5
#define BRDDEV_AXISPI_NUM                      4
#define BRDDEV_AXIGPIO_NUM                     14
#define BRDDEV_AXIUART_NUM                     9
#define BRDDEV_PORTS_NUM                       58
#define BRDDEV_SPIDEV_NUM                      10
#define BRDDEV_EEPROM_NUM                      10
#define BRDDEV_PMBUS_NUM                       12
#define BRDDEV_FANCTLS_NUM                     1
#define BRDDEV_RTC_NUM                         3
#define BRDDEV_I2CIO_NUM                       2
#define BRDDEV_CURRMON_NUM                     6
#define BRDDEV_UART_NUM                        9
#define BRDDEV_SMARTCARD_NUM                   1
#define BRDDEV_LCD_NUM                         1
#define BRDDEV_TRANS_NUM                       28
#define BRDDEV_XADC_NUM                        1
#define BRDDEV_SYSMON_NUM                      1
#define BRDDEV_DRAM_NUM                        3


/* board devices signatures enumerator */
typedef enum tage_board_device_sig {
    eDEVBRD_CPLD_SIG                 = (BRDDEV_CPLD_MAJOR      <<16|0), 
    eDEVBRD_I2CBUS_SIG               = (BRDDEV_I2CBUS_MAJOR    <<16|0), 
    eDEVBRD_AXISPI_0_SIG             = (BRDDEV_AXISPI_MAJOR    <<16|0), 
    eDEVBRD_AXISPI_1_SIG             = (BRDDEV_AXISPI_MAJOR    <<16|1), 
    eDEVBRD_AXISPI_2_SIG             = (BRDDEV_AXISPI_MAJOR    <<16|2), 
    eDEVBRD_AXISPI_3_SIG             = (BRDDEV_AXISPI_MAJOR    <<16|3), 
    eDEVBRD_I2CCORE_0_SIG            = (BRDDEV_I2CCORE_MAJOR   <<16|0), 
    eDEVBRD_I2CCORE_1_SIG            = (BRDDEV_I2CCORE_MAJOR   <<16|1), 
    eDEVBRD_I2CCORE_2_SIG            = (BRDDEV_I2CCORE_MAJOR   <<16|2), 
    eDEVBRD_I2CCORE_3_SIG            = (BRDDEV_I2CCORE_MAJOR   <<16|3), 
    eDEVBRD_I2CCORE_4_SIG            = (BRDDEV_I2CCORE_MAJOR   <<16|4), 
    eDEVBRD_I2CCORE_5_SIG            = (BRDDEV_I2CCORE_MAJOR   <<16|5), 
    eDEVBRD_AXIUART_0_SIG            = (BRDDEV_AXIUART_MAJOR   <<16|0), 
    eDEVBRD_AXIUART_1_SIG            = (BRDDEV_AXIUART_MAJOR   <<16|1), 
    eDEVBRD_AXIUART_2_SIG            = (BRDDEV_AXIUART_MAJOR   <<16|2), 
    eDEVBRD_AXIUART_3_SIG            = (BRDDEV_AXIUART_MAJOR   <<16|3), 
    eDEVBRD_AXIUART_4_SIG            = (BRDDEV_AXIUART_MAJOR   <<16|4), 
    eDEVBRD_AXIUART_5_SIG            = (BRDDEV_AXIUART_MAJOR   <<16|5), 
    eDEVBRD_AXIUART_6_SIG            = (BRDDEV_AXIUART_MAJOR   <<16|6), 
    eDEVBRD_AXIUART_7_SIG            = (BRDDEV_AXIUART_MAJOR   <<16|7), 
    eDEVBRD_AXIUART_8_SIG            = (BRDDEV_AXIUART_MAJOR   <<16|8), 
    eDEVBRD_SPIDEV_0_SIG             = (BRDDEV_SPIDEV_MAJOR    <<16|0),
    eDEVBRD_SPIDEV_1_SIG             = (BRDDEV_SPIDEV_MAJOR    <<16|1),
    eDEVBRD_SPIDEV_2_SIG             = (BRDDEV_SPIDEV_MAJOR    <<16|2),
    eDEVBRD_SPIDEV_3_SIG             = (BRDDEV_SPIDEV_MAJOR    <<16|3),
    eDEVBRD_SPIDEV_4_SIG             = (BRDDEV_SPIDEV_MAJOR    <<16|4),
    eDEVBRD_SPIDEV_5_SIG             = (BRDDEV_SPIDEV_MAJOR    <<16|5),
    eDEVBRD_SPIDEV_6_SIG             = (BRDDEV_SPIDEV_MAJOR    <<16|6),
    eDEVBRD_SPIDEV_7_SIG             = (BRDDEV_SPIDEV_MAJOR    <<16|7),
    eDEVBRD_SPIDEV_8_SIG             = (BRDDEV_SPIDEV_MAJOR    <<16|8),
    eDEVBRD_SPIDEV_9_SIG             = (BRDDEV_SPIDEV_MAJOR    <<16|9),
    eDEVBRD_PMBUS_0_SIG              = (BRDDEV_PMBUS_MAJOR     <<16|0), 
    eDEVBRD_PMBUS_1_SIG              = (BRDDEV_PMBUS_MAJOR     <<16|1), 
    eDEVBRD_PMBUS_2_SIG              = (BRDDEV_PMBUS_MAJOR     <<16|2), 
    eDEVBRD_PMBUS_3_SIG              = (BRDDEV_PMBUS_MAJOR     <<16|3), 
    eDEVBRD_PMBUS_4_SIG              = (BRDDEV_PMBUS_MAJOR     <<16|4), 
    eDEVBRD_PMBUS_5_SIG              = (BRDDEV_PMBUS_MAJOR     <<16|5), 
    eDEVBRD_PMBUS_6_SIG              = (BRDDEV_PMBUS_MAJOR     <<16|6), 
    eDEVBRD_PMBUS_7_SIG              = (BRDDEV_PMBUS_MAJOR     <<16|7), 
    eDEVBRD_PMBUS_8_SIG              = (BRDDEV_PMBUS_MAJOR     <<16|8), 
    eDEVBRD_PMBUS_9_SIG              = (BRDDEV_PMBUS_MAJOR     <<16|9), 
    eDEVBRD_PMBUS_10_SIG             = (BRDDEV_PMBUS_MAJOR     <<16|10), 
    eDEVBRD_PMBUS_CLIENT_0_SIG       = (BRDDEV_PMBUS_MAJOR     <<16|11),
    eDEVBRD_MBOX_0_SIG               = (BRDDEV_MAILBOX_MAJOR   <<16|0), 
    eDEVBRD_MBOX_1_SIG               = (BRDDEV_MAILBOX_MAJOR   <<16|1), 
    eDEVBRD_MBOX_2_SIG               = (BRDDEV_MAILBOX_MAJOR   <<16|2), 
    eDEVBRD_MBOX_3_SIG               = (BRDDEV_MAILBOX_MAJOR   <<16|3), 
    eDEVBRD_MBOX_4_SIG               = (BRDDEV_MAILBOX_MAJOR   <<16|4), 
    eDEVBRD_MBOX_5_SIG               = (BRDDEV_MAILBOX_MAJOR   <<16|5), 
    eDEVBRD_MBOX_6_SIG               = (BRDDEV_MAILBOX_MAJOR   <<16|6), 
    eDEVBRD_PORTMAP_SIG              = (BRDDEV_PORTMAP_MAJOR   <<16|0), 
    eDEVBRD_LCD_SIG                  = (BRDDEV_LCD_MAJOR       <<16|0), 
    eDEVBRD_FAN_SIG                  = (BRDDEV_FAN_MAJOR       <<16|0), 
    eDEVBRD_MAIN_EEPROM_SIG          = (BRDDEV_EEPROM_MAJOR    <<16|0), 
    eDEVBRD_COMX0_EEPROM_SIG         = (BRDDEV_EEPROM_MAJOR    <<16|1), 
    eDEVBRD_COMX1_EEPROM_SIG         = (BRDDEV_EEPROM_MAJOR    <<16|2), 
    eDEVBRD_COMX2_EEPROM_SIG         = (BRDDEV_EEPROM_MAJOR    <<16|3), 
    eDEVBRD_COMX3_EEPROM_SIG         = (BRDDEV_EEPROM_MAJOR    <<16|4), 
    eDEVBRD_ATP_0_EEPROM_SIG         = (BRDDEV_EEPROM_MAJOR    <<16|5), 
    eDEVBRD_ATP_1_EEPROM_SIG         = (BRDDEV_EEPROM_MAJOR    <<16|6), 
    eDEVBRD_SODIMM_EEPROM_1_SIG      = (BRDDEV_EEPROM_MAJOR    <<16|7), 
    eDEVBRD_SODIMM_EEPROM_2_SIG      = (BRDDEV_EEPROM_MAJOR    <<16|8), 
    eDEVBRD_SODIMM_EEPROM_3_SIG      = (BRDDEV_EEPROM_MAJOR    <<16|9), 
    eDEVBRD_COMX0_CURRMON_SIG        = (BRDDEV_CURRMON_MAJOR   <<16|0),
    eDEVBRD_COMX1_CURRMON_SIG        = (BRDDEV_CURRMON_MAJOR   <<16|1),
    eDEVBRD_COMX2_CURRMON_SIG        = (BRDDEV_CURRMON_MAJOR   <<16|2),
    eDEVBRD_COMX3_CURRMON_SIG        = (BRDDEV_CURRMON_MAJOR   <<16|3),
    eDEVBRD_SMARC_MAIN_CURRMON_SIG   = (BRDDEV_CURRMON_MAJOR   <<16|4),
    eDEVBRD_SMARC_CTRL_CURRMON_SIG   = (BRDDEV_CURRMON_MAJOR   <<16|5),
    eDEVBRD_MAIN_RTC_SIG             = (BRDDEV_RTC_MAJOR       <<16|0), 
    eDEVBRD_ATP_0_RTC_SIG            = (BRDDEV_RTC_MAJOR       <<16|1), 
    eDEVBRD_ATP_1_RTC_SIG            = (BRDDEV_RTC_MAJOR       <<16|2), 
    eDEVBRD_I2CIO_0_SIG              = (BRDDEV_I2CIO_MAJOR     <<16|0), 
    eDEVBRD_I2CIO_1_SIG              = (BRDDEV_I2CIO_MAJOR     <<16|1), 
    eDEVBRD_I2CIO_2_SIG              = (BRDDEV_I2CIO_MAJOR     <<16|2), 
    eDEVBRD_I2CIO_3_SIG              = (BRDDEV_I2CIO_MAJOR     <<16|3), 
    eDEVBRD_I2CIO_4_SIG              = (BRDDEV_I2CIO_MAJOR     <<16|4), 
    eDEVBRD_I2CIO_5_SIG              = (BRDDEV_I2CIO_MAJOR     <<16|5), 
    eDEVBRD_I2CIO_6_SIG              = (BRDDEV_I2CIO_MAJOR     <<16|6), 
    eDEVBRD_I2CIO_7_SIG              = (BRDDEV_I2CIO_MAJOR     <<16|7), 
    eDEVBRD_I2CIO_8_SIG              = (BRDDEV_I2CIO_MAJOR     <<16|8), 
    eDEVBRD_I2CIO_9_SIG              = (BRDDEV_I2CIO_MAJOR     <<16|9), 
    eDEVBRD_AXIGPIO_0_SIG            = (BRDDEV_AXIGPIO_MAJOR   <<16|0), 
    eDEVBRD_AXIGPIO_1_SIG            = (BRDDEV_AXIGPIO_MAJOR   <<16|1), 
    eDEVBRD_AXIGPIO_2_SIG            = (BRDDEV_AXIGPIO_MAJOR   <<16|2), 
    eDEVBRD_AXIGPIO_3_SIG            = (BRDDEV_AXIGPIO_MAJOR   <<16|3), 
    eDEVBRD_AXIGPIO_4_SIG            = (BRDDEV_AXIGPIO_MAJOR   <<16|4), 
    eDEVBRD_AXIGPIO_5_SIG            = (BRDDEV_AXIGPIO_MAJOR   <<16|5), 
    eDEVBRD_AXIGPIO_6_SIG            = (BRDDEV_AXIGPIO_MAJOR   <<16|6), 
    eDEVBRD_AXIGPIO_7_SIG            = (BRDDEV_AXIGPIO_MAJOR   <<16|7), 
    eDEVBRD_AXIGPIO_8_SIG            = (BRDDEV_AXIGPIO_MAJOR   <<16|8), 
    eDEVBRD_AXIGPIO_9_SIG            = (BRDDEV_AXIGPIO_MAJOR   <<16|9), 
    eDEVBRD_AXIGPIO_10_SIG           = (BRDDEV_AXIGPIO_MAJOR   <<16|10),
    eDEVBRD_AXIGPIO_11_SIG           = (BRDDEV_AXIGPIO_MAJOR   <<16|11),
    eDEVBRD_AXIGPIO_12_SIG           = (BRDDEV_AXIGPIO_MAJOR   <<16|12),
    eDEVBRD_AXIGPIO_13_SIG           = (BRDDEV_AXIGPIO_MAJOR   <<16|13),
    eDEVBRD_AXIGPIO_14_SIG           = (BRDDEV_AXIGPIO_MAJOR   <<16|14),
    eDEVBRD_AXIGPIO_15_SIG           = (BRDDEV_AXIGPIO_MAJOR   <<16|15),
    eDEVBRD_TRANSR_0_SIG             = (BRDDEV_TRANSR_MAJOR    <<16|0),     
    eDEVBRD_TRANSR_1_SIG             = (BRDDEV_TRANSR_MAJOR    <<16|1),     
    eDEVBRD_TRANSR_2_SIG             = (BRDDEV_TRANSR_MAJOR    <<16|2),     
    eDEVBRD_TRANSR_3_SIG             = (BRDDEV_TRANSR_MAJOR    <<16|3),     
    eDEVBRD_TRANSR_4_SIG             = (BRDDEV_TRANSR_MAJOR    <<16|4),     
    eDEVBRD_TRANSR_5_SIG             = (BRDDEV_TRANSR_MAJOR    <<16|5),     
    eDEVBRD_TRANSR_6_SIG             = (BRDDEV_TRANSR_MAJOR    <<16|6),     
    eDEVBRD_TRANSR_7_SIG             = (BRDDEV_TRANSR_MAJOR    <<16|7),     
    eDEVBRD_TRANSR_8_SIG             = (BRDDEV_TRANSR_MAJOR    <<16|8),     
    eDEVBRD_TRANSR_9_SIG             = (BRDDEV_TRANSR_MAJOR    <<16|9),     
    eDEVBRD_TRANSR_10_SIG            = (BRDDEV_TRANSR_MAJOR    <<16|10),    
    eDEVBRD_TRANSR_11_SIG            = (BRDDEV_TRANSR_MAJOR    <<16|11),    
    eDEVBRD_TRANSR_12_SIG            = (BRDDEV_TRANSR_MAJOR    <<16|12),    
    eDEVBRD_TRANSR_13_SIG            = (BRDDEV_TRANSR_MAJOR    <<16|13),    
    eDEVBRD_TRANSR_14_SIG            = (BRDDEV_TRANSR_MAJOR    <<16|14),    
    eDEVBRD_TRANSR_15_SIG            = (BRDDEV_TRANSR_MAJOR    <<16|15),    
    eDEVBRD_TRANSR_16_SIG            = (BRDDEV_TRANSR_MAJOR    <<16|16),    
    eDEVBRD_TRANSR_17_SIG            = (BRDDEV_TRANSR_MAJOR    <<16|17),    
    eDEVBRD_TRANSR_18_SIG            = (BRDDEV_TRANSR_MAJOR    <<16|18),    
    eDEVBRD_TRANSR_19_SIG            = (BRDDEV_TRANSR_MAJOR    <<16|19),    
    eDEVBRD_TRANSR_20_SIG            = (BRDDEV_TRANSR_MAJOR    <<16|20),    
    eDEVBRD_TRANSR_21_SIG            = (BRDDEV_TRANSR_MAJOR    <<16|21),    
    eDEVBRD_TRANSR_22_SIG            = (BRDDEV_TRANSR_MAJOR    <<16|22),    
    eDEVBRD_TRANSR_23_SIG            = (BRDDEV_TRANSR_MAJOR    <<16|23),    
    eDEVBRD_TRANSR_24_SIG            = (BRDDEV_TRANSR_MAJOR    <<16|24),    
    eDEVBRD_TRANSR_25_SIG            = (BRDDEV_TRANSR_MAJOR    <<16|25),    
    eDEVBRD_TRANSR_26_SIG            = (BRDDEV_TRANSR_MAJOR    <<16|26),    
    eDEVBRD_TRANSR_27_SIG            = (BRDDEV_TRANSR_MAJOR    <<16|27),
    eDEVBRD_1G_MAC_0_SIG             = (BRDDEV_1G_MAC_MAJOR    <<16|0),
    eDEVBRD_1G_MAC_1_SIG             = (BRDDEV_1G_MAC_MAJOR    <<16|1),
    eDEVBRD_1G_MAC_2_SIG             = (BRDDEV_1G_MAC_MAJOR    <<16|2),
    eDEVBRD_1G_MAC_3_SIG             = (BRDDEV_1G_MAC_MAJOR    <<16|3),
    eDEVBRD_1G_MAC_4_SIG             = (BRDDEV_1G_MAC_MAJOR    <<16|4),
    eDEVBRD_1G_MAC_5_SIG             = (BRDDEV_1G_MAC_MAJOR    <<16|5),
    eDEVBRD_1G_MAC_6_SIG             = (BRDDEV_1G_MAC_MAJOR    <<16|6),
    eDEVBRD_1G_MAC_7_SIG             = (BRDDEV_1G_MAC_MAJOR    <<16|7),
    eDEVBRD_1G_MAC_8_SIG             = (BRDDEV_1G_MAC_MAJOR    <<16|8),
    eDEVBRD_1G_MAC_9_SIG             = (BRDDEV_1G_MAC_MAJOR    <<16|9),
    eDEVBRD_1G_MAC_10_SIG            = (BRDDEV_1G_MAC_MAJOR    <<16|10),
    eDEVBRD_1G_MAC_11_SIG            = (BRDDEV_1G_MAC_MAJOR    <<16|11),
    eDEVBRD_1G_MAC_12_SIG            = (BRDDEV_1G_MAC_MAJOR    <<16|12),
    eDEVBRD_1G_MAC_13_SIG            = (BRDDEV_1G_MAC_MAJOR    <<16|13),
    eDEVBRD_1G_MAC_14_SIG            = (BRDDEV_1G_MAC_MAJOR    <<16|14),
    eDEVBRD_10G_MAC_0_SIG            = (BRDDEV_10G_MAC_MAJOR   <<16|0),
    eDEVBRD_10G_MAC_1_SIG            = (BRDDEV_10G_MAC_MAJOR   <<16|1),
    eDEVBRD_10G_MAC_2_SIG            = (BRDDEV_10G_MAC_MAJOR   <<16|2),
    eDEVBRD_10G_MAC_3_SIG            = (BRDDEV_10G_MAC_MAJOR   <<16|3),
    eDEVBRD_10G_MAC_4_SIG            = (BRDDEV_10G_MAC_MAJOR   <<16|4),
    eDEVBRD_10G_MAC_5_SIG            = (BRDDEV_10G_MAC_MAJOR   <<16|5),
    eDEVBRD_10G_MAC_6_SIG            = (BRDDEV_10G_MAC_MAJOR   <<16|6),
    eDEVBRD_10G_MAC_7_SIG            = (BRDDEV_10G_MAC_MAJOR   <<16|7),
    eDEVBRD_10G_MAC_8_SIG            = (BRDDEV_10G_MAC_MAJOR   <<16|8),
    eDEVBRD_10G_MAC_9_SIG            = (BRDDEV_10G_MAC_MAJOR   <<16|9),
    eDEVBRD_10G_MAC_10_SIG           = (BRDDEV_10G_MAC_MAJOR   <<16|10),  
    eDEVBRD_10G_MAC_11_SIG           = (BRDDEV_10G_MAC_MAJOR   <<16|11),  
    eDEVBRD_10G_MAC_12_SIG           = (BRDDEV_10G_MAC_MAJOR   <<16|12),  
    eDEVBRD_10G_MAC_13_SIG           = (BRDDEV_10G_MAC_MAJOR   <<16|13),  
    eDEVBRD_10G_MAC_14_SIG           = (BRDDEV_10G_MAC_MAJOR   <<16|14),  
    eDEVBRD_10G_MAC_15_SIG           = (BRDDEV_10G_MAC_MAJOR   <<16|15),  
    eDEVBRD_10G_MAC_16_SIG           = (BRDDEV_10G_MAC_MAJOR   <<16|16),  
    eDEVBRD_10G_MAC_17_SIG           = (BRDDEV_10G_MAC_MAJOR   <<16|17),  
    eDEVBRD_10G_MAC_18_SIG           = (BRDDEV_10G_MAC_MAJOR   <<16|18),  
    eDEVBRD_10G_MAC_19_SIG           = (BRDDEV_10G_MAC_MAJOR   <<16|19),  
    eDEVBRD_10G_MAC_20_SIG           = (BRDDEV_10G_MAC_MAJOR   <<16|20),  
    eDEVBRD_10G_MAC_21_SIG           = (BRDDEV_10G_MAC_MAJOR   <<16|21),  
    eDEVBRD_10G_MAC_22_SIG           = (BRDDEV_10G_MAC_MAJOR   <<16|22),  
    eDEVBRD_10G_MAC_23_SIG           = (BRDDEV_10G_MAC_MAJOR   <<16|23),  
    eDEVBRD_10G_MAC_24_SIG           = (BRDDEV_10G_MAC_MAJOR   <<16|24),  
    eDEVBRD_10G_MAC_25_SIG           = (BRDDEV_10G_MAC_MAJOR   <<16|25),  
    eDEVBRD_10G_MAC_26_SIG           = (BRDDEV_10G_MAC_MAJOR   <<16|26),  
    eDEVBRD_10G_MAC_27_SIG           = (BRDDEV_10G_MAC_MAJOR   <<16|27),  
    eDEVBRD_10G_MAC_28_SIG           = (BRDDEV_10G_MAC_MAJOR   <<16|28),  
    eDEVBRD_10G_MAC_29_SIG           = (BRDDEV_10G_MAC_MAJOR   <<16|29),  
    eDEVBRD_10G_MAC_30_SIG           = (BRDDEV_10G_MAC_MAJOR   <<16|30),  
    eDEVBRD_10G_MAC_31_SIG           = (BRDDEV_10G_MAC_MAJOR   <<16|31),  
    eDEVBRD_10G_MAC_32_SIG           = (BRDDEV_10G_MAC_MAJOR   <<16|32),  
    eDEVBRD_10G_MAC_33_SIG           = (BRDDEV_10G_MAC_MAJOR   <<16|33),  
    eDEVBRD_10G_MAC_34_SIG           = (BRDDEV_10G_MAC_MAJOR   <<16|34),  
    eDEVBRD_10G_MAC_35_SIG           = (BRDDEV_10G_MAC_MAJOR   <<16|35),  
    eDEVBRD_10G_MAC_36_SIG           = (BRDDEV_10G_MAC_MAJOR   <<16|36),  
    eDEVBRD_10G_MAC_37_SIG           = (BRDDEV_10G_MAC_MAJOR   <<16|37),  
    eDEVBRD_10G_MAC_38_SIG           = (BRDDEV_10G_MAC_MAJOR   <<16|38),  
    eDEVBRD_10G_MAC_39_SIG           = (BRDDEV_10G_MAC_MAJOR   <<16|39),  
    eDEVBRD_100G_MAC_0_SIG           = (BRDDEV_100G_MAC_MAJOR  <<16|0),  
    eDEVBRD_100G_MAC_1_SIG           = (BRDDEV_100G_MAC_MAJOR  <<16|1),  
    eDEVBRD_100G_MAC_2_SIG           = (BRDDEV_100G_MAC_MAJOR  <<16|2),  
    eDEVBRD_100G_MAC_3_SIG           = (BRDDEV_100G_MAC_MAJOR  <<16|3), 
    eDEVBRD_XADC_0_SIG               = (BRDDEV_XADC_MAJOR      <<16|0),   
    eDEVBRD_SYSMON_0_SIG             = (BRDDEV_SYSMON_MAJOR    <<16|0),       
    eDEVBRD_ESWITCH_0_SIG            = (BRDDEV_ESWITCH_MAJOR   <<16|0),   
    eDEVBRD_INTC_0_SIG               = (BRDDEV_INTC_MAJOR      <<16|0),   
    eDEVBRD_INTC_1_SIG               = (BRDDEV_INTC_MAJOR      <<16|1),   
    eDEVBRD_INTC_2_SIG               = (BRDDEV_INTC_MAJOR      <<16|2), 
    eDEVBRD_SMARTCARD_0_SIG          = (BRDDEV_SMARTCARD_MAJOR <<16|0),      
    eDEVBRD_ATP_SIG                  = (BRDDEV_ATP_MAJOR       <<16|0),   
    eDEVBRD_CPU_SIG                  = (BRDDEV_CPU_MAJOR       <<16|0), 
    eDEVBRD_DRAMTESTER_0_SIG         = (BRDDEV_DRAMTESTER_MAJOR <<16|0),  
    eDEVBRD_DRAMTESTER_1_SIG         = (BRDDEV_DRAMTESTER_MAJOR <<16|1),  
    eDEVBRD_DRAMTESTER_2_SIG         = (BRDDEV_DRAMTESTER_MAJOR <<16|2),      
    eDEVBRD_HAL_SIG                  = (BRDDEV_HAL_MAJOR       <<16|0),   
    eDEVBRD_UNDEFINED_SIG            = 0xffffffff
} board_device_sig_e;

/*============================================================================*/
/* The board specific I2C subsystem addresses                                 */
/*----------------------------------------------------------------------------*/

/* 1. System IIC AXI IIC Bus Interface v2.0*/

/* 1.1 i2c addresses for the System bus */
typedef enum {
    eDEVBRD_I2CA_LCD                  = (0x50>>1), /*0x28*/ 
    eDEVBRD_I2CA_MAIN_RTC             = (0xd0>>1), /*0x68*/  
    eDEVBRD_I2CA_MAIN_EEPROM          = (0xa8>>1), /*0x54*/ 
    eDEVBRD_I2CA_COMX0_EEPROM         = (0xa0>>1), /*0x50*/ 
    eDEVBRD_I2CA_COMX1_EEPROM         = (0xa2>>1), /*0x51*/ 
    eDEVBRD_I2CA_COMX2_EEPROM         = (0xa4>>1), /*0x52*/ 
    eDEVBRD_I2CA_COMX3_EEPROM         = (0xa6>>1), /*0x53*/ 
    eDEVBRD_I2CA_COMX0_CURRMON        = (0x80>>1), /*0x40*/ 
    eDEVBRD_I2CA_COMX1_CURRMON        = (0x82>>1), /*0x41*/ 
    eDEVBRD_I2CA_COMX2_CURRMON        = (0x88>>1), /*0x44*/ 
    eDEVBRD_I2CA_COMX3_CURRMON        = (0x8a>>1), /*0x45*/ 
    eDEVBRD_I2CA_SMARC_MAIN_CURRMON   = (0x84>>1), /*0x42*/ 
    eDEVBRD_I2CA_SMARC_CTRL_CURRMON   = (0x86>>1), /*0x43*/ 
    eDEVBRD_I2CA_SYS_NUMBER           = 13   
} board_sys_i2cbus_addresses_e;

/* 1.2 i2c addresses for the PMBUS */
typedef enum {
    eDEVBRD_I2CA_VCCINT_O_85V         = (0xb8>>1),  /*0x5c*/ 
    eDEVBRD_I2CA_VCCAUX_1_8V          = (0x50>>1),  /*0x28*/
    eDEVBRD_I2CA_VCCO_1_2V            = (0x30>>1),  /*0x18*/
    eDEVBRD_I2CA_VCCO_3_3V            = (0x32>>1),  /*0x19*/
    eDEVBRD_I2CA_VCCO_2_5V            = (0x36>>1),  /*0x1b*/
    eDEVBRD_I2CA_MGTAVCC_0_9V         = (0x52>>1),  /*0x29*/
    eDEVBRD_I2CA_1_0V                 = (0x54>>1),  /*0x2a*/
    eDEVBRD_I2CA_MGTAVTT_1_2V         = (0x38>>1),  /*0x1c*/
    eDEVBRD_I2CA_5V                   = (0x3c>>1),  /*0x1e*/
    eDEVBRD_I2CA_MAIN_PS_0            = (0xb0>>1),  /*0x53*/
    eDEVBRD_I2CA_MAIN_PS_1            = (0xb2>>1),  /*0x55*/
} board_pmbus_addresses_e;

/* 1.3 i2c addresses for USB Box */

/* 1.4 i2c addresses for the Red PMBUS */
typedef enum {
    eDEVBRD_I2CA_VCCO_CLIENT_3_3V     = (0x34>>1),  /*0x1a*/ 
    eDEVBRD_I2CA_FAN_CTL              = (0x5c>>1)   /*0x2e*/    
} board_pmbus_red_addresses_e;

/* 2. ATP Card IIC AXI IIC Bus Interface v2.0*/

/* 2.1 i2c addresses for ATP card*/
typedef enum {
    eDEVBRD_I2CA_ATP_LB1              = (0x40>>1),  /*loopback*/  
    eDEVBRD_I2CA_ATP_LB2              = (0x40>>1),  /*loopback*/  
    eDEVBRD_I2CA_ATP_RTC1             = (0xd0>>1),  /*0x68*/      
    eDEVBRD_I2CA_ATP_RTC2             = (0xd0>>1),  /*0x68*/      
    eDEVBRD_I2CA_ATP_EEPROM1          = (0xa0>>1),  /*0x50*/      
    eDEVBRD_I2CA_ATP_EEPROM2          = (0xa0>>1),  /*0x50*/      
    eDEVBRD_I2CA_ATP_I2CIO1           = (0x40>>1),  /*0x20*/      
    eDEVBRD_I2CA_ATP_I2CIO2           = (0x42>>1)   /*0x21*/      
} board_atp_addresses;

/* 3. Red/Black SFP/QSFP I2C */

/* 3.1 i2c addresses for transceivers */
typedef enum {
    eDEVBRD_I2CA_TRANS                = (0xa0>>1),  /*0x50*/ 
    eDEVBRD_I2CA_TRANS_DIG            = (0xa2>>1)   /*0x51*/ 
} board_trans_addresses_e;

/* 3.2 i2c addresses for i2c IO expanders */
typedef enum {
    eDEVBRD_I2CA_I2CIO_0              = (0x40>>1),  /*0x20*/ 
    eDEVBRD_I2CA_I2CIO_1              = (0x42>>1)   /*0x21*/ 
} board_i2cio_addresses_e;

typedef enum {
    eDEVBRD_I2C3_RTC                  = (0xd0>>1),  /*0x68*/
    eDEVBRD_I2C3_NUMBER               = 1                   
} board_debug_i2cbus_addresses_e;

/* i2c addresses for i2c SODIMM eeproms */
typedef enum {
    eDEVBRD_I2CA_SODIMM_EEPROM1       = (0xa0>>1), /*0x50*/ 
    eDEVBRD_I2CA_SODIMM_EEPROM2       = (0xa2>>1), /*0x51*/ 
    eDEVBRD_I2CA_SODIMM_EEPROM3       = (0xa4>>1)  /*0x52*/ 
};
typedef enum {
    eDEVBRD_I2C_MBOX_0                = (0x40>>1),  /*0x20*/ //TODO check correct i2c address
} board_mbox_addresses_e;

typedef enum {
    eDEVBRD_10G_NETWORK_0         = 0,  
    eDEVBRD_10G_NETWORK_1         = 1,  
    eDEVBRD_10G_NETWORK_2         = 2,  
    eDEVBRD_10G_NETWORK_3         = 3,  
    eDEVBRD_10G_NETWORK_4         = 4,  
    eDEVBRD_10G_NETWORK_5         = 5,  
    eDEVBRD_10G_NETWORK_6         = 6,  
    eDEVBRD_10G_NETWORK_7         = 7,  
    eDEVBRD_10G_NETWORK_8         = 8,  
    eDEVBRD_10G_NETWORK_9         = 9,  
    eDEVBRD_10G_NETWORK_10        = 10,
    eDEVBRD_10G_NETWORK_11        = 11,
    eDEVBRD_10G_CLIENT_0          = 12,  
    eDEVBRD_10G_CLIENT_1          = 13,  
    eDEVBRD_10G_CLIENT_2          = 14,  
    eDEVBRD_10G_CLIENT_3          = 15,  
    eDEVBRD_10G_CLIENT_4          = 16,  
    eDEVBRD_10G_CLIENT_5          = 17,  
    eDEVBRD_10G_CLIENT_6          = 18,  
    eDEVBRD_10G_CLIENT_7          = 19,  
    eDEVBRD_10G_CLIENT_8          = 20,  
    eDEVBRD_10G_CLIENT_9          = 21,  
    eDEVBRD_10G_CLIENT_10         = 22, 
    eDEVBRD_10G_CLIENT_11         = 23, 
    eDEVBRD_100G_NETWORK_0        = 24,
    eDEVBRD_100G_NETWORK_1        = 25,
    eDEVBRD_100G_CLIENT_0         = 26,
    eDEVBRD_100G_CLIENT_1         = 27,
    eDEVBRD_KR_COMX0_0            = 28,
    eDEVBRD_KR_COMX0_1            = 29,
    eDEVBRD_KR_COMX0_2            = 30,
    eDEVBRD_KR_COMX0_3            = 31,
    eDEVBRD_KR_COMX1_0            = 32,
    eDEVBRD_KR_COMX1_1            = 33,
    eDEVBRD_KR_COMX1_2            = 34,
    eDEVBRD_KR_COMX1_3            = 35,
    eDEVBRD_KR_COMX2_0            = 36,
    eDEVBRD_KR_COMX2_1            = 37,
    eDEVBRD_KR_COMX2_2            = 38,
    eDEVBRD_KR_COMX2_3            = 39,
    eDEVBRD_KR_COMX3_0            = 40,
    eDEVBRD_KR_COMX3_1            = 41,
    eDEVBRD_KR_COMX3_2            = 42,
    eDEVBRD_KR_COMX3_3            = 43,
    eDEVBRD_1G_AV_INTERCHIP_0     = 44,
    eDEVBRD_1G_AV_INTERCHIP_1     = 45,
    eDEVBRD_1G_MAIN_CPU_0         = 46,
    eDEVBRD_1G_MAIN_CPU_1         = 47,
    eDEVBRD_1G_CTRL_CPU_0         = 48,
    eDEVBRD_1G_CTRL_CPU_1         = 49,
    eDEVBRD_1G_COMX0              = 50,
    eDEVBRD_1G_COMX1              = 51,
    eDEVBRD_1G_COMX2              = 52,
    eDEVBRD_1G_COMX3              = 53,
    eDEVBRD_1G_ATP                = 54,
    eDEVBRD_1G_OOB                = 55,
    eDEVBRD_1G_VA_INTERCHIP_0     = 56,
    eDEVBRD_1G_VA_INTERCHIP_1     = 57 
} board_ports_e;


/*============================================================================*/
/* The board Logical Ports / Logical SFPs / HW Specified SFPs mapping,        */ 
/* see HW spec port mapping table                                             */
/*----------------------------------------------------------------------------*/
#define DEVBRD_COMX_MAXNUM                       4
#define DEVBRD_LOGPORT_1G_ARTIX_MAXNUM           12
#define DEVBRD_LOGPORT_10G_CLIENT_MAXNUM         12
#define DEVBRD_LOGPORT_100G_CLIENT_MAXNUM        2
#define DEVBRD_LOGPORT_10G_NETWORK_MAXNUM        12
#define DEVBRD_LOGPORT_100G_NETWORK_MAXNUM       2
#define DEVBRD_LOGPORT_KR_COMX_MAXNUM            4
#define DEVBRD_LOGPORT_1G_VIRTEX_ARTIX_MAXNUM    2
#define DEVBRD_LOGPORT_ETH_SWITCH_MAXNUM         10

#define DEVBRD_LOGPORT_MAXNUM (DEVBRD_LOGPORT_10G_CLIENT_MAXNUM + \
                               DEVBRD_LOGPORT_1G_ARTIX_MAXNUM + \
                               DEVBRD_LOGPORT_100G_CLIENT_MAXNUM + \
                               DEVBRD_LOGPORT_10G_NETWORK_MAXNUM + \
                               DEVBRD_LOGPORT_100G_NETWORK_MAXNUM + \
                               DEVBRD_LOGPORT_KR_COMX_MAXNUM * DEVBRD_COMX_MAXNUM + \
                               DEVBRD_LOGPORT_1G_VIRTEX_ARTIX_MAXNUM)
/* 1G ports*/
#define LOGPORT_1G_0            0
#define LOGPORT_1G_1            1
#define LOGPORT_1G_2            2
#define LOGPORT_1G_3            3
#define LOGPORT_1G_4            4
#define LOGPORT_1G_5            5
#define LOGPORT_1G_6            6
#define LOGPORT_1G_7            7
#define LOGPORT_1G_8            8
#define LOGPORT_1G_9            9
#define LOGPORT_1G_10           10
#define LOGPORT_1G_11           11
#define LOGPORT_1G_V_A_0        12
#define LOGPORT_1G_V_A_1        13

/* 10G ports */
#define LOGPORT_10G_0           0
#define LOGPORT_10G_1           1
#define LOGPORT_10G_2           2
#define LOGPORT_10G_3           3
#define LOGPORT_10G_4           4
#define LOGPORT_10G_5           5
#define LOGPORT_10G_6           6
#define LOGPORT_10G_7           7
#define LOGPORT_10G_8           8
#define LOGPORT_10G_9           9
#define LOGPORT_10G_10          10
#define LOGPORT_10G_11          11
#define LOGPORT_10G_12          12
#define LOGPORT_10G_13          13
#define LOGPORT_10G_14          14
#define LOGPORT_10G_15          15
#define LOGPORT_10G_16          16
#define LOGPORT_10G_17          17
#define LOGPORT_10G_18          18
#define LOGPORT_10G_19          19
#define LOGPORT_10G_20          20
#define LOGPORT_10G_21          21
#define LOGPORT_10G_22          22
#define LOGPORT_10G_23          23
/* KR ports */
#define LOGPORT_KR_0            24 
#define LOGPORT_KR_1            25 
#define LOGPORT_KR_2            26 
#define LOGPORT_KR_3            27 
#define LOGPORT_KR_4            28 
#define LOGPORT_KR_5            29 
#define LOGPORT_KR_6            30 
#define LOGPORT_KR_7            31 
#define LOGPORT_KR_8            32 
#define LOGPORT_KR_9            33 
#define LOGPORT_KR_10           34
#define LOGPORT_KR_11           35
#define LOGPORT_KR_12           36
#define LOGPORT_KR_13           37
#define LOGPORT_KR_14           38
#define LOGPORT_KR_15           39

/* 100G ports */
#define LOGPORT_100G_0          0
#define LOGPORT_100G_1          1
#define LOGPORT_100G_2          2
#define LOGPORT_100G_3          3


#define LOGPORT_01_HWSPEC_SFP   1
#define LOGPORT_02_HWSPEC_SFP   2
#define LOGPORT_03_HWSPEC_SFP   3
#define LOGPORT_04_HWSPEC_SFP   4
#define LOGPORT_05_HWSPEC_SFP   5
#define LOGPORT_06_HWSPEC_SFP   6
#define LOGPORT_07_HWSPEC_SFP   7
#define LOGPORT_08_HWSPEC_SFP   8
#define LOGPORT_09_HWSPEC_SFP   9
#define LOGPORT_10_HWSPEC_SFP   10
#define LOGPORT_11_HWSPEC_SFP   11
#define LOGPORT_12_HWSPEC_SFP   12
#define LOGPORT_13_HWSPEC_SFP   13
#define LOGPORT_14_HWSPEC_SFP   14
#define LOGPORT_15_HWSPEC_SFP   15
#define LOGPORT_16_HWSPEC_SFP   16
#define LOGPORT_17_HWSPEC_SFP   17
#define LOGPORT_18_HWSPEC_SFP   18
#define LOGPORT_19_HWSPEC_SFP   19
#define LOGPORT_20_HWSPEC_SFP   20
#define LOGPORT_21_HWSPEC_SFP   21
#define LOGPORT_22_HWSPEC_SFP   22
#define LOGPORT_23_HWSPEC_SFP   23
#define LOGPORT_24_HWSPEC_SFP   24
#define LOGPORT_25_HWSPEC_SFP   25 
#define LOGPORT_26_HWSPEC_SFP   26
#define LOGPORT_27_HWSPEC_SFP   27
#define LOGPORT_28_HWSPEC_SFP   28


/* logical port number to hwsfp idx mapping for 32-mode */
static int _logport_hwspec_sfp_index[] = 
{
    LOGPORT_01_HWSPEC_SFP, LOGPORT_02_HWSPEC_SFP, LOGPORT_03_HWSPEC_SFP, LOGPORT_04_HWSPEC_SFP, 
    LOGPORT_05_HWSPEC_SFP, LOGPORT_06_HWSPEC_SFP, LOGPORT_07_HWSPEC_SFP, LOGPORT_08_HWSPEC_SFP, 
    LOGPORT_09_HWSPEC_SFP, LOGPORT_10_HWSPEC_SFP, LOGPORT_11_HWSPEC_SFP, LOGPORT_12_HWSPEC_SFP,
    LOGPORT_13_HWSPEC_SFP, LOGPORT_14_HWSPEC_SFP, LOGPORT_15_HWSPEC_SFP, LOGPORT_16_HWSPEC_SFP,  
    LOGPORT_17_HWSPEC_SFP, LOGPORT_18_HWSPEC_SFP, LOGPORT_19_HWSPEC_SFP, LOGPORT_20_HWSPEC_SFP,  
    LOGPORT_21_HWSPEC_SFP, LOGPORT_22_HWSPEC_SFP, LOGPORT_23_HWSPEC_SFP, LOGPORT_24_HWSPEC_SFP,  
    LOGPORT_25_HWSPEC_SFP, LOGPORT_26_HWSPEC_SFP, LOGPORT_27_HWSPEC_SFP, LOGPORT_28_HWSPEC_SFP,  
};

#define ETH_SWITCH_PORT_NUM     12

#endif/* _TSG_H_ */
