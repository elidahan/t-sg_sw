#ifndef _DEV_TRANS_H_
#define _DEV_TRANS_H_

/**
 * @name Enumerator to specify TRANS (transceiver) device 
 *       attributes
 */
enum trans_attr_e {
    eTRANS_IS_PRESENT,
    eTRANS_LOW_ID,                  
    eTRANS_LOW_STATUS,              
    eTRANS_LOW_INTR_FLAG_LOS,       
    eTRANS_LOW_INTR_FLAG_FLT,       
    eTRANS_LOW_INTR_FLAG_TMP,       
    eTRANS_LOW_INTR_FLAG_VCC,       
    eTRANS_LOW_INTR_FLAG_RX12_PWR,  
    eTRANS_LOW_INTR_FLAG_RX34_PWR,  
    eTRANS_LOW_INTR_FLAG_TX12_BIAS, 
    eTRANS_LOW_INTR_FLAG_TX34_BIAS, 
    eTRANS_LOW_CTRL_TX_DIS,         
    eTRANS_LOW_CTRL_RX_RS,          
    eTRANS_LOW_CTRL_TX_RS,          
    eTRANS_MSA_RAW,                 
    eTRANS_MSA_ID,                  
    eTRANS_MSA_EXTENDED,            
    eTRANS_MSA_CONNECTOR,           
    eTRANS_MSA_TRANSCODE,           
    eTRANS_MSA_ENCODING,            
    eTRANS_MSA_BR_NOMINAL,          
    eTRANS_MSA_LEN_9UM_KM,          
    eTRANS_MSA_LEN_9UM,             
    eTRANS_MSA_LEN_50UM,            
    eTRANS_MSA_LEN_625UM,           
    eTRANS_MSA_LEN_COPPER,          
    eTRANS_MSA_VENDOR_NAME,         
    eTRANS_MSA_VENDOR_OUI,          
    eTRANS_MSA_VENDOR_PN,           
    eTRANS_MSA_VENDOR_REV,          
    eTRANS_MSA_WAVELENGTH,          
    eTRANS_MSA_CC_BASE,             
    eTRANS_MSA_OPTIONS,             
    eTRANS_MSA_BR_MAX,              
    eTRANS_MSA_BR_MIN,              
    eTRANS_MSA_VENDOR_SN,           
    eTRANS_MSA_DATE_CODE,           
    eTRANS_MSA_DDM_TYPE,            
    eTRANS_MSA_ENHANCED_OPS,        
    eTRANS_MSA_SFF_8472,            
    eTRANS_MSA_CC_EXT,              
    eTRANS_MSA_VENDOR_SPEC,         
    eTRANS_DD_RAW,                  
    eTRANS_DD_TEMP_HIGH_ALARM,      
    eTRANS_DD_TEMP_LOW_ALARM,       
    eTRANS_DD_TEMP_HIGH_WARN,       
    eTRANS_DD_TEMP_LOW_WARN,        
    eTRANS_DD_VCC_HIGH_ALARM,       
    eTRANS_DD_VCC_LOW_ALARM,        
    eTRANS_DD_VCC_HIGH_WARN,        
    eTRANS_DD_VCC_LOW_WARN,         
    eTRANS_DD_BIAS_HIGH_ALARM,      
    eTRANS_DD_BIAS_LOW_ALARM,       
    eTRANS_DD_BIAS_HIGH_WARN,       
    eTRANS_DD_BIAS_LOW_WARN,        
    eTRANS_DD_TX_PWR_HIGH_ALARM,    
    eTRANS_DD_TX_PWR_LOW_ALARM,     
    eTRANS_DD_TX_PWR_HIGH_WARN,     
    eTRANS_DD_TX_PWR_LOW_WARN,      
    eTRANS_DD_RX_PWR_HIGH_ALARM,    
    eTRANS_DD_RX_PWR_LOW_ALARM,     
    eTRANS_DD_RX_PWR_HIGH_WARN,     
    eTRANS_DD_RX_PWR_LOW_WARN,      
    eTRANS_DD_CAL_RX_PWR_4,         
    eTRANS_DD_CAL_RX_PWR_3,         
    eTRANS_DD_CAL_RX_PWR_2,         
    eTRANS_DD_CAL_RX_PWR_1,         
    eTRANS_DD_CAL_RX_PWR_0,         
    eTRANS_DD_CAL_TX_I_SLOPE,       
    eTRANS_DD_CAL_TX_I_OFFSET,      
    eTRANS_DD_CAL_TX_PWR_SLOPE,     
    eTRANS_DD_CAL_TX_PWR_OFFSET,    
    eTRANS_DD_CAL_T_SLOPE,          
    eTRANS_DD_CAL_T_OFFSET,         
    eTRANS_DD_CAL_V_SLOPE,          
    eTRANS_DD_CAL_V_OFFSET,         
    eTRANS_DD_CAL_CHECKSUM,         
    eTRANS_DD_AD_TEMP,              
    eTRANS_DD_AD_VCC,               
    eTRANS_DD_AD_TX_BIAS,           
    eTRANS_DD_AD_TX_PWR,            
    eTRANS_DD_AD_RX_PWR,            
    eTRANS_DD_AD_STATUS_110,        
    eTRANS_DD_AD_STATUS_112,        
    eTRANS_DD_AD_STATUS_113,        
    eTRANS_DD_AD_STATUS_116,        
    eTRANS_DD_AD_STATUS_117,        
    eTRANS_DD_AD_VENDOR_SPEC,       
    eTRANS_DD_USER_WRITABLE,
    eTRANS_MEDIA_DECODE,
    eTRANS_INIT,
    eTRANS_CONFIGURE_TEST,
    eTRANS_RUN_TEST,
};

/* APIs */
/**
* @brief Read TRANS (transceiver) device attribute
*
* @param[in] p_priv points to the private context of the device
* @param[in] attrid id of the attribute
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] idx attribute's index in case the attribute id corresponds to array
* @param[out] p_value pointer to the buffer to hold returned attribute's value
* @param[in] size indicates size of the attribute
*
* @return	error code
*/
ccl_err_t trans_attr_read(void *p_priv, b_u32 attrid, 
                          b_u32 offset, b_u32 offset_sz, 
                          b_u32 idx, b_u8 *p_value, b_u32 size);

/**
* @brief Write TRANS (transceiver) device attribute
*
* @param[in] p_priv points to the private context of the device
* @param[in] attrid id of the attribute
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] idx attribute's index in case the attribute id corresponds to array
* @param[in] p_value pointer to the buffer which holds written 
*            data value
* @param[in] size indicates size of the attribute
*
* @return	error code
*/
ccl_err_t trans_attr_write(void *p_priv, b_u32 attrid, 
                           b_u32 offset, b_u32 offset_sz, 
                           b_u32 idx, b_u8 *p_value, b_u32 size);

#endif/* _DEV_TRANS_H_ */
