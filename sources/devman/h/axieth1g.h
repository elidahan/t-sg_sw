#ifndef _DEV_AXIETH1G_H_
#define _DEV_AXIETH1G_H_

/**
 * @name Enumerator to specify AXI 1G/2.5G Ethernet Subsystem 
 *       device attributes
 */
enum axieth1g_attr_e {
    eAXIETH1G_RAF,
    eAXIETH1G_TPF,
    eAXIETH1G_IFGP,
    eAXIETH1G_IS,
    eAXIETH1G_IP,
    eAXIETH1G_IE,
    eAXIETH1G_TTAG,
    eAXIETH1G_RTAG,
    eAXIETH1G_UAWL,
    eAXIETH1G_UAWU,
    eAXIETH1G_TPID0,
    eAXIETH1G_TPID1,
    eAXIETH1G_RXBL,
    eAXIETH1G_TXBL,
    eAXIETH1G_RXUNDRL,
    eAXIETH1G_RXFRAGL,
    eAXIETH1G_RX64BL,
    eAXIETH1G_RX65B127L,
    eAXIETH1G_RX128B255L,
    eAXIETH1G_RX256B511L,
    eAXIETH1G_RX512B1023L,
    eAXIETH1G_RX1024BL,
    eAXIETH1G_RXOVRL,
    eAXIETH1G_TX64BL,
    eAXIETH1G_TX65B127L,
    eAXIETH1G_TX128B255L,
    eAXIETH1G_TX256B511L,
    eAXIETH1G_TX512B1023L,
    eAXIETH1G_TX1024L,
    eAXIETH1G_TXOVRL,
    eAXIETH1G_RXFL,
    eAXIETH1G_RXFCSERL,
    eAXIETH1G_RXBCSTFL,
    eAXIETH1G_RXMCSTFL,
    eAXIETH1G_RXCTRFL,
    eAXIETH1G_RXLTERL,
    eAXIETH1G_RXVLANFL,
    eAXIETH1G_RXPFL,
    eAXIETH1G_RXUOPFL,
    eAXIETH1G_TXFL,
    eAXIETH1G_TXBCSTFL,
    eAXIETH1G_TXMCSTFL,
    eAXIETH1G_TXUNDRERL,
    eAXIETH1G_TXCTRFL,
    eAXIETH1G_TXVLANFL,
    eAXIETH1G_TXPFL,
    eAXIETH1G_TXSCL,
    eAXIETH1G_TXMCL,
    eAXIETH1G_TXDEF,
    eAXIETH1G_TXLTCL,
    eAXIETH1G_TXAECL,
    eAXIETH1G_TXEDEFL,
    eAXIETH1G_RXAERL,
    eAXIETH1G_RCW0,
    eAXIETH1G_RCW1,
    eAXIETH1G_TC,
    eAXIETH1G_FCC,
    eAXIETH1G_EMMC,
    eAXIETH1G_RXFC,
    eAXIETH1G_TXFC,
    eAXIETH1G_TX_TIMESTAMP_ADJ,
    eAXIETH1G_PHYC,
    eAXIETH1G_IDREG,
    eAXIETH1G_ARREG,
    eAXIETH1G_MDIO_MC,
    eAXIETH1G_MDIO_MCR,
    eAXIETH1G_MDIO_MWD,
    eAXIETH1G_MDIO_MRD,
    eAXIETH1G_MDIO_MIS,
    eAXIETH1G_MDIO_MIP,
    eAXIETH1G_MDIO_MIE,
    eAXIETH1G_MDIO_MIC,
    eAXIETH1G_UAW0,
    eAXIETH1G_UAW1,
    eAXIETH1G_FMFC,
    eAXIETH1G_FMFV,
    eAXIETH1G_FMFMV,
    eAXIETH1G_TX_VLAN_DATA,
    eAXIETH1G_RX_VLAN_DATA,
    eAXIETH1G_ETH_AVB,
    eAXIETH1G_MCAST_TABLE
};

/* APIs */
/**
* @brief Read AXI 1G/2.5G Ethernet Subsystem device attribute
*
* @param[in] p_priv points to the private context of the device
* @param[in] attrid id of the attribute
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] idx attribute's index in case the attribute id corresponds to array
* @param[out] p_value pointer to the buffer to hold returned attribute's value
* @param[in] size indicates size of the attribute
*
* @return	error code
*/
ccl_err_t axieth1g_attr_read(void *p_priv, b_u32 attrid, 
                             b_u32 offset, b_u32 offset_sz, 
                             b_u32 idx, b_u8 *p_value, b_u32 size);

/**
* @brief Write AXI 1G/2.5G Ethernet Subsystem device attribute
*
* @param[in] p_priv points to the private context of the device
* @param[in] attrid id of the attribute
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] idx attribute's index in case the attribute id corresponds to array
* @param[in] p_value pointer to the buffer which holds written 
*            data value
* @param[in] size indicates size of the attribute
*
* @return	error code
*/
ccl_err_t axieth1g_attr_write(void *p_priv, b_u32 attrid, 
                              b_u32 offset, b_u32 offset_sz, 
                              b_u32 idx, b_u8 *p_value, b_u32 size);

#endif/* _DEV_AXIETH1G_H_ */
