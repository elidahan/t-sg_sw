#ifndef DEV_I2CBUS_H
#define DEV_I2CBUS_H

/**
 * @name Enumerator to specify I2C bus device attributes
 */
enum i2c_attr_e {
    eI2CBUS_BUS_ID,
    eI2CBUS_CDEV_NAME,
    eI2CBUS_SLAVES_NUM,
    eI2CBUS_SLAVE_NAME,
    eI2CBUS_SLAVE_I2CA
};

/* read helpers */
/**
* @brief Read byte via I2C bus
*
* @param[in] p_i2c_point points to data structure describing i2c
*       route internals
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[out] p_value pointer to the buffer to hold returned 
*       data value
*
* @return	error code
*/
ccl_err_t devi2cbus_read_byte(i2c_route_t *p_i2c_point, 
                              b_u32 offset, 
                              b_u32 offset_sz, 
                              b_u8 *p_value);

/**
* @brief Read half word via I2C bus
*
* @param[in] p_i2c_point points to data structure describing i2c
*       route internals
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[out] p_value pointer to the buffer to hold returned 
*       data value
*
* @return	error code
*/
ccl_err_t devi2cbus_read_half(i2c_route_t *p_i2c_point, 
                              b_u32 offset, 
                              b_u32 offset_sz, 
                              b_u16 *p_value);

/**
* @brief Read word via I2C bus
*
* @param[in] p_i2c_point points to data structure describing i2c
*       route internals
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[out] p_value pointer to the buffer to hold returned 
*       data value
*  
* @return	error code
*/
ccl_err_t devi2cbus_read_word(i2c_route_t *p_i2c_point, 
                              b_u32 offset, 
                              b_u32 offset_sz, 
                              b_u32 *p_value);


/**
* @brief Read data via I2C bus
*
* @param[in] p_i2c_point points to data structure describing i2c
*       route internals
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[out] p_value pointer to the buffer to hold returned 
*       data value
* @param[in] size indicates size of the data
*
* @return	error code
*/
ccl_err_t devi2cbus_read_buff(i2c_route_t *p_i2c_point, 
                              b_u32 offset, 
                              b_u32 offset_sz, 
                              b_u8 *p_value, 
                              b_u32 size);

/* write helpers */
/**
* @brief Write byte via I2C bus
*
* @param[in] p_i2c_point points to data structure describing i2c
*       route internals
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] value written data
*
* @return	error code
*/
ccl_err_t devi2cbus_write_byte(i2c_route_t *p_i2c_point, 
                               b_u32 offset, 
                               b_u32 offset_sz, 
                               b_u8 value);

/**
* @brief Write half word via I2C bus
*
* @param[in] p_i2c_point points to data structure describing i2c
*       route internals
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] value written data
*
* @return	error code
*/
ccl_err_t devi2cbus_write_half(i2c_route_t *p_i2c_point, 
                               b_u32 offset, 
                               b_u32 offset_sz, 
                               b_u16 value);

/**
* @brief Write word via I2C bus
*
* @param[in] p_i2c_point points to data structure describing i2c
*       route internals
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] value written data
*
* @return	error code
*/
ccl_err_t devi2cbus_write_word(i2c_route_t *p_i2c_point, 
                               b_u32 offset, 
                               b_u32 offset_sz, 
                               b_u32 value);

/**
* @brief Write data via I2C bus
*
* @param[in] p_i2c_point points to data structure describing i2c
*       route internals
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] p_value pointer to the buffer which holds written 
*       data value
* @param[in] size indicates size of the data
*
* @return	error code
*/
ccl_err_t devi2cbus_write_buff(i2c_route_t *p_i2c_point, 
                               b_u32 offset, 
                               b_u32 offset_sz, 
                               b_u8 *p_value, 
                               b_u32 size);


#endif/* DEV_I2CBUS_H */
