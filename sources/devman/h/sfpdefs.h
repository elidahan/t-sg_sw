/********************************************************************************/
/**
 * @file sfpdefs.h
 * @brief SFP ID information access definitions header file
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/

#ifndef _SFPDEFS_h_
#define _SFPDEFS_h_

/******************************************* Constants *******************************************/
/* Register addresses */
#define SFP_IDENTIFIER							0x00
#define SFP_EXT_IDENTIFIER						0x01
#define SFP_CONNECTOR							0x02
#define SFP_TRANSCEIVER_START					0x03
#define SFP_TRANSCEIVER_END						0x0A
#define SFP_ENCODING							0x0B
#define SFP_BR_NOMINAL							0x0C
#define SFP_LENGTH_9M_KM						0x0E
#define SFP_LENGTH_9M							0x0F
#define SFP_LENGTH_50M							0x10
#define SFP_LENGTH_62_5M						0x11
#define SFP_LENGTH_COPPER						0x12
#define SFP_VENDOR_NAME_START					0x14
#define SFP_VENDOR_NAME_END						0x23
#define SFP_VENDOR_OUI_START					0x25
#define SFP_VENDOR_OUI_END						0x27
#define SFP_VENDOR_PN_START						0x28
#define SFP_VENDOR_PN_END						0x37
#define SFP_VENDOR_REV_START					0x38
#define SFP_VENDOR_REV_END						0x3B
#define SFP_CC_BASE								0x3F
#define SFP_OPTIONS								0x41
#define SFP_BR_MAX								0x42
#define SFP_BR_MIN								0x43
#define SFP_VENDOR_SN_START						0x44
#define SFP_VENDOR_SN_END						0x53
#define SFP_DATE_CODE_START						0x54
#define SFP_DATE_CODE_END						0x5B
#define SFP_CC_EXT								0x5F

#define SFP_ID_UNKNOWN 							0x00
#define SFP_ID_GBIC 							0x01
#define SFP_ID_SOLDERED							0x02
#define SFP_ID_SFP 								0x03

#define SFP_CONNECTOR_UNKNOWN 					0x00
#define SFP_CONNECTOR_SC 						0x01
#define SFP_CONNECTOR_FIBRE_1 					0x02
#define SFP_CONNECTOR_FIBRE_2 					0x03
#define SFP_CONNECTOR_BNC_TNC 					0x04
#define SFP_CONNECTOR_FIBRE_COAX 				0x05
#define SFP_CONNECTOR_FIBERJACK 				0x06
#define SFP_CONNECTOR_LC 						0x07
#define SFP_CONNECTOR_MT_RJ 					0x08
#define SFP_CONNECTOR_MU 						0x09
#define SFP_CONNECTOR_SG 						0x0A
#define SFP_CONNECTOR_OPT_PIGTAIL 				0x0B
#define SFP_CONNECTOR_HSSDC_II 					0x20
#define SFP_CONNECTOR_COP_PIGTAIL 				0x21

// Address 0x04
#define SFP_TRANSCEIVER_OC_48_LONG 				0x04
#define SFP_TRANSCEIVER_OC_48_INTER				0x02
#define SFP_TRANSCEIVER_OC_48_SHORT				0x01

// Address 0x05
#define SFP_TRANSCEIVER_OC_12_SINGLE_LONG 		0x40
#define SFP_TRANSCEIVER_OC_12_SINGLE_INTER 		0x20
#define SFP_TRANSCEIVER_OC_12_MULTI_SHORT 		0x10
#define SFP_TRANSCEIVER_OC_3_SINGLE_LONG 		0x04
#define SFP_TRANSCEIVER_OC_3_SINGLE_INTER 		0x02
#define SFP_TRANSCEIVER_OC_3_MULTI_SHORT 		0x01

// Address 0x06
#define SFP_TRANSCEIVER_1000BASE_T 				0x08
#define SFP_TRANSCEIVER_1000BASE_CX 			0x04
#define SFP_TRANSCEIVER_1000BASE_LX				0x02
#define SFP_TRANSCEIVER_1000BASE_SX 			0x01

// Address 0x07
#define SFP_FIBRE_LINK_VERY_LONG 				0x80
#define SFP_FIBRE_LINK_SHORT 					0x40
#define SFP_FIBRE_LINK_INTER 					0x20
#define SFP_FIBRE_LINK_LONG 					0x10

#define SFP_FIBRE_TECH_LW_LASER_LC 				0x02
#define SFP_FIBRE_TECH_ELECTR_INTER_ENCLOSURE 	0x01

// Address 0x08
#define SFP_FIBRE_TECH_ELECTR_INTRA_ENCLOSURE	0x80
#define SFP_FIBRE_TECH_SW_LASER_NO_OFC 			0x40
#define SFP_FIBRE_TECH_SW_LASER_OFC				0x20
#define SFP_FIBRE_TECH_LW_LASER_LL				0x10

// Address 0x09
#define SFP_FIBRE_MEDIA_TWIN_AXIAL_PAIR 		0x80
#define SFP_FIBRE_MEDIA_SHIELDED_TWISTED_PAIR 	0x40
#define SFP_FIBRE_MEDIA_MINIATURE_COAX 			0x20
#define SFP_FIBRE_MEDIA_VIDEO_COAX 	 			0x10
#define SFP_FIBRE_MEDIA_MULTI_62_5 	 			0x08
#define SFP_FIBRE_MEDIA_MULTI_50 				0x04
#define SFP_FIBRE_MEDIA_SINGLE 					0x01

// Address 0x0A
#define SFP_FIBRE_SPEED_400MB_SEC 				0x10
#define SFP_FIBRE_SPEED_200MB_SEC 				0x04
#define SFP_FIBRE_SPEED_100MB_SEC 				0x01

// Address 0x0B
#define SFP_ENCODING_UNKNOWN 					0x00
#define SFP_ENCODING_8B10B 						0x01
#define SFP_ENCODING_4B5B 						0x02
#define SFP_ENCODING_NRZ 						0x03
#define SFP_ENCODING_MANCHESTER 				0x04

// Address 0x41
#define SFP_OPTIONS_RATE_SELECT 				0x20
#define SFP_OPTIONS_TX_DISABLE 					0x10
#define SFP_OPTIONS_TX_FAULT 					0x08
#define SFP_OPTIONS_LOS_INV 					0x04
#define SFP_OPTIONS_LOS_NO_INV 					0x02

#if 0
    #define SFP_1									1
    #define SFP_2									2
    #define SFP_3									3
    #define SFP_4									4
    #define SFP_5									5
    #define SFP_6									6
    #define SFP_7									7
    #define SFP_8									8
    #define SFP_9									9
    #define SFP_10									10
    #define SFP_11									11
    #define SFP_12									12
    #define SFP_13									13
    #define SFP_14									14
    #define SFP_15									15
    #define SFP_16									16
    #define SFP_17									17
    #define SFP_18									18
    #define SFP_19									19
    #define SFP_20									20
    #define SFP_21									21
    #define SFP_22									22
    #define SFP_23									23
    #define SFP_24									24

/* Channel */
    #define iSFP_CHANNEL_OFFS                       8 /* Offset between the SFP number and the channel number */


/* SPF Number to I2C Channel Number */
//#define SFP_TO_CHAN(sfp)                         (((sfp)-1)+iSFP_CHANNEL_OFFS) 
#endif 


#endif/* !_SFPDEFS_h_ */
