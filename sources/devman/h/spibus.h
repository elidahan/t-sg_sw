#ifndef _SPI_FPGA_H_
#define _SPI_FPGA_H_

/* APIs */
ccl_err_t devspibus_read_buff(char *devname, 
                            b_u32 offset, b_u32 idx, 
                            b_u8 *p_value, b_u32 size);
ccl_err_t devspibus_write_buff(char *devname, 
                             b_u32 offset, b_u32 idx, 
                             b_u8 *p_value, b_u32 size);
ccl_err_t devspibus_write_burst(char *devname, 
                                b_u32 offset, b_u32 idx, 
                                b_u8 *p_value, b_u32 size);

#endif/* _SPI_FPGA_H_ */
