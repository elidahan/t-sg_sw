#ifndef _DEV_DRAMTESTER_H_
#define _DEV_DRAMTESTER_H_

/**
 * @name Enumerator to specify DRAMTESTER device attributes
 */
enum dramtester_attr_e {
    eDRAMTESTER_RUN_TEST,
};

/**
* @brief Write DRAMTESTER device attribute
*
* @param[in] p_priv points to the private context of the device
* @param[in] attrid id of the attribute
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] idx attribute's index in case the attribute id corresponds to array
* @param[in] p_value pointer to the buffer which holds written 
*            data value
* @param[in] size indicates size of the attribute
*
* @return	error code
*/
ccl_err_t dramtester_attr_write(void *p_priv, b_u32 attrid, 
                                b_u32 offset, b_u32 offset_sz, 
                                b_u32 idx, b_u8 *p_value, b_u32 size);

#endif/* _DEV_DRAMTESTER_H_ */
