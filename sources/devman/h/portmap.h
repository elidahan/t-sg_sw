#ifndef _DEV_PORTMAP_H_
#define _DEV_PORTMAP_H_

/**
 * @name Enumerator to specify PORTMAP device attributes 
 */
enum portmapu_attr_e {
    ePORTMAP_TRANS_ID,       
    ePORTMAP_IS_PRESENT,
    ePORTMAP_LED,                 
    ePORTMAP_ENABLE, 
    ePORTMAP_RESET, 
    ePORTMAP_TX_FAULT,
    ePORTMAP_RX_LOS,
    ePORTMAP_RS0,
    ePORTMAP_RS1,
    ePORTMAP_MODULE_SELECT,
    ePORTMAP_LPMODE,
    ePORTMAP_MEDIA_PRESENT,
    ePORTMAP_MEDIA_ID,
    ePORTMAP_MEDIA_DDTYPE,
    ePORTMAP_MEDIA_MSA,
    ePORTMAP_MEDIA_DD,
    ePORTMAP_MEDIA_PHY,
    ePORTMAP_COUNTER_READ,
    ePORTMAP_INIT,
    ePORTMAP_CONFIGURE_TEST,
    ePORTMAP_RUN_TEST,
    ePORTMAP_COUNTERS_RESET,
    ePORTMAP_COUNTER_RX_BYTES,
    ePORTMAP_COUNTER_TX_BYTES,
    ePORTMAP_COUNTER_RX_PACKETS,
    ePORTMAP_COUNTER_TX_PACKETS,
    ePORTMAP_COUNTER_RX_TOTAL_BYTES,
    ePORTMAP_COUNTER_RX_TOTAL_PACKETS,
    ePORTMAP_COUNTER_RX_BCAST_PACKETS,
    ePORTMAP_COUNTER_RX_MCAST_PACKETS,
    ePORTMAP_COUNTER_RX_CRC_ERRORS,
    ePORTMAP_COUNTER_OVERSIZE,
    ePORTMAP_COUNTER_FRAGMENTS,
    ePORTMAP_COUNTER_JABBER,
    ePORTMAP_COUNTER_COLLISIONS,
    ePORTMAP_COUNTER_LATE_COLLISIONS,
    ePORTMAP_COUNTER_RX_PKT_64,
    ePORTMAP_COUNTER_RX_PKT_65_127,
    ePORTMAP_COUNTER_RX_PKT_128_255,
    ePORTMAP_COUNTER_RX_PKT_256_511,
    ePORTMAP_COUNTER_RX_PKT_512_1023,
    ePORTMAP_COUNTER_RX_PKT_1024_1522,
    ePORTMAP_COUNTER_RX_ERRORS,
    ePORTMAP_COUNTER_RX_DROPPED,
    ePORTMAP_COUNTER_RX_UCAST_PACKETS,
    ePORTMAP_COUNTER_RX_NOUCAST_PACKETS,
    ePORTMAP_COUNTER_TX_ERRORS,
    ePORTMAP_COUNTER_TX_NOUCAST_PACKETS,
    ePORTMAP_COUNTER_TX_UCAST_PACKETS,
    ePORTMAP_COUNTER_TX_MCAST_PACKETS,
    ePORTMAP_COUNTER_TX_BCAST_PACKETS,
    ePORTMAP_COUNTER_RX_UNDERSIZE_PACKETS,
    ePORTMAP_COUNTER_RX_UNKNOWN_PACKETS,
    ePORTMAP_COUNTER_RX_ALIGNMENT_PACKETS,
    ePORTMAP_COUNTER_RX_FCS_ERRORS,
    ePORTMAP_COUNTER_SQETEST_ERRORS,
    ePORTMAP_COUNTER_CSE_ERRORS,
    ePORTMAP_COUNTER_SYMBOL_ERRORS,
    ePORTMAP_COUNTER_TX_MAC_ERRORS,
    ePORTMAP_COUNTER_RX_MAC_ERRORS,
    ePORTMAP_COUNTER_TOO_LONG_ERRORS,
    ePORTMAP_COUNTER_SINGLE_COLLISIONS,
    ePORTMAP_COUNTER_MULTI_COLLISIONS,
    ePORTMAP_COUNTER_EXCESS_COLLISIONS,
    ePORTMAP_COUNTER_RX_UNKNOWN_OPCODE,
    ePORTMAP_COUNTER_TX_DEFERRED,
    ePORTMAP_COUNTER_RX_PAUSE_PACKETS,
    ePORTMAP_COUNTER_TX_PAUSE_PACKETS,
    ePORTMAP_COUNTER_RX_OVERSIZE,
    ePORTMAP_COUNTER_TX_OVERSIZE,
    ePORTMAP_COUNTER_RX_FRAGMENTS,
    ePORTMAP_COUNTER_TX_FRAGMENTS,
    ePORTMAP_COUNTER_RX_JABBER,
    ePORTMAP_COUNTER_TX_JABBER,
    ePORTMAP_IF_NAME,
    ePORTMAP_IF_MTU,
    ePORTMAP_IF_SPEED_MAX,
    ePORTMAP_IF_SPEED,
    ePORTMAP_IF_DUPLEX,
    ePORTMAP_IF_AUTONEG,
    ePORTMAP_IF_ENCAP,
    ePORTMAP_IF_MEDIUM,
    ePORTMAP_IF_INTERFACE_TYPE,
    ePORTMAP_IF_MDIX,
    ePORTMAP_IF_MDIX_STATUS,
    ePORTMAP_IF_LEARN,
    ePORTMAP_IF_IFILTER,
    ePORTMAP_IF_SELF_EFILTER,
    ePORTMAP_IF_LOCK,
    ePORTMAP_IF_DROP_ON_LOCK,
    ePORTMAP_IF_FWD_UNK,
    ePORTMAP_IF_BCAST_LIMIT,
    ePORTMAP_IF_ABILITY,
    ePORTMAP_IF_FLOWCONTROL,
    ePORTMAP_IF_ENABLE,
    ePORTMAP_IF_SFP_PRESENT,
    ePORTMAP_IF_SFP_TX_STATE,
    ePORTMAP_IF_SFP_RX_STATE,
    ePORTMAP_IF_DEFAULT_VLAN,
    ePORTMAP_IF_PRIORITY,
    ePORTMAP_IF_LED_STATE,
    ePORTMAP_IF_MEDIA_PARAMETERS,
    ePORTMAP_IF_MEDIA_DETAILS,
    ePORTMAP_IF_LOCAL_ADVERT,
    ePORTMAP_IF_MEDIA_TYPE,
    ePORTMAP_IF_SFP_PORT_TYPE,
    ePORTMAP_IF_MEDIA_CONFIG_TYPE,
    ePORTMAP_IF_MEDIA_ERROR_CONFIG_TYPE,
    ePORTMAP_IF_MEDIA_SGMII_FIBER,
    ePORTMAP_IF_TX_ENABLE,
    ePORTMAP_IF_SFP_PORT_NUMBERS,
    ePORTMAP_IF_AUTONEG_DUPLEX,
    ePORTMAP_IF_AUTONEG_SPEED ,
    ePORTMAP_IF_DDM_PARAMETERS, /**< Digital diagnostic monitoring parameters */
    ePORTMAP_IF_DDM_STATUS,     /**< Digital diagnostic monitoring status     */
    ePORTMAP_IF_DDM_SUPPORTED,  /**< Digital diagnostic monitoring Supported ?    */ 
    ePORTMAP_IF_HWADDRESS_GET,
    ePORTMAP_IF_ACTIVE_LINK,    /**< is Link Active*/

};

/* APIs */
/**
* @brief Read PORTMAP device attribute
*
* @param[in] p_priv points to the private context of the device
* @param[in] attrid id of the attribute
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] idx attribute's index in case the attribute id corresponds to array
* @param[out] p_value pointer to the buffer to hold returned attribute's value
* @param[in] size indicates size of the attribute
*
* @return	error code
*/
ccl_err_t portmap_attr_read(void *p_priv, b_u32 attrid, 
                            b_u32 offset, b_u32 offset_sz, 
                            b_u32 idx, b_u8 *p_value, b_u32 size);

/**
* @brief Write PORTMAP device attribute
*
* @param[in] p_priv points to the private context of the device
* @param[in] attrid id of the attribute
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] idx attribute's index in case the attribute id corresponds to array
* @param[in] p_value pointer to the buffer which holds written 
*            data value
* @param[in] size indicates size of the attribute
*
* @return	error code
*/
ccl_err_t portmap_attr_write(void *p_priv, b_u32 attrid, 
                             b_u32 offset, b_u32 offset_sz, 
                             b_u32 idx, b_u8 *p_value, b_u32 size);

#endif/* _DEV_PORTMAP_H_ */
