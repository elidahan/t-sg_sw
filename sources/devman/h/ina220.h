#ifndef _DEV_INA220_H_
#define _DEV_INA220_H_

/**
 * @name Enumerator to specify Current Monitor INA220 device 
 *       attributes
 */
enum ina220_attr_e {
	eINA220_CONFIG,             /**< Configuration */                              
    eINA220_SHUNTV,             /**< Shunt voltage measurement data */             
    eINA220_BUSV,               /**< Bus voltage measurement data */               
    eINA220_POWER,              /**< Power measurement data */                     
    eINA220_CURRENT,            /**< Current flowing through the shunt register */ 
    eINA220_CALIB,              /**< System calibration */
    eINA220_MEASUREMENT_DATA,   /**< Aggregation of the measurement data */
    eINA220_INIT,
    eINA220_CONFIGURE_TEST,
    eINA220_RUN_TEST,
};

/* APIs */
/**
* @brief Read Current Monitor INA220 device attribute
*
* @param[in] p_priv points to the private context of the device
* @param[in] attrid id of the attribute
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] idx attribute's index in case the attribute id corresponds to array
* @param[out] p_value pointer to the buffer to hold returned attribute's value
* @param[in] size indicates size of the attribute
*
* @return	error code
*/
ccl_err_t ina220_attr_read(void *p_priv, b_u32 attrid, 
                           b_u32 offset, b_u32 offset_sz, 
                           b_u32 idx, b_u8 *p_value, b_u32 size);

/**
* @brief Write Current Monitor INA220 device attribute
*
* @param[in] p_priv points to the private context of the device
* @param[in] attrid id of the attribute
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] idx attribute's index in case the attribute id corresponds to array
* @param[in] p_value pointer to the buffer which holds written 
*            data value
* @param[in] size indicates size of the attribute
*
* @return	error code
*/
ccl_err_t ina220_attr_write(void *p_priv, b_u32 attrid, 
                            b_u32 offset, b_u32 offset_sz, 
                            b_u32 idx, b_u8 *p_value, b_u32 size);

#endif/* _DEV_INA220_H_ */
