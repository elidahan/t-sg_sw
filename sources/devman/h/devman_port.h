/********************************************************************************/
/**
 * @file devman_port.h
 * @brief Ports definitions  
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#ifndef _DEVMAN_PORT_H_
#define _DEVMAN_PORT_H_


typedef enum {
	eDEVMAN_PORT_TYPE_ONBOARD,		
	eDEVMAN_PORT_TYPE_MEDIA_SLOT, /* SFP, XFP */
	eDEVMAN_PORT_TYPE_DUAL,
    eDEVMAN_PORT_TYPE_MODULE_SPECIFIC,
    eDEVMAN_PORT_TYPE_INTERNAL,
    eDEVMAN_PORT_TYPE_UNDEFINED
} devman_port_type_t;
#if 0
typedef enum {
	DEVMAN_IF_NAME = 0,
	DEVMAN_IF_MTU,
	DEVMAN_IF_SPEED_MAX,
	DEVMAN_IF_SPEED,
	DEVMAN_IF_DUPLEX,
	DEVMAN_IF_AUTONEG,
	DEVMAN_IF_ENCAP,
	DEVMAN_IF_MEDIUM,
	DEVMAN_IF_INTERFACE_TYPE,
	DEVMAN_IF_MDIX,
	DEVMAN_IF_MDIX_STATUS,
	DEVMAN_IF_LEARN,
    DEVMAN_IF_IFILTER,
    DEVMAN_IF_SELF_EFILTER,
    DEVMAN_IF_LOCK,
    DEVMAN_IF_DROP_ON_LOCK,
    DEVMAN_IF_FWD_UNK,
	DEVMAN_IF_BCAST_LIMIT,
	DEVMAN_IF_ABILITY,
	DEVMAN_IF_FLOWCONTROL,
	DEVMAN_IF_ENABLE,
	DEVMAN_IF_SFP_PRESENT,
	DEVMAN_IF_SFP_TX_STATE,
	DEVMAN_IF_SFP_RX_STATE,
    DEVMAN_IF_DEFAULT_VLAN,
    DEVMAN_IF_PRIORITY,
    DEVMAN_IF_LED_STATE,
	DEVMAN_IF_MEDIA_PARAMETERS,
	DEVMAN_IF_MEDIA_DETAILS,
	DEVMAN_IF_LOCAL_ADVERT,
    DEVMAN_IF_MEDIA_TYPE,
    DEVMAN_IF_SFP_PORT_TYPE,
    DEVMAN_IF_MEDIA_CONFIG_TYPE,
    DEVMAN_IF_MEDIA_ERROR_CONFIG_TYPE,
    DEVMAN_IF_MEDIA_SGMII_FIBER,
	DEVMAN_IF_TX_ENABLE,
	DEVMAN_IF_SFP_PORT_NUMBERS,
	DEVMAN_IF_AUTONEG_DUPLEX,
	DEVMAN_IF_AUTONEG_SPEED ,
    DEVMAN_IF_DDM_PARAMETERS, /**< Digital diagnostic monitoring parameters */
    DEVMAN_IF_DDM_STATUS,     /**< Digital diagnostic monitoring status     */
    DEVMAN_IF_DDM_SUPPORTED,  /**< Digital diagnostic monitoring Supported ?    */ 
    DEVMAN_IF_HWADDRESS_GET,
    DEVMAN_IF_ACTIVE_LINK,    /**< is Link Active*/
    DEVMAN_PROP_MAX
} devman_if_property_t;

#define DEVMAN_IF_PROPERTIES_STR \
{ \
    "if_name", \
    "if_mtu", \
    "if_speed_max", \
    "if_speed", \
    "if_duplex", \
    "if_autoneg", \
    "if_encap", \
    "if_medium", \
    "if_interface_type", \
    "if_mdix", \
    "if_mdix_status", \
    "if_learn", \
    "if_ifilter", \
    "if_self_efilter", \
    "if_lock", \
    "if_drop_on_lock", \
    "if_fwd_unk", \
    "if_bcast_limit", \
    "if_ability", \
    "if_flow_control", \
    "if_enable", \
    "if_sfp_present", \
    "if_sfp_tx_state", \
    "if_sfp_rx_state", \
    "if_default_vlan", \
    "if_priority", \
    "if_led_state", \
    "if_media_parameters", \
    "if_media_details", \
    "if_local_advert", \
    "if_media_type", \
    "if_sfp_port_type", \
    "if_media_config_type", \
    "if_media_error_config_type", \
    "if_media_sgmii_fiber", \
    "if_tx_enable", \
    "if_sfp_port_numbers", \
    "if_autoneg_duplex", \
    "if_autoneg_speed", \
    "if_ddm_parameters", \
    "if_ddm_status", \
    "if_ddm_supported", \
    "if_hwaddress_get", \
    "if_active_link" \
};
 
/** Interface type enumeration */
typedef enum {
    DEVMAN_IF_TYPE_PORT = 1,
    DEVMAN_IF_TYPE_LAG,
    DEVMAN_IF_TYPE_L2VLAN,
    DEVMAN_IF_TYPE_NOT_VALID
} devman_if_type_t;
#endif

#endif /* _DEVMAN_PORT_H_*/
