/********************************************************************************/
/**
 * @file devman_if_stat.h
 * @brief Ethernet interface statistics definitions header file
 * @copyright 2020 CELARE. All rights reserved.
 */
/********************************************************************************/
#ifndef _DEVMAN_IF_STAT_H_
#define _DEVMAN_IF_STAT_H_

#if 0
/**  Network interfaces MIB counters */
typedef enum
{
   DEVMAN_IF_MIB_BYTEREC = 0,
   DEVMAN_IF_MIB_BYTSENT,
   DEVMAN_IF_MIB_FRAREC,
   DEVMAN_IF_MIB_FRASENT,
   DEVMAN_IF_MIB_TOTALBYTEREC,
   DEVMAN_IF_MIB_TOTALFRAMEREC,
   DEVMAN_IF_MIB_BRDFRAREC,
   DEVMAN_IF_MIB_MULFRAREC,
   DEVMAN_IF_MIB_CRCERR,
   DEVMAN_IF_MIB_FRAOVERSIZE,
   DEVMAN_IF_MIB_FRAFRAGMENTS,
   DEVMAN_IF_MIB_JABBER,
   DEVMAN_IF_MIB_COLL,
   DEVMAN_IF_MIB_LATECOL,
   DEVMAN_IF_MIB_FRA64,
   DEVMAN_IF_MIB_FRA65T127,
   DEVMAN_IF_MIB_FRA128T255,
   DEVMAN_IF_MIB_FRA256T511,
   DEVMAN_IF_MIB_FRA512T1023,
   DEVMAN_IF_MIB_FRA1024T1522,
   DEVMAN_IF_MIB_MACRXERR,
   DEVMAN_IF_MIB_DROPPED,
   DEVMAN_IF_MIB_INUCASTPKT,
   DEVMAN_IF_MIB_INNOUCASTPKT,
   DEVMAN_IF_MIB_INERRORS,
   DEVMAN_IF_MIB_OUTERRORS,
   DEVMAN_IF_MIB_OUTNOUCASTPKT,
   DEVMAN_IF_MIB_OUTUCASTPKT,
   DEVMAN_IF_MIB_MULTIOUT,
   DEVMAN_IF_MIB_BROADOUT,
   DEVMAN_IF_MIB_UNDERSIZ,
   DEVMAN_IF_MIB_INUNKNOWNPROTOS,
   DEVMAN_IF_MIB_ALIGN_ERR,
   DEVMAN_IF_MIB_FCS_ERR,
   DEVMAN_IF_MIB_SQETEST_ERR,
   DEVMAN_IF_MIB_CSE_ERR,
   DEVMAN_IF_MIB_SYMBOL_ERR,
   DEVMAN_IF_MIB_MACTX_ERR,
   DEVMAN_IF_MIB_MACRX_ERR,
   DEVMAN_IF_MIB_TOOLONGFRA,
   DEVMAN_IF_MIB_SNGL_COLLISION,
   DEVMAN_IF_MIB_MULT_COLLISION,
   DEVMAN_IF_MIB_LATE_COLLISION,
   DEVMAN_IF_MIB_EXCESS_COLLISION,
   DEVMAN_IF_MIB_INUNKNOWNOPCODE,
   DEVMAN_IF_MIB_DEFERREDTX,
   DEVMAN_IF_MIB_INPAUSE_FRAMES,
   DEVMAN_IF_MIB_OUTPAUSE_FRAMES,
   DEVMAN_IF_MIB_INFRAOVERSIZE,
   DEVMAN_IF_MIB_OUTFRAOVERSIZE,
   DEVMAN_IF_MIB_INFRAFRAGMENTS,
   DEVMAN_IF_MIB_OUTFRAFRAGMENTS,
   DEVMAN_IF_MIB_INJABBERS,
   DEVMAN_IF_MIB_OUTJABBERS,
   DEVMAN_IF_MIB_MAX
} devman_if_counter_t;

#define DEVMAN_IF_COUNTERS_STR \
{ \
    "rx_bytes", \
    "tx_bytes", \
    "rx_packets", \
    "tx_packets", \
    "rx_total_bytes", \
    "rx_total_packets", \
    "rx_bcast_packets", \
    "rx_mcast_packets", \
    "rx_crc_errors", \
    "oversize", \
    "fragments", \
    "jabber", \
    "collisions", \
    "late_collistions", \
    "rx_packet_64_bytes", \
    "rx_packet_65_127_bytes", \
    "rx_packet_128_255_bytes", \
    "rx_packet_256_511_bytes", \
    "rx_packet_512_1023_bytes", \
    "rx_packet_1024_1522_bytes", \
    "rx_errors", \
    "rx_dropped", \
    "rx_ucast_packets", \
    "rx_noucast_packets", \
    "rx_errors", \
    "tx_errors", \
    "tx_noucast_packets", \
    "tx_ucast_packets", \
    "tx_mcast_packets", \
    "tx_bcast_packets", \
    "rx_undersize_packets", \
    "rx_uknown_protocols", \
    "rx_alignment_errors", \
    "rx_fcs_errors", \
    "sqetest_errors", \
    "cse_errors", \
    "symbol_errors", \
    "tx_mac_errors", \
    "rx_mac_errors", \
    "too_long_errors", \
    "single_collisions",\
    "multi_collisions", \
    "late_collisions", \
    "excess_collisions", \
    "rx_unknown_opcode", \
    "tx_deferred", \
    "rx_pause_packets", \
    "tx_pause_packets", \
    "rx_oversize", \
    "tx_oversize", \
    "rx_fragments", \
    "tx_fragments", \
    "rx_jabber", \
    "tx_jabber" \
};
#endif

#endif /* _DEVMAN_IF_STAT_H_ */

