#ifndef _DEV_M41ST87W_H_
#define _DEV_M41ST87W_H_

/**
 * @name Enumerator to specify M41ST87W Real Time Clock (RTC) 
 *       device attributes
 */
enum m41st87w_attr_e {
    eM41ST87W_I2C_BUS,
    eM41ST87W_I2C_ADDR,
    eM41ST87W_TENS,
    eM41ST87W_SECS,
    eM41ST87W_MINUTES,
    eM41ST87W_CENT_HOURS,
    eM41ST87W_DAY,
    eM41ST87W_DATE,
    eM41ST87W_MONTH,
    eM41ST87W_YEAR,
    eM41ST87W_CTRL,
    eM41ST87W_WATCHDOG,
    eM41ST87W_AL_MONTH,
    eM41ST87W_AL_DATE,
    eM41ST87W_AL_MIN,
    eM41ST87W_AL_SEC,
    eM41ST87W_FLAGS,
    eM41ST87W_SQW,
    eM41ST87W_TAMPER_1,
    eM41ST87W_TAMPER_2,
    eM41ST87W_TAMPER_3,
    eM41ST87W_SERIAL_NUM,
    eM41ST87W_BUFFER,             /**< Raw buffer */
    eM41ST87W_MEASUREMENT_DATA,   /**< Aggregation of the measurement data */
    eM41ST87W_INIT,
    eM41ST87W_CONFIGURE_TEST,
    eM41ST87W_RUN_TEST,
};

/* APIs */
/**
* @brief Read M41ST87W Real Time Clock (RTC) device attribute
*
* @param[in] p_priv points to the private context of the device
* @param[in] attrid id of the attribute
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] idx attribute's index in case the attribute id corresponds to array
* @param[out] p_value pointer to the buffer to hold returned attribute's value
* @param[in] size indicates size of the attribute
*
* @return	error code
*/
ccl_err_t rtc_attr_read(void *p_priv, b_u32 attrid, 
                        b_u32 offset, b_u32 offset_sz, 
                        b_u32 idx, b_u8 *p_value, b_u32 size);

/**
* @brief Write M41ST87W Real Time Clock (RTC) device attribute
*
* @param[in] p_priv points to the private context of the device
* @param[in] attrid id of the attribute
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] idx attribute's index in case the attribute id corresponds to array
* @param[in] p_value pointer to the buffer which holds written 
*            data value
* @param[in] size indicates size of the attribute
*
* @return	error code
*/
ccl_err_t rtc_attr_write(void *p_priv, b_u32 attrid, 
                         b_u32 offset, b_u32 offset_sz, 
                         b_u32 idx, b_u8 *p_value, b_u32 size);

#endif/* _DEV_M41ST87W_H_ */
