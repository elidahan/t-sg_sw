#ifndef _DEV_AXIGPIO_H_
#define _DEV_AXIGPIO_H_

/**
 * @name Enumerator to specify AXI GPIO device attributes 
 */
enum axigpio_attr_e {
    eAXIGPIO_DATA,
    eAXIGPIO_TRI,
    eAXIGPIO_DATA2,
    eAXIGPIO_TRI2,
    eAXIGPIO_GIE,
    eAXIGPIO_ISR,
    eAXIGPIO_IER,
    eAXIGPIO_INIT,
    eAXIGPIO_CONFIGURE_TEST,
    eAXIGPIO_RUN_TEST,
};

/* APIs */
/**
* @brief Read AXI GPIO device attribute
*
* @param[in] p_priv points to the private context of the device
* @param[in] attrid id of the attribute
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] idx attribute's index in case the attribute id corresponds to array
* @param[out] p_value pointer to the buffer to hold returned attribute's value
* @param[in] size indicates size of the attribute
*
* @return	error code
*/
ccl_err_t axigpio_attr_read(void *p_priv, b_u32 attrid, 
                            b_u32 offset, b_u32 offset_sz, 
                            b_u32 idx, b_u8 *p_value, b_u32 size);

/**
* @brief Write AXI GPIO device attribute
*
* @param[in] p_priv points to the private context of the device
* @param[in] attrid id of the attribute
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] idx attribute's index in case the attribute id corresponds to array
* @param[in] p_value pointer to the buffer which holds written 
*            data value
* @param[in] size indicates size of the attribute
*
* @return	error code
*/
ccl_err_t axigpio_attr_write(void *p_priv, b_u32 attrid, 
                             b_u32 offset, b_u32 offset_sz, 
                             b_u32 idx, b_u8 *p_value, b_u32 size);

#endif/* _DEV_AXIGPIO_H_ */
