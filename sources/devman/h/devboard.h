/********************************************************************************/
/**
 * @file devboard.h
 * @brief The board common definitions header file  
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#ifndef _BATM_DEVBOARD_H_
#define _BATM_DEVBOARD_H_

/* the board devices signatures */
#define BRDDEV_MAJOR_MASK       0xffff0000
#define BRDDEV_MINOR_MASK       0x0000ffff
#define BRDDEV_PORTMAP_MAJOR    0x00003fff
#define BRDDEV_CPLD_MAJOR       0x00000001
#define BRDDEV_I2CBUS_MAJOR     0x00000002
#define BRDDEV_AXISPI_MAJOR     0x00000003 /**< Xilinx AXI SPI core */
#define BRDDEV_I2CCORE_MAJOR    0x00000004 /**< Xilinx I2C core */
#define BRDDEV_AXIUART_MAJOR    0x00000005 /**< Xilinx AXI UART core */
#define BRDDEV_AXIGPIO_MAJOR    0x00000006 /**< Xilinx AXI GPIO core */
#define BRDDEV_INTC_MAJOR       0x00000007 /**< Xilinx Interrupt Controller core */
#define BRDDEV_XADC_MAJOR       0x00000008 /**< Xilinx Voltage/Temperature core */
#define BRDDEV_SYSMON_MAJOR     0x00000009 /**< Xilinx System Monitor core */
#define BRDDEV_1G_MAC_MAJOR     0x0000000a /**< Xilinx 1G MAC core */
#define BRDDEV_10G_MAC_MAJOR    0x0000000b /**< Xilinx 10G MAC core */
#define BRDDEV_100G_MAC_MAJOR   0x0000000c /**< Xilinx 100G MAC core */
#define BRDDEV_ETH_SWITCH_MAJOR 0x0000000d /**< Comcores Ethernet switch */
#define BRDDEV_LCD_MAJOR        0x0000000f /**< LCD display - LK204-7T-1U */
#define BRDDEV_I2CIO_MAJOR      0x00000010 /**< I2C IO expander */
#define BRDDEV_PCA9548_MAJOR    0x00000011 /**< I2C switch */
#define BRDDEV_TRANSR_MAJOR     0x00000012 /**< Transceiver */
#define BRDDEV_MAILBOX_MAJOR    0x00000013 /**< CPLD mailbox */
#define BRDDEV_RTC_MAJOR        0x00000014 /**< RTC */
#define BRDDEV_FAN_MAJOR        0x00000015 /**< PWM fan controller */
#define BRDDEV_FPGA_MAJOR       0x00000016 /**< main FPGA */
#define BRDDEV_POWR1220_MAJOR   0x00000017 /**< Power Sequencer - ispPAC-POWR1220AT8 */
#define BRDDEV_PMBUS_MAJOR      0x00000018 /**< PMBus */
#define BRDDEV_LM75_MAJOR       0x00000019 /**< Temperature sensor */
#define BRDDEV_EEPROM_MAJOR     0x0000001a /**< Serial EEPROM */
#define BRDDEV_CURRMON_MAJOR    0x0000001b /**< Current Monitor */
#define BRDDEV_SPIDEV_MAJOR     0x0000001c /**< SPI Flash */
#define BRDDEV_SMARTCARD_MAJOR  0x0000001d /**< Smart Card */
#define BRDDEV_ATP_MAJOR        0x0000001e /**< ATP (sw testing) module */
#define BRDDEV_CPU_MAJOR        0x0000001f /**< CPU */
#define BRDDEV_DRAMTESTER_MAJOR 0x00000020 /**< DRAM TESTER */
#define BRDDEV_ESWITCH_MAJOR    0x00000021 /**< Comcores Ethernet Switch */
#define BRDDEV_HAL_MAJOR        0x00000022 /**< HAL (hal layer sw testing) module */



#define DEVBRD_PORTS_MAXNUM     64
#define DEVBRD_CURRMON_MAXNUM   6
#define DEVBRD_PMBUS_MAXNUM     13
#define DEVBRD_FANCTLS_MAXNUM   3
#define DEVBRD_RTC_MAXNUM       3
#define DEVBRD_XADC_MAXNUM      1
#define DEVBRD_SYSMON_MAXNUM    1
#define DEVBRD_DRAM_MAXNUM      3

typedef enum {
    eDEVBRD_OP_MODE_POLL = 0,
    eDEVBRD_OP_MODE_INTR = 1
} op_mode_t;

/* i2c device routing */
/* CPU i2c bus IDs */
typedef enum {
    eDEVBRD_I2C_SYS_BUS_0,
    eDEVBRD_I2C_SYS_BUS_1,
    eDEVBRD_I2C_SYS_BUS_2,
    eDEVBRD_I2C_SYS_BUS_3,
    eDEVBRD_I2C_SYS_BUS_4,
    eDEVBRD_I2C_SYS_BUSES_NUM,
} i2c_cpu_bus_id_e;

/* FPGA i2c bus IDs */
typedef enum {
    eDEVBRD_I2C_BUS_0,
    eDEVBRD_I2C_BUS_1,
    eDEVBRD_I2C_BUS_2,
    eDEVBRD_I2C_BUS_3,
    eDEVBRD_I2C_BUS_4,
    eDEVBRD_I2C_BUS_5,
    eDEVBRD_I2C_BUSES_NUM
} i2c_bus_id_e;

#define DEVBRD_I2C_MUXES_IN_CHAIN_MAXNUM    4

typedef struct {
    b_u8         bus_id;
    b_u16        dev_i2ca;
    b_u16        mux_i2ca[DEVBRD_I2C_MUXES_IN_CHAIN_MAXNUM];
    b_u8         mux_pin[DEVBRD_I2C_MUXES_IN_CHAIN_MAXNUM];
} i2c_route_t;

/* board specific info for the I2CBUS device */
#define DEVBRD_I2CDEVS_MAXNUM    32
#define DEVBRD_I2CBUSES_MAXNUM   5
#define DEVBRD_I2CDEVS_NAMELEN   32
typedef struct {
    b_u8   bus_id;
    b_u8   slaves_num;
    char   slave_name[DEVBRD_I2CDEVS_MAXNUM][DEVBRD_I2CDEVS_NAMELEN];
    b_u16  slave_i2ca[DEVBRD_I2CDEVS_MAXNUM];
} i2cbus_brdspec_t;

#define DEVBRD_I2CCORE_MUX_MAXNUM 32

typedef struct {
    b_u8       devname[DEVMAN_DEVNAME_LEN];
    b_u8       dev_id;
    b_u32      slaves_num[DEVBRD_I2CBUSES_MAXNUM];
    char       slave_name[DEVBRD_I2CBUSES_MAXNUM][DEVBRD_I2CDEVS_MAXNUM][DEVBRD_I2CDEVS_NAMELEN];
    b_u16      slave_i2ca[DEVBRD_I2CBUSES_MAXNUM][DEVBRD_I2CDEVS_MAXNUM];
    op_mode_t  op_mode;
    b_u32      irq;
    b_u32      offset;
    b_u8       name[DEVMAN_DEVNAME_LEN];
} i2ccore_brdspec_t;

typedef enum {
    eDEVBRD_INTC_DEV_ARTIX_0,
    eDEVBRD_INTC_DEV_ARTIX_1,
    eDEVBRD_INTC_DEV_VIRTEX,
    eDEVBRD_INTC_DEV_NUM
} intc_dev_id_e;

/**
 * @name Enumerator to specify Interrupt Controller mode
 * @{
 */
typedef enum {
    eDEVBRD_INTC_MODE_NOCASCADE  = 	0,	/**< Normal - No Cascade Mode */
    eDEVBRD_INTC_MODE_PRIMARY    = 	1,	/**< Master/Primary controller */
    eDEVBRD_INTC_MODE_SECONDARY  = 	2,	/**< Secondary Slave Controllers */
    eDEVBRD_INTC_MODE_LAST       =  3   /**< Last Slave Controller */
} intc_mode_t;

/**
 * @name Configuration options
 */
#define eDEVBRD_INTC_SVC_SGL_ISR_OPTION  1UL            /**< Service the highest priority pending
 				                                          *  interrupt and then return 
                                                          */
#define eDEVBRD_INTC_SVC_ALL_ISRS_OPTION 2UL            /**< Service all of the  pending
 				                                          *  interrupts and then return 
                                                          */ 
/**
 * @name Start modes
 */
#define INTC_SIMULATION_MODE     0              /**< Simulation only operating mode, no 
                                                  *  hardware interrupts recognized 
                                                  */
#define INTC_REAL_MODE           1              /**< Real operating  mode, no simulation allowed, 
                                                  *  hardware interrupts recognized 
                                                  */

/* board specific info for the Interrupt Controller device */
typedef struct {
    b_u8              devname[DEVMAN_DEVNAME_LEN];
    intc_mode_t       intc_mode;
    b_u8              isr_option;
    b_u8              op_mode;
    b_u8              dev_id;
    b_u8              irq;
    int               offset;
    b_u8              name[DEVMAN_DEVNAME_LEN];
} intc_brdspec_t;

/* board specific info for the basic i2c devices */
typedef struct {
    b_u8    bus_id;
    b_u16   dev_i2ca;
    b_u16   mux_i2ca[DEVBRD_I2C_MUXES_IN_CHAIN_MAXNUM];
    b_u8    mux_pin[DEVBRD_I2C_MUXES_IN_CHAIN_MAXNUM];
} i2c_dev_brdspec_t;

#define DEVBRD_TRANS_RS_ID_MAXNUM 2

#define DEVBRD_EEPROM_PARAM_DESC_LEN 50

/* eeprom fields indexes */
typedef enum {
    eDEVBRD_EEPROM_FIELD_BOARD_SN,     /**< Board Serial Number */
    eDEVBRD_EEPROM_FIELD_ASSEMBLY_NUM, /**< Assembly Number */
    eDEVBRD_EEPROM_FIELD_HW_REV,       /**< Hardware Revision */
    eDEVBRD_EEPROM_FIELD_HW_SUB_REV,   /**< Hardware Subrevision */
    eDEVBRD_EEPROM_FIELD_PART_NUM,     /**< Part Number */
    eDEVBRD_EEPROM_FIELD_CLEI,         /**< CLEI */
    eDEVBRD_EEPROM_FIELD_MFG_DATE,     /**< Manufacturing Date */
    eDEVBRD_EEPROM_FIELD_LICENSE,      /**< License value */
    eDEVBRD_EEPROM_FIELD_DEV_TYPE_ID,  /**< Device type ID */
    eDEVBRD_EEPROM_FIELDS_MAXNUM      
} eeprom_field_t;

typedef  struct {
    b_u16 offset;                                   /**< Field offset */
	b_u16 size;                                     /**< Field size */
	void *defval;                                   /**< Field's default value */
	char param_descr[DEVBRD_EEPROM_PARAM_DESC_LEN]; /**< Field's description */
} eeprom_param_t;

typedef struct {
    b_u16 sba0;   /**< Set bank address 0 */
    b_u16 sba1;   /**< Set bank address 1 */
    b_u16 rba;    /**< Read bank address */
    b_u32 bsize;  /**< Bank size */
    b_u32 ndummy; /**< Number of dummy bytes for setting bank address */
} spd_ctrl_t;

/* board specific info for the EEPROM device */
typedef struct {
    i2c_dev_brdspec_t i2c;
    b_u8              addr_width;      /**< Address width */
    b_u16             size;            /**< EEPROM size */
    b_u8              page_size;       /**< Page size */
    b_u8              extra_mux_pin;
    eeprom_param_t    param[eDEVBRD_EEPROM_FIELDS_MAXNUM];  
    b_u8              name[DEVMAN_DEVNAME_LEN];
    b_bool            is_spd;     /**< Indicates if this Serial Presense Detect compliant device */
    spd_ctrl_t        spd_ctrl;
} eeprom_brdspec_t;

/* board specific info for the Current Monitor device */
typedef struct {
    i2c_dev_brdspec_t i2c;
    b_double64        rshunt;          /**< Shunt resistance */
    b_double64        current_lsb;     /**< current measurement resolution in Amperes */  
    b_u8              extra_mux_pin;
    b_u32             current_max;     //TODO: remove this pmbus i2c problem workaround
    b_u8              name[DEVMAN_DEVNAME_LEN];
} currmon_brdspec_t;

/* board specific info for the Fan Controller device */
typedef struct {
    i2c_dev_brdspec_t i2c;
    b_u8              pulse_num;      /**< Number of pulses per revolution */
    b_u8              extra_mux_pin;
    b_u8              name[DEVMAN_DEVNAME_LEN];
} fan_brdspec_t;

#define DEVBRD_PMBUS_CMDS_NUM    256
#define DEVBRD_PMBUS_CRC_VAL_NUM 256

typedef struct {
    b_bool            is_supported;
    b_bool            size;
    b_u16             extra_i2ca;
} pmbus_cmd_t;
/* board specific info for the PMBUS device */
typedef struct {
    i2c_dev_brdspec_t i2c;
    b_u8              extra_mux_pin;  
    pmbus_cmd_t       cmd[DEVBRD_PMBUS_CMDS_NUM];
    b_u8              crc_tbl[DEVBRD_PMBUS_CRC_VAL_NUM];
    b_i32             exponent;
    b_bool            use_pec;          /**< packet error code */
    b_u32             vout_max;         //TODO: remove this pmbus i2c problem workaround
    b_u32             iout_max;         //TODO: remove this pmbus i2c problem workaround
    b_u32             vin_max;          //TODO: remove this pmbus i2c problem workaround
    b_u8              name[DEVMAN_DEVNAME_LEN];
} pmbus_brdspec_t;

/* board specific info for the TRANS device */
typedef struct {
    i2c_dev_brdspec_t i2c;
    b_u16             extra_i2ca;
    b_u8              extra_mux_pin;  
    b_bool            is_qsfp;
    b_u16             native;
//    b_u8              name[DEVMAN_DEVNAME_LEN];
} trans_brdspec_t;

#define CHANNELS_PER_PORT_MAXNUM 4 

typedef struct {
    char        vendor_name[DEVMAN_DEVNAME_LEN];
    char        vendor_pn[DEVMAN_DEVNAME_LEN];
    char        vendor_sn[DEVMAN_DEVNAME_LEN];
    b_double64  temperature;
    b_double64  vcc;
    b_double64  tx_bias[CHANNELS_PER_PORT_MAXNUM];
    b_double64  tx_power[CHANNELS_PER_PORT_MAXNUM];
    b_double64  rx_power[CHANNELS_PER_PORT_MAXNUM];
    b_u8        channel_num;
} media_decoder_t;

#define eDEVBRD_MBOX_NUM 7

/** board specific info for the Mailbox device */
typedef struct {
    b_u8              devname[DEVMAN_DEVNAME_LEN];
    i2c_dev_brdspec_t i2c;
    b_u32             dev_id;
    b_u32             offset;
    b_u8              name[DEVMAN_DEVNAME_LEN];
} mbox_brdspec_t;

/* board specific info for generic I2C device */
typedef struct {
    i2c_dev_brdspec_t i2c;
    b_u8              addr_width;      /**< Address width */
    b_u8              extra_mux_pin;  
    b_u8              name[DEVMAN_DEVNAME_LEN];
} gen_i2c_dev_brdspec_t;

/* AXI SPI devices IDs */
typedef enum {
    eDEVBRD_AXISPI_0,
    eDEVBRD_AXISPI_1,
    eDEVBRD_AXISPI_2,
    eDEVBRD_AXISPI_3,
    eDEVBRD_AXISPIS_NUM
} spi_dev_id_e;

#define DEVBRD_SPIDEVS_MAXNUM   32
#define DEVBRD_SPIDEVS_NAMELEN  32

typedef struct {
    b_u8              devname[DEVMAN_DEVNAME_LEN];
    b_u32             dev_id;
    b_u8              slaves_num;
    char              slave_name[DEVBRD_SPIDEVS_MAXNUM][DEVBRD_SPIDEVS_NAMELEN];
    b_u32             slave_cs[DEVBRD_SPIDEVS_MAXNUM];
    op_mode_t         op_mode;
    b_u8              irq;
    b_u32             offset;   
    b_u8              name[DEVMAN_DEVNAME_LEN];
} axispi_brdspec_t;

/* board specific info for generic spi device */
typedef struct {
    b_u8              addr_width;
    b_u32             page_size;
    b_u32             page_width;
    b_u32             sector_size;
    b_u32             bloff;      /**< bootloader offset */
    b_u8              sector_erase_cmd;
    b_u8              sector_erase_cmd_width;
    b_bool            is_nand;
    b_u32             burst_l;
    b_u32             dev_id;
    b_u8              cs;  
    b_u8              name[DEVMAN_DEVNAME_LEN];
} gen_spi_dev_brdspec_t;

/* spi route */
typedef struct {
    b_u32        dev_id;
    b_u8         slave_id;
} spi_route_t;

typedef struct {
    b_u8         uart_id;
    b_u8         name[DEVMAN_DEVNAME_LEN];
} smartcard_dev_brdspec_t;

#define eDEVBRD_AXIUART_NO_PARITY    0
#define eDEVBRD_AXIUART_PARITY_EVEN  1
#define eDEVBRD_AXIUART_PARITY_ODD   2

#define eDEVBRD_AXIUART_1_STOP_BITS  1
#define eDEVBRD_AXIUART_2_STOP_BITS  2

#define eDEVBRD_AXIUART_6_DATA_BITS	 1 /**< 6 Data bits selection */
#define eDEVBRD_AXIUART_7_DATA_BITS	 2 /**< 7 Data bits selection */
#define eDEVBRD_AXIUART_8_DATA_BITS	 3 /**< 8 Data bits selection */

/* AXI UART devices IDs */
typedef enum {
    eDEVBRD_AXIUART_0,
    eDEVBRD_AXIUART_1,
    eDEVBRD_AXIUART_2,
    eDEVBRD_AXIUART_3,
    eDEVBRD_AXIUART_4,
    eDEVBRD_AXIUART_5,
    eDEVBRD_AXIUART_6,
    eDEVBRD_AXIUART_7,
    eDEVBRD_AXIUART_8,
    eDEVBRD_AXIUARTS_NUM
} uart_dev_id_e;

typedef struct {
    b_u8              devname[DEVMAN_DEVNAME_LEN];
    b_u8              stop_bits;
    b_u8              data_bits;  
    b_u8              parity;
    b_u32             baudrate;
    b_u32             input_clk;
    b_u32             dev_id;
    op_mode_t         op_mode;
    b_u8              irq;
    b_u32             offset;   
    b_bool            istest;  /**< used to identify if specific device 
                                 *  can be tested on specific board 
                                 */
    b_u8              name[DEVMAN_DEVNAME_LEN];
} axiuart_brdspec_t;

#define DEVBRD_PORT_TRANS_NONE  0xdeafffff
/** board specific info for the PORTMAP device */
typedef struct {
    b_u32   ports_num;
    b_u32   port_types[DEVBRD_PORTS_MAXNUM];
    b_u32   port_id[DEVBRD_PORTS_MAXNUM];
    b_u32   trans_id[DEVBRD_PORTS_MAXNUM];
    char    name[DEVBRD_PORTS_MAXNUM][DEVMAN_DEVNAME_LEN];
    b_u32   offset[DEVBRD_PORTS_MAXNUM];
} portmap_brdspec_t;

typedef enum {
    eDEVBRD_IFTYPE_RGMII = 1,
    eDEVBRD_IFTYPE_SGMII,
    eDEVBRD_IFTYPE_R,
    eDEVBRD_IFTYPE_KR
} iftype_t;

typedef struct {
    char name[DEVMAN_ATTRS_NAME_LEN];
    b_u32 offset;
    b_u32 value;
    b_u32 value_prev;
} eth_counter_t;

typedef struct {
    b_u32 ifmib;
    b_u32 offset;
} mib_counter_map_t;

#define CALIB_PARAMS_PER_CHANNEL_NUM 3

enum {
    CALIB_PARAM_TXPOST_IDX,
    CALIB_PARAM_TXPRE_IDX,
    CALIB_PARAM_TXDIFFSWING_IDX,
    CALIB_PARAM_RXTERM_IDX,
    CALIB_PARAM_AUTONEG_IDX,
};

typedef struct {
    char        name[DEVMAN_ATTRS_NAME_LEN];
    b_u32       offset;
    b_double64  value;
    b_u8        encoded_value;
} calib_param_t;

typedef struct {
    calib_param_t calib[CALIB_PARAMS_PER_CHANNEL_NUM];
} channel_calib_params_t;

typedef struct {
    b_bool                  enable;
    b_u32                   channel_num;
    channel_calib_params_t  channel_calib_params[CHANNELS_PER_PORT_MAXNUM];
} port_calib_ctl_t;

#define COUNTERS_PER_PORT_MAXNUM 6

typedef struct {
    char        name[DEVMAN_ATTRS_NAME_LEN];
    b_u32       offset;
} counter_param_t;

#define COUNTER_CTL_ROLLOVER_FREE_RUNNING_MODE 0x0
#define COUNTER_CTL_ROLLOVER_OVERFLOW_MODE     0x1
#define COUNTER_CTL_ROLLOVER_STOP_ON_MAX_MODE  0x2
#define COUNTER_CTL_ROLLOVER_FREEZE_ALL_MODE   0x3

typedef struct {
    b_u32 tx_pkt_rst_mask;
    b_u32 rx_pkt_rst_mask;
    b_u32 rx_bad_pkt_rst_mask;
    b_u32 freeze_counters_mask;
    b_u32 rollover_mask;
    b_u32 rollover_mode;
} counter_ctl_t;

typedef struct {
    b_bool           enable;
    b_u8             width; /**< counter width */
    b_u32            ctl_offset;
    counter_ctl_t    ctl;
    counter_param_t  counter[COUNTERS_PER_PORT_MAXNUM];
} port_counter_ctl_t;

#define AUTONEG_PARAMS_PER_PORT_NUM 1

typedef struct {
    char        name[DEVMAN_ATTRS_NAME_LEN];
    b_u32       offset;
    b_u32       value;
} autoneg_param_t;

typedef struct {
    b_bool           enable;
    autoneg_param_t  autoneg[AUTONEG_PARAMS_PER_PORT_NUM];
} port_autoneg_ctl_t;

/** board specific info for the ETH device */
typedef struct {
    b_u8                devname[DEVMAN_DEVNAME_LEN]; 
    b_u32               portid;                          
    b_u8                irq;                             
    b_u32               offset;                          
    b_u8                phy_addr;                        
    iftype_t            iftype;                          
    port_calib_ctl_t    calib_ctl;  
    port_autoneg_ctl_t  autoneg_ctl;                     
    port_counter_ctl_t  counter_ctl;
    b_u8                name[DEVMAN_DEVNAME_LEN];
} eth_brdspec_t;

#define DEVBRD_GPIO_PIN_PER_CHANNEL_MAXNUM 32
#define DEVBRD_GPIO_CHANNELS_MAXNUM        2
#define DEVBRD_GPIO_PIN_MAXNUM             DEVBRD_GPIO_PIN_PER_CHANNEL_MAXNUM * \
                                           DEVBRD_GPIO_CHANNELS_MAXNUM

/**
 * @name Enumerator to specify GPIO direction
 * @{
 */
typedef enum {
    eDEVBRD_AXIGPIO_DIR_INPUT      = 0,	/**< GPIO input */
    eDEVBRD_AXIGPIO_DIR_OUTPUT     = 1,	/**< GPIO output */
    eDEVBRD_AXIGPIO_DIR_BIDI       = 2, /**< Bidirectional */
    eDEVBRD_AXIGPIO_DIR_NOT_VALID  = 3
} axigpio_dir_t;

/** board specific info for the AXI GPIO device */
typedef struct {
    b_u8   devname[DEVMAN_DEVNAME_LEN];
    b_u32  pins_num;
    b_bool direction[DEVBRD_GPIO_PIN_MAXNUM];
    b_u8   irq;
    b_u32  offset;
    b_u8   name[DEVMAN_DEVNAME_LEN];
} axigpio_brdspec_t;

/** board specific info for the XADC device */
typedef struct {
    b_u8  devname[DEVMAN_DEVNAME_LEN];
    b_u8  irq;
    b_u32 offset;
    b_u8  name[DEVMAN_DEVNAME_LEN];
} xadc_brdspec_t;

/** board specific info for the SYSMON device */
typedef struct {
    b_u8  devname[DEVMAN_DEVNAME_LEN];
    b_u8  irq;
    b_u32 offset;
    b_u8  name[DEVMAN_DEVNAME_LEN];
} sysmon_brdspec_t;

/** board specific info for a transceiver */
typedef struct {
    b_bool          present;
    media_decoder_t media_decoder;
} trans_info_t;

/** board specific info for the SPI bus */
typedef struct {
    b_u8  spi_bus_id;
    b_u8  spi_cs_id;
    b_u8  name[DEVMAN_DEVNAME_LEN];
} spibus_brdspec_t;

/** board specific info for the Ethernet Switch device */
typedef struct {
    b_u8  devname[DEVMAN_DEVNAME_LEN];
    b_u8  irq;
    b_u32 offset;
    b_u32 port_num;
    char  port_name[DEVBRD_PORTS_MAXNUM][DEVMAN_DEVNAME_LEN];
    b_u8  name[DEVMAN_DEVNAME_LEN];
} eswitch_brdspec_t;

/** board specific info for the CPU */
typedef struct {
    sys_sensors_t sys_sensors;
    b_u8          name[DEVMAN_DEVNAME_LEN];
} cpu_brdspec_t;

/** board specific info for the DRAM TESTER device */
typedef struct {
    b_u8   devname[DEVMAN_DEVNAME_LEN];
    b_u32  slot;
    b_u32  offset;
    b_u8   name[DEVMAN_DEVNAME_LEN];
} dramtester_brdspec_t;

/** data structure describing port counter */
typedef struct {
    char  name[DEVMAN_ATTRS_NAME_LEN];
    b_u64 value;
    b_u64 value_prev;
    b_u64 delta;
} port_counter_t;

#define PORT_STATS_COUNTER_NUM 26
/** data structure describing port statistics */
typedef struct {
    char                      name[DEVMAN_ATTRS_NAME_LEN];
    trans_info_t              trans_info;
    port_counter_t            tx_bytes;
    port_counter_t            tx_packets;                  
    port_counter_t            tx_packets_64;               
    port_counter_t            tx_packets_65_127;           
    port_counter_t            tx_packets_128_255;          
    port_counter_t            tx_packets_256_511;          
    port_counter_t            tx_packets_512_1023;         
    port_counter_t            tx_packets_1024_max;         
    port_counter_t            tx_bcast;                    
    port_counter_t            tx_mcast;                    
    port_counter_t            tx_packets_oversize;         
    port_counter_t            tx_packets_underrun_err;             
    port_counter_t            rx_bytes;                    
    port_counter_t            rx_packets;                  
    port_counter_t            rx_packets_64;               
    port_counter_t            rx_packets_65_127;           
    port_counter_t            rx_packets_128_255;          
    port_counter_t            rx_packets_256_511;          
    port_counter_t            rx_packets_512_1023;         
    port_counter_t            rx_packets_1024_max;         
    port_counter_t            rx_bcast;                    
    port_counter_t            rx_mcast;                    
    port_counter_t            rx_packets_oversize;          
    port_counter_t            rx_packets_undersize;         
    port_counter_t            rx_packets_frag;              
    port_counter_t            rx_packets_fcs_err;           
} port_stats_t;

/** data structure describing current monitor statuses */
typedef struct {
    char                      name[DEVMAN_ATTRS_NAME_LEN];
    b_double64                bus_voltage;
    b_double64                current;                       
} currmon_stats_t;

/** data structure describing pmbus device statuses */
typedef struct {
    char                      name[DEVMAN_ATTRS_NAME_LEN];
    b_double64                vin;
    b_double64                iin;
    b_double64                vout;                       
    b_double64                iout;                       
} pmbus_stats_t;

/** data structure describing fan controller statuses */
typedef struct {
    char                      name[DEVMAN_ATTRS_NAME_LEN];
    b_u32                     temp_local;
    b_u32                     temp_remote1;
    b_u32                     temp_remote2;                       
    b_u32                     speed_rpm1;        /**< fan speed measured in RPMs */
    b_u32                     speed_rpm2;        /**< fan speed measured in RPMs */
    b_u32                     speed_rpm3;        /**< fan speed measured in RPMs */
} fanctl_stats_t;

/** data structure describing RTC statuses */
typedef struct {
    char                      name[DEVMAN_ATTRS_NAME_LEN];
    b_u32                     hours;
    b_u32                     minutes;
    b_u32                     seconds;
} rtc_stats_t;

#define DEVBRD_SPIDEV_JEDEC_ID_SIZE 5

/** data structure describing SPIDEV statuses */
typedef struct {
    char                      name[DEVMAN_ATTRS_NAME_LEN];
    b_u8                      jedec[DEVBRD_SPIDEV_JEDEC_ID_SIZE];
} spidev_stats_t;

/** data structure describing XADC statuses */
typedef struct {
    char                      name[DEVMAN_ATTRS_NAME_LEN];
    b_double64                temp;
    b_double64                vccint;
    b_double64                vccaux;
    b_double64                vbram;
} xadc_stats_t;

/** data structure describing SYSMON statuses */
typedef struct {
    char                      name[DEVMAN_ATTRS_NAME_LEN];
    b_double64                temp;
    b_double64                vccint;
    b_double64                vccaux;
    b_double64                vbram;
} sysmon_stats_t;

typedef struct {
    char                      name[DEVMAN_ATTRS_NAME_LEN];
    sys_res_usages_stat_t     res_usage;
    sensor_t                  voltage_sensors[SENSORS_MAXNUM];
    b_u32                     voltage_sensors_num;
    sensor_t                  temp_sensors[SENSORS_MAXNUM];
    b_u32                     temp_sensors_num;
} cpu_stats_t;

typedef struct {
    char                      name[DEVMAN_ATTRS_NAME_LEN];
    char                      boot_test_status[DEVMAN_DEVNAME_LEN];
    char                      custom_test_status[DEVMAN_DEVNAME_LEN];
    char                      prbs_test_status[DEVMAN_DEVNAME_LEN];
} dram_stats_t;

typedef struct {
    char                      module_sn[DEVMAN_DEVNAME_LEN];
    char                      module_pn[DEVMAN_DEVNAME_LEN];
    char                      manufacturer_id[DEVMAN_DEVNAME_LEN];
} eeprom_spd_info_t;

typedef struct {
    char                      name[DEVMAN_ATTRS_NAME_LEN];
    eeprom_spd_info_t         eeprom_spd_info;
} eeprom_spd_stats_t;

/** statistics and statuses of various devices */
typedef struct {
    cpu_stats_t               cpu_stats;
    port_stats_t              port_stats[DEVBRD_PORTS_MAXNUM];
    b_u32                     port_num;
    currmon_stats_t           currmon_stats[DEVBRD_CURRMON_MAXNUM];
    b_u32                     currmon_num;
    pmbus_stats_t             pmbus_stats[DEVBRD_PMBUS_MAXNUM];
    b_u32                     pmbus_num;
    fanctl_stats_t            fanctl_stats[DEVBRD_FANCTLS_MAXNUM];
    b_u32                     fanctls_num;
    rtc_stats_t               rtc_stats[DEVBRD_RTC_MAXNUM];   
    b_u32                     rtc_num;
    spidev_stats_t            spidev_stats[DEVBRD_SPIDEVS_MAXNUM];
    b_u32                     spidev_num;
    xadc_stats_t              xadc_stats[DEVBRD_XADC_MAXNUM];
    b_u32                     xadc_num;
    sysmon_stats_t            sysmon_stats[DEVBRD_SYSMON_MAXNUM];
    b_u32                     sysmon_num;
    dram_stats_t              dram_stats[DEVBRD_DRAM_MAXNUM];
    b_u32                     dram_num;
    eeprom_spd_stats_t        eeprom_spd_stats[DEVBRD_DRAM_MAXNUM];
    b_u32                     eeprom_spd_num;
} board_dev_stats_t;

/** board specific info for the ATP device */
typedef struct {
    board_dev_stats_t board_stats;
} atp_brdspec_t;

/** board specific info for the ATP device */
typedef struct {
    char              name[DEVMAN_ATTRS_NAME_LEN];
} hal_brdspec_t;

/** board specific union */
typedef union {
    spibus_brdspec_t           spibus;    
    i2cbus_brdspec_t           i2cbus;    
    axispi_brdspec_t           axispi;    
    i2ccore_brdspec_t          i2ccore;   
    axiuart_brdspec_t          axiuart;   
    axigpio_brdspec_t          axigpio;   
    intc_brdspec_t             intc;      
    mbox_brdspec_t             mbox;      
    pmbus_brdspec_t            pmbus;     
    eeprom_brdspec_t           eeprom;    
    currmon_brdspec_t          currmon;   
    fan_brdspec_t              fan;       
    gen_i2c_dev_brdspec_t      rtc;       
    gen_i2c_dev_brdspec_t      lcd;       
    gen_i2c_dev_brdspec_t      i2cio;     
    gen_spi_dev_brdspec_t      spidev;    
    trans_brdspec_t            trans;     
    xadc_brdspec_t             xadc;      
    sysmon_brdspec_t           sysmon;    
    portmap_brdspec_t          portmap;   
    eth_brdspec_t              temac;     
    eth_brdspec_t              hses10g;   
    eth_brdspec_t              usplus100g;
    smartcard_dev_brdspec_t    smartcard; 
    eswitch_brdspec_t          eswitch;
    atp_brdspec_t              atp;   
    cpu_brdspec_t              cpu;    
    dramtester_brdspec_t       dramtester;
    hal_brdspec_t              hal;   
} dev_brdspec_u;

#define DEVBRD_DEVNAME_LEN 32
/** board available devices description */
typedef struct {
    char            type_name[DEVBRD_DEVNAME_LEN];
    char            subtype_name[DEVBRD_DEVNAME_LEN];
    b_u32           sig;
    dev_brdspec_u   spec;
    devo_t          devo;
} board_device_descr_t;

/** Initialize all board types/devices
 *  @param[in] bmode - board operational mode
 *  @return CCL_OK on success, CCL_FAIL on failure
 */
extern ccl_err_t devboard_init(b_u32 bmode);

/** Deinitialize all board devices/types
 *  @return CCL_OK on success, CCL_FAIL on failure
 */
extern ccl_err_t devboard_fini(void);

/** Get appropriate device/attr/significant_mask for port "media_pres" operating
 *  @param[in] logport - logical port number
 *  @param[out] dev_name - device 
 *  @param[out] attr_name - attribute
 *  @param[out] p_sign_mask - significant bits mask
 *  @param[out] p_is_active_hi - active high/low flag
 *  @return CCL_OK on success, error code on failure 
 */
extern ccl_err_t devboard_portprs_to_devattr(int logport, char dev_name[], 
                                             char attr_name[], b_u32 *p_sign_mask, 
                                             int *p_is_active_hi);

/** Get appropriate device/attr/significant_mask for port "led" operating
 *  @param[in] logport - logical port number
 *  @param[out] dev_name - device 
 *  @param[out] attr_name - attribute
 *  @param[out] p_sign_mask - significant bits mask
 *  @param[out] p_is_active_hi - active high/low flag
 *  @return CCL_OK on success, error code on failure 
 */
extern ccl_err_t devboard_portled_to_devattr(int logport, char dev_name[], 
                                             char attr_name[], b_u32 *p_sign_mask,
                                             int *p_is_active_hi);

/** Get appropriate device/attr/significant_mask for port "enable" operating
 *  @param[in] logport - logical port number
 *  @param[out] dev_name - device 
 *  @param[out] attr_name - attribute
 *  @param[out] p_sign_mask - significant bits mask
 *  @param[out] p_is_active_hi - active high/low flag
 *  @return CCL_OK on success, error code on failure 
 */
extern ccl_err_t devboard_portena_to_devattr(int logport, char dev_name[], 
                                             char attr_name[], b_u32 *p_sign_mask, 
                                             int *p_is_active_hi);

/** Get appropriate device/attr/significant_mask for port
 *  "reset" operating
 *  @param[in] logport - logical port number
 *  @param[out] dev_name - device 
 *  @param[out] attr_name - attribute
 *  @param[out] p_sign_mask - significant bits mask
 *  @param[out] p_is_active_hi - active high/low flag
 *  @return CCL_OK on success, error code on failure 
 */
extern ccl_err_t devboard_portrst_to_devattr(int logport, char dev_name[], 
                                             char attr_name[], b_u32 *p_sign_mask, 
                                             int *p_is_active_hi);

/** Get appropriate device/attr/significant_mask for port
 *  "tx_fault" operating
 *  @param[in] logport - logical port number
 *  @param[out] dev_name - device 
 *  @param[out] attr_name - attribute
 *  @param[out] p_sign_mask - significant bits mask
 *  @return CCL_OK on success, error code on failure 
 */
extern ccl_err_t devboard_porttxf_to_devattr(int logport, char dev_name[], 
                                             char attr_name[], b_u8 *p_sign_mask);

/** Get appropriate device/attr/significant_mask for port
 *  "rx_los" operating
 *  @param[in] logport - logical port number
 *  @param[out] dev_name - device 
 *  @param[out] attr_name - attribute
 *  @param[out] p_sign_mask - significant bits mask
 *  @return CCL_OK on success, error code on failure 
 */
extern ccl_err_t devboard_portrxlos_to_devattr(int logport, char dev_name[], 
                                               char attr_name[], b_u8 *p_sign_mask);

/** Get appropriate device/attr/significant_mask for port "rs" operating
 *  @param[in] logport - logical port number
 *  @param[in] rs_id - rate select index
 *  @param[out] dev_name - device 
 *  @param[out] attr_name - attribute
 *  @param[out] p_sign_mask - significant bits mask
 *  @return CCL_OK on success, error code on failure 
 */
extern ccl_err_t devboard_portrs_to_devattr(int logport, b_u8 rs_id, 
                                            char dev_name[], char attr_name[], 
                                            b_u8 *p_sign_mask);

/** Get appropriate device/attr/significant_mask for port
 *  "modsell" operating
 *  @param[in] logport - logical port number
 *  @param[out] dev_name - device 
 *  @param[out] attr_name - attribute
 *  @param[out] p_sign_mask - significant bits mask
 *  @return CCL_OK on success, error code on failure 
 */
extern ccl_err_t devboard_portmodsell_to_devattr(int logport, char dev_name[], 
                                                 char attr_name[], b_u8 *p_sign_mask);

/** Get appropriate device/attr/significant_mask for port
 *  "lpmode" operating
 *  @param[in] logport - logical port number
 *  @param[out] dev_name - device 
 *  @param[out] attr_name - attribute
 *  @param[out] p_sign_mask - significant bits mask
 *  @return CCL_OK on success, error code on failure 
 */
extern ccl_err_t devboard_portlpmode_to_devattr(int logport, char dev_name[], 
                                                char attr_name[], b_u8 *p_sign_mask);
extern ccl_err_t devboard_portinit_to_devattr(int port, char dev_name[], 
                                              char attr_name[], int *discr);
extern ccl_err_t devboard_portcntr_to_devattr(int port, char dev_name[], 
                                              char attr_name[], int *discr);
extern ccl_err_t devboard_portprop_to_devattr(int port, char dev_name[], 
                                              char attr_name[], int *discr);
extern ccl_err_t devboard_portcntrs_to_devattr(int port, char dev_name[], 
                                               char attr_name[], int *discr);
extern ccl_err_t devboard_porttrgen_to_devattr(int port, char dev_name[], 
                                               char attr_name[]);
extern ccl_err_t devboard_portcntrsrst_to_devattr(int port, char dev_name[], 
                                                  char attr_name[], int *discr);

/** Run Self Tests for all board types/devices
 *  @return CCL_OK on success, CCL_FAIL on failure
 */
extern ccl_err_t devboard_bist(void);

#endif /* _BATM_DEVBOARD_H_ */
