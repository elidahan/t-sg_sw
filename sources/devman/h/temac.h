#ifndef _DEV_TEMAC_H_
#define _DEV_TEMAC_H_

/**< WORD mask */
#define TEMAC_WORD_MASK 0xffffffff
/**< Half word mask */
#define TEMAC_HALF_WORD_MASK 0xFFFF
/**< Macro to indicate 16 bits */
#define TEMAC_SIXTEEN_BITS 16
/**< 16 LSB bits mask */
#define TEMAC_TWO_LSB_MASK 0xFFFF
/**< 16 MSB bits mask */
#define TEMAC_TWO_MSB_MASK 0xFFFF0000
/**< Number of bytes in a WORD */
#define TEMAC_NUM_WORD_BYTES  4
/**< Number of bytes in a DWORD */
#define TEMAC_NUM_DWORD_BYTES 8
/**< Number of bits in a WORD */
#define TEMAC_NUM_WORD_BITS  32

/**
 * @name Enumerator to specify Tri-Mode Ethernet MAC device 
 *       attributes
 */
enum temac_attr_e {
    eTEMAC_RXBL,
    eTEMAC_TXBL,
    eTEMAC_RXUNDRL,
    eTEMAC_RXFRAGL,
    eTEMAC_RX64BL,
    eTEMAC_RX65B127L,
    eTEMAC_RX128B255L,
    eTEMAC_RX256B511L,
    eTEMAC_RX512B1023L,
    eTEMAC_RX1024BL,
    eTEMAC_RXOVRL,
    eTEMAC_TX64BL,
    eTEMAC_TX65B127L,
    eTEMAC_TX128B255L,
    eTEMAC_TX256B511L,
    eTEMAC_TX512B1023L,
    eTEMAC_TX1024L,
    eTEMAC_TXOVRL,
    eTEMAC_RXFL,
    eTEMAC_RXFCSERL,
    eTEMAC_RXBCSTFL,
    eTEMAC_RXMCSTFL,
    eTEMAC_RXCTRFL,
    eTEMAC_RXLTERL,
    eTEMAC_RXVLANFL,
    eTEMAC_RXPFL,
    eTEMAC_RXUOPFL,
    eTEMAC_TXFL,
    eTEMAC_TXBCSTFL,
    eTEMAC_TXMCSTFL,
    eTEMAC_TXUNDRERL,
    eTEMAC_TXCTRFL,
    eTEMAC_TXVLANFL,
    eTEMAC_TXPFL,
    eTEMAC_TXSCL,
    eTEMAC_TXMCL,
    eTEMAC_TXDEF,
    eTEMAC_TXLTCL,
    eTEMAC_TXAECL,
    eTEMAC_TXEDEFL,
    eTEMAC_RXAERL,
    eTEMAC_RCW0,
    eTEMAC_RCW1,
    eTEMAC_TC,
    eTEMAC_FCC,
    eTEMAC_MSC,
    eTEMAC_RXFC,
    eTEMAC_TXFC,
    eTEMAC_TX_TIMESTAMP_ADJ,
    eTEMAC_PHYC,
    eTEMAC_IDREG,
    eTEMAC_ARREG,
    eTEMAC_MDIO_MC,
    eTEMAC_MDIO_MCR,
    eTEMAC_MDIO_MWD,
    eTEMAC_MDIO_MRD,
    eTEMAC_IS,
    eTEMAC_IP,
    eTEMAC_IE,
    eTEMAC_IC,
    eTEMAC_STAT_SINGLE_COUNTER,
    eTEMAC_IF_PROPERTY,
    eTEMAC_STAT_COUNTERS,
    eTEMAC_INIT,
    eTEMAC_CONFIGURE_TEST,
    eTEMAC_RUN_TEST,
};

/* APIs */
/**
* @brief Read Tri-Mode Ethernet MAC device attribute
*
* @param[in] p_priv points to the private context of the device
* @param[in] attrid id of the attribute
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] idx attribute's index in case the attribute id corresponds to array
* @param[out] p_value pointer to the buffer to hold returned attribute's value
* @param[in] size indicates size of the attribute
*
* @return	error code
*/
ccl_err_t temac_attr_read(void *p_priv, b_u32 attrid, 
                             b_u32 offset, b_u32 offset_sz, 
                             b_u32 idx, b_u8 *p_value, b_u32 size);

/**
* @brief Write Tri-Mode Ethernet MAC device attribute
*
* @param[in] p_priv points to the private context of the device
* @param[in] attrid id of the attribute
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] idx attribute's index in case the attribute id corresponds to array
* @param[in] p_value pointer to the buffer which holds written 
*            data value
* @param[in] size indicates size of the attribute
*
* @return	error code
*/
ccl_err_t temac_attr_write(void *p_priv, b_u32 attrid, 
                              b_u32 offset, b_u32 offset_sz, 
                              b_u32 idx, b_u8 *p_value, b_u32 size);

#endif/* _DEV_TEMAC_H_ */
