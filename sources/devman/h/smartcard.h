#ifndef _DEV_SMARTCARD_H_
#define _DEV_SMARTCARD_H_

/**
 * @name Enumerator to specify SMARTCARD device attributes 
 */
enum spidev_attr_e {
    eSMARTCARD_UART_ID,
    eSMARTCARD_INIT,
    eSMARTCARD_RUN_TEST,
    eSMARTCARD_CONFIGURE_TEST,
};

#endif /* _DEV_SMARTCARD_H_ */
