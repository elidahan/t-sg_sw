#ifndef _DEV_SYSMON_H_
#define _DEV_SYSMON_H_

/**
 * @name Enumerator to specify UltraScale System Monitor device 
 *       attributes
 */
enum sysmon_attr_e {
    eSYSMON_GIER,
    eSYSMON_IPISR,
    eSYSMON_IPIER,
    eSYSMON_TEMP,
    eSYSMON_VCCINT,
    eSYSMON_VCCAUX,
    eSYSMON_VPVN,
    eSYSMON_VREFP,
    eSYSMON_VREFN,
    eSYSMON_VBRAM,
    eSYSMON_SUPPLY_CALIB,
    eSYSMON_ADC_CALIB,
    eSYSMON_GAINERR_CALIB,
    eSYSMON_MAX_TEMP,
    eSYSMON_MAX_VCCINT,
    eSYSMON_MAX_VCCAUX,
    eSYSMON_MAX_VCCBRAM,
    eSYSMON_MIN_TEMP,
    eSYSMON_MIN_VCCINT,
    eSYSMON_MIN_VCCAUX,
    eSYSMON_MIN_VCCBRAM,
    eSYSMON_CFR0,
    eSYSMON_CFR1,
    eSYSMON_CFR2,
    eSYSMON_ATR_TEMP_UPPER,
    eSYSMON_ATR_VCCINT_UPPER,
    eSYSMON_ATR_VCCAUX_UPPER,
    eSYSMON_ATR_OT_UPPER,
    eSYSMON_ATR_TEMP_LOWER,
    eSYSMON_ATR_VCCINT_LOWER,
    eSYSMON_ATR_VCCAUX_LOWER,
    eSYSMON_ATR_OT_LOWER,
    eSYSMON_ATR_VBRAM_UPPER,
    eSYSMON_ATR_VBRAM_LOWER,
    eSYSMON_INIT,
    eSYSMON_CONFIGURE_TEST,
    eSYSMON_RUN_TEST,
};

/* APIs */
/**
* @brief Read SYSMON device attribute
*
* @param[in] p_priv points to the private context of the device
* @param[in] attrid id of the attribute
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] idx attribute's index in case the attribute id corresponds to array
* @param[out] p_value pointer to the buffer to hold returned attribute's value
* @param[in] size indicates size of the attribute
*
* @return	error code
*/
ccl_err_t sysmon_attr_read(void *p_priv, b_u32 attrid, 
                           b_u32 offset, b_u32 offset_sz, 
                           b_u32 idx, b_u8 *p_value, b_u32 size);

/**
* @brief Write SYSMON device attribute
*
* @param[in] p_priv points to the private context of the device
* @param[in] attrid id of the attribute
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] idx attribute's index in case the attribute id corresponds to array
* @param[in] p_value pointer to the buffer which holds written 
*            data value
* @param[in] size indicates size of the attribute
*
* @return	error code
*/
ccl_err_t sysmon_attr_write(void *p_priv, b_u32 attrid, 
                           b_u32 offset, b_u32 offset_sz, 
                           b_u32 idx, b_u8 *p_value, b_u32 size);

#endif/* _DEV_SYSMON_H_ */
