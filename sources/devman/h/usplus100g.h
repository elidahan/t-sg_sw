/********************************************************************************/
/**
 * @file usplus100g.h
 * @brief Contains types and prototypes for Xilinx UltraScale+ Devices 
 *        Integrated 100G Ethernet IP subsystem (USPLUS100G) driver
 * @copyright 2019 CELARE. All rights reserved.
 */
/********************************************************************************/
#ifndef _USPLUS100G_H
#define _USPLUS100G_H

/**< Half word mask */
#define USPLUS100G_HALF_WORD_MASK 0xFFFF
/**< Macro to indicate 16 bits */
#define USPLUS100G_SIXTEEN_BITS 16
/**< 16 LSB bits mask */
#define USPLUS100G_TWO_LSB_MASK 0xFFFF
/**< 16 MSB bits mask */
#define USPLUS100G_TWO_MSB_MASK 0xFFFF0000
/**< Number of bytes in a WORD */
#define USPLUS100G_NUM_WORD_BYTES  4
/**< Number of bytes in a DWORD */
#define USPLUS100G_NUM_DWORD_BYTES 8
/**< Number of bits in a WORD */
#define USPLUS100G_NUM_WORD_BITS  32
/**< Total refresh timers in the CMAC IP */
#define USPLUS100G_TOTAL_TX_PAUSE_REFRESH_TIMERS 8
/** Total Pause Refresh Timers */
#define USPLUS100G_TOTAL_PAUSE_REFRESH_TIMERS 8
/** Total Pause Quantas */
#define USPLUS100G_TOTAL_PAUSE_QUANTAS 8
/**< WORD mask */
#define USPLUS100G_WORD_MASK 0xffffffff
/**< USPLUS100G_PCS_LANE_COUNT specifies the count of PCS lanes. */
#define USPLUS100G_PCS_LANE_COUNT    (20)

/**
 * This enum represents CMAC core
 */
typedef enum {
    USPLUS100G_CORE_RX,      /*!< Indicates Rx path */
    USPLUS100G_CORE_TX,      /*!< Indicates Tx path */
    USPLUS100G_CORE_BOTH,    /*!< Indicates Rx and Tx path */
} usplus100g_core_type_t;       

/**
 * This enum represents CMAC CAUI mode
 */
typedef enum {
    USPLUS100G_CAUI_MODE_10 = 0, /*!< Indicates CAUI-10 mode */
    USPLUS100G_CAUI_MODE_4  = 1, /*!< Indicates CAUI-4 mode */
} usplus100g_caui_mode_t;

/**
 * This enum represents flow control packet type
 */    
typedef enum {
    USPLUS100G_FC_GLOBAL_CONTROL_PACKET,     /*!< Indicates global control packet */
    USPLUS100G_FC_PRIORITY_CONTROL_PACKET,   /*!< Indicates priority control packet */
    USPLUS100G_FC_GLOBAL_PAUSE_PACKET,       /*!< Indicates global pause packet */
    USPLUS100G_FC_PRIORITY_PAUSE_PACKET,     /*!< Indicates priority pause packet */
} usplus100g_flow_control_packet_type_t;

/**
 * This typedef represents BIP8 signature error count per PCS lane
 */
typedef b_u64 usplus100g_bip8_error_t[USPLUS100G_PCS_LANE_COUNT];

/**
 * This typedef represents Sync header framing error count per PCS lane
 */
typedef b_u64 usplus100g_framing_error_t[USPLUS100G_PCS_LANE_COUNT];


/**
 * @brief Structure for CMAC driver instance specific variables.
 *
 */
typedef struct {
    /**< PCIe BAR in which CMAC IP is present */
    b_u64 base_address;
    /**< Indicates device is initialized and ready */
    b_u32 is_ready;
    /**< Indicates device has been started */
    b_u32 is_started;
    /**< Global variable to hold the PCI Bar Data details */
} usplus100g_t;

/**
 * @brief Structure for fault indication specific variables.
 *
 */
typedef struct {
    /**< Send Remote Fault Indication on Tx */
    b_u8 tx_send_rfi;
} usplus100g_fault_indication_t;

/**
 * @brief Structure for Bip7 specific variables.
 */
typedef struct {
    /**< bip7 feature enable/disable */
    b_u8 bip7_valid;
    /**< 8-bit bip7 value */
    b_u8 bip7_value;
} usplus100g_bip7_config_t;

/**
 * @brief Structure for Tx Flow control specific variables.
 */
typedef struct {
    /**< Pause packets enable/disable */
    b_u16 pause_enable;
    /**< Refresh interval to send pause packet */
    b_u16 refresh_interval[USPLUS100G_TOTAL_TX_PAUSE_REFRESH_TIMERS];
    /**< Pause quanta */
    b_u16 pause_quanta[USPLUS100G_TOTAL_PAUSE_QUANTAS];
} usplus100g_tx_flow_control_t;

/**
 * @brief Structure for Rx Flow control specific variables.
 */
typedef struct {
    /**< Packet processing enable/disable */
    b_u8 enable;
    /**< Multicast destination address processing */
    b_u8 check_multicast;
    /**< Unicast destination address processing */
    b_u8 check_unicast;
    /**< Enable/Disable source address processing */
    b_u8 check_source_address;
    /**< Enable/Disable Ethertype processing */
    b_u8 check_ether_type;
    /**< Enable/Disable opcode processing */
    b_u8 check_opcode;
} usplus100g_rx_flow_control_t;

/**
 * @brief Structure for Tx status specific variables.
 * PTP - Precise Timing Protocol
 */
typedef struct {
    /**< Tx Local Fault status */
    b_u8 local_fault;
    /**< Tx PTP FIFO read error */
    b_u8 ptp_fifo_read_error;
    /**< Tx PTP FIFO write error */
    b_u8 ptp_fifo_write_error;
} usplus100g_tx_status_t;

/**
 * @brief Structure for Rx lane status.
 */
typedef struct {
    /**< PCS status */
    b_u8 pcs_status;
    /**< All PCS aligned/de-skewed status */
    b_u8 aligned;
    /**< Alignment Error */
    b_u8 mis_aligned;
    /**< Loss of lane alignment/De-Skew status */
    b_u8 aligned_error;
    /**< PCS block lock status */
    b_u8 block_lock[USPLUS100G_PCS_LANE_COUNT];
    /**< Word boundary sync status */
    b_u8 synced[USPLUS100G_PCS_LANE_COUNT];
    /**< Sync error status */
    b_u8 synced_error[USPLUS100G_PCS_LANE_COUNT];
    /**< Lane marker error status */
    b_u8 lane_marker_error[USPLUS100G_PCS_LANE_COUNT];
    /**< Lane marker length error status */
    b_u8 lane_marker_len_error[USPLUS100G_PCS_LANE_COUNT];
    /**< Consecutive lane marker error status */
    b_u8 lane_marker_repeat_error[USPLUS100G_PCS_LANE_COUNT];
} usplus100g_rx_lane_am_status_t;

/**
 * @brief Structure for  Rx Fault status.
 */
typedef struct {
    /**< remote fault indication */
    b_u8 remote_fault;
    /**< local fault indication */
    b_u8 local_fault;
    /**< internal local fault indication */
    b_u8 internal_local_fault;
    /**< received local fault indication */
    b_u8 received_local_fault;
} usplus100g_rx_fault_status_t;

/**
 * @brief Structure for Rx Ethernet data status.
 */
typedef struct {
    /**< High Bit error rate indication */
    b_u8 hi_ber;
    /**< Bad Preamble indication */
    b_u8 bad_preamble;
    /**< Invalid Start of frame delimiter indication */
    b_u8 bad_sfd;
    /**< Signal Ordered Sets indication */
    b_u8 got_signal_os;
} usplus100g_rx_packet_status_t;

/**
 * @brief Structure for Rx Virtual lane Demux status.
 */
typedef struct {
    /**< pcs lane demultiplex status */
    b_u8 vldemuxed[USPLUS100G_PCS_LANE_COUNT];
    /**< pcs lane mapping, 5 lines per 20 lanes */
    b_u8 vlnumber[USPLUS100G_PCS_LANE_COUNT];
} usplus100g_rx_vldemux_status_t;

/**
 * @brief Structure for packets histogram.
 */
typedef struct {
    /**< Total number of packets */
    b_u64 total_packets;
    /**< Total number of good packets */
    b_u64 total_good_packets;
    /**< Total number of bytes */
    b_u64 total_bytes;
    /**< Total number of good bytes */
    b_u64 total_good_bytes;
    /**< Total packets of size 64 bytes */
    b_u64 packets_0_to_64_bytes;
    /**< packets size between 64 & 127 bytes */
    b_u64 packets_65_to_127_bytes;
    /**< packets size between 128 & 255 bytes */
    b_u64 packets_128_to_255_bytes;
    /**< packets size between 256 & 511 bytes */
    b_u64 packets_256_to_511_bytes;
    /**< packets size between 512 & 1023 bytes */
    b_u64 packets_512_to_1023_bytes;
    /**< packets size between 1024 & 1518 bytes */
    b_u64 packets_1024_to_1518_bytes;
    /**< packets size between 1519 & 1522 bytes */
    b_u64 packets_1519_to_1522_bytes;
    /**< packets size between 1523 & 1548 bytes */
    b_u64 packets_1523_to_1548_bytes;
    /**< packets size between 1549 & 2047 bytes */
    b_u64 packets_1549_to_2047_bytes;
    /**< packets size between 2048 & 4095 bytes */
    b_u64 packets_2048_to_4095_bytes;
    /**< packets size between 4096 & 8191 bytes */
    b_u64 packets_4096_to_8191_bytes;
    /**< packets size between 8192 & 9215 bytes */
    b_u64 packets_8192_to_9215_bytes;
    /**< packets size less than 64 bytes */
    b_u64 packets_small;
    /**< packets size more than 9215 bytes */
    b_u64 packets_large;
} usplus100g_packet_count_statistics_t;

/**
 * @brief Structure for packet type statistics.
 */
typedef struct {
    /**< Unicast packets count */
    b_u64 packets_unicast;
    /**< Multicast packets count */
    b_u64 packets_multicast;
    /**< Broadcast packets count */
    b_u64 packets_broadcast;
    /**< VLAN packets count */
    b_u64 packets_vlan;
    /**< Pause packets with good FCS */
    b_u64 packets_pause;
    /**< Priority pause packets with good FCS */
    b_u64 packets_priority_pause;
} usplus100g_packet_type_statistics_t;

/**
 * @brief Structure for Malformed packets statistics.
 */
typedef struct {
    /**< packets less than min packet length with good FCS */
    b_u64 under_size_count;
    /**< packets less than min packet length with bad FCS */
    b_u64 fragment_count;
    /**< packets longer than max packet length with good FCS */
    b_u64 oversize_count;
    /**< packets longer than max packet length with good and bad FCS */
    b_u64 toolong_count;
    /**< packets longer than max packet length with bad FCS */
    b_u64 jabber_count;
    /**< crc32 error */
    b_u64 bad_fcs_count;
    /**< packets between 64 bytes and max packet length with FCS errors */
    b_u64 packet_bad_fcs_count;
    /**< packets with bitwise inverse of expected good FCS */
    b_u64 stomped_fcs_count;
} usplus100g_malformed_statistics_t;

/** Function macro to left shift 1 by x times, used by set_bit() and clear_bit() Macros */
#define bit(x)                (0x1 << x)
/** Function macro that sets a bit in the given value
 *  If user wants to set a bit num 0 in bits [31:0], then pass Pos as 0 */
#define set_bit(val, pos)    ( val |= bit(pos) )
/** Function macro that clears a bit in the given value
 *  If user wants to clear a bit num 0 in bits [31:0], then pass pos as 0 */
#define clear_bit(val, pos)     ( val &= ~bit(pos) )

/**
 * @name Enumerator to specify Xilinx UltraScale+ Devices 
 *       Integrated 100G Ethernet IP subsystem (USPLUS100G)
 *       device attributes
 */
enum usplus100g_attr_e {
    eUSPLUS100G_GT_RESET_REG,
    eUSPLUS100G_RESET_REG,
    eUSPLUS100G_SWITCH_CORE_MODE_REG,
    eUSPLUS100G_CONFIGURATION_TX_REG1,
    eUSPLUS100G_CONFIGURATION_RX_REG1,
    eUSPLUS100G_CORE_MODE_REG,
    eUSPLUS100G_CORE_VERSION_REG,
    eUSPLUS100G_CONFIGURATION_TX_BIP_OVERRIDE,
    eUSPLUS100G_CONFIGURATION_TX_FLOW_CONTROL_CONTROL_REG1,
    eUSPLUS100G_CONFIGURATION_TX_FLOW_CONTROL_REFRESH_REG1,
    eUSPLUS100G_CONFIGURATION_TX_FLOW_CONTROL_QUANTA_REG1,
    eUSPLUS100G_CONFIGURATION_TX_OTN_PKT_LEN_REG,
    eUSPLUS100G_CONFIGURATION_TX_OTN_CTL_REG,
    eUSPLUS100G_CONFIGURATION_RX_FLOW_CONTROL_CONTROL_REG1,
    eUSPLUS100G_CONFIGURATION_RX_FLOW_CONTROL_CONTROL_REG2,
    eUSPLUS100G_GT_LOOPBACK_REG,
    eUSPLUS100G_CONFIGURATION_AN_CONTROL_REG1,
    eUSPLUS100G_CONFIGURATION_AN_CONTROL_REG2,
    eUSPLUS100G_CONFIGURATION_AN_ABILITY,
    eUSPLUS100G_CONFIGURATION_LT_CONTROL_REG1,
    eUSPLUS100G_CONFIGURATION_LT_TRAINED_REG,
    eUSPLUS100G_CONFIGURATION_LT_PRESET_REG,
    eUSPLUS100G_CONFIGURATION_LT_INIT_REG,
    eUSPLUS100G_CONFIGURATION_LT_SEED_REG0,
    eUSPLUS100G_CONFIGURATION_LT_COEFFICIENT_REG0,
    eUSPLUS100G_RSFEC_CONFIG_INDICATION_CORRECTION,
    eUSPLUS100G_RSFEC_CONFIG_ENABLE,
    eUSPLUS100G_STAT_TX_STATUS_REG,
    eUSPLUS100G_STAT_RX_STATUS_REG,
    eUSPLUS100G_STAT_STATUS_REG1,
    eUSPLUS100G_STAT_RX_BLOCK_LOCK_REG,
    eUSPLUS100G_STAT_RX_LANE_SYNC_REG,
    eUSPLUS100G_STAT_RX_LANE_SYNC_ERR_REG,
    eUSPLUS100G_STAT_RX_AM_ERR_REG,
    eUSPLUS100G_STAT_RX_AM_LEN_ERR_REG,
    eUSPLUS100G_STAT_RX_AM_REPEAT_ERR_REG,
    eUSPLUS100G_STAT_RX_PCSL_DEMUXED_REG,
    eUSPLUS100G_STAT_RX_PCS_LANE_NUM_REG1,
    eUSPLUS100G_STAT_RX_BIP_OVERRIDE_REG,
    eUSPLUS100G_STAT_TX_OTN_STATUS_REG,
    eUSPLUS100G_STAT_AN_STATUS_REG,
    eUSPLUS100G_STAT_AN_ABILITY_REG,
    eUSPLUS100G_STAT_AN_LINK_CTL_REG,
    eUSPLUS100G_STAT_LT_STATUS_REG1,
    eUSPLUS100G_STAT_LT_STATUS_REG2,
    eUSPLUS100G_STAT_LT_STATUS_REG3,
    eUSPLUS100G_STAT_LT_STATUS_REG4,
    eUSPLUS100G_STAT_LT_COEFFICIENT0_REG,
    eUSPLUS100G_TICK_REG,
    eUSPLUS100G_STAT_CYCLE_COUNT,
    eUSPLUS100G_STAT_RX_BIP_ERR_0,
    eUSPLUS100G_STAT_RX_FRAMING_ERR0,
    eUSPLUS100G_STAT_RX_BAD_CODE,
    eUSPLUS100G_STAT_TX_FRAME_ERROR,
    eUSPLUS100G_STAT_TX_TOTAL_PACKETS,
    eUSPLUS100G_STAT_TX_TOTAL_GOOD_PACKETS,
    eUSPLUS100G_STAT_TX_TOTAL_BYTES,
    eUSPLUS100G_STAT_TX_TOTAL_GOOD_BYTES,
    eUSPLUS100G_STAT_TX_PACKET_64_BYTES,
    eUSPLUS100G_STAT_TX_PACKET_65_127_BYTES,
    eUSPLUS100G_STAT_TX_PACKET_128_255_BYTES,
    eUSPLUS100G_STAT_TX_PACKET_256_511_BYTES,
    eUSPLUS100G_STAT_TX_PACKET_512_1023_BYTES,
    eUSPLUS100G_STAT_TX_PACKET_1024_1518_BYTES,
    eUSPLUS100G_STAT_TX_PACKET_1519_1522_BYTES,
    eUSPLUS100G_STAT_TX_PACKET_1523_1548_BYTES,
    eUSPLUS100G_STAT_TX_PACKET_1549_2047_BYTES,
    eUSPLUS100G_STAT_TX_PACKET_2048_4095_BYTES,
    eUSPLUS100G_STAT_TX_PACKET_4096_8191_BYTES,
    eUSPLUS100G_STAT_TX_PACKET_8192_9215_BYTES,
    eUSPLUS100G_STAT_TX_PACKET_LARGE,
    eUSPLUS100G_STAT_TX_PACKET_SMALL,
    eUSPLUS100G_STAT_TX_BAD_FCS,
    eUSPLUS100G_STAT_TX_UNICAST,
    eUSPLUS100G_STAT_TX_MULTICAST,
    eUSPLUS100G_STAT_TX_BROADCAST,
    eUSPLUS100G_STAT_TX_VLAN,
    eUSPLUS100G_STAT_TX_PAUSE,
    eUSPLUS100G_STAT_TX_USER_PAUSE,
    eUSPLUS100G_STAT_RX_TOTAL_PACKETS,
    eUSPLUS100G_STAT_RX_TOTAL_GOOD_PACKETS,
    eUSPLUS100G_STAT_RX_TOTAL_BYTES,
    eUSPLUS100G_STAT_RX_TOTAL_GOOD_BYTES,
    eUSPLUS100G_STAT_RX_PACKET_64_BYTES,
    eUSPLUS100G_STAT_RX_PACKET_65_127_BYTES,
    eUSPLUS100G_STAT_RX_PACKET_128_255_BYTES,
    eUSPLUS100G_STAT_RX_PACKET_256_511_BYTES,
    eUSPLUS100G_STAT_RX_PACKET_512_1023_BYTES,
    eUSPLUS100G_STAT_RX_PACKET_1024_1518_BYTES,
    eUSPLUS100G_STAT_RX_PACKET_1519_1522_BYTES,
    eUSPLUS100G_STAT_RX_PACKET_1523_1548_BYTES,
    eUSPLUS100G_STAT_RX_PACKET_1549_2047_BYTES,
    eUSPLUS100G_STAT_RX_PACKET_2048_4095_BYTES,
    eUSPLUS100G_STAT_RX_PACKET_4096_8191_BYTES,
    eUSPLUS100G_STAT_RX_PACKET_8192_9215_BYTES,
    eUSPLUS100G_STAT_RX_PACKET_LARGE,
    eUSPLUS100G_STAT_RX_PACKET_SMALL,
    eUSPLUS100G_STAT_RX_UNDERSIZE,
    eUSPLUS100G_STAT_RX_FRAGMENT,
    eUSPLUS100G_STAT_RX_OVERSIZE,
    eUSPLUS100G_STAT_RX_TOOLONG,
    eUSPLUS100G_STAT_RX_JABBER,
    eUSPLUS100G_STAT_RX_BAD_FCS,
    eUSPLUS100G_STAT_RX_PACKET_BAD_FCS,
    eUSPLUS100G_STAT_RX_STOMPED_FCS,
    eUSPLUS100G_STAT_RX_UNICAST,
    eUSPLUS100G_STAT_RX_MULTICAST,
    eUSPLUS100G_STAT_RX_BROADCAST,
    eUSPLUS100G_STAT_RX_VLAN,
    eUSPLUS100G_STAT_RX_PAUSE,
    eUSPLUS100G_STAT_RX_USER_PAUSE,
    eUSPLUS100G_STAT_RX_INRANGEERR,
    eUSPLUS100G_STAT_RX_TRUNCATED,
    eUSPLUS100G_STAT_OTN_TX_JABBER,
    eUSPLUS100G_STAT_OTN_TX_OVERSIZE,
    eUSPLUS100G_STAT_OTN_TX_UNDERSIZE,
    eUSPLUS100G_STAT_OTN_TX_TOOLONG,
    eUSPLUS100G_STAT_OTN_TX_FRAGMENT,
    eUSPLUS100G_STAT_OTN_TX_PACKET_BAD_FCS,
    eUSPLUS100G_STAT_OTN_TX_STOMPED_FCS,
    eUSPLUS100G_STAT_OTN_TX_BAD_CODE,
    eUSPLUS100G_STAT_SINGLE_COUNTER,
    eUSPLUS100G_IF_PROPERTY,
    eUSPLUS100G_STAT_COUNTERS,
    eUSPLUS100G_INIT,
    eUSPLUS100G_CONFIGURE_TEST,
    eUSPLUS100G_RUN_TEST,
    eUSPLUS100G_COUNTERS_RESET,
};
 
/* APIs */
/**
* @brief Read Xilinx UltraScale+ Devices 
*       Integrated 100G Ethernet IP subsystem
*       (USPLUS100G) device attribute
*
* @param[in] p_priv points to the private context of the device
* @param[in] attrid id of the attribute
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] idx attribute's index in case the attribute id corresponds to array
* @param[out] p_value pointer to the buffer to hold returned attribute's value
* @param[in] size indicates size of the attribute
*
* @return	error code
*/
ccl_err_t usplus100g_attr_read(void *p_priv, b_u32 attrid, 
                               b_u32 offset, b_u32 offset_sz, 
                               b_u32 idx, b_u8 *p_value, b_u32 size);

/**
* @brief Write Xilinx UltraScale+ Devices 
*       Integrated 100G Ethernet IP subsystem
*       (USPLUS100G) device attribute
*
* @param[in] p_priv points to the private context of the device
* @param[in] attrid id of the attribute
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] idx attribute's index in case the attribute id corresponds to array
* @param[in] p_value pointer to the buffer which holds written 
*            data value
* @param[in] size indicates size of the attribute
*
* @return	error code
*/
ccl_err_t usplus100g_attr_write(void *p_priv, b_u32 attrid, 
                                b_u32 offset, b_u32 offset_sz, 
                                b_u32 idx, b_u8 *p_value, b_u32 size);


#endif /* _USPLUS100G_H */


