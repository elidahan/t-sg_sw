#ifndef _DEV_SPIDEV_H_
#define _DEV_SPIDEV_H_

/**
 * @name Enumerator to specify SPIDEV device attributes 
 */
enum spidev_attr_e {
    eSPIDEV_CMD_MANUFACTURER_ID,
    eSPIDEV_CMD_JEDEC_ID,
    eSPIDEV_CMD_UNIQUE_ID,
    eSPIDEV_BUFFER,
    eSPIDEV_INIT,
    eSPIDEV_CONFIGURE_TEST,
    eSPIDEV_RUN_TEST,
};

/* APIs */
/**
* @brief Read SPIDEV device attribute
*
* @param[in] p_priv points to the private context of the device
* @param[in] attrid id of the attribute
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] idx attribute's index in case the attribute id corresponds to array
* @param[out] p_value pointer to the buffer to hold returned attribute's value
* @param[in] size indicates size of the attribute
*
* @return	error code
*/
ccl_err_t spidev_attr_read(void *p_priv, b_u32 attrid, 
                            b_u32 offset, b_u32 offset_sz, 
                            b_u32 idx, b_u8 *p_value, b_u32 size);
/**
* @brief Write SPIDEV device attribute
*
* @param[in] p_priv points to the private context of the device
* @param[in] attrid id of the attribute
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] idx attribute's index in case the attribute id corresponds to array
* @param[in] p_value pointer to the buffer which holds written 
*            data value
* @param[in] size indicates size of the
*       attribute
*
* @return	error code
*/
ccl_err_t spidev_attr_write(void *p_priv, b_u32 attrid, 
                             b_u32 offset, b_u32 offset_sz, 
                             b_u32 idx, b_u8 *p_value, b_u32 size);

#endif /* _DEV_SPIDEV_H_ */
