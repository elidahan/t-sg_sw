#ifndef _DEV_LK2047T1U_H_
#define _DEV_LK2047T1U_H_

/**
 * @name Enumerator to specify LK204-T7-1U LCD Display device 
 *       attributes
 */
enum lk2047t1u_attr_e {
    eLK204T71U_I2C_SLAVE_ADDR,
    eLK204T71U_TRANS_PROTO,
    eLK204T71U_CLEAR_SCREEN,
    eLK204T71U_CHANGE_STARTUP_SCREEN,
    eLK204T71U_AUTO_SCROLL_ON,
    eLK204T71U_AUTO_SCROLL_OFF,
    eLK204T71U_AUTO_LINE_WRAP_ON,
    eLK204T71U_AUTO_LINE_WRAP_OFF,
    eLK204T71U_SET_CURSOR_POSITION,
    eLK204T71U_GO_HOME,
    eLK204T71U_MOVE_CURSOR_BACK,
    eLK204T71U_MOVE_CURSOR_FWRD,
    eLK204T71U_UNDERLINE_CURSOR_ON,
    eLK204T71U_UNDERLINE_CURSOR_OFF,
    eLK204T71U_BLINKING_BLOCK_CURSOR_ON,
    eLK204T71U_BLINKING_BLOCK_CURSOR_OFF,
    eLK204T71U_CREATE_CUSTOM_CHARACTER,
    eLK204T71U_SAVE_CUSTOM_CHARACTERS,
    eLK204T71U_LOAD_CUSTOM_CHARACTERS,
    eLK204T71U_SAVE_STARTUP_SCREEN_CUST_CHARS,
    eLK204T71U_INIT_MEDIUM_NUMBERS,
    eLK204T71U_PLACE_MEDIUM_NUMBERS,
    eLK204T71U_INIT_LARGE_NUMBERS,
    eLK204T71U_PLACE_LARGE_NUMBERS,
    eLK204T71U_INIT_HORIZONTAL_BAR,
    eLK204T71U_PLACE_HORIZONTAL_BAR_GRAPH,
    eLK204T71U_INIT_NARROW_VERT_BAR,
    eLK204T71U_INIT_WIDE_VERT_BAR,
    eLK204T71U_PLACE_VERT_BAR,
    eLK204T71U_GEN_PURPOSE_OUTPUT_ON,
    eLK204T71U_GEN_PURPOSE_OUTPUT_OFF,
    eLK204T71U_SET_STARTUP_GPO_STATE,
    eLK204T71U_AUTO_TRANSMIT_KEY_PRESSES_ON,
    eLK204T71U_AUTO_TRANSMIT_KEY_PRESSES_OFF,
    eLK204T71U_POLL_KEY_PRESS,
    eLK204T71U_CLEAR_KEY_BUFFER,
    eLK204T71U_SET_DEBOUNCE_TIME,
    eLK204T71U_SET_AUTO_REPEAT_MODE,
    eLK204T71U_AUTO_REPEAT_MODE_OFF,
    eLK204T71U_ASSIGN_KEYPAD_CODES,
    eLK204T71U_KEYPAD_BACKLIGHT_OFF,
    eLK204T71U_SET_KEYPAD_BRIGHTNESS,
    eLK204T71U_SET_AUTO_BACKLIGHT,
    eLK204T71U_BACKLIGHT_ON,
    eLK204T71U_BACKLIGHT_OFF,
    eLK204T71U_SET_BRIGHTNESS,
    eLK204T71U_SET_AND_SAVE_BRIGHTNESS,
    eLK204T71U_SET_BACKLIGHT_COLOUR,
    eLK204T71U_SET_CONTRAST,
    eLK204T71U_SET_AND_SAVE_CONTRAST,
    eLK204T71U_SET_REMEMBER,
    eLK204T71U_SET_DATA_LOCK,
    eLK204T71U_SET_AND_SAVE_DATA_LOCK,
    eLK204T71U_WRITE_CUSTOMER_DATA,
    eLK204T71U_READ_CUSTOMER_DATA,
    eLK204T71U_READ_VERSION_NUMBER,
    eLK204T71U_READ_MODULE_TYPE,
    eLK204T71U_WRITE_TEXT,
    eLK204T71U_INIT,
    eLK204T71U_CONFIGURE_TEST,
    eLK204T71U_RUN_TEST,
};

/* APIs */
/**
* @brief Read LK204-T7-1U LCD Display device attribute
*
* @param[in] p_priv points to the private context of the device
* @param[in] attrid id of the attribute
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] idx attribute's index in case the attribute id corresponds to array
* @param[out] p_value pointer to the buffer to hold returned attribute's value
* @param[in] size indicates size of the attribute
*
* @return	error code
*/
ccl_err_t lk2047t1u_attr_read(void *p_priv, b_u32 attrid, 
                              b_u32 offset, b_u32 offset_sz, 
                              b_u32 idx, b_u8 *p_value, b_u32 size);

/**
* @brief Write LK204-T7-1U LCD Display device attribute
*
* @param[in] p_priv points to the private context of the device
* @param[in] attrid id of the attribute
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] idx attribute's index in case the attribute id corresponds to array
* @param[in] p_value pointer to the buffer which holds written 
*            data value
* @param[in] size indicates size of the attribute
*
* @return	error code
*/
ccl_err_t lk2047t1u_attr_write(void *p_priv, b_u32 attrid, 
                               b_u32 offset, b_u32 offset_sz, 
                               b_u32 idx, b_u8 *p_value, b_u32 size);

#endif/* _DEV_LK2047T1U_H_ */
