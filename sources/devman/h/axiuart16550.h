#ifndef _DEV_AXIUART16550_H_
#define _DEV_AXIUART16550_H_

/**
 * @name Enumerator to specify AXI UART 16550 device attributes
 */
enum axiuart_attr_e {
    eAXIUART16550_RBR,
    eAXIUART16550_THR,
    eAXIUART16550_IER,
    eAXIUART16550_IIR,
    eAXIUART16550_FCR,
    eAXIUART16550_LCR,
    eAXIUART16550_MCR,
    eAXIUART16550_LSR,
    eAXIUART16550_MSR,
    eAXIUART16550_SCR,
    eAXIUART16550_DLL,
    eAXIUART16550_DLM,
    eAXIUART16550_READ,
    eAXIUART16550_WRITE,
    eAXIUART16550_INIT,
    eAXIUART16550_CONFIGURE_TEST,
    eAXIUART16550_RUN_TEST,
};

/* APIs */
/**
* @brief Read AXI UART 16550 device attribute
*
* @param[in] p_priv points to the private context of the device
* @param[in] attrid id of the attribute
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] idx attribute's index in case the attribute id corresponds to array
* @param[out] p_value pointer to the buffer to hold returned attribute's value
* @param[in] size indicates size of the attribute
*
* @return	error code
*/
ccl_err_t axiuart_attr_read(void *p_priv, b_u32 attrid, 
                            b_u32 offset, b_u32 offset_sz, 
                            b_u32 idx, b_u8 *p_value, b_u32 size);


/**
* @brief Write AXI UART 16550 device attribute
*
* @param[in] p_priv points to the private context of the device
* @param[in] attrid id of the attribute
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] idx attribute's index in case the attribute id corresponds to array
* @param[out] p_value pointer to the buffer to hold returned attribute's value
* @param[in] size indicates size of the attribute
*
* @return	error code
*/
ccl_err_t axiuart_attr_write(void *p_priv, b_u32 attrid, 
                             b_u32 offset, b_u32 offset_sz, 
                             b_u32 idx, b_u8 *p_value, b_u32 size);

/**
* @brief Read data through AXI UART device
*
* @param[in] dev_id specific UART device id
* @param[out] p_value pointer to the buffer to hold returned 
*       data value
* @param[in] size indicates size of the data
*
* @return	error code
*/
ccl_err_t devaxiuart_read_buff(b_u32 dev_id, b_u8 *p_value, b_u32 size);

/**
* @brief Write data through AXI UART device
*
* @param[in] dev_id specific UART device id
* @param[in] p_value pointer to the buffer which holds written 
*            data value
* @param[in] size indicates size of the data
*
* @return	error code
*/
ccl_err_t devaxiuart_write_buff(b_u32 dev_id, b_u8 *p_value, b_u32 size);

#endif/* _DEV_AXIUART16550_H_ */
