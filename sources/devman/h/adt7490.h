#ifndef _DEV_ADT7490_H_
#define _DEV_ADT7490_H_

/**
 * @name Enumerator to specify ADT7490 Fan Controller device 
 *       attributes
 */
enum adt7490_attr_e {
    eADT7490_CONFIG_1,                    
    eADT7490_CONFIG_2,                    
    eADT7490_CONFIG_3,                    
    eADT7490_CONFIG_4,                    
    eADT7490_CONFIG_5,                    
    eADT7490_CONFIG_6,                    
    eADT7490_CONFIG_7,                    
    eADT7490_PECI0,                       
    eADT7490_PECI1,                       
    eADT7490_PECI2,                       
    eADT7490_PECI3,                       
    eADT7490_IMON,                        
    eADT7490_VTT,                         
    eADT7490_EXT_RES_3,                   
    eADT7490_VOLT_READ_PIN22,             
    eADT7490_VOLT_READ_PIN23,             
    eADT7490_VOLT_READ_PIN4,              
    eADT7490_VOLT_READ_PIN20,             
    eADT7490_VOLT_READ_PIN21,             
    eADT7490_TEMP_READ_REM_1,             
    eADT7490_TEMP_READ_LOCAL,             
    eADT7490_TEMP_READ_REM_2,             
    eADT7490_TACH1_READ_LOW,              
    eADT7490_TACH1_READ_HIGH,             
    eADT7490_TACH2_READ_LOW,              
    eADT7490_TACH2_READ_HIGH,             
    eADT7490_TACH3_READ_LOW,              
    eADT7490_TACH3_READ_HIGH,             
    eADT7490_TACH4_READ_LOW,              
    eADT7490_TACH4_READ_HIGH,             
    eADT7490_DUTY_CYCLE_PWM1,             
    eADT7490_DUTY_CYCLE_PWM2,             
    eADT7490_DUTY_CYCLE_PWM3,             
    eADT7490_PECI_LIMIT_LOW,              
    eADT7490_PECI_LIMIT_HIGH,             
    eADT7490_PECI_CONFIG_1,               
    eADT7490_PECI_CONFIG_2,               
    eADT7490_DUTY_CYCLE_MAX_PWM1,         
    eADT7490_DUTY_CYCLE_MAX_PWM2,         
    eADT7490_DUTY_CYCLE_MAX_PWM3,         
    eADT7490_PECI_T_MIN,                  
    eADT7490_PECI_T_RANGE_ENHCD,          
    eADT7490_PECI_T_CTRL_LIMIT,           
    eADT7490_VERSION,                     
    eADT7490_INT_STATUS_1,                
    eADT7490_INT_STATUS_2,                
    eADT7490_INT_STATUS_3,                
    eADT7490_INT_STATUS_4,                
    eADT7490_VIN_2_5_LIMIT_LOW,           
    eADT7490_VIN_2_5_LIMIT_HIGH,          
    eADT7490_VCCP_LIMIT_LOW,              
    eADT7490_VCCP_LIMIT_HIGH,             
    eADT7490_VCC_LIMIT_LOW,               
    eADT7490_VCC_LIMIT_HIGH,              
    eADT7490_VIN_5_LIMIT_LOW,             
    eADT7490_VIN_5_LIMIT_HIGH,            
    eADT7490_VIN_12_LIMIT_LOW,            
    eADT7490_VIN_12_LIMIT_HIGH,           
    eADT7490_TEMP_REM_1_LIMIT_LOW,        
    eADT7490_TEMP_REM_1_LIMIT_HIGH,       
    eADT7490_TEMP_LOCAL_LIMIT_LOW,        
    eADT7490_TEMP_LOCAL_LIMIT_HIGH,       
    eADT7490_TEMP_REM_2_LIMIT_LOW,        
    eADT7490_TEMP_REM_2_LIMIT_HIGH,       
    eADT7490_TACH1_LIMIT_LOW_BYTE,        
    eADT7490_TACH1_LIMIT_HIGH_BYTE,       
    eADT7490_TACH2_LIMIT_LOW_BYTE,        
    eADT7490_TACH2_LIMIT_HIGH_BYTE,       
    eADT7490_TACH3_LIMIT_LOW_BYTE,        
    eADT7490_TACH3_LIMIT_HIGH_BYTE,       
    eADT7490_TACH4_LIMIT_LOW_BYTE,        
    eADT7490_TACH4_LIMIT_HIGH_BYTE,       
    eADT7490_CONFIG_PWM1,                 
    eADT7490_CONFIG_PWM2,                 
    eADT7490_CONFIG_PWM3,                 
    eADT7490_TEMP_REM1_T_RANGE, 
    eADT7490_TEMP_LOCAL_T_RANGE,
    eADT7490_TEMP_REM2_T_RANGE, 
    eADT7490_ENHANCED_ACOUSTICS1,         
    eADT7490_ENHANCED_ACOUSTICS2,         
    eADT7490_DUTY_CYCLE_MIN_PWM1,         
    eADT7490_DUTY_CYCLE_MIN_PWM2,         
    eADT7490_DUTY_CYCLE_MIN_PWM3,         
    eADT7490_TEMP_MIN_REM_1,              
    eADT7490_TEMP_MIN_LOCAL,              
    eADT7490_TEMP_MIN_REM_2,              
    eADT7490_TEMP_REM_1_THERM_LIMIT,      
    eADT7490_TEMP_LOCAL_THERM_LIMIT,      
    eADT7490_TEMP_REM_2_THERM_LIMIT,      
    eADT7490_TEMP_REM_1_LOCAL_HIST,       
    eADT7490_TEMP_REM_2_PECI_HIST,        
    eADT7490_XNOR_TREE_TEST_EN,           
    eADT7490_TEMP_REM_1_OFFSET,           
    eADT7490_TEMP_LOCAL_OFFSET,           
    eADT7490_TEMP_REM_2_OFFSET,           
    eADT7490_INT_MASK_1,                  
    eADT7490_INT_MASK_2,                  
    eADT7490_INT_MASK_3,                  
    eADT7490_INT_MASK_4,                  
    eADT7490_EXTENDED_RESOLUTION_1,       
    eADT7490_EXTENDED_RESOLUTION_2,       
    eADT7490_THERM_TIMER_STATUS,          
    eADT7490_THERM_TIMER_LIMIT,           
    eADT7490_TACH_PULSES_PER_REVOL,       
    eADT7490_MFG_TEST_1,                  
    eADT7490_MFG_TEST_2,                  
    eADT7490_GPIO_CONFIG,                 
    eADT7490_VTT_LIMIT_LOW,               
    eADT7490_IMON_LIMIT_LOW,              
    eADT7490_VTT_LIMIT_HIGH,              
    eADT7490_IMON_LIMIT_HIGH,             
    eADT7490_OPER_POINT_PECI,             
    eADT7490_OPER_POINT_REM1,             
    eADT7490_OPER_POINT_LOCAL,            
    eADT7490_OPER_POINT_REM2,             
    eADT7490_DYNAMIC_T_MIN_CTRL_1,        
    eADT7490_DYNAMIC_T_MIN_CTRL_2,        
    eADT7490_DYNAMIC_T_MIN_CTRL_3,        
    eADT7490_PECI0_TEMP_OFFSET,           
    eADT7490_PECI1_TEMP_OFFSET,           
    eADT7490_PECI2_TEMP_OFFSET,           
    eADT7490_PECI3_TEMP_OFFSET,
    eADT7490_MEASUREMENT_DATA,   /**< Aggregation of the measurement data */           
    eADT7490_INIT,
    eADT7490_CONFIGURE_TEST,
    eADT7490_RUN_TEST,
};

/* APIs */
/**
* @brief Read ADT7490 Fan Controller device attribute
*
* @param[in] p_priv points to the private context of the device
* @param[in] attrid id of the attribute
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] idx attribute's index in case the attribute id corresponds to array
* @param[out] p_value pointer to the buffer to hold returned attribute's value
* @param[in] size indicates size of the attribute
*
* @return	error code
*/
ccl_err_t adt7490_attr_read(void *p_priv, b_u32 attrid, 
                            b_u32 offset, b_u32 offset_sz, 
                            b_u32 idx, b_u8 *p_value, b_u32 size);
/**
* @brief Write ADT7490 Fan Controller device attribute
*
* @param[in] p_priv points to the private context of the device
* @param[in] attrid id of the attribute
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] idx attribute's index in case the attribute id corresponds to array
* @param[in] p_value pointer to the buffer which holds written 
*            data value
* @param[in] size indicates size of the
*       attribute
*
* @return	error code
*/
ccl_err_t adt7490_attr_write(void *p_priv, b_u32 attrid, 
                             b_u32 offset, b_u32 offset_sz, 
                             b_u32 idx, b_u8 *p_value, b_u32 size);

#endif /* _DEV_ADT7490_H_ */
