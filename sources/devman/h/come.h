#ifndef _COME_H_
#define _COME_H_

#define GPIO_BOARD_ID_LSB 506
#define GPIO_BOARD_ID_MSB 507

/* board devices signatures enumerator */
typedef enum tage_board_device_sig {
    eDEVBRD_CPLD_SIG                 = (BRDDEV_CPLD_MAJOR      <<16|0), 
    eDEVBRD_I2CBUS_SIG               = (BRDDEV_I2CBUS_MAJOR    <<16|0), 
    eDEVBRD_SPIDEV_0_SIG             = (BRDDEV_SPIDEV_MAJOR    <<16|0),
    eDEVBRD_PMBUS_0_SIG              = (BRDDEV_PMBUS_MAJOR     <<16|0), 
    eDEVBRD_MBOX_0_SIG               = (BRDDEV_MAILBOX_MAJOR   <<16|0), 
    eDEVBRD_EEPROM_SIG               = (BRDDEV_EEPROM_MAJOR    <<16|0), 
    eDEVBRD_ATP_SIG                  = (BRDDEV_ATP_MAJOR       <<16|0),   
    eDEVBRD_CPU_SIG                  = (BRDDEV_CPU_MAJOR       <<16|0),   
    eDEVBRD_UNDEFINED_SIG            = 0xffffffff
} board_device_sig_e;

/*============================================================================*/
/* The board specific I2C subsystem addresses                                 */
/*----------------------------------------------------------------------------*/

/* 1. System IIC AXI IIC Bus Interface v2.0*/

/* 1.1 i2c addresses for the System bus */
typedef enum {
    eDEVBRD_I2CA_EEPROM         = (0xa8>>1), /*0x54*/ 
} board_sys_i2cbus_addresses_e;

/* 1.2 i2c addresses for the PMBUS */
typedef enum {
    eDEVBRD_I2CA_VCCO_3_3V     = (0x5a>>1),  /*0x2d*/ 
} board_pmbus_addresses_e;

/*============================================================================*/
/* The board Logical Ports / Logical SFPs / HW Specified SFPs mapping,        */ 
/* see HW spec port mapping table                                             */
/*----------------------------------------------------------------------------*/
#define DEVBRD_COMX_MAXNUM                       4
#define DEVBRD_LOGPORT_1G_ARTIX_MAXNUM           12
#define DEVBRD_LOGPORT_10G_RED_MAXNUM            12
#define DEVBRD_LOGPORT_100G_RED_MAXNUM           2
#define DEVBRD_LOGPORT_10G_BLACK_MAXNUM          12
#define DEVBRD_LOGPORT_100G_BLACK_MAXNUM         2
#define DEVBRD_LOGPORT_KR_COMX_MAXNUM            4
#define DEVBRD_LOGPORT_1G_VIRTEX_ARTIX_MAXNUM    2
#define DEVBRD_LOGPORT_ETH_SWITCH_MAXNUM         10

#define DEVBRD_LOGPORT_MAXNUM (DEVBRD_LOGPORT_10G_RED_MAXNUM + \
                               DEVBRD_LOGPORT_1G_ARTIX_MAXNUM + \
                               DEVBRD_LOGPORT_100G_RED_MAXNUM + \
                               DEVBRD_LOGPORT_10G_BLACK_MAXNUM + \
                               DEVBRD_LOGPORT_100G_BLACK_MAXNUM + \
                               DEVBRD_LOGPORT_KR_COMX_MAXNUM * DEVBRD_COMX_MAXNUM + \
                               DEVBRD_LOGPORT_1G_VIRTEX_ARTIX_MAXNUM)

/* 1G ports*/
#define LOGPORT_1G_0            0
#define LOGPORT_1G_1            1
#define LOGPORT_1G_2            2
#define LOGPORT_1G_3            3
#define LOGPORT_1G_4            4
#define LOGPORT_1G_5            5
#define LOGPORT_1G_6            6
#define LOGPORT_1G_7            7
#define LOGPORT_1G_8            8
#define LOGPORT_1G_9            9
#define LOGPORT_1G_10           10
#define LOGPORT_1G_11           11
#define LOGPORT_1G_V_A_0        12
#define LOGPORT_1G_V_A_1        13

/* 10G ports */
#define LOGPORT_10G_0           0
#define LOGPORT_10G_1           1
#define LOGPORT_10G_2           2
#define LOGPORT_10G_3           3
#define LOGPORT_10G_4           4
#define LOGPORT_10G_5           5
#define LOGPORT_10G_6           6
#define LOGPORT_10G_7           7
#define LOGPORT_10G_8           8
#define LOGPORT_10G_9           9
#define LOGPORT_10G_10          10
#define LOGPORT_10G_11          11
#define LOGPORT_10G_12          12
#define LOGPORT_10G_13          13
#define LOGPORT_10G_14          14
#define LOGPORT_10G_15          15
#define LOGPORT_10G_16          16
#define LOGPORT_10G_17          17
#define LOGPORT_10G_18          18
#define LOGPORT_10G_19          19
#define LOGPORT_10G_20          20
#define LOGPORT_10G_21          21
#define LOGPORT_10G_22          22
#define LOGPORT_10G_23          23
/* KR ports */
#define LOGPORT_KR_0            24 
#define LOGPORT_KR_1            25 
#define LOGPORT_KR_2            26 
#define LOGPORT_KR_3            27 
#define LOGPORT_KR_4            28 
#define LOGPORT_KR_5            29 
#define LOGPORT_KR_6            30 
#define LOGPORT_KR_7            31 
#define LOGPORT_KR_8            32 
#define LOGPORT_KR_9            33 
#define LOGPORT_KR_10           34
#define LOGPORT_KR_11           35
#define LOGPORT_KR_12           36
#define LOGPORT_KR_13           37
#define LOGPORT_KR_14           38
#define LOGPORT_KR_15           39

/* 100G ports */
#define LOGPORT_100G_0          0
#define LOGPORT_100G_1          1
#define LOGPORT_100G_2          2
#define LOGPORT_100G_3          3


#define LOGPORT_01_HWSPEC_SFP   1
#define LOGPORT_02_HWSPEC_SFP   2
#define LOGPORT_03_HWSPEC_SFP   3
#define LOGPORT_04_HWSPEC_SFP   4
#define LOGPORT_05_HWSPEC_SFP   5
#define LOGPORT_06_HWSPEC_SFP   6
#define LOGPORT_07_HWSPEC_SFP   7
#define LOGPORT_08_HWSPEC_SFP   8
#define LOGPORT_09_HWSPEC_SFP   9
#define LOGPORT_10_HWSPEC_SFP   10
#define LOGPORT_11_HWSPEC_SFP   11
#define LOGPORT_12_HWSPEC_SFP   12
#define LOGPORT_13_HWSPEC_SFP   13
#define LOGPORT_14_HWSPEC_SFP   14
#define LOGPORT_15_HWSPEC_SFP   15
#define LOGPORT_16_HWSPEC_SFP   16
#define LOGPORT_17_HWSPEC_SFP   17
#define LOGPORT_18_HWSPEC_SFP   18
#define LOGPORT_19_HWSPEC_SFP   19
#define LOGPORT_20_HWSPEC_SFP   20
#define LOGPORT_21_HWSPEC_SFP   21
#define LOGPORT_22_HWSPEC_SFP   22
#define LOGPORT_23_HWSPEC_SFP   23
#define LOGPORT_24_HWSPEC_SFP   24
#define LOGPORT_25_HWSPEC_SFP   25 
#define LOGPORT_26_HWSPEC_SFP   26
#define LOGPORT_27_HWSPEC_SFP   27
#define LOGPORT_28_HWSPEC_SFP   28


/* logical port number to hwsfp idx mapping for 32-mode */
static int _logport_hwspec_sfp_index[] = 
{
    LOGPORT_01_HWSPEC_SFP, LOGPORT_02_HWSPEC_SFP, LOGPORT_03_HWSPEC_SFP, LOGPORT_04_HWSPEC_SFP, 
    LOGPORT_05_HWSPEC_SFP, LOGPORT_06_HWSPEC_SFP, LOGPORT_07_HWSPEC_SFP, LOGPORT_08_HWSPEC_SFP, 
    LOGPORT_09_HWSPEC_SFP, LOGPORT_10_HWSPEC_SFP, LOGPORT_11_HWSPEC_SFP, LOGPORT_12_HWSPEC_SFP,
    LOGPORT_13_HWSPEC_SFP, LOGPORT_14_HWSPEC_SFP, LOGPORT_15_HWSPEC_SFP, LOGPORT_16_HWSPEC_SFP,  
    LOGPORT_17_HWSPEC_SFP, LOGPORT_18_HWSPEC_SFP, LOGPORT_19_HWSPEC_SFP, LOGPORT_20_HWSPEC_SFP,  
    LOGPORT_21_HWSPEC_SFP, LOGPORT_22_HWSPEC_SFP, LOGPORT_23_HWSPEC_SFP, LOGPORT_24_HWSPEC_SFP,  
    LOGPORT_25_HWSPEC_SFP, LOGPORT_26_HWSPEC_SFP, LOGPORT_27_HWSPEC_SFP, LOGPORT_28_HWSPEC_SFP,  
};

#endif/* _COME_H_ */
