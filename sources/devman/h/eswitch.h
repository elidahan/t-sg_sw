#ifndef _DEV_ESWITCH_H_
#define _DEV_ESWITCH_H_

/**
 * @name Enumerator to specify ESWITCH (ComCores Ethernet 
 *       Switch) device attributes
 */
enum eswitch_attr_e {
    eESWITCH_IP_ID,
    eESWITCH_IP_VERSION,
    eESWITCH_GEN_INFO_BASIC,
    eESWITCH_GEN_CORE_KHZ,
    eESWITCH_GEN_INFO_MEM,
    eESWITCH_GEN_INFO_OTHER,
    eESWITCH_PORT_SEL,
    eESWITCH_MAC_ENTRY_L,
    eESWITCH_MAC_ENTRY_H,
    eESWITCH_VLAN_CFG,
    eESWITCH_QUEUE_CFG,
    eESWITCH_MAC_ENTRY_PORTS_1,
    eESWITCH_MAC_ENTRY_PORTS_2,
    eESWITCH_MAC_ENTRY_PORTS_3,
    eESWITCH_MAC_AGEOUT,
    eESWITCH_GLOBAL_SNAPSHOT,
    eESWITCH_PORT_TX_PCK,
    eESWITCH_PORT_RX_PCK,
    eESWITCH_PORT_TX_BYTE,
    eESWITCH_PORT_RX_BYTE,
    eESWITCH_PORT_TX_ABORT_OVFL,
    eESWITCH_PORT_RX_ABORT_OVFL,
    eESWITCH_PORT_TX_PCK_ABORT,
    eESWITCH_PORT_RX_PCK_ABORT,
    eESWITCH_MTU,
    eESWITCH_INIT,
    eESWITCH_CONFIGURE_TEST,
    eESWITCH_RUN_TEST,
};

/* APIs */
/**
* @brief Read ESWITCH (ComCores Ethernet Switch) device attribute
*
* @param[in] p_priv points to the private context of the device
* @param[in] attrid id of the attribute
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] idx attribute's index in case the attribute id corresponds to array
* @param[out] p_value pointer to the buffer to hold returned attribute's value
* @param[in] size indicates size of the attribute
*
* @return	error code
*/
ccl_err_t eswitch_attr_read(void *p_priv, b_u32 attrid, 
                         b_u32 offset, b_u32 offset_sz, 
                         b_u32 idx, b_u8 *p_value, b_u32 size);

/**
* @brief Write ESWITCH (ComCores Ethernet Switch) device 
*        attribute
*
* @param[in] p_priv points to the private context of the device
* @param[in] attrid id of the attribute
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] idx attribute's index in case the attribute id corresponds to array
* @param[in] p_value pointer to the buffer which holds written 
*            data value
* @param[in] size indicates size of the attribute
*
* @return	error code
*/
ccl_err_t eswitch_attr_write(void *p_priv, b_u32 attrid, 
                          b_u32 offset, b_u32 offset_sz, 
                          b_u32 idx, b_u8 *p_value, b_u32 size);

#endif/* _DEV_ESWITCH_H_ */
