#ifndef _DEV_AXISPI_H_
#define _DEV_AXISPI_H_

/**
 * @name Enumerator to specify AXI SPI device attributes 
 */
enum axispi_attr_e {
    eAXISPI_IISR,
    eAXISPI_IIER,
    eAXISPI_SRR,
    eAXISPI_CR,
    eAXISPI_SR,
    eAXISPI_DTR,
    eAXISPI_DRR,
    eAXISPI_SSR,
    eAXISPI_TFO,
    eAXISPI_RFO,
    eAXISPI_DGIER,
    eAXISPI_INIT,
    eAXISPI_CONFIGURE_TEST,
    eAXISPI_RUN_TEST,
};

/* APIs */

/**
* @brief Read AXI SPI device attribute
*
* @param[in] p_priv points to the private context of the device
* @param[in] attrid id of the attribute
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] idx attribute's index in case the attribute id corresponds to array
* @param[out] p_value pointer to the buffer to hold returned attribute's value
* @param[in] size indicates size of the attribute
*
* @return	error code
*/
ccl_err_t axispi_attr_read(void *p_priv, b_u32 attrid, 
                           b_u32 offset, b_u32 offset_sz, 
                           b_u32 idx, b_u8 *p_value, b_u32 size);

/**
* @brief Write AXI SPI device attribute
*
* @param[in] p_priv points to the private context of the device
* @param[in] attrid id of the attribute
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] idx attribute's index in case the attribute id corresponds to array
* @param[out] p_value pointer to the buffer to hold returned attribute's value
* @param[in] size indicates size of the attribute
*
* @return	error code
*/
ccl_err_t axispi_attr_write(void *p_priv, b_u32 attrid, 
                            b_u32 offset, b_u32 offset_sz, 
                            b_u32 idx, b_u8 *p_value, b_u32 size);

/**
* @brief Read data through AXI SPI device
*
* @param[in] p_spi_point points to data structure describing spi 
*       route internals
* @param[in] sendbuf  pointer to the buffer to hold sent data 
*       value
* @param[out] recvbuf pointer to the buffer to hold returned 
*       data value
* @param[in] length indicates length of the data
*
* @return	error code
*/
ccl_err_t devaxispi_read_buff(spi_route_t *p_spi_point, b_u8 *sendbuf, 
                              b_u8 *recvbuf, b_u32 length);
/**
* @brief Write data through AXI SPI device
*
* @param[in] p_spi_point points to data structure describing spi 
*       route internals
 @param[in] sendbuf  pointer to the buffer to hold sent data 
*       value
* @param[out] recvbuf pointer to the buffer to hold returned 
*       data value
* @param[in] length indicates length of the data
*
* @return	error code
*/
ccl_err_t devaxispi_write_buff(spi_route_t *p_spi_point, b_u8 *sendbuf, 
                               b_u8 *recvbuf, b_u32 length);

/**
* @brief Write burst of data through AXI SPI device
*
* @param[in] p_spi_point points to data structure describing spi 
*       route internals
 @param[in] sendbuf  pointer to the buffer to hold sent data 
*       value
* @param[out] recvbuf pointer to the buffer to hold returned 
*       data value
* @param[in] burst_l indicates length of the burst 
* @param[in] length indicates length of the data
*
* @return	error code
*/
ccl_err_t devaxispi_write_burst(spi_route_t *p_spi_point, b_u8 *sendbuf, 
                                b_u8 *recvbuf, b_u32 burst_l, b_u32 length);

#endif/* _DEV_AXISPI_H_ */
