#ifndef _DEV_XADC_H_
#define _DEV_XADC_H_

/**
 * @name Enumerator to specify AXI XADC (analog-to-digital 
 *       converter with on-chip sensors) device attributes
 */
enum xadc_attr_e {
    eXADC_SRR,
    eXADC_SR,
    eXADC_AOSR,
    eXADC_CONVSTR,
    eXADC_ADC_RESET,
    eXADC_GIER,
    eXADC_IPISR,
    eXADC_IPIER,
    eXADC_TEMP,
    eXADC_VCCINT,
    eXADC_VCCAUX,
    eXADC_VP_VN,
    eXADC_VREFP,
    eXADC_VREFN,
    eXADC_VBRAM,
    eXADC_SUPPLY_CALIB,
    eXADC_ADC_CALIB,
    eXADC_GAINERR_CALIB,
    eXADC_MAX_TEMP,
    eXADC_MAX_VCCINT,
    eXADC_MAX_VCCAUX,
    eXADC_MAX_VCCBRAM,
    eXADC_MIN_TEMP,
    eXADC_MIN_VCCINT,
    eXADC_MIN_VCCAUX,
    eXADC_MIN_VCCBRAM,
    eXADC_CFR0,
    eXADC_CFR1,
    eXADC_CFR2,
    eXADC_ATR_TEMP_UPPER,
    eXADC_ATR_VCCINT_UPPER,
    eXADC_ATR_VCCAUX_UPPER,
    eXADC_ATR_OT_UPPER,
    eXADC_ATR_TEMP_LOWER,
    eXADC_ATR_VCCINT_LOWER,
    eXADC_ATR_VCCAUX_LOWER,
    eXADC_ATR_OT_LOWER,
    eXADC_ATR_VBRAM_UPPER,
    eXADC_ATR_VBRAM_LOWER,
    eXADC_INIT,
    eXADC_CONFIGURE_TEST,
    eXADC_RUN_TEST,
};

/* APIs */
/**
* @brief Read XADC (analog-to-digital converter 
*       with on-chip sensors) device attribute
*
* @param[in] p_priv points to the private context of the device
* @param[in] attrid id of the attribute
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] idx attribute's index in case the attribute id corresponds to array
* @param[out] p_value pointer to the buffer to hold returned attribute's value
* @param[in] size indicates size of the attribute
*
* @return	error code
*/
ccl_err_t xadc_attr_read(void *p_priv, b_u32 attrid, 
                         b_u32 offset, b_u32 offset_sz, 
                         b_u32 idx, b_u8 *p_value, b_u32 size);

/**
* @brief Write XADC (analog-to-digital converter 
*       with on-chip sensors) device attribute
*
* @param[in] p_priv points to the private context of the device
* @param[in] attrid id of the attribute
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] idx attribute's index in case the attribute id corresponds to array
* @param[in] p_value pointer to the buffer which holds written 
*            data value
* @param[in] size indicates size of the attribute
*
* @return	error code
*/
ccl_err_t xadc_attr_write(void *p_priv, b_u32 attrid, 
                          b_u32 offset, b_u32 offset_sz, 
                          b_u32 idx, b_u8 *p_value, b_u32 size);

#endif/* _DEV_XADC_H_ */
