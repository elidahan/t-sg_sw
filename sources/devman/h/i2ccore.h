#ifndef _DEV_I2CCORE_H_
#define _DEV_I2CCORE_H_

/**
 * @name Enumerator to specify Xilinx I2C Bus Interface device 
 *       attributes
 */
enum i2ccore_attr_e {
    eI2CCORE_GIE,
    eI2CCORE_ISR,
    eI2CCORE_IER,
    eI2CCORE_SOFTR,
    eI2CCORE_CR,
    eI2CCORE_SR,
    eI2CCORE_TX_FIFO,
    eI2CCORE_RX_FIFO,
    eI2CCORE_ADR,
    eI2CCORE_TX_FIFO_OCY,
    eI2CCORE_RX_FIFO_OCY,
    eI2CCORE_TEN_ADR,
    eI2CCORE_RX_FIFO_PIRQ,
    eI2CCORE_GPO,
    eI2CCORE_INIT,
    eI2CCORE_RESET,
    eI2CCORE_CONFIGURE_TEST,
    eI2CCORE_RUN_TEST,
};

/* APIs */
/**
* @brief Read Xilinx I2C Bus Interface device attribute
*
* @param[in] p_priv points to the private context of the device
* @param[in] attrid id of the attribute
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] idx attribute's index in case the attribute id corresponds to array
* @param[out] p_value pointer to the buffer to hold returned attribute's value
* @param[in] size indicates size of the attribute
*
* @return	error code
*/
ccl_err_t i2ccore_attr_read(void *p_priv, b_u32 attrid, 
                            b_u32 offset, b_u32 offset_sz, 
                            b_u32 idx, b_u8 *p_value, b_u32 size);

/**
* @brief Write Xilinx I2C Bus Interface device attribute
*
* @param[in] p_priv points to the private context of the device
* @param[in] attrid id of the attribute
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] idx attribute's index in case the attribute id corresponds to array
* @param[in] p_value pointer to the buffer which holds written 
*            data value
* @param[in] size indicates size of the attribute
*
* @return	error code
*/
ccl_err_t i2ccore_attr_write(void *p_priv, b_u32 attrid, 
                             b_u32 offset, b_u32 offset_sz, 
                             b_u32 idx, b_u8 *p_value, b_u32 size);

/**
* @brief Read data via Xilinx I2C Bus Interface
*
* @param[in] p_i2c_point points to data structure describing i2c
*       route internals
* @param[in] mux_pin indicates which mux port to activate in 
*       order to access specific device
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[out] p_value pointer to the buffer to hold returned 
*       data value
* @param[in] size indicates size of the data
*
* @return	error code
*/
ccl_err_t devi2ccore_read_buff(i2c_route_t *p_i2c_point, 
                               b_u8 mux_pin, 
                               b_u32 offset, b_u32 offset_sz, 
                               b_u8 *p_value, b_u32 size);

/**
* @brief Write data via Xilinx I2C Bus Interface
*
* @param[in] p_i2c_point points to data structure describing i2c
*       route internals
* @param[in] mux_pin indicates which mux port to activate in 
*       order to access specific device
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] p_value pointer to the buffer which holds written 
*            data value
* @param[in] size indicates size of the data
*
* @return	error code
*/
ccl_err_t devi2ccore_write_buff(i2c_route_t *p_i2c_point,
                                b_u8 mux_pin, 
                                b_u32 offset, b_u32 offset_sz, 
                                b_u8 *p_value, b_u32 size);

#endif/* _DEV_I2CCORE_H_ */
