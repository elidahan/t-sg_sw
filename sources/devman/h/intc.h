#ifndef _DEV_INTC_H_
#define _DEV_INTC_H_

/**
 * @name Enumerator to specify Xilinx Interrupt Controller 
 *       device attributes
 */
enum intc_attr_e {
    eINTC_ISR,  
    eINTC_IPR,  
    eINTC_IER,  
    eINTC_IAR,  
    eINTC_SIE,  
    eINTC_CIE,  
    eINTC_IVR,  
    eINTC_MER,  
    eINTC_IMR,  
    eINTC_ILR,  
    eINTC_IVAR, 
    eINTC_IVEAR,
    eINTC_INIT,
    eINTC_CONFIGURE_TEST,
    eINTC_RUN_TEST,
};

/* APIs */
/**
* @brief Read Xilinx Interrupt Controller device attribute
*
* @param[in] p_priv points to the private context of the device
* @param[in] attrid id of the attribute
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] idx attribute's index in case the attribute id corresponds to array
* @param[out] p_value pointer to the buffer to hold returned attribute's value
* @param[in] size indicates size of the attribute
*
* @return	error code
*/
ccl_err_t intc_attr_read(void *p_priv, b_u32 attrid, 
                         b_u32 offset, b_u32 offset_sz, 
                         b_u32 idx, b_u8 *p_value, b_u32 size);

/**
* @brief Write Xilinx Interrupt Controller device attribute
*
* @param[in] p_priv points to the private context of the device
* @param[in] attrid id of the attribute
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] idx attribute's index in case the attribute id corresponds to array
* @param[in] p_value pointer to the buffer which holds written 
*            data value
* @param[in] size indicates size of the attribute
*
* @return	error code
*/
ccl_err_t intc_attr_write(void *p_priv, b_u32 attrid, 
                          b_u32 offset, b_u32 offset_sz, 
                          b_u32 idx, b_u8 *p_value, b_u32 size);

#endif/* _DEV_INTC_H_ */
