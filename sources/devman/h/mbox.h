#ifndef _DEV_MBOX_H_
#define _DEV_MBOX_H_

/**
 * @name Enumerator to specify Xilinx Mailbox Core device 
 *       attributes
 */
enum mbox_attr_e {
    eMBOX_WRITE_REG,
    eMBOX_READ_REG,
    eMBOX_STATUS_REG,
    eMBOX_ERROR_REG,
    eMBOX_SIT_REG,
    eMBOX_RIT_REG,
    eMBOX_IS_REG,
    eMBOX_IE_REG,
    eMBOX_IP_REG,
    eMBOX_CTRL_REG
};

/* APIs */
/**
* @brief Read Xilinx Mailbox Core device attribute
*
* @param[in] p_priv points to the private context of the device
* @param[in] attrid id of the attribute
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] idx attribute's index in case the attribute id corresponds to array
* @param[out] p_value pointer to the buffer to hold returned attribute's value
* @param[in] size indicates size of the attribute
*
* @return	error code
*/
ccl_err_t mbox_attr_read(void *p_priv, b_u32 attrid, 
                         b_u32 offset, b_u32 offset_sz, 
                         b_u32 idx, b_u8 *p_value, b_u32 size);

/**
* @brief Write Xilinx Mailbox Core device attribute
*
* @param[in] p_priv points to the private context of the device
* @param[in] attrid id of the attribute
* @param[in] offset attribute's relative address inside the device
* @param[in] offset_sz size of the address
* @param[in] idx attribute's index in case the attribute id corresponds to array
* @param[in] p_value pointer to the buffer which holds written 
*            data value
* @param[in] size indicates size of the attribute
*
* @return	error code
*/
ccl_err_t mbox_attr_write(void *p_priv, b_u32 attrid, 
                          b_u32 offset, b_u32 offset_sz, 
                          b_u32 idx, b_u8 *p_value, b_u32 size);

#endif/* _DEV_MBOX_H_ */
