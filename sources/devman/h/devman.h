/********************************************************************************/
/**
 * @file devman.h
 * @brief Device Manager framework header file 
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#ifndef _BATM_DEVMAN_H_
#define _BATM_DEVMAN_H_

#define UNUSED(v) ((void)(v))
#define PDEBUGG(fmt, args...)
#define PRINTL(fmt, args...) fprintf(stderr, fmt, ## args) 

#define DEVMAN_DEVNAME_SIZE 16

/** device manager commands enumeration */
typedef enum {
    eDEVMAN_CMD_NONE = 0,
    eDEVMAN_CMD_SHOW,
    eDEVMAN_CMD_CONF,
    eDEVMAN_CMD_TEST,
    eDEVMAN_CMD_EXEC,
    eDEVMAN_CMD_HELP,

} devman_cmd_type_e;

#define DEVMAN_CMD_PARMS_MAXNUM     16
#define DEVMAN_CMD_PARM_LEN         256

/** device manager command */
typedef struct {
    devman_cmd_type_e   type;
    char                parm[DEVMAN_CMD_PARMS_MAXNUM][DEVMAN_CMD_PARM_LEN];
    int                 parms_num;                     
} devman_cmd_t;

/** device object(instance) handle */
typedef void *devo_t;

/** device attribute types enumerator */
typedef enum {
    eDEVMAN_ATTR_BOOL,
    eDEVMAN_ATTR_BYTE,
    eDEVMAN_ATTR_HALF,
    eDEVMAN_ATTR_WORD,
    eDEVMAN_ATTR_DOUBLE,
    eDEVMAN_ATTR_STRING,
    eDEVMAN_ATTR_BUFF,
    eDEVMAN_ATTR_ETHADDR,
    eDEVMAN_ATTR_CMD,
    eDEVBRD_ATTRTYPES_NUM
} attr_type_e;

/** device attribute flags enumerator */
typedef enum {
    eDEVMAN_ATTRFLAG_REGULAR  = 0,
    eDEVMAN_ATTRFLAG_HIDDEN   = 0x00000001,
    eDEVMAN_ATTRFLAG_RO       = 0x00000002,
    eDEVMAN_ATTRFLAG_WO       = 0x00000004,
    eDEVMAN_ATTRFLAG_OPTIONAL = 0x00000008,
    eDEVBRD_ATTRFLAGS_NUM
} attr_flag_e;

#define DEVMAN_ATTRS_MAXNUM             512
#define DEVMAN_ATTRS_NAME_LEN           128
#define DEVMAN_ATTRS_VISIBLE_DEPTH      256
#define DEVMAN_ATTRS_PRINT_ELEMS_DSHOW  32
#define DEVMAN_ATTRS_PRINT_ELEMS_ASHOW  64
#define DEVMAN_ATTRS_PRINT_MAXBYTES     16
#define DEVMAN_ATTRS_HELP_STRLEN        256
#define DEVMAN_ATTRS_SIZE_MAX           256

/** device attribute description structure */
typedef struct {
    char            name[DEVMAN_ATTRS_NAME_LEN];
    b_u32           id;
    attr_type_e     type;
    b_u32           size;
    b_u32           maxnum;
    b_u32           offset;
    b_u32           offset_sz;
    const char     *format;
    b_u32           flags;
    char            help[DEVMAN_ATTRS_HELP_STRLEN];
} device_attr_t;

#define  DEVMAN_DEVICE_CMD_NAME_LEN        32 
#define  DEVMAN_DEVICE_CMD_PARM_NAME_LEN   32
#define  DEVMAN_DEVICE_CMD_PARM_STR_LEN    64
#define  DEVMAN_DEVICE_CMD_PARMS_MAXNUM    11
#define  DEVMAN_DEVICE_CMD_HELP_STRLEN     1024
/** device command parameter description structure */
typedef struct {
    char            name[DEVMAN_DEVICE_CMD_PARM_NAME_LEN];
    attr_type_e     type;
    b_u32           size;
    b_u32           rsize; /**< real size for buffer type parameter */
    const char     *format;
    attr_flag_e     flags;
    b_u32           value;
    char            buf[DEVMAN_CMD_PARM_LEN];
} device_cmd_parm_t;

/** device command description structure */
typedef struct {
    char                name[DEVMAN_DEVICE_CMD_NAME_LEN];
    ccl_err_t           (*f_cmd)(devo_t devo, device_cmd_parm_t parms[], b_u16 parms_num);
    device_cmd_parm_t  *parms;
    b_u32               parms_num;
    char                help[DEVMAN_DEVICE_CMD_HELP_STRLEN];    
} device_cmd_t;

/** driver control structure */
typedef struct tagt_device_driver device_driver_t;

#define DEVMAN_DEVNAME_LEN 32
#define DEVMAN_DEVICES_MAXNUM 256
/** device control structure */
typedef struct {
    char             name[DEVMAN_DEVNAME_LEN];
    b_u32            sig;
    device_driver_t  *p_drv;
    void             *p_priv;

} devman_device_t;

#define DEVMAN_DRIVERS_MAXNUM 32
/** driver control structure */
struct tagt_device_driver {
    char            name[DEVMAN_DEVNAME_LEN];
    ccl_err_t       (*f_add)(void *v_devo, void *v_brdspec);
    ccl_err_t       (*f_cut)(void *v_devo);
    b_u32           instances;
    device_attr_t  *attrs;
    b_u32           attrs_num;
    device_cmd_t   *cmds;
    b_u32           cmds_num;
    b_u32           priv_size;
};

/**============================================================================*/
/** Device Manager user/drivers APIs                                           */
/**----------------------------------------------------------------------------*/

/** Devices types/instances maintanance */

/**
 * @brief Register device type 
 * @param[in]  p_drv - device driver
 * @return CCL_OK on success, CL_BAD_PARAM_ERR on failure 
 */
extern ccl_err_t devman_driver_register(device_driver_t *p_drv);

/**
 * @brief Unregister device type 
 * @param[in]  p_drv - device driver
 * @return CCL_OK on success, CL_BAD_PARAM_ERR on failure 
 */
extern ccl_err_t devman_driver_unregister(device_driver_t *p_drv);

/**
 * @brief Find device type by name
 * @param[in]  name - name of device type 
 * @param[out] pp_drv - retrieved device type
 * @return CCL_OK on success, CL_BAD_PARAM_ERR on failure 
 */
extern ccl_err_t devman_device_type(char *name, device_driver_t **pp_drv);

/**
 * @brief Create device instance of registered type; magic 
 *        number and board specific parameters are designated
 * @param[in] type_name - device type name
 * @param[in] sig - device type signature
 * @param[in] p_brdspec - device board specific content
 * @param[out] p_devo - device handle
 * @return CCL_OK on success, CL_BAD_PARAM_ERR on failure 
 */
extern ccl_err_t devman_device_create(char *type_name, b_u32 sig, 
                                      void *p_brdspec, devo_t *p_devo);

/**
 * @brief Delete device instance
 * @param[in] devo - device handle
 * @return CCL_OK on success, CL_BAD_PARAM_ERR on failure 
 */
extern ccl_err_t devman_device_delete(IN devo_t devo);

/**
 * @brief Find device instance handle by name that can be 
 * "type/discr"; if discriminator isn't specified, the object/0 
 * is returned 
 * @param[in] dev_name - name 
 * @param[out] p_devo - device handle
 * @return CCL_OK on success, CL_BAD_PARAM_ERR on failure 
 * @note p_devo is NULL if not found
 */
extern ccl_err_t devman_device_object(char *dev_name, devo_t *p_devo);

/**
 * @brief Find device instance handle by type name and specified
 *        integer discriminator
 * @param[in] type_name - name 
 * @param[in] discr - discriminator 
 * @param[out] p_devo - device handle
 * @return CCL_OK on success, CCL_BAD_PARAM_ERR on failure 
 * @note p_devo is NULL if not found
 */
extern ccl_err_t devman_device_object_identify(char *type_name, 
                                               int discr, devo_t *p_devo);

/**
 * @brief Get the device private context(user driver context)
 * @param[in] devo - device handle
 * @return pointer to device private context, NULL on failure 
 */
extern void *devman_device_private(devo_t devo);

/**
 * @brief Launch device command by name with parameters, given as strings array
 * @param[in] devo - device handle 
 * @param[in] cmd_name - command string 
 * @param[in] parm_vals_str - array of command parameters 
 * @param[in] parms_num - number of command parameters 
 * @return CCL_OK on success, error code on failure 
 */
extern ccl_err_t devman_device_cmd_exec(devo_t devo, char *cmd_name, 
                                        char *parm_vals_str[], b_u32 parms_num);

/*** Devices Attributes maintanance */


/**
 * @brief Find attribute descriptor by name
 * @param[in] devo - device handle 
 * @param[in] name - the name of attribute
 * @param[out] pp_attr - pointer to retrieved attribute
 * @return CCL_OK on success, CL_BAD_PARAM_ERR on failure 
 * @note *pp_attr is NULL if not found 
 */
extern ccl_err_t devman_attr_descr_find(devo_t devo, char *name, 
                                        device_attr_t **pp_attr);

/**
 * @brief Write attribute by the given descriptor
 * @param[in] devo - device handle 
 * @param[in] p_attr - attribute 
 * @param[in] idx - index of array element 
 * @param[in] p_value - value to set
 * @return CCL_OK on success, error code on failure 
 * @note CCL_OK is returned if attribute is read-only 
 */
extern ccl_err_t devman_attr_set(IN devo_t devo, IN device_attr_t *p_attr, 
                                 IN b_u32 idx, IN b_u8 *p_value);

/**
 * @brief Read attribute by the given descriptor
 * @param[in] devo - device handle 
 * @param[in] p_attr - attribute 
 * @param[in] idx - index of array element 
 * @param[out] p_value - retrieved value
 * @return CCL_OK on success, error code on failure 
 */
extern ccl_err_t devman_attr_get(IN devo_t devo, IN device_attr_t *p_attr, 
                                 IN b_u32 idx, OUT b_u8 *p_value);

/**
 * @brief Write string-represented attribute by the given 
 *        descriptor
 * @param[in] devo - device handle 
 * @param[in] p_attr - attribute 
 * @param[in] idx - index of array element 
 * @param[in] string - string to set
 * @return CCL_OK on success, error code on failure 
 */
extern ccl_err_t devman_attr_set_s(IN devo_t devo, IN device_attr_t *p_attr, 
                                   IN b_u32 idx, IN char *string);

/**
 * @brief Read attribute by the given descriptor and 
 *        string-represent it
 * @param[in] devo - device handle 
 * @param[in] p_attr - attribute 
 * @param[in] idx - index of array element 
 * @param[in] size - size of string represented attribute 
 * @param[out] string - Retrieved string attribute 
 * @return CCL_OK on success, error code on failure 
 */
extern ccl_err_t devman_attr_get_s(IN devo_t devo, IN device_attr_t *p_attr, 
                                   IN b_u32 idx, IN size_t size, OUT char *string);

/**
 * @brief Write attributes array element as bool value 
 * @param[in] devo - device handle 
 * @param[in] name - name of the attribute 
 * @param[in] idx - index of array element 
 * @param[in] value - boolean value
 * @return CCL_OK on success, error code on failure 
 * @note CCL_OK is returned if attribute is read-only 
 */
extern ccl_err_t devman_attr_array_set_bool(IN devo_t devo, IN char *name, 
                                            IN b_u32 idx, IN b_bool value);

/**
 * @brief Read attributes array element as bool value 
 * @param[in] devo - device handle 
 * @param[in] name - name of the attribute 
 * @param[in] idx - index of array element 
 * @param[out] p_value - retrieved boolean value
 * @return CCL_OK on success, error code on failure 
 */
extern ccl_err_t devman_attr_array_get_bool(IN devo_t devo, IN char *name, 
                                            IN b_u32 idx, OUT b_bool *p_value);

/**
 * @brief Write attributes array element as byte value 
 * @param[in] devo - device handle 
 * @param[in] name - name of the attribute 
 * @param[in] idx - index of array element 
 * @param[in] value - boolean value
 * @return CCL_OK on success, error code on failure 
 * @note CCL_OK is returned if attribute is read-only 
 */
extern ccl_err_t devman_attr_array_set_byte(IN devo_t devo, IN char *name, 
                                            IN b_u32 idx, IN b_u8 value);

/**
 * @brief Read attributes array element as byte value 
 * @param[in] devo - device handle 
 * @param[in] name - name of the attribute 
 * @param[in] idx - index of array element 
 * @param[out] p_value - retrieved boolean value
 * @return CCL_OK on success, error code on failure 
 */
extern ccl_err_t devman_attr_array_get_byte(IN devo_t devo, IN char *name, 
                                            IN b_u32 idx, OUT b_u8 *p_value);

/**
 * @brief Write attributes array element as half word value 
 * @param[in] devo - device handle 
 * @param[in] name - name of the attribute 
 * @param[in] idx - index of array element 
 * @param[in] value - half word value
 * @return CCL_OK on success, error code on failure 
 * @note CCL_OK is returned if attribute is read-only 
 */
extern ccl_err_t devman_attr_array_set_half(IN devo_t devo, IN char *name, 
                                            IN b_u32 idx, IN b_u16 value);

/**
 * @brief Read attributes array element as half word value 
 * @param[in] devo - device handle 
 * @param[in] name - name of the attribute 
 * @param[in] idx - index of array element 
 * @param[out] p_value - retrieved half word value
 * @return CCL_OK on success, error code on failure 
 */
extern ccl_err_t devman_attr_array_get_half(IN devo_t devo, IN char *name, 
                                            IN b_u32 idx, OUT b_u16 *p_value);

/**
 * @brief Write attributes array element as word value 
 * @param[in] devo - device handle 
 * @param[in] name - name of the attribute 
 * @param[in] idx - index of array element 
 * @param[in] value - word value
 * @return CCL_OK on success, error code on failure 
 * @note CCL_OK is returned if attribute is read-only 
 */
extern ccl_err_t devman_attr_array_set_word(IN devo_t devo, IN char *name, 
                                            IN b_u32 idx, IN b_u32 value);

/**
 * @brief Read attributes array element as word value 
 * @param[in] devo - device handle 
 * @param[in] name - name of the attribute 
 * @param[in] idx - index of array element 
 * @param[out] p_value - retrieved word value
 * @return CCL_OK on success, error code on failure 
 */
extern ccl_err_t devman_attr_array_get_word(IN devo_t devo, IN char *name, 
                                            IN b_u32 idx, OUT b_u32 *p_value);

/**
 * @brief Write attributes array element as double value 
 * @param[in] devo - device handle 
 * @param[in] name - name of the attribute 
 * @param[in] idx - index of array element 
 * @param[in] value - double value
 * @return CCL_OK on success, error code on failure 
 * @note CCL_OK is returned if attribute is read-only 
 */
extern ccl_err_t devman_attr_array_set_double(IN devo_t devo, IN char *name, 
                                            IN b_u32 idx, IN b_u64 value);

/**
 * @brief Read attributes array element as double value 
 * @param[in] devo - device handle 
 * @param[in] name - name of the attribute 
 * @param[in] idx - index of array element 
 * @param[out] p_value - retrieved double value
 * @return CCL_OK on success, error code on failure 
 */
extern ccl_err_t devman_attr_array_get_double(IN devo_t devo, IN char *name, 
                                            IN b_u32 idx, OUT b_u64 *p_value);

/**
 * @brief Write attributes array element as buffer value 
 * @param[in] devo - device handle 
 * @param[in] name - name of the attribute 
 * @param[in] idx - index of array element 
 * @param[in] p_value - byte array 
 * @param[in] length - number of array elements 
 * @return CCL_OK on success, error code on failure 
 * @note CCL_OK is returned if attribute is read-only 
 */
extern ccl_err_t devman_attr_array_set_buff(IN devo_t devo, IN char *name, 
                                            IN b_u32 idx, IN b_u8 *p_value, 
                                            IN b_u32 length);

/**
 * @brief Read attributes array element as buffer value 
 * @param[in] devo - device handle 
 * @param[in] name - name of the attribute 
 * @param[in] idx - index of array element 
 * @param[out] p_value - retrieved byte array 
 * @param[in] length - number of array elements 
 * @return CCL_OK on success, error code on failure 
 */
extern ccl_err_t devman_attr_array_get_buff(IN devo_t devo, IN char *name, 
                                            IN b_u32 idx, OUT b_u8 *p_value, 
                                            IN b_u32 length);


/**
 * @brief Write attributes array element as buffer value from 
 *        specified offset
 * @param[in] devo - device handle 
 * @param[in] name - name of the attribute 
 * @param[in] idx - index of array element 
 * @param[in] p_value - byte array 
 * @param[in] offset - offset to write byte array to
 * @param[in] offset_sz - offset size
 * @param[in] length - number of array elements 
 * @return CCL_OK on success, error code on failure 
 * @note CCL_OK is returned if attribute is read-only 
 */
ccl_err_t devman_attr_array_set_buff_with_offset(IN devo_t devo, IN char *name, 
                                                 IN b_u32 idx, IN b_u8 *p_value, 
                                                 IN b_u32 offset, IN b_u32 offset_sz,
                                                 IN b_u32 length);

/**
 * @brief Read attributes array element as buffer value from 
 *        specified offset
 *        
 * @param[in] devo - device handle 
 * @param[in] name - name of the attribute 
 * @param[in] idx - index of array element 
 * @param[out] p_value - retrieved byte array 
 * @param[in] offset - offset to read byte array from 
 * @param[in] offset_sz - offset size
 * @param[in] length - number of array elements 
 * @return CCL_OK on success, error code on failure 
 */
extern ccl_err_t devman_attr_array_get_buff_with_offset(IN devo_t devo, IN char *name, 
                                                        IN b_u32 idx, IN b_u8 *p_value, 
                                                        IN b_u32 offset, IN b_u32 offset_sz,
                                                        IN b_u32 length);

/**
 * @brief Write attributes array element as command 
 * @param[in] devo - device handle 
 * @param[in] name - name of the attribute 
 * @param[in] idx - index of array element 
 * @return CCL_OK on success, error code on failure 
 * @note CCL_OK is returned if attribute is read-only 
 */
ccl_err_t devman_attr_array_set_cmd(IN devo_t devo, IN char *name, IN b_u32 idx);
/**
 * @brief Write attributes array element as string 
 * @param[in] devo - device handle 
 * @param[in] name - name of the attribute 
 * @param[in] idx - index of array element 
 * @param[in] p_value - byte array 
 * @return CCL_OK on success, error code on failure 
 * @note CCL_OK is returned if attribute is read-only 
 */
extern ccl_err_t devman_attr_array_set_string(IN devo_t devo, IN char *name, 
                                              IN b_u32 idx, IN b_u8 *p_value);

/**
 * @brief Read attributes array element as string
 * @param[in] devo - device handle 
 * @param[in] name - name of the attribute 
 * @param[in] idx - index of array element 
 * @param[out] p_value - retrieved byte array 
 * @return CCL_OK on success, error code on failure 
 */
extern ccl_err_t devman_attr_array_get_string(IN devo_t devo, IN char *name, 
                                              IN b_u32 idx, OUT b_u8 *p_value);

/**
 * @brief Write attribute as bool value 
 * @param[in] devo - device handle 
 * @param[in] name - name of the attribute 
 * @param[in] value - boolean value
 * @return CCL_OK on success, error code on failure 
 * @note CCL_OK is returned if attribute is read-only 
 */
extern ccl_err_t devman_attr_set_bool(IN devo_t devo, IN char *name, IN b_bool value);

/**
 * @brief Read attribute as bool value 
 * @param[in] devo - device handle 
 * @param[in] name - name of the attribute 
 * @param[out] p_value - retrieved boolean value
 * @return CCL_OK on success, error code on failure 
 */
extern ccl_err_t devman_attr_get_bool(IN devo_t devo, IN char *name, OUT b_bool *p_value);

/**
 * @brief Write attribute as byte value 
 * @param[in] devo - device handle 
 * @param[in] name - name of the attribute 
 * @param[in] value - byte value
 * @return CCL_OK on success, error code on failure 
 * @note CCL_OK is returned if attribute is read-only 
 */
extern ccl_err_t devman_attr_set_byte(IN devo_t devo, IN char *name, IN b_u8 value);

/**
 * @brief Read attribute as byte value 
 * @param[in] devo - device handle 
 * @param[in] name - name of the attribute 
 * @param[out] p_value - retrieved byte value
 * @return CCL_OK on success, error code on failure 
 */
extern ccl_err_t devman_attr_get_byte(IN devo_t devo, IN char *name, OUT b_u8 *p_value);

/**
 * @brief Write attribute as half word value 
 * @param[in] devo - device handle 
 * @param[in] name - name of the attribute 
 * @param[in] value - half word value
 * @return CCL_OK on success, error code on failure 
 * @note CCL_OK is returned if attribute is read-only 
 */
extern ccl_err_t devman_attr_set_half(IN devo_t devo, IN char *name, IN b_u16 value);

/**
 * @brief Read attribute as half word value 
 * @param[in] devo - device handle 
 * @param[in] name - name of the attribute 
 * @param[out] p_value - retrieved half word value
 * @return CCL_OK on success, error code on failure 
 */
extern ccl_err_t devman_attr_get_half(IN devo_t devo, IN char *name, OUT b_u16 *p_value);

/**
 * @brief Write attribute as word value 
 * @param[in] devo - device handle 
 * @param[in] name - name of the attribute 
 * @param[in] value - word value
 * @return CCL_OK on success, error code on failure 
 * @note CCL_OK is returned if attribute is read-only 
 */
extern ccl_err_t devman_attr_set_word(IN devo_t devo, IN char *name, IN b_u32 value);

/**
 * @brief Read attribute as word value 
 * @param[in] devo - device handle 
 * @param[in] name - name of the attribute 
 * @param[out] p_value - retrieved word value
 * @return CCL_OK on success, error code on failure 
 */
extern ccl_err_t devman_attr_get_word(IN devo_t devo, IN char *name, OUT b_u32 *p_value);

/**
 * @brief Write attribute as double value 
 * @param[in] devo - device handle 
 * @param[in] name - name of the attribute 
 * @param[in] value - double value
 * @return CCL_OK on success, error code on failure 
 * @note CCL_OK is returned if attribute is read-only 
 */
extern ccl_err_t devman_attr_set_double(IN devo_t devo, IN char *name, IN b_u64 value);

/**
 * @brief Read attribute as double value 
 * @param[in] devo - device handle 
 * @param[in] name - name of the attribute 
 * @param[out] p_value - retrieved double value
 * @return CCL_OK on success, error code on failure 
 */
extern ccl_err_t devman_attr_get_double(IN devo_t devo, IN char *name, OUT b_u64 *p_value);

/**
 * @brief Write attribute as buffer value 
 * @param[in] devo - device handle 
 * @param[in] name - name of the attribute 
 * @param[in] p_value - byte array 
 * @param[in] length - number of array elements 
 * @return CCL_OK on success, error code on failure 
 * @note CCL_OK is returned if attribute is read-only 
 */
extern ccl_err_t devman_attr_set_buff(IN devo_t devo, IN char *name, 
                                      IN b_u8 *p_value, IN b_u32 length);

/**
 * @brief Read attribute as buffer value 
 * @param[in] devo - device handle 
 * @param[in] name - name of the attribute 
 * @param[out] p_value - retrieved byte array 
 * @param[in] length - number of array elements 
 * @return CCL_OK on success, error code on failure 
 */
extern ccl_err_t devman_attr_get_buff(IN devo_t devo, IN char *name, 
                                      OUT b_u8 *p_value, IN b_u32 length);

/**
 * @brief Write attribute as string 
 * @param[in] devo - device handle 
 * @param[in] name - name of the attribute 
 * @param[in] p_value - byte array 
 * @return CCL_OK on success, error code on failure 
 * @note CCL_OK is returned if attribute is read-only 
 */
extern ccl_err_t devman_attr_set_string(IN devo_t devo, 
                                        IN char *name, 
                                        IN b_u8 *p_value);

/**
 * @brief Read attribute as string
 * @param[in] devo - device handle 
 * @param[in] name - name of the attribute 
 * @param[out] p_value - retrieved byte array 
 * @return CCL_OK on success, error code on failure 
 */
extern ccl_err_t devman_attr_get_string(IN devo_t devo, 
                                        IN char *name, 
                                        OUT b_u8 *p_value);

/**
 * @brief Show all device instances of the given type
 * @param[in] type_name - name of the type
 * @return void 
 * @note prints to syslog 
 */
extern void devman_type_devices_show(char *type_name);

/**
 * @brief Show all registered types of devices 
 * @return void 
 * @note prints to syslog 
 */
extern void devman_types_show(void);

/**
 * @brief Show all devices instances for all registered types 
 * @return void 
 * @note prints to syslog 
 */
extern void devman_devices_show_all(void);

/**
 * @brief Device Manager Command processing routine
 * @param[in] p_cmd - Device Manager command
 * @return CCL_OK on success, error code on failure 
 */
extern ccl_err_t devman_command_handle(devman_cmd_t *p_cmd);

/**
 * @brief Show Device Manager Command
 * @param[in] p_cmd - Device Manager command
 * @return void 
 * @note prints to syslog 
 */
extern void devman_command_show(devman_cmd_t *p_cmd);

/**
 * @brief Byte reorder in the given buffer
 * @param[in] p_buf pointer to the buffer 
 * @param[in] size buffer size 
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_byte_reorder(b_u8 *p_buf, b_u32 size);

/**
 * @brief Init crc table
 * @param[out] p_buf pointer to the buffer 
 * @param[in] size buffer size 
 * @return CCL_OK on success, error code on failure 
 */
ccl_err_t devman_init_crc_table(b_u8 *p_buf, b_u32 size);

/**
 * @brief get crc value
 * @param[in] message pointer to the buffer to calculate crc on
 * @param[in] size buffer size 
 * @param[in] crc_table table with calculated crc coefficients 
 *  
 * @return calculated crc value 
 */
b_u8 devman_get_crc(b_u8 message[], b_u32 size, b_u8 crc_table[]);
/**
 * @brief Call the ioctl syscall for the devman kernel side
 * @param[in] opcode - ioctl opcode 
 * @param[in] v_parm - ioctl parameters 
 * @return CCL_OK on success, error code on failure 
 * @note prints to syslog 
 */
ccl_err_t devman_ioctl(b_i32 opcode, void *v_parm);

#endif /* _BATM_DEVMAN_H_*/
