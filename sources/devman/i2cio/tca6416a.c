/********************************************************************************/
/**
 * @file tca6416a.c
 * @brief I2C IO Expander TCA6416A device manager driver 
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <semaphore.h>
#include <linux/limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "i2ccore.h"       
#include "tca6416a.h"

#define TCA6416A_DEBUG
/* printing/error-returning macros */
#ifdef TCA6416A_DEBUG_DEBUG
    #define PDEBUG(fmt, args...) \
         ccl_syslog_err("TCA6416A_DEBUG: %s(): " fmt, __FUNCTION__, ## args)
#else
    #define PDEBUG(fmt, args...)
#endif
#define TCA6416A_ERROR_RETURN(err) do { \
   ccl_syslog_err("Error! %s[%d], error=%i\n", __FUNCTION__, __LINE__, (err)); \
   return err; \
} while (0)

/* tca6416a registers offsets */
#define TCA6416A_INPUT_PORT_0            0x00
#define TCA6416A_INPUT_PORT_1            0x01
#define TCA6416A_OUTPUT_PORT_0           0x02
#define TCA6416A_OUTPUT_PORT_1           0x03
#define TCA6416A_POLARITY_INV_PORT_0     0x04
#define TCA6416A_POLARITY_INV_PORT_1     0x05
#define TCA6416A_CONFIG_0                0x06
#define TCA6416A_CONFIG_1                0x07

#define TCA6416A_MAX_OFFSET TCA6416A_CONFIG_1


/* the tca6416a internal control structure */      
typedef struct tca6416a_ctx_t {
    void                    *devo;
    gen_i2c_dev_brdspec_t   brdspec;
} tca6416a_ctx_t;

static ccl_err_t _ret;                               

/* tca6416a register read */
static ccl_err_t _tca6416a_reg_read(void *p_priv, b_u32 offset, 
                                    b_u32 offset_sz, b_u32 idx, 
                                    b_u8 *p_value, b_u32 size)
{
    tca6416a_ctx_t   *p_tca6416a;
    i2c_route_t      i2c_route;

    if ( !p_priv || offset > TCA6416A_MAX_OFFSET || 
         !p_value || idx ) {
        PDEBUG("Error! Invalid input parameters\n");
        TCA6416A_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    PDEBUGG("123$=> offset=0x%03x, size=%d\n", offset, size);

    p_tca6416a = (tca6416a_ctx_t *)p_priv;     
    memcpy(&i2c_route, &p_tca6416a->brdspec.i2c.bus_id, sizeof(i2c_route_t));

    _ret = devi2ccore_read_buff(&i2c_route, p_tca6416a->brdspec.extra_mux_pin, 
                                offset, p_tca6416a->brdspec.addr_width, 
                                p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to write over spi bus\n");
        TCA6416A_ERROR_RETURN(_ret);
    }

    return _ret;
}

/* tca6416a register write */
static ccl_err_t _tca6416a_reg_write(void *p_priv, b_u32 offset, 
                                     b_u32 offset_sz, b_u32 idx, 
                                     b_u8 *p_value, b_u32 size)
{
    tca6416a_ctx_t   *p_tca6416a;
    i2c_route_t      i2c_route;

    if ( !p_priv || offset > TCA6416A_MAX_OFFSET || 
         !p_value || idx ) {
        PDEBUG("Error! Invalid input parameters\n");
        TCA6416A_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    PDEBUG("123$=> offset=0x%03x, value=0x%02x\n", offset, *p_value);

    p_tca6416a = (tca6416a_ctx_t *)p_priv;     
    memcpy(&i2c_route, &p_tca6416a->brdspec.i2c.bus_id, sizeof(i2c_route_t));

    _ret = devi2ccore_write_buff(&i2c_route, p_tca6416a->brdspec.extra_mux_pin, 
                                 offset, p_tca6416a->brdspec.addr_width, 
                                 p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to write over spi bus\n");
        TCA6416A_ERROR_RETURN(_ret);
    }

    return _ret;
}

static ccl_err_t _tca6416a_init(void *p_priv)
{
    tca6416a_ctx_t   *p_tca6416a;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        TCA6416A_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_tca6416a = (tca6416a_ctx_t *)p_priv;

    return CCL_OK;
}

static ccl_err_t _tca6416a_configure_test(void *p_priv)
{
    tca6416a_ctx_t   *p_tca6416a;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        TCA6416A_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_tca6416a = (tca6416a_ctx_t *)p_priv;

    return CCL_OK;
}

static ccl_err_t _tca6416a_run_test(void *p_priv, 
                                    __attribute__((unused)) b_u8 *buf, 
                                    __attribute__((unused)) b_u32 size)
{
    tca6416a_ctx_t   *p_tca6416a;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        TCA6416A_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_tca6416a = (tca6416a_ctx_t *)p_priv;

    return CCL_OK;
}

ccl_err_t tca6416a_attr_read(void *p_priv, b_u32 attrid, 
                             b_u32 offset, b_u32 offset_sz, 
                             b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || offset > TCA6416A_MAX_OFFSET || 
         !p_value || idx  ) {
        PDEBUG("Error! Invalid input parameters\n");
        TCA6416A_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch (attrid) {
    case eTCA6416A_INPUT_PORT_0:
    case eTCA6416A_INPUT_PORT_1:
    case eTCA6416A_OUTPUT_PORT_0:
    case eTCA6416A_OUTPUT_PORT_1:
    case eTCA6416A_POLARITY_INV_PORT_0:
    case eTCA6416A_POLARITY_INV_PORT_1:
    case eTCA6416A_CONFIG_0:
    case eTCA6416A_CONFIG_1:
        _ret = _tca6416a_reg_read(p_priv, offset, offset_sz, idx, p_value, size);
        break;
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK) {
        PDEBUG("Failure in attribute id: %d\n", attrid);
    }

    return _ret;               
}

ccl_err_t tca6416a_attr_write(void *p_priv, b_u32 attrid, 
                              b_u32 offset, b_u32 offset_sz, 
                              b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || offset > TCA6416A_MAX_OFFSET || 
         !p_value || idx ) {
        PDEBUG("Error! Invalid input parameters\n");
        TCA6416A_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch (attrid) {
    case eTCA6416A_INPUT_PORT_0:
    case eTCA6416A_INPUT_PORT_1:
    case eTCA6416A_OUTPUT_PORT_0:
    case eTCA6416A_OUTPUT_PORT_1:
    case eTCA6416A_POLARITY_INV_PORT_0:
    case eTCA6416A_POLARITY_INV_PORT_1:
    case eTCA6416A_CONFIG_0:
    case eTCA6416A_CONFIG_1:
        _ret = _tca6416a_reg_write(p_priv, offset, offset_sz, idx, p_value, size);
        break;
    case eTCA6416A_INIT:
        _ret = _tca6416a_init(p_priv);
        break;
    case eTCA6416A_CONFIGURE_TEST:
        _ret = _tca6416a_configure_test(p_priv);
        break;
    case eTCA6416A_RUN_TEST:
        _ret = _tca6416a_run_test(p_priv, p_value, size);
        break;
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK) {
        PDEBUG("Failure in attribute id: %d\n", attrid);
    }

    return _ret;               
}

/* tca6416a attributes definition */
static device_attr_t _attrs[] = {
    {
        .name = "i2c_bus", 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset = offsetof(struct tca6416a_ctx_t, brdspec.i2c.bus_id),
        .format = "%d"
    },
    {
        .name = "i2c_addr", 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset = offsetof(struct tca6416a_ctx_t, brdspec.i2c.dev_i2ca),
        .format = "0x%03x"
    },
    {
        .name = "name", 
        .type = eDEVMAN_ATTR_STRING, .maxnum = 1, 
        .offset = offsetof(struct tca6416a_ctx_t, brdspec.name),
        .size = DEVMAN_DEVNAME_LEN,
        .format = "%s"
    },
    {
        .name = "iport7_0", .id = eTCA6416A_INPUT_PORT_0, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TCA6416A_INPUT_PORT_0
    },
    {
        .name = "iport15_8", .id = eTCA6416A_INPUT_PORT_1, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TCA6416A_INPUT_PORT_1
    },
    {
        .name = "oport7_0", .id = eTCA6416A_OUTPUT_PORT_0, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TCA6416A_OUTPUT_PORT_0
    },
    {
        .name = "oport15_8", .id = eTCA6416A_OUTPUT_PORT_1, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TCA6416A_OUTPUT_PORT_1
    },
    {
        .name = "pol_inv_port7_0", .id = eTCA6416A_POLARITY_INV_PORT_0, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TCA6416A_POLARITY_INV_PORT_0
    },
    {
        .name = "pol_inv_port15_8", .id = eTCA6416A_POLARITY_INV_PORT_1, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TCA6416A_POLARITY_INV_PORT_1
    },
    {
        .name = "config7_0", .id = eTCA6416A_CONFIG_0, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TCA6416A_CONFIG_0
    },
    {
        .name = "config15_8", .id = eTCA6416A_CONFIG_1, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = TCA6416A_CONFIG_1
    },
    {
        .name = "init", .id = eTCA6416A_INIT, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "config_test", .id = eTCA6416A_CONFIGURE_TEST, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "run_test", .id = eTCA6416A_RUN_TEST, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, 
        .size = sizeof(board_dev_stats_t), 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
};

/* Device instance hw-specific initializations 
 */
static ccl_err_t _ftca6416a_add(void *devo, void *brdspec)
{
    tca6416a_ctx_t            *p_tca6416a;
    gen_i2c_dev_brdspec_t     *p_brdspec;

    if ( !devo || !brdspec ) {
        PDEBUG("Error! Invalid input parameters\n");
        TCA6416A_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_tca6416a = devman_device_private(devo);
    if ( !p_tca6416a ) {
        PDEBUG("NULL context area\n");
        TCA6416A_ERROR_RETURN(CCL_FAIL);
    }
    p_brdspec = (gen_i2c_dev_brdspec_t *)brdspec;

    PDEBUG("123$=> p_brdspec->i2c.bus_id=%d, dev_i2ca=0x%x\n", 
           p_brdspec->i2c.bus_id, p_brdspec->i2c.dev_i2ca);

    p_tca6416a->devo = devo;    
    memcpy(&p_tca6416a->brdspec, p_brdspec, sizeof(gen_i2c_dev_brdspec_t));

    return CCL_OK;
}


/* Device instance hw-specific deinitializations 
 */
static ccl_err_t _ftca6416a_cut(void *devo)
{
    if ( !devo ) {
        PDEBUG("Error! Invalid input parameters\n");
        TCA6416A_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    return CCL_OK;
}

/* device type description structure */
static device_driver_t _tca6416a_driver = {
    .name = "i2cio",
    .f_add = _ftca6416a_add,
    .f_cut = _ftca6416a_cut,
    .attrs_num = sizeof(_attrs)/sizeof(_attrs[0]),
                 .attrs = _attrs,
    .priv_size = sizeof(tca6416a_ctx_t)
}; 

/* Initialize TCA6416A device type
 */
ccl_err_t devtca6416a_init(void)
{
    /* register device type */
    _ret = devman_driver_register(&_tca6416a_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to register driver\n");
        TCA6416A_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

/* Deinitialize TCA6416A device type
 */
ccl_err_t devtca6416a_fini(void)
{
    /* unregister device type */
    _ret = devman_driver_unregister(&_tca6416a_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to unregister driver\n");
        TCA6416A_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}


