/********************************************************************************/
/**
 * @file i2cbus.c
 * @brief I2CBUS device manager driver
 * @copyright 2019 CELARE. All rights reserved.
 */
/********************************************************************************/
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <semaphore.h>
#include <linux/limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>

#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "i2cbus.h"


//#define I2CBUS_DEBUG
/* printing/error-returning macros */
#ifdef I2CBUS_DEBUG
    #define PDEBUG(fmt, args...) \
         ccl_syslog_err("I2CBUS: %s(): " fmt, __FUNCTION__, ## args)
#else
    #define PDEBUG(fmt, args...)
#endif
#define I2CBUS_ERROR_RETURN(err) do { \
   ccl_syslog_err("Error! %s[%d], error=%i\n", __FUNCTION__, __LINE__, (err)); \
   return err; \
} while (0)

/* devfs filename sise */
#define I2C_DEVNAME_SIZE 16

/* To determine what functionality is present */
#define I2C_FUNC_I2C                        0x00000001
#define I2C_FUNC_10BIT_ADDR                 0x00000002
#define I2C_FUNC_PROTOCOL_MANGLING          0x00000004 /* I2C_M_{REV_DIR_ADDR,NOSTART,..} */
#define I2C_FUNC_SMBUS_PEC                  0x00000008
#define I2C_FUNC_SMBUS_BLOCK_PROC_CALL      0x00008000 /* SMBus 2.0 */
#define I2C_FUNC_SMBUS_QUICK                0x00010000 
#define I2C_FUNC_SMBUS_READ_BYTE            0x00020000 
#define I2C_FUNC_SMBUS_WRITE_BYTE           0x00040000 
#define I2C_FUNC_SMBUS_READ_BYTE_DATA       0x00080000 
#define I2C_FUNC_SMBUS_WRITE_BYTE_DATA      0x00100000 
#define I2C_FUNC_SMBUS_READ_WORD_DATA       0x00200000 
#define I2C_FUNC_SMBUS_WRITE_WORD_DATA      0x00400000 
#define I2C_FUNC_SMBUS_PROC_CALL            0x00800000 
#define I2C_FUNC_SMBUS_READ_BLOCK_DATA      0x01000000 
#define I2C_FUNC_SMBUS_WRITE_BLOCK_DATA     0x02000000 
#define I2C_FUNC_SMBUS_READ_I2C_BLOCK       0x04000000 /* I2C-like block xfer  */
#define I2C_FUNC_SMBUS_WRITE_I2C_BLOCK      0x08000000 /* w/ 1-byte reg. addr. */
#define I2C_FUNC_SMBUS_READ_I2C_BLOCK_2     0x10000000 /* I2C-like block xfer  */
#define I2C_FUNC_SMBUS_WRITE_I2C_BLOCK_2    0x20000000 /* w/ 2-byte reg. addr. */

#define I2C_FUNC_SMBUS_BYTE         (I2C_FUNC_SMBUS_READ_BYTE | I2C_FUNC_SMBUS_WRITE_BYTE)
#define I2C_FUNC_SMBUS_BYTE_DATA    (I2C_FUNC_SMBUS_READ_BYTE_DATA | I2C_FUNC_SMBUS_WRITE_BYTE_DATA)
#define I2C_FUNC_SMBUS_WORD_DATA    (I2C_FUNC_SMBUS_READ_WORD_DATA | I2C_FUNC_SMBUS_WRITE_WORD_DATA)
#define I2C_FUNC_SMBUS_BLOCK_DATA   (I2C_FUNC_SMBUS_READ_BLOCK_DATA | I2C_FUNC_SMBUS_WRITE_BLOCK_DATA)
#define I2C_FUNC_SMBUS_I2C_BLOCK    (I2C_FUNC_SMBUS_READ_I2C_BLOCK | I2C_FUNC_SMBUS_WRITE_I2C_BLOCK)
#define I2C_FUNC_SMBUS_I2C_BLOCK_2  (I2C_FUNC_SMBUS_READ_I2C_BLOCK_2 | I2C_FUNC_SMBUS_WRITE_I2C_BLOCK_2)
/* old name, for compatibility */
#define I2C_FUNC_SMBUS_HWPEC_CALC	I2C_FUNC_SMBUS_PEC

/* smbus_access read or write markers */
#define I2C_SMBUS_READ      1
#define I2C_SMBUS_WRITE     0

/* SMBus transaction types (size parameter in the above functions) 
   Note: these no longer correspond to the (arbitrary) PIIX4 internal codes! */
#define I2C_SMBUS_QUICK             0
#define I2C_SMBUS_BYTE              1
#define I2C_SMBUS_BYTE_DATA         2 
#define I2C_SMBUS_WORD_DATA         3
#define I2C_SMBUS_PROC_CALL         4
#define I2C_SMBUS_BLOCK_DATA        5
#define I2C_SMBUS_I2C_BLOCK_BROKEN  6
#define I2C_SMBUS_BLOCK_PROC_CALL   7   /* SMBus 2.0 */
#define I2C_SMBUS_I2C_BLOCK_DATA    8


/* ----- commands for the ioctl like i2c_command call:
 * note that additional calls are defined in the algorithm and hw 
 * dependent layers - these can be listed here, or see the 
 *corresponding header files */
#define I2C_RETRIES         0x0701  /* number of times a device address */
#define I2C_TIMEOUT         0x0702  /* set timeout - call with int	*/
#define I2C_SLAVE           0x0703  /* change slave address	*/
#define I2C_SLAVE_FORCE     0x0706
#define I2C_TENBIT          0x0704
#define I2C_FUNCS           0x0705  /* get the adapter functionality */
#define I2C_RDWR            0x0707  /* combined R/W transfer (one stop only) */
#define I2C_PEC             0x0708  /* != 0 for SMBus PEC */
#define I2C_SMBUS           0x0720  /* SMBus-level access */

/* data field for i2c SMBUS transaction
 */
#define I2C_SMBUS_BLOCK_MAX	32	
typedef union tagu_i2c_smbus_data {
    b_u8    byte;
    b_u16   word;
    b_u8    block[I2C_SMBUS_BLOCK_MAX + 2]; /* block[0] is used for length */
                                            /* and one more for PEC */
} i2c_smbus_data_u;

/* This is the structure as used in the I2C_SMBUS ioctl call */
typedef struct tagt_i2c_smbus_ioctl_parm {
    b_u8                read_write;
    b_u8                command;
    b_u32               size;
    i2c_smbus_data_u   *data;
} i2c_smbus_ioctl_parm_t;

#define I2C_RW_BLOCK_MAX	(256 + sizeof(b_u32))	
/* i2c message, used for i2c RDWR transaction
 */
typedef struct tagt_i2c_msg {
    b_u16           addr;   /* slave address */
    b_u16           flags;      
#define I2C_M_TEN	0x10	/* we have a ten bit chip address */
#define I2C_M_RD	0x01
#define I2C_M_NOSTART	0x4000
#define I2C_M_REV_DIR_ADDR	0x2000
#define I2C_M_IGNORE_NAK	0x1000
#define I2C_M_NO_RD_ACK		0x0800
    b_u16           len;    /* msg length */
    b_u8           *buf;    /* pointer to msg data */
} i2c_msg_t;

/* i2c RDWR ioctl parameters */
typedef struct tagt_i2c_rdwr_ioctl_parm {
    i2c_msg_t   *msgs;  /* pointers to i2c_msgs */
    int          nmsgs; /* number of i2c_msgs */
} i2c_rdwr_ioctl_parm_t;

/* i2cbus context structure */
typedef struct tagt_i2cbus_ctx {
    void               *devo;
    i2cbus_brdspec_t    brdspec;
    char                cdev_name[I2C_DEVNAME_SIZE];
    pthread_mutex_t     mutex;
    /* current channel handlers */
    int                 fd;
    int                 mux_i2ca;
    b_u8                mux_mask;
    int                 dev_i2ca;  
} i2cbus_ctx_t;

struct timespec   ts;
static int        _rc;

/* i2cbuses references */
static i2cbus_ctx_t *_p_i2cs[eDEVBRD_I2C_BUSES_NUM];

static ccl_err_t _fi2cbus_release(i2cbus_ctx_t *p_i2cbus);
static ccl_err_t _fi2cbus_raw_read_data(i2c_route_t *p_i2c_point, char *preamble, 
                                        char pream_len, char *p_data, b_u16 data_len);
static ccl_err_t _fi2cbus_raw_write_data(i2c_route_t *p_i2c_point, 
                                         char *p_data, b_u16 data_len);

/* Check if I2C route is valid
 */
static b_bool _fi2cbus_route_is_valid(i2c_route_t *p_route)
{

    int i, j = 0;

    if ( !p_route || !p_route->dev_i2ca ||
         p_route->bus_id >= eDEVBRD_I2C_BUSES_NUM || 
         !_p_i2cs[p_route->bus_id] ) {
        PDEBUG("Error! Invalid input parameters\n");
        return CCL_FALSE;
    }
    /* check muxes */
    while ( p_route->mux_i2ca[j] ) {
        for ( i = 0; i < _p_i2cs[p_route->bus_id]->brdspec.slaves_num; i++ ) {
            if ( p_route->mux_i2ca[j] == 
                 _p_i2cs[p_route->bus_id]->brdspec.slave_i2ca[i] )
                break;
        }
        if ( i >= _p_i2cs[p_route->bus_id]->brdspec.slaves_num )
            goto _fi2cbus_route_is_invalid;
        j++;
    }
    /* check i2ca */
    for ( i = 0; i < _p_i2cs[p_route->bus_id]->brdspec.slaves_num; i++ ) {
        if ( p_route->dev_i2ca == 
             _p_i2cs[p_route->bus_id]->brdspec.slave_i2ca[i] )
            return CCL_TRUE;
    }
    _fi2cbus_route_is_invalid:
    PDEBUG("Error! I2C route is invalid: bus=%d,i2ca=0x%x,mux[0]=0x%x,mux[1]=0x%x\n", 
           p_route->bus_id, p_route->dev_i2ca, p_route->mux_i2ca[0], p_route->mux_i2ca[1]);    
    return CCL_FALSE;
}

/* i2cbus ioctl user-side handler
 */
static int _fi2cbus_rdwr_ioctl(int fd, b_u16 i2ca, 
                               b_u16 flags, char *p_data, 
                               b_u16 data_len)
{
    i2c_rdwr_ioctl_parm_t   ioctl_args;
    i2c_msg_t               write_msg;

    if (!p_data) {
        PDEBUG("Error! Invalid input parameters\n");
        return -1;
    }
    /* clear data structures */
    memset(&ioctl_args, 0, sizeof(i2c_rdwr_ioctl_parm_t));
    memset(&write_msg, 0, sizeof(i2c_msg_t));
    /* set write message parameters */
    write_msg.addr = i2ca;
    write_msg.flags = flags;
    write_msg.len = data_len;
    write_msg.buf = (b_u8 *)p_data;
    /* set ioctl parameters */
    ioctl_args.msgs = &write_msg;
    ioctl_args.nmsgs = 1;

    PDEBUGG("123$=> flags=0x%x,p_data[0-3]=0x%02x,%02x,%02x,%02x,data_len=%d\n",
            flags,p_data[0],p_data[1],p_data[2],p_data[3],data_len);

    /* operation start */
    return ioctl(fd, I2C_RDWR, &ioctl_args);
}


/* Catch the specified route on the bus
 */
static ccl_err_t _fi2cbus_catch(i2cbus_ctx_t *p_i2cbus, 
                                i2c_route_t *p_point, 
                                __attribute__((unused)) b_bool is_blocking_call)
{
    int mux_depth = 0;
    /* check input parameters */
    if ( !p_i2cbus || !p_point ) {
        PDEBUG("Error! Invalid input parameters\n");
        I2CBUS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    _rc = pthread_mutex_lock(&p_i2cbus->mutex);
    if (_rc) {
        PDEBUG("pthread_mutex_lock error\n");
        I2CBUS_ERROR_RETURN(CCL_FAIL);
    }

    /* try to open the devfs file */
    p_i2cbus->fd = open(p_i2cbus->cdev_name, O_RDWR);
    if ( p_i2cbus->fd < 0 ) {
        PDEBUG("Error! Failed to open bus dev file\n");
        _rc = pthread_mutex_unlock(&p_i2cbus->mutex);
        if (_rc) {
            PDEBUG("pthread_mutex_unlock error\n");
        }
        I2CBUS_ERROR_RETURN(CCL_FAIL);
    }
    /* if we're working through mux, setup the appropriate mux channel */
    while ( p_point->mux_i2ca[mux_depth] > 0 ) {
        /* force the mux i2c address */
        p_i2cbus->mux_i2ca = p_point->mux_i2ca[mux_depth];
        if ( ioctl(p_i2cbus->fd, I2C_SLAVE_FORCE, p_i2cbus->mux_i2ca) < 0 ) {
            PDEBUG("Error! Failed to set proper bus mux address\n");
            _fi2cbus_release(p_i2cbus);
            I2CBUS_ERROR_RETURN(CCL_FAIL);
        }
        /* switch on necessary channel at the mux */
        p_i2cbus->mux_mask = (1 << (p_point->mux_pin[mux_depth] % 8));
        PDEBUG("123$=> pass mux: p_point->dev_i2ca=0x%02x,"
               "p_i2cbus->mux_i2ca=0x%02x,p_i2cbus->mux_mask=0x%02x\n",
               p_point->dev_i2ca,p_i2cbus->mux_i2ca,p_i2cbus->mux_mask);
        if ( _fi2cbus_rdwr_ioctl(p_i2cbus->fd, p_i2cbus->mux_i2ca, 
                                 0, (char *)&p_i2cbus->mux_mask, sizeof(b_u8)) < 0 ) {
            PDEBUG("Error! Failed to set the mux with proper device mask\n");
            _fi2cbus_release(p_i2cbus);
            I2CBUS_ERROR_RETURN(CCL_FAIL);
        }
        mux_depth++;
    }
    p_i2cbus->dev_i2ca = p_point->dev_i2ca;
    /* force the target device i2c address */
    if ( ioctl(p_i2cbus->fd, I2C_SLAVE_FORCE, p_i2cbus->dev_i2ca) < 0) {
        PDEBUG("Error! Failed to set the bus with proper device address\n");
        _fi2cbus_release(p_i2cbus);
        I2CBUS_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

/* Release the bus
 */
static ccl_err_t _fi2cbus_release(i2cbus_ctx_t *p_i2cbus)
{
    /* check input parameters */
    if ( !p_i2cbus ) {
        PDEBUG("Error! Invalid input parameters\n");
        I2CBUS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* if we're working through mux, release the mux channel */
    if ( p_i2cbus->fd  > 0 && p_i2cbus->mux_i2ca > 0 ) {
        /* force the mux i2c address */
        if ( ioctl(p_i2cbus->fd, I2C_SLAVE_FORCE, p_i2cbus->mux_i2ca) < 0 ) {
            PDEBUG("Error! Failed to force proper bus mux address\n");
            goto error;
        }
        /* switch off the mux channels */
        p_i2cbus->mux_mask = 0;
        if ( _fi2cbus_rdwr_ioctl(p_i2cbus->fd, p_i2cbus->mux_i2ca, 0, 
                                 (char *)&p_i2cbus->mux_mask, sizeof(b_u8))  < 0 ) {
            PDEBUG("Error! Failed to clear the mux\n");
            goto error;
        }
    }
    /* close smbus device file */
    if ( p_i2cbus->fd >= 0 ) {
        if ( close(p_i2cbus->fd) < 0) {
            PDEBUG("Error! Failed to close file descriptor\n");
        }
        p_i2cbus->fd = -1;
    }
    _rc = pthread_mutex_unlock(&p_i2cbus->mutex);
    if (_rc) {
        PDEBUG("pthread_mutex_unlock error\n");
    }

    p_i2cbus->mux_i2ca = 0;
    p_i2cbus->dev_i2ca = 0;

    return CCL_OK;

error:
    _rc = pthread_mutex_unlock(&p_i2cbus->mutex);
    if (_rc) {
        PDEBUG("pthread_mutex_unlock error\n");
    }
    I2CBUS_ERROR_RETURN(CCL_FAIL);
}

/*  Raw write on the i2cbus
 */
static ccl_err_t _fi2cbus_raw_write_data(i2c_route_t *p_i2c_point, 
                                         char *p_data, b_u16 data_len)
{
    i2cbus_ctx_t        *p_i2cbus;
    b_bool               is_blocking_call = 1;

    /* check input parameters */
    if ( !_fi2cbus_route_is_valid(p_i2c_point)|| !p_data 
         || !data_len || p_i2c_point->bus_id >= eDEVBRD_I2C_BUSES_NUM) {
        PDEBUG("Error! Invalid input parameters: p_i2c_point=0x%x, p_data=0x%x, "
               "data_len=%d, bus_id=%d, dev_i2ca=0x%x, mux_i2ca=0x%x\n", 
               p_i2c_point, p_data, data_len, p_i2c_point->bus_id, 
               p_i2c_point->dev_i2ca, p_i2c_point->mux_i2ca[0]);
        I2CBUS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* get the current smbus context */
    p_i2cbus = _p_i2cs[p_i2c_point->bus_id];

    /* capture the bus */
    _rc = _fi2cbus_catch(p_i2cbus, p_i2c_point, is_blocking_call);
    if ( _rc ) {
        PDEBUG("Error! Can't catch the bus\n");
        I2CBUS_ERROR_RETURN(_rc);
    }

    /* start write transaction */
    _rc = _fi2cbus_rdwr_ioctl(p_i2cbus->fd, p_i2c_point->dev_i2ca, 0, p_data, data_len);
    if ( _rc < 0 ) {
        PDEBUG("Warning! Can't write over i2c bus, ioctl is failed\n");
        PDEBUG("p_i2c_point=0x%x, p_data=0x%x, data_len=%d, "
               "bus_id=%d, dev_i2ca=0x%x, mux_i2ca=0x%x\n", 
               p_i2c_point, p_data, data_len, 
               p_i2c_point->bus_id, p_i2c_point->dev_i2ca, 
               p_i2c_point->mux_i2ca[0]);
    }

    /* release the bus */
    _rc = _fi2cbus_release(p_i2cbus);
    if ( _rc ) {
        PDEBUG("Error! Can't release the bus\n");
        I2CBUS_ERROR_RETURN(_rc);
    }

    return CCL_OK; 
}

/* Write byte over i2cbus
 */
ccl_err_t devi2cbus_write_byte(i2c_route_t *p_i2c_point, b_u32 offset, 
                               b_u32 offset_sz, b_u8 value)
{
    char    write_buff[8];

    /* check input parameters */
    if ( !p_i2c_point || !offset_sz || offset_sz > sizeof(b_u32) ) {
        PDEBUG("Error! Invalid input parameters\n");
        I2CBUS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* prepare write buffer's offset prefix */
    memcpy(&write_buff[0], (char *)(&offset)+sizeof(int)-offset_sz, offset_sz);
    /* setup the target value */ 
    write_buff[offset_sz] = value; 

    PDEBUG("123$=> bus=%d,i2ca=0x%0x,offset=%d,offset_sz=%d,value=0x%x\n", 
           p_i2c_point->bus_id, p_i2c_point->dev_i2ca, offset, offset_sz, value);

    /* write buffer down */
    _rc = _fi2cbus_raw_write_data(p_i2c_point, write_buff, offset_sz+sizeof(b_u8));
    if ( _rc ) {
        PDEBUG("Error! Can't write the byte\n");
        I2CBUS_ERROR_RETURN(_rc);
    }

    return CCL_OK; 
}

/* Write half over i2cbus
 */
ccl_err_t devi2cbus_write_half(i2c_route_t *p_i2c_point, b_u32 offset, 
                               b_u32 offset_sz, b_u16 value)
{
    char    write_buff[8];

    /* check input parameters */
    if ( !p_i2c_point || !offset_sz || offset_sz > sizeof(b_u32) ) {
        PDEBUG("Error! Invalid input parameters\n");
        I2CBUS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* prepare write buffer's offset prefix */
    memcpy(&write_buff[0], (char *)(&offset)+sizeof(int)-offset_sz, offset_sz);
    /* setup the target value */ 
    memcpy(&write_buff[offset_sz], &value, sizeof(b_u16)); 

    PDEBUG("123$=> bus=%d,i2ca=0x%0x,offset=%d,offset_sz=%d,value=0x%x\n", 
           p_i2c_point->bus_id, p_i2c_point->dev_i2ca, offset, offset_sz, value);

    /* write buffer down */
    _rc = _fi2cbus_raw_write_data(p_i2c_point, write_buff, offset_sz+sizeof(b_u16));
    if ( _rc ) {
        PDEBUG("Error! Can't write half\n");
        I2CBUS_ERROR_RETURN(_rc);
    }

    return CCL_OK; 
}

/*  Write word over i2cbus
 */
ccl_err_t devi2cbus_write_word(i2c_route_t *p_i2c_point, b_u32 offset, 
                               b_u32 offset_sz, b_u32 value)
{
    char    write_buff[8];

    /* check input parameters */
    if ( !p_i2c_point || !offset_sz || offset_sz > sizeof(b_u32) ) {
        PDEBUG("Error! Invalid input parameters\n");
        I2CBUS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* prepare write buffer's offset prefix */
    memcpy(&write_buff[0], (char *)(&offset)+sizeof(int)-offset_sz, offset_sz);
    /* setup the target value */ 
    memcpy(&write_buff[offset_sz], &value, sizeof(b_u32)); 

    PDEBUG("123$=> bus=%d,i2ca=0x%0x,offset=%d,offset_sz=%d,value=0x%x\n", 
           p_i2c_point->bus_id, p_i2c_point->dev_i2ca, offset, offset_sz, value);

    /* write buffer down */
    _rc = _fi2cbus_raw_write_data(p_i2c_point, write_buff, offset_sz+sizeof(b_u32));
    if ( _rc ) {
        PDEBUG("Error! Can't write word\n");
        I2CBUS_ERROR_RETURN(_rc);
    }
    return CCL_OK; 
}

/* Write buffer over i2cbus
 */
ccl_err_t devi2cbus_write_buff(i2c_route_t *p_i2c_point, b_u32 offset, 
                               b_u32 offset_sz, b_u8 *p_value, b_u32 size)
{
    char    write_buff[I2C_RW_BLOCK_MAX];

    /* check input parameters */
    if ( !p_i2c_point || !offset_sz || offset_sz > sizeof(b_u32) || !p_value || 
         !size || (size + offset_sz) > I2C_RW_BLOCK_MAX ) {
        PDEBUG("Error! Invalid input parameters\n");
        I2CBUS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* prepare write buffer's offset prefix */
    memcpy(&write_buff[0], (char *)(&offset)+sizeof(int)-offset_sz, offset_sz);
    /* setup the target value */ 
    memcpy(&write_buff[offset_sz], p_value, size); 

    PDEBUG("123$=> bus=%d,i2ca=0x%0x,offset=%d,offset_sz=%d,size=%d\n", 
           p_i2c_point->bus_id, p_i2c_point->dev_i2ca, offset, offset_sz, size);

    /* write buffer down */
    _rc = _fi2cbus_raw_write_data(p_i2c_point, write_buff, offset_sz+size);
    if ( _rc ) {
        PDEBUG("Error! Can't write word\n");
        I2CBUS_ERROR_RETURN(_rc);
    }
    return CCL_OK; 
}

/* Raw read over i2cbus
 */
static ccl_err_t _fi2cbus_raw_read_data(i2c_route_t *p_i2c_point, char *preamble, 
                                        char pream_len, char *p_data, b_u16 data_len)
{
    i2cbus_ctx_t     *p_i2cbus;
    b_bool            is_blocking_call = 1;
    PDEBUG("123$=> data_len=%d\n", data_len);

    /* check input parameters */
    if ( !_fi2cbus_route_is_valid(p_i2c_point)|| !p_data || 
         !data_len || p_i2c_point->bus_id >= eDEVBRD_I2C_BUSES_NUM) {
        PDEBUG("Error! Invalid input parameters: p_i2c_point=0x%x, p_data=0x%x, "
               "data_len=%d, bus_id=%d, dev_i2ca=0x%x, mux_i2ca=0x%x\n", 
               p_i2c_point, p_data, data_len, 
               p_i2c_point->bus_id, p_i2c_point->dev_i2ca, 
               p_i2c_point->mux_i2ca[0]);
        I2CBUS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* get the current smbus context */
    p_i2cbus = _p_i2cs[p_i2c_point->bus_id];

    /* capture the bus */
    _rc = _fi2cbus_catch(p_i2cbus, p_i2c_point, is_blocking_call);
    if ( _rc ) {
        PDEBUG("Error! Can't catch the bus\n");
        I2CBUS_ERROR_RETURN(_rc);
    }

    /* start write subtransaction, usually send internal offset on device */
    _rc = _fi2cbus_rdwr_ioctl(p_i2cbus->fd, p_i2c_point->dev_i2ca, 
                              0, preamble, pream_len);
    if ( _rc < 0 ) {
        PDEBUG("Warning! Can't write preamble over i2c bus, ioctl is failed\n");
    }
    /* start read subtransaction */
    _rc = (_rc < 0)? _rc: _fi2cbus_rdwr_ioctl(p_i2cbus->fd, p_i2c_point->dev_i2ca, 
                                              I2C_M_RD, p_data, data_len);
    if ( _rc < 0 ) {
        PDEBUG("Warning! Can't read data over i2c bus, ioctl is failed\n");
    }

    /* release the bus */
    _rc = _fi2cbus_release(p_i2cbus);
    if ( _rc ) {
        PDEBUG("Error! Can't release the bus\n");
        I2CBUS_ERROR_RETURN(_rc);
    }

    return CCL_OK; 
}

/* Read byte over i2cbus
 */
ccl_err_t devi2cbus_read_byte(i2c_route_t *p_i2c_point, b_u32 offset, 
                              b_u32 offset_sz, b_u8 *p_value)
{
    char    write_buff[sizeof(b_u32)];

    /* check input parameters */
    if ( !p_i2c_point || !offset_sz || offset_sz > sizeof(b_u32) || !p_value ) {
        PDEBUG("Error! Invalid input parameters\n");
        I2CBUS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* prepare internal dev offset as preamble write buffer */
    memcpy(&write_buff[0], (char *)(&offset)+sizeof(int)-offset_sz, offset_sz);

    PDEBUG("123$=> bus=%d,i2ca=0x%0x,offset=%d,offset_sz=%d\n", 
           p_i2c_point->bus_id, p_i2c_point->dev_i2ca, offset, offset_sz);

    /* write/read transaction */
    _rc = _fi2cbus_raw_read_data(p_i2c_point, write_buff, 
                                  offset_sz, (char *)p_value, sizeof(b_u8));
    if ( _rc ) {
        PDEBUG("Error! Can't read byte\n");
        I2CBUS_ERROR_RETURN(_rc);
    }
    return CCL_OK; 
}

/* Read half over i2cbus
 */
ccl_err_t devi2cbus_read_half(i2c_route_t *p_i2c_point, b_u32 offset, 
                              b_u32 offset_sz, b_u16 *p_value)
{
    char    write_buff[sizeof(b_u32)];

    /* check input parameters */
    if ( !p_i2c_point || !offset_sz || offset_sz > sizeof(b_u32) || !p_value ) {
        PDEBUG("Error! Invalid input parameters\n");
        I2CBUS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* prepare internal dev offset as preamble write buffer */
    memcpy(&write_buff[0], (char *)(&offset)+sizeof(int)-offset_sz, offset_sz);

    PDEBUG("123$=> bus=%d,i2ca=0x%0x,offset=%d,offset_sz=%d\n", 
           p_i2c_point->bus_id, p_i2c_point->dev_i2ca, offset, offset_sz);

    /* write/read transaction */
    _rc = _fi2cbus_raw_read_data(p_i2c_point, write_buff, offset_sz, (char *)p_value, sizeof(b_u16));
    if ( _rc ) {
        PDEBUG("Error! Can't read byte\n");
        I2CBUS_ERROR_RETURN(_rc);
    }
    return CCL_OK; 
}

/* Read word over i2cbus
 */
ccl_err_t devi2cbus_read_word(i2c_route_t *p_i2c_point, b_u32 offset, 
                              b_u32 offset_sz, b_u32 *p_value)
{
    char    write_buff[sizeof(b_u32)];

    /* check input parameters */
    if ( !p_i2c_point || !offset_sz || offset_sz > sizeof(b_u32) || !p_value ) {
        PDEBUG("Error! Invalid input parameters\n");
        I2CBUS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* prepare internal dev offset as preamble write buffer */
    memcpy(&write_buff[0], (char *)(&offset)+sizeof(int)-offset_sz, offset_sz);

    PDEBUG("123$=> bus=%d,i2ca=0x%0x,offset=%d,offset_sz=%d\n", 
           p_i2c_point->bus_id, p_i2c_point->dev_i2ca, offset, offset_sz);

    /* write/read transaction */
    _rc = _fi2cbus_raw_read_data(p_i2c_point, write_buff, 
                                  offset_sz, (char *)p_value, sizeof(b_u32));
    if ( _rc ) {
        PDEBUG("Error! Can't read byte\n");
        I2CBUS_ERROR_RETURN(_rc);
    }
    return CCL_OK; 
}

/* Read buffer over i2cbus
 */
ccl_err_t devi2cbus_read_buff(i2c_route_t *p_i2c_point, b_u32 offset, 
                              b_u32 offset_sz, b_u8 *p_value, b_u32 size)
{
    char    write_buff[sizeof(b_u32)];

    /* check input parameters */
    if ( !p_i2c_point || !offset_sz || offset_sz > sizeof(b_u32) || 
         !p_value || !size || size > (int)I2C_RW_BLOCK_MAX ) {
        PDEBUG("Error! Invalid input parameters\n");
        I2CBUS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /* prepare internal dev offset as preamble write buffer */
    //memcpy(&write_buff[0], (char *)(&offset)+sizeof(int)-offset_sz, offset_sz);
    memcpy(&write_buff[0], (char *)&offset, offset_sz);

    PDEBUG("123$=> bus=%d,i2ca=0x%0x,offset=%d,offset_sz=%d,size=%d\n", 
           p_i2c_point->bus_id, p_i2c_point->dev_i2ca, offset, offset_sz, size);

    /* write/read transaction */
    _rc = _fi2cbus_raw_read_data(p_i2c_point, write_buff, 
                                  offset_sz, (char *)p_value, size);
    if ( _rc ) {
        PDEBUG("Error! Can't read byte\n");
        I2CBUS_ERROR_RETURN(_rc);
    }
    return CCL_OK; 
}

/* device "readb" command target */
static ccl_err_t _fi2cbus_cmd_readb(devo_t devo, device_cmd_parm_t parms[], b_u16 parms_num)
{
    i2cbus_ctx_t    *p_i2cbus = devman_device_private(devo);
    i2c_route_t      i2c_point;
    b_u16            dev_i2ca;
    int              offset;
    int              offset_sz; 
    b_u8             value;

    if (parms_num != 3) {
        PDEBUG("Error! Invalid input parameters\n");
        I2CBUS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    dev_i2ca = (b_u16)parms[0].value;
    offset = parms[1].value;
    offset_sz = parms[2].value; 

    memset(&i2c_point, 0, sizeof(i2c_route_t));
    i2c_point.bus_id = p_i2cbus->brdspec.bus_id;
    i2c_point.dev_i2ca = dev_i2ca;
    _rc = devi2cbus_read_byte(&i2c_point, offset, offset_sz, &value);
    if ( _rc ) {
        PDEBUG("Error! Can't read byte via I2C bus %d,offset=0x%03x,offset_sz=%d\n", 
               p_i2cbus->brdspec.bus_id, offset, offset_sz);
        return _rc;
    }
    PRINTL("value=0x%02x\n", value);
    return 0;
}

/* device "writeb" command target */
static ccl_err_t _fi2cbus_cmd_writeb(devo_t devo, device_cmd_parm_t parms[], b_u16 parms_num)
{
    i2cbus_ctx_t    *p_i2cbus = devman_device_private(devo);
    i2c_route_t      i2c_point;
    b_u16            dev_i2ca;
    b_u32              offset;
    b_u32              offset_sz; 
    b_u8             value;

    if (parms_num != 4) {
        PDEBUG("Error! Invalid input parameters\n");
        I2CBUS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    dev_i2ca = (b_u16)parms[0].value;
    offset = parms[1].value;
    offset_sz = parms[2].value; 
    value = (b_u8)parms[3].value;
    memset(&i2c_point, 0, sizeof(i2c_route_t));
    i2c_point.bus_id = p_i2cbus->brdspec.bus_id;
    i2c_point.dev_i2ca = dev_i2ca;
    _rc = devi2cbus_write_byte(&i2c_point, offset, offset_sz, value);
    if ( _rc ) {
        PDEBUG("Error! Can't write byte via I2C bus %d,offset=0x%03x,"
               "offset_sz=%d,value=%d\n", 
               p_i2cbus->brdspec.bus_id, offset, offset_sz, value);
        return _rc;
    }
    return 0;
}

/* device "readh" command target */
static ccl_err_t _fi2cbus_cmd_readh(devo_t devo, device_cmd_parm_t parms[], b_u16 parms_num)
{
    i2cbus_ctx_t    *p_i2cbus = devman_device_private(devo);
    i2c_route_t      i2c_point;
    b_u16            dev_i2ca;
    int              offset;
    int              offset_sz; 
    b_u16            value;

    if (parms_num != 3) {
        PDEBUG("Error! Invalid input parameters\n");
        I2CBUS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    dev_i2ca = (b_u16)parms[0].value;
    offset = parms[1].value;
    offset_sz = parms[2].value; 
    memset(&i2c_point, 0, sizeof(i2c_route_t));
    i2c_point.bus_id = p_i2cbus->brdspec.bus_id;
    i2c_point.dev_i2ca = dev_i2ca;
    _rc = devi2cbus_read_half(&i2c_point, offset, offset_sz, &value);
    if ( _rc ) {
        PDEBUG("Error! Can't read half via I2C bus %d,offset=0x%03x,offset_sz=%d\n", 
               p_i2cbus->brdspec.bus_id, offset, offset_sz);
        return _rc;
    }
    PRINTL("value=0x%04x\n", value);
    return 0;
}

/* device "writeh" command target */
static ccl_err_t _fi2cbus_cmd_writeh(devo_t devo, device_cmd_parm_t parms[], b_u16 parms_num)
{
    i2cbus_ctx_t    *p_i2cbus = devman_device_private(devo);
    i2c_route_t      i2c_point;
    b_u16            dev_i2ca;
    int              offset;
    int              offset_sz; 
    b_u16            value;

    if (parms_num != 4) {
        PDEBUG("Error! Invalid input parameters\n");
        I2CBUS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    dev_i2ca = (b_u16)parms[0].value;
    offset = parms[1].value;
    offset_sz = parms[2].value; 
    value = (b_u16)parms[3].value;
    memset(&i2c_point, 0, sizeof(i2c_route_t));
    i2c_point.bus_id = p_i2cbus->brdspec.bus_id;
    i2c_point.dev_i2ca = dev_i2ca;
    _rc = devi2cbus_write_half(&i2c_point, offset, offset_sz, value);
    if ( _rc ) {
        PDEBUG("Error! Can't write half via I2C bus %d,"
               "offset=0x%03x,offset_sz=%d,value=%d\n", 
               p_i2cbus->brdspec.bus_id, offset, offset_sz, value);
        return _rc;
    }
    return 0;
}

/* device "readw" command target */
static ccl_err_t _fi2cbus_cmd_readw(devo_t devo, device_cmd_parm_t parms[], b_u16 parms_num)
{
    i2cbus_ctx_t    *p_i2cbus = devman_device_private(devo);
    i2c_route_t      i2c_point;
    b_u16            dev_i2ca;
    int              offset;
    int              offset_sz; 
    b_u32            value;

    if (parms_num != 3) {
        PDEBUG("Error! Invalid input parameters\n");
        I2CBUS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    dev_i2ca = (b_u16)parms[0].value;
    offset = parms[1].value;
    offset_sz = parms[2].value; 
    memset(&i2c_point, 0, sizeof(i2c_route_t));
    i2c_point.bus_id = p_i2cbus->brdspec.bus_id;
    i2c_point.dev_i2ca = dev_i2ca;
    _rc = devi2cbus_read_word(&i2c_point, offset, offset_sz, &value);
    if ( _rc ) {
        PDEBUG("Error! Can't read word via I2C bus %d,offset=0x%03x,offset_sz=%d\n", 
               p_i2cbus->brdspec.bus_id, offset, offset_sz);
        return _rc;
    }
    PRINTL("value=0x%08x\n", value);
    return 0;
}

/* device "writew" command target */
static ccl_err_t _fi2cbus_cmd_writew(devo_t devo, device_cmd_parm_t parms[], b_u16 parms_num)
{
    i2cbus_ctx_t    *p_i2cbus = devman_device_private(devo);
    i2c_route_t      i2c_point;
    b_u16            dev_i2ca;
    int              offset;
    int              offset_sz; 
    b_u32            value;

    if (parms_num != 4) {
        PDEBUG("Error! Invalid input parameters\n");
        I2CBUS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    dev_i2ca = (b_u16)parms[0].value;
    offset = parms[1].value;
    offset_sz = parms[2].value; 
    value = (b_u32)parms[3].value;
    memset(&i2c_point, 0, sizeof(i2c_route_t));
    i2c_point.bus_id = p_i2cbus->brdspec.bus_id;
    i2c_point.dev_i2ca = dev_i2ca;
    _rc = devi2cbus_write_word(&i2c_point, offset, offset_sz, value);
    if ( _rc ) {
        PDEBUG("Error! Can't write word via I2C bus %d,offset=0x%03x,"
               "offset_sz=%d,value=%d\n", 
               p_i2cbus->brdspec.bus_id, offset, offset_sz, value);
        return _rc;
    }
    return 0;
}

/* device commands */
static device_cmd_t _cmds[] = 
{
    { 
        .name = "readb", .f_cmd = _fi2cbus_cmd_readb, .parms_num = 3,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "i2caddr", .type = eDEVMAN_ATTR_HALF, .size = sizeof(b_u16), .format = "%x" },
            { .name = "offset", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" },
            { .name = "offset_width", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
        }, 
        .help = "Read byte via I2C bus" 
    },
    { 
        .name = "readh", .f_cmd = _fi2cbus_cmd_readh, .parms_num = 3,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "i2caddr", .type = eDEVMAN_ATTR_HALF, .size = sizeof(b_u16), .format = "%x" },
            { .name = "offset", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" },
            { .name = "offset_width", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
        }, 
        .help = "Read half via I2C bus" 
    },
    { 
        .name = "readw", .f_cmd = _fi2cbus_cmd_readw, .parms_num = 3,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "i2caddr", .type = eDEVMAN_ATTR_HALF, .size = sizeof(b_u16), .format = "%x" },
            { .name = "offset", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" },
            { .name = "offset_width", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
        }, 
        .help = "Read word via I2C bus" 
    },
    { 
        .name = "writeb", .f_cmd = _fi2cbus_cmd_writeb, .parms_num = 4,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "i2caddr", .type = eDEVMAN_ATTR_HALF, .size = sizeof(b_u16), .format = "%x" },
            { .name = "offset", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" },
            { .name = "offset_width", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
            { .name = "value", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%x" },
        }, 
        .help = "Write byte via I2C bus" 
    },
    { 
        .name = "writeh", .f_cmd = _fi2cbus_cmd_writeh, .parms_num = 4,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "i2caddr", .type = eDEVMAN_ATTR_HALF, .size = sizeof(b_u16), .format = "%x" },
            { .name = "offset", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" },
            { .name = "offset_width", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
            { .name = "value", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u16), .format = "%x" },
        }, 
        .help = "Write half via I2C bus" 
    },
    { 
        .name = "writew", .f_cmd = _fi2cbus_cmd_writew, .parms_num = 4,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "i2caddr", .type = eDEVMAN_ATTR_HALF, .size = sizeof(b_u16), .format = "%x" },
            { .name = "offset", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" },
            { .name = "offset_width", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d" },
            { .name = "value", .type = eDEVMAN_ATTR_WORD, .size = sizeof(b_u32), .format = "%x" },
        }, 
        .help = "Write word via I2C bus" 
    },
};

/* i2cbus device attributes definition */
static device_attr_t _attrs[] =
{
    {
        .name = "bus_id", .id = eI2CBUS_BUS_ID, .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset = offsetof(struct tagt_i2cbus_ctx, brdspec.bus_id),
        .format = "%d"
    },
    {
        .name = "cdev_name", .id = eI2CBUS_CDEV_NAME, .type = eDEVMAN_ATTR_STRING, .maxnum = 1, 
        .offset = offsetof(struct tagt_i2cbus_ctx, cdev_name),
        .size = I2C_DEVNAME_SIZE,
    },
    {
        .name = "slaves_num", .id = eI2CBUS_SLAVES_NUM, .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset = offsetof(struct tagt_i2cbus_ctx, brdspec.slaves_num),
        .format = "%d"
    },
    {
        .name = "slave_name", .id = eI2CBUS_SLAVE_NAME, .type = eDEVMAN_ATTR_STRING, .maxnum = DEVBRD_I2CDEVS_MAXNUM, 
        .offset = offsetof(struct tagt_i2cbus_ctx, brdspec.slave_name),
        .size = DEVBRD_I2CDEVS_NAMELEN,
    },
    {
        .name = "slave_i2ca", .id = eI2CBUS_SLAVE_I2CA, .type = eDEVMAN_ATTR_HALF, .maxnum = DEVBRD_I2CDEVS_MAXNUM, 
        .offset = offsetof(struct tagt_i2cbus_ctx, brdspec.slave_i2ca),
        .format = "0x%02x"
    },
};

/* Device instance hw-specific initializations 
 */
static ccl_err_t _fi2cbus_add(void *devo, void *brdspec)
{
    i2cbus_ctx_t       *p_i2cbus;
    i2cbus_brdspec_t   *p_brdspec;
    b_u32              i;

    if ( !devo || !brdspec ) {
        PDEBUG("Error! Invalid input parameters\n");
        I2CBUS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_i2cbus = devman_device_private(devo);
    if ( !p_i2cbus ) {
        PDEBUG("NULL context area\n");
        I2CBUS_ERROR_RETURN(CCL_FAIL);
    }
    p_brdspec = (i2cbus_brdspec_t *)brdspec;
    if ( p_brdspec->bus_id >= eDEVBRD_I2C_BUSES_NUM ) {
        PDEBUG("invalid bus_id: %d\n", p_brdspec->bus_id);
        I2CBUS_ERROR_RETURN(CCL_FAIL);
    }
    /* back reference the device manager object handle */    
    p_i2cbus->devo = devo;    
    /* save the board specific info */
    memcpy(&p_i2cbus->brdspec, p_brdspec, sizeof(i2cbus_brdspec_t));
    /* cdev name */
    snprintf(p_i2cbus->cdev_name, sizeof(p_i2cbus->cdev_name), "/dev/i2c-%d", p_brdspec->bus_id);
    /* put in order the attributes arrays according to the real slaves number */
    for ( i = 0; i < sizeof(_attrs)/sizeof(_attrs[0]); i++ ) {
        if ( _attrs[i].maxnum == DEVBRD_I2CDEVS_MAXNUM )
            _attrs[i].maxnum = p_brdspec->slaves_num;
    }
    /* get mutex */
    _rc = pthread_mutex_init(&p_i2cbus->mutex, NULL);
    if ( _rc ) {
        PDEBUG("pthread_mutex_init error\n");
        I2CBUS_ERROR_RETURN(CCL_FAIL);
    }
    /* store the context */
    _p_i2cs[p_brdspec->bus_id] = p_i2cbus;
    return CCL_OK;
}

/* Device instance hw-specific deinitializations 
 */
static ccl_err_t _fi2cbus_cut(void *devo)
{
    i2cbus_ctx_t  *p_i2cbus;
    if ( !devo ) {
        PDEBUG("Error! Invalid input parameters\n");
        I2CBUS_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_i2cbus = devman_device_private(devo);
    _fi2cbus_release(p_i2cbus);
    _rc = pthread_mutex_destroy(&p_i2cbus->mutex);
    if ( _rc ) {
        PDEBUG("pthread_mutex_destroy error\n");
        I2CBUS_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}

/* device type description structure */
static device_driver_t _i2cbus_driver = 
{
    .name = "i2cbus",
    .f_add = _fi2cbus_add,
    .f_cut = _fi2cbus_cut,
    .attrs_num = sizeof(_attrs)/sizeof(_attrs[0]),
                 .attrs = _attrs,
    .cmds_num = sizeof(_cmds)/sizeof(_cmds[0]),
                .cmds = _cmds,
    .priv_size = sizeof(i2cbus_ctx_t)
}; 

/* Initialize I2CBUS device type
 */
ccl_err_t devi2cbus_init(void)
{
    /* register device type */
    _rc = devman_driver_register(&_i2cbus_driver);
    if ( _rc ) {
        PDEBUG("Error! Failed to register driver\n");
        I2CBUS_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}

/* Deinitialize I2CBUS device type
 */
ccl_err_t devi2cbus_fini(void)
{
    /* unregister device type */
    _rc = devman_driver_unregister(&_i2cbus_driver);
    if ( _rc ) {
        PDEBUG("Error! Failed to unregister driver\n");
        I2CBUS_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}
