/********************************************************************************/
/**
 * @file devman_core.c
 * @brief Device Manager kernel device driver 
 * @copyright 2019 CELARE. All rights reserved.
 */
/********************************************************************************/

#if !defined(__KERNEL__)
    #error This is a Linux kernel module and should NOT be compiled as user space program!
#endif

#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/ioport.h>
#include <linux/sched.h>
#include <linux/spinlock.h>
#include <linux/rwlock.h>
#include <linux/mutex.h>
#include <linux/io.h>
#include <linux/workqueue.h>
#include <linux/slab.h>
#include <linux/of.h>
#include <linux/delay.h>
#include <net/sock.h>

#include "devman_mod.h"

//#define DEVMAN_CORE_DEBUG
/* printing/error-returning macros */
#define PRINTL(fmt, args...) printk(KERN_INFO"%s: " fmt, "devman", ## args)
#ifdef DEVMAN_CORE_DEBUG
    #define PDEBUG(fmt, args...) printk(KERN_ERR"devman: %s[%d]: " fmt, __FUNCTION__, __LINE__, ## args)
#else
    #define PDEBUG(fmt, args...)
#endif
#define DEVMAN_ERROR_RETURN(err)                            \
{                                                           \
   PDEBUG("Error! line=%i, error=%i\n", __LINE__, (err));   \
   return err;                                              \
}
#define PDEBUGG(fmt, args...)


/* the DEVMAN context */
typedef struct tagt_devman_ctx {
    struct cdev         cdev;
    atomic_t            refcount;
    struct mutex        mutex;
    devman_kdevice_t    devs[DEVMAN_KDRV_DEVICES_MAXNUM];
} devman_ctx_t;

/* devman driver private context */
static devman_ctx_t _devman_ctx;

/* cdev major */
static int _major= DEVMAN_CHARDEV_MAJOR;

static int _rc;

/* character device operations */
static int _fdevman_cdev_open(struct inode *inode, struct file *filp);
static int _fdevman_cdev_release(struct inode *inode, struct file *filp);
static long _fdevman_cdev_ioctl(struct file *filp, unsigned int cmd, unsigned long arg);

static const struct
file_operations _devman_cdev_fops =
{
    .owner           = THIS_MODULE,
    .open            = _fdevman_cdev_open,
    .release         = _fdevman_cdev_release,
    .unlocked_ioctl  = _fdevman_cdev_ioctl,
};

/*============================================================================*/
/* CDEV framework targets implementation                                      */
/*----------------------------------------------------------------------------*/
/*============================================================================*/
/* Charachter device open callback
*/
static int _fdevman_cdev_open(struct inode* pinode, struct file* pfile)
{
    devman_ctx_t *p_devman  = NULL;

    BUG_ON(!pinode);
    BUG_ON(!pfile);

    p_devman = container_of(pinode->i_cdev, devman_ctx_t, cdev);

    BUG_ON(!p_devman);
    atomic_inc(&p_devman->refcount);
    pfile->private_data = p_devman;

    wmb();

    return 0;
}

/* Close the device and release the file's associated resources
 */
static int _fdevman_cdev_release(struct inode* pinode, struct file* pfile)
{
    devman_ctx_t  *p_devman = NULL;

    BUG_ON(!pinode);
    BUG_ON(!pfile);

    p_devman = pfile->private_data;
    BUG_ON (!p_devman);

    atomic_dec(&p_devman->refcount);
    pfile->private_data = NULL;

    wmb();

    return 0;
}

/* Performs the ioctl syscall operations on the device
*/
static long _fdevman_cdev_ioctl( struct file       *pfile, 
                                unsigned int       cmd,
                                unsigned long      param )
{
    devman_ctx_t          *p_devman;
    devman_ioctl_parm_t   *p_parm;
    devman_kdevice_t      *p_dev = 0;
    int                    i;

    /* check input parameters */
    if ( !pfile || !(p_devman = pfile->private_data) || !param || _IOC_TYPE(cmd) != DEVMAN_IOC_MAGIC ) {
        PDEBUG("Error! Invalid input parameters, _IOC_TYPE(cmd)=%c\n", _IOC_TYPE(cmd));
        DEVMAN_ERROR_RETURN(-EINVAL);
    }

    /* ioctl system call parameters */
    p_parm = (devman_ioctl_parm_t *)param;

    PDEBUG("123$=> cmd=0x%08x,p_parm=%p\n"
            "name=%s, page=0x%08x, offset=0x%08x, value=0x%08x, p=0x%08x, length=0x%08x\n", 
            cmd, p_parm,
            p_parm->name, p_parm->page, p_parm->offset, p_parm->value, p_parm->p, p_parm->length);

    /* check parameters area validity */
    if ( !access_ok(VERIFY_WRITE, p_parm, sizeof(devman_ioctl_parm_t)) || 
         (p_parm->p && !access_ok(VERIFY_WRITE, p_parm->p, p_parm->length)) ) {
        PDEBUG("Error! Can't access system call parameters, cmd=0x%08x,param=%p\n", cmd, p_parm);
        DEVMAN_ERROR_RETURN(-EPERM);
    }

    mutex_lock(&p_devman->mutex);

    /* look for the device in the database  */
    for ( i = 0; i < DEVMAN_KDRV_DEVICES_MAXNUM; i++ ) {
        if ( !strncmp(p_parm->name, p_devman->devs[i].name, DEVMAN_KDRV_DEVICE_NAME_LEN) ) {
            p_dev = &p_devman->devs[i];
            break;
        }
    }
    if ( !p_dev || !p_dev->f_devop ) {
        PDEBUG("Error! Can't find device %s\n", p_parm->name);
        mutex_unlock(&p_devman->mutex);
        DEVMAN_ERROR_RETURN(-EPERM);
    }

    /* perform the operation */
    _rc = p_dev->f_devop(cmd, (void *)param, p_dev->priv);
    if ( _rc ) {
        PDEBUG("Error! Can't operate %s device\n", p_dev->name);
    }

    mutex_unlock(&p_devman->mutex);

    return _rc;
}

/* Register device
 */
int devman_device_register(char *name, int (* f_devop)(unsigned int cmd, void *v_parm, void *priv), void *priv)
{
    devman_kdevice_t    *p_dev = 0;
    int                  i;

    /* chips */
    if ( !name || !f_devop || !priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        DEVMAN_ERROR_RETURN(-EINVAL);
    }

    /* look for first free place in the database  */
    for ( i = 0; i < DEVMAN_KDRV_DEVICES_MAXNUM; i++ ) {
        if ( !_devman_ctx.devs[i].name[0] ) {
            p_dev = &_devman_ctx.devs[i];
            break;
        }
    }
    if ( !p_dev ) {
        PDEBUG("Error! Can't locate device %s in DEVMAN\n", name);
        DEVMAN_ERROR_RETURN(-ENODEV);    
    }

    /* store the device */
    strcpy(p_dev->name, name);
    p_dev->f_devop = f_devop;
    p_dev->priv = priv;

    return 0;
}

/* Unregister device
 */
void devman_device_unregister(char *name)
{
    devman_kdevice_t    *p_dev = 0;
    int                  i;

    /* chips */
    if ( !name ) {
        PDEBUG("Error! Invalid input parameters\n");
        return;
    }

    /* look for device in the database  */
    for ( i = 0; i < DEVMAN_KDRV_DEVICES_MAXNUM; i++ ) {
        if ( _devman_ctx.devs[i].name[0] && 
             !strncmp(name, _devman_ctx.devs[i].name, DEVMAN_KDRV_DEVICE_NAME_LEN-1) ) {
            p_dev = &_devman_ctx.devs[i];
            break;
        }
    }
    if ( !p_dev ) {
        PDEBUG("Error! Can't find device %s in DEVMAN base\n", name);
        return;
    }

    /* clean up the slot */
    memset(p_dev, 0, sizeof(devman_kdevice_t));

    return;
}

/*==============================================================================
 * Name:   _fdevman_init
 * 
 * Descr:  Initialize the DEVMAN cdev, configure board specific stuff
 *----------------------------------------------------------------------------*/
static int __init _fdevman_init(void)
{
#ifdef MODULE_VERSION_TAGSTR
    PRINTL("Module version %s: Initializing the device driver... \n", MODULE_VERSION_TAGSTR);
#else
    PRINTL("Loading the %s kernel module\n", DEVMAN_DEVICE_DESCR);
#endif

    mutex_init(&_devman_ctx.mutex);
    atomic_set(&_devman_ctx.refcount, 0);
    /* create character device */
    cdev_init(&_devman_ctx.cdev, &_devman_cdev_fops);
    _devman_ctx.cdev.owner = THIS_MODULE;
    _devman_ctx.cdev.ops = &_devman_cdev_fops;
    _rc = cdev_add(&_devman_ctx.cdev, MKDEV(_major, 0), 1);
    if ( _rc < 0 ) {
        PDEBUG("Can't create %s char device! major=%d,minor=%d\n", DEVMAN_DEVICE_NAME, _major, 0);
        goto _fdevman_init_fail;
    }
    PDEBUG("Char device %s created, major=%d,minor=%d\n", DEVMAN_DEVICE_NAME, _major, 0);

    /* initialize the board specific devices */
    _rc = devman_board_init();
    if ( _rc ) {
        PDEBUG("Error! Can't initialize the board specific devices!\n");
        goto _fdevman_init_fail;
    }

#ifdef MODULE_VERSION_TAGSTR
    PRINTL("Module version %s is successfully loaded :)\n", MODULE_VERSION_TAGSTR);
#else
    PRINTL("Loading of the %s kernel module has been completed successfully :)\n", DEVMAN_DEVICE_DESCR);
#endif

    return 0;

_fdevman_init_fail:
    cdev_del(&_devman_ctx.cdev);
    mutex_destroy(&_devman_ctx.mutex);
    return _rc;
}

/*==============================================================================
 * Name:            _fdevman_fini
 * Description:     Shut down the DEVMAN module
 *----------------------------------------------------------------------------*/
static void __exit _fdevman_fini(void)
{
    /* remove the board specific stuff */
    devman_board_fini();

    /* unregister the cdev */
    cdev_del(&_devman_ctx.cdev);

    BUG_ON( atomic_read(&_devman_ctx.refcount) != 0 );

    memset(&_devman_ctx, 0, sizeof(devman_ctx_t));
}

MODULE_DESCRIPTION  ("Kernel Driver for the Device Manager framework");
#if defined(MODULE_VERSION_TAGSTR)
MODULE_INFO         (version, MODULE_VERSION_TAGSTR);
#endif
MODULE_AUTHOR       ("Yuri S.");
MODULE_INFO         (copyright, "Celare. All rights reserved.");
/*MODULE_LICENSE    ("Proprietary"); 123$*/
MODULE_LICENSE      ("GPL");

module_init(_fdevman_init);
module_exit(_fdevman_fini);

