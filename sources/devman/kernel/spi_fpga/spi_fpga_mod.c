/********************************************************************************/
/**
 * @file fpga_spi.c
 * @brief FPGA SPI kernel device driver 
 * @copyright 2019 CELARE. All rights reserved.
 */
/********************************************************************************/
#if !defined(__KERNEL__)
    #error This is a Linux kernel module and should NOT be compiled as user space program!
#endif

#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/ioport.h>
#include <linux/sched.h>
#include <linux/spinlock.h>
#include <linux/rwlock.h>
#include <linux/mutex.h>
#include <linux/io.h>
#include <linux/workqueue.h>
#include <linux/slab.h>
#include <linux/of.h>
#include <linux/delay.h>
#include <linux/spi/spi.h>
#include <linux/gpio.h>
#include <net/sock.h>

#include "devman_mod.h"
#include "spi_mod.h"

//#define FPGA_DEBUG
/* printing/error-returning macros */
#define PRINTL(fmt, args...) printk(KERN_INFO"%s: " fmt, FPGA_DEVICE_NAME, ## args)
#ifdef FPGA_DEBUG
    #define PDEBUG(fmt, args...) printk("FPGA-K: %s[%d]: " fmt, __FUNCTION__, __LINE__, ## args)
#else
    #define PDEBUG(fmt, args...)
#endif
#define FPGA_ERROR_RETURN(err) { \
   PRINTL("Error! %s[%d], error=%i\n", __FUNCTION__, __LINE__, (err)); \
   return err; \
}
#define PDEBUGG(fmt, args...)

/* There's an off-by-one between the gpio bank number and the gpiochip */
/* range e.g. GPIO_1_5 is gpio 5 under linux */
#define IMX_GPIO_NR(bank, nr)		(((bank) - 1) * 32 + (nr))

/* the FPGA device context */
typedef struct tagt_fpga_ctx {
    char               name[DEVMAN_KDRV_DEVICE_NAME_LEN];
    struct mutex       mutex;  
    struct spi_device  *p_spidev;
    int                hw_virq;        /* hw virtual irq number */

} fpga_ctx_t;

/* fpga driver private context */
static fpga_ctx_t _fpga_ctx[2];

static int _rc;


/* FPGA write access operation
 */
static int _fpga_spi_reg_write(fpga_ctx_t *p_fpga, char page, 
                               unsigned int offset, char *p_value, int size)
{
    char    spi_tx_buff[16];
    int     cmd;
    int     i;
    unsigned int offset1 = offset >> 12;

    /* check input parameters */
    if ( !p_fpga || !p_value || !size ) {
        PDEBUG("Error: Invalid input parameters, p_fpga=%p,page=0x%02x,offset=0x%x,p_value=%p,size=0x%x\n", 
               p_fpga, page, offset, p_value, size);
        FPGA_ERROR_RETURN(-EINVAL);
    }
    PDEBUG("123$=> page=0x%02x,offset=0x%02x,value=0x%08x,size=%d\n", page, offset, *(u32 *)p_value, size);

    memset(spi_tx_buff, 0, sizeof(spi_tx_buff));
    mutex_lock(&p_fpga->mutex);
    if (offset1) {
        /* setup the SFR0 register (higher 20 bits of 32-bit offset) */
        spi_tx_buff[0] = (FPGA_SPI_CMD_WR_SFR << 4) | ((FPGA_SPI_SFR_ADDR >> 8) & 0xf);
        spi_tx_buff[1] = FPGA_SPI_SFR_ADDR & 0xff;
        spi_tx_buff[2] = (offset1 >> 24) & 0xff;
        spi_tx_buff[3] = (offset1 >> 16) & 0xff;
        spi_tx_buff[4] = (offset1 >> 8) & 0xff;
        spi_tx_buff[5] = offset1 & 0xff;


        PDEBUG("After setting SFR: %02x %02x %02x %02x %02x %02x\n",
               spi_tx_buff[0], spi_tx_buff[1], spi_tx_buff[2],
               spi_tx_buff[3], spi_tx_buff[4], spi_tx_buff[5]);
        _rc = spi_write_then_read(p_fpga->p_spidev, spi_tx_buff, 6, NULL, 0);
        if ( _rc ) {
            PDEBUG("ERROR: Can't set fpga page register!, page=0x%02x\n", page);
            goto fpga_spi_error;
        }
    }

    cmd = (size == 1) ? FPGA_SPI_CMD_WR_8 :
          (size == 2) ? FPGA_SPI_CMD_WR_16 :
          (size == 4) ? FPGA_SPI_CMD_WR_32 : FPGA_SPI_CMD_INVALID;

    if (cmd == FPGA_SPI_CMD_INVALID) {
        PDEBUG("ERROR: Invalid size: %d\n", size);
        goto fpga_spi_error;
    }

    memset(spi_tx_buff, 0, sizeof(spi_tx_buff));

    /* prepare the register offset */
    spi_tx_buff[0] = (cmd << 4) | (offset >> 8) & 0xf; 
    spi_tx_buff[1] = offset & 0xff;
    /* prepare the write value, then write down */
    /* starting from the lowest byte */
    for ( i = 0; i < size; i++ )
        spi_tx_buff[2+i] = p_value[size-i-1];

    PDEBUG("After setting data: cmd: 0x%01x, data: %02x %02x %02x %02x %02x %02x\n",
           cmd, spi_tx_buff[0], spi_tx_buff[1], spi_tx_buff[2],
           spi_tx_buff[3], spi_tx_buff[4], spi_tx_buff[5]);

    _rc = spi_write_then_read(p_fpga->p_spidev, spi_tx_buff, 2+i, NULL, 0);
    if ( _rc ) {
        PDEBUG("Error! Can't write the data, offset=0x%03x,value=0x%08x\n", offset, *(int *)p_value);
        goto fpga_spi_error;
    }

    mutex_unlock(&p_fpga->mutex);

    return 0;

fpga_spi_error:
    mutex_unlock(&p_fpga->mutex);
    FPGA_ERROR_RETURN(-EINVAL);
}

/* FPGA write access operation
 */
static int _fpga_spi_buf_write(fpga_ctx_t *p_fpga, char page, 
                               unsigned int offset, char *p_value, int size)
{
    char    spi_tx_buff[FPGA_SPI_MAX_BURST_CMD_LEN];
    int     cmd;
    int     i;
    int     spi_burst_l;
    struct timeval t1, t2, t3, t4;

    do_gettimeofday(&t1);

    /* check input parameters */
    if ( !p_fpga || !p_value || !size || size > FPGA_SPI_MAX_BURST_LEN) {
        PDEBUG("Error: Invalid input parameters, p_fpga=%p,page=0x%02x,offset=0x%x,p_value=%p,size=0x%x\n", 
               p_fpga, page, offset, p_value, size);
        FPGA_ERROR_RETURN(-EINVAL);
    }
    PDEBUG("123$=> page=0x%02x,offset=0x%02x,value=0x%08x,size=%d\n", page, offset, *(u32 *)p_value, size);

    memset(spi_tx_buff, 0, sizeof(spi_tx_buff));
    do_gettimeofday(&t2);
    spi_burst_l = size - 1;

    spi_tx_buff[0] = (FPGA_SPI_CMD_WR_BURST << 4);
    spi_tx_buff[0] |= ((spi_burst_l >> 8) & 0x07);
    spi_tx_buff[1] = spi_burst_l & 0xff;
    spi_tx_buff[2] = (offset >> 24) & 0xff;
    spi_tx_buff[3] = (offset >> 16) & 0xff;
    spi_tx_buff[4] = (offset >> 8) & 0xff;
    spi_tx_buff[5] = offset & 0xff;
    for ( i = 0; i < size; i++ )
        spi_tx_buff[6+i] = p_value[i];


    PDEBUG("After setting data: cmd: 0x%01x, data: %02x %02x %02x %02x %02x %02x  %02x  %02x  %02x\n",
           cmd, spi_tx_buff[0], spi_tx_buff[1], spi_tx_buff[2],
           spi_tx_buff[3], spi_tx_buff[4], spi_tx_buff[5],
           spi_tx_buff[6], spi_tx_buff[7], spi_tx_buff[8]);


    do_gettimeofday(&t3);
    mutex_lock(&p_fpga->mutex);
    _rc = spi_write(p_fpga->p_spidev, spi_tx_buff, 6+size);
    if ( _rc ) {
        PDEBUG("Error! Can't write the data, offset=0x%03x,value=0x%08x\n", offset, *(int *)p_value);
        goto fpga_spi_error;
    }
    do_gettimeofday(&t4);
    PDEBUG("%ld:%ld, %ld:%ld, %ld:%ld, %ld:%ld\n", 
           t1.tv_sec, t1.tv_usec, 
           t2.tv_sec, t2.tv_usec,
           t3.tv_sec, t3.tv_usec,
           t4.tv_sec, t4.tv_usec);

    mutex_unlock(&p_fpga->mutex);

    return 0;

fpga_spi_error:
    mutex_unlock(&p_fpga->mutex);
    FPGA_ERROR_RETURN(-EINVAL);
}

static int _fpga_spi_reg_write_cs_low(fpga_ctx_t *p_fpga, char *p_value, int len)
{
    struct spi_message message;
    struct spi_transfer transfer[1] = {};

    /* check input parameters */
    if ( !p_fpga || !p_value || !len ) {
        PDEBUG("Error: Invalid input parameters, p_fpga=%p,p_value=%p,len=%d\n", 
               p_fpga, p_value, len);
        FPGA_ERROR_RETURN(-EINVAL);
    }

    spi_message_init(&message);

    transfer[0].cs_change = 1;
    transfer[0].len = len;
    transfer[0].tx_buf = p_value;
    PDEBUG("cs_change=%d, len=%d, tx_buf[0]=0x%02x, tx_buf[1]=0x%02x\n",
           transfer[0].cs_change, transfer[0].len, *(char *)(transfer[0].tx_buf), *((char *)(transfer[0].tx_buf)+1));
    spi_message_add_tail(&transfer[0], &message);

    _rc = spi_sync(p_fpga->p_spidev, &message);
    if (_rc) {
        PDEBUG("spi_sync error\n");
        FPGA_ERROR_RETURN(_rc);
    }

    return 0;
}

/* FPGA read access operation
 */
static int _fpga_spi_reg_read(fpga_ctx_t *p_fpga, char page, 
                              unsigned int offset, char *p_value, int size)
{
    char    spi_tx_buff[16], spi_rx_buff[64];
    int     cmd;
    int     i, j = 0;
    unsigned int offset1 = offset >> 12;
    int     gpio_spi = IMX_GPIO_NR(FPGA_GPIO_SPI_BANK, FPGA_GPIO_SPI_IO);
    int     gpio_val;

    /* check input parameters */
    if ( !p_fpga || !p_value || !size ) {
        PDEBUG("Error: Invalid input parameters, p_fpga=%p,page=0x%02x,offset=0x%x,p_value=%p,size=0x%x\n", 
               p_fpga, page, offset, p_value, size);
        FPGA_ERROR_RETURN(-EINVAL);
    }
    PDEBUG("123$=> page=0x%02x,offset=0x%x,size=%d, cs=%d\n", 
           page, offset, size, p_fpga->p_spidev->chip_select);

    /* clear the output buffer */
    memset(spi_tx_buff, 0, sizeof(spi_tx_buff));
    /* dirty the input buffer */
    memset(spi_rx_buff, 0xff, sizeof(spi_rx_buff));

    PDEBUG("line: %d\n", __LINE__);
    mutex_lock(&p_fpga->mutex);
    if (offset1) {
        /* setup the SFR0 register (higher 20 bits of 32-bit offset) */
        spi_tx_buff[0] = (FPGA_SPI_CMD_WR_SFR << 4) | ((FPGA_SPI_SFR_ADDR >> 8) & 0xf);
        spi_tx_buff[1] = FPGA_SPI_SFR_ADDR & 0xff;
        spi_tx_buff[2] = (offset1 >> 24) & 0xff;
        spi_tx_buff[3] = (offset1 >> 16) & 0xff;
        spi_tx_buff[4] = (offset1 >> 8) & 0xff;
        spi_tx_buff[5] = offset1 & 0xff;

        PDEBUG("After setting SFR: %02x %02x %02x %02x %02x %02x\n",
               spi_tx_buff[0], spi_tx_buff[1], spi_tx_buff[2],
               spi_tx_buff[3], spi_tx_buff[4], spi_tx_buff[5]);

        _rc = spi_write_then_read(p_fpga->p_spidev, spi_tx_buff, 6, NULL, 0);
        if ( _rc < 0) {
            PDEBUG("Error! Can't setup offset/read the register, page=0x%02x,offset=0x%02x\n", page, offset);
            goto fpga_spi_error;
        }
    }

    cmd = (size == 1) ? FPGA_SPI_CMD_RD_8 :
          (size == 2) ? FPGA_SPI_CMD_RD_16 :
          (size == 4) ? FPGA_SPI_CMD_RD_32 : FPGA_SPI_CMD_INVALID;

    if (cmd == FPGA_SPI_CMD_INVALID) {
        PDEBUG("ERROR: Invalid size: %d\n", size);
        goto fpga_spi_error;
    }

    memset(spi_tx_buff, 0, sizeof(spi_tx_buff));

    /* prepare the register offset */
    spi_tx_buff[0] = (cmd << 4) | (offset >> 8) & 0xf; 
    spi_tx_buff[1] = offset & 0xff;

    PDEBUG("After setting data: cmd: 0x%01x, data: %02x %02x %02x %02x %02x %02x\n",
           cmd, spi_tx_buff[0], spi_tx_buff[1], spi_tx_buff[2],
           spi_tx_buff[3], spi_tx_buff[4], spi_tx_buff[5]);

    gpio_val = gpio_get_value(gpio_spi);
    PDEBUG("check gpio_spi before writing offset: %d\n", gpio_val);
    _rc = _fpga_spi_reg_write_cs_low(p_fpga, spi_tx_buff, 2);
    if ( _rc ) {
        PDEBUG("_fpga_spi_reg_write_cs_low error\n");
        goto fpga_spi_error;
    }

    gpio_val = gpio_get_value(gpio_spi);
    int cnt = 0;
    while (1) {
        if (gpio_val) {
            PDEBUG("spi_read!!!\n");
            _rc = spi_read(p_fpga->p_spidev, spi_rx_buff, size);
            if ( _rc ) {
                PDEBUG("ERROR: Can't set fpga page register!, page=0x%02x\n", page);
                goto fpga_spi_error;
            }
            PDEBUG("gpio_spi=%d, cnt=%d\n", gpio_val, cnt);
            break;           
        }
        cnt++;
        gpio_val = gpio_get_value(gpio_spi);
    }

    PDEBUG("After receiving data: data: %02x %02x %02x %02x\n",
           spi_rx_buff[0], spi_rx_buff[1], spi_rx_buff[2], spi_rx_buff[3]);

    /* starting from the lowest byte, after RACK byte */
    for ( i = size - 1, j = 0; i >= 0; i-- )
        p_value[i] = spi_rx_buff[j++];

    PDEBUG("123$=> got register: page=0x%02x,offset=0x%02x,size=%d,value=0x%x\n", page, offset, size, *(int *)p_value);
    mutex_unlock(&p_fpga->mutex);

    return 0;

fpga_spi_error:
    mutex_unlock(&p_fpga->mutex);
    FPGA_ERROR_RETURN(-EINVAL);
}

static int _fpga_spi_program(fpga_ctx_t *p_fpga, char *p_value, int size)
{
    /* check input parameters */
    if ( !p_fpga || !p_value || !size ) {
        PDEBUG("Error: Invalid input parameters, p_fpga=%p,page=0x%02x,offset=0x%x,p_value=%p,size=0x%x\n", 
               p_fpga, p_value, size);
        FPGA_ERROR_RETURN(-EINVAL);
    }

    PDEBUGG("pvalue=0x%08x, size=%d\n", p_value, size);

    mutex_lock(&p_fpga->mutex);
    _rc = spi_write(p_fpga->p_spidev, p_value, size);
    if ( _rc ) {
        PDEBUG("Error! Can't write the data, pvalue=0x%08x, size=%d\n", 
               p_value, size);
        goto fpga_spi_error;
    }

    mutex_unlock(&p_fpga->mutex);

    return 0;

fpga_spi_error:
    mutex_unlock(&p_fpga->mutex);
    FPGA_ERROR_RETURN(-EINVAL);
}

static int _fpga_logispi_program(fpga_ctx_t *p_fpga, char *p_value, int size)
{
#define BITSTREAM_BUFFER_OFFSET        0x00060000
#define AXIGPIO_VIRTEX_CTRL_OFFSET     0x00026000
#define AXIGPIO_DATA_OFFSET            0
#define INIT_B_MASK                    0x00000002 /* input */
#define DONE_MASK                      0x00000004 /* input */       
#define BITSTREAM_BUSY_MASK            0x00001000 /* input */
#define DATA_ARRAY_LEN                 16
    int          burst_size = 2048;
    int          burst_last_size;
    int          len;
    int          full_burst_num;
    int          i, j;
    unsigned int gpio_data;   
    int          gpio_data_sz = 4;
    char         *buf = p_value;
    char         buf1[DATA_ARRAY_LEN];
    char         *buf2 = NULL;
    int          sz, remainder, padding_len;

    /* check input parameters */
    if ( !p_fpga || !p_value || !size ) {
        PDEBUG("Error!!! Invalid input parameters, p_fpga=%p,page=0x%02x,offset=0x%x,p_value=%p,size=0x%x\n", 
               p_fpga, p_value, size);
        FPGA_ERROR_RETURN(-EINVAL);
    }

    full_burst_num = size / burst_size;
    burst_last_size = size % burst_size;
    sz = burst_last_size;
    remainder = burst_last_size % 4;
    if (remainder) {
        padding_len = 4 - remainder;
        sz = burst_last_size + padding_len;
        buf2 = kmalloc(sz, GFP_KERNEL);
        if (!buf2) {
            PDEBUG("Error! Can't allocate memory");
            FPGA_ERROR_RETURN(-EINVAL);
        }
        int offset = full_burst_num * burst_size;
        memcpy(buf2, buf+offset, burst_last_size);
        for (i = 0; i < sz-burst_last_size; i++)
            *(buf2+burst_last_size+i) = 0xff;
    }
    PRINTL("pvalue=0x%08x, file_size=%d, burst_size=%d, full_burst_num=%d, burst_last_size=%d, padding_len=%d\n", 
           p_value, size, burst_size, full_burst_num, burst_last_size, sz-burst_last_size);
    for (i = 0; i < full_burst_num + 1; i++) {
        if (i<2 || i==full_burst_num) {
            PDEBUG("%02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x\n",
                   buf[0], buf[1], buf[2], buf[3],
                   buf[4], buf[5], buf[6], buf[7],
                   buf[8], buf[9], buf[10], buf[11],
                   buf[12], buf[13], buf[14], buf[15]);
        }

        _rc = _fpga_spi_reg_read(p_fpga, 0, 
                                 AXIGPIO_VIRTEX_CTRL_OFFSET, 
                                 (char *)&gpio_data, gpio_data_sz);
        if ( _rc ) {
            PDEBUG("Error! Can't read register, offset=0x%08x, size=%d\n", 
                   AXIGPIO_VIRTEX_CTRL_OFFSET, gpio_data_sz);
            goto logispi_program_error;
        }
        j = 0;
        while (gpio_data & BITSTREAM_BUSY_MASK) {
            if (j++ > 1000000) {
                PRINTL("BITSTREAM_BUSY has not gone low: i=%d\n", i);
                goto logispi_program_error;
            }
            _rc = _fpga_spi_reg_read(p_fpga, 0, 
                                     AXIGPIO_VIRTEX_CTRL_OFFSET, 
                                     (char *)&gpio_data, gpio_data_sz);
            if ( _rc ) {
                PDEBUG("Error! Can't read register, offset=0x%08x, size=%d\n", 
                       AXIGPIO_VIRTEX_CTRL_OFFSET, gpio_data_sz);
                goto logispi_program_error;
            }
        }

        if (i < full_burst_num)
            len = burst_size;
        else
            len = sz;
        if (!(i%1000 || len == burst_last_size)) {
            PDEBUG("_fpga_spi_buf_write: offset=0x%08x, len=%d, i=%d\n",
                   BITSTREAM_BUFFER_OFFSET, len, i);
        }
        struct timeval t1, t2;
        do_gettimeofday(&t1);

        if (len < full_burst_num)
            _rc = _fpga_spi_buf_write(p_fpga, 0, 
                                      BITSTREAM_BUFFER_OFFSET,  
                                      buf, len);
        else
            _rc = _fpga_spi_buf_write(p_fpga, 0, 
                                      BITSTREAM_BUFFER_OFFSET,  
                                      buf2, len);
        if ( _rc ) {
            PDEBUG("Error! Can't write buffer, offset=0x%08x, size=%d\n", 
                   BITSTREAM_BUFFER_OFFSET, len);
            goto logispi_program_error;
        }
        do_gettimeofday(&t2);
        PDEBUG("(i=%d) - %ld:%ld, %ld:%ld\n", 
               i, t1.tv_sec, t1.tv_usec, t2.tv_sec, t2.tv_usec);
        buf += burst_size;
    }
    /* check INIT_B after programming*/
    PRINTL("check INIT_B after programming\n");
    _rc = _fpga_spi_reg_read(p_fpga, 0, 
                             AXIGPIO_VIRTEX_CTRL_OFFSET, 
                             (char *)&gpio_data, gpio_data_sz);
    if ( _rc ) {
        PDEBUG("Error! Can't read register, offset=0x%08x, size=%d\n", 
               AXIGPIO_VIRTEX_CTRL_OFFSET, gpio_data_sz);
        goto logispi_program_error;
    }
    PRINTL("INIT_B must be 1: INIT_B=%d\n", !!(gpio_data & INIT_B_MASK));
    if (!(gpio_data & INIT_B_MASK)) {
        PDEBUG("INIT_B is low\n");
        goto logispi_program_error;
    }

    for (i = 0; i < DATA_ARRAY_LEN; i++)
        buf1[i] = 0xff;

    i = 0;
    PRINTL("check DONE after programming: DONE=%d\n",
           !!(gpio_data & DONE_MASK));
    while (!(gpio_data & DONE_MASK)) {
        j = 0;
        while (gpio_data & BITSTREAM_BUSY_MASK) {
            if (j++ > 1000000) {
                PRINTL("BITSTREAM_BUSY has not gone low: i=%d\n", i);
                goto logispi_program_error;
            }
        }
        _rc = _fpga_spi_buf_write(p_fpga, 0, 
                                  BITSTREAM_BUFFER_OFFSET,  
                                  buf1, DATA_ARRAY_LEN);
        if ( _rc ) {
            PDEBUG("Error! Can't write buffer, offset=0x%08x, size=%d\n", 
                   BITSTREAM_BUFFER_OFFSET, DATA_ARRAY_LEN);
            FPGA_ERROR_RETURN(-EINVAL);
        }

        _rc = _fpga_spi_reg_read(p_fpga, 0, 
                                 AXIGPIO_VIRTEX_CTRL_OFFSET, 
                                 (char *)&gpio_data, gpio_data_sz);
        if ( _rc ) {
            PDEBUG("Error! Can't read register, offset=0x%08x, size=%d\n", 
                   AXIGPIO_VIRTEX_CTRL_OFFSET, gpio_data_sz);
            goto logispi_program_error;
        }
        if (!(i++ % 1000))
            PRINTL("i=%d, gpio_data=0x%08x, DONE=%d\n", 
                   i, gpio_data, !!(gpio_data & DONE_MASK));
    }

    if ( _rc ) {
        PDEBUG("Error! Can't write the data, pvalue=0x%08x, size=%d\n", 
               p_value, size);
        goto logispi_program_error;
    }

    PRINTL("Programming finished\n");            
    return 0;

logispi_program_error:
    kfree(buf2);
    FPGA_ERROR_RETURN(-EINVAL);
}

/* Performs the device operations
 */
static int _fpga_dev_op(unsigned int cmd, void *v_parm, void *priv)
{
    fpga_ctx_t              *p_fpga = (fpga_ctx_t *)priv;
    devman_ioctl_parm_t     *p_parm;
    char                    *buff;

    /* check input parameters */
    if ( !cmd || !v_parm ) {
        PDEBUG("Error! Invalid input parameters, cmd=0x%08x, parm=%p\n", cmd, v_parm);
        FPGA_ERROR_RETURN(-EINVAL);
    }

    /* get parameters */
    p_parm = (devman_ioctl_parm_t *)v_parm;
    buff = __vmalloc(p_parm->length, GFP_KERNEL, PAGE_KERNEL);
    if (!buff) {
        PDEBUG("Error! Can't allocate memory");
        FPGA_ERROR_RETURN(-EINVAL);
    }
    PDEBUG("line: %d\n", __LINE__);
    /* perform the command */
    switch ( cmd ) {
    /* read register value */
    case DEVMAN_IOC_READR:
        PDEBUG("line: %d\n", __LINE__);
        memset(buff, 0, p_parm->length);
        _rc = _fpga_spi_reg_read(p_fpga, (char)p_parm->page, p_parm->offset, buff/*123$; p_parm->p*/, p_parm->length);
        if ( _rc ) {
            PDEBUG("Error! Can't read register, page=0x%02x, offset0x%02x, size=%d\n", p_parm->page, p_parm->offset, p_parm->length);
            break;
        }
        PDEBUG("line: %d\n", __LINE__);
        _rc = copy_to_user(p_parm->p, buff, p_parm->length);
        if ( _rc ) {
            PDEBUG("Error! Can't copy to user memory\n");
        }
        break;
        /* write register value */
    case DEVMAN_IOC_WRITER:
        _rc = copy_from_user(buff, p_parm->p, p_parm->length);
        if ( _rc ) {
            PDEBUG("Error! Can't copy from user memory\n");
            break;
        }
        _rc = _fpga_spi_reg_write(p_fpga, (char)p_parm->page, p_parm->offset, buff, p_parm->length);
        if ( _rc ) {
            PDEBUG("Error! Can't write register, page=0x%02x, offset0x%02x, size=%d\n, value=0x%08x\n", 
                   p_parm->page, p_parm->offset, p_parm->length, *(u32 *)buff);
        }
        break;
    case DEVMAN_IOC_WRITEBUF:
        _rc = copy_from_user(buff, p_parm->p, p_parm->length);
        if ( _rc ) {
            PDEBUG("Error! Can't copy from user memory\n");
            break;
        }
        _rc = _fpga_spi_buf_write(p_fpga, (char)p_parm->page, p_parm->offset, buff, p_parm->length);
        if ( _rc ) {
            PDEBUG("Error! Can't write buffer, page=0x%02x, offset0x%02x, size=%d\n, value=0x%08x\n", 
                   p_parm->page, p_parm->offset, p_parm->length, *(u32 *)buff);
        }
        break;
    case DEVMAN_IOC_EXEC:
        _rc = copy_from_user(buff, p_parm->p, p_parm->length);
        if ( _rc ) {
            PDEBUG("Error! Can't copy from user memory\n");
            break;
        }
#define PROGRAM_OPTIMIZATION
#ifdef PROGRAM_OPTIMIZATION
        _rc = _fpga_logispi_program(p_fpga, buff, p_parm->length);
        if ( _rc ) {
            PDEBUG("Error! Can't write buffer, size=%d\n, value=0x%08x\n", 
                   p_parm->length, *(u32 *)buff);
        }
#else
        _rc = _fpga_spi_program(p_fpga, buff, p_parm->length);
        if ( _rc ) {
            PDEBUG("Error! Can't write buffer, size=%d\n, value=0x%08x\n", 
                   p_parm->length, *(u32 *)buff);
        }
#endif /* PROGRAM_OPTIMIZATION */
        break;
    default:
        PDEBUG("Error! Can't recognize the operation, cmd=0x%08x, parm=%p\n", cmd, v_parm); 
        _rc = -EINVAL;
        break;
    }
    if (buff)
        kvfree(buff);

    return _rc;
}

/*==============================================================================
 * Name:   _fpga_spi_add
 * 
 * Descr:  Initialize the FPGA SPI access
 *----------------------------------------------------------------------------*/
static int _fpga_spi_add(fpga_ctx_t *p_fpga, int cs)
{
    struct spi_master  *spi_master;
    struct spi_device  *spi_device;
    struct device      *pdev;
    char                buff[64];

    /*123$; chipa*/

    spi_master = (struct spi_master *)spi_busnum_to_master(FPGA_SPI_BUS);        
    if ( !spi_master ) {
        PDEBUG("Unable to get spi bus master! bus=%d\n", FPGA_SPI_BUS);
        FPGA_ERROR_RETURN(-EINVAL);
    }
    spi_device = spi_alloc_device(spi_master);
    if ( !spi_device ) {
        PDEBUG("Can't allocate spi device! bus=%d\n", FPGA_SPI_BUS);
        FPGA_ERROR_RETURN(-EINVAL);
    }
    /* specify the chip select */
    spi_device->chip_select = cs;

    /* check if the cs is already claimed */
    snprintf(buff, sizeof(buff),"%s.%u",dev_name(&spi_device->master->dev), spi_device->chip_select);
    PDEBUG("buff=%s\n",buff);
    pdev = bus_find_device_by_name(spi_device->dev.bus, NULL, buff);
    if ( pdev ) {
        /* not going to use this spi_device, so free it */
        spi_dev_put(spi_device);

        /* there is already a device configured for this bus.cs, 
         forex if we previously loaded and unloaded the driver;
         if it is not us, we stop with fail */
        if ( pdev->driver && pdev->driver->name && 
             strncmp(FPGA_DEVICE_NAME, pdev->driver->name, 
                     strnlen(FPGA_DEVICE_NAME, sizeof(FPGA_DEVICE_NAME))) ) {
            PDEBUG("Driver [%s] already registered for %s\n", pdev->driver->name, buff);
            FPGA_ERROR_RETURN(-EINVAL);
        }
    } else {
        spi_device->max_speed_hz = FPGA_SPI_MAX_SPEED;
        spi_device->mode = FPGA_SPI_MODE;
        spi_device->bits_per_word = FPGA_SPI_BITS_P_WORD;
        spi_device->irq = -1;
        spi_device->controller_state = NULL;
        spi_device->controller_data = NULL;
        strlcpy(spi_device->modalias, FPGA_DEVICE_NAME, SPI_NAME_SIZE);
        _rc = spi_add_device(spi_device);
        if (_rc < 0) {
            spi_dev_put(spi_device);
            PDEBUG("spi_add_device() failed!\n");
            FPGA_ERROR_RETURN(_rc);
        }
        printk("max_speed_hz=%lu\n",spi_device->max_speed_hz);
        put_device(&spi_master->dev);        
    }

    /* back reference to spi device */
    p_fpga->p_spidev = spi_device;

    return 0;
}

/*==============================================================================
 * Name:   _fpga_spi_cut
 * 
 * Descr:  Deinitialize the FPGA SPI access
 *----------------------------------------------------------------------------*/
static void _fpga_spi_cut(fpga_ctx_t *p_fpga)
{
    /*123$; chipa*/
    if ( p_fpga->p_spidev )
        spi_unregister_device(p_fpga->p_spidev);
}

/* Hardware dedicated line Interrupr Service Routine
 */
static irqreturn_t _fpga_hw_isr(int irq, void *priv)
{
    static int cnt = 0;
    /* chipas */
    if ( !irq || !priv ) {
        PDEBUG("123$=> Error! Bad input: irq=%d, priv=%p\n", irq, priv);
        return IRQ_HANDLED;
    }

    if (!cnt) {
        PDEBUG("Interrupt handled successfully: irq=%d, priv=%p\n", irq, priv);
        cnt++;
    }

    return IRQ_HANDLED;

}

/* Initialize the FPGA device, configure registers, driver data
 * structures, prepare a proper operational environment for the driver.
 */
int fpga_init(void)
{
    int     gpio_brd = IMX_GPIO_NR(FPGA_GPIO_BOARD_ID_BANK, FPGA_GPIO_BOARD_ID_IO);
    int     gpio_val;
    int     i;

    PDEBUG("Launch the kernel device driver..\n");

    gpio_val = gpio_get_value(gpio_brd);
    printk("Board ID: %d\n", gpio_val);


    for (i = 0; i < 2; i++) {
        mutex_init(&_fpga_ctx[i].mutex);
        /* organize device SPI access */
        _rc = _fpga_spi_add(&_fpga_ctx[i], i);
        if ( _rc ) {
            PDEBUG("Error! Can't organize device SPI access\n");
            goto _ffpga_init_fail;        
        }

        snprintf(_fpga_ctx[i].name, DEVMAN_KDRV_DEVICE_NAME_LEN, FPGA_DEVICE_NAME"%d", i);
        printk("devname: %s\n", _fpga_ctx[i].name);
        /* register device with devman */
        _rc = devman_device_register(_fpga_ctx[i].name, _fpga_dev_op, &_fpga_ctx[i]); 
        if ( _rc ) {
            PDEBUG("Error! Can't register FPGA with DEVMAN\n");
            goto _ffpga_init_fail;        
        }
    }
#if 0
    _fpga_ctx[0].hw_virq = gpio_to_irq(IMX_GPIO_NR(FPGA_GPIO_IRQ_BANK, FPGA_GPIO_IRQ_IO));

    /* setup hw dedicated line interrupt service routine */
    _rc = request_irq(_fpga_ctx[0].hw_virq, _fpga_hw_isr, 0, _fpga_ctx[0].name, (void *)&_fpga_ctx[0]);
    if ( _rc ) {
        PDEBUG("Error! Can't request irq %d for the FPGA hw line\n", _fpga_ctx[0].hw_virq);
        FPGA_ERROR_RETURN(-ENODEV);
    }

    PDEBUG("Kernel device driver is successfully initialized; irq: %d\n",
           _fpga_ctx[0].hw_virq);
#endif
    return 0;

    _ffpga_init_fail:
    _fpga_spi_cut(&_fpga_ctx[i]);
    mutex_destroy(&_fpga_ctx[i].mutex);
    return _rc;
}

/* Shut down the FPGA device
 */
void fpga_fini(void)
{
    int i;

    for (i = 0; i < 2; i++) {
        _fpga_spi_cut(&_fpga_ctx[i]);

        mutex_destroy(&_fpga_ctx[i].mutex);

        /* cleanup the control structure */
        memset(&_fpga_ctx[i], 0, sizeof(fpga_ctx_t));
    }
}

