/********************************************************************************/
/**
 * @file devman_mod.h
 * @brief Header file of the kernel device driver for the DEVMAN 
 * @copyright 2019 CELARE. All rights reserved.
 */
/********************************************************************************/

#ifndef _BATM_DEVMAN_MOD_H_
#define _BATM_DEVMAN_MOD_H_

/* Common textual names and descriptions definition */
#define DEVMAN_DEVICE_NAME         "devman"
#define DEVMAN_DEVICE_DESCR        "Device Manager"

#define DEVMAN_KDRV_DEVICES_MAXNUM   16
#define DEVMAN_KDRV_DEVICE_NAME_LEN  32

typedef struct tagt_devman_kdevice
{
    char    name[DEVMAN_KDRV_DEVICE_NAME_LEN];
    int     (*f_devop)(unsigned int cmd, void *v_parm, void *priv);
    void    *priv;
} devman_kdevice_t;

#define DEVMAN_MAX_NUM_OF_INODE    1

/* Character device major number for this kernel module */
#define DEVMAN_CHARDEV_MAJOR   214

/* magic number for device required by the kernel */
#define DEVMAN_IOC_MAGIC		'e'

/* parameters for ioctl system call and netlink messages */
#define DEVMAN_IOC_READR      _IOR(DEVMAN_IOC_MAGIC, 0,  int)	/* read register */
#define DEVMAN_IOC_WRITER     _IOR(DEVMAN_IOC_MAGIC, 1,  int)	/* write register */
#define DEVMAN_IOC_ANDR       _IOR(DEVMAN_IOC_MAGIC, 2,  int)	/* andmask */
#define DEVMAN_IOC_ORR        _IOR(DEVMAN_IOC_MAGIC, 3,  int)	/* ormask */
#define DEVMAN_IOC_CLRR       _IOR(DEVMAN_IOC_MAGIC, 4,  int)	/* clearmask */
#define DEVMAN_IOC_READBUF    _IOR(DEVMAN_IOC_MAGIC, 5,  int)	/* read buffer */
#define DEVMAN_IOC_WRITEBUF   _IOR(DEVMAN_IOC_MAGIC, 6,  int)	/* write buffer */
#define DEVMAN_IOC_EXEC       _IOR(DEVMAN_IOC_MAGIC, 7,  int)	/* perform procedure */

/* ioctl command parameter layout */
typedef struct tagt_devman_ioctl_parm 
{
    char            name[16];
    unsigned int    segment;
    unsigned int    page;    
    unsigned int    offset;
    unsigned int    value;
    char            *p;
    unsigned int    length;
} devman_ioctl_parm_t;

int devman_board_init(void);
void devman_board_fini(void);

int devman_device_register(char *name, int (* f_devop)(unsigned int cmd, void *v_parm, void *priv), void *priv);
void devman_device_unregister(char *name);

#endif/* _BATM_DEVMAN_MOD_H_ */
