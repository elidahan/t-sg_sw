/********************************************************************************/
/**
 * @file gpio.c
 * @brief GPIO GPIO kernel device driver 
 * @copyright 2019 CELARE. All rights reserved.
 */
/********************************************************************************/
#if !defined(__KERNEL__)
    #error This is a Linux kernel module and should NOT be compiled as user space program!
#endif

#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/ioport.h>
#include <linux/sched.h>
#include <linux/spinlock.h>
#include <linux/rwlock.h>
#include <linux/mutex.h>
#include <linux/io.h>
#include <linux/workqueue.h>
#include <linux/slab.h>
#include <linux/of.h>
#include <linux/delay.h>
#include <linux/spi/spi.h>
#include <linux/gpio.h>
#include <net/sock.h>

#include "devman_mod.h"
#include "spi_mod.h"

#define GPIO_DEBUG
/* printing/error-returning macros */
#define PRINTL(fmt, args...) printk(KERN_INFO"%s: " fmt, "GPIO", ## args)
#ifdef GPIO_DEBUG
    #define PDEBUG(fmt, args...) printk("GPIO-K: %s[%d]: " fmt, __FUNCTION__, __LINE__, ## args)
#else
    #define PDEBUG(fmt, args...)
#endif
#define GPIO_ERROR_RETURN(err) { \
   PRINTL("Error! %s[%d], error=%i\n", __FUNCTION__, __LINE__, (err)); \
   return err; \
}
#define PDEBUGG(fmt, args...)

/* There's an off-by-one between the gpio bank number and the gpiochip */
/* range e.g. GPIO_1_5 is gpio 5 under linux */
#define IMX_GPIO_NR(bank, nr)		(((bank) - 1) * 32 + (nr))


/* FPGA Programmer GPIO definition */
#define IMX_FPGA_GPIO_INIT_B_BANK         2
#define IMX_FPGA_GPIO_INIT_B_IO           0

#define IMX_FPGA_GPIO_DONE_BANK           2
#define IMX_FPGA_GPIO_DONE_IO             1

#define IMX_FPGA_GPIO_PROGRAM_B_BANK      2
#define IMX_FPGA_GPIO_PROGRAM_B_IO        2

#define IMX_FPGA_GPIO_SERIAL_OUT_BANK     2
#define IMX_FPGA_GPIO_SERIAL_OUT_IO       3

#define IMX_FPGA_GPIO_CCLK_BANK           2
#define IMX_FPGA_GPIO_CCLK_IO             4

#define IMX_GPIO_BASE_ADDRESS             0x30210000
#define IMX_GPIO_BANK_SIZE                0x10000
#define IMX_GPIO_BLOCK_SIZE               0x10000

#define IMX_GPIO_OUTPUT                   1
#define IMX_GPIO_INPUT                    0


/* the GPIO device context */
typedef struct tagt_gpio_ctx {
    char               name[DEVMAN_KDRV_DEVICE_NAME_LEN];
    struct mutex       mutex;  
} gpio_ctx_t;

/* gpio driver private context */
static gpio_ctx_t _gpio_ctx;

static int _rc;

static u8 _swap_bits_u8(u8 data)
{
    u8 swapped = ((data >> 7) & 0x01) |
                 ((data >> 5) & 0x02) |
                 ((data >> 3) & 0x04) |
                 ((data >> 1) & 0x08) |
                 ((data & 0x01) << 7) |
                 ((data & 0x02) << 5) |
                 ((data & 0x04) << 3) |
                 ((data & 0x08) << 1);
//    PDEBUG("data=0x%02x, swapped=0x%02x\n", data, swapped);
    return swapped;
}

static u32 _swap_u32(u32 data, u32 flag){
    u32 swapped;

    if (flag == 1)
        swapped = ((data << 24) & 0xFF000000) |
            ((data << 8) & 0x00FF0000) |
            ((data >> 8) & 0x0000FF00) |
            ((data >> 24) & 0x000000FF);
    else 
        swapped = data;

//    PDEBUG("data=0x%08x, swapped=0x%08x\n", data, swapped );
    return swapped;
}


static void _shift_cclk(int count)
{
    int i;

    for (i = 0; i < count; i++) {
        gpio_set_value(IMX_GPIO_NR(IMX_FPGA_GPIO_CCLK_BANK, IMX_FPGA_GPIO_CCLK_IO), 
                       1);
        udelay(2);
        gpio_set_value(IMX_GPIO_NR(IMX_FPGA_GPIO_CCLK_BANK, IMX_FPGA_GPIO_CCLK_IO), 
                       0);
        udelay(2);
    }
}

static void _shift_byte_out(u8 data)
{
    int i;
    u8 val;

    for (i = 7; i >= 0; --i) {
        val = !!(data & (1 << i));
        if (val) 
            gpio_set_value(IMX_GPIO_NR(IMX_FPGA_GPIO_SERIAL_OUT_BANK, IMX_FPGA_GPIO_SERIAL_OUT_IO), 
                           1);
        else 
            gpio_set_value(IMX_GPIO_NR(IMX_FPGA_GPIO_SERIAL_OUT_BANK, IMX_FPGA_GPIO_SERIAL_OUT_IO), 
                           0);
        _shift_cclk(1);
    }
}

static void _shift_word_out(unsigned int data)
{
    int i;
    u8 val;

    for (i = 31; i >= 0; --i) {
//        val = !!(data & (1 << i));
        val = (data & (1 << i)) ? 1 : 0;
        if (val) 
            gpio_set_value(IMX_GPIO_NR(IMX_FPGA_GPIO_SERIAL_OUT_BANK, IMX_FPGA_GPIO_SERIAL_OUT_IO), 
                           1);
        else 
            gpio_set_value(IMX_GPIO_NR(IMX_FPGA_GPIO_SERIAL_OUT_BANK, IMX_FPGA_GPIO_SERIAL_OUT_IO), 
                           0);
        _shift_cclk(1);
    }
}

/* GPIO write access operation
 */
static int _gpio_fpga_program(gpio_ctx_t *p_gpio, char page, 
                              unsigned int offset, char *buf, int size)
{
    int     gpio;
    int     i;
    int     step;

    /* check input parameters */
    if ( !p_gpio || !buf || !size) {
        PDEBUG("Error: Invalid input parameters, p_gpio=%p,page=0x%02x,offset=0x%x,p_value=%p,size=0x%x\n", 
               p_gpio, page, offset, buf, size);
        GPIO_ERROR_RETURN(-EINVAL);
    }
    PDEBUG("buf size=%d\n", size);

    _rc = gpio_direction_output(IMX_GPIO_NR(IMX_FPGA_GPIO_PROGRAM_B_BANK, IMX_FPGA_GPIO_PROGRAM_B_IO), 
                              0);
    _rc |= gpio_direction_output(IMX_GPIO_NR(IMX_FPGA_GPIO_SERIAL_OUT_BANK, IMX_FPGA_GPIO_SERIAL_OUT_IO), 
                              0);
    _rc |= gpio_direction_output(IMX_GPIO_NR(IMX_FPGA_GPIO_CCLK_BANK, IMX_FPGA_GPIO_CCLK_IO), 
                              0);
    if (_rc) {
        PDEBUG("Error! gpio_direction_output\n");
        return _rc;
    }
    gpio_set_value(IMX_GPIO_NR(IMX_FPGA_GPIO_PROGRAM_B_BANK, IMX_FPGA_GPIO_PROGRAM_B_IO), 
                   0);
    mdelay(5);
    gpio = gpio_get_value(IMX_GPIO_NR(IMX_FPGA_GPIO_INIT_B_BANK, IMX_FPGA_GPIO_INIT_B_IO));
    PDEBUG("INIT_B=%d (expected 0)\n", gpio);
    gpio_set_value(IMX_GPIO_NR(IMX_FPGA_GPIO_PROGRAM_B_BANK, IMX_FPGA_GPIO_PROGRAM_B_IO), 
                   1);

    i = 0;
    while (!(gpio = gpio_get_value(IMX_GPIO_NR(IMX_FPGA_GPIO_INIT_B_BANK, IMX_FPGA_GPIO_INIT_B_IO)))) {
        if (i++ > 1000000) {
          PDEBUG("INIT_B has not gone high: i=%d\n", i);
          GPIO_ERROR_RETURN(-EINVAL);
        }
    }
    PDEBUG("INIT_B=%d (expected 1)\n", gpio);

    step = 4;
    for (i = 0; i < size; i += step) {
        if (!(i % 100000)) {
            PDEBUG("i=%d\n", i);
        }
//        _shift_byte_out(_swap_bits_u8(*(u8 *)(buf + i)));
        _shift_word_out(_swap_u32(*(unsigned int *)(buf + i), 1));
    }

    if (!(gpio = gpio_get_value(IMX_GPIO_NR(IMX_FPGA_GPIO_INIT_B_BANK, IMX_FPGA_GPIO_INIT_B_IO)))) {
        PDEBUG("INIT_B low after programming error\n");
        GPIO_ERROR_RETURN(-EINVAL);
    }
    PDEBUG("INIT_B high after programming\n");
    i = 0;
    while (!(gpio = gpio_get_value(IMX_GPIO_NR(IMX_FPGA_GPIO_DONE_BANK, IMX_FPGA_GPIO_DONE_IO)))) {
        _shift_word_out(0xffffffff);
//        _shift_byte_out(0xff);
        i++;
        if (!(i % 100000)) {
          PDEBUG("DONE has not gone high. cnt=%d\n", i);
//          GPIO_ERROR_RETURN(-EINVAL);
        }
    }
    PDEBUG("DONE high after programming\n");
    _shift_cclk(8);

    return 0;
}

/* Performs the device operations
 */
static int _gpio_dev_op(unsigned int cmd, void *v_parm, void *priv)
{
    gpio_ctx_t          *p_gpio = (gpio_ctx_t *)priv;
    devman_ioctl_parm_t *p_parm;
    char                *buff;

    /* check input parameters */
    if ( !cmd || !v_parm ) {
        PDEBUG("Error! Invalid input parameters, cmd=0x%08x, parm=%p\n", cmd, v_parm);
        GPIO_ERROR_RETURN(-EINVAL);
    }

    /* get parameters */
    p_parm = (devman_ioctl_parm_t *)v_parm;
    /* perform the command */
    switch ( cmd ) {
    case DEVMAN_IOC_EXEC:
        buff = kmalloc(p_parm->length, GFP_KERNEL);
        if (!buff) {
            PDEBUG("Error! Can't allocate memory");
            GPIO_ERROR_RETURN(-EINVAL);
        }
        _rc = copy_from_user(buff, p_parm->p, p_parm->length);
        if ( _rc ) {
            PDEBUG("Error! Can't copy from user memory\n");
            break;
        }
        _rc = _gpio_fpga_program(p_gpio, (char)p_parm->page, p_parm->offset, buff, p_parm->length);
        if ( _rc ) {
            PDEBUG("Error! Can't write buffer, page=0x%02x, offset0x%02x, size=%d\n, value=0x%08x\n", 
                   p_parm->page, p_parm->offset, p_parm->length, *(u32 *)buff);
        }
        break;
    default:
        PDEBUG("Error! Can't recognize the operation, cmd=0x%08x, parm=%p\n", cmd, v_parm); 
        _rc = -EINVAL;
        break;
    }

    kfree(buff);

    return _rc;
}

/* Initialize the GPIO device, configure registers, driver data
 * structures, prepare a proper operational environment for the driver.
 */
int gpio_init(void)
{
    int gpio;

    snprintf(_gpio_ctx.name, DEVMAN_KDRV_DEVICE_NAME_LEN, "gpio");
    printk("devname: %s\n", _gpio_ctx.name);
    /* register device with devman */
    _rc = devman_device_register(_gpio_ctx.name, _gpio_dev_op, &_gpio_ctx); 
    if ( _rc ) {
        PDEBUG("Error! Can't register FPGA with DEVMAN\n");
        GPIO_ERROR_RETURN(-EINVAL);
    }
    _rc = gpio_request(IMX_GPIO_NR(IMX_FPGA_GPIO_INIT_B_BANK, IMX_FPGA_GPIO_INIT_B_IO), 
                 "gpio32");
    _rc |= gpio_request(IMX_GPIO_NR(IMX_FPGA_GPIO_DONE_BANK, IMX_FPGA_GPIO_DONE_IO), 
                 "gpio33");
    _rc |= gpio_request(IMX_GPIO_NR(IMX_FPGA_GPIO_PROGRAM_B_BANK, IMX_FPGA_GPIO_PROGRAM_B_IO), 
                 "gpio34");
    _rc |= gpio_request(IMX_GPIO_NR(IMX_FPGA_GPIO_SERIAL_OUT_BANK, IMX_FPGA_GPIO_SERIAL_OUT_IO), 
                 "gpio35");
    _rc |= gpio_request(IMX_GPIO_NR(IMX_FPGA_GPIO_CCLK_BANK, IMX_FPGA_GPIO_CCLK_IO), 
                 "gpio36");
    if (_rc) {
        PDEBUG("Error! gpio_request\n");
        return _rc;
    }

    _rc = gpio_direction_input(IMX_GPIO_NR(IMX_FPGA_GPIO_INIT_B_BANK, IMX_FPGA_GPIO_INIT_B_IO));
    _rc |= gpio_direction_input(IMX_GPIO_NR(IMX_FPGA_GPIO_DONE_BANK, IMX_FPGA_GPIO_DONE_IO));
    _rc |= gpio_direction_input(IMX_GPIO_NR(IMX_FPGA_GPIO_PROGRAM_B_BANK, IMX_FPGA_GPIO_PROGRAM_B_IO));
    _rc |= gpio_direction_input(IMX_GPIO_NR(IMX_FPGA_GPIO_SERIAL_OUT_BANK, IMX_FPGA_GPIO_SERIAL_OUT_IO));
    _rc |= gpio_direction_input(IMX_GPIO_NR(IMX_FPGA_GPIO_CCLK_BANK, IMX_FPGA_GPIO_CCLK_IO));
    if (_rc) {
        PDEBUG("Error! gpio_direction_input\n");
        return _rc;
    }

    gpio_export(IMX_GPIO_NR(IMX_FPGA_GPIO_INIT_B_BANK, IMX_FPGA_GPIO_INIT_B_IO),
                1);
    gpio_export(IMX_GPIO_NR(IMX_FPGA_GPIO_DONE_BANK, IMX_FPGA_GPIO_DONE_IO), 
                1);
    gpio_export(IMX_GPIO_NR(IMX_FPGA_GPIO_PROGRAM_B_BANK, IMX_FPGA_GPIO_PROGRAM_B_IO), 
                1);
    gpio_export(IMX_GPIO_NR(IMX_FPGA_GPIO_SERIAL_OUT_BANK, IMX_FPGA_GPIO_SERIAL_OUT_IO), 
                1);
    gpio_export(IMX_GPIO_NR(IMX_FPGA_GPIO_CCLK_BANK, IMX_FPGA_GPIO_CCLK_IO), 
                1);

    return 0;
}

/* Shut down the GPIO device
 */
void gpio_fini(void)
{  
    gpio_free(IMX_GPIO_NR(IMX_FPGA_GPIO_INIT_B_BANK, IMX_FPGA_GPIO_INIT_B_IO));
    gpio_free(IMX_GPIO_NR(IMX_FPGA_GPIO_DONE_BANK, IMX_FPGA_GPIO_DONE_IO));
    gpio_free(IMX_GPIO_NR(IMX_FPGA_GPIO_PROGRAM_B_BANK, IMX_FPGA_GPIO_PROGRAM_B_IO));
    gpio_free(IMX_GPIO_NR(IMX_FPGA_GPIO_SERIAL_OUT_BANK, IMX_FPGA_GPIO_SERIAL_OUT_IO));
    gpio_free(IMX_GPIO_NR(IMX_FPGA_GPIO_CCLK_BANK, IMX_FPGA_GPIO_CCLK_IO));
}

