/********************************************************************************/
/**
 * @file come_mode.c
 * @brief Init/Deinit of COME board specific devices 
 * @copyright 2019 CELARE. All rights reserved.
 */
/********************************************************************************/

#if !defined(__KERNEL__)
#error This is a Linux kernel module and should NOT be compiled as user space program!
#endif

#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/ioport.h>
#include <linux/sched.h>
#include <linux/spinlock.h>
#include <linux/rwlock.h>
#include <linux/mutex.h>
#include <linux/io.h>
#include <linux/workqueue.h>
#include <linux/slab.h>
#include <linux/of.h>
#include <linux/delay.h>
#include <net/sock.h>

#include "come_mod.h"
#include "devman_mod.h"
#include "spi_mod.h"

//#define COME_DEBUG
/* printing/error-returning macros */
#define PRINTL(fmt, args...) printk(KERN_INFO"%s: " fmt, "come", ## args)
#ifdef COME_DEBUG
 #define PDEBUG(fmt, args...) printk(KERN_ERR"come: %s(): " fmt, __FUNCTION__, ## args)
#else
 #define PDEBUG(fmt, args...)
#endif
#define COME_ERROR_RETURN(err)                            \
{                                                           \
   PDEBUG("Error! line=%i, error=%i\n", __LINE__, (err));   \
   return err;                                              \
}
#define PDEBUGG(fmt, args...)


static int _rc;


/*==============================================================================
 * Name:   devman_board_init
 * 
 * Descr:  Initialize the TSG board specific stuff
 *----------------------------------------------------------------------------*/
int devman_board_init(void)
{
    PDEBUG("before init\n");
    _rc = come_init();
    if ( _rc ) {
        PDEBUG("Error! Can't create COME device!\n");
        COME_ERROR_RETURN(_rc);
    }
    _rc = flash_init();
    if ( _rc ) {
        PDEBUG("Error! Can't create FLASH device!\n");
        COME_ERROR_RETURN(_rc);
    }

    PDEBUG("after init\n");
	return 0;
}

/*==============================================================================
 * Name:            devman_board_fini
 * Description:     Shut down the board specific stuff
 *----------------------------------------------------------------------------*/
void devman_board_fini(void)
{       
    return;
}


