#ifndef _COME_MOD_H_
#define _COME_MOD_H_

/* Common textual names and descriptions definition */
#define COME_DEVICE_NAME  		"come"
#define COME_DEVICE_DESCR		"COME"
#define COME_DRIVER_NAME  		"come_driver"
#define COME_DRIVER_DESCR		"COME Device Driver"

/* IRQ definitions*/
#define COME_GPIO_BOARD_ID_BANK 4
#define COME_GPIO_BOARD_ID_IO   6


int come_init(void);
void come_fini(void);

#endif/* _COME_MOD_H_ */
