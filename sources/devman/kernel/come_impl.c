/********************************************************************************/
/**
 * @file come_spi.c
 * @brief FPGA SPI kernel device driver 
 * @copyright 2019 CELARE. All rights reserved.
 */
/********************************************************************************/
#if !defined(__KERNEL__)
    #error This is a Linux kernel module and should NOT be compiled as user space program!
#endif

#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/ioport.h>
#include <linux/sched.h>
#include <linux/spinlock.h>
#include <linux/rwlock.h>
#include <linux/mutex.h>
#include <linux/io.h>
#include <linux/workqueue.h>
#include <linux/slab.h>
#include <linux/of.h>
#include <linux/delay.h>
#include <linux/spi/spi.h>
#include <linux/gpio.h>
#include <net/sock.h>

#include "devman_mod.h"
#include "come_mod.h"

//#define COME_IMPL_DEBUG
/* printing/error-returning macros */
#define PRINTL(fmt, args...) printk(KERN_INFO"%s: " fmt, COME_DEVICE_NAME, ## args)
#ifdef COME_IMPL_DEBUG
    #define PDEBUG(fmt, args...) printk("FPGA-K: %s[%d]: " fmt, __FUNCTION__, __LINE__, ## args)
#else
    #define PDEBUG(fmt, args...)
#endif
#define COME_IMPL_ERROR_RETURN(err) { \
   PRINTL("Error! %s[%d], error=%i\n", __FUNCTION__, __LINE__, (err)); \
   return err; \
}
#define PDEBUGG(fmt, args...)

/* the FPGA device context */
typedef struct tagt_come_ctx {
    char               name[DEVMAN_KDRV_DEVICE_NAME_LEN];
    struct mutex       mutex;  
    int                hw_virq;        /* hw virtual irq number */
} come_ctx_t;

/* come driver private context */
static come_ctx_t _come_ctx;

static int _rc;

/* Performs the device operations
 */
static int _come_dev_op(unsigned int cmd, void *v_parm, void *priv)
{
    come_ctx_t              *p_come = (come_ctx_t *)priv;
    devman_ioctl_parm_t     *p_parm;
    char                     buff[64];

    /* check input parameters */
    if ( !cmd || !v_parm ) {
        PDEBUG("Error! Invalid input parameters, cmd=0x%08x, parm=%p\n", cmd, v_parm);
        COME_IMPL_ERROR_RETURN(-EINVAL);
    }

    /* get parameters */
    p_parm = (devman_ioctl_parm_t *)v_parm;
    PDEBUG("line: %d\n", __LINE__);
    /* perform the command */
    switch ( cmd ) {
    /* read register value */
    case DEVMAN_IOC_READR:
        PDEBUG("line: %d\n", __LINE__);
        memset(buff, 0, p_parm->length);
        _rc = copy_to_user(p_parm->p, buff, p_parm->length);
        if ( _rc ) {
            PDEBUG("Error! Can't copy to user memory\n");
        }
        break;
        /* write register value */
    case DEVMAN_IOC_WRITER:
        _rc = copy_from_user(buff, p_parm->p, p_parm->length);
        if ( _rc ) {
            PDEBUG("Error! Can't copy from user memory\n");
            break;
        }
        break;
    default:
        PDEBUG("Error! Can't recognize the operation, cmd=0x%08x, parm=%p\n", cmd, v_parm); 
        _rc = -EINVAL;
        break;
    }
    return _rc;
}

/* Initialize the FPGA device, configure registers, driver data
 * structures, prepare a proper operational environment for the driver.
 */
int come_init(void)
{
    PDEBUG("Launch the kernel device driver..\n");

    strncpy(_come_ctx.name, COME_DEVICE_NAME, DEVMAN_KDRV_DEVICE_NAME_LEN);
    printk("devname: %s\n", _come_ctx.name);
    /* register device with devman */
    _rc = devman_device_register(_come_ctx.name, _come_dev_op, &_come_ctx); 
    if ( _rc ) {
        PDEBUG("Error! Can't register COME with DEVMAN\n");
        return _rc;
    }

    return 0;
}

/* Shut down the FPGA device
 */
void come_fini(void)
{
    mutex_destroy(&_come_ctx.mutex);

    /* cleanup the control structure */
    memset(&_come_ctx, 0, sizeof(come_ctx_t));
}

