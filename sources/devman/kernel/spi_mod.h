#ifndef _SPI_MOD_H_
#define _SPI_MOD_H_

/* Common textual names and descriptions definition */
#define FPGA_DEVICE_NAME  		"fpga"
#define FPGA_DEVICE_DESCR		"FPGA"
#define FPGA_DRIVER_NAME  		"fpga_driver"
#define FPGA_DRIVER_DESCR		"FPGA Device Driver"

#define FLASH_DEVICE_NAME  		"flash"
#define FLASH_DEVICE_DESCR		"FLASH"
#define FLASH_DRIVER_NAME  		"flash_driver"
#define FLASH_DRIVER_DESCR		"FLASH Device Driver"

/* IRQ definitions*/
#define FPGA_GPIO_IRQ_BANK      4
#define FPGA_GPIO_IRQ_IO        1

/* FPGA SPI controller GPIO definition */
#define FPGA_GPIO_SPI_BANK      2
#define FPGA_GPIO_SPI_IO        7

/* FPGA SPI controller GPIO definition */
#define FPGA_GPIO_BOARD_ID_BANK 4
#define FPGA_GPIO_BOARD_ID_IO   6


/*123$*/
#define FPGA_SPI_BUS            2
#define FPGA_SPI_CS             0
#define FPGA_SPI_MAX_SPEED      16500000 //3000000
#define FPGA_SPI_MODE           SPI_MODE_0
#define FPGA_SPI_BITS_P_WORD    8
#define FPGA_SPI_RBIT           0x80
#define FPGA_SPI_SFR_ADDR       0x0
#define FPGA_SPI_CMD_RD_8       0x0
#define FPGA_SPI_CMD_RD_16      0x1
#define FPGA_SPI_CMD_RD_32      0x2
#define FPGA_SPI_CMD_RD_SFR     0x3
#define FPGA_SPI_CMD_RD_BURST   0x4
#define FPGA_SPI_CMD_WR_8       0x8
#define FPGA_SPI_CMD_WR_16      0x9
#define FPGA_SPI_CMD_WR_32      0xa
#define FPGA_SPI_CMD_WR_SFR     0xb
#define FPGA_SPI_CMD_WR_BURST   0xc
#define FPGA_SPI_CMD_MASK       0x0f
#define FPGA_SPI_CMD_INVALID    0xffffffff



#define FPGA_SPI_CMD_FREAD      0x10
#define FPGA_SPI_CMD_READ       0x60
#define FPGA_SPI_CMD_WRITE      0x61
#define FPGA_SPI_DATAIO_REG     0xf0
#define FPGA_SPI_STATUS_REG     0xfe
#define FPGA_SPI_PAGE_REG       0xff
#define FPGA_SPI_RACK_ON_BUS    0x01
#define FPGA_SPI_RACK_PREAM_LEN 0

#define FPGA_SPI_MAX_BURST_LEN       2048
#define FPGA_SPI_MAX_BURST_CMD_LEN   FPGA_SPI_MAX_BURST_LEN + 2 + 4 //FPGA_SPI_MAX_BURST_LEN+2Bytes(opcode+burst len)+4Bytes(addr)


#define FLASH_SPI_BUS            0
#define FLASH_SPI_CS             0
#define FLASH_SPI_MAX_SPEED      24000000
#define FLASH_SPI_MODE           SPI_MODE_0
#define FLASH_SPI_BITS_P_WORD    8
#define FLASH_SPI_RBIT           0x80
#define FLASH_SPI_CMD_FREAD      0x10
#define FLASH_SPI_CMD_READ       0x60
#define FLASH_SPI_CMD_WRITE      0x61
#define FLASH_SPI_DATAIO_REG     0xf0
#define FLASH_SPI_STATUS_REG     0xfe
#define FLASH_SPI_PAGE_REG       0xff
#define FLASH_SPI_RACK_ON_BUS    0x01
#define FLASH_SPI_RACK_PREAM_LEN 0


int fpga_init(void);
void fpga_fini(void);

int flash_init(void);
void flash_fini(void);

#endif/* _SPI_MOD_H_ */
