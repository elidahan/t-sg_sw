/********************************************************************************/
/**
 * @file flash_spi.c
 * @brief FLASH SPI kernel device driver 
 * @copyright 2019 CELARE. All rights reserved.
 */
/********************************************************************************/
#if !defined(__KERNEL__)
    #error This is a Linux kernel module and should NOT be compiled as user space program!
#endif

#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/ioport.h>
#include <linux/sched.h>
#include <linux/spinlock.h>
#include <linux/rwlock.h>
#include <linux/mutex.h>
#include <linux/io.h>
#include <linux/workqueue.h>
#include <linux/slab.h>
#include <linux/of.h>
#include <linux/delay.h>
#include <linux/spi/spi.h>
#include <linux/gpio.h>
#include <net/sock.h>

#include "devman_mod.h"
#include "spi_mod.h"

#define FLASH_DEBUG
/* printing/error-returning macros */
#define PRINTL(fmt, args...) printk(KERN_INFO"%s: " fmt, FLASH_DEVICE_NAME, ## args)
#ifdef FLASH_DEBUG
    #define PDEBUG(fmt, args...) printk("FLASH-K: %s[%d]: " fmt, __FUNCTION__, __LINE__, ## args)
#else
    #define PDEBUG(fmt, args...)
#endif
#define FLASH_ERROR_RETURN(err) { \
   PRINTL("Error! %s[%d], error=%i\n", __FUNCTION__, __LINE__, (err)); \
   return err; \
}
#define PDEBUGG(fmt, args...)

/* There's an off-by-one between the gpio bank number and the gpiochip */
/* range e.g. GPIO_1_5 is gpio 5 under linux */
#define IMX_GPIO_NR(bank, nr)		(((bank) - 1) * 32 + (nr))

/* the FLASH device context */
typedef struct tagt_flash_ctx {
    char               name[DEVMAN_KDRV_DEVICE_NAME_LEN];
    struct mutex       mutex;  
    struct spi_device  *p_spidev;
    int                hw_virq;        /* hw virtual irq number */

} flash_ctx_t;

/* flash driver private context */
static flash_ctx_t _flash_ctx;

static int _rc;


/* FLASH write access operation
 */
static int _flash_spi_reg_write(flash_ctx_t *p_flash, char page, char offset, char *p_value, int size)
{
    char    spi_tx_buff[16];
    int     i;

    /* check input parameters */
    if ( !p_flash || !p_value || !size ) {
        PDEBUG("Error: Invalid input parameters, p_flash=%p,page=0x%02x,offset=0x%x,p_value=%p,size=0x%x\n", 
               p_flash, page, offset, p_value, size);
        FLASH_ERROR_RETURN(-EINVAL);
    }
    PDEBUG("123$=> page=0x%02x,offset=0x%02x,value=0x%08x,size=%d\n", page, offset, *(u32 *)p_value, size);

    memset(spi_tx_buff, 0, sizeof(spi_tx_buff));
    mutex_lock(&p_flash->mutex);
    /* setup the page register */
    spi_tx_buff[0] = FLASH_SPI_CMD_WRITE;
    spi_tx_buff[1] = FLASH_SPI_PAGE_REG;
    spi_tx_buff[2] = page;
    _rc = spi_write_then_read(p_flash->p_spidev, spi_tx_buff, 3, NULL, 0);
    if ( _rc ) {
        PDEBUG("ERROR: Can't set flash page register!, page=0x%02x\n", page);
        goto flash_spi_error;
    }

    /* prepare the in-page register offset */
    spi_tx_buff[0] = FLASH_SPI_CMD_WRITE;
    spi_tx_buff[1] = offset;

    /* prepare the write value, then write down */
    /* starting from the lowest byte */
    for ( i = 0; i < size; i++ )
        spi_tx_buff[2+i] = p_value[size-i-1];

    _rc = spi_write_then_read(p_flash->p_spidev, spi_tx_buff, 2+i, NULL, 0);
    if ( _rc ) {
        PDEBUG("Error! Can't write the data, page=0x%02x,offset=0x%02x,value=0x%08x\n", page, offset, *(int *)p_value);
        goto flash_spi_error;
    }

    mutex_unlock(&p_flash->mutex);

    return 0;

flash_spi_error:
    mutex_unlock(&p_flash->mutex);
    FLASH_ERROR_RETURN(-EINVAL);
}

/* FLASH read access operation
 */
static int _flash_spi_reg_read(flash_ctx_t *p_flash, char page, char offset, char *p_value, int size)
{
    char    spi_tx_buff[16], spi_rx_buff[8*8 + FLASH_SPI_RACK_PREAM_LEN];
    int     i, j = 0;

    /* check input parameters */
    if ( !p_flash || !p_value || !size ) {
        PDEBUG("Error: Invalid input parameters, p_flash=%p,page=0x%02x,offset=0x%x,p_value=%p,size=0x%x\n", 
               p_flash, page, offset, p_value, size);
        FLASH_ERROR_RETURN(-EINVAL);
    }
    PDEBUG("123$=> page=0x%02x,offset=0x%02x,size=%d\n", page, offset, size);

    /* dirty the input buffer */
    memset(spi_rx_buff, 0xff, sizeof(spi_rx_buff));
    mutex_lock(&p_flash->mutex);

#if 0
    /* setup the page register */
    spi_tx_buff[0] = FLASH_SPI_CMD_WRITE;
    spi_tx_buff[1] = FLASH_SPI_PAGE_REG;
    spi_tx_buff[2] = page;
    _rc = spi_write_then_read(p_flash->p_spidev, spi_tx_buff, 3, NULL, 0);
    if ( _rc ) {
        PDEBUG("ERROR: Can't set flash page register!, page=0x%02x\n", page);
        FLASH_ERROR_RETURN(_rc);
    }
#endif
    PDEBUG("line: %d\n", __LINE__);

    /* setup the inpage register offset */
//    spi_tx_buff[0] = FLASH_SPI_CMD_FREAD;
    spi_tx_buff[0] = offset;
    /* RACK in the first received bytes */
    _rc = spi_write_then_read(p_flash->p_spidev, spi_tx_buff, 1, spi_rx_buff, size + 1);
    if ( _rc < 0) {
        PDEBUG("Error! Can't setup offset/read the register, page=0x%02x,offset=0x%02x\n", page, offset);
        goto flash_spi_error;
    }
    PDEBUG("123$=> rx: rbuff[0]=0x%02x,rbuff[1]=0x%02x,rbuff[2]=0x%02x,rbuff[3]=0x%02x,rbuff[4]=0x%02x,rbuff[5]=0x%02x,rbuff[6]=0x%02x,rbuff[7]=0x%02x\n", 
            spi_rx_buff[0],spi_rx_buff[1],spi_rx_buff[2],spi_rx_buff[3],spi_rx_buff[4],spi_rx_buff[5], spi_rx_buff[6], spi_rx_buff[7]);
    /* starting from the lowest byte, after RACK byte */
    for ( i = size - 1; i >= 0; i-- )
        p_value[i] = spi_rx_buff[j++];

    PDEBUG("123$=> got register: page=0x%02x,offset=0x%02x,size=%d,value=0x%x\n", page, offset, size, *(int *)p_value);

    mutex_unlock(&p_flash->mutex);

    return 0;

flash_spi_error:
    mutex_unlock(&p_flash->mutex);
    FLASH_ERROR_RETURN(-EINVAL);
}

/* Performs the device operations
 */
static int _flash_dev_op(unsigned int cmd, void *v_parm, void *priv)
{
    flash_ctx_t          *p_flash = (flash_ctx_t *)priv;
    devman_ioctl_parm_t     *p_parm;
    char                     buff[64];

    /* check input parameters */
    if ( !cmd || !v_parm ) {
        PDEBUG("Error! Invalid input parameters, cmd=0x%08x, parm=%p\n", cmd, v_parm);
        FLASH_ERROR_RETURN(-EINVAL);
    }

    /* get parameters */
    p_parm = (devman_ioctl_parm_t *)v_parm;
    PDEBUG("line: %d\n", __LINE__);
    /* perform the command */
    switch ( cmd ) {
    /* read register value */
    case DEVMAN_IOC_READR:
        PDEBUG("line: %d\n", __LINE__);
        memset(buff, 0, p_parm->length);
        _rc = _flash_spi_reg_read(p_flash, (char)p_parm->page, (char)p_parm->offset, buff/*123$; p_parm->p*/, p_parm->length);
        if ( _rc ) {
            PDEBUG("Error! Can't read register, page=0x%02x, offset0x%02x, size=%d\n", p_parm->page, p_parm->offset, p_parm->length);
            break;
        }
        PDEBUG("line: %d\n", __LINE__);
        _rc = copy_to_user(p_parm->p, buff, p_parm->length);
        if ( _rc ) {
            PDEBUG("Error! Can't copy to user memory\n");
        }
        break;
        /* write register value */
    case DEVMAN_IOC_WRITER:
        _rc = copy_from_user(buff, p_parm->p, p_parm->length);
        if ( _rc ) {
            PDEBUG("Error! Can't copy from user memory\n");
            break;
        }
        _rc = _flash_spi_reg_write(p_flash, (char)p_parm->page, (char)p_parm->offset, buff, p_parm->length);
        if ( _rc ) {
            PDEBUG("Error! Can't write register, page=0x%02x, offset0x%02x, size=%d\n, value=0x%08x\n", 
                   p_parm->page, p_parm->offset, p_parm->length, *(u32 *)buff);
        }
        break;
    default:
        PDEBUG("Error! Can't recognize the operation, cmd=0x%08x, parm=%p\n", cmd, v_parm); 
        _rc = -EINVAL;
        break;
    }
    return _rc;
}

/*==============================================================================
 * Name:   _flash_spi_add
 * 
 * Descr:  Initialize the FLASH SPI access
 *----------------------------------------------------------------------------*/
static int _flash_spi_add(flash_ctx_t *p_flash)
{
    struct spi_master  *spi_master;
    struct spi_device  *spi_device;
    struct device      *pdev;
    char                buff[64];

    /*123$; chipa*/

    spi_master = (struct spi_master *)spi_busnum_to_master(FLASH_SPI_BUS);        
    if ( !spi_master ) {
        PDEBUG("Unable to get spi bus master! bus=%d\n", FLASH_SPI_BUS);
        FLASH_ERROR_RETURN(-EINVAL);
    }
    spi_device = spi_alloc_device(spi_master);
    if ( !spi_device ) {
        PDEBUG("Can't allocate spi device! bus=%d\n", FLASH_SPI_BUS);
        FLASH_ERROR_RETURN(-EINVAL);
    }
    /* specify the chip select */
    spi_device->chip_select = FLASH_SPI_CS;

    /* check if the cs is already claimed */
    snprintf(buff, sizeof(buff),"%s.%u",dev_name(&spi_device->master->dev), spi_device->chip_select);
    PDEBUG("buff=%s\n",buff);
    pdev = bus_find_device_by_name(spi_device->dev.bus, NULL, buff);
    if ( pdev ) {
        /* not going to use this spi_device, so free it */
        spi_dev_put(spi_device);

        /* there is already a device configured for this bus.cs, 
         forex if we previously loaded and unloaded the driver;
         if it is not us, we stop with fail */
        if ( pdev->driver && pdev->driver->name && 
             strncmp(FLASH_DEVICE_NAME, pdev->driver->name, 
                     strnlen(FLASH_DEVICE_NAME, sizeof(FLASH_DEVICE_NAME))) ) {
            PDEBUG("Driver [%s] already registered for %s\n", pdev->driver->name, buff);
            FLASH_ERROR_RETURN(-EINVAL);
        }
    } else {
        spi_device->max_speed_hz = FLASH_SPI_MAX_SPEED;
        spi_device->mode = FLASH_SPI_MODE;
        spi_device->bits_per_word = FLASH_SPI_BITS_P_WORD;
        spi_device->irq = -1;
        spi_device->controller_state = NULL;
        spi_device->controller_data = NULL;
        strlcpy(spi_device->modalias, FLASH_DEVICE_NAME, SPI_NAME_SIZE);
        _rc = spi_add_device(spi_device);
        if (_rc < 0) {
            spi_dev_put(spi_device);
            PDEBUG("spi_add_device() failed!\n");
            FLASH_ERROR_RETURN(_rc);
        }
        PDEBUG("%d.%d, max_speed_hz=%lu\n",
               FLASH_SPI_BUS, FLASH_SPI_CS, spi_device->max_speed_hz);
        put_device(&spi_master->dev);        
    }

    /* back reference to spi device */
    p_flash->p_spidev = spi_device;

    return 0;
}

/*==============================================================================
 * Name:   _flash_spi_cut
 * 
 * Descr:  Deinitialize the FLASH SPI access
 *----------------------------------------------------------------------------*/
static void _flash_spi_cut(flash_ctx_t *p_flash)
{
    /*123$; chipa*/
    if ( p_flash->p_spidev )
        spi_unregister_device(p_flash->p_spidev);
}

/* Initialize the FLASH device, configure registers, driver data
 * structures, prepare a proper operational environment for the driver.
 */
int flash_init(void)
{
    PDEBUG("Launch the kernel device driver..\n");

    mutex_init(&_flash_ctx.mutex);

    /* organize device SPI access */
    _rc = _flash_spi_add(&_flash_ctx);
    if ( _rc ) {
        PDEBUG("Error! Can't organize device SPI access\n");
        goto _fflash_init_fail;        
    }

    strncpy(_flash_ctx.name, FLASH_DEVICE_NAME, DEVMAN_KDRV_DEVICE_NAME_LEN);

    /* register device with devman */
    _rc = devman_device_register(FLASH_DEVICE_NAME, _flash_dev_op, &_flash_ctx); 
    if ( _rc ) {
        PDEBUG("Error! Can't register FLASH with DEVMAN\n");
        goto _fflash_init_fail;        
    }

    PDEBUG("Kernel device driver is successfully initialized; irq: %d\n",
           _flash_ctx.hw_virq);

    return 0;

    _fflash_init_fail:
    _flash_spi_cut(&_flash_ctx);
    mutex_destroy(&_flash_ctx.mutex);
    return _rc;
}

/* Shut down the FLASH device
 */
void flash_fini(void)
{
    _flash_spi_cut(&_flash_ctx);

    mutex_destroy(&_flash_ctx.mutex);

    /* cleanup the control structure */
    memset(&_flash_ctx, 0, sizeof(flash_ctx_t));
}

