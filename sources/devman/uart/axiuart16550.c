/********************************************************************************/
/**
 * @file axiuart16550.c
 * @brief AXI Universal Asynchronous Receiver Transmitter 16550 (AXI UART 16550) device manager driver
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/

#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <semaphore.h>
#include <linux/limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "spibus.h"
#include "axiuart16550.h"

#define AXIUART16550_DEBUG
/* printing/error-returning macros */
#ifdef AXIUART16550_DEBUG
    #define PDEBUG(fmt, args...) \
            ccl_syslog_err("AXIUART16550_DEBUG:  %s[%d]: " fmt, __FUNCTION__, __LINE__, ## args)
#else
    #define PDEBUG(fmt, args...)
#endif
#define AXIUART16550_ERROR_RETURN(err) do { \
   ccl_syslog_err("Error! %s[%d], error=%i\n", __FUNCTION__, __LINE__, (err)); \
   return err; \
} while (0)

#define AXIUART16550_REG_READ(context, offset, offset_sz, idx, pvalue, size) do { \
    _ret = _axiuart_reg_read((context), \
                             (offset), \
                             (offset_sz), (idx), \
                             (pvalue), (size)); \
    if ( _ret ) { \
        PDEBUG("_axiuart_reg_read error\n"); \
        AXIUART16550_ERROR_RETURN(_ret); \
    } \
} while (0)

#define AXIUART16550_REG_WRITE(context, offset, offset_sz, idx, pvalue, size) do { \
    _ret = _axiuart_reg_write((context), \
                              (offset), \
                              (offset_sz), (idx), \
                              (pvalue), (size)); \
    if ( _ret ) { \
        PDEBUG("_axiuart_reg_write error\n"); \
        AXIUART16550_ERROR_RETURN(_ret); \
    } \
} while (0)


/* AXIUART16550 registers offsets */
#define AXIUART16550_REG_OFFSET       0x1000

#define AXIUART16550_RBR              (AXIUART16550_REG_OFFSET)        /**< Receiver Buffer register */
#define AXIUART16550_THR              (AXIUART16550_REG_OFFSET)        /**< Transmitter Holding register */
#define AXIUART16550_DLL              (AXIUART16550_REG_OFFSET)        /**< Divisor Latch (LSB) register */
#define AXIUART16550_DLM              (AXIUART16550_REG_OFFSET + 0x04) /**< Divisor Latch (MSB) register */
#define AXIUART16550_IER              (AXIUART16550_REG_OFFSET + 0x04) /**< Interrupt Enable register */
#define AXIUART16550_IIR              (AXIUART16550_REG_OFFSET + 0x08) /**< Interrupt Identification register */
#define AXIUART16550_FCR              (AXIUART16550_REG_OFFSET + 0x08) /**< FIFO Control register */
#define AXIUART16550_SCR              (AXIUART16550_REG_OFFSET + 0x0C) /**< Scratch register */
#define AXIUART16550_LCR              (AXIUART16550_REG_OFFSET + 0x0C) /**< Line Control register */
#define AXIUART16550_MCR              (AXIUART16550_REG_OFFSET + 0x10) /**< Modem Control register */
#define AXIUART16550_LSR              (AXIUART16550_REG_OFFSET + 0x14) /**< Line Status register */
#define AXIUART16550_MSR              (AXIUART16550_REG_OFFSET + 0x18) /**< Modem Status register */

#define AXIUART16550_MAX_OFFSET AXIUART16550_MSR

/*
 * The following constant specifies the size of the FIFOs, the size of the
 * FIFOs includes the transmitter and receiver such that it is the total number
 * of bytes that the UART can buffer
 */
#define AXIUART16550_FIFO_SIZE			16


/**
 * @name Interrupt Enable Register (IER) mask(s)
 * @{
 */
#define AXIUART16550_IER_MODEM_STATUS	0x00000008 /**< Modem status interrupt */
#define AXIUART16550_IER_RX_LINE		0x00000004 /**< Receive status interrupt */
#define AXIUART16550_IER_TX_EMPTY	    0x00000002 /**< Transmitter empty interrupt */
#define AXIUART16550_IER_RX_DATA		0x00000001 /**< Receiver data available */

#define AXIUART16550_IER_ALL            (AXIUART16550_IER_MODEM_STATUS | \
                                         AXIUART16550_IER_RX_LINE | \
                                         AXIUART16550_IER_TX_EMPTY | \
                                         AXIUART16550_IER_RX_DATA)
/* @} */

/**
 * @name Interrupt ID Register (INT_ID) mask(s)
 * @{
 */
#define AXIUART16550_INT_ID_MASK		  0x0000000F /**< Only the interrupt ID */
#define AXIUART16550_INT_ID_FIFOS_ENABLED 0x000000C0 /**< Only the FIFOs enable */
/* @} */

/**
 * @name FIFO Control Register mask(s)
 * @{
 */
#define AXIUART16550_FIFO_RX_TRIG_MSB	0x00000080 /**< Trigger level MSB */
#define AXIUART16550_FIFO_RX_TRIG_LSB	0x00000040 /**< Trigger level LSB */
#define AXIUART16550_FIFO_TX_RESET	    0x00000004 /**< Reset the transmit FIFO */
#define AXIUART16550_FIFO_RX_RESET	    0x00000002 /**< Reset the receive FIFO */
#define AXIUART16550_FIFO_ENABLE		0x00000001 /**< Enable the FIFOs */
#define AXIUART16550_FIFO_RX_TRIGGER	0x000000C0 /**< Both trigger level bits */
/* @} */

/**
 * @name Line Control Register(LCR) mask(s)
 * @{
 */
#define AXIUART16550_LCR_DLAB		    0x00000080 /**< Divisor latch access */
#define AXIUART16550_LCR_SET_BREAK	    0x00000040 /**< Cause a break condition */
#define AXIUART16550_LCR_STICK_PARITY	0x00000020 /**< Stick Parity */
#define AXIUART16550_LCR_EVEN_PARITY	0x00000010 /**< 1 = even, 0 = odd parity */
#define AXIUART16550_LCR_ENABLE_PARITY	0x00000008 /**< 1 = Enable, 0 = Disable parity*/
#define AXIUART16550_LCR_2_STOP_BITS	0x00000004 /**< 1= 2 stop bits,0 = 1 stop bit */
#define AXIUART16550_LCR_8_DATA_BITS	0x00000003 /**< 8 Data bits selection */
#define AXIUART16550_LCR_7_DATA_BITS	0x00000002 /**< 7 Data bits selection */
#define AXIUART16550_LCR_6_DATA_BITS	0x00000001 /**< 6 Data bits selection */
#define AXIUART16550_LCR_LENGTH_MASK	0x00000003 /**< Both length bits mask */
#define AXIUART16550_LCR_PARITY_MASK	0x00000018 /**< Both parity bits mask */
/* @} */

/**
 * @name Mode Control Register(MCR) mask(s)
 * @{
 */
#define AXIUART16550_MCR_LOOP		0x00000010 /**< Local loopback */
#define AXIUART16550_MCR_OUT_2		0x00000008 /**< General output 2 signal */
#define AXIUART16550_MCR_OUT_1		0x00000004 /**< General output 1 signal */
#define AXIUART16550_MCR_RTS		0x00000002 /**< RTS signal */
#define AXIUART16550_MCR_DTR		0x00000001 /**< DTR signal */
/* @} */

/**
 * @name Line Status Register(LSR) mask(s)
 * @{
 */
#define AXIUART16550_LSR_RX_FIFO_ERROR	 0x00000080 /**< An errored byte is in FIFO */
#define AXIUART16550_LSR_TX_EMPTY	     0x00000040 /**< Transmitter is empty */
#define AXIUART16550_LSR_TX_BUFFER_EMPTY 0x00000020 /**< Transmit holding reg empty */
#define AXIUART16550_LSR_BREAK_INT	     0x00000010 /**< Break detected interrupt */
#define AXIUART16550_LSR_FRAMING_ERROR	 0x00000008 /**< Framing error on current byte */
#define AXIUART16550_LSR_PARITY_ERROR	 0x00000004 /**< Parity error on current byte */
#define AXIUART16550_LSR_OVERRUN_ERROR	 0x00000002 /**< Overrun error on receive FIFO */
#define AXIUART16550_LSR_DATA_READY	     0x00000001 /**< Receive data ready */
#define AXIUART16550_LSR_ERROR_BREAK	 0x0000001E /**< Errors except FIFO error and break detected */
/* @} */

#define AXIUART16550_DIVISOR_BYTE_MASK	0x000000FF

/** @name Configuration options
 * @{
 */
/**
 * These constants specify the options that may be set or retrieved
 * with the driver, each is a unique bit mask such that multiple options
 * may be specified.  These constants indicate the function of the option
 * when in the active state.
 */
#define AXIUART16550_OPTION_RXLINE_INTR		0x0800 /**< Enable status interrupt */
#define AXIUART16550_OPTION_SET_BREAK		0x0400 /**< Set a break condition */
#define AXIUART16550_OPTION_LOOPBACK		0x0200 /**< Enable local loopback */
#define AXIUART16550_OPTION_DATA_INTR		0x0100 /**< Enable data interrupts */
#define AXIUART16550_OPTION_MODEM_INTR		0x0080 /**< Enable modem interrupts */
#define AXIUART16550_OPTION_FIFOS_ENABLE	0x0040 /**< Enable FIFOs */
#define AXIUART16550_OPTION_RESET_TX_FIFO	0x0020 /**< Reset the transmit FIFO */
#define AXIUART16550_OPTION_RESET_RX_FIFO	0x0010 /**< Reset the receive FIFO */
#define AXIUART16550_OPTION_ASSERT_OUT2		0x0008 /**< Assert out2 signal */
#define AXIUART16550_OPTION_ASSERT_OUT1		0x0004 /**< Assert out1 signal */
#define AXIUART16550_OPTION_ASSERT_RTS		0x0002 /**< Assert RTS signal */
#define AXIUART16550_OPTION_ASSERT_DTR		0x0001 /**< Assert DTR signal */
/*@}*/

/** @name Data format values
 * @{
 */
/**
 * These constants specify the data format that may be set or retrieved
 * with the driver.  The data format includes the number of data bits, the
 * number of stop bits and parity.
 *
 */
#define AXIUART16550_FORMAT_8_BITS		3 /**< 8 data bits */
#define AXIUART16550_FORMAT_7_BITS		2 /**< 7 data bits */
#define AXIUART16550_FORMAT_6_BITS		1 /**< 6 data bits */
#define AXIUART16550_FORMAT_5_BITS 		0 /**< 5 data bits */

#define AXIUART16550_FORMAT_EVEN_PARITY		2 /**< Even Parity */
#define AXIUART16550_FORMAT_ODD_PARITY		1 /**< Odd Parity */
#define AXIUART16550_FORMAT_NO_PARITY		0 /**< No Parity */

#define AXIUART16550_FORMAT_2_STOP_BIT		1 /**< 2 stop bits */
#define AXIUART16550_FORMAT_1_STOP_BIT		0 /**< 1 stop bit */
/*@}*/

/** @name FIFO trigger values
 * @{
 */
/*
 * These constants specify receive FIFO trigger levels which specify
 * the number of bytes at which a receive data event (interrupt) will occur.
 *
 */
#define AXIUART16550_FIFO_TRIGGER_14		0xC0 /**< 14 byte trigger level */
#define AXIUART16550_FIFO_TRIGGER_08		0x80 /**< 8 byte trigger level */
#define AXIUART16550_FIFO_TRIGGER_04		0x40 /**< 4 byte trigger level */
#define AXIUART16550_FIFO_TRIGGER_01		0x00 /**< 1 byte trigger level */
/*@}*/

/** @name Modem status values
 * @{
 */
/**
 * These constants specify the modem status that may be retrieved
 * from the driver.
 *
  */
#define AXIUART16550_MODEM_DCD_DELTA_MASK	0x08 /**< DCD signal changed state */
#define AXIUART16550_MODEM_DSR_DELTA_MASK	0x02 /**< DSR signal changed state */
#define AXIUART16550_MODEM_CTS_DELTA_MASK	0x01 /**< CTS signal changed state */
#define AXIUART16550_MODEM_RINGING_MASK		0x40 /**< Ring signal is active */
#define AXIUART16550_MODEM_DSR_MASK		    0x20 /**< Current state of DSR signal */
#define AXIUART16550_MODEM_CTS_MASK		    0x10 /**< Current state of CTS signal */
#define AXIUART16550_MODEM_DCD_MASK 		0x80 /**< Current state of DCD signal */
#define AXIUART16550_MODEM_RING_STOP_MASK	0x04 /**< Ringing has stopped */
/*@}*/

/** @name Callback events
 * @{
 */
/**
 * These constants specify the handler events that are passed to
 * a handler from the driver.  These constants are not bit masks such that
 * only one will be passed at a time to the handler.
 *
 */
#define AXIUART16550_EVENT_RECV_DATA		1 /**< Data has been received */
#define AXIUART16550_EVENT_RECV_TIMEOUT		2 /**< A receive timeout occurred */
#define AXIUART16550_EVENT_SENT_DATA		3 /**< Data has been sent */
#define AXIUART16550_EVENT_RECV_ERROR		4 /**< A receive error was detected */
#define AXIUART16550_EVENT_MODEM	 		5 /**< A change in modem status */
/*@}*/

/** @name Error values
 * @{
 */
/**
 * These constants specify the errors that may be retrieved from
 * the driver using the XUartNs550_GetLastErrors function. All of them are
 * bit masks, except no error, such that multiple errors may be specified.
 *
 */
#define AXIUART16550_ERROR_BREAK_MASK		0x10 /**< Break detected */
#define AXIUART16550_ERROR_FRAMING_MASK		0x08 /**< Receive framing error */
#define AXIUART16550_ERROR_PARITY_MASK		0x04 /**< Receive parity error */
#define AXIUART16550_ERROR_OVERRUN_MASK		0x02 /**< Receive overrun error */
#define AXIUART16550_ERROR_NONE 			0x00 /**< No error */
/*@}*/


/** @struct axiuart_ctx_t
 *  axiuart context structure
 */
typedef struct axiuart_ctx_t {
    void                *devo;
    axiuart_brdspec_t   brdspec;
} axiuart_ctx_t;

static ccl_err_t          _ret;
static axiuart_ctx_t *_p_axiuart[eDEVBRD_AXIUARTS_NUM];

static ccl_err_t _axiuart_init(void *p_priv);

/* axiuart register read */
static ccl_err_t _axiuart_reg_read(void *p_priv, int offset, 
                                   __attribute__((unused)) b_u32 offset_sz, 
                                   b_u32 idx, b_u8 *p_value, b_u32 size)
{
    axiuart_ctx_t   *p_axiuart;

    if ( !p_priv || offset > AXIUART16550_MAX_OFFSET || 
         !p_value || idx || size != sizeof(b_u32) ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIUART16550_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_axiuart = (axiuart_ctx_t *)p_priv;     
    offset += p_axiuart->brdspec.offset;
    _ret = devspibus_read_buff(p_axiuart->brdspec.devname, offset, idx, p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to read over spi bus\n");
        AXIUART16550_ERROR_RETURN(CCL_FAIL);
    }
    PDEBUG("p_axiuart->brdspec.offset=0x%x, offset=0x%04x, value=0x%08x\n", 
           p_axiuart->brdspec.offset, offset, *(b_u32 *)p_value);

    return CCL_OK;
}

/* axiuart register write */
static ccl_err_t _axiuart_reg_write(void *p_priv, int offset, 
                                    __attribute__((unused)) b_u32 offset_sz, 
                                    b_u32 idx, b_u8 *p_value, b_u32 size)
{
    axiuart_ctx_t   *p_axiuart;

    if ( !p_priv || offset > AXIUART16550_MAX_OFFSET || 
         !p_value || idx || size != sizeof(b_u32) ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIUART16550_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_axiuart = (axiuart_ctx_t *)p_priv;     
    offset += p_axiuart->brdspec.offset;
    PDEBUG("p_axiuart->brdspec.offset=0x%x, offset=0x%04x, value=0x%08x\n", 
           p_axiuart->brdspec.offset, offset, *(b_u32 *)p_value);
    _ret = devspibus_write_buff(p_axiuart->brdspec.devname, offset, idx, p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to write over spi bus\n");
        AXIUART16550_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

ccl_err_t devaxiuart_read_buff(b_u32 dev_id, b_u8 *p_value, b_u32 size)
{
    b_u32 regval = 0, ier = 0, lsr = 0;
    b_u32 cnt = 0;
    b_u8 *buffer = p_value;

    if ( !p_value || dev_id > eDEVBRD_AXIUARTS_NUM ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIUART16550_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Enter a critical region by disabling all the UART interrupts to allow
     * this call to stop a previous operation that may be interrupt driven
     */
    AXIUART16550_REG_READ(_p_axiuart[dev_id], AXIUART16550_IER, 
                          sizeof(b_u32), 0, 
                          (b_u8 *)&ier, sizeof(b_u32));
    regval = 0;
    AXIUART16550_REG_WRITE(_p_axiuart[dev_id], AXIUART16550_IER, 
                           sizeof(b_u32), 0, 
                           (b_u8 *)&regval, sizeof(b_u32));

    /*
     * Loop until there is not more data buffered by the UART or the
     * specified number of bytes is received
     */
    while (cnt < size) {
        PDEBUG("bytes to read: %d, rx bytes: %d\n", size, cnt);
        /*
         * Read the Line Status Register to determine if there is any
         * data in the receiver/FIFO
         */
        AXIUART16550_REG_READ(_p_axiuart[dev_id], AXIUART16550_LSR, 
                              sizeof(b_u32), 0, 
                              (b_u8 *)&lsr, sizeof(b_u32));

        /*
         * If there is a break condition then a zero data byte was put
         * into the receiver, just read it and dump it and update the
         * stats
         */
        if (lsr & AXIUART16550_LSR_BREAK_INT)
            AXIUART16550_REG_READ(_p_axiuart[dev_id], AXIUART16550_RBR, 
                                  sizeof(b_u32), 0, 
                                  (b_u8 *)&regval, sizeof(b_u32));
        /*
         * If there is data ready to be removed, then put the next byte
         * received into the specified buffer and update the stats to
         * reflect any receive errors for the byte
         */
        else if (lsr & AXIUART16550_LSR_DATA_READY) {
            AXIUART16550_REG_READ(_p_axiuart[dev_id], AXIUART16550_RBR, 
                                  sizeof(b_u32), 0, 
                                  (b_u8 *)&regval, sizeof(b_u32));
            buffer[cnt++] = (b_u8)regval;
        } else
            /*
             * There's no more data buffered, wait and start over
             */
            usleep(100);
    }

    /*
     * Restore the interrupt enable register to it's previous value such
     * that the critical region is exited
     */
    AXIUART16550_REG_WRITE(_p_axiuart[dev_id], AXIUART16550_IER, 
                           sizeof(b_u32), 0, 
                           (b_u8 *)&ier, sizeof(b_u32));

    return CCL_OK;
}

ccl_err_t devaxiuart_write_buff(b_u32 dev_id, b_u8 *p_value, b_u32 size)
{
    b_u32 regval = 0, lsr = 0; 
    b_u32 fifosz;
    b_u32 bytes_to_send;
    b_u32 remaining_bytes = size;
    b_u8 *buffer = p_value;
    b_u32 cnt = 0;

    if ( !p_value || dev_id > eDEVBRD_AXIUARTS_NUM ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIUART16550_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Enter a critical region by disabling the UART transmit interrupts to
     * allow this call to stop a previous operation that may be interrupt
     * driven, only stop the transmit interrupt since this critical region
     * is not really exited in the normal manner
     */
    AXIUART16550_REG_READ(_p_axiuart[dev_id], AXIUART16550_IER, 
                          sizeof(b_u32), 0, 
                          (b_u8 *)&regval, sizeof(b_u32)); 
    regval &= ~AXIUART16550_IER_TX_EMPTY;
    AXIUART16550_REG_WRITE(_p_axiuart[dev_id], AXIUART16550_IER, 
                           sizeof(b_u32), 0, 
                           (b_u8 *)&regval, sizeof(b_u32));

    /*
     * Read the line status register to determine if the transmitter is
     * empty
     */
    AXIUART16550_REG_READ(_p_axiuart[dev_id], AXIUART16550_LSR, 
                          sizeof(b_u32), 0, 
                          (b_u8 *)&lsr, sizeof(b_u32));
    /*
     * If the transmitter is not empty then don't send any data, the empty
     * room in the FIFO is not available
     */
    while (!(lsr & AXIUART16550_LSR_TX_BUFFER_EMPTY)) {
        if (cnt++ > 10000) {
            return CCL_FAIL;
        }
        usleep(100);
        AXIUART16550_REG_READ(_p_axiuart[dev_id], AXIUART16550_LSR, 
                              sizeof(b_u32), 0, 
                              (b_u8 *)&lsr, sizeof(b_u32));
    }

    while (remaining_bytes > 0) {
        /*
         * Read the interrupt ID register to determine if FIFOs
         * are enabled
         */
        AXIUART16550_REG_READ(_p_axiuart[dev_id], AXIUART16550_IIR, 
                              sizeof(b_u32), 0, 
                              (b_u8 *)&regval, sizeof(b_u32)); 

        /*
         * When there are FIFOs, send up to the FIFO size. When there
         * are no FIFOs, only send 1 byte of data
         */
        if (regval & AXIUART16550_INT_ID_FIFOS_ENABLED) {
            /*
             * Determine how many bytes can be sent depending on if
             * the transmitter is empty, a FIFO size of N is really
             * N - 1 plus the transmitter register
             */
            if (lsr & AXIUART16550_LSR_TX_EMPTY)
                fifosz = AXIUART16550_FIFO_SIZE;
            else
                fifosz = AXIUART16550_FIFO_SIZE - 1;

            /*
             * FIFOs are enabled, if the number of bytes to send
             * is less than the size of the FIFO, then send all
             * bytes, otherwise fill the FIFO
             */
            if (remaining_bytes < fifosz)
                bytes_to_send = remaining_bytes;
            else
                bytes_to_send = fifosz;
        } else
            /*
             * Without FIFOs, we can only send 1 byte
             */
            bytes_to_send = 1;

        /*
         * Fill the FIFO if it's present or the transmitter only from
         * the the buffer that was specified
         */
        for (cnt = 0; cnt < bytes_to_send; cnt++)
            AXIUART16550_REG_WRITE(_p_axiuart[dev_id], AXIUART16550_THR, 
                                   sizeof(b_u32), 0, 
                                   (b_u8 *)&buffer[cnt], sizeof(b_u32));
        /*
         * Update the buffer to reflect the bytes that were sent from it
         */
        buffer += cnt;
        remaining_bytes -= cnt;
    }

    /*
     * If interrupts are enabled as indicated by the receive interrupt, then
     * enable the transmit interrupt, it is not enabled continuously because
     * it causes an interrupt whenever the FIFO is empty
     */
    AXIUART16550_REG_READ(_p_axiuart[dev_id], AXIUART16550_IER, 
                          sizeof(b_u32), 0, 
                          (b_u8 *)&regval, sizeof(b_u32)); 
    if (regval & AXIUART16550_IER_RX_DATA) {
        regval |= AXIUART16550_IER_TX_EMPTY;
        AXIUART16550_REG_WRITE(_p_axiuart[dev_id], AXIUART16550_IER, 
                               sizeof(b_u32), 0, 
                               (b_u8 *)&regval, sizeof(b_u32)); 
    }

    return CCL_OK;
}

static ccl_err_t _axiuart_buf_read(void *p_priv, 
                                   __attribute__((unused)) b_u32 offset, 
                                   __attribute__((unused)) b_u32 offset_sz, 
                                   b_u32 idx, b_u8 *p_value, b_u32 size)
{
    axiuart_ctx_t   *p_axiuart;

    if ( !p_priv || !p_value || 
         idx || size != sizeof(b_u32) ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIUART16550_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    PDEBUG("123$=> offset=0x%03x, value=0x%08x\n", offset, *p_value);

    p_axiuart = (axiuart_ctx_t *)p_priv;     
    _ret = devaxiuart_read_buff(p_axiuart->brdspec.dev_id, p_value, size);
    if ( _ret ) {
        PDEBUG("devaxiuart_read_buff error\n");
        AXIUART16550_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

static ccl_err_t _axiuart_buf_write(void *p_priv, 
                                    __attribute__((unused)) b_u32 offset, 
                                    __attribute__((unused)) b_u32 offset_sz, 
                                    b_u32 idx, b_u8 *p_value, b_u32 size)
{
    axiuart_ctx_t   *p_axiuart;

    if ( !p_priv || !p_value || 
         idx || size != sizeof(b_u32) ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIUART16550_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    PDEBUG("123$=> offset=0x%03x, value=0x%08x\n", offset, *p_value);

    p_axiuart = (axiuart_ctx_t *)p_priv;     
    _ret = devaxiuart_write_buff(p_axiuart->brdspec.dev_id, p_value, size);
    if ( _ret ) {
        PDEBUG("devaxiuart_read_buff error\n");
        AXIUART16550_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

static ccl_err_t _axiuart_configure_test(void *p_priv)
{
    axiuart_ctx_t *p_axiuart;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIUART16550_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_axiuart = (axiuart_ctx_t *)p_priv;

    return CCL_OK;
}

static ccl_err_t _axiuart_run_test(void *p_priv, 
                                   __attribute__((unused)) b_u8 *buf, 
                                   __attribute__((unused)) b_u32 size)
{
    axiuart_ctx_t *p_axiuart;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIUART16550_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_axiuart = (axiuart_ctx_t *)p_priv;

    return CCL_OK;
}

ccl_err_t axiuart_attr_read(void *p_priv, b_u32 attrid, 
                            b_u32 offset, b_u32 offset_sz, 
                            b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || offset > AXIUART16550_MAX_OFFSET || 
         !p_value || idx || size != sizeof(b_u32) ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIUART16550_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch ( attrid ) {
    case eAXIUART16550_RBR:
    case eAXIUART16550_THR:
    case eAXIUART16550_IER:
    case eAXIUART16550_IIR:
    case eAXIUART16550_FCR:
    case eAXIUART16550_LCR:
    case eAXIUART16550_MCR:
    case eAXIUART16550_LSR:
    case eAXIUART16550_MSR:
    case eAXIUART16550_SCR:
    case eAXIUART16550_DLL:
    case eAXIUART16550_DLM:
        AXIUART16550_REG_READ(p_priv, offset, offset_sz, idx, p_value, size);
        break;
    case eAXIUART16550_READ:
        _ret = _axiuart_buf_read(p_priv, offset, offset_sz, idx, p_value, size);
        break;
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK) {
        PDEBUG("Failure in attribute id: %d\n", attrid);
    }

    return _ret;               
}

ccl_err_t axiuart_attr_write(void *p_priv, b_u32 attrid, 
                             b_u32 offset, b_u32 offset_sz, 
                             b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || offset > AXIUART16550_MAX_OFFSET || 
         !p_value || idx || size != sizeof(b_u32) ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIUART16550_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch ( attrid ) {
    case eAXIUART16550_RBR:
    case eAXIUART16550_THR:
    case eAXIUART16550_IER:
    case eAXIUART16550_IIR:
    case eAXIUART16550_FCR:
    case eAXIUART16550_LCR:
    case eAXIUART16550_MCR:
    case eAXIUART16550_LSR:
    case eAXIUART16550_MSR:
    case eAXIUART16550_SCR:
    case eAXIUART16550_DLL:
    case eAXIUART16550_DLM:
        AXIUART16550_REG_WRITE(p_priv, offset, offset_sz, idx, p_value, size);
        break;
    case eAXIUART16550_WRITE:
        _ret = _axiuart_buf_write(p_priv, offset, offset_sz, idx, p_value, size);
        break;
    case eAXIUART16550_INIT:
        _ret = _axiuart_init(p_priv);
        break;
    case eAXIUART16550_CONFIGURE_TEST:
        _ret = _axiuart_configure_test(p_priv);
        break;
    case eAXIUART16550_RUN_TEST:
        _ret = _axiuart_run_test(p_priv, p_value, size);
        break;
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK) {
        PDEBUG("Failure in attribute id: %d\n", attrid);
    }

    return _ret;               
}

/* axiuart device attributes definition */
static device_attr_t _attrs[] = {
    {
        .name = "istest", 
        .type = eDEVMAN_ATTR_BOOL, .maxnum = 1, 
        .offset = offsetof(struct axiuart_ctx_t, brdspec.istest),
        .format = "%d"
    },
    {
        .name = "name", 
        .type = eDEVMAN_ATTR_STRING, .maxnum = 1, 
        .offset = offsetof(struct axiuart_ctx_t, brdspec.name),
        .size = DEVMAN_DEVNAME_LEN,
        .format = "%s"
    },
    {
        .name = "offset", 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset = offsetof(struct axiuart_ctx_t, brdspec.offset),
        .format = "0x%08x"
    },
    {
        .name = "rbuf", .id = eAXIUART16550_RBR, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIUART16550_RBR
    },
    {
        .name = "thold", .id = eAXIUART16550_THR, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIUART16550_THR
    },
    {
        .name = "ier", .id = eAXIUART16550_IER, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIUART16550_IER
    },
    {
        .name = "iir", .id = eAXIUART16550_IIR, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIUART16550_IIR
    },
    {
        .name = "fifoctl", .id = eAXIUART16550_FCR, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIUART16550_FCR
    },
    {
        .name = "linectl", .id = eAXIUART16550_LCR, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIUART16550_LCR
    },
    {
        .name = "modemctl", .id = eAXIUART16550_MCR, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIUART16550_MCR
    },
    {
        .name = "linestat", .id = eAXIUART16550_LSR, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIUART16550_LSR
    },
    {
        .name = "modemstat", .id = eAXIUART16550_MSR, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIUART16550_MSR
    },
    {
        .name = "scratch", .id = eAXIUART16550_SCR, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIUART16550_SCR
    },
    {
        .name = "divl", .id = eAXIUART16550_DLL, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIUART16550_DLL
    },
    {
        .name = "divm", .id = eAXIUART16550_DLM, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIUART16550_DLM
    },
    {
        .name = "fread", .id = eAXIUART16550_READ, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIUART16550_DLM
    },
    {
        .name = "fwrite", .id = eAXIUART16550_WRITE, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = AXIUART16550_DLM
    },
    {
        .name = "init", .id = eAXIUART16550_INIT, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "config_test", .id = eAXIUART16550_CONFIGURE_TEST, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "run_test", .id = eAXIUART16550_RUN_TEST, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, 
        .size = sizeof(board_dev_stats_t), 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
};

/**
 * @brief Set the baud rate for the UART.
 *
 * @param[in] p_axiuart is a pointer to the axiuart_ctx_t 
 *       instance to be worked on.
 *
 * @return CCL_OK if initialization was successful, 
 *         CCL_BAD_PARAM_ERR otherwise
 */
static ccl_err_t _devaxiuart_set_baud_rate(axiuart_ctx_t *p_axiuart)
{
    b_u32 baud_lsb, baud_msb;
    b_u32 regval = 0, lcr = 0;
    b_u32 divisor;

    if ( !p_axiuart || !p_axiuart->brdspec.input_clk || 
         !p_axiuart->brdspec.baudrate ) {
        PDEBUG("Error! Invalid input parameters\n"
               "p_axiuart=%p, p_axiuart->brdspec.input_clk=%d, p_axiuart->brdspec.baudrate=%d\n",
               p_axiuart, p_axiuart->brdspec.input_clk, p_axiuart->brdspec.baudrate);
        AXIUART16550_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Determine what the divisor should be to get the specified baud
     * rater based upon the input clock frequency and a baud clock prescaler
     * of 16
     */
    divisor = ((p_axiuart->brdspec.input_clk +
                ((p_axiuart->brdspec.baudrate * 16UL)/2)) /
               (p_axiuart->brdspec.baudrate * 16UL));
    /*
     * Get the least significant and most significant bytes of the divisor
     * so they can be written to 2 byte registers
     */
    baud_lsb = divisor & AXIUART16550_DIVISOR_BYTE_MASK;
    baud_msb = (divisor >> 8) & AXIUART16550_DIVISOR_BYTE_MASK;

    /*
     * Set the divisor latch access bit so the baud rate can be set
     */
    regval = AXIUART16550_LCR_DLAB;
    AXIUART16550_REG_WRITE(p_axiuart, AXIUART16550_LCR, 
                           sizeof(b_u32), 0, 
                           (b_u8 *)&regval, sizeof(b_u32)); 
    lcr = regval;

    /*
     * Set the baud Divisors to set rate, the initial write of 0xFF is to
     * keep the divisor from being 0 which is not recommended as per the
     * NS16550D spec sheet
     */
    regval = 0xff;
    AXIUART16550_REG_WRITE(p_axiuart, AXIUART16550_DLL, 
                           sizeof(b_u32), 0, 
                           (b_u8 *)&regval, sizeof(b_u32)); 
    regval = baud_msb;
    AXIUART16550_REG_WRITE(p_axiuart, AXIUART16550_DLM, 
                           sizeof(b_u32), 0, 
                           (b_u8 *)&regval, sizeof(b_u32)); 
    regval = baud_lsb;
    AXIUART16550_REG_WRITE(p_axiuart, AXIUART16550_DLL, 
                           sizeof(b_u32), 0, 
                           (b_u8 *)&regval, sizeof(b_u32)); 
    /*
     * Clear the Divisor latch access bit, DLAB to allow normal
     * operation and write to the line control register
     */
    AXIUART16550_REG_WRITE(p_axiuart, AXIUART16550_LCR, 
                           sizeof(b_u32), 0, 
                           (b_u8 *)&lcr, sizeof(b_u32)); 

    return CCL_OK;
}

/**
 * Initializes a specific AXIUART16550 instance such that it is 
 * ready to be used. If the device has FIFOs (16550), they are 
 * enabled and the a receive FIFO threshold is set for 8 bytes. 
 * The default operating mode of the driver is polled mode. 
 *
 * @param[in] p_axiuart is a pointer to the axiuart_ctx_t 
 *       instance to be worked on.
 *
 * @return CCL_OK if initialization was successful, 
 *         CCL_BAD_PARAM_ERR otherwise
 */
static ccl_err_t _axiuart_init(void *p_priv)
{
    axiuart_ctx_t *p_axiuart;
    b_u32         regval = 0;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIUART16550_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_axiuart = (axiuart_ctx_t *)p_priv;
    if ( p_axiuart->brdspec.stop_bits < eDEVBRD_AXIUART_1_STOP_BITS || 
         p_axiuart->brdspec.stop_bits > eDEVBRD_AXIUART_2_STOP_BITS || 
         p_axiuart->brdspec.data_bits < eDEVBRD_AXIUART_6_DATA_BITS ||
         p_axiuart->brdspec.data_bits > eDEVBRD_AXIUART_8_DATA_BITS ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIUART16550_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    _ret = _devaxiuart_set_baud_rate(p_axiuart);
    if (_ret) {
        PDEBUG("_devaxiuart_set_baud_rate error\n");
        AXIUART16550_ERROR_RETURN(_ret);
    }

    /*
     * Set up the default format for the data, 8 bit data, 1 stop bit,
     * no parity
     */
    if (p_axiuart->brdspec.stop_bits != eDEVBRD_AXIUART_1_STOP_BITS)
        regval |= AXIUART16550_LCR_2_STOP_BITS;
    if (p_axiuart->brdspec.parity == eDEVBRD_AXIUART_PARITY_EVEN)
        regval |= AXIUART16550_LCR_ENABLE_PARITY | 
                  AXIUART16550_LCR_EVEN_PARITY;
    else if (p_axiuart->brdspec.parity == eDEVBRD_AXIUART_PARITY_ODD)
        regval |= AXIUART16550_LCR_ENABLE_PARITY;
    if (p_axiuart->brdspec.data_bits == eDEVBRD_AXIUART_6_DATA_BITS)
        regval |= AXIUART16550_FORMAT_6_BITS;
    else if (p_axiuart->brdspec.data_bits == eDEVBRD_AXIUART_7_DATA_BITS)
        regval |= AXIUART16550_FORMAT_7_BITS;
    else 
        regval |= AXIUART16550_FORMAT_8_BITS;
    AXIUART16550_REG_WRITE(p_axiuart, AXIUART16550_LCR, 
                           sizeof(b_u32), 0, 
                           (b_u8 *)&regval, sizeof(b_u32)); 

    /*
     * Enable the FIFOs assuming they are present and set the receive FIFO
     * trigger level for 8 bytes assuming that this will work best with most
     * baud rates, enabling the FIFOs also clears them, note that this must
     * be done with 2 writes, 1st enabling the FIFOs then set the trigger
     * level
     */
    regval = AXIUART16550_FIFO_ENABLE;
    AXIUART16550_REG_WRITE(p_axiuart, AXIUART16550_FCR, 
                           sizeof(b_u32), 0, 
                           (b_u8 *)&regval, sizeof(b_u32)); 
    regval |= AXIUART16550_FIFO_RX_TRIG_MSB;
    AXIUART16550_REG_WRITE(p_axiuart, AXIUART16550_FCR, 
                           sizeof(b_u32), 0, 
                           (b_u8 *)&regval, sizeof(b_u32)); 

    /*
     * Update the Global Interrupt Enable just after we start.
     */
    if ( p_axiuart->brdspec.op_mode == eDEVBRD_OP_MODE_POLL ) 
        regval = 0;
    else 
        regval = AXIUART16550_IER_ALL;

    AXIUART16550_REG_WRITE(p_axiuart, AXIUART16550_IER, 
                           sizeof(b_u32), 0, 
                           (b_u8 *)&regval, sizeof(b_u32)); 
 
    return CCL_OK;
}

static ccl_err_t _axiuart_self_test(axiuart_ctx_t *p_axiuart)
{
    b_u32 ier, mcr, lsr, regval;
    b_u32 i;
    b_u8 test_string[33] = "abcdefghABCDEFGH0123456776543210";
    b_u8 recv_string[33] = {0};


    if ( !p_axiuart || 
         p_axiuart->brdspec.stop_bits < eDEVBRD_AXIUART_1_STOP_BITS || 
         p_axiuart->brdspec.stop_bits > eDEVBRD_AXIUART_2_STOP_BITS || 
         p_axiuart->brdspec.data_bits < eDEVBRD_AXIUART_6_DATA_BITS ||
         p_axiuart->brdspec.data_bits > eDEVBRD_AXIUART_8_DATA_BITS) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIUART16550_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Setup for polling by disabling all interrupts in the interrupt enable
     * register
     */
    AXIUART16550_REG_READ(p_axiuart, AXIUART16550_IER, 
                          sizeof(b_u32), 0, 
                          (b_u8 *)&ier, sizeof(b_u32)); 
    regval = 0;
    AXIUART16550_REG_WRITE(p_axiuart, AXIUART16550_IER, 
                           sizeof(b_u32), 0, 
                           (b_u8 *)&regval, sizeof(b_u32)); 

	/*
	 * Setup for loopback by enabling the loopback in the modem control
	 * register
	 */
    AXIUART16550_REG_READ(p_axiuart, AXIUART16550_MCR, 
                          sizeof(b_u32), 0, 
                          (b_u8 *)&mcr, sizeof(b_u32)); 
    regval = mcr | AXIUART16550_MCR_LOOP;
    AXIUART16550_REG_WRITE(p_axiuart, AXIUART16550_MCR, 
                           sizeof(b_u32), 0, 
                           (b_u8 *)&regval, sizeof(b_u32)); 

	/*
	 * Send a number of bytes and receive them, one at a time so this
	 * test will work for 450 and 550
	 */
	for (i = 0; i < 32; i++) {
		/*
		 * Send out the byte and if it was not sent then the failure
		 * will be caught in the compare at the end
		 */
        _ret = devaxiuart_write_buff(p_axiuart->brdspec.dev_id, &test_string[i], 1);
        if ( _ret ) {
            PDEBUG("devaxiuart_read_buff error\n");
            AXIUART16550_ERROR_RETURN(CCL_FAIL);
        }

		/*
		 * Wait til the byte is received such that it should be waiting
		 * in the receiver. This can hang if the HW is broken
		 */
		do {
            AXIUART16550_REG_READ(p_axiuart, AXIUART16550_LSR, 
                                  sizeof(b_u32), 0, 
                                  (b_u8 *)&lsr, sizeof(b_u32)); 
		}
		while ((lsr & AXIUART16550_LSR_DATA_READY) == 0);

		/*
		 * Receive the byte that should have been received because of
		 * the loopback, if it wasn't received then it will be caught
		 * in the compare at the end
		 */
        _ret = devaxiuart_read_buff(p_axiuart->brdspec.dev_id, &recv_string[i], 1);
        if ( _ret ) {
            PDEBUG("devaxiuart_read_buff error\n");
            AXIUART16550_ERROR_RETURN(CCL_FAIL);
        }
	}

	/*
	 * Compare the bytes received to the bytes sent to verify the exact data
	 * was received
	 */
	for (i = 0; i < 32; i++) {
		if (test_string[i] != recv_string[i]) {
            ccl_syslog_err("the test failed: test_string[%d]=%c, recv_string[%d]=%c\n",
                   i, test_string[i], i, recv_string[i]);
			_ret = CCL_FAIL;
		}
	}
    ccl_syslog_err("test_string=%s, recv_string=%s\n", test_string, recv_string);

	/*
	 * Restore the registers which were altered to put into polling and
	 * loopback modes so that this test is not destructive
	 */
    AXIUART16550_REG_WRITE(p_axiuart, AXIUART16550_IER, 
                           sizeof(b_u32), 0, 
                           (b_u8 *)&ier, sizeof(b_u32)); 
    AXIUART16550_REG_WRITE(p_axiuart, AXIUART16550_MCR, 
                           sizeof(b_u32), 0, 
                           (b_u8 *)&mcr, sizeof(b_u32)); 

    return _ret;
}

static ccl_err_t _faxiuart_cmd_init(devo_t devo, 
                                         __attribute__((unused)) device_cmd_parm_t parms[], 
                                         __attribute__((unused)) b_u16 parms_num)
{
    void *p_priv = devman_device_private(devo);

    /* start the device */
    _ret = _axiuart_init(p_priv);
    if ( _ret ) {
        PDEBUG("_axiuart_init error\n");
        AXIUART16550_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

static ccl_err_t _faxiuart_cmd_self_test(devo_t devo, 
                                         __attribute__((unused)) device_cmd_parm_t parms[], 
                                         __attribute__((unused)) b_u16 parms_num)
{
    axiuart_ctx_t *p_axiuart = devman_device_private(devo);

    /* start the device */
    _ret = _axiuart_self_test(p_axiuart);
    if ( _ret ) {
        PDEBUG("_axispi_self_test error\n");
        AXIUART16550_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

/* device commands */
static device_cmd_t _cmds[] = 
{
    { 
        .name = "init", .f_cmd = _faxiuart_cmd_init, .parms_num = 0,
        .help = "Init AXI UART device" 
    },
    { 
        .name = "self_test", .f_cmd = _faxiuart_cmd_self_test, .parms_num = 0,
        .help = "Perform self test of AXI UART device" 
    },
};

/* Device instance hw-specific initializations 
 */
static ccl_err_t _faxiuart_add(void *devo, void *brdspec) 
{
    axiuart_ctx_t       *p_axiuart;
    axiuart_brdspec_t   *p_brdspec;

    if ( !devo || !brdspec ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIUART16550_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_axiuart = devman_device_private(devo);
    if ( !p_axiuart ) {
        PDEBUG("NULL context area\n");
        AXIUART16550_ERROR_RETURN(CCL_FAIL);
    }

    p_brdspec = (axiuart_brdspec_t *)brdspec;
    /* back reference the device manager object handle */    
    p_axiuart->devo = devo;    
    /* save the board specific info */
    memcpy(&p_axiuart->brdspec, p_brdspec, sizeof(axiuart_brdspec_t));
#if 0
    _ret = _axiuart_init(p_axiuart);
    if ( !p_axiuart ) {
        PDEBUG("_axiuart_init error\n");
        AXIUART16550_ERROR_RETURN(CCL_FAIL);
    }
#endif
    /* store the context */
    _p_axiuart[p_axiuart->brdspec.dev_id] = p_axiuart;

    return CCL_OK;
}

/* Device instance hw-specific deinitializations 
 */
static ccl_err_t _faxiuart_cut(void *devo)
{
    axiuart_ctx_t  *p_axiuart;

    if ( !devo ) {
        PDEBUG("Error! Invalid input parameters\n");
        AXIUART16550_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_axiuart = devman_device_private(devo);
    if ( !p_axiuart ) {
        PDEBUG("NULL context area\n");
        AXIUART16550_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

/* device type description structure */
static device_driver_t _axiuart_driver = {
    .name = "axiuart",
    .f_add = _faxiuart_add,
    .f_cut = _faxiuart_cut,
    .attrs = _attrs,
    .attrs_num = sizeof(_attrs)/sizeof(_attrs[0]),
    .cmds = _cmds,
    .cmds_num = sizeof(_cmds)/sizeof(_cmds[0]),
    .priv_size = sizeof(axiuart_ctx_t)
}; 

/* Initialize AXIUART16550 device type
 */
ccl_err_t devaxiuart_init(void)
{
    /* register device type */
    _ret = devman_driver_register(&_axiuart_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to register driver\n");
        AXIUART16550_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}

/* Deinitialize AXIUART16550 device type
 */
ccl_err_t devaxiuart_fini(void)
{
    /* unregister device type */
    _ret = devman_driver_unregister(&_axiuart_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to unregister driver\n");
        AXIUART16550_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}
