/********************************************************************************/
/**
 * @file xadc.c
 * @brief XADC (analog-to-digital converter with on-chip sensors) 
 * device manager driver
 * @copyright 2019 CELARE. All rights reserved.
 */
/********************************************************************************/

#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <semaphore.h>
#include <linux/limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "spibus.h"
#include "xadc.h"

//#define XADC_DEBUG
/* pointing/error-returning macros */
#ifdef XADC_DEBUG
 #define PDEBUG(fmt, args...) \
         ccl_syslog_err("XADC_DEBUG:  %s[%d]: " fmt, __FUNCTION__, __LINE__, ## args)
#else
 #define PDEBUG(fmt, args...)
#endif
#define XADC_ERROR_RETURN(err) do { \
   ccl_syslog_err("Error! %s[%d], error=%i\n", __FUNCTION__, __LINE__, (err)); \
   return err; \
} while (0)

#define XADC_REG_READ(context, offset, offset_sz, idx, pvalue, size) do { \
    _ret = _xadc_reg_read((context), \
                          (offset), \
                          (offset_sz), (idx), \
                          (pvalue), (size)); \
    if ( _ret ) { \
        PDEBUG("_xadc_reg_read error\n"); \
        XADC_ERROR_RETURN(_ret); \
    } \
} while (0)

#define XADC_REG_WRITE(context, offset, offset_sz, idx, pvalue, size) do { \
    _ret = _xadc_reg_write((context), \
                           (offset), \
                           (offset_sz), (idx), \
                           (pvalue), (size)); \
    if ( _ret ) { \
        PDEBUG("_xadc_reg_write error\n"); \
        XADC_ERROR_RETURN(_ret); \
    } \
} while (0)

/** @name Registers
 *
 * Register offsets for this device.
 * @{
 */
#define XADC_SRR_OFFSET                         0x00
#define XADC_SR_OFFSET                          0x04
#define XADC_AOSR_OFFSET                        0x08
#define XADC_CONVSTR_OFFSET                     0x0C 
#define XADC_ADC_RESET_OFFSET                   0x10
#define XADC_GIER_OFFSET                        0x5C
#define XADC_IPISR_OFFSET                       0x60
#define XADC_IPIER_OFFSET                       0x68
#define XADC_TEMP_OFFSET                        0x200        /**< On-chip Temperature Reg */         
#define XADC_VCCINT_OFFSET                      0x204        /**< On-chip VCCINT data Reg */       
#define XADC_VCCAUX_OFFSET                      0x208        /**< On-chip VCCAUX data Reg */       
#define XADC_VP_VN_OFFSET                       0x20C        /**< ADC out of VP/VN */              
#define XADC_VREFP_OFFSET                       0x210        /**< On-chip VREFP data Reg */        
#define XADC_VREFN_OFFSET                       0x214        /**< On-chip VREFN data Reg */        
#define XADC_VBRAM_OFFSET                       0x218        /**< On-chip VBRAM , 7 Series */
#define XADC_SUPPLY_CALIB_OFFSET                0x220        /**< ADC A Supply Offset Reg */
#define XADC_ADC_CALIB_OFFSET                   0x224        /**< ADC A Offset data Reg */         
#define XADC_GAINERR_CALIB_OFFSET               0x228        /**< ADC A Gain Error Reg */          
#define XADC_MAX_TEMP_OFFSET                    0x280        /**< Max Temperature Reg */                
#define XADC_MAX_VCCINT_OFFSET                  0x284        /**< Max VCCINT Register */         
#define XADC_MAX_VCCAUX_OFFSET                  0x288        /**< Max VCCAUX Register */         
#define XADC_MAX_VCCBRAM_OFFSET                 0x28C        /**< Max BRAM Register, 7 series */ 
#define XADC_MIN_TEMP_OFFSET                    0x290        /**< Min Temperature Reg */         
#define XADC_MIN_VCCINT_OFFSET                  0x294        /**< Min VCCINT Register */         
#define XADC_MIN_VCCAUX_OFFSET                  0x298        /**< Min VCCAUX Register */         
#define XADC_MIN_VCCBRAM_OFFSET                 0x29C        /**< Min BRAM Register, 7 series */ 
#define XADC_CFR0_OFFSET                        0x300        /**< Configuration Register 0 */ 
#define XADC_CFR1_OFFSET                        0x304        /**< Configuration Register 1 */ 
#define XADC_CFR2_OFFSET                        0x308        /**< Configuration Register 2 */ 
#define XADC_ATR_TEMP_UPPER_OFFSET              0x340        /**< Temp Upper Alarm Register */   
#define XADC_ATR_VCCINT_UPPER_OFFSET            0x344        /**< VCCINT Upper Alarm Reg */      
#define XADC_ATR_VCCAUX_UPPER_OFFSET            0x348        /**< VCCAUX Upper Alarm Reg */      
#define XADC_ATR_OT_UPPER_OFFSET                0x34C        /**< Over Temp Upper Alarm Reg */   
#define XADC_ATR_TEMP_LOWER_OFFSET              0x350        /**< Temp Lower Alarm Register */   
#define XADC_ATR_VCCINT_LOWER_OFFSET            0x354        /**< VCCINT Lower Alarm Reg */      
#define XADC_ATR_VCCAUX_LOWER_OFFSET            0x358        /**< VCCAUX Lower Alarm Reg */      
#define XADC_ATR_OT_LOWER_OFFSET                0x35C        /**< Over Temp Lower Alarm Reg */   
#define XADC_ATR_VBRAM_UPPER_OFFSET             0x360        /**< VBRAM Upper Alarm, 7 series */ 
#define XADC_ATR_VBRAM_LOWER_OFFSET             0x370        /**< VRBAM Lower Alarm, 7 Series */

#define XADC_MAX_OFFSET XADC_ATR_VBRAM_LOWER_OFFSET


/* @} */

/**
 * @name Configuration Register 0 (CFR0) mask(s)
 * @{
 */
#define XADC_CFR0_CAL_AVG_MASK	    0x8000 /**< Averaging enable Mask */
#define XADC_CFR0_AVG_VALID_MASK	0x3000 /**< Averaging bit Mask */
#define XADC_CFR0_AVG1_MASK		    0x0000 /**< No Averaging */
#define XADC_CFR0_AVG16_MASK		0x1000 /**< average 16 samples */
#define XADC_CFR0_AVG64_MASK	 	0x2000 /**< average 64 samples */
#define XADC_CFR0_AVG256_MASK 	    0x3000 /**< average 256 samples */
#define XADC_CFR0_AVG_SHIFT	 	    12     /**< Averaging bits shift */
#define XADC_CFR0_MUX_MASK	 	    0x0800 /**< External Mask Enable */
#define XADC_CFR0_DU_MASK	 	    0x0400 /**< Bipolar/Unipolar mode */
#define XADC_CFR0_EC_MASK	 	    0x0200 /**< Event driven/
						                     *  Continuous mode selection
						                     */
#define XADC_CFR0_ACQ_MASK	 	    0x0100 /**< Add acquisition by 6 ADCCLK */
#define XADC_CFR0_CHANNEL_MASK	    0x001F /**< channel number bit Mask */

/*@}*/

/**
 * @name Configuration Register 1 (CFR1) mask(s)
 * @{
 */
#define XADC_CFR1_SEQ_VALID_MASK	         0xF000 /**< Sequence bit Mask */
#define XADC_CFR1_SEQ_SAFEMODE_MASK	         0x0000 /**< Default Safe mode */
#define XADC_CFR1_SEQ_ONEPASS_MASK	         0x1000 /**< Onepass through Seq */
#define XADC_CFR1_SEQ_CONTINPASS_MASK	     0x2000 /**< Continuous Cycling Seq */
#define XADC_CFR1_SEQ_SINGCHAN_MASK	         0x3000 /**< Single channel - No Seq */
#define XADC_CFR1_SEQ_SIMUL_SAMPLING_MASK    0x4000 /**< Simulataneous Sampling Mask */
#define XADC_CFR1_SEQ_INDEPENDENT_MASK       0x8000 /**< Independent mode */
#define XADC_CFR1_SEQ_SHIFT     		     12     /**< Sequence bit shift */
#define XADC_CFR1_ALM_VCCPDRO_MASK	         0x0800 /**< Alm 6 - VCCPDRO, Zynq  */
#define XADC_CFR1_ALM_VCCPAUX_MASK	         0x0400 /**< Alm 5 - VCCPAUX, Zynq */
#define XADC_CFR1_ALM_VCCPINT_MASK	         0x0200 /**< Alm 4 - VCCPINT, Zynq */
#define XADC_CFR1_ALM_VBRAM_MASK	         0x0100 /**< Alm 3 - VBRAM, 7 series */
#define XADC_CFR1_CAL_VALID_MASK	         0x00F0 /**< Valid calibration Mask */
#define XADC_CFR1_CAL_PS_GAIN_OFFSET_MASK    0x0080 /**< calibration 3 -Power
							                          *  Supply Gain/Offset Enable 
                                                      */
#define XADC_CFR1_CAL_PS_OFFSET_MASK	     0x0040 /**< calibration 2 - Power
							                          *  Supply Offset Enable 
                                                      */
#define XADC_CFR1_CAL_ADC_GAIN_OFFSET_MASK   0x0020 /**< calibration 1 -ADC Gain
							                          * Offset Enable 
                                                      */
#define XADC_CFR1_CAL_ADC_OFFSET_MASK	     0x0010 /**< calibration 0 -ADC Offset
							                          *  Enable 
                                                      */
#define XADC_CFR1_CAL_DISABLE_MASK	         0x0000 /**< No calibration */
#define XADC_CFR1_ALM_ALL_MASK	             0x0F0F /**< Mask for all alarms */
#define XADC_CFR1_ALM_VCCAUX_MASK	         0x0008 /**< Alarm 2 - VCCAUX Enable */
#define XADC_CFR1_ALM_VCCINT_MASK	         0x0004 /**< Alarm 1 - VCCINT Enable */
#define XADC_CFR1_ALM_TEMP_MASK	             0x0002 /**< Alarm 0 - Temperature */
#define XADC_CFR1_OT_MASK		             0x0001 /**< Over Temperature Enable */

/*@}*/

/**
 * @name Configuration Register 2 (CFR2) mask(s)
 * @{
 */
#define XADC_CFR2_CD_VALID_MASK	             0xFF00  /**< Clock divisor bit Mask   */
#define XADC_CFR2_CD_SHIFT		             8	     /**< Num of shift on division */
#define XADC_CFR2_CD_MIN		             8	     /**< Minimum value of divisor */
#define XADC_CFR2_CD_MAX		             255	 /**< Maximum value of divisor */

#define XADC_CFR2_CD_MIN		             8	     /**< Minimum value of divisor */
#define XADC_CFR2_PD_MASK		             0x0030	 /**< Power Down Mask */
#define XADC_CFR2_PD_XADC_MASK	             0x0030	 /**< Power Down XADC Mask */
#define XADC_CFR2_PD_ADC1_MASK	             0x0020	 /**< Power Down ADC1 Mask */
#define XADC_CFR2_PD_SHIFT		             4	     /**< Power Down Shift */
/*@}*/

/**
 * @name OT Upper Alarm Threshold Register Bit Definitions
 * @{
 */

#define XADC_ATR_OT_UPPER_ENB_MASK	0x000F /**< Mask for OT enable */
#define XADC_ATR_OT_UPPER_VAL_MASK	0xFFF0 /**< Mask for OT value */
#define XADC_ATR_OT_UPPER_VAL_SHIFT	4      /**< Shift for OT value */
#define XADC_ATR_OT_UPPER_ENB_VAL	0x0003 /**< value for OT enable */
#define XADC_ATR_OT_UPPER_VAL_MAX	0x0FFF /**< Max OT value */

/**
 * @name Averaging to be done for the channels.
 * @{
 */
#define XADC_AVG_0_SAMPLES	    0  /**< No Averaging */            
#define XADC_AVG_16_SAMPLES	    1  /**< average 16 samples */      
#define XADC_AVG_64_SAMPLES	    2  /**< average 64 samples */      
#define XADC_AVG_256_SAMPLES	3  /**< average 256 samples */

/**
 * @name Channel Sequencer modes of operation
 * @{
 */
#define XADC_SEQ_MODE_SAFE		        0  /**< Default Safe mode */
#define XADC_SEQ_MODE_ONEPASS		    1  /**< Onepass through Sequencer */
#define XADC_SEQ_MODE_CONTINPASS	    2  /**< Continuous Cycling Sequencer */
#define XADC_SEQ_MODE_SINGCHAN	        3  /**< Single channel -No Sequencing */
#define XADC_SEQ_MODE_SIMUL_SAMPLING	4  /**< Simultaneous sampling */
#define XADC_SEQ_MODE_INDEPENDENT	    8  /**< Independent mode */

/* @} */
/**
 * @name Indexes for the different channels.
 * @{
 */
#define XADC_CH_TEMP		    0x0  /**< On Chip Temperature */
#define XADC_CH_VCCINT	        0x1  /**< VCCINT */
#define XADC_CH_VCCAUX	        0x2  /**< VCCAUX */
#define XADC_CH_VPVN		    0x3  /**< VP/VN Dedicated analog inputs */
#define XADC_CH_VREFP		    0x4  /**< VREFP */
#define XADC_CH_VREFN		    0x5  /**< VREFN */
#define XADC_CH_VBRAM		    0x6  /**< On-chip VBRAM Data Reg, 7 series */
#define XADC_CH_SUPPLY_CALIB	0x07 /**< Supply Calib Data Reg */
#define XADC_CH_ADC_CALIB	    0x08 /**< ADC Offset Channel Reg */
#define XADC_CH_GAINERR_CALIB   0x09 /**< Gain Error Channel Reg  */
#define XADC_CH_VCCPINT	        0x0D /**< On-chip PS VCCPINT Channel , Zynq */
#define XADC_CH_VCCPAUX	        0x0E /**< On-chip PS VCCPAUX Channel , Zynq */
#define XADC_CH_VCCPDRO	        0x0F /**< On-chip PS VCCPDRO Channel , Zynq */
#define XADC_CH_AUX_MIN	        16   /**< Channel number for 1st Aux Channel */
#define XADC_CH_AUX_MAX	        31   /**< Channel number for Last Aux channel */

/*@}*/


/**
 * @name Indexes for reading the calibration Coefficient Data.
 * @{
 */
#define XADC_CALIB_SUPPLY_COEFF     0 /**< Supply Offset Calib Coefficient */
#define XADC_CALIB_ADC_COEFF        1 /**< ADC Offset Calib Coefficient */
#define XADC_CALIB_GAIN_ERROR_COEFF 2 /**< Gain Error Calib Coefficient*/
/*@}*/


/**
 * @name Indexes for reading the Minimum/Maximum Measurement Data.
 * @{
 */
#define XADC_MAX_TEMP		0   /**< Maximum Temperature Data */
#define XADC_MAX_VCCINT	    1   /**< Maximum VCCINT Data */
#define XADC_MAX_VCCAUX	    2   /**< Maximum VCCAUX Data */
#define XADC_MAX_VBRAM	    3   /**< Maximum VBRAM Data */
#define XADC_MIN_TEMP		4   /**< Minimum Temperature Data */
#define XADC_MIN_VCCINT	    5   /**< Minimum VCCINT Data */
#define XADC_MIN_VCCAUX	    6   /**< Minimum VCCAUX Data */
#define XADC_MIN_VBRAM	    7   /**< Minimum VBRAM Data */
#define XADC_MAX_VCCPINT	8   /**< Maximum VCCPINT Register , Zynq */
#define XADC_MAX_VCCPAUX	9   /**< Maximum VCCPAUX Register , Zynq */
#define XADC_MAX_VCCPDRO	0xA /**< Maximum VCCPDRO Register , Zynq */
#define XADC_MIN_VCCPINT	0xC /**< Minimum VCCPINT Register , Zynq */
#define XADC_MIN_VCCPAUX	0xD /**< Minimum VCCPAUX Register , Zynq */
#define XADC_MIN_VCCPDRO	0xE /**< Minimum VCCPDRO Register , Zynq */

/**
 * @name Power Down modes
 * @{
 */
#define XADC_PD_MODE_NONE		0  /**< No Power Down  */
#define XADC_PD_MODE_ADCB		1  /**< Power Down ADC B */
#define XADC_PD_MODE_XADC		2  /**< Power Down ADC A and ADC B */
/*@}*/

#define XADC_CALC_TEMPERATURE(regval) \
    ((b_double64)((((regval) >> 4) & 0xfff)*503.975)/4096 - 273.15)

#define XADC_CALC_VOLTAGE(regval) \
    ((b_double64)((((regval) >> 4) & 0xfff)*3)/4096)

/** @struct xadc_ctx_t
 *  xadc context structure
 */
typedef struct xadc_ctx_t {
    void                *devo;
    xadc_brdspec_t      brdspec;
} xadc_ctx_t;

static ccl_err_t    _ret;
static ccl_err_t _xadc_init(void *p_priv);

/* xadc register read */
static ccl_err_t _xadc_reg_read(void *p_priv, b_u32 offset, 
                                __attribute__((unused)) b_u32 offset_sz, 
                                b_u32 idx, b_u8 *p_value, b_u32 size)
{
    xadc_ctx_t *p_xadc;

    if ( !p_priv || offset > XADC_MAX_OFFSET || 
         !p_value || idx || size != sizeof(b_u32) ) {
        PDEBUG("Error! Invalid input parameters\n");
        XADC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    PDEBUGG("123$=> offset=0x%03x, size=%d\n", offset, size);

    p_xadc = (xadc_ctx_t *)p_priv;
    offset += p_xadc->brdspec.offset;

    _ret = devspibus_read_buff(p_xadc->brdspec.devname, offset, idx, p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to read over spi bus\n");
        XADC_ERROR_RETURN(CCL_FAIL);
    }

    PDEBUG("p_xadc->brdspec.offset=0x%x, offset=0x%04x, value=0x%08x\n", 
           p_xadc->brdspec.offset, offset, *(b_u32 *)p_value);

    return _ret;
}

/* xadc register write */
static ccl_err_t _xadc_reg_write(void *p_priv, b_u32 offset, 
                                 __attribute__((unused)) b_u32 offset_sz, 
                                 b_u32 idx, b_u8 *p_value, b_u32 size)
{
    xadc_ctx_t *p_xadc;

    if ( !p_priv || offset > XADC_MAX_OFFSET || 
         !p_value || idx || size != sizeof(b_u32) ) {
        PDEBUG("Error! Invalid input parameters\n");
        XADC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    PDEBUGG("123$=> offset=0x%02x, value=0x%08x\n", offset, *p_value);

    p_xadc = (xadc_ctx_t *)p_priv;
    offset += p_xadc->brdspec.offset;

    PDEBUG("p_xadc->brdspec.offset=0x%x, offset=0x%04x, value=0x%08x\n", 
           p_xadc->brdspec.offset, offset, *(b_u32 *)p_value);

    _ret = devspibus_write_buff(p_xadc->brdspec.devname, offset, idx, p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to write over spi bus\n");
        XADC_ERROR_RETURN(_ret);
    }    

    return _ret;
}

static ccl_err_t _xadc_configure_test(void *p_priv)
{
    xadc_ctx_t   *p_xadc;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        XADC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_xadc = (xadc_ctx_t *)p_priv;

    return CCL_OK;
}

static ccl_err_t _xadc_run_test(void *p_priv, 
                                b_u8 *buf, 
                                b_u32 size)
{
    xadc_ctx_t       *p_xadc;
    b_u32            regval;
    b_double64       temp;
    b_double64       vccint;
    b_double64       vccaux;
    b_double64       vbram;
    xadc_stats_t     *stats;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        XADC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_xadc = (xadc_ctx_t *)p_priv;

    if (size == sizeof(xadc_stats_t)) {
        stats = (xadc_stats_t *)buf;
        memset(&stats->temp, 0, sizeof(*stats)-DEVMAN_ATTRS_NAME_LEN);
    }
    _ret = _xadc_reg_read(p_priv, XADC_TEMP_OFFSET, sizeof(b_u32), 
                          0, (b_u8 *)&regval, sizeof(b_u32));
    if ( _ret == CCL_OK ) { 
        temp = XADC_CALC_TEMPERATURE(regval);
        if (size == sizeof(xadc_stats_t)) 
            stats->temp = temp;
    }
    _ret |= _xadc_reg_read(p_priv, XADC_VCCINT_OFFSET, sizeof(b_u32), 
                             0, (b_u8 *)&regval, sizeof(b_u32));
    if ( _ret == CCL_OK ) { 
        vccint = XADC_CALC_VOLTAGE(regval);
        if (size == sizeof(xadc_stats_t)) 
            stats->vccint = vccint;
    }
    _ret |= _xadc_reg_read(p_priv, XADC_VCCAUX_OFFSET, sizeof(b_u32), 
                             0, (b_u8 *)&regval, sizeof(b_u32));
    if ( _ret == CCL_OK ) { 
        vccaux = XADC_CALC_VOLTAGE(regval);
        if (size == sizeof(xadc_stats_t)) 
            stats->vccaux = vccaux;
    }
    _ret |= _xadc_reg_read(p_priv, XADC_VCCAUX_OFFSET, sizeof(b_u32), 
                             0, (b_u8 *)&regval, sizeof(b_u32));
    if ( _ret == CCL_OK ) { 
        vbram = XADC_CALC_VOLTAGE(regval);
        if (size == sizeof(xadc_stats_t)) 
            stats->vbram = vbram;
    }

    if ( _ret ) 
        XADC_ERROR_RETURN(_ret);

    PDEBUG("temp=%lf vccint=%lf, vccaux=%lf, vbram=%lf\n",
           temp, vccint, vccaux, vbram);

    return CCL_OK;
}

ccl_err_t xadc_attr_read(void *p_priv, b_u32 attrid, 
                         b_u32 offset, b_u32 offset_sz, 
                         b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || offset > XADC_MAX_OFFSET || 
         !p_value || idx ) {
        PDEBUG("Error! Invalid input parameters\n");
        XADC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch ( attrid ) {
    case eXADC_SR:
    case eXADC_AOSR:
    case eXADC_GIER:
    case eXADC_IPISR:
    case eXADC_IPIER:
    case eXADC_TEMP:
    case eXADC_VCCINT:
    case eXADC_VCCAUX:
    case eXADC_VP_VN:
    case eXADC_VREFP:
    case eXADC_VREFN:
    case eXADC_VBRAM:
    case eXADC_SUPPLY_CALIB:
    case eXADC_ADC_CALIB:
    case eXADC_GAINERR_CALIB:
    case eXADC_MAX_TEMP:
    case eXADC_MAX_VCCINT:
    case eXADC_MAX_VCCAUX:
    case eXADC_MAX_VCCBRAM:
    case eXADC_MIN_TEMP:
    case eXADC_MIN_VCCINT:
    case eXADC_MIN_VCCAUX:
    case eXADC_MIN_VCCBRAM:
    case eXADC_CFR0:
    case eXADC_CFR1:
    case eXADC_CFR2:
    case eXADC_ATR_TEMP_UPPER:
    case eXADC_ATR_VCCINT_UPPER:
    case eXADC_ATR_VCCAUX_UPPER:
    case eXADC_ATR_OT_UPPER:
    case eXADC_ATR_TEMP_LOWER:
    case eXADC_ATR_VCCINT_LOWER:
    case eXADC_ATR_VCCAUX_LOWER:
    case eXADC_ATR_OT_LOWER:
    case eXADC_ATR_VBRAM_UPPER:
    case eXADC_ATR_VBRAM_LOWER:
        XADC_REG_READ(p_priv, offset, offset_sz, idx, p_value, size);
        break;
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK) {
        PDEBUG("Failure in attribute id: %d\n", attrid);
    }

    return _ret;
}

ccl_err_t xadc_attr_write(void *p_priv, b_u32 attrid, 
                          b_u32 offset, b_u32 offset_sz, 
                          b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || offset > XADC_MAX_OFFSET || 
         !p_value || idx ) {
        PDEBUG("Error! Invalid input parameters\n");
        XADC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch ( attrid ) {
    case eXADC_SRR:          
    case eXADC_CONVSTR:
    case eXADC_ADC_RESET:
    case eXADC_GIER:
    case eXADC_IPISR:
    case eXADC_IPIER:
    case eXADC_CFR0:
    case eXADC_CFR1:
    case eXADC_CFR2:
    case eXADC_ATR_TEMP_UPPER:
    case eXADC_ATR_VCCINT_UPPER:
    case eXADC_ATR_VCCAUX_UPPER:
    case eXADC_ATR_OT_UPPER:
    case eXADC_ATR_TEMP_LOWER:
    case eXADC_ATR_VCCINT_LOWER:
    case eXADC_ATR_VCCAUX_LOWER:
    case eXADC_ATR_OT_LOWER:
    case eXADC_ATR_VBRAM_UPPER:
    case eXADC_ATR_VBRAM_LOWER:
        XADC_REG_WRITE(p_priv, offset, offset_sz, idx, p_value, size);
        break;
    case eXADC_INIT:
        _ret = _xadc_init(p_priv);
        break;
    case eXADC_CONFIGURE_TEST:
        _ret = _xadc_configure_test(p_priv);
        break;
    case eXADC_RUN_TEST:
        _ret = _xadc_run_test(p_priv, p_value, size);
        break;
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK) {
        PDEBUG("Failure in attribute id: %d\n", attrid);
    }

    return _ret;
}

/* xadc device attributes definition */
static device_attr_t _attrs[] = {
    {
        .name = "name", 
        .type = eDEVMAN_ATTR_STRING, .maxnum = 1, 
        .offset = offsetof(struct xadc_ctx_t, brdspec.name),
        .size = DEVMAN_DEVNAME_LEN,
        .format = "%s"
    },
    {
        .name = "offset", 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset = offsetof(struct xadc_ctx_t, brdspec.offset),
        .format = "0x%08x"
    },
    {
        .name = "srr", .id = eXADC_SRR, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = XADC_SRR_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "sr", .id = eXADC_SR, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = XADC_SR_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "aosr", .id = eXADC_AOSR, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = XADC_AOSR_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "convstr", .id = eXADC_CONVSTR, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = XADC_CONVSTR_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "reset", .id = eXADC_ADC_RESET, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = XADC_ADC_RESET_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "gier", .id = eXADC_GIER, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = XADC_GIER_OFFSET
    },
    {
        .name = "ipisr", .id = eXADC_IPISR, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = XADC_IPISR_OFFSET
    },
    {
        .name = "ipier", .id = eXADC_IPIER, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = XADC_IPIER_OFFSET
    },
    {
        .name = "temp", .id = eXADC_TEMP, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = XADC_TEMP_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "vccint", .id = eXADC_VCCINT, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = XADC_VCCINT_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "vccaux", .id = eXADC_VCCAUX, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = XADC_VCCAUX_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "vpvn", .id = eXADC_VP_VN, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = XADC_VP_VN_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "vrefp", .id = eXADC_VREFP, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = XADC_VREFP_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "vrefn", .id = eXADC_VREFN, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = XADC_VREFN_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "vbram", .id = eXADC_VBRAM, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = XADC_VBRAM_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "sup_calib", .id = eXADC_SUPPLY_CALIB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = XADC_SUPPLY_CALIB_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "adc_calib", .id = eXADC_ADC_CALIB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = XADC_ADC_CALIB_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "gainerr_calib", .id = eXADC_GAINERR_CALIB, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = XADC_GAINERR_CALIB_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "max_temp", .id = eXADC_MAX_TEMP, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = XADC_MAX_TEMP_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "max_vccint", .id = eXADC_MAX_VCCINT, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = XADC_MAX_VCCINT_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "max_vccaux", .id = eXADC_MAX_VCCAUX, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = XADC_MAX_VCCAUX_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "max_vccbram", .id = eXADC_MAX_VCCBRAM, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = XADC_MAX_VCCBRAM_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "min_temp", .id = eXADC_MIN_TEMP, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = XADC_MIN_TEMP_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "min_vccint", .id = eXADC_MIN_VCCINT, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = XADC_MIN_VCCINT_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "min_vccaux", .id = eXADC_MIN_VCCAUX, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = XADC_MIN_VCCAUX_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "min_vccbram", .id = eXADC_MIN_VCCBRAM, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = XADC_MIN_VCCBRAM_OFFSET,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "cfr0", .id = eXADC_CFR0, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = XADC_CFR0_OFFSET
    },
    {
        .name = "cfr1", .id = eXADC_CFR1, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = XADC_CFR1_OFFSET
    },
    {
        .name = "cfr2", .id = eXADC_CFR2, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = XADC_CFR2_OFFSET
    },
    {
        .name = "alm_temp_upper", .id = eXADC_ATR_TEMP_UPPER, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = XADC_ATR_TEMP_UPPER_OFFSET
    },
    {
        .name = "alm_vccint_upper", .id = eXADC_ATR_VCCINT_UPPER, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = XADC_ATR_VCCINT_UPPER_OFFSET
    },
    {
        .name = "alm_vccaux_upper", .id = eXADC_ATR_VCCAUX_UPPER, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = XADC_ATR_VCCAUX_UPPER_OFFSET
    },
    {
        .name = "alm_ot_upper", .id = eXADC_ATR_OT_UPPER, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = XADC_ATR_OT_UPPER_OFFSET
    },
    {
        .name = "alm_temp_lower", .id = eXADC_ATR_TEMP_LOWER, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = XADC_ATR_TEMP_LOWER_OFFSET
    },
    {
        .name = "alm_vccint_lower", .id = eXADC_ATR_VCCINT_LOWER, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = XADC_ATR_VCCINT_LOWER_OFFSET
    },
    {
        .name = "alm_vccaux_lower", .id = eXADC_ATR_VCCAUX_LOWER, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = XADC_ATR_VCCAUX_LOWER_OFFSET
    },
    {
        .name = "alm_ot_lower", .id = eXADC_ATR_OT_LOWER, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = XADC_ATR_OT_LOWER_OFFSET
    },
    {
        .name = "alm_vbram_upper", .id = eXADC_ATR_VBRAM_UPPER, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = XADC_ATR_VBRAM_UPPER_OFFSET
    },
    {
        .name = "alm_vbram_lower", .id = eXADC_ATR_VBRAM_LOWER, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = XADC_ATR_VBRAM_LOWER_OFFSET
    },
    {
        .name = "init", .id = eXADC_INIT, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "config_test", .id = eXADC_CONFIGURE_TEST, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "run_test", .id = eXADC_RUN_TEST, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, 
        .size = sizeof(board_dev_stats_t), 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
};

/**
 * This function initializes a specific XADC device/instance. 
 * It must be called prior to using the XADC device. 
 *
 * @param[in]	p_priv is a pointer to the xadc_ctx_t instance.
 *
 * @return error code
 */
static ccl_err_t _xadc_init(void *p_priv)
{
    xadc_ctx_t *p_xadc;

    /*
     * Assert the input arguments.
     */
    if ( !p_xadc ) {
        PDEBUG("Error! Invalid input parameters\n");
        XADC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_xadc = (xadc_ctx_t *)p_priv;

    return CCL_OK;
}

/** 
 *
 * This function sets the number of samples of averaging that is to be done for
 * all the channels in both the single channel mode and sequence mode of
 * operations.
 *
 * @param[in] p_xadc is a pointer to the xadc_ctx_t instance.
 * @param[in] average is the number of samples of averaging
 *       programmed to the Configuration Register 0. Use the
 *       XADC_AVG_* definitions:
 *		- XADC_AVG_0_SAMPLES for no averaging
 *		- XADC_AVG_16_SAMPLES for 16 samples of averaging
 *		- XADC_AVG_64_SAMPLES for 64 samples of averaging
 *		- XADC_AVG_256_SAMPLES for 256 samples of averaging
 *
 * @return	error code
 */
static ccl_err_t _xadc_set_avg(xadc_ctx_t *p_xadc, b_u8 average)
{
    b_u32 regval;

    /*
     * Assert the input arguments.
     */
    if ( !p_xadc || (average > XADC_AVG_256_SAMPLES) ) {
        PDEBUG("Error! Invalid input parameters\n");
        XADC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Write the averaging value into the Configuration Register 0.
     */
    XADC_REG_READ(p_xadc, XADC_CFR0_OFFSET, 
                           sizeof(b_u32), 0, 
                           (b_u8 *)&regval, sizeof(b_u32));

    regval &= ~XADC_CFR0_AVG_VALID_MASK;
    regval |=  (((b_u32) average << XADC_CFR0_AVG_SHIFT));
    XADC_REG_WRITE(p_xadc, XADC_CFR0_OFFSET, 
                            sizeof(b_u32), 0, 
                            (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/**
 * This function gets the number of samples of averaging 
 * configured for all the channels in the Configuration Register 
 * 0. 
 *
 * @param[in]	p_xadc is a pointer to the xadc_ctx_t instance.
 * @param[out] p_data is the memory that is updated with the 
 *       averaging read from the Configuration Register 0 is
 *       returned. Use the XADC_AVG_* bit definitions to
 *       interpret the returned value:
 *		- XADC_AVG_0_SAMPLES means no averaging
 *		- XADC_AVG_16_SAMPLES means 16 samples of averaging
 *		- XADC_AVG_64_SAMPLES means 64 samples of averaging
 *		- XADC_AVG_256_SAMPLES means 256 samples of averaging
 *  
 * @return error code
 */
static ccl_err_t _xadc_get_avg(xadc_ctx_t *p_xadc, b_u8 *p_data)
{
    b_u32 regval;

    /*
     * Assert the input arguments.
     */
    if ( !p_xadc || !p_data ) {
        PDEBUG("Error! Invalid input parameters\n");
        XADC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Read the averaging value from the Configuration Register 0.
     */
    XADC_REG_READ(p_xadc, XADC_CFR0_OFFSET, 
                           sizeof(b_u32), 0, 
                           (b_u8 *)&regval, sizeof(b_u32));
    regval &= XADC_CFR0_AVG_VALID_MASK;

    *p_data = (b_u8) (regval >> XADC_CFR0_AVG_SHIFT);

    return CCL_OK;
}

/**
 * This function sets the specified channel Sequencer mode in the Configuration
 * Register 1 :
 *		- Default safe mode (XADC_SEQ_MODE_SAFE)
 *		- One pass through sequence (XADC_SEQ_MODE_ONEPASS)
 *		- Continuous channel sequencing (XADC_SEQ_MODE_CONTINPASS)
 *		- Single channel/Sequencer off (XADC_SEQ_MODE_SINGCHAN)
 *		- Simulataneous sampling mode (XADC_SEQ_MODE_SIMUL_SAMPLING)
 *		- Independent mode (XADC_SEQ_MODE_INDEPENDENT)
 *
 * @param[in] p_xadc is a pointer to the xadc_ctx_t instance.
 * @param[in] mode is the sequencer mode to be set.
 *		Use XADC_SEQ_MODE_* bits
 *  
 * @return	error code
 *
 * @note Only one of the modes can be enabled at a time. 
 *       Please read the Spec of the XADC for further 
 *       information about the sequencer modes.
 */
static ccl_err_t _xadc_set_sequencer_mode(xadc_ctx_t *p_xadc, b_u8 mode)
{
    b_u32 regval;

    /*
     * Assert the arguments.
     */
    /*
     * Assert the input arguments.
     */
    if ( !p_xadc || 
         ((mode > XADC_SEQ_MODE_SIMUL_SAMPLING) && 
          (mode != XADC_SEQ_MODE_INDEPENDENT)) ) {
        PDEBUG("Error! Invalid input parameters\n");
        XADC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /*
     * Set the specified sequencer mode in the Configuration Register 1.
     */
    XADC_REG_READ(p_xadc, XADC_CFR1_OFFSET, 
                           sizeof(b_u32), 0, 
                           (b_u8 *)&regval, sizeof(b_u32));
    regval &= (~ XADC_CFR1_SEQ_VALID_MASK);
    regval |= ((mode  << XADC_CFR1_SEQ_SHIFT) &
               XADC_CFR1_SEQ_VALID_MASK);
    XADC_REG_WRITE(p_xadc, XADC_CFR1_OFFSET, 
                            sizeof(b_u32), 0, 
                            (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/** 
 * This function gets the channel sequencer mode from the Configuration
 * Register 1.
 *
 * @param[in] p_xadc is a pointer to the xadc_ctx_t instance.
 * @param[out] p_data is the memory that is updated with the
 *        channel sequencer mode :
 *		- XADC_SEQ_MODE_SAFE : Default safe mode
 *		- XADC_SEQ_MODE_ONEPASS : One pass through sequence
 *		- XADC_SEQ_MODE_CONTINPASS : Continuous channel sequencing
 *		- XADC_SEQ_MODE_SINGCHAN : Single channel/Sequencer off
 *		- XADC_SEQ_MODE_SIMUL_SAMPLING : Simulataneous sampling mode
 *		- XADC_SEQ_MODE_INDEPENDENT : Independent mode
 *
 * @return error code
 */
static ccl_err_t _xadc_get_sequencer_mode(xadc_ctx_t *p_xadc, b_u8 *p_data)
{
    b_u32 regval;

    /*
     * Assert the input arguments.
     */
    if ( !p_xadc || !p_data) {
        PDEBUG("Error! Invalid input parameters\n");
        XADC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Read the channel sequencer mode from the Configuration Register 1.
     */
    XADC_REG_READ(p_xadc, XADC_CFR1_OFFSET, 
                           sizeof(b_u32), 0, 
                           (b_u8 *)&regval, sizeof(b_u32));
    regval &= XADC_CFR1_SEQ_VALID_MASK;
    *p_data = (b_u8)(regval >> XADC_CFR1_SEQ_SHIFT);

    return CCL_OK;
}

/**
 *
 * The function sets the given parameters in the Configuration Register 0 in
 * the single channel mode.
 *
 * @param[in]	p_xadc is a pointer to the xadc_ctx_t instance.
 * @param[in]	channel is the channel number for the singel channel mode.
 *		The valid channels are 0 to 6, 8, and 13 to 31.
 *		If the external Mux is used then this specifies the channel
 *		oonnected to the external Mux. Please read the Device Spec
 *		to know which channels are valid.
 * @param[in] 	incr_acq_cycles is a boolean parameter which specifies whether
 *		the Acquisition time for the external channels has to be
 *		increased to 10 ADCCLK cycles (specify CCL_TRUE) or remain at the
 *		default 4 ADCCLK cycles (specify CCL_FALSE). This parameter is
 *		only valid for the external channels.
 * @param[in] 	is_event_mode specifies whether the operation of the ADC is Event
 * 		driven or Continuous mode.
 * @param[in] 	is_diff_mode is a boolean parameter which specifies
 *		unipolar(specify CCL_FALSE) or differential mode (specify CCL_TRUE) for
 *		the analog inputs. The 	input mode is only valid for the
 *		external channels.
 *
 * @return error code
 *
 * @note The number of samples for the averaging for all the channels
 *		 is set by using the function _xadc_SetAvg.
 *		 The calibration of the device is done by doing a ADC
 *		 conversion on the calibration channel(channel 8). The input
 *		 parameters incr_acq_cycles, is_diff_mode and
 *		 is_event_mode are not valid for this channel
 *
 */
static ccl_err_t _xadc_set_single_ch_params(xadc_ctx_t *p_xadc,
                                            b_u8 channel,
                                            b_bool incr_acq_cycles,
                                            b_bool is_event_mode,
                                            b_bool is_diff_mode)
{
    b_u32 regval;
    b_u8 mode;

    /*
     * Assert the input arguments.
     */
    if ( !p_xadc || 
         ((channel > XADC_CH_VBRAM) &&  
          (channel != XADC_CH_ADC_CALIB) &&
          ((channel < XADC_CH_VCCPINT) || 
           (channel > XADC_CH_AUX_MAX)))) {
        PDEBUG("Error! Invalid input parameters\n");
        XADC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Check if the device is in single channel mode else return failure
     */
    _ret = _xadc_get_sequencer_mode(p_xadc, &mode);
    if (_ret) {
        PDEBUG("_xadc_get_sequencer_mode error\n");
        XADC_ERROR_RETURN(_ret);
    }
    if (mode != XADC_SEQ_MODE_SINGCHAN) {
        PDEBUG("invalid sequencer mode: %d\n", mode);
        XADC_ERROR_RETURN(CCL_FAIL);
    }

    /*
     * Read the Configuration Register 0.
     */
    XADC_REG_READ(p_xadc, XADC_CFR0_OFFSET, 
                           sizeof(b_u32), 0, 
                           (b_u8 *)&regval, sizeof(b_u32));
    regval &= XADC_CFR0_AVG_VALID_MASK;

    /*
     * Select the number of acquisition cycles. The acquisition cycles is
     * only valid for the external channels.
     */
    if (incr_acq_cycles == CCL_TRUE) {
        if (((channel >= XADC_CH_AUX_MIN) &&
             (channel <= XADC_CH_AUX_MAX)) ||
            (channel == XADC_CH_VPVN)) {
            regval |= XADC_CFR0_ACQ_MASK;
        } else {
            XADC_ERROR_RETURN(CCL_FAIL);
        }
    }

    /*
     * Select the input mode. The input mode is only valid for the
     * external channels.
     */
    if (is_diff_mode == CCL_TRUE) {
        if (((channel >= XADC_CH_AUX_MIN) &&
             (channel <= XADC_CH_AUX_MAX)) ||
            (channel == XADC_CH_VPVN)) {
            regval |= XADC_CFR0_DU_MASK;
        } else {
            XADC_ERROR_RETURN(CCL_FAIL);
        }
    }

    /*
     * Select the ADC mode.
     */
    if (is_event_mode == CCL_TRUE)
        regval |= XADC_CFR0_EC_MASK;

    /*
     * Write the given values into the Configuration Register 0.
     */
    regval |= (channel & XADC_CFR0_CHANNEL_MASK);
    XADC_REG_WRITE(p_xadc, XADC_CFR0_OFFSET, 
                            sizeof(b_u32), 0, 
                            (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/**
 * This function enables the alarm outputs for the specified alarms in the
 * Configuration Register 1.
 *
 * @param[in] p_xadc is a pointer to the xadc_ctx_t instance.
 * @param[in] mask is the bit-mask of the alarm outputs to be 
 *       enabled in the Configuration Register 1. Bit positions
 *       of 1 will be enabled. Bit positions of 0 will be
 *       disabled. This mask is formed by OR'ing
 *       XADC_CFR1_ALM_*_MASK and XADC_CFR1_OT_MASK masks
 *
 * @return	error code
 *
 * @note The implementation of the alarm enables in the 
 *       Configuration register 1 is such that the alarms for bit
 *       positions of 1 will be disabled and alarms for bit
 *       positions of 0 will be enabled. The alarm outputs
 *       specified by the mask are negated before writing to the
 *       Configuration Register 1.
 */
static ccl_err_t _xadc_set_alarm_enables(xadc_ctx_t *p_xadc, b_u16 mask)
{
    b_u32 regval;

    /*
     * Assert the input arguments.
     */
    if ( !p_xadc ) {
        PDEBUG("Error! Invalid input parameters\n");
        XADC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    XADC_REG_READ(p_xadc, XADC_CFR1_OFFSET, 
                  sizeof(b_u32), 0, 
                  (b_u8 *)&regval, sizeof(b_u32));

    regval &= (b_u32)~XADC_CFR1_ALM_ALL_MASK;
    regval |= (~mask & XADC_CFR1_ALM_ALL_MASK);

    /*
     * Enable/disables the alarm enables for the specified alarm bits in the
     * Configuration Register 1.
     */
    XADC_REG_WRITE(p_xadc, XADC_CFR1_OFFSET, 
                   sizeof(b_u32), 0, 
                   (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/**
 * This function gets the status of the alarm output enables in the
 * Configuration Register 1.
 *
 * @param[in] p_xadc is a pointer to the xadc_ctx_t instance.
 * @param[out] p_data is the memory that is updated with
 *      the bit-mask of the enabled alarm outputs in the
 *      Configuration Register 1. Use the masks XADC_CFR1_ALM*_*
 *      and XADC_CFR1_OT_MASK to interpret the returned value.
 *      Bit positions of 1 indicate that the alarm output is
 *      enabled. Bit positions of 0 indicate that the alarm
 *      output is disabled.
 * 
 * @return	error code
 *
 *  @note The implementation of the alarm enables in the
 *       Configuration register 1 is such that alarms for the bit
 *       positions of 1 will be disabled and alarms for bit
 *       positions of 0 will be enabled. The enabled alarm
 *       outputs returned by this function is the negated value
 *       of the the data read from the Configuration Register 1.
 */
static ccl_err_t _xadc_get_alarm_enables(xadc_ctx_t *p_xadc, b_u16 *p_data)
{
    b_u32 regval;

    /*
     * Assert the input arguments.
     */
    if ( !p_xadc || !p_data) {
        PDEBUG("Error! Invalid input parameters\n");
        XADC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Read the status of alarm output enables from the Configuration
     * Register 1.
     */
    XADC_REG_READ(p_xadc, XADC_CFR1_OFFSET,
                  sizeof(b_u32), 0, 
                  (b_u8 *)&regval, sizeof(b_u32));

    regval &= XADC_CFR1_ALM_ALL_MASK;
    *p_data = (b_u16) (~regval & XADC_CFR1_ALM_ALL_MASK);

    return CCL_OK;
}

/**
 * This function enables the specified calibration in the Configuration
 * Register 1 :
 *
 * - XADC_CFR1_CAL_ADC_OFFSET_MASK : calibration 0 -ADC offset correction
 * - XADC_CFR1_CAL_ADC_GAIN_OFFSET_MASK : calibration 1 -ADC gain and offset
 *						correction
 * - XADC_CFR1_CAL_PS_OFFSET_MASK : calibration 2 -Power Supply sensor
 *					offset correction
 * - XADC_CFR1_CAL_PS_GAIN_OFFSET_MASK : calibration 3 -Power Supply sensor
 *						gain and offset correction
 * - XADC_CFR1_CAL_DISABLE_MASK : No calibration
 *
 * @param[in] p_xadc is a pointer to the xadc_ctx_t instance.
 * @param[in] calibration is the calibration to be applied.
 *		Use XADC_CFR1_CAL*_* bits.
 *		Multiple calibrations can be enabled at a time by oring the
 *		XADC_CFR1_CAL_ADC_* and XADC_CFR1_CAL_PS_* bits.
 *		calibration can be disabled by specifying
 *		XADC_CFR1_CAL_DISABLE_MASK;
 *
 * @return	error code
 */
static ccl_err_t _xadc_set_calib_enables(xadc_ctx_t *p_xadc, b_u16 calibration)
{
    b_u32 regval;

    /*
     * Assert the input arguments.
     */
    if ( !p_xadc || 
         (((calibration < XADC_CFR1_CAL_ADC_OFFSET_MASK) ||
           (calibration > XADC_CFR1_CAL_VALID_MASK)) &&
          (calibration != XADC_CFR1_CAL_DISABLE_MASK)) ) {
        PDEBUG("Error! Invalid input parameters\n");
        XADC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Set the specified calibration in the Configuration Register 1.
     */
    XADC_REG_READ(p_xadc, XADC_CFR1_OFFSET,
                  sizeof(b_u32), 0, 
                  (b_u8 *)&regval, sizeof(b_u32));

    regval &= (~ XADC_CFR1_CAL_VALID_MASK);
    regval |= (calibration & XADC_CFR1_CAL_VALID_MASK);
    XADC_REG_WRITE(p_xadc, XADC_CFR1_OFFSET,
                   sizeof(b_u32), 0, 
                   (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/**
 * This function reads the value of the calibration enables from the
 * Configuration Register 1.
 *
 * @param[in] p_xadc is a pointer to the xadc_ctx_t instance.
 * @param[out] p_data is the memory that is updated with value of
 *       the calibration enables in the Configuration Register 1:
 *		- XADC_CFR1_CAL_ADC_OFFSET_MASK : ADC offset correction
 *		- XADC_CFR1_CAL_ADC_GAIN_OFFSET_MASK : ADC gain and offset
 *				correction
 *		- XADC_CFR1_CAL_PS_OFFSET_MASK : Power Supply sensor offset
 *				correction
 *		- XADC_CFR1_CAL_PS_GAIN_OFFSET_MASK : Power Supply sensor
 *				gain and offset correction
 *		- XADC_CFR1_CAL_DISABLE_MASK : No calibration
 * 
 * @return error code
 */
static ccl_err_t _xadc_get_calib_enables(xadc_ctx_t *p_xadc, b_u16 *p_data)
{
    b_u32 regval;

    /*
     * Assert the input arguments.
     */
    if ( !p_xadc || !p_data) {
        PDEBUG("Error! Invalid input parameters\n");
        XADC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Read the calibration enables from the Configuration Register 1.
     */
    XADC_REG_READ(p_xadc, XADC_CFR1_OFFSET,
                  sizeof(b_u32), 0, 
                  (b_u8 *)&regval, sizeof(b_u32));

    regval &= XADC_CFR1_CAL_VALID_MASK;
    *p_data = (b_u16)regval;

    return CCL_OK;
}

/**
 * The function sets the frequency of the ADCCLK by configuring the DCLK to
 * ADCCLK ratio in the Configuration Register #2
 *
 * @param[in] p_xadc is a pointer to the xadc_ctx_t instance.
 * @param[in] divisor is clock divisor used to derive ADCCLK from
 *       DCLK. Valid values of the divisor are
 *		 - 0 to 255. values 0, 1, 2 are all mapped to 2.
 *		Refer to the device specification for more details
 *
 * @return	error code
 *
 * @note  The ADCCLK is an int clock used by the ADC and is
 *		  synchronized to the DCLK clock. The ADCCLK is equal to DCLK
 *        divided by the user selection in the Configuration
 *        Register 2. There is no Assert on the minimum value of
 *        the divisor.
 */
static ccl_err_t _xadc_set_adc_clk_divisor(xadc_ctx_t *p_xadc, b_u8 divisor)
{
    b_u32 regval;

    /*
     * Assert the input arguments.
     */
    if ( !p_xadc ) {
        PDEBUG("Error! Invalid input parameters\n");
        XADC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Write the divisor value into the Configuration Register #2.
     */
    regval = divisor << XADC_CFR2_CD_SHIFT;
    XADC_REG_WRITE(p_xadc, XADC_CFR2_OFFSET, 
                   sizeof(b_u32), 0, 
                   (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/**
 * The function gets the ADCCLK divisor from the Configuration Register 2.
 *
 * @param[in]	p_xadc is a pointer to the xadc_ctx_t instance.
 * @param[out] p_data is the memory that is updated with the 
 *       divisor read from the Configuration Register 2.
 *  
 * @return error code
 *
 * @note The ADCCLK is an int clock used by the ADC and is
 *		synchronized to the DCLK clock. The ADCCLK is equal to DCLK
 *		divided by the user selection in the Configuration Register 2.
 */
static ccl_err_t _xadc_get_adc_clk_divisor(xadc_ctx_t *p_xadc, b_u8 *p_data)
{
    b_u32 regval;

    /*
     * Assert the input arguments.
     */
    if ( !p_xadc || !p_data ) {
        PDEBUG("Error! Invalid input parameters\n");
        XADC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Read the divisor value from the Configuration Register 2.
     */
    XADC_REG_READ(p_xadc, XADC_CFR2_OFFSET,
                  sizeof(b_u32), 0, 
                  (b_u8 *)&regval, sizeof(b_u32));

    *p_data =  (b_u8)(regval >> XADC_CFR2_CD_SHIFT);

    return CCL_OK;
}

/**
 *
 * This function enables programming of the powerdown temperature for the
 * OverTemp signal in the OT Powerdown register.
 *
 * @param[in]	p_xadc is a pointer to the xadc_ctx_t instance.
 *
 * @return	error code
 *
 */
static ccl_err_t _xadc_enable_user_over_temp(xadc_ctx_t *p_xadc)
{
    b_u32 regval;

    /*
     * Assert the input arguments.
     */
    if ( !p_xadc ) {
        PDEBUG("Error! Invalid input parameters\n");
        XADC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Read the OT upper Alarm Threshold Register.
     */
    XADC_REG_READ(p_xadc, XADC_ATR_OT_UPPER_OFFSET,
                  sizeof(b_u32), 0, 
                  (b_u8 *)&regval, sizeof(b_u32));

    regval &= ~(XADC_ATR_OT_UPPER_ENB_MASK);

    /*
     * Preserve the powerdown value and write OT enable value the into the
     * OT Upper Alarm Threshold Register.
     */
    regval |= XADC_ATR_OT_UPPER_ENB_VAL;

    XADC_REG_WRITE(p_xadc, XADC_ATR_OT_UPPER_OFFSET, 
                   sizeof(b_u32), 0, 
                   (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/**
 *
 * This function disables programming of the powerdown temperature for the
 * OverTemp signal in the OT Powerdown register.
 *
 * @param[in]	p_xadc is a pointer to the xadc_ctx_t instance.
 *
 * @return	error code
 *
 */
static ccl_err_t _xadc_disable_user_over_temp(xadc_ctx_t *p_xadc)
{
    b_u32 regval;

    /*
     * Assert the input arguments.
     */
    if ( !p_xadc ) {
        PDEBUG("Error! Invalid input parameters\n");
        XADC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Read the OT Upper Alarm Threshold Register.
     */
    XADC_REG_READ(p_xadc, XADC_ATR_OT_UPPER_OFFSET, 
                  sizeof(b_u32), 0, 
                  (b_u8 *)&regval, sizeof(b_u32));
    regval &= ~XADC_ATR_OT_UPPER_ENB_MASK;
    XADC_REG_WRITE(p_xadc, XADC_ATR_OT_UPPER_OFFSET,
                   sizeof(b_u32), 0, 
                   (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/**
 * The function enables the Event mode or Continuous mode in the sequencer mode.
 *
 * @param[in] p_xadc is a pointer to the xadc_ctx_t instance.
 *  @param[in] is_event_mode is a boolean parameter that
 *       specifies continuous sampling (specify CCL_FALSE) or
 *       event driven sampling mode (specify CCL_TRUE) for the
 *       given channel.
 *
 * @return	error code
 *
 */
static ccl_err_t _xadc_set_sequencer_event(xadc_ctx_t *p_xadc, 
                                           b_bool is_event_mode)
{
    b_u32 regval;

    /*
     * Assert the input arguments.
     */
    if ( !p_xadc ) {
        PDEBUG("Error! Invalid input parameters\n");
        XADC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Read the Configuration Register 0.
     */
    XADC_REG_READ(p_xadc, XADC_CFR0_OFFSET, 
                  sizeof(b_u32), 0, 
                  (b_u8 *)&regval, sizeof(b_u32));
    regval &= ~XADC_CFR0_EC_MASK;

    /*
     * Set the ADC mode.
     */
    if (is_event_mode == CCL_TRUE)
        regval |= XADC_CFR0_EC_MASK;
    else
        regval &= ~XADC_CFR0_EC_MASK;

    XADC_REG_WRITE(p_xadc, XADC_CFR0_OFFSET, 
                   sizeof(b_u32), 0, 
                   (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/**
 * This function return the sampling mode.
 *
 * @param[in] p_xadc is a pointer to the xadc_ctx_t instance.
 * @param[out] p_data is the memory that is updated with the sampling mode
 *		- 0 specifies continuous sampling
 *		- 1 specifies event driven sampling mode
 *
 * @return error code
 */
static ccl_err_t _xadc_get_sampling_mode(xadc_ctx_t *p_xadc, b_u32 *p_data)
{
    b_u32 regval;

    /*
     * Assert the input arguments.
     */
    if ( !p_xadc || !p_data ) {
        PDEBUG("Error! Invalid input parameters\n");
        XADC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Read the sampling mode from the Configuration Register 0.
     */
    XADC_REG_READ(p_xadc, XADC_CFR0_OFFSET, 
                  sizeof(b_u32), 0, 
                  (b_u8 *)&regval, sizeof(b_u32));
    regval &= XADC_CFR0_EC_MASK;
    *p_data = !!regval;

    return CCL_OK;
}

/**
 * This function sets the External Mux mode.
 *
 * @param[in]	p_xadc is a pointer to the xadc_ctx_t instance.
 * @param[in] 	mode specifies whether External Mux is used
 *		- CCL_FALSE specifies NO external MUX
 *		- CCL_TRUE specifies External Mux is used
 * @param[in]	channel specifies the channel to be used for the
 *		external Mux. Please read the Device Spec for which
 *		channels are valid for which mode.
 *
 * @return	error code
 */
static ccl_err_t _xadc_set_mode(xadc_ctx_t *p_xadc, b_bool mode, b_u8 channel)
{
    b_u32 regval;

    /*
     * Assert the input arguments.
     */
    if ( !p_xadc ) {
        PDEBUG("Error! Invalid input parameters\n");
        XADC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Read the Configuration Register 0.
     */
    XADC_REG_READ(p_xadc, XADC_CFR0_OFFSET, 
                  sizeof(b_u32), 0, 
                  (b_u8 *)&regval, sizeof(b_u32));
    regval &= ~XADC_CFR0_MUX_MASK;

    /*
     * Select the Mux mode and the channel to be used.
     */
    if (mode == CCL_TRUE) {
        regval |= (channel & XADC_CFR0_CHANNEL_MASK) | 
                  XADC_CFR0_MUX_MASK;
    }

    /*
     * Write the mux mode into the Configuration Register 0.
     */
    XADC_REG_WRITE(p_xadc, XADC_CFR0_OFFSET, 
                   sizeof(b_u32), 0, 
                   (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/**
 * This function sets the Power Down mode.
 *
 * @param[in] p_xadc is a pointer to the xadc_ctx_t instance.
 * @param[in] mode specifies the Power Down mode
 *		- XADC_PD_MODE_NONE specifies NO Power Down (Both ADC A and
 *		ADC B are enabled)
 *		- XADC_PD_MODE_ADCB specfies the Power Down of ADC B
 *		- XADC_PD_MODE_XADC specifies the Power Down of
 *		both ADC A and ADC B.
 *
 * @return	error code
 */
static ccl_err_t _xadc_set_powerdown_mode(xadc_ctx_t *p_xadc, b_u32 mode)
{
    b_u32 regval;

    /*
     * Assert the input arguments.
     */
    if ( !p_xadc ||
         (mode >= XADC_PD_MODE_XADC) ) {
        PDEBUG("Error! Invalid input parameters\n");
        XADC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Read the Configuration Register 2.
     */
    XADC_REG_READ(p_xadc, XADC_CFR2_OFFSET, 
                  sizeof(b_u32), 0, 
                  (b_u8 *)&regval, sizeof(b_u32));
    regval &= ~XADC_CFR2_PD_MASK;
    /*
     * Select the Power Down mode.
     */
    regval |= (mode << XADC_CFR2_PD_SHIFT);

    XADC_REG_WRITE(p_xadc, XADC_CFR2_OFFSET, 
                   sizeof(b_u32), 0, 
                   (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/**
 * This function gets the Power Down mode.
 *
 * @param[in] p_xadc is a pointer to the xadc_ctx_t instance.
 * @param[out] p_data is the memory that is updated with the Power Down mode
 *		- XADC_PD_MODE_NONE specifies NO Power Down (Both ADC A and
 *		ADC B are enabled)
 *		- XADC_PD_MODE_ADCB specfies the Power Down of ADC B
 *		- XADC_PD_MODE_XADC specifies the Power Down of
 *		both ADC A and ADC B.
 * 
 * @return error code
 */
static ccl_err_t _xadc_get_powerdown_mode(xadc_ctx_t *p_xadc, b_u32 *p_data)
{
    b_u32 regval;

    /*
     * Assert the input arguments.
     */
    if ( !p_xadc || !p_data ) {
        PDEBUG("Error! Invalid input parameters\n");
        XADC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    /*
     * Read the Power Down mode.
     */
    XADC_REG_READ(p_xadc, XADC_CFR2_OFFSET, 
                  sizeof(b_u32), 0, 
                  (b_u8 *)&regval, sizeof(b_u32));
    regval &= ~XADC_CFR2_PD_MASK;
    /*
     * Return the Power Down mode.
     */
    *p_data =  (regval >> XADC_CFR2_PD_SHIFT);

    return CCL_OK;
}

static ccl_err_t _fxadc_cmd_init(devo_t devo, 
                                 __attribute__((unused)) device_cmd_parm_t parms[], 
                                 __attribute__((unused)) b_u16 parms_num)
{
    void *p_priv = devman_device_private(devo);

    /* start the device */
    _ret = _xadc_init(p_priv);
    if ( _ret ) {
        PDEBUG("_xadc_init error\n");
        XADC_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

/* device commands */
static device_cmd_t _cmds[] = 
{
    { 
        .name = "init", .f_cmd = _fxadc_cmd_init, .parms_num = 0,
        .help = "Init XADC device" 
    },
};

/* Device instance hw-specific initializations 
 */
static ccl_err_t _fxadc_add(void *devo, void *brdspec) {
    xadc_ctx_t       *p_xadc;
    xadc_brdspec_t   *p_brdspec;

    if ( !devo || !brdspec ) {
        PDEBUG("Error! Invalid input parameters\n");
        XADC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_xadc = devman_device_private(devo);
    if ( !p_xadc ) {
        PDEBUG("NULL context area\n");
        XADC_ERROR_RETURN(CCL_FAIL);
    }
    p_brdspec = (xadc_brdspec_t *)brdspec;
    /* back reference the device manager object handle */    
    p_xadc->devo = devo;    
    /* save the board specific info */
    memcpy(&p_xadc->brdspec, p_brdspec, sizeof(xadc_brdspec_t));

    return CCL_OK;
}

/* Device instance hw-specific deinitializations 
 */
static ccl_err_t _fxadc_cut(void *devo)
{
    xadc_ctx_t  *p_xadc;

    if ( !devo ) {
        PDEBUG("Error! Invalid input parameters\n");
        XADC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_xadc = devman_device_private(devo);
    if ( !p_xadc ) {
        PDEBUG("NULL context area\n");
        XADC_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

/* device type description structure */
static device_driver_t _xadc_driver = {
    .name = "xadc",
    .f_add = _fxadc_add,
    .f_cut = _fxadc_cut,
    .attrs = _attrs,
    .attrs_num = sizeof(_attrs)/sizeof(_attrs[0]),
    .cmds = _cmds,
    .cmds_num = sizeof(_cmds)/sizeof(_cmds[0]),
    .priv_size = sizeof(xadc_ctx_t)
}; 

/* Initialize XADC device type
 */
ccl_err_t devxadc_init(void)
{
    /* register device type */
    _ret = devman_driver_register(&_xadc_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to register driver\n");
        XADC_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}

/* Deinitialize XADC device type
 */
ccl_err_t devxadc_fini(void)
{
    /* unregister device type */
    _ret = devman_driver_unregister(&_xadc_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to unregister driver\n");
        XADC_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}
