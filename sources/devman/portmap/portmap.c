/********************************************************************************/
/**
 * @file portmap.c
 * @brief PORTMAP device manager driver 
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <semaphore.h>
#include <linux/limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devman_port.h"
#include "devman_brd_gen.h"
#include "devman_brd_media.h"
#include "devboard.h"
#include "portmap.h"

#define PORTMAP_DEBUG
/* printing/error-returning macros */
#ifdef PORTMAP_DEBUG
    #define PDEBUG(fmt, args...) \
         ccl_syslog_err("PORTMAP: %s[%d]: " fmt, __FUNCTION__, __LINE__, ## args)
#else
    #define PDEBUG(fmt, args...)
#endif
#define PORTMAP_ERROR_RETURN(err) do { \
   ccl_syslog_err("Error! %s[%d], error=%i\n", __FUNCTION__, __LINE__, (err)); \
   return err; \
} while (0)

extern ccl_err_t media_data_layout_get(b_u8 media_id, 
                                       devman_media_data_block_t **pmedia_data_layout);
extern ccl_err_t media_parameters_parse(b_u8 media_id, b_u8 *buf, 
                                        b_u32 start_offset, b_u32 buf_len, 
                                        devman_media_description_t *p_media_descr);
extern ccl_err_t media_ddm_parameters_parse(b_u8 media_id, b_u8 buf, 
                                            devman_media_diag_type_t *pddm);
extern ccl_err_t media_digital_diagnostic_params_parse(b_u8 media_id, IN char *buf, 
                                                       devman_media_diag_param_t *pddm);
extern ccl_err_t media_digital_diagnostic_status(b_u8 media_id, b_u32 channel_id, b_u8 *buf, 
                                                 devman_media_diag_status_t *pdiag_status);
extern ccl_err_t media_link_type_get(devman_media_description_t *pdescr, 
                                     devman_media_link_type_t *plink_type);
/** @struct portmap_ctx_t
 *  portmap device internal control structure
 */
typedef struct portmap_ctx_t {
    void                *devo;
    portmap_brdspec_t    brdspec;
} portmap_ctx_t;

static ccl_err_t _ret;

/** portmap attributes definition */
static device_attr_t _attrs[] = {
    {
        .name = "maxnum", 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset = offsetof(struct portmap_ctx_t, brdspec.ports_num),
        .format = "%d"
    },
    {
        .name = "type", 
        .type = eDEVMAN_ATTR_WORD, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .offset = offsetof(struct portmap_ctx_t, brdspec.port_types),
        .format = "%d"
    },
    {
        .name = "name", 
        .type = eDEVMAN_ATTR_STRING, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .offset = offsetof(struct portmap_ctx_t, brdspec.name),
        .size = DEVMAN_DEVNAME_LEN,
        .format = "%s"
    },
    {
        .name = "offset", 
        .type = eDEVMAN_ATTR_WORD, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .offset = offsetof(struct portmap_ctx_t, brdspec.offset),
        .format = "0x%08x"
    },
    {
        .name = "trans_id", .id = ePORTMAP_TRANS_ID,
        .type = eDEVMAN_ATTR_WORD, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .offset_sz = sizeof(b_u8), .format = "%d"
    },
    {
        .name = "led", .id = ePORTMAP_LED, 
        .type = eDEVMAN_ATTR_BOOL, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .offset_sz = sizeof(b_u8), .offset = 0
    },
    {
        .name = "enable", .id = ePORTMAP_ENABLE, 
        .type = eDEVMAN_ATTR_BOOL, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .offset_sz = sizeof(b_u8), .offset = 0
    },
    {
        .name = "reset", .id = ePORTMAP_RESET, 
        .type = eDEVMAN_ATTR_BOOL, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .offset_sz = sizeof(b_u8), .offset = 0,
        .flags = eDEVMAN_ATTRFLAG_WO 
    },
    {
        .name = "tx_fault", .id = ePORTMAP_TX_FAULT, 
        .type = eDEVMAN_ATTR_BOOL, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .offset_sz = sizeof(b_u8), .offset = 0
    },
    {
        .name = "rx_los", .id = ePORTMAP_RX_LOS, 
        .type = eDEVMAN_ATTR_BOOL, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .offset_sz = sizeof(b_u8), .offset = 0
    },
    {
        .name = "rs0", .id = ePORTMAP_RS0, 
        .type = eDEVMAN_ATTR_BOOL, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .offset_sz = sizeof(b_u8), .offset = 0
    },
    {
        .name = "rs1", .id = ePORTMAP_RS1, 
        .type = eDEVMAN_ATTR_BOOL, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .offset_sz = sizeof(b_u8), .offset = 0
    },
    {
        .name = "modsel", .id = ePORTMAP_MODULE_SELECT, 
        .type = eDEVMAN_ATTR_BOOL, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .offset_sz = sizeof(b_u8), .offset = 0
    },
    {
        .name = "lpmode", .id = ePORTMAP_LPMODE, 
        .type = eDEVMAN_ATTR_BOOL, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .offset_sz = sizeof(b_u8), .offset = 0
    },
    {
        .name = "media_pres", .id = ePORTMAP_MEDIA_PRESENT, 
        .type = eDEVMAN_ATTR_BOOL, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .offset_sz = sizeof(b_u8), .offset = 0
    },
    {
        .name = "media_id", .id = ePORTMAP_MEDIA_ID, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .offset_sz = sizeof(b_u8), .offset = 0
    },
    {
        .name = "media_ddtype", .id = ePORTMAP_MEDIA_DDTYPE, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .offset_sz = sizeof(b_u8), .offset = 0
    },
    {
        .name = "media_msa", .id = ePORTMAP_MEDIA_MSA, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = 128, .offset_sz = sizeof(b_u8), .offset = 0x00
    },
    {
        .name = "media_dd", .id = ePORTMAP_MEDIA_DD, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = 128, .offset_sz = sizeof(b_u8), .offset = 0x00
    },
    {
        .name = "media_phy", .id = ePORTMAP_MEDIA_PHY, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = 64, .offset_sz = sizeof(b_u8), .offset = 0x00
    },
    {
        .name = "init", .id = ePORTMAP_INIT, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "config_test", .id = ePORTMAP_CONFIGURE_TEST, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "run_test", .id = ePORTMAP_RUN_TEST, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(port_stats_t), 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "counters_reset", .id = ePORTMAP_COUNTERS_RESET, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "rx_bytes", .id = ePORTMAP_COUNTER_RX_BYTES, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_BYTEREC,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_bytes", .id = ePORTMAP_COUNTER_TX_BYTES, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_BYTSENT,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packets", .id = ePORTMAP_COUNTER_RX_PACKETS, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_FRAREC,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_packets", .id = ePORTMAP_COUNTER_TX_PACKETS, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_FRASENT,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_total_bytes", .id = ePORTMAP_COUNTER_RX_TOTAL_BYTES, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_TOTALBYTEREC,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_total_packets", .id = ePORTMAP_COUNTER_RX_TOTAL_PACKETS, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_TOTALFRAMEREC,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_bcast_packets", .id = ePORTMAP_COUNTER_RX_BCAST_PACKETS, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_BRDFRAREC,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_mcast_packets", .id = ePORTMAP_COUNTER_RX_MCAST_PACKETS, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_MULFRAREC,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_crc_errors", .id = ePORTMAP_COUNTER_RX_CRC_ERRORS, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_CRCERR,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "oversize", .id = ePORTMAP_COUNTER_OVERSIZE, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_FRAOVERSIZE,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "fragments", .id = ePORTMAP_COUNTER_FRAGMENTS, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_FRAFRAGMENTS,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "jabber", .id = ePORTMAP_COUNTER_JABBER, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_JABBER,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "collisions", .id = ePORTMAP_COUNTER_COLLISIONS, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_COLL,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "late_collistions", .id = ePORTMAP_COUNTER_LATE_COLLISIONS, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_LATECOL,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_64_bytes", .id = ePORTMAP_COUNTER_RX_PKT_64, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_FRA64,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_65_127_bytes", .id = ePORTMAP_COUNTER_RX_PKT_65_127, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_FRA65T127,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_128_255_bytes", .id = ePORTMAP_COUNTER_RX_PKT_128_255, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_FRA128T255,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_256_511_bytes", .id = ePORTMAP_COUNTER_RX_PKT_256_511, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_FRA256T511,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_512_1023_bytes", .id = ePORTMAP_COUNTER_RX_PKT_512_1023, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_FRA512T1023,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_packet_1024_1522_bytes", .id = ePORTMAP_COUNTER_RX_PKT_1024_1522, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_FRA1024T1522,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_errors", .id = ePORTMAP_COUNTER_RX_ERRORS, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_MACRXERR,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_dropped", .id = ePORTMAP_COUNTER_RX_DROPPED, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_DROPPED,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_ucast_packets", .id = ePORTMAP_COUNTER_RX_UCAST_PACKETS, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_INUCASTPKT,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_noucast_packets", .id = ePORTMAP_COUNTER_RX_NOUCAST_PACKETS, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_INNOUCASTPKT,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_errors", .id = ePORTMAP_COUNTER_TX_ERRORS, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_OUTERRORS,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_noucast_packets", .id = ePORTMAP_COUNTER_TX_NOUCAST_PACKETS, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_OUTNOUCASTPKT,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_ucast_packets", .id = ePORTMAP_COUNTER_TX_UCAST_PACKETS, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_OUTUCASTPKT,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_mcast_packets", .id = ePORTMAP_COUNTER_TX_MCAST_PACKETS, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_MULTIOUT,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_bcast_packets", .id = ePORTMAP_COUNTER_TX_BCAST_PACKETS, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_BROADOUT,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_undersize_packets", .id = ePORTMAP_COUNTER_RX_UNDERSIZE_PACKETS, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_UNDERSIZ,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_uknown_protocols", .id = ePORTMAP_COUNTER_RX_UNKNOWN_PACKETS, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_INUNKNOWNPROTOS,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_alignment_errors", .id = ePORTMAP_COUNTER_RX_ALIGNMENT_PACKETS, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_ALIGN_ERR,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_fcs_errors", .id = ePORTMAP_COUNTER_RX_FCS_ERRORS, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_FCS_ERR,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "sqetest_errors", .id = ePORTMAP_COUNTER_SQETEST_ERRORS, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_SQETEST_ERR,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "cse_errors", .id = ePORTMAP_COUNTER_CSE_ERRORS, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_CSE_ERR,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "symbol_errors", .id = ePORTMAP_COUNTER_SYMBOL_ERRORS, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_SYMBOL_ERR,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_mac_errors", .id = ePORTMAP_COUNTER_TX_MAC_ERRORS, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_MACTX_ERR,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_mac_errors", .id = ePORTMAP_COUNTER_RX_MAC_ERRORS, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_MACRX_ERR,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "too_long_errors", .id = ePORTMAP_COUNTER_TOO_LONG_ERRORS, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_TOOLONGFRA,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "single_collisions", .id = ePORTMAP_COUNTER_SINGLE_COLLISIONS, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_SNGL_COLLISION,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "multi_collisions", .id = ePORTMAP_COUNTER_MULTI_COLLISIONS, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_MULT_COLLISION,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "excess_collisions", .id = ePORTMAP_COUNTER_EXCESS_COLLISIONS, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_EXCESS_COLLISION,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_unknown_opcode", .id = ePORTMAP_COUNTER_RX_UNKNOWN_OPCODE, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_INUNKNOWNOPCODE,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_deferred", .id = ePORTMAP_COUNTER_TX_DEFERRED, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_DEFERREDTX,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_pause_packets", .id = ePORTMAP_COUNTER_RX_PAUSE_PACKETS, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_INPAUSE_FRAMES,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_pause_packets", .id = ePORTMAP_COUNTER_TX_PAUSE_PACKETS, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_OUTPAUSE_FRAMES,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_oversize", .id = ePORTMAP_COUNTER_RX_OVERSIZE, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_INFRAOVERSIZE,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_oversize", .id = ePORTMAP_COUNTER_TX_OVERSIZE, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_OUTFRAOVERSIZE,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_fragments", .id = ePORTMAP_COUNTER_RX_FRAGMENTS, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_INFRAFRAGMENTS,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_fragments", .id = ePORTMAP_COUNTER_TX_FRAGMENTS, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_OUTFRAFRAGMENTS,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_jabber", .id = ePORTMAP_COUNTER_RX_JABBER, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_INJABBERS,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_jabber", .id = ePORTMAP_COUNTER_TX_JABBER, 
        .type = eDEVMAN_ATTR_DOUBLE, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = sizeof(b_u64), .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MIB_OUTJABBERS,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "if_name", .id = ePORTMAP_IF_NAME, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = 128, .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_NAME
    },
    {
        .name = "if_mtu", .id = ePORTMAP_IF_MTU, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = 128, .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MTU
    },
    {
        .name = "if_speed_max", .id = ePORTMAP_IF_SPEED_MAX, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = 128, .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_SPEED_MAX
    },
    {
        .name = "if_speed", .id = ePORTMAP_IF_SPEED, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = 128, .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_SPEED
    },
    {
        .name = "if_duplex", .id = ePORTMAP_IF_DUPLEX, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = 128, .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_DUPLEX
    },
    {
        .name = "if_autoneg", .id = ePORTMAP_IF_AUTONEG, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = 128, .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_AUTONEG
    },
    {
        .name = "if_encap", .id = ePORTMAP_IF_ENCAP, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = 128, .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_ENCAP
    },
    {
        .name = "if_medium", .id = ePORTMAP_IF_MEDIUM, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = 128, .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MEDIUM
    },
    {
        .name = "if_interface_type", .id = ePORTMAP_IF_INTERFACE_TYPE, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = 128, .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_INTERFACE_TYPE
    },
    {
        .name = "if_mdix", .id = ePORTMAP_IF_MDIX, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = 128, .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MDIX
    },
    {
        .name = "if_mdix_status", .id = ePORTMAP_IF_MDIX_STATUS, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = 128, .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MDIX_STATUS
    },
    {
        .name = "if_learn", .id = ePORTMAP_IF_LEARN, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = 128, .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_LEARN
    },
    {
        .name = "if_ifilter", .id = ePORTMAP_IF_IFILTER, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = 128, .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_IFILTER
    },
    {
        .name = "if_self_efilter", .id = ePORTMAP_IF_SELF_EFILTER, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = 128, .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_SELF_EFILTER
    },
    {
        .name = "if_lock", .id = ePORTMAP_IF_LOCK, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = 128, .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_LOCK
    },
    {
        .name = "if_drop_on_lock", .id = ePORTMAP_IF_DROP_ON_LOCK, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = 128, .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_DROP_ON_LOCK
    },
    {
        .name = "if_fwd_unk", .id = ePORTMAP_IF_FWD_UNK, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = 128, .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_FWD_UNK
    },
    {
        .name = "if_bcast_limit", .id = ePORTMAP_IF_BCAST_LIMIT, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = 128, .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_BCAST_LIMIT
    },
    {
        .name = "if_ability", .id = ePORTMAP_IF_ABILITY, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = 128, .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_ABILITY
    },
    {
        .name = "if_flow_control", .id = ePORTMAP_IF_FLOWCONTROL, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = 128, .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_FLOWCONTROL
    },
    {
        .name = "if_enable", .id = ePORTMAP_IF_ENABLE, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = 128, .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_ENABLE
    },
    {
        .name = "if_sfp_present", .id = ePORTMAP_IF_SFP_PRESENT, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = 128, .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_SFP_PRESENT
    },
    {
        .name = "if_sfp_tx_state", .id = ePORTMAP_IF_SFP_TX_STATE, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = 128, .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_SFP_TX_STATE
    },
    {
        .name = "if_sfp_rx_state", .id = ePORTMAP_IF_SFP_RX_STATE, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = 128, .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_SFP_RX_STATE
    },
    {
        .name = "if_default_vlan", .id = ePORTMAP_IF_DEFAULT_VLAN, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = 128, .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_DEFAULT_VLAN
    },
    {
        .name = "if_priority", .id = ePORTMAP_IF_PRIORITY, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = 128, .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_PRIORITY
    },
    {
        .name = "if_led_state", .id = ePORTMAP_IF_LED_STATE, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = 128, .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_LED_STATE
    },
    {
        .name = "if_media_parameters", .id = ePORTMAP_IF_MEDIA_PARAMETERS, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = 128, .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MEDIA_PARAMETERS
    },
    {
        .name = "if_media_details", .id = ePORTMAP_IF_MEDIA_DETAILS, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = 128, .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MEDIA_DETAILS
    },
    {
        .name = "if_local_advert", .id = ePORTMAP_IF_LOCAL_ADVERT, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = 128, .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_LOCAL_ADVERT
    },
    {
        .name = "if_media_type", .id = ePORTMAP_IF_MEDIA_TYPE, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = 128, .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MEDIA_TYPE
    },
    {
        .name = "if_sfp_port_type", .id = ePORTMAP_IF_SFP_PORT_TYPE, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = 128, .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_SFP_PORT_TYPE
    },
    {
        .name = "if_media_config_type", .id = ePORTMAP_IF_MEDIA_CONFIG_TYPE, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = 128, .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MEDIA_CONFIG_TYPE
    },
    {
        .name = "if_media_error_config_type", .id = ePORTMAP_IF_MEDIA_ERROR_CONFIG_TYPE, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = 128, .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MEDIA_ERROR_CONFIG_TYPE
    },
    {
        .name = "if_media_sgmii_fiber", .id = ePORTMAP_IF_MEDIA_SGMII_FIBER, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = 128, .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_MEDIA_SGMII_FIBER
    },
    {
        .name = "if_tx_enable", .id = ePORTMAP_IF_TX_ENABLE, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = 128, .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_TX_ENABLE
    },
    {
        .name = "if_sfp_port_numbers", .id = ePORTMAP_IF_SFP_PORT_NUMBERS, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = 128, .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_SFP_PORT_NUMBERS
    },
    {
        .name = "if_autoneg_duplex", .id = ePORTMAP_IF_AUTONEG_DUPLEX, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = 128, .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_AUTONEG_DUPLEX
    },
    {
        .name = "if_autoneg_speed", .id = ePORTMAP_IF_AUTONEG_SPEED, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = 128, .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_AUTONEG_SPEED
    },
    {
        .name = "if_ddm_parameters", .id = ePORTMAP_IF_DDM_PARAMETERS, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = 128, .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_DDM_PARAMETERS
    },
    {
        .name = "if_ddm_status", .id = ePORTMAP_IF_DDM_STATUS, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = 128, .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_DDM_STATUS
    },
    {
        .name = "if_ddm_supported", .id = ePORTMAP_IF_DDM_SUPPORTED, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = 128, .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_DDM_SUPPORTED
    },
    {
        .name = "if_hwaddress_get", .id = ePORTMAP_IF_HWADDRESS_GET, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = 128, .offset_sz = sizeof(b_u32),
        .offset = DEVMAN_IF_HWADDRESS_GET
    },
    {
        .name = "if_active_link", .id = ePORTMAP_IF_ACTIVE_LINK, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = DEVBRD_PORTS_MAXNUM, 
        .size = 1, .offset_sz = sizeof(b_u32), 
        .offset = DEVMAN_IF_ACTIVE_LINK
    }
};

/** 
 *  "trans" attribute read accessor
 */
static ccl_err_t _portmap_transid_read(void *p_priv, b_u32 idx, b_u8 *p_value)
{
    portmap_ctx_t  *p_portmap;

    if ( !p_priv || !p_value ) {
        PDEBUG("Error! Invalid input parameters\n");
        PORTMAP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    p_portmap = (portmap_ctx_t *)p_priv;
    if ( p_portmap->brdspec.trans_id[idx] == DEVBRD_PORT_TRANS_NONE ) {
        PDEBUG("Error! Can't identify transceiver for the port %d\n", idx);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    /* returned value */
    *(b_u32 *)p_value = p_portmap->brdspec.trans_id[idx];
    return CCL_OK;
}

/** 
 *  "led" attribute write accessor
 */
static ccl_err_t _portmap_led_write(void *p_priv, b_u32 idx, b_u8 *p_value)
{
    portmap_ctx_t  *p_portmap;
    devo_t          devo=0; 
    char            dev_name[DEVMAN_DEVNAME_LEN]; 
    char            attr_name[DEVMAN_DEVNAME_LEN];
    b_u32           attr_val, sign_mask;        
    int             is_active_hi;        

    if ( !p_priv || !p_value ) {
        PDEBUG("Error! Invalid input parameters\n");
        PORTMAP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_portmap = (portmap_ctx_t *)p_priv;

    /* figure out the appropriate to "led" device/attr */     
    _ret = devboard_portled_to_devattr(p_portmap->brdspec.port_id[idx], 
                                       dev_name, attr_name, 
                                       &sign_mask, &is_active_hi);
    if (_ret == CCL_NOT_SUPPORT) {
        PDEBUG("\"led\" is not supported for trans %d, port %d\n", 
               p_portmap->brdspec.trans_id[idx], idx);
        return CCL_OK;
    }
    _ret = _ret? _ret: devman_device_object(dev_name, &devo);
    if ( _ret || !devo ) {
        PDEBUG("Error! Can't identify device/attr for the led processing\n", dev_name);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    PDEBUG("idx=%d,dev_name=%s,attr_name=%s,sign_mask=0x%08x,value=%d\n",
           idx, dev_name, attr_name, sign_mask, *p_value);
    /* get curr leds configuration */
    _ret = devman_attr_get_word(devo, attr_name, &attr_val);
    if ( _ret ) {
        PDEBUG("Error! Can't get %s attribute\n", attr_name);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    /* prepare new leds configuration  */
    if ( *p_value )
        attr_val = is_active_hi? attr_val | sign_mask: attr_val & ~sign_mask;
    else
        attr_val = is_active_hi? attr_val & ~sign_mask: attr_val | sign_mask;
    /* set new led configuration */
    _ret = devman_attr_set_word(devo, attr_name, attr_val);
    if ( _ret ) {
        PDEBUG("Error! Can't set %s attribute\n", attr_name);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    PDEBUG("idx=%d,dev_name=%s,attr_name=%s,attr_val=0x%08x,sign_mask=0x%08x\n",
           idx, dev_name, attr_name, attr_val, sign_mask);
    return CCL_OK;
}

/** 
 *  "led" attribute read accessor
 */
static ccl_err_t _portmap_led_read(void *p_priv, b_u32 idx, b_u8 *p_value)
{
    portmap_ctx_t  *p_portmap;
    devo_t          devo=0; 
    char            dev_name[DEVMAN_DEVNAME_LEN]; 
    char            attr_name[DEVMAN_DEVNAME_LEN];
    b_u32            attr_val, sign_mask;     
    int             is_active_hi;        

    if ( !p_priv || !p_value ) {
        PDEBUG("Error! Invalid input parameters\n");
        PORTMAP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_portmap = (portmap_ctx_t *)p_priv;

    /* figure out the appropriate to "led" device/attr */     
    _ret = devboard_portled_to_devattr(p_portmap->brdspec.port_id[idx], 
                                       dev_name, attr_name, 
                                       &sign_mask, &is_active_hi);
    if (_ret == CCL_NOT_SUPPORT) {
        PDEBUG("\"led\" is not supported for trans %d, port %d\n", 
               p_portmap->brdspec.trans_id[idx], idx);
        return CCL_OK;
    }
    _ret = _ret? _ret: devman_device_object(dev_name, &devo);
    if ( _ret || !devo ) {
        PDEBUG("Error! Can't identify device/attr for the led processing\n", dev_name);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    PDEBUGG("123$=> idx=%d,dev_name=%s,attr_name=%s,sign_mask=0x%02x\n",
            idx, dev_name, attr_name, sign_mask);
    /* get curr led configuration */
    _ret = devman_attr_get_word(devo, attr_name, &attr_val);
    if ( _ret ) {
        PDEBUG("Error! Can't get %s attribute\n", attr_name);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    /* prepare returned attribute value */

    *p_value = ((is_active_hi && ( attr_val & sign_mask )) ||
                (!is_active_hi && !( attr_val & sign_mask ))) ? CCL_TRUE: CCL_FALSE;
    return CCL_OK;
}

/** 
 *  "enable" attribute write accessor
 */
static ccl_err_t _portmap_enable_write(void *p_priv, b_u32 idx, b_u8 *p_value)
{
    portmap_ctx_t  *p_portmap;
    devo_t          devo = 0; 
    char            dev_name[DEVMAN_DEVNAME_LEN]; 
    char            attr_name[DEVMAN_DEVNAME_LEN];
    b_u32           attr_val, sign_mask;
    int             is_active_hi;        

    if ( !p_priv || !p_value  ) {
        PDEBUG("Error! Invalid input parameters\n");
        PORTMAP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_portmap = (portmap_ctx_t *)p_priv;

    /* figure out the appropriate to "enable" device/attr */     
    _ret = devboard_portena_to_devattr(p_portmap->brdspec.port_id[idx], 
                                       dev_name, attr_name, &sign_mask, &is_active_hi);
    if (_ret == CCL_NOT_SUPPORT) {
        PDEBUG("\"tx enable\" is not supported for trans %d, port %d\n", 
               p_portmap->brdspec.trans_id[idx], idx);
        return CCL_OK;
    }
    _ret = _ret? _ret: devman_device_object(dev_name, &devo);
    if ( _ret || !devo ) {
        PDEBUG("Error! Can't identify %s for the portena processing\n", dev_name);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    /* get curr enabled ports mask */
    _ret = devman_attr_get_word(devo, attr_name, &attr_val);
    if ( _ret ) {
        PDEBUG("Error! Can't get %s attribute\n", attr_name);
        //123$; believe it disabled if can't detect the state.. PORTMAP_ERROR_RETURN(CCL_FAIL);
        return CCL_OK;
    }
    PDEBUG("idx=%d, enable=%d, is_active_high=%d, dev_name=%s,attr_name=%s,sign_mask=0x%x, attr_val=0x%x\n",
           idx, *p_value, is_active_hi, dev_name, attr_name, sign_mask, attr_val);
    /* prepare new ena configuration  */
    if ( *p_value )
        attr_val = is_active_hi? attr_val | sign_mask: attr_val & ~sign_mask;
    else
        attr_val = is_active_hi? attr_val & ~sign_mask: attr_val | sign_mask;

    /* set new enabled ports mask */
    _ret = devman_attr_set_word(devo, attr_name, attr_val);
    if ( _ret ) {
        PDEBUG("Error! Can't set %s attribute\n", attr_name);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }

    PDEBUG("End: idx=%d,dev_name=%s,attr_name=%s,sign_mask=0x%x, attr_val=0x%x\n",
           idx, dev_name, attr_name, sign_mask, attr_val);

    return CCL_OK;
}

/** 
 *  "enable" attribute read accessor
 */
static ccl_err_t _portmap_enable_read(void *p_priv, b_u32 idx, b_u8 *p_value)
{
    portmap_ctx_t  *p_portmap;
    devo_t          devo=0; 
    char            dev_name[DEVMAN_DEVNAME_LEN]; 
    char            attr_name[DEVMAN_DEVNAME_LEN];
    b_u32           attr_val, sign_mask;     
    int             is_active_hi;        

    if ( !p_priv || !p_value ) {
        PDEBUG("Error! Invalid input parameters\n");
        PORTMAP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_portmap = (portmap_ctx_t *)p_priv;

    /* figure out the appropriate to "enable" device/attr */     
    _ret = devboard_portena_to_devattr(p_portmap->brdspec.port_id[idx], 
                                       dev_name, attr_name, &sign_mask, &is_active_hi);
    if (_ret == CCL_NOT_SUPPORT) {
        PDEBUG("\"tx enable\" is not supported for trans %d, port %d\n", 
               p_portmap->brdspec.trans_id[idx], idx);
        return CCL_OK;
    }
    _ret = _ret? _ret: devman_device_object(dev_name, &devo);
    if ( _ret || !devo ) {
        PDEBUG("Error! Can't identify %s for the portena processing\n", dev_name);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    /* get curr ena configuration */
    _ret = devman_attr_get_word(devo, attr_name, &attr_val);
    if ( _ret ) {
        PDEBUG("Error! Can't get %s attribute\n", attr_name);
        //123$; believe it disabled if can't detect the state.. PORTMAP_ERROR_RETURN(CCL_FAIL);
        *p_value = CCL_FALSE;
        return CCL_OK;
    }
    PDEBUG("123$=> idx=%d,dev_name=%s,attr_name=%s,sign_mask=0x%02x,attr_val=0x%02x\n",idx, dev_name, attr_name, sign_mask,attr_val);
    /* prepare returned attribute value */
    *p_value = !( attr_val & sign_mask )? !is_active_hi: is_active_hi;
    return CCL_OK;
}

/** 
 *  "enable" attribute write accessor
 */
static ccl_err_t _portmap_reset_write(void *p_priv, b_u32 idx, b_u8 *p_value)
{
    portmap_ctx_t  *p_portmap;
    devo_t          devo=0; 
    char            dev_name[DEVMAN_DEVNAME_LEN]; 
    char            attr_name[DEVMAN_DEVNAME_LEN];
    b_u32           attr_val, sign_mask;
    int             is_active_hi;        

    if ( !p_priv || !p_value ) {
        PDEBUG("Error! Invalid input parameters\n");
        PORTMAP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_portmap = (portmap_ctx_t *)p_priv;

    /* figure out the appropriate to "reset" device/attr */     
    _ret = devboard_portrst_to_devattr(p_portmap->brdspec.port_id[idx], 
                                       dev_name, attr_name, &sign_mask, &is_active_hi);
    if (_ret == CCL_NOT_SUPPORT) {
        PDEBUG("Reset is not supported for trans %d, port %d\n", 
               p_portmap->brdspec.trans_id[idx], idx);
        return CCL_OK;
    }
    _ret = _ret? _ret: devman_device_object(dev_name, &devo);
    if ( _ret || !devo ) {
        PDEBUG("Error! Can't identify %s for the reset processing\n", dev_name);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    PDEBUG("123$=> idx=%d,dev_name=%s,attr_name=%s,sign_mask=0x%02x\n",idx, dev_name, attr_name, sign_mask);
    /* get curr enabled ports mask */
    _ret = devman_attr_get_word(devo, attr_name, &attr_val);
    if ( _ret ) {
        PDEBUG("Error! Can't get %s attribute\n", attr_name);
        //123$; believe it disabled if can't detect the state.. PORTMAP_ERROR_RETURN(CCL_FAIL);
        return CCL_OK;
    }
    /* prepare new ena configuration  */
    if ( *p_value )
        attr_val = is_active_hi? attr_val | sign_mask: attr_val & ~sign_mask;
    else
        attr_val = is_active_hi? attr_val & ~sign_mask: attr_val | sign_mask;

    /* set new enabled ports mask */
    _ret = devman_attr_set_word(devo, attr_name, attr_val);
    if ( _ret ) {
        PDEBUG("Error! Can't set %s attribute\n", attr_name);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    PDEBUG("123$=> END!! idx=%d,dev_name=%s,attr_name=%s,attr_val=0x%08x,sign_mask=0x%02x,*p_value=%d,is_active_hi=%d\n",
           idx, dev_name, attr_name, attr_val, sign_mask, *p_value, is_active_hi);
    return CCL_OK;
}

/** 
 *  "tx_fault" attribute read accessor
 */
static ccl_err_t _portmap_tx_fault_read(void *p_priv, b_u32 idx, b_u8 *p_value)
{
    portmap_ctx_t  *p_portmap;
    devo_t          devo=0; 
    char            dev_name[DEVMAN_DEVNAME_LEN]; 
    char            attr_name[DEVMAN_DEVNAME_LEN];
    b_u8            attr_val, sign_mask;     

    if ( !p_priv || !p_value ) {
        PDEBUG("Error! Invalid input parameters\n");
        PORTMAP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_portmap = (portmap_ctx_t *)p_priv;

    if ( p_portmap->brdspec.trans_id[idx] == DEVBRD_PORT_TRANS_NONE ) {
        PDEBUG("Error! Can't identify transceiver for the port %d\n", idx);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }

    /* figure out the appropriate to "rs" device/attr */     
    _ret = devboard_porttxf_to_devattr(p_portmap->brdspec.port_id[idx], 
                                       dev_name, attr_name, &sign_mask);
    _ret = _ret? _ret: devman_device_object(dev_name, &devo);
    if ( _ret || !devo ) {
        PDEBUG("Error! Can't identify device/attr for the portrs processing\n", dev_name);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    /* get curr rs configuration */
    _ret = devman_attr_get_byte(devo, attr_name, &attr_val);
    if ( _ret ) {
        PDEBUG("Error! Can't get %s attribute\n", attr_name);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    PDEBUGG("123$=> idx=%d,dev_name=%s,attr_name=%s,sign_mask=0x%02x,attr_val=0x%02x\n",idx, dev_name, attr_name, sign_mask,attr_val);
    /* prepare returned attribute value */
    *p_value = !( attr_val & sign_mask )? CCL_TRUE: CCL_FALSE;
    return CCL_OK;
}

/** 
 *  "rx_los" attribute read accessor
 */
static ccl_err_t _portmap_rx_los_read(void *p_priv, b_u32 idx, b_u8 *p_value)
{
    portmap_ctx_t  *p_portmap;
    devo_t          devo=0; 
    char            dev_name[DEVMAN_DEVNAME_LEN]; 
    char            attr_name[DEVMAN_DEVNAME_LEN];
    b_u8            attr_val, sign_mask;     

    if ( !p_priv || !p_value ) {
        PDEBUG("Error! Invalid input parameters\n");
        PORTMAP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_portmap = (portmap_ctx_t *)p_priv;

    if ( p_portmap->brdspec.trans_id[idx] == DEVBRD_PORT_TRANS_NONE ) {
        PDEBUG("Error! Can't identify transceiver for the port %d\n", idx);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }

    /* figure out the appropriate to "rs" device/attr */     
    _ret = devboard_portrxlos_to_devattr(p_portmap->brdspec.port_id[idx], 
                                         dev_name, attr_name, &sign_mask);
    _ret = _ret? _ret: devman_device_object(dev_name, &devo);
    if ( _ret || !devo ) {
        PDEBUG("Error! Can't identify device/attr for the portrs processing\n", dev_name);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    /* get curr rs configuration */
    _ret = devman_attr_get_byte(devo, attr_name, &attr_val);
    if ( _ret ) {
        PDEBUG("Error! Can't get %s attribute\n", attr_name);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    PDEBUGG("123$=> idx=%d,dev_name=%s,attr_name=%s,sign_mask=0x%02x,attr_val=0x%02x\n",idx, dev_name, attr_name, sign_mask,attr_val);
    /* prepare returned attribute value */
    *p_value = !( attr_val & sign_mask )? CCL_TRUE: CCL_FALSE;
    return CCL_OK;
}

/** 
 *  "rs" attribute write accessor
 */
static ccl_err_t _portmap_rs_write(void *p_priv, b_u32 idx, 
                                   b_u8 rs_id, b_u8 *p_value)
{
    portmap_ctx_t  *p_portmap;
    devo_t          devo=0; 
    char            dev_name[DEVMAN_DEVNAME_LEN]; 
    char            attr_name[DEVMAN_DEVNAME_LEN];
    b_u8            attr_val, sign_mask;        

    if ( !p_priv || !p_value ||
         rs_id >= DEVBRD_TRANS_RS_ID_MAXNUM) {
        PDEBUG("Error! Invalid input parameters\n");
        PORTMAP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_portmap = (portmap_ctx_t *)p_priv;

    if ( p_portmap->brdspec.trans_id[idx] == DEVBRD_PORT_TRANS_NONE ) {
        PDEBUG("Error! Can't identify transceiver for the port %d\n", idx);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }

    /* figure out the appropriate to "rs" device/attr */     
    _ret = devboard_portrs_to_devattr(p_portmap->brdspec.port_id[idx], 
                                      rs_id, dev_name, attr_name, &sign_mask);
    _ret = _ret? _ret: devman_device_object(dev_name, &devo);
    if ( _ret || !devo ) {
        PDEBUG("Error! Can't identify device/attr for the portrs processing\n", dev_name);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    PDEBUGG("123$=> idx=%d,dev_name=%s,attr_name=%s,sign_mask=0x%02x\n",idx, dev_name, attr_name, sign_mask);
    /* get curr rs'ed ports mask */
    _ret = devman_attr_get_byte(devo, attr_name, &attr_val);
    if ( _ret ) {
        PDEBUG("Error! Can't get %s attribute\n", attr_name);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    /* prepare new rs configuration  */
    attr_val = (*p_value)? attr_val & ~sign_mask: attr_val | sign_mask;
    /* set new rs ports mask */
    _ret = devman_attr_set_byte(devo, attr_name, attr_val);
    if ( _ret ) {
        PDEBUG("Error! Can't set %s attribute\n", attr_name);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}

/** 
 *  "rs" attribute read accessor
 */
static ccl_err_t _portmap_rs_read(void *p_priv, b_u32 idx, 
                                  b_u8 rs_id, b_u8 *p_value)
{
    portmap_ctx_t  *p_portmap;
    devo_t          devo=0; 
    char            dev_name[DEVMAN_DEVNAME_LEN]; 
    char            attr_name[DEVMAN_DEVNAME_LEN];
    b_u8            attr_val, sign_mask;     

    if ( !p_priv || !p_value ||
         rs_id >= DEVBRD_TRANS_RS_ID_MAXNUM) {
        PDEBUG("Error! Invalid input parameters\n");
        PORTMAP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_portmap = (portmap_ctx_t *)p_priv;

    if ( p_portmap->brdspec.trans_id[idx] == DEVBRD_PORT_TRANS_NONE ) {
        PDEBUG("Error! Can't identify transceiver for the port %d\n", idx);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }

    /* figure out the appropriate to "rs" device/attr */     
    _ret = devboard_portrs_to_devattr(p_portmap->brdspec.port_id[idx], 
                                      rs_id, dev_name, attr_name, &sign_mask);
    _ret = _ret? _ret: devman_device_object(dev_name, &devo);
    if ( _ret || !devo ) {
        PDEBUG("Error! Can't identify device/attr for the portrs processing\n", dev_name);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    /* get curr rs configuration */
    _ret = devman_attr_get_byte(devo, attr_name, &attr_val);
    if ( _ret ) {
        PDEBUG("Error! Can't get %s attribute\n", attr_name);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    PDEBUGG("123$=> idx=%d,dev_name=%s,attr_name=%s,sign_mask=0x%02x,attr_val=0x%02x\n",idx, dev_name, attr_name, sign_mask,attr_val);
    /* prepare returned attribute value */
    *p_value = !( attr_val & sign_mask )? CCL_TRUE: CCL_FALSE;
    return CCL_OK;
}

/** 
 *  "modsell" attribute write accessor
 */
static ccl_err_t _portmap_modsel_write(void *p_priv, b_u32 idx, b_u8 *p_value)
{
    portmap_ctx_t  *p_portmap;
    devo_t          devo=0; 
    char            dev_name[DEVMAN_DEVNAME_LEN]; 
    char            attr_name[DEVMAN_DEVNAME_LEN];
    b_u8            attr_val, sign_mask;        

    if ( !p_priv || !p_value ) {
        PDEBUG("Error! Invalid input parameters\n");
        PORTMAP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_portmap = (portmap_ctx_t *)p_priv;

    if ( p_portmap->brdspec.trans_id[idx] == DEVBRD_PORT_TRANS_NONE ) {
        PDEBUG("Error! Can't identify transceiver for the port %d\n", idx);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }

    /* figure out the appropriate to "rs" device/attr */     
    _ret = devboard_portmodsell_to_devattr(p_portmap->brdspec.port_id[idx], 
                                           dev_name, attr_name, &sign_mask);
    _ret = _ret? _ret: devman_device_object(dev_name, &devo);
    if ( _ret || !devo ) {
        PDEBUG("Error! Can't identify device/attr for the portrs processing\n", dev_name);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    PDEBUGG("123$=> idx=%d,dev_name=%s,attr_name=%s,sign_mask=0x%02x\n",idx, dev_name, attr_name, sign_mask);
    /* get curr rs'ed ports mask */
    _ret = devman_attr_get_byte(devo, attr_name, &attr_val);
    if ( _ret ) {
        PDEBUG("Error! Can't get %s attribute\n", attr_name);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    /* prepare new rs configuration  */
    attr_val = (*p_value)? attr_val & ~sign_mask: attr_val | sign_mask;
    /* set new rs ports mask */
    _ret = devman_attr_set_byte(devo, attr_name, attr_val);
    if ( _ret ) {
        PDEBUG("Error! Can't set %s attribute\n", attr_name);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}

/** 
 *  "lpmode" attribute write accessor
 */
static ccl_err_t _portmap_lpmode_write(void *p_priv, b_u32 idx, b_u8 *p_value)
{
    portmap_ctx_t  *p_portmap;
    devo_t          devo=0; 
    char            dev_name[DEVMAN_DEVNAME_LEN]; 
    char            attr_name[DEVMAN_DEVNAME_LEN];
    b_u8            attr_val, sign_mask;        

    if ( !p_priv || !p_value ) {
        PDEBUG("Error! Invalid input parameters\n");
        PORTMAP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_portmap = (portmap_ctx_t *)p_priv;

    if ( p_portmap->brdspec.trans_id[idx] == DEVBRD_PORT_TRANS_NONE ) {
        PDEBUG("Error! Can't identify transceiver for the port %d\n", idx);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }

    /* figure out the appropriate to "rs" device/attr */     
    _ret = devboard_portlpmode_to_devattr(p_portmap->brdspec.port_id[idx], 
                                          dev_name, attr_name, &sign_mask);
    _ret = _ret? _ret: devman_device_object(dev_name, &devo);
    if ( _ret || !devo ) {
        PDEBUG("Error! Can't identify device/attr for the portrs processing\n", dev_name);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    PDEBUGG("123$=> idx=%d,dev_name=%s,attr_name=%s,sign_mask=0x%02x\n",idx, dev_name, attr_name, sign_mask);
    /* get curr rs'ed ports mask */
    _ret = devman_attr_get_byte(devo, attr_name, &attr_val);
    if ( _ret ) {
        PDEBUG("Error! Can't get %s attribute\n", attr_name);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    /* prepare new rs configuration  */
    attr_val = (*p_value)? attr_val & ~sign_mask: attr_val | sign_mask;
    /* set new rs ports mask */
    _ret = devman_attr_set_byte(devo, attr_name, attr_val);
    if ( _ret ) {
        PDEBUG("Error! Can't set %s attribute\n", attr_name);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}

/** 
 *  "media_pres" attribute read accessor
 */
static ccl_err_t _portmap_media_pres_read(void *p_priv, b_u32 idx, b_u8 *p_value)
{
    portmap_ctx_t  *p_portmap;
    devo_t          devo=0; 
    char            dev_name[DEVMAN_DEVNAME_LEN]; 
    char            attr_name[DEVMAN_DEVNAME_LEN];
    b_u32           attr_val, sign_mask;     
    int             is_active_hi;        
    if ( !p_priv || !p_value ) {
        PDEBUG("Error! Invalid input parameters\n");
        PORTMAP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_portmap = (portmap_ctx_t *)p_priv;

    if ( p_portmap->brdspec.trans_id[idx] == DEVBRD_PORT_TRANS_NONE ) {
        PDEBUG("Error! Can't identify transceiver for the port %d\n", idx);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }

    /* figure out the appropriate to "present" device/attr */     
    _ret = devboard_portprs_to_devattr(p_portmap->brdspec.port_id[idx], 
                                       dev_name, attr_name, &sign_mask, &is_active_hi);
    PDEBUG("trans=%d, dev_name=%s, attr_name=%s, sign_mask=0x%x, is_active_hi=%d\n",
           p_portmap->brdspec.trans_id[idx], dev_name, attr_name, sign_mask, is_active_hi);
    _ret = _ret? _ret: devman_device_object(dev_name, &devo);
    if ( _ret || !devo ) {
        PDEBUG("Error! Can't identify device/attr for the media_pres processing\n", dev_name);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    /* get curr media presense state */
    _ret = devman_attr_get_word(devo, attr_name, &attr_val);
    if ( _ret ) {
        PDEBUG("Error! Can't get %s attribute\n", attr_name);
        *p_value = CCL_FALSE;
        return CCL_OK;
    }
    /* prepare returned attribute value */
    *p_value = ( attr_val & sign_mask )? is_active_hi: !is_active_hi;
    PDEBUG("idx=%d,dev_name=%s,attr_name=%s,sign_mask=0x%02x,attr_val=0x%02x, *p_value=%d\n",
           idx, dev_name, attr_name, sign_mask,attr_val, *p_value);
    return CCL_OK;
}

/** 
 *  "media_id" attribute read accessor
 */
static ccl_err_t _portmap_media_id_read(void *p_priv, b_u32 idx, b_u8 *p_value)
{
    portmap_ctx_t  *p_portmap;
    devo_t          devo=0;
    char            dev_name[DEVMAN_DEVNAME_LEN]; 
    b_bool          is_present;

    if ( !p_priv || !p_value ) {
        PDEBUG("Error! Invalid input parameters\n");
        PORTMAP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    *p_value = 0;   
    p_portmap = (portmap_ctx_t *)p_priv;
    if ( p_portmap->brdspec.trans_id[idx] == DEVBRD_PORT_TRANS_NONE ) {
        PDEBUG("Error! Can't identify transceiver for the port %d\n", idx);
        PORTMAP_ERROR_RETURN(CCL_NOT_SUPPORT);
    }

    /* get presence field */
    _ret = _portmap_media_pres_read(p_priv, p_portmap->brdspec.trans_id[idx],                               
                                    &is_present);
    if ( _ret ) {
        PDEBUG("Error! _portmap_media_pres_read failed on port %d\n", idx);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    if ( !is_present ) {
        PDEBUG("Error! The transceiver %d port %d slot is empty\n", 
               p_portmap->brdspec.trans_id[idx], idx);
        PORTMAP_ERROR_RETURN(CCL_NOT_FOUND);
    }
    /* get transciever object handle */
    snprintf(dev_name, sizeof(dev_name), "trans/%d", p_portmap->brdspec.trans_id[idx]);    
    _ret = devman_device_object(dev_name, &devo);    
    if ( _ret || !devo ) {
        PDEBUG("Error! Can't get device %s object handle\n", dev_name);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    /* get media info identifier field */
    _ret = devman_attr_get_byte(devo, "msa_identifier", p_value);
    if ( _ret ) {
        PDEBUG("Error! Can't get %s attribute\n", "msa_identifier");
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

/** 
 *  "media_msa" attribute read accessor
 */
static ccl_err_t _portmap_media_msa_read(void *p_priv, b_u32 idx, 
                                         b_u8 *p_value, b_u32 size)
{
    portmap_ctx_t  *p_portmap;
    devo_t          devo = 0;
    char            dev_name[DEVMAN_DEVNAME_LEN]; 
    b_bool          is_present;

    if ( !p_priv || !p_value || !size ) {
        PDEBUG("Error! Invalid input parameters\n");
        PORTMAP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    memset(p_value, 0, size);
    p_portmap = (portmap_ctx_t *)p_priv;

    if ( p_portmap->brdspec.trans_id[idx] == DEVBRD_PORT_TRANS_NONE ) {
        PDEBUG("Error! Can't identify transceiver for the port %d\n", idx);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }

    PDEBUG("port: %d, trans: %d\n",
           idx, p_portmap->brdspec.trans_id[idx]);

    /* get presence field */
    _ret = _portmap_media_pres_read(p_priv, p_portmap->brdspec.trans_id[idx],                               
                                    &is_present);
    if ( _ret ) {
        PDEBUG("Error! _portmap_media_pres_read failed on port %d\n", idx);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    if ( !is_present ) {
        PDEBUG("Error! The transceiver %d port %d slot is empty\n", 
               p_portmap->brdspec.trans_id[idx], idx);
        PORTMAP_ERROR_RETURN(CCL_NOT_FOUND);
    }

    /* get transceiver object handle */
    _ret = devman_device_object_identify("trans", 
                                         p_portmap->brdspec.trans_id[idx], 
                                         &devo);
    if ( _ret || !devo) {
        PDEBUG("Error! Can't identify \"trans\", %d discr object\n", 
               p_portmap->brdspec.trans_id[idx]);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    /* get msa raw buffer */
    _ret = devman_attr_get_buff(devo, "msa_raw", p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Can't get msa_raw attribute\n");
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}

/** 
 *  "media_ddtype" attribute read accessor
 */
static ccl_err_t _portmap_media_ddtype_read(void *p_priv, b_u32 idx, b_u8 *p_value)
{
    portmap_ctx_t  *p_portmap;
    devo_t          devo=0;
    char            dev_name[DEVMAN_DEVNAME_LEN]; 
    b_bool          is_present;

    if ( !p_priv || !p_value ) {
        PDEBUG("Error! Invalid input parameters\n");
        PORTMAP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    *p_value = 0;   
    p_portmap = (portmap_ctx_t *)p_priv;
    if ( p_portmap->brdspec.trans_id[idx] == DEVBRD_PORT_TRANS_NONE ) {
        PDEBUG("Error! Can't identify transceiver for the port %d\n", idx);
        PORTMAP_ERROR_RETURN(CCL_NOT_SUPPORT);
    }
    /* get presence field */
    _ret = _portmap_media_pres_read(p_priv, p_portmap->brdspec.trans_id[idx],                               
                                    &is_present);
    if ( _ret ) {
        PDEBUG("Error! _portmap_media_pres_read failed on port %d\n", idx);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    if ( !is_present ) {
        PDEBUG("Error! The transceiver %d port %d slot is empty\n", 
               p_portmap->brdspec.trans_id[idx], idx);
        PORTMAP_ERROR_RETURN(CCL_NOT_FOUND);
    }
    /* get transciever object handle */
    snprintf(dev_name, sizeof(dev_name), "trans/%d", p_portmap->brdspec.trans_id[idx]);    
    _ret = devman_device_object(dev_name, &devo);    
    if ( _ret || !devo ) {
        PDEBUG("Error! Can't get device %s object handle\n", dev_name);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    /* get media info identifier field */
    _ret = devman_attr_get_byte(devo, "msa_ddm_type", p_value);
    if ( _ret ) {
        PDEBUG("Error! Can't get %s attribute\n", "msa_ddm_type");
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}

/** 
 *  "media_dd" attribute read accessor
 */
static ccl_err_t _portmap_media_dd_read(void *p_priv, b_u32 idx, 
                                        b_u8 *p_value, b_u32 size)
{
    portmap_ctx_t   *p_portmap;
    devo_t           devo=0;
    char             dev_name[DEVMAN_DEVNAME_LEN]; 
    b_bool           is_present;

    if ( !p_priv || !p_value || !size ) {
        PDEBUG("Error! Invalid input parameters\n");
        PORTMAP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    memset(p_value, 0, size);
    p_portmap = (portmap_ctx_t *)p_priv;
    if ( p_portmap->brdspec.trans_id[idx] == DEVBRD_PORT_TRANS_NONE ) {
        PDEBUG("Error! Can't identify transceiver for the port %d\n", idx);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }

    _ret = _portmap_media_pres_read(p_priv, p_portmap->brdspec.trans_id[idx],                               
                                    &is_present);
    if ( _ret ) {
        PDEBUG("Error! _portmap_media_pres_read failed on port %d\n", idx);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    if ( !is_present ) {
        PDEBUG("Error! The transceiver %d port %d slot is empty\n", 
               p_portmap->brdspec.trans_id[idx], idx);
        PORTMAP_ERROR_RETURN(CCL_NOT_FOUND);
    }

    /* get transciever object handle */
    snprintf(dev_name, sizeof(dev_name), "trans/%d", p_portmap->brdspec.trans_id[idx]);    
    _ret = devman_device_object(dev_name, &devo);    
    if ( _ret || !devo ) {
        PDEBUG("Error! Can't get device %s object handle\n", dev_name);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    /* get dd raw buffer */
    _ret = devman_attr_get_buff(devo, "dd_raw", p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Can't get dd_raw attribute\n");
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}

/** 
 *  "media_phy" attribute read accessor
 */
static ccl_err_t _portmap_media_phy_read(void *p_priv, b_u32 idx, 
                                         b_u8 *p_value, b_u32 size)
{
    portmap_ctx_t  *p_portmap;
    devo_t          devo=0;
    char            dev_name[DEVMAN_DEVNAME_LEN]; 
    b_bool          is_present;

    if ( !p_priv || !p_value || !size ) {
        PDEBUG("Error! Invalid input parameters\n");
        PORTMAP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    memset(p_value, 0, size);
    p_portmap = (portmap_ctx_t *)p_priv;
    if ( p_portmap->brdspec.trans_id[idx] == DEVBRD_PORT_TRANS_NONE ) {
        PDEBUG("Error! Can't identify transceiver for the port %d\n", idx);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    /* get presence field */
    _ret = _portmap_media_pres_read(p_priv, p_portmap->brdspec.trans_id[idx],                               
                                    &is_present);
    if ( _ret ) {
        PDEBUG("Error! _portmap_media_pres_read failed on port %d\n", idx);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    if ( !is_present ) {
        PDEBUG("Error! The transceiver %d port %d slot is empty\n", 
               p_portmap->brdspec.trans_id[idx], idx);
        PORTMAP_ERROR_RETURN(CCL_NOT_FOUND);
    }
    /* get transciever object handle */
    snprintf(dev_name, sizeof(dev_name), "trans/%d", p_portmap->brdspec.trans_id[idx]);    
    _ret = devman_device_object(dev_name, &devo);    
    if ( _ret || !devo ) {
        PDEBUG("Error! Can't get device %s object handle\n", dev_name);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    /* get phy raw buffer */
    _ret = devman_attr_get_buff(devo, "phy_raw", p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Can't get phy_raw attribute\n");
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}

/** 
 *  "media_msa" attribute read accessor
 */
static ccl_err_t _portmap_media_decode(void *p_priv, 
                                       b_u32 idx, 
                                       media_decoder_t *media_info)
{
    portmap_ctx_t  *p_portmap;
    b_bool          is_present;
    devo_t          devo=0;

    if ( !p_priv || !media_info) {
        PDEBUG("Error! Invalid input parameters\n");
        PORTMAP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_portmap = (portmap_ctx_t *)p_priv;

    if ( p_portmap->brdspec.trans_id[idx] == DEVBRD_PORT_TRANS_NONE ) {
        PDEBUG("Error! Can't identify transceiver for the port %d\n", idx);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }

    PDEBUG("port: %d, trans: %d\n",
           idx, p_portmap->brdspec.trans_id[idx]);

    /* get presence field */
    _ret = _portmap_media_pres_read(p_priv, p_portmap->brdspec.trans_id[idx],                               
                                    &is_present);
    if ( _ret ) {
        PDEBUG("Error! _portmap_media_pres_read failed on port %d\n", idx);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    if ( !is_present ) {
        PDEBUG("Error! The transceiver %d port %d slot is empty\n", 
               p_portmap->brdspec.trans_id[idx], idx);
        PORTMAP_ERROR_RETURN(CCL_NOT_FOUND);
    }

    /* get transceiver object handle */
    _ret = devman_device_object_identify("trans", 
                                         p_portmap->brdspec.trans_id[idx], 
                                         &devo);
    if ( _ret || !devo) {
        PDEBUG("Error! Can't identify \"trans\", %d discr object\n", 
               p_portmap->brdspec.trans_id[idx]);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    /* get msa raw buffer */
    _ret = devman_attr_get_buff(devo, "media_decode", 
                                (b_u8 *)media_info, 
                                sizeof(media_decoder_t));
    if ( _ret ) {
        PDEBUG("Error! Can't get msa_raw attribute\n");
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    PDEBUG("port: %d, trans: %d, temperature=%lf\n",
           idx, p_portmap->brdspec.trans_id[idx], ((media_decoder_t *)media_info)->temperature);

    return CCL_OK;
}

static ccl_err_t _portmap_media_params_raw_read(void *p_priv, b_u32 idx, 
                                                devman_media_data_type_t media_data_type,
                                                b_u32 start_offset, b_u32 block_size, 
                                                b_u8 *media_id, b_u8 *media_data)
{
    portmap_ctx_t              *p_portmap;  
    b_bool                     is_present; 
    devo_t                     devo=0;     
    char                       attr_name[DEVMAN_DEVNAME_LEN];
    b_u8                       raw_buffer[256];
    devman_media_data_block_t  *pmedia_data_layout;
    devman_media_param_t       *media_param;

    memset(raw_buffer, 0, sizeof(raw_buffer));

    if ( !p_priv || !media_id || !media_data ) {
        PDEBUG("Error! Invalid input parameters\n");
        PORTMAP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_portmap = (portmap_ctx_t *)p_priv;

    if ( p_portmap->brdspec.trans_id[idx] == DEVBRD_PORT_TRANS_NONE ) {
        PDEBUG("Error! Can't identify transceiver for the port %d\n", idx);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }

    PDEBUG("port: %d, trans: %d\n",
           idx, p_portmap->brdspec.trans_id[idx]);

    /* get presence field */
    _ret = _portmap_media_pres_read(p_priv, p_portmap->brdspec.trans_id[idx],                               
                                    &is_present);
    if ( _ret ) {
        PDEBUG("Error! _portmap_media_pres_read failed on port %d\n", idx);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    if ( !is_present ) {
        PDEBUG("Error! The transceiver %d port %d slot is empty\n", 
               p_portmap->brdspec.trans_id[idx], idx);
        PORTMAP_ERROR_RETURN(CCL_NOT_FOUND);
    }

    /* get media id */
    _ret = _portmap_media_id_read(p_priv, p_portmap->brdspec.trans_id[idx],                               
                                  &media_id);
    if ( _ret ) {
        PDEBUG("Error! _portmap_media_id_read failed on port %d\n", idx);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    _ret = media_data_layout_get(media_id, &pmedia_data_layout);
    if (_ret) {
        PDEBUG("Error! media_data_layout_get failed on port %d\n", idx);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }

    /* get attribute name */
    if ( (pmedia_data_layout->page_index == 0x50) || 
         (pmedia_data_layout->page_index == 0x58) )
        strncpy(attr_name, "media_msa", DEVMAN_DEVNAME_LEN);    
    else if ( (pmedia_data_layout->page_index == 0x51) || 
              (pmedia_data_layout->page_index == 0x59) )
        strncpy(attr_name, "media_dd", DEVMAN_DEVNAME_LEN);    
    else if ( pmedia_data_layout->page_index == 0x56 ) {
        /* check if there is the trivial get phy single register; 123$ */
        if ( start_offset >= 0 && start_offset < 32 && 
             block_size == sizeof(b_u16) ) {
            devo_t  trans_devo;
            b_u16   val;
            /* get device handle */
            _ret = devman_device_object_identify("trans", 
                                                p_portmap->brdspec.trans_id[idx], 
                                                &trans_devo);
            if (_ret) {
                PDEBUG("Error! Can't identify DEVMAN trans/%d device\n", 
                       p_portmap->brdspec.trans_id[idx]);
                PORTMAP_ERROR_RETURN(CCL_FAIL);
            }
            /* set 'trans/n' device 'phy_reg' attribute */
            _ret = devman_attr_array_get_half(trans_devo, "phy_reg", start_offset, &val);
            if (_ret) {
                PDEBUG("Error! Can't get trans/%d phy_reg[%d] attr\n", 
                       p_portmap->brdspec.trans_id[idx], 
                       start_offset);
                PORTMAP_ERROR_RETURN(CCL_FAIL);
            }
            /* copy the value */
            memcpy(media_data, (b_u8 *)&val, sizeof(b_u16));

            return CCL_OK;
        } else {
            /* user wants phy regs area as 32*sizeof(b_u16) buffer */
            strncpy(attr_name, "media_phy", DEVMAN_DEVNAME_LEN);    
            start_offset *= sizeof(b_u16);
        }
    }        
    /* get PORTMAP's "media_msa", "media_dd" or "media_phy" attribute */
    if ( !strcmp(attr_name, "media_msa") ) 
        _ret = _portmap_media_msa_read(p_priv, idx, raw_buffer, 
                                       sizeof(raw_buffer));
    else if ( !strcmp(attr_name, "media_dd") ) 
        _ret = _portmap_media_dd_read(p_priv, idx, raw_buffer, 
                                      sizeof(raw_buffer));
    else
        _ret = _portmap_media_phy_read(p_priv, idx, raw_buffer, 
                                       sizeof(raw_buffer));
    if (_ret) {
        PDEBUG("Error! Can't get portmap's %s attr\n", attr_name);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    /* copy a part of data from the read offset */
    memcpy(media_data, raw_buffer + 
           pmedia_data_layout[media_data_type].page_offset + 
           start_offset, block_size);

    return CCL_OK;
}

static ccl_err_t _portmap_media_params_read(void *p_priv, b_u32 idx, 
                                            devman_media_data_type_t media_data_type,
                                            b_u32 start_offset, b_u32 block_size, 
                                            b_u8 *p_value, b_u32 size)
{
    devman_media_param_t  *media_param;
    b_u8                  media_id;
    b_u8                  *media_data;

    if ( !p_priv || !p_value || 
         size != sizeof(devman_media_param_t) ) {
        PDEBUG("Error! Invalid input parameters\n");
        PORTMAP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    media_param = (devman_media_param_t *)p_value;

    media_data = calloc(1, block_size);
    if (!media_data) {
        PDEBUG("Can't allocate memory\n");
        PORTMAP_ERROR_RETURN(CCL_NO_MEM_ERR);;
    }

    _ret = _portmap_media_params_raw_read(p_priv, idx, media_data_type, 
                                          start_offset, block_size, 
                                          &media_id, media_data);
    if (_ret) {
        free(media_data);
        PDEBUG("Error! _portmap_media_params_raw_read\n");
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }

    _ret = media_parameters_parse(media_id, media_data, start_offset, 
                                  block_size, &media_param->description);
    if (_ret) {
        free(media_data);
        PDEBUG("Error! media_parameters_parse\n");
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }

    free(media_data);

    return CCL_OK;
}

static ccl_err_t _portmap_media_params_details_get(void *p_priv, b_u32 idx, 
                                                   b_u8 *p_value, b_u32 size)
{
    devman_media_param_t    media_param;

    if ( !p_priv || !p_value ) {
        PDEBUG("Error! Invalid input parameters\n");
        PORTMAP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    _ret = _portmap_media_params_read(p_priv, idx, 
                                      MEDIA_DATA_TYPE_MEDIA_DESCRIPTION,
                                      0, 96, 
                                      (b_u8 *)&media_param, 
                                      sizeof(media_param));
    if (_ret) {
        PDEBUG("Error! _portmap_media_params_read\n");
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    _ret = media_params_description_build(&media_param, p_value, size);
    if (_ret) {
        PDEBUG("Error! media_params_description_build\n");
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

static ccl_err_t _portmap_media_is_ddm_supported(void *p_priv, b_u32 idx, 
                                                 b_u8 *p_value, b_u32 size)
{
    b_u8                           media_id;
    devman_media_diag_type_t       ddm;
    b_u8                           cdiag;
    b_bool                         *b_ddm;

    if ( !p_priv || !p_value ) {
        PDEBUG("Error! Invalid input parameters\n");
        PORTMAP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    b_ddm = (b_bool *)p_value;

    _ret = _portmap_media_params_raw_read(p_priv, idx, 
                                      MEDIA_DATA_TYPE_MEDIA_DESCRIPTION, 
                                      92, 1, &media_id, &cdiag);
    if (_ret) {
        PDEBUG("Error! _portmap_media_params_raw_read\n");
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }

    _ret = media_ddm_parameters_parse(media_id, cdiag, &ddm);
    if (_ret) {
        PDEBUG("Error! media_ddm_parameters_parse\n");
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }

    *b_ddm = ddm.ddm_supported.status;

    return CCL_OK;
}

static ccl_err_t _portmap_media_ddm_params_get(void *p_priv, b_u32 idx, 
                                               b_u8 *p_value, b_u32 size)
{
    devman_media_param_t           media_param;
    devman_media_diag_param_t      *pddm_param;
    b_u8                           media_id;
    b_bool                         *b_ddm;
    b_u8                           raw_buffer[96];

    if ( !p_priv || !p_value ) {
        PDEBUG("Error! Invalid input parameters\n");
        PORTMAP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    pddm_param = (devman_media_diag_param_t *)p_value;

    memset(raw_buffer, 0, sizeof(raw_buffer));

    _ret = _portmap_media_is_ddm_supported(p_priv, idx, (b_u8 *)&b_ddm, 1);
    if (_ret) {
        PDEBUG("Error! _portmap_media_is_ddm_supported\n");
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }

    if ( !b_ddm ) {
        PDEBUG("Error! DDM is not supported for port %d\n", idx);
        PORTMAP_ERROR_RETURN(CCL_NOT_SUPPORT);
    }

    _ret = _portmap_media_params_raw_read(p_priv, idx, 
                                          MEDIA_DATA_TYPE_DDM_PARAMETERS, 
                                          0, sizeof(raw_buffer), 
                                          &media_id, raw_buffer);
    if (_ret) {
        PDEBUG("Error! _portmap_media_params_raw_read\n");
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }

    _ret = media_digital_diagnostic_params_parse(media_id, raw_buffer, pddm_param);
    if (_ret) {
        PDEBUG("Error! media_digital_diagnosic_params_parse\n");
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

static ccl_err_t _portmap_media_ddm_status(void *p_priv, b_u32 idx, 
                                           b_u8 *p_value, b_u32 size)
{
    devman_media_param_t           media_param;
    devman_media_diag_status_t     *pddm_status;
    b_u8                           media_id;
    b_bool                         cdiag;
    b_u8                           raw_buffer[16];

    if ( !p_priv || !p_value ) {
        PDEBUG("Error! Invalid input parameters\n");
        PORTMAP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    pddm_status = (devman_media_diag_status_t *)p_value;

    memset(raw_buffer, 0, sizeof(raw_buffer));

    _ret = _portmap_media_is_ddm_supported(p_priv, idx, (b_u8 *)&cdiag, 1);
    if (_ret) {
        PDEBUG("Error! _portmap_media_is_ddm_supported\n");
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }

    if ( !cdiag ) {
        PDEBUG("Error! DDM is not supported for port %d\n", idx);
        PORTMAP_ERROR_RETURN(CCL_NOT_SUPPORT);
    }

    _ret = _portmap_media_params_raw_read(p_priv, idx, 
                                          MEDIA_DATA_TYPE_DDM_VALUES, 
                                          0, sizeof(raw_buffer), 
                                          &media_id, raw_buffer);
    if (_ret) {
        PDEBUG("Error! _portmap_media_params_raw_read\n");
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }

    _ret = media_digital_diagnostic_status(media_id, 0, raw_buffer, pddm_status);
    if (_ret) {
        PDEBUG("Error! media_digital_diagnosic_params_parse\n");
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

static ccl_err_t _portmap_media_link_type(void *p_priv, b_u32 idx, 
                                          b_u8 *p_value, b_u32 size)
{
    b_u8                           media_id;
    devman_media_link_type_t       *link_type;
    devman_media_param_t           media_param;
    b_u8                           raw_buffer[20];

    if ( !p_priv || !p_value ) {
        PDEBUG("Error! Invalid input parameters\n");
        PORTMAP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    memset(&media_param, 0, sizeof(media_param));

    link_type = (devman_media_link_type_t *)p_value;

    _ret = _portmap_media_params_raw_read(p_priv, idx, 
                                          MEDIA_DATA_TYPE_MEDIA_DESCRIPTION, 
                                          0, 20, &media_id, raw_buffer);
    if (_ret) {
        PDEBUG("Error! _portmap_media_params_raw_read\n");
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }

    _ret = media_parameters_parse(media_id, raw_buffer, 0, 20, 
                                  &media_param.description);
    if (_ret) {
        PDEBUG("Error! media_parameters_parse\n");
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }

    _ret = media_link_type_get(&media_param.description, &media_param.link_type);
    if (_ret) {
        PDEBUG("Error! media_link_type_get\n");
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }

    *link_type = media_param.link_type;

    return CCL_OK;
}

static ccl_err_t _portmap_if_type_get(void *p_priv, b_u32 idx, b_u8 *p_value)
{

    return CCL_OK;
}

static ccl_err_t _portmap_init(void *p_priv, b_u32 idx)
{
    portmap_ctx_t  *p_portmap;
    devo_t         devo = 0;                      
    char           dev_name[DEVMAN_DEVNAME_LEN];  
    char           attr_name[DEVMAN_DEVNAME_LEN]; 
    b_u8           enable = 1;                    
    b_u8           reset = 1;                     
    b_i32          discr;                         

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        PORTMAP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_portmap = (portmap_ctx_t *)p_priv;

    _ret = _portmap_reset_write(p_priv, p_portmap->brdspec.port_id[idx], 
                                &reset);
    if ( _ret ) {
        PDEBUG("Error! _portmap_reset_write failed\n");
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }

    _ret = _portmap_enable_write(p_priv, p_portmap->brdspec.port_id[idx], 
                                 &enable);
    if ( _ret ) {
        PDEBUG("Error! _portmap_enable_write failed\n");
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }

    /* figure out the appropriate to "init" device/attr */     
    _ret = devboard_portinit_to_devattr(p_portmap->brdspec.port_id[idx], 
                                        dev_name, attr_name, &discr);
    if ( _ret ) {
        PDEBUG("Error! devboard_portinit_to_devattr failed for %d\n",
               idx);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    _ret = devman_device_object_identify(dev_name, discr, &devo);
    if ( _ret || !devo) {
        ccl_syslog_err("Error! Can't identify %s/%d object\n", 
               dev_name, idx);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    _ret = devman_attr_set_word(devo, attr_name, 1);
    if (_ret) {
        ccl_syslog_err("Error! Can't set %s attribute for %s/%d object\n", 
               attr_name, dev_name, idx);
        PORTMAP_ERROR_RETURN(_ret);
    }
    return CCL_OK;
}

static ccl_err_t _portmap_configure_test(void *p_priv, b_u32 idx)
{
    portmap_ctx_t  *p_portmap;
    devo_t          devo=0;
    char            dev_name[DEVMAN_DEVNAME_LEN]; 
    char            attr_name[DEVMAN_DEVNAME_LEN];
    b_u32           attr_val, sign_mask;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        PORTMAP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_portmap = (portmap_ctx_t *)p_priv;

    /* figure out the appropriate to "traffic gen" device/attr */     
    _ret = devboard_porttrgen_to_devattr(p_portmap->brdspec.port_id[idx], 
                                         dev_name, attr_name);
    if (_ret == CCL_NOT_SUPPORT) {
        PDEBUG("\"tx enable\" is not supported for trans %d, port %d\n", 
               p_portmap->brdspec.trans_id[idx], idx);
        return CCL_OK;
    }
    _ret = _ret? _ret: devman_device_object(dev_name, &devo);
    if ( _ret || !devo ) {
        PDEBUG("Error! Can't identify %s for the portena processing\n", dev_name);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    PDEBUG("123$=> idx=%d,dev_name=%s,attr_name=%s,sign_mask=0x%02x\n",idx, dev_name, attr_name, sign_mask);
    /* get curr enabled ports mask */
    _ret = devman_attr_get_word(devo, attr_name, &attr_val);
    if ( _ret ) {
        PDEBUG("Error! Can't get %s attribute\n", attr_name);
        //123$; believe it disabled if can't detect the state.. PORTMAP_ERROR_RETURN(CCL_FAIL);
        return CCL_OK;
    }
    /* prepare new ena configuration  */
    attr_val = attr_val | sign_mask;

    /* set new enabled ports mask */
    _ret = devman_attr_set_word(devo, attr_name, attr_val);
    if ( _ret ) {
        PDEBUG("Error! Can't set %s attribute\n", attr_name);
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }
    PDEBUG("123$=> END!! idx=%d,dev_name=%s,attr_name=%s,sign_mask=0x%02x\n",idx, dev_name, attr_name, sign_mask);
    return CCL_OK;
}

static ccl_err_t _portmap_test(void *p_priv, b_u32 idx, b_u8 *buf, b_u32 size)
{
    portmap_ctx_t  *p_portmap;
    devo_t         devo = 0; 
    char           dev_name[DEVMAN_DEVNAME_LEN]; 
    char           attr_name[DEVMAN_DEVNAME_LEN];
    b_i32          discr;
    b_u8           led;
    port_stats_t   *port_stats;

    if ( !buf || !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        PORTMAP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_portmap = (portmap_ctx_t *)p_priv;

    port_stats = (port_stats_t *)buf;
    memset(&port_stats->trans_info.present, 0, sizeof(port_stats->trans_info));

    /* turn on led */
    led = 1;
    _portmap_led_write(p_priv, idx, &led);
    /* check if transceiver is present */
    _ret = _portmap_media_decode(p_priv, idx, 
                                 &port_stats->trans_info.media_decoder);
    if ( _ret == CCL_OK) 
        port_stats->trans_info.present = CCL_TRUE;

    /* figure out the appropriate to "read_counters" device/attr */     
    _ret = devboard_portcntrs_to_devattr(p_portmap->brdspec.port_id[idx], 
                                         dev_name, attr_name, &discr);
    if ( _ret ) 
        PORTMAP_ERROR_RETURN(CCL_FAIL);

    _ret = devman_device_object_identify(dev_name, discr, &devo);
    if ( _ret || !devo) 
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    _ret = devman_attr_get_buff(devo, attr_name, 
                                buf,
                                size);
    /* turn off led */
    led = 0;
    _portmap_led_write(p_priv, idx, &led);

    if (_ret) 
        PORTMAP_ERROR_RETURN(_ret);

    return CCL_OK;
}

static ccl_err_t _portmap_counters_reset(void *p_priv, b_u32 idx)
{
    portmap_ctx_t  *p_portmap;
    devo_t         devo = 0; 
    char           dev_name[DEVMAN_DEVNAME_LEN]; 
    char           attr_name[DEVMAN_DEVNAME_LEN];
    b_i32          discr;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        PORTMAP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_portmap = (portmap_ctx_t *)p_priv;

    /* figure out the appropriate to "counters_reset" device/attr */     
    _ret = devboard_portcntrsrst_to_devattr(p_portmap->brdspec.port_id[idx], 
                                            dev_name, attr_name, &discr);
    if ( _ret == CCL_NOT_SUPPORT) 
        return CCL_OK;
    else if ( _ret ) 
        PORTMAP_ERROR_RETURN(CCL_FAIL);

    _ret = devman_device_object_identify(dev_name, discr, &devo);
    if ( _ret || !devo) 
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    _ret = devman_attr_set_word(devo, attr_name, 1);
    if (_ret) 
        PORTMAP_ERROR_RETURN(_ret);

    return CCL_OK;
}

static ccl_err_t _portmap_single_counter_read(void *p_priv, b_u32 attrid, b_u32 idx, 
                                              b_u32 cntr_enum, b_u8 *p_value)
{
    portmap_ctx_t  *p_portmap;
    devo_t         devo = 0; 
    char           dev_name[DEVMAN_DEVNAME_LEN]; 
    char           attr_name[DEVMAN_DEVNAME_LEN];
    b_i32          discr;

    if ( !p_priv || !p_value) {
        PDEBUG("Error! Invalid input parameters\n");
        PORTMAP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_portmap = (portmap_ctx_t *)p_priv;

    /* figure out the appropriate to "counter" device/attr */     
    _ret = devboard_portcntr_to_devattr(p_portmap->brdspec.port_id[idx], 
                                        dev_name, attr_name, &discr);
    if ( _ret ) {
        PDEBUG("Error! devboard_portcntr_to_devattr()\n");
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }

    _ret = devman_device_object_identify(dev_name, discr, &devo);
    if ( _ret || !devo) 
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    PDEBUG("devman_attr_array_get_double().\n"
           "attr_name: %s, attrid: %d, offset: %d\n",
           attr_name, attrid, _attrs[attrid+2].offset);
    _ret = devman_attr_array_get_double(devo, attr_name, 
                                        cntr_enum, 
                                        (b_u64 *)p_value);
    if (_ret) 
        PDEBUG("Error! devman_attr_array_get_double().\n"
               "attr_name: %s, attrid: %d, offset: %d\n",
               attr_name, attrid, _attrs[attrid].offset);
        PORTMAP_ERROR_RETURN(_ret);

    return CCL_OK;
}

static ccl_err_t _portmap_if_property_get(void *p_priv, b_u32 attrid, b_u32 idx, 
                                          b_u32 prop_enum, b_u8 *p_value, b_u32 size)
{
    portmap_ctx_t  *p_portmap;
    devo_t         devo = 0; 
    char           dev_name[DEVMAN_DEVNAME_LEN]; 
    char           attr_name[DEVMAN_DEVNAME_LEN];
    b_i32          discr;

    if ( !p_priv || !p_value) {
        PDEBUG("Error! Invalid input parameters: p_priv: %p, p_value: %p\n",
               p_priv, p_value);
        PORTMAP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_portmap = (portmap_ctx_t *)p_priv;

    /* figure out the appropriate to "counter" device/attr */     
    _ret = devboard_portprop_to_devattr(p_portmap->brdspec.port_id[idx], 
                                        dev_name, attr_name, &discr);
    if ( _ret ) {
        PDEBUG("Error! devboard_portprop_to_devattr()\n");
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }

    _ret = devman_device_object_identify(dev_name, discr, &devo);
    if ( _ret || !devo) 
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    _ret = devman_attr_array_get_buff(devo, attr_name, 
                                      prop_enum, p_value, size);
    if (_ret) {
        PDEBUG("Error! devman_attr_array_get_buff()\n");
        PORTMAP_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

static ccl_err_t _portmap_if_property_set(void *p_priv, b_u32 attrid, b_u32 idx, 
                                          b_u32 prop_enum, b_u8 *p_value, b_u32 size)
{
    portmap_ctx_t  *p_portmap;
    devo_t         devo = 0; 
    char           dev_name[DEVMAN_DEVNAME_LEN]; 
    char           attr_name[DEVMAN_DEVNAME_LEN];
    b_i32          discr;

    if ( !p_priv || !p_value) {
        PDEBUG("Error! Invalid input parameters\n");
        PORTMAP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_portmap = (portmap_ctx_t *)p_priv;

    /* figure out the appropriate to "counter" device/attr */     
    _ret = devboard_portprop_to_devattr(p_portmap->brdspec.port_id[idx], 
                                        dev_name, attr_name, &discr);
    if ( _ret ) {
        PDEBUG("Error! devboard_portprop_to_devattr()\n");
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }

    _ret = devman_device_object_identify(dev_name, discr, &devo);
    if ( _ret || !devo) 
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    _ret = devman_attr_array_set_buff(devo, attr_name, 
                                      prop_enum, p_value, size);
    if (_ret) {
        PDEBUG("Error! devman_attr_array_set_buff()\n");
        PORTMAP_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

ccl_err_t portmap_attr_read(void *p_priv, b_u32 attrid, 
                            b_u32 offset, 
                            __attribute__((unused)) b_u32 offset_sz, 
                            b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || !p_value ) {
        PDEBUG("Error! Invalid input parameters\n");
        PORTMAP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch (attrid) {
    case ePORTMAP_TRANS_ID:
        _ret = _portmap_transid_read(p_priv, idx, p_value);
        break;
    case ePORTMAP_LED:
        _ret = _portmap_led_read(p_priv, idx, p_value);
        break;
    case ePORTMAP_ENABLE:
        _ret = _portmap_enable_read(p_priv, idx, p_value);
        break;
    case ePORTMAP_TX_FAULT:
        _ret = _portmap_tx_fault_read(p_priv, idx, p_value);
        break;
    case ePORTMAP_RX_LOS:
        _ret = _portmap_rx_los_read(p_priv, idx, p_value);
        break;
    case ePORTMAP_RS0:
        _ret = _portmap_rs_read(p_priv, idx, 0, p_value);
        break;
    case ePORTMAP_RS1:
        _ret = _portmap_rs_read(p_priv, idx, 1, p_value);
        break;
    case ePORTMAP_MEDIA_PRESENT:
        _ret = _portmap_media_pres_read(p_priv, idx, p_value);
        break;
    case ePORTMAP_MEDIA_ID:
        _ret = _portmap_media_id_read(p_priv, idx, p_value);
        break;
    case ePORTMAP_MEDIA_DDTYPE:
        _ret = _portmap_media_ddtype_read(p_priv, idx, p_value);
        break;
    case ePORTMAP_MEDIA_MSA:
        _ret = _portmap_media_msa_read(p_priv, idx, p_value, size);
        break;
    case ePORTMAP_MEDIA_DD:
        _ret = _portmap_media_dd_read(p_priv, idx, p_value, size);
        break;
    case ePORTMAP_MEDIA_PHY:
        _ret = _portmap_media_phy_read(p_priv, idx, p_value, size);
        break;
    case  ePORTMAP_COUNTER_RX_BYTES:
    case  ePORTMAP_COUNTER_TX_BYTES:
    case  ePORTMAP_COUNTER_RX_PACKETS:
    case  ePORTMAP_COUNTER_TX_PACKETS:
    case  ePORTMAP_COUNTER_RX_TOTAL_BYTES:
    case  ePORTMAP_COUNTER_RX_TOTAL_PACKETS:
    case  ePORTMAP_COUNTER_RX_BCAST_PACKETS:
    case  ePORTMAP_COUNTER_RX_MCAST_PACKETS:
    case  ePORTMAP_COUNTER_RX_CRC_ERRORS:
    case  ePORTMAP_COUNTER_OVERSIZE:
    case  ePORTMAP_COUNTER_FRAGMENTS:
    case  ePORTMAP_COUNTER_JABBER:
    case  ePORTMAP_COUNTER_COLLISIONS:
    case  ePORTMAP_COUNTER_LATE_COLLISIONS:
    case  ePORTMAP_COUNTER_RX_PKT_64:
    case  ePORTMAP_COUNTER_RX_PKT_65_127:
    case  ePORTMAP_COUNTER_RX_PKT_128_255:
    case  ePORTMAP_COUNTER_RX_PKT_256_511:
    case  ePORTMAP_COUNTER_RX_PKT_512_1023:
    case  ePORTMAP_COUNTER_RX_PKT_1024_1522:
    case  ePORTMAP_COUNTER_RX_ERRORS:
    case  ePORTMAP_COUNTER_RX_DROPPED:
    case  ePORTMAP_COUNTER_RX_UCAST_PACKETS:
    case  ePORTMAP_COUNTER_RX_NOUCAST_PACKETS:
    case  ePORTMAP_COUNTER_TX_ERRORS:
    case  ePORTMAP_COUNTER_TX_NOUCAST_PACKETS:
    case  ePORTMAP_COUNTER_TX_UCAST_PACKETS:
    case  ePORTMAP_COUNTER_TX_MCAST_PACKETS:
    case  ePORTMAP_COUNTER_TX_BCAST_PACKETS:
    case  ePORTMAP_COUNTER_RX_UNDERSIZE_PACKETS:
    case  ePORTMAP_COUNTER_RX_UNKNOWN_PACKETS:
    case  ePORTMAP_COUNTER_RX_ALIGNMENT_PACKETS:
    case  ePORTMAP_COUNTER_RX_FCS_ERRORS:
    case  ePORTMAP_COUNTER_SQETEST_ERRORS:
    case  ePORTMAP_COUNTER_CSE_ERRORS:
    case  ePORTMAP_COUNTER_SYMBOL_ERRORS:
    case  ePORTMAP_COUNTER_TX_MAC_ERRORS:
    case  ePORTMAP_COUNTER_RX_MAC_ERRORS:
    case  ePORTMAP_COUNTER_TOO_LONG_ERRORS:
    case  ePORTMAP_COUNTER_SINGLE_COLLISIONS:
    case  ePORTMAP_COUNTER_MULTI_COLLISIONS:
    case  ePORTMAP_COUNTER_EXCESS_COLLISIONS:
    case  ePORTMAP_COUNTER_RX_UNKNOWN_OPCODE:
    case  ePORTMAP_COUNTER_TX_DEFERRED:
    case  ePORTMAP_COUNTER_RX_PAUSE_PACKETS:
    case  ePORTMAP_COUNTER_TX_PAUSE_PACKETS:
    case  ePORTMAP_COUNTER_RX_OVERSIZE:
    case  ePORTMAP_COUNTER_TX_OVERSIZE:
    case  ePORTMAP_COUNTER_RX_FRAGMENTS:
    case  ePORTMAP_COUNTER_TX_FRAGMENTS:
    case  ePORTMAP_COUNTER_RX_JABBER:
    case  ePORTMAP_COUNTER_TX_JABBER:
        _ret = _portmap_single_counter_read(p_priv, attrid, idx, offset, p_value);
        break;
    case ePORTMAP_IF_NAME:
    case ePORTMAP_IF_ACTIVE_LINK:
        _ret = _portmap_if_property_get(p_priv, attrid, idx, 
                                        offset, p_value, size);
        break;
    case ePORTMAP_IF_MTU:
    case ePORTMAP_IF_SPEED_MAX:
    case ePORTMAP_IF_SPEED:
    case ePORTMAP_IF_DUPLEX:
    case ePORTMAP_IF_AUTONEG:
    case ePORTMAP_IF_ENCAP:
    case ePORTMAP_IF_MEDIUM:
    case ePORTMAP_IF_INTERFACE_TYPE:
        _ret = _portmap_if_type_get(p_priv, idx, p_value);
        break;
    case ePORTMAP_IF_MDIX:
    case ePORTMAP_IF_MDIX_STATUS:
    case ePORTMAP_IF_LEARN:
    case ePORTMAP_IF_IFILTER:
    case ePORTMAP_IF_SELF_EFILTER:
    case ePORTMAP_IF_LOCK:
    case ePORTMAP_IF_DROP_ON_LOCK:
    case ePORTMAP_IF_FWD_UNK:
    case ePORTMAP_IF_BCAST_LIMIT:
    case ePORTMAP_IF_ABILITY:
    case ePORTMAP_IF_FLOWCONTROL:
    case ePORTMAP_IF_ENABLE:
    case ePORTMAP_IF_SFP_PRESENT:
        _ret = _portmap_media_pres_read(p_priv, idx, p_value);
        break;
    case ePORTMAP_IF_SFP_TX_STATE:
    case ePORTMAP_IF_SFP_RX_STATE:
    case ePORTMAP_IF_DEFAULT_VLAN:
    case ePORTMAP_IF_PRIORITY:
    case ePORTMAP_IF_LED_STATE:
        _ret = _portmap_led_read(p_priv, idx, p_value);
        break;
    case ePORTMAP_IF_MEDIA_PARAMETERS:
        _ret = _portmap_media_params_read(p_priv, idx, 
                                          MEDIA_DATA_TYPE_MEDIA_DESCRIPTION,
                                          0, 96, 
                                          p_value, size);
    case ePORTMAP_IF_MEDIA_DETAILS:
        _ret = _portmap_media_params_details_get(p_priv, idx, p_value, size);
    case ePORTMAP_IF_LOCAL_ADVERT:
    case ePORTMAP_IF_MEDIA_TYPE:
        _ret = _portmap_media_link_type(p_priv, idx, p_value, size);
    case ePORTMAP_IF_SFP_PORT_TYPE:
    case ePORTMAP_IF_MEDIA_CONFIG_TYPE:
    case ePORTMAP_IF_MEDIA_ERROR_CONFIG_TYPE:
    case ePORTMAP_IF_MEDIA_SGMII_FIBER:
    case ePORTMAP_IF_TX_ENABLE:
        _ret = _portmap_enable_read(p_priv, idx, p_value);
        break;
    case ePORTMAP_IF_SFP_PORT_NUMBERS:
    case ePORTMAP_IF_AUTONEG_DUPLEX:
    case ePORTMAP_IF_AUTONEG_SPEED :
    case ePORTMAP_IF_DDM_PARAMETERS:
        _ret = _portmap_media_ddm_params_get(p_priv, idx, p_value, size);
        break;
    case ePORTMAP_IF_DDM_STATUS: 
        _ret = _portmap_media_ddm_status(p_priv, idx, p_value, size);
        break;
    case ePORTMAP_IF_DDM_SUPPORTED:
        _ret = _portmap_media_is_ddm_supported(p_priv, idx, p_value, size);
        break;
    case ePORTMAP_IF_HWADDRESS_GET:
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK) {
        PDEBUG("Failure in attribute id: %d\n", attrid);
    }

    return _ret;               
}

ccl_err_t portmap_attr_write(void *p_priv, b_u32 attrid, 
                             b_u32 offset, 
                             __attribute__((unused)) b_u32 offset_sz, 
                             b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || !p_value ) {
        PDEBUG("Error! Invalid input parameters\n");
        PORTMAP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch (attrid) {
    case ePORTMAP_LED:
        _ret = _portmap_led_write(p_priv, idx, p_value);
        break;
    case ePORTMAP_ENABLE:
        _ret = _portmap_enable_write(p_priv, idx, p_value);
        break;
    case ePORTMAP_RESET:
        _ret = _portmap_reset_write(p_priv, idx, p_value);
        break;
    case ePORTMAP_RS0:
        _ret = _portmap_rs_write(p_priv, idx, 0, p_value);
        break;
    case ePORTMAP_RS1:
        _ret = _portmap_rs_write(p_priv, idx, 1, p_value);
        break;
    case ePORTMAP_MODULE_SELECT:
        _ret = _portmap_modsel_write(p_priv, idx, p_value);
        break;
    case ePORTMAP_LPMODE:
        _ret = _portmap_lpmode_write(p_priv, idx, p_value);
        break;
    case ePORTMAP_INIT:
        _ret = _portmap_init(p_priv, idx);
        break;
    case ePORTMAP_CONFIGURE_TEST:
        _ret = _portmap_configure_test(p_priv, idx);
        break;
    case ePORTMAP_RUN_TEST:
        _ret = _portmap_test(p_priv, idx, p_value, size);
        break;
    case ePORTMAP_COUNTERS_RESET:
        _ret = _portmap_counters_reset(p_priv, idx);
        break;
    case ePORTMAP_IF_NAME:
    case ePORTMAP_IF_MTU:
    case ePORTMAP_IF_SPEED_MAX:
    case ePORTMAP_IF_SPEED:
    case ePORTMAP_IF_DUPLEX:
    case ePORTMAP_IF_AUTONEG:
    case ePORTMAP_IF_ENCAP:
    case ePORTMAP_IF_MEDIUM:
    case ePORTMAP_IF_INTERFACE_TYPE:
    case ePORTMAP_IF_MDIX:
    case ePORTMAP_IF_MDIX_STATUS:
    case ePORTMAP_IF_LEARN:
    case ePORTMAP_IF_IFILTER:
    case ePORTMAP_IF_SELF_EFILTER:
    case ePORTMAP_IF_LOCK:
    case ePORTMAP_IF_DROP_ON_LOCK:
    case ePORTMAP_IF_FWD_UNK:
    case ePORTMAP_IF_BCAST_LIMIT:
    case ePORTMAP_IF_ABILITY:
    case ePORTMAP_IF_FLOWCONTROL:
    case ePORTMAP_IF_ENABLE:
    case ePORTMAP_IF_SFP_PRESENT:
    case ePORTMAP_IF_SFP_TX_STATE:
    case ePORTMAP_IF_SFP_RX_STATE:
    case ePORTMAP_IF_DEFAULT_VLAN:
    case ePORTMAP_IF_PRIORITY:
    case ePORTMAP_IF_LED_STATE:
    case ePORTMAP_IF_MEDIA_PARAMETERS:
    case ePORTMAP_IF_MEDIA_DETAILS:
    case ePORTMAP_IF_LOCAL_ADVERT:
    case ePORTMAP_IF_MEDIA_TYPE:
    case ePORTMAP_IF_SFP_PORT_TYPE:
    case ePORTMAP_IF_MEDIA_CONFIG_TYPE:
    case ePORTMAP_IF_MEDIA_ERROR_CONFIG_TYPE:
    case ePORTMAP_IF_MEDIA_SGMII_FIBER:
    case ePORTMAP_IF_TX_ENABLE:
    case ePORTMAP_IF_SFP_PORT_NUMBERS:
    case ePORTMAP_IF_AUTONEG_DUPLEX:
    case ePORTMAP_IF_AUTONEG_SPEED :
    case ePORTMAP_IF_DDM_PARAMETERS:
    case ePORTMAP_IF_DDM_STATUS: 
    case ePORTMAP_IF_DDM_SUPPORTED:
    case ePORTMAP_IF_HWADDRESS_GET:
    case ePORTMAP_IF_ACTIVE_LINK:
        _ret = _portmap_if_property_set(p_priv, attrid, offset, 
                                        idx, p_value, size);
        break;
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK) {
        PDEBUG("Failure in attribute id: %d\n", attrid);
    }

    return _ret;               
}

/** device specific initialization */
static ccl_err_t _fportmap_add(void *devo, void *brdspec)
{
    portmap_ctx_t       *p_portmap;
    portmap_brdspec_t   *p_brdspec;
    b_u32               i;

    if ( !devo || !brdspec ) {
        PDEBUG("Error! Invalid input parameters\n");
        PORTMAP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_portmap = (portmap_ctx_t *)devman_device_private(devo);
    p_brdspec = (portmap_brdspec_t *)brdspec;

    p_portmap->devo = devo;    
    /* save the board specific info */
    memcpy(&p_portmap->brdspec, p_brdspec, sizeof(portmap_brdspec_t));

    for (i = 0; i < p_brdspec->ports_num; i++) {
        PDEBUG("name[%d]=%s\n", i, p_brdspec->name[i]);
    }

    /* put in order the attributes arrays according to the real ports number */
    for ( i = 0; i < sizeof(_attrs)/sizeof(_attrs[0]); i++ ) {
        if ( _attrs[i].maxnum == DEVBRD_PORTS_MAXNUM )
            _attrs[i].maxnum = p_brdspec->ports_num;
    }
    return CCL_OK;
}

/** device specific deinitialization */
static ccl_err_t _fportmap_cut(void *devo)
{
    if ( !devo ) {
        PDEBUG("Error! Invalid input parameters\n");
        PORTMAP_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    return CCL_OK;
}

/** portmap device type */
static device_driver_t _portmap_driver = {
    .name = "portmap",
    .f_add = _fportmap_add,
    .f_cut = _fportmap_cut,
    .attrs_num = sizeof(_attrs)/sizeof(_attrs[0]),
    .attrs = _attrs,
    .priv_size = sizeof(portmap_ctx_t)
}; 

/** 
 *  Initialize PORTMAP device type
 */

ccl_err_t devportmap_init(void)
{
    /* register device type */
    _ret = devman_driver_register(&_portmap_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to register driver\n");
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

/** 
 *  Deinitialize PORTMAP device type
 */
ccl_err_t devportmap_fini(void)
{
    /* unregister device type */
    _ret = devman_driver_unregister(&_portmap_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to unregister driver\n");
        PORTMAP_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

