/********************************************************************************/
/**
 * @file eswitch.c
 * @brief ComCores Lit Switch IP device manager driver 
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <semaphore.h>
#include <linux/limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "spibus.h"
#include "eswitch.h"

#define ESWITCH_DEBUG
/* printing/error-returning macros */
#ifdef ESWITCH_DEBUG
    #define PDEBUG(fmt, args...) \
         ccl_syslog_err("ESWITCH_DEBUG: %s(): " fmt, __FUNCTION__, ## args)
#else
    #define PDEBUG(fmt, args...)
#endif
#define ESWITCH_ERROR_RETURN(err) do { \
   ccl_syslog_err("Error! %s[%d], error=%i\n", __FUNCTION__, __LINE__, (err)); \
   return err; \
} while (0)

#define ESWITCH_REG_READ(context, offset, offset_sz, idx, pvalue, size) do { \
    _ret = _eswitch_reg_read((context), \
                          (offset), \
                          (offset_sz), (idx), \
                          (pvalue), (size)); \
    if ( _ret ) { \
        PDEBUG("_eswitch_reg_read error\n"); \
        ESWITCH_ERROR_RETURN(_ret); \
    } \
} while (0)

#define ESWITCH_REG_WRITE(context, offset, offset_sz, idx, pvalue, size) do { \
    _ret = _eswitch_reg_write((context), \
                           (offset), \
                           (offset_sz), (idx), \
                           (pvalue), (size)); \
    if ( _ret ) { \
        PDEBUG("_eswitch_reg_write error\n"); \
        ESWITCH_ERROR_RETURN(_ret); \
    } \
} while (0)

/* eswitch registers offsets */
#define ESWITCH_IP_ID                   0x00   /**< Unique IP ID */
#define ESWITCH_IP_VERSION              0x04   /**< IP version information */
#define ESWITCH_GEN_INFO_BASIC          0x08   /**< Generic information */
#define ESWITCH_GEN_CORE_KHZ            0x0C   /**< Speed of the core clock */
#define ESWITCH_GEN_INFO_MEM            0x10   /**< Generated memory information */
#define ESWITCH_GEN_INFO_OTHER          0x14   /**< Other generic information */
#define ESWITCH_PORT_SEL                0x18   /**< Port select for statistics and configuration */ 
#define ESWITCH_MAC_ENTRY_L             0x1C   /**< 32 lsb of MAC address */
#define ESWITCH_MAC_ENTRY_H             0x20   /**< 16 msb of MAC address and MAC controls */
#define ESWITCH_VLAN_CFG                0x24   /**< VLAN config for selected port */
#define ESWITCH_QUEUE_CFG               0x28   /**< Scheduling algorithm selection */
#define ESWITCH_MAC_ENTRY_PORTS_1       0x2C   /**< Port mask for MAC insert for ports 0-31 */
#define ESWITCH_MAC_ENTRY_PORTS_2       0x30   /**< Port mask for MAC insert for ports 32-63 */
#define ESWITCH_MAC_ENTRY_PORTS_3       0x34   /**< Port mask for MAC insert for ports 94-95 */
#define ESWITCH_MAC_AGEOUT              0x38   /**< MAC ageout control */
#define ESWITCH_GLOBAL_SNAPSHOT         0x3C   /**< Global snapshot */
#define ESWITCH_PORT_TX_PCK             0x40   /**< Tx frames from the selected port */
#define ESWITCH_PORT_RX_PCK             0x44   /**< Rx frames on the selected port */
#define ESWITCH_PORT_TX_BYTE            0x48   /**< Tx bytes from the selected port (NOT IMPLEMENTED in IP) */
#define ESWITCH_PORT_RX_BYTE            0x4C   /**< Rx bytes on the selected port (NOT IMPLEMENTED in IP) */
#define ESWITCH_PORT_TX_ABORT_OVFL      0x50   /**< Tx overflow errors on the selected port */
#define ESWITCH_PORT_RX_ABORT_OVFL      0x54   /**< Rx overflow errors on the selected port */
#define ESWITCH_PORT_TX_PCK_ABORT       0x58   /**< Tx PCS errors */
#define ESWITCH_PORT_RX_PCK_ABORT       0x5C   /**< Rx PCS errors */
#define ESWITCH_MTU                     0x60   /**< Global Maximum Transmission Unit */

#define ESWITCH_MAX_OFFSET ESWITCH_MTU

/* ESWITCH_IP_VERSION masks aESWITCH_GEN_INFO_MEMnd shifts */
#define ESWITCH_IP_VERSION_STATUS_MASK               0x80000000 
#define ESWITCH_IP_VERSION_MAJ_MASK                  0x7FFF0000 
#define ESWITCH_IP_VERSION_MIN_MASK                  0x0000FFFF 
                                                                
/* ESWITCH_GEN_INFO_BASIC masks and shifts */                   
#define ESWITCH_GEN_INFO_RESET_ACTIVE_MASK           0x20000000 
#define ESWITCH_GEN_INFO_FAIR_QUEUE_MASK             0x10000000 
#define ESWITCH_GEN_INFO_VLAN_EN_MASK                0x08000000 
#define ESWITCH_GEN_INFO_PTP_EN_MASK                 0x04000000 
#define ESWITCH_GEN_INFO_SF_CROSSBAR_MASK            0x02000000 
#define ESWITCH_GEN_INFO_DC_SCHED_MASK               0x01000000 
#define ESWITCH_GEN_INFO_N_1G_PORTS_MASK             0x00FF0000 
#define ESWITCH_GEN_INFO_N_10G_PORTS_MASK            0x0000FF00 
#define ESWITCH_GEN_INFO_N_25G_PORTS_MASK            0x000000FF 
#define ESWITCH_GEN_INFO_N_1G_PORTS_SHIFT            16 


/* ESWITCH_GEN_INFO_MEM masks and shifts */
#define ESWITCH_GEN_INFO_MEM_MAC_TABLE_DEPTH_MASK    0xFF000000 
#define ESWITCH_GEN_INFO_MEM_METH_BUF_DEPTH_MASK     0x00FF0000 
#define ESWITCH_GEN_INFO_MEM_RX_FIFO_DEPTH_MASK      0x0000FF00 
#define ESWITCH_GEN_INFO_MEM_TX_FIFO_DEPTH_MASK      0x000000FF 

/* ESWITCH_GEN_INFO_OTHER masks and shifts */
#define ESWITCH_GEN_INFO_OTHER_MAC_BUCKET_BIT_MASK   0x0000FF00  
#define ESWITCH_GEN_INFO_OTHER_PTP_TS_DEPTH_MASK     0x000000FF  
                                                                 
/* ESWITCH_PORT_SEL masks and shifts */                          
#define ESWITCH_PORT_SEL_SNAPSHOT_MASK               0x00000100  
#define ESWITCH_PORT_SEL_MASK                        0x000000FF  
                                                                 
/* ESWITCH_MAC_ENTRY_H masks and shifts */                       
#define ESWITCH_MAC_ENTRY_H_MAC_INSERT_PROC_MASK     0x00080000  
#define ESWITCH_MAC_ENTRY_H_ENTRY_CFG_MASK           0x00070000  
#define ESWITCH_MAC_ENTRY_H_MAC_ADDR_MASK            0x0000FFFF  
                                                                 
/* ESWITCH_VLAN_CFG masks and shifts */                          
#define ESWITCH_VLAN_CFG_INSERT_PROC                 0x80000000  
#define ESWITCH_VLAN_CFG_PORT_TX_PCP_OVERRIDE_MASK   0x10000000  
#define ESWITCH_VLAN_CFG_PORT_TX_DEI_MASK            0x08000000  
#define ESWITCH_VLAN_CFG_PORT_TX_PCP_MASK            0x07000000  
#define ESWITCH_VLAN_CFG_PORT_RX_PCP_OVERRIDE_MASK   0x00100000  
#define ESWITCH_VLAN_CFG_PORT_RX_DEI_MASK            0x00080000  
#define ESWITCH_VLAN_CFG_PORT_RX_PCP_MASK            0x00070000  
#define ESWITCH_VLAN_CFG_PORT_ACCEPT_ALL_MASK        0x00002000  
#define ESWITCH_VLAN_CFG_PORT_TRUNK_MASK             0x00001000  
#define ESWITCH_VLAN_CFG_PORT_PVID_MASK              0x00000FFF  
                                                                 
/* ESWITCH_QUEUE_CFG masks and shifts */                         
#define ESWITCH_QUEUE_CFG_FAIR_QUEUE_EN_MASK         0x00000001  
                                                                 
/* ESWITCH_MAC_AGEOUT masks and shifts */                        
#define ESWITCH_MAC_AGEOUT_AGE_EN_MASK               0x00000040  
#define ESWITCH_MAC_AGEOUT_MAC_AGETOUT_MASK          0x0000003F  
                                                                 
/* ESWITCH_GLOBAL_SNAPSHOT masks and shifts */                   
#define ESWITCH_GLOBAL_SNAPSHOT_MASK                 0x00000001  
                                                                 
/* ESWITCH_MTU masks and shifts */                               
#define ESWITCH_MTU_MASK                             0x00003FFF  

#define ESWITCH_DEFAULT_AGING_TIME                   5
#define ESWITCH_DEFAULT_MTU                          9216


/* the eswitch internal control structure */      
typedef struct eswitch_ctx_t {
    void                    *devo;
    eswitch_brdspec_t       brdspec;
    eth_counter_t           **counters;
    b_u32                   counters_num;
} eswitch_ctx_t;

static ccl_err_t _ret;                               


static eth_counter_t _eswitch_counters[] = {
    {
        .name = "tx_packets", .offset = ESWITCH_PORT_TX_PCK
    },
    {
        .name = "rx_packets", .offset = ESWITCH_PORT_RX_PCK
    },
    {
        .name = "tx_overflow_err", .offset = ESWITCH_PORT_TX_ABORT_OVFL
    },
    {
        .name = "rx_overflow_err", .offset = ESWITCH_PORT_RX_ABORT_OVFL
    },
    {
        .name = "tx_pcs_err", .offset = ESWITCH_PORT_TX_PCK_ABORT
    },
    {
        .name = "rx_pcs_err", .offset = ESWITCH_PORT_TX_PCK_ABORT
    },
};

/* eswitch register read */
static ccl_err_t _eswitch_reg_read(void *p_priv, b_u32 offset, 
                                   __attribute__((unused)) b_u32 offset_sz, 
                                   b_u32 idx, b_u8 *p_value, b_u32 size)
{
    eswitch_ctx_t *p_eswitch;

    if ( !p_priv || offset > ESWITCH_MAX_OFFSET || 
         !p_value || idx || size != sizeof(b_u32) ) {
        PDEBUG("Error! Invalid input parameters\n");
        ESWITCH_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    PDEBUGG("123$=> offset=0x%03x, size=%d\n", offset, size);

    p_eswitch = (eswitch_ctx_t *)p_priv;
    offset += p_eswitch->brdspec.offset;

    _ret = devspibus_read_buff(p_eswitch->brdspec.devname, 
                               offset, idx, p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to read over spi bus\n");
        ESWITCH_ERROR_RETURN(CCL_FAIL);
    }

    PDEBUG("p_eswitch->brdspec.offset=0x%x, offset=0x%04x, value=0x%08x\n", 
           p_eswitch->brdspec.offset, offset, *(b_u32 *)p_value);

    return _ret;
}

/* eswitch register write */
static ccl_err_t _eswitch_reg_write(void *p_priv, b_u32 offset, 
                                 __attribute__((unused)) b_u32 offset_sz, 
                                 b_u32 idx, b_u8 *p_value, b_u32 size)
{
    eswitch_ctx_t *p_eswitch;

    if ( !p_priv || offset > ESWITCH_MAX_OFFSET || 
         !p_value || idx || size != sizeof(b_u32) ) {
        PDEBUG("Error! Invalid input parameters\n");
        ESWITCH_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    PDEBUGG("123$=> offset=0x%02x, value=0x%08x\n", offset, *p_value);

    p_eswitch = (eswitch_ctx_t *)p_priv;
    offset += p_eswitch->brdspec.offset;

    PDEBUG("p_eswitch->brdspec.offset=0x%x, offset=0x%04x, value=0x%08x\n", 
           p_eswitch->brdspec.offset, offset, *(b_u32 *)p_value);

    _ret = devspibus_write_buff(p_eswitch->brdspec.devname, 
                                offset, idx, p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to write over spi bus\n");
        ESWITCH_ERROR_RETURN(_ret);
    }    

    return _ret;
}

static ccl_err_t _eswitch_init(void *p_priv)
{
    eswitch_ctx_t   *p_eswitch;
    b_u32           regval;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        ESWITCH_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_eswitch = (eswitch_ctx_t *)p_priv;

    /* configure aging */
    regval = ESWITCH_MAC_AGEOUT_AGE_EN_MASK | 
             (ESWITCH_DEFAULT_AGING_TIME & 
             ESWITCH_MAC_AGEOUT_MAC_AGETOUT_MASK);

    ESWITCH_REG_WRITE(p_eswitch, ESWITCH_MAC_AGEOUT, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));

    /* configure mtu */
    regval = ESWITCH_DEFAULT_MTU & ESWITCH_MTU_MASK;
    ESWITCH_REG_WRITE(p_eswitch, ESWITCH_MTU, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

static ccl_err_t _eswitch_configure_test(void *p_priv)
{
    eswitch_ctx_t   *p_eswitch;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        ESWITCH_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_eswitch = (eswitch_ctx_t *)p_priv;

    return CCL_OK;
}

static ccl_err_t _eswitch_run_test(void *p_priv, 
                                    __attribute__((unused)) b_u8 *buf, 
                                    __attribute__((unused)) b_u32 size)
{
    eswitch_ctx_t   *p_eswitch;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        ESWITCH_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_eswitch = (eswitch_ctx_t *)p_priv;

    return CCL_OK;
}

ccl_err_t eswitch_attr_read(void *p_priv, b_u32 attrid, 
                             b_u32 offset, b_u32 offset_sz, 
                             b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || offset > ESWITCH_MAX_OFFSET || 
         !p_value || idx  ) {
        PDEBUG("Error! Invalid input parameters\n");
        ESWITCH_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch (attrid) {
    case eESWITCH_IP_ID:                     
    case eESWITCH_IP_VERSION:
    case eESWITCH_GEN_INFO_BASIC:
    case eESWITCH_GEN_CORE_KHZ:
    case eESWITCH_GEN_INFO_MEM:
    case eESWITCH_GEN_INFO_OTHER:
    case eESWITCH_PORT_SEL:
    case eESWITCH_MAC_ENTRY_L:
    case eESWITCH_MAC_ENTRY_H:
    case eESWITCH_VLAN_CFG:
    case eESWITCH_QUEUE_CFG:
    case eESWITCH_MAC_ENTRY_PORTS_1:
    case eESWITCH_MAC_ENTRY_PORTS_2:
    case eESWITCH_MAC_ENTRY_PORTS_3:
    case eESWITCH_MAC_AGEOUT:
    case eESWITCH_PORT_TX_PCK:
    case eESWITCH_PORT_RX_PCK:
    case eESWITCH_PORT_TX_ABORT_OVFL:
    case eESWITCH_PORT_RX_ABORT_OVFL:
    case eESWITCH_PORT_TX_PCK_ABORT:
    case eESWITCH_PORT_RX_PCK_ABORT:
    case eESWITCH_MTU:
        ESWITCH_REG_READ(p_priv, offset, offset_sz, 
                         idx, p_value, size);
        break;
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK) {
        PDEBUG("Failure in attribute id: %d\n", attrid);
    }

    return _ret;               
}

ccl_err_t eswitch_attr_write(void *p_priv, b_u32 attrid, 
                              b_u32 offset, b_u32 offset_sz, 
                              b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || offset > ESWITCH_MAX_OFFSET || 
         !p_value || idx ) {
        PDEBUG("Error! Invalid input parameters\n");
        ESWITCH_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch (attrid) {
    case eESWITCH_PORT_SEL:
    case eESWITCH_MAC_ENTRY_L:
    case eESWITCH_MAC_ENTRY_H:
    case eESWITCH_VLAN_CFG:
    case eESWITCH_QUEUE_CFG:
    case eESWITCH_MAC_ENTRY_PORTS_1:
    case eESWITCH_MAC_ENTRY_PORTS_2:
    case eESWITCH_MAC_ENTRY_PORTS_3:
    case eESWITCH_MAC_AGEOUT:
    case eESWITCH_GLOBAL_SNAPSHOT:
    case eESWITCH_MTU:
        ESWITCH_REG_WRITE(p_priv, offset, offset_sz, 
                          idx, p_value, size);
        break;
    case eESWITCH_INIT:
        _ret = _eswitch_init(p_priv);
        break;
    case eESWITCH_CONFIGURE_TEST:
        _ret = _eswitch_configure_test(p_priv);
        break;
    case eESWITCH_RUN_TEST:
        _ret = _eswitch_run_test(p_priv, p_value, size);
        break;
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK) {
        PDEBUG("Failure in attribute id: %d\n", attrid);
    }

    return _ret;               
}

/* eswitch attributes definition */
static device_attr_t _attrs[] = {
    {
        .name = "name", 
        .type = eDEVMAN_ATTR_STRING, .maxnum = 1, 
        .offset = offsetof(struct eswitch_ctx_t, brdspec.name),
        .size = DEVMAN_DEVNAME_LEN,
        .format = "%s"
    },
    {
        .name = "offset", 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset = offsetof(struct eswitch_ctx_t, brdspec.offset),
        .format = "0x%08x"
    },
    {
        .name = "id", .id = eESWITCH_IP_ID, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = ESWITCH_IP_ID,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "version", .id = eESWITCH_IP_VERSION, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = ESWITCH_IP_VERSION,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "gen_info", .id = eESWITCH_GEN_INFO_BASIC, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = ESWITCH_GEN_INFO_BASIC,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "gen_core_khz", .id = eESWITCH_GEN_CORE_KHZ, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = ESWITCH_GEN_CORE_KHZ,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "gen_info_mem", .id = eESWITCH_GEN_INFO_MEM, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = ESWITCH_GEN_INFO_MEM,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "gen_info_other", .id = eESWITCH_GEN_INFO_OTHER, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = ESWITCH_GEN_INFO_OTHER,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "port_sel", .id = eESWITCH_PORT_SEL, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = ESWITCH_PORT_SEL
    },
    {
        .name = "mac_entry_l", .id = eESWITCH_MAC_ENTRY_L, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = ESWITCH_MAC_ENTRY_L
    },
    {
        .name = "mac_entry_h", .id = eESWITCH_MAC_ENTRY_H, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = ESWITCH_MAC_ENTRY_H
    },
    {
        .name = "vlan_cfg", .id = eESWITCH_VLAN_CFG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = ESWITCH_VLAN_CFG
    },
    {
        .name = "queue_cfg", .id = eESWITCH_QUEUE_CFG, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = ESWITCH_QUEUE_CFG
    },
    {
        .name = "mac_entry_ports_1", .id = eESWITCH_MAC_ENTRY_PORTS_1, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = ESWITCH_MAC_ENTRY_PORTS_1
    },
    {
        .name = "mac_entry_ports_2", .id = eESWITCH_MAC_ENTRY_PORTS_2, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = ESWITCH_MAC_ENTRY_PORTS_2
    },
    {
        .name = "mac_entry_ports_3", .id = eESWITCH_MAC_ENTRY_PORTS_3, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = ESWITCH_MAC_ENTRY_PORTS_3
    },
    {
        .name = "mac_ageout", .id = eESWITCH_MAC_AGEOUT, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = ESWITCH_MAC_AGEOUT
    },
    {
        .name = "snapshot", .id = eESWITCH_GLOBAL_SNAPSHOT, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = ESWITCH_GLOBAL_SNAPSHOT,
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "tx_pkt", .id = eESWITCH_PORT_TX_PCK, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = ESWITCH_PORT_TX_PCK,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_pkt", .id = eESWITCH_PORT_RX_PCK, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = ESWITCH_PORT_RX_PCK,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_ovfl_err", .id = eESWITCH_PORT_TX_ABORT_OVFL, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = ESWITCH_PORT_TX_ABORT_OVFL,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_ovfl_err", .id = eESWITCH_PORT_RX_ABORT_OVFL, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = ESWITCH_PORT_RX_ABORT_OVFL,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "tx_pcs_err", .id = eESWITCH_PORT_TX_PCK_ABORT, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = ESWITCH_PORT_TX_PCK_ABORT,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "rx_pcs_err", .id = eESWITCH_PORT_RX_PCK_ABORT, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = ESWITCH_PORT_RX_PCK_ABORT,
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "mtu", .id = eESWITCH_MTU, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = ESWITCH_MTU
    },
    {
        .name = "init", .id = eESWITCH_INIT, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "config_test", .id = eESWITCH_CONFIGURE_TEST, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "run_test", .id = eESWITCH_RUN_TEST, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, 
        .size = sizeof(board_dev_stats_t), 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
};

static ccl_err_t _eswitch_read_counters(eswitch_ctx_t *p_eswitch)
{
    b_u32 regval;
    b_u32 i, j;

    if ( !p_eswitch ) {
        PDEBUG("Error! Invalid input parameters\n");
        ESWITCH_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    regval = ESWITCH_GLOBAL_SNAPSHOT_MASK;

    ESWITCH_REG_WRITE(p_eswitch, ESWITCH_GLOBAL_SNAPSHOT, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));

    for (i = 0; i < p_eswitch->brdspec.port_num; i++) {
        regval = i;
        ESWITCH_REG_WRITE(p_eswitch, ESWITCH_PORT_SEL, 
                          sizeof(b_u32), 0, 
                          (b_u8 *)&regval, sizeof(b_u32));
        for (j = 0; j < p_eswitch->counters_num; j++) {
            ESWITCH_REG_READ(p_eswitch, p_eswitch->counters[i][j].offset, 
                           sizeof(b_u32), 0, 
                           (b_u8 *)&p_eswitch->counters[i][j].value, 
                           sizeof(b_u32));
        }
    }

    return CCL_OK;
}

static ccl_err_t _feswitch_cmd_init(devo_t devo, 
                                    __attribute__((unused)) device_cmd_parm_t parms[], 
                                    __attribute__((unused)) b_u16 parms_num)
{
    eswitch_ctx_t *p_eswitch = devman_device_private(devo);

    /* init the device */
    _ret = _eswitch_init(p_eswitch);
    if ( _ret ) {
        PDEBUG("_eswitch_init error\n");
        ESWITCH_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

static ccl_err_t _feswitch_cmd_show_counters_changed(devo_t devo, 
                                                   __attribute__((unused)) device_cmd_parm_t parms[], 
                                                   __attribute__((unused)) b_u16 parms_num)
{
    eswitch_ctx_t *p_eswitch = devman_device_private(devo);
    char          cname[100] = {0};
    char          tmpstr[50] = {0};
    b_u32         offset = 45;
    b_u32         clen;
    b_u32         regval;
    b_u32         value;
    b_u32         i, j;

    _ret = _eswitch_read_counters(p_eswitch);
    if ( _ret ) {
        PDEBUG("_eswitch_read_counters\n");
        ESWITCH_ERROR_RETURN(_ret);
    }

    for (i = 0; i < p_eswitch->brdspec.port_num; i++) {
        snprintf(tmpstr, sizeof(tmpstr), "------port %d - %s",
                 i, p_eswitch->brdspec.port_name[i]);
        snprintf(cname, sizeof(cname), "%s%.*s", 
                 tmpstr,
                 offset - strnlen(tmpstr, sizeof(tmpstr)) + 1, 
                 "--------------------------------------------");
        PRINTL("%s\n", cname);
        for (j = 0; j < p_eswitch->counters_num; j++) {
            value = p_eswitch->counters[i][j].value - p_eswitch->counters[i][j].value_prev;
            if (p_eswitch->counters[i][j].value_prev > p_eswitch->counters[i][j].value) {
                value = (p_eswitch->counters[i][j].value_prev - 
                         p_eswitch->counters[i][j].value + 
                         MAX_UINT32);
            }
            snprintf(cname, sizeof(cname), "%s:", p_eswitch->counters[i][j].name);
            clen = strnlen(cname, sizeof(cname));
            if(offset <= clen) 
                offset = clen + 1;
            snprintf(cname+clen, sizeof(cname), "%*s", 
                     offset-clen, " ");
            PRINTL("%s%u\n", 
                   cname,
                   value);
            p_eswitch->counters[i][j].value_prev = p_eswitch->counters[i][j].value;
        }
    }

    return CCL_OK;
}

/* device commands */
static device_cmd_t _cmds[] = 
{
    { 
        .name = "init", .f_cmd = _feswitch_cmd_init, .parms_num = 0,
        .help = "Init Ethernet Switch device" 
    },
    { 
        .name = "counters_changed", .f_cmd = _feswitch_cmd_show_counters_changed, .parms_num = 0,
        .help = "Show Ethernet Switch changed counters for all ports" 
    },
};

/* Device instance hw-specific initializations 
 */
static ccl_err_t _feswitch_add(void *devo, void *brdspec)
{
    eswitch_ctx_t        *p_eswitch;
    eswitch_brdspec_t    *p_brdspec;
    b_u32                i, j;

    if ( !devo || !brdspec ) {
        PDEBUG("Error! Invalid input parameters\n");
        ESWITCH_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_eswitch = devman_device_private(devo);
    if ( !p_eswitch ) {
        PDEBUG("NULL context area\n");
        ESWITCH_ERROR_RETURN(CCL_FAIL);
    }
    p_brdspec = (eswitch_brdspec_t *)brdspec;

    p_eswitch->devo = devo;    
    memcpy(&p_eswitch->brdspec, p_brdspec, sizeof(eswitch_brdspec_t));

    p_eswitch->counters = 
               (eth_counter_t **)calloc(p_brdspec->port_num, sizeof(eth_counter_t *));
    if (!p_eswitch->counters) {
        PDEBUG("Memory allocation failed\n");
        ESWITCH_ERROR_RETURN(CCL_NO_MEM_ERR);
    }
    p_eswitch->counters_num = sizeof(_eswitch_counters)/sizeof(_eswitch_counters[0]);
    for (i = 0; i < p_brdspec->port_num; i++) {
        p_eswitch->counters[i] = 
                   (eth_counter_t *)calloc(p_eswitch->counters_num, sizeof(eth_counter_t));
        if (!p_eswitch->counters[i]) {
            PDEBUG("Memory allocation failed\n");
            ESWITCH_ERROR_RETURN(CCL_NO_MEM_ERR);
        }
        for (j = 0; j < p_eswitch->counters_num; j++) 
            memcpy(&p_eswitch->counters[i][j], &_eswitch_counters[j], sizeof(eth_counter_t));
    }

    return CCL_OK;
}


/* Device instance hw-specific deinitializations 
 */
static ccl_err_t _feswitch_cut(void *devo)
{
    if ( !devo ) {
        PDEBUG("Error! Invalid input parameters\n");
        ESWITCH_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    return CCL_OK;
}

/* device type description structure */
static device_driver_t _eswitch_driver = {
    .name = "eswitch",
    .f_add = _feswitch_add,
    .f_cut = _feswitch_cut,
    .attrs = _attrs,
    .attrs_num = sizeof(_attrs)/sizeof(_attrs[0]),
    .cmds = _cmds,
    .cmds_num = sizeof(_cmds)/sizeof(_cmds[0]),
    .priv_size = sizeof(eswitch_ctx_t)
}; 

/* Initialize ESWITCH device type
 */
ccl_err_t deveswitch_init(void)
{
    /* register device type */
    _ret = devman_driver_register(&_eswitch_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to register driver\n");
        ESWITCH_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

/* Deinitialize ESWITCH device type
 */
ccl_err_t deveswitch_fini(void)
{
    /* unregister device type */
    _ret = devman_driver_unregister(&_eswitch_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to unregister driver\n");
        ESWITCH_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}


