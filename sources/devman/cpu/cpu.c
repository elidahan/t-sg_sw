/********************************************************************************/
/**
 * @file cpu.c
 * @brief CPU sensors
 * device manager driver
 * @copyright 2019 CELARE. All rights reserved.
 */
/********************************************************************************/

#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <semaphore.h>
#include <linux/limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"

//#define CPU_DEBUG
/* pointing/error-returning macros */
#ifdef CPU_DEBUG
 #define PDEBUG(fmt, args...) \
         ccl_syslog_err("CPU_DEBUG:  %s[%d]: " fmt, __FUNCTION__, __LINE__, ## args)
#else
 #define PDEBUG(fmt, args...)
#endif
#define CPU_ERROR_RETURN(err) do { \
   ccl_syslog_err("Error! %s[%d], error=%i\n", __FUNCTION__, __LINE__, (err)); \
   return err; \
} while (0)

/** @struct i2ccore_ctx_t
 *  i2ccore context structure
 */
typedef struct cpu_ctx_t {
    void              *devo;
    cpu_brdspec_t     brdspec;
} cpu_ctx_t;

static ccl_err_t  _ret;

/* cpu device attributes definition */
static device_attr_t _attrs[] = {
    {
        .name = "sensors", .id = 0, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, 
        .offset = offsetof(struct cpu_ctx_t, brdspec.sys_sensors)
    }
};

/* Device instance hw-specific initializations 
 */
static ccl_err_t _fcpu_add(void *devo, void *brdspec) {
    cpu_ctx_t       *p_cpu;
    cpu_brdspec_t   *p_brdspec;

    if ( !devo || !brdspec ) {
        PDEBUG("Error! Invalid input parameters\n");
        CPU_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_cpu = devman_device_private(devo);
    if ( !p_cpu ) {
        PDEBUG("NULL context area\n");
        CPU_ERROR_RETURN(CCL_FAIL);
    }
    p_brdspec = (cpu_brdspec_t *)brdspec;
    /* back reference the device manager object handle */    
    p_cpu->devo = devo;    
    /* save the board specific info */
    memcpy(&p_cpu->brdspec, p_brdspec, sizeof(cpu_brdspec_t));

    return CCL_OK;
}

/* Device instance hw-specific deinitializations 
 */
static ccl_err_t _fcpu_cut(void *devo)
{
    cpu_ctx_t  *p_cpu;

    if ( !devo ) {
        PDEBUG("Error! Invalid input parameters\n");
        CPU_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_cpu = devman_device_private(devo);
    if ( !p_cpu ) {
        PDEBUG("NULL context area\n");
        CPU_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

/* device type description structure */
static device_driver_t _cpu_driver = {
    .name = "cpu",
    .f_add = _fcpu_add,
    .f_cut = _fcpu_cut,
    .attrs = _attrs,
    .attrs_num = sizeof(_attrs)/sizeof(_attrs[0]),
    .priv_size = sizeof(cpu_ctx_t)
}; 

/* Initialize CPU device type
 */
ccl_err_t devcpu_init(void)
{
    /* register device type */
    _ret = devman_driver_register(&_cpu_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to register driver\n");
        CPU_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}

/* Deinitialize CPU device type
 */
ccl_err_t devcpu_fini(void)
{
    /* unregister device type */
    _ret = devman_driver_unregister(&_cpu_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to unregister driver\n");
        CPU_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}
