/********************************************************************************/
/**
 * @file smartcard.c
 * @brief Generic Smart Card device manager driver
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <semaphore.h>
#include <linux/limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "axispi.h"
#include "smartcard.h"

//#define SMARTCARD_DEBUG
/* printing/error-returning macros */
#ifdef SMARTCARD_DEBUG
    #define PDEBUG(fmt, args...) \
         ccl_syslog_err("SMARTCARD_DEBUG: %s[%d]: " fmt, __FUNCTION__, __LINE__, ## args)
#else
    #define PDEBUG(fmt, args...)
#endif
#define SMARTCARD_ERROR_RETURN(err) do { \
   ccl_syslog_err("Error! %s[%d], error=%i\n", __FUNCTION__, __LINE__, (err)); \
   return err; \
} while (0)

/* smart card instructions */
//TODO

/* the smartcard internal control structure */
typedef struct smartcard_ctx_t {
    void                    *devo;
    smartcard_dev_brdspec_t brdspec;
} smartcard_ctx_t;

static ccl_err_t    _ret;


static ccl_err_t _smartcard_apdu_tx(b_u8 instr_class, b_u8 instr_code, 
                                    b_u16 intst_params, b_u8 *p_data, 
                                    b_u32 size, b_u32 resp_size)
{
    return CCL_OK;
}

static ccl_err_t _smartcard_init(void *p_priv)
{
    smartcard_ctx_t     *p_smartcard;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        SMARTCARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_smartcard = (smartcard_ctx_t *)p_priv;

    return CCL_OK;
}

static ccl_err_t _smartcard_configure_test(void *p_priv)
{
    smartcard_ctx_t     *p_smartcard;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        SMARTCARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_smartcard = (smartcard_ctx_t *)p_priv;

    return CCL_OK;
}

static ccl_err_t _smartcard_run_test(void *p_priv, 
                                     __attribute__((unused)) b_u8 *buf, 
                                     __attribute__((unused)) b_u32 size)
{
    smartcard_ctx_t     *p_smartcard;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        SMARTCARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_smartcard = (smartcard_ctx_t *)p_priv;

    return CCL_OK;
}

ccl_err_t smartcard_attr_read(void *p_priv, b_u32 attrid, 
                           b_u32 offset, 
                           __attribute__((unused)) b_u32 offset_sz, 
                           b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || !p_value || idx || !size ) {
        PDEBUG("Error! Invalid input parameters\n");
        SMARTCARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    PDEBUG("attrid: %d, offset: %d, size:%d\n\n", attrid, offset, size);
    switch (attrid) {
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK) {
        PDEBUG("Failure in attribute id: %d\n", attrid);
    }

    return _ret;
}

ccl_err_t smartcard_attr_write(void *p_priv, b_u32 attrid, 
                            b_u32 offset, 
                            __attribute__((unused)) b_u32 offset_sz, 
                            b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || !p_value || idx || !size ) {
        PDEBUG("Error! Invalid input parameters\n");
        SMARTCARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    PDEBUG("attrid: %d, offset: %d, size:%d\n\n", attrid, offset, size);
    switch (attrid) {
    case eSMARTCARD_INIT:
        _ret = _smartcard_init(p_priv);
        break;
    case eSMARTCARD_CONFIGURE_TEST:
        _ret = _smartcard_configure_test(p_priv);
        break;
    case eSMARTCARD_RUN_TEST:
        _ret = _smartcard_run_test(p_priv, p_value, size);
        break;
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK) {
        PDEBUG("Failure in attribute id: %d\n", attrid);
    }

    return _ret;
}

/* smartcard attributes definition */
static device_attr_t _attrs[] = {
    {
        .name = "uart_id", .id = eSMARTCARD_UART_ID, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset = offsetof(struct smartcard_ctx_t, brdspec.uart_id),
        .format = "%d"
    },
    {
        .name = "init", .id = eSMARTCARD_INIT, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "config_test", .id = eSMARTCARD_CONFIGURE_TEST, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "run_test", .id = eSMARTCARD_RUN_TEST, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, 
        .size = sizeof(board_dev_stats_t), 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
};

/* device commands */
static device_cmd_t _cmds[] = {
};

/* Device instance hw-specific initializations 
 */
static ccl_err_t _fsmartcard_add(void *devo, void *brdspec)
{
    smartcard_ctx_t           *p_smartcard;
    smartcard_dev_brdspec_t   *p_brdspec;

    if ( !devo || !brdspec ) {
        PDEBUG("Error! Invalid input parameters\n");
        SMARTCARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_smartcard = devman_device_private(devo);
    if ( !p_smartcard ) {
        PDEBUG("NULL context area\n");
        SMARTCARD_ERROR_RETURN(CCL_FAIL);
    }
    p_brdspec = (smartcard_dev_brdspec_t *)brdspec;

    PDEBUG("123$=> p_brdspec->dev_id=%d, p_brdspec->cs=%d\n", 
           p_brdspec->dev_id, p_brdspec->cs);

    p_smartcard->devo = devo;    
    memcpy(&p_smartcard->brdspec, p_brdspec, sizeof(smartcard_dev_brdspec_t));

    return CCL_OK;
}


/* Device instance hw-specific deinitializations 
 */
static ccl_err_t _fsmartcard_cut(void *devo)
{
    smartcard_ctx_t  *p_smartcard;
    if ( !devo ) {
        PDEBUG("Error! Invalid input parameters\n");
        SMARTCARD_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_smartcard = devman_device_private(devo);
    if ( !p_smartcard ) {
        PDEBUG("NULL context area\n");
        SMARTCARD_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

/* device type description structure */
static device_driver_t _smartcard_driver = {
    .name = "smartcard",
    .f_add = _fsmartcard_add,
    .f_cut = _fsmartcard_cut,
    .attrs = _attrs,
    .attrs_num = sizeof(_attrs)/sizeof(_attrs[0]),                 
    .cmds = _cmds,
    .cmds_num = sizeof(_cmds)/sizeof(_cmds[0]),
    .priv_size = sizeof(smartcard_ctx_t)
}; 

/* Initialize SMARTCARD device type
 */
ccl_err_t devsmartcard_init(void)
{
    /* register device type */
    _ret = devman_driver_register(&_smartcard_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to register driver\n");
        SMARTCARD_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

/* Deinitialize SMARTCARD device type
 */
ccl_err_t devsmartcard_fini(void)
{
    /* unregister device type */
    _ret = devman_driver_unregister(&_smartcard_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to unregister driver\n");
        SMARTCARD_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}


