/********************************************************************************/
/**
 * @file adt7490.c
 * @brief ADT7490 Fan Conroller device manager driver
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <semaphore.h>
#include <linux/limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "i2ccore.h"       
#include "adt7490.h"

#define ADT7490_DEBUG
/* printing/error-returning macros */
#ifdef ADT7490_DEBUG
    #define PDEBUG(fmt, args...) \
         ccl_syslog_err("ADT7490_DEBUG: %s[%d]: " fmt, __FUNCTION__, __LINE__, ## args)
#else
    #define PDEBUG(fmt, args...)
#endif
#define ADT7490_ERROR_RETURN(err) do { \
   ccl_syslog_err("Error! %s[%d], error=%i\n", __FUNCTION__, __LINE__, (err)); \
   return err; \
} while (0)

#define ADT7490_REG_READ(context, offset, offset_sz, idx, pvalue, size) do { \
    _ret = _adt7490_reg_read((context), \
                             (offset), \
                             (offset_sz), (idx), \
                             (pvalue), (size)); \
    if ( _ret ) { \
        PDEBUG("_adt7490_reg_read error\n"); \
        ADT7490_ERROR_RETURN(_ret); \
    } \
} while (0)

#define ADT7490_REG_WRITE(context, offset, offset_sz, idx, pvalue, size) do { \
    _ret = _adt7490_reg_write((context), \
                              (offset), \
                              (offset_sz), (idx), \
                              (pvalue), (size)); \
    if ( _ret ) { \
        PDEBUG("_adt7490_reg_write error\n"); \
        ADT7490_ERROR_RETURN(_ret); \
    } \
} while (0)


/* adt7490 registers offsets */
#define ADT7490_CONFIG_1                         0x40 /**< Configuration register 1 */                                                                                             
#define ADT7490_CONFIG_2                         0x73 /**< Configuration register 2 */                                                                                             
#define ADT7490_CONFIG_3                         0x78 /**< Configuration register 3 */                                                                                             
#define ADT7490_CONFIG_4                         0x7D /**< Configuration register 4 */                                                                                             
#define ADT7490_CONFIG_5                         0x7C /**< Configuration register 5 */                                                                                             
#define ADT7490_CONFIG_6                         0x10 /**< Configuration register 6 */                                                                                             
#define ADT7490_CONFIG_7                         0x11 /**< Configuration register 7 */                                                                                             
#define ADT7490_PECI0                            0x33 /**< Platform Environment Control 
                                                        *  Interface 0 (PECI) register. 
                                                        *  Eight Bits Representative of 
                                                        *  PECI Client Address 0x30 
                                                        */      
#define ADT7490_PECI1                            0x1A /**< PECI1 register. Eight Bits 
                                                        *  Representative of PECI Client 
                                                        *  Address 0x31 
                                                        */                                                
#define ADT7490_PECI2                            0x1B /**< PECI2 register. Eight Bits 
                                                        *  Representative of PECI Client 
                                                        *  Address 0x32 
                                                        */                                                
#define ADT7490_PECI3                            0x1C /**< PECI3 register. Eight Bits 
                                                        *  Representative of PECI Client 
                                                        *  Address 0x33 
                                                        */                                                
#define ADT7490_IMON                             0x1D /**< Reflects the voltage measurement 
                                                        *  at the IMON input on Pin 19 
                                                        *  (8 MSBs of reading). Input range 
                                                        *  of 0 V to 2.25 V 
                                                        */       
#define ADT7490_VTT                              0x1E /**< Reflects the voltage measurement 
                                                        *  at the VTT input on Pin 8 
                                                        *  (8 MSBs of reading). Input range 
                                                        *  of 0 V to 2.25 V 
                                                        */         
#define ADT7490_EXT_RES_3                        0x1F /**< Extended resolution 3*/                                                                                                 
#define ADT7490_VOLT_READ_PIN22                  0x20 /**< Reflects the Voltage Measurement 
                                                        *  at the 2.5 VIN Input on Pin 22 
                                                        */                                                      
#define ADT7490_VOLT_READ_PIN23                  0x21 /**< Reflects the Voltage Measurement 
                                                        *  at the VCCP Input on Pin 23 
                                                        */                                                         
#define ADT7490_VOLT_READ_PIN4                   0x22 /**< Reflects the Voltage Measurement 
                                                        *  at the VCC Input on Pin 4 
                                                        */                                                           
#define ADT7490_VOLT_READ_PIN20                  0x23 /**< Reflects the Voltage Measurement 
                                                        *  at the 5 VIN Input on Pin 20 
                                                        */                                                        
#define ADT7490_VOLT_READ_PIN21                  0x24 /**< Reflects the Voltage Measurement 
                                                        *  at the 12 VIN Input on Pin 21 
                                                        */                                                       
#define ADT7490_TEMP_READ_REM_1                  0x25 /**< Remote 1 Temperature Reading */                                                                                         
#define ADT7490_TEMP_READ_LOCAL                  0x26 /**< Local Temperature Reading */                                                                                            
#define ADT7490_TEMP_READ_REM_2                  0x27 /**< Remote 2 Temperature Reading */                                                                                         
#define ADT7490_TACH1_READ_LOW                   0x28 /**< TACH1 Low Byte */                                                                                                       
#define ADT7490_TACH1_READ_HIGH                  0x29 /**< TACH1 High Byte*/                                                                                                       
#define ADT7490_TACH2_READ_LOW                   0x2A /**< TACH2 Low Byte */                                                                                                       
#define ADT7490_TACH2_READ_HIGH                  0x2B /**< TACH2 High Byte*/                                                                                                       
#define ADT7490_TACH3_READ_LOW                   0x2C /**< TACH3 Low Byte */                                                                                                       
#define ADT7490_TACH3_READ_HIGH                  0x2D /**< TACH3 High Byte*/                                                                                                       
#define ADT7490_TACH4_READ_LOW                   0x2E /**< TACH4 Low Byte */                                                                                                       
#define ADT7490_TACH4_READ_HIGH                  0x2F /**< TACH4 High Byte*/                                                                                                       
#define ADT7490_DUTY_CYCLE_PWM1                  0x30 /**< PWM1 Current Duty Cycle */                                                                                              
#define ADT7490_DUTY_CYCLE_PWM2                  0x31 /**< PWM2 Current Duty Cycle */                                                                                              
#define ADT7490_DUTY_CYCLE_PWM3                  0x32 /**< PWM3 Current Duty Cycle */                                                                                              
#define ADT7490_PECI_LIMIT_LOW                   0x34 /**< PECI Low Limit */                                                                                                       
#define ADT7490_PECI_LIMIT_HIGH                  0x35 /**< PECI High Limit */                                                                                                      
#define ADT7490_PECI_CONFIG_1                    0x36 /**< PECI Configuration register 1 */                                                                                        
#define ADT7490_PECI_CONFIG_2                    0x88 /**< PECI Configuration register 2 */                                                                                        
#define ADT7490_DUTY_CYCLE_MAX_PWM1              0x38 /**< Maximum Duty Cycle for PWM1 */                                                                                          
#define ADT7490_DUTY_CYCLE_MAX_PWM2              0x39 /**< Maximum Duty Cycle for PWM2 */                                                                                          
#define ADT7490_DUTY_CYCLE_MAX_PWM3              0x3A /**< Maximum Duty Cycle for PWM3 */                                                                                          
#define ADT7490_PECI_T_MIN                       0x3B /**< When the PECI measurement exceeds 
                                                        *  PECI TMIN, the appropriate fans 
                                                        *  run at PWMMIN and increase according 
                                                        *  to the automatic fan speed control slope 
                                                        */                                               
#define ADT7490_PECI_T_RANGE_ENHCD               0x3C /**< PECI T-RANGE/Enhanced Acoustics register */                                                                             
#define ADT7490_PECI_T_CTRL_LIMIT                0x3D /**< PECI T-CONTROL Limit register */                                                                                        
#define ADT7490_VERSION                          0x3F /**< Version register */                                                                                                     
#define ADT7490_INT_STATUS_1                     0x41 /**< Interrupt Status 1 */                                                                                                   
#define ADT7490_INT_STATUS_2                     0x42 /**< Interrupt Status 2 */                                                                                                   
#define ADT7490_INT_STATUS_3                     0x43 /**< Interrupt Status 3 */                                                                                                   
#define ADT7490_INT_STATUS_4                     0x81 /**< Interrupt Status 4 */                                                                                                   
#define ADT7490_VIN_2_5_LIMIT_LOW                0x44 /**< +2.5 VIN Low Limit */                                                                                                   
#define ADT7490_VIN_2_5_LIMIT_HIGH               0x45 /**< +2.5 VIN High Limit */                                                                                                  
#define ADT7490_VCCP_LIMIT_LOW                   0x46 /**< VCCP Low Limit */                                                                                                       
#define ADT7490_VCCP_LIMIT_HIGH                  0x47 /**< VCCP High Limit */                                                                                                      
#define ADT7490_VCC_LIMIT_LOW                    0x48 /**< VCC Low Limit */                                                                                                        
#define ADT7490_VCC_LIMIT_HIGH                   0x49 /**< VCC High Limit */                                                                                                       
#define ADT7490_VIN_5_LIMIT_LOW                  0x4A /**< +5 VIN Low Limit */                                                                                                     
#define ADT7490_VIN_5_LIMIT_HIGH                 0x4B /**< +5 VIN High Limit */                                                                                                    
#define ADT7490_VIN_12_LIMIT_LOW                 0x4C /**< +12 VIN Low Limit*/                                                                                                     
#define ADT7490_VIN_12_LIMIT_HIGH                0x4D /**< +12 VIN High Limit */                                                                                                   
#define ADT7490_TEMP_REM_1_LIMIT_LOW             0x4E /**< Remote 1 Temperature Low Limit */                                                                                       
#define ADT7490_TEMP_REM_1_LIMIT_HIGH            0x4F /**< Remote 1 Temperature High Limit*/                                                                                       
#define ADT7490_TEMP_LOCAL_LIMIT_LOW             0x50 /**< Local Temperature Low Limit*/                                                                                           
#define ADT7490_TEMP_LOCAL_LIMIT_HIGH            0x51 /**< Local Temperature High Limit*/                                                                                          
#define ADT7490_TEMP_REM_2_LIMIT_LOW             0x52 /**< Remote 2 Temperature Low Limit */                                                                                       
#define ADT7490_TEMP_REM_2_LIMIT_HIGH            0x53 /**< Remote 2 Temperature High Limit*/                                                                                       
#define ADT7490_TACH1_LIMIT_LOW_BYTE             0x54 /**< TACH1 Minimum Low Byte */                                                                                               
#define ADT7490_TACH1_LIMIT_HIGH_BYTE            0x55 /**< TACH1 Minimum High Byte*/                                                                                               
#define ADT7490_TACH2_LIMIT_LOW_BYTE             0x56 /**< TACH2 Minimum Low Byte */                                                                                               
#define ADT7490_TACH2_LIMIT_HIGH_BYTE            0x57 /**< TACH2 Minimum High Byte*/                                                                                               
#define ADT7490_TACH3_LIMIT_LOW_BYTE             0x58 /**< TACH3 Minimum Low Byte */                                                                                               
#define ADT7490_TACH3_LIMIT_HIGH_BYTE            0x59 /**< TACH3 Minimum High Byte*/                                                                                               
#define ADT7490_TACH4_LIMIT_LOW_BYTE             0x5A /**< TACH4 Minimum Low Byte */                                                                                               
#define ADT7490_TACH4_LIMIT_HIGH_BYTE            0x5B /**< TACH4 Minimum High Byte*/                                                                                               
#define ADT7490_CONFIG_PWM1                      0x5C /**< PWM1 Configuration */                                                                                                   
#define ADT7490_CONFIG_PWM2                      0x5D /**< PWM2 Configuration*/                                                                                                    
#define ADT7490_CONFIG_PWM3                      0x5E /**< PWM3 Configuration*/                                                                                                    
#define ADT7490_TEMP_REM1_T_RANGE                0x5F /**< Remote 1 T-RANGE/PWM1 Frequency*/                                                                                       
#define ADT7490_TEMP_LOCAL_T_RANGE               0x60 /**< Local T-RANGE/PWM2 Frequency*/                                                                                                                             
#define ADT7490_TEMP_REM2_T_RANGE                0x61 /**< Remote 2 T-RANGE/PWM3 Frequency */                                                                                                                             
#define ADT7490_ENHANCED_ACOUSTICS1              0x62 /**< Enhanced Acoustics register 1 */                                                                                                                             
#define ADT7490_ENHANCED_ACOUSTICS2              0x63 /**< Enhanced Acoustics register 2 */                                                                                                                             
#define ADT7490_DUTY_CYCLE_MIN_PWM1              0x64 /**< PWM1 Minimum Duty Cycle */                                                                                                                             
#define ADT7490_DUTY_CYCLE_MIN_PWM2              0x65 /**< PWM2 Minimum Duty Cycle*/                                                                                                                             
#define ADT7490_DUTY_CYCLE_MIN_PWM3              0x66 /**< PWM3 Minimum Duty Cycle */                                                                                                                             
#define ADT7490_TEMP_MIN_REM_1                   0x67 /**< Remote 1 Temperature Tmin */                                                                                                                             
#define ADT7490_TEMP_MIN_LOCAL                   0x68 /**< Local Temperature Tmin */                                                                                                                             
#define ADT7490_TEMP_MIN_REM_2                   0x69 /**< Remote 2 Temperature Tmin */                                                                                                                             
#define ADT7490_TEMP_REM_1_THERM_LIMIT           0x6A /**< Remote 1 THERM Temperature Limit */                                                                                                                             
#define ADT7490_TEMP_LOCAL_THERM_LIMIT           0x6B /**< Local THERM Temperature Limit */                                                                                                                               
#define ADT7490_TEMP_REM_2_THERM_LIMIT           0x6C /**< Remote 2 THERM Temperature Limit */                                                                                                                              
#define ADT7490_TEMP_REM_1_LOCAL_HIST            0x6D /**< Remote 1 and Local Temperature/Tmin 
                                                        *  Hysteresis 
                                                        */                                                                                                                             
#define ADT7490_TEMP_REM_2_PECI_HIST             0x6E /**< PECI and Remote 2 Temperature/Tmin 
                                                        *  Hysteresis 
                                                        */                                                                                                                             
#define ADT7490_XNOR_TREE_TEST_EN                0x6F /**< XNOR TREE Test Enable */                                                                                                                             
#define ADT7490_TEMP_REM_1_OFFSET                0x70 /**< Remote 1 Temperature Offset */                                                                                                                             
#define ADT7490_TEMP_LOCAL_OFFSET                0x71 /**< Local Temperature Offset */                                                                                                                             
#define ADT7490_TEMP_REM_2_OFFSET                0x72 /**< Remote 2 Temperature Offset */                                                                                                                              
#define ADT7490_INT_MASK_1                       0x74 /**< Interrupt Mask 1 */                                                                                                                             
#define ADT7490_INT_MASK_2                       0x75 /**< Interrupt Mask 2 */                                                                                                                              
#define ADT7490_INT_MASK_3                       0x82 /**< Interrupt Mask 3 */                                                                                                                              
#define ADT7490_INT_MASK_4                       0x83 /**< Interrupt Mask 4 */                                                                                                                              
#define ADT7490_EXTENDED_RESOLUTION_1            0x76 /**< Extended Resolution register 1 */                                                                                                                              
#define ADT7490_EXTENDED_RESOLUTION_2            0x77 /**< Extended Resolution register 2 */                                                                                                                             
#define ADT7490_THERM_TIMER_STATUS               0x79 /**< THERM Timer Status */                                                                                                                             
#define ADT7490_THERM_TIMER_LIMIT                0x7A /**< THERM Timer Limit */                                                                                                                              
#define ADT7490_TACH_PULSES_PER_REVOL            0x7B /**< TACH Pulses per Revolution */                                                                                                                             
#define ADT7490_MFG_TEST_1                       0x7E /**< Manufacturer's Test register 1 */                                                                                                                              
#define ADT7490_MFG_TEST_2                       0x7F /**< Manufacturer's Test register 2 */                                                                                                                              
#define ADT7490_GPIO_CONFIG                      0x80 /**< GPIO Configuration */                                                                                                                             
#define ADT7490_VTT_LIMIT_LOW                    0x84 /**< VTT Low Limit */                                                                                                                             
#define ADT7490_IMON_LIMIT_LOW                   0x85 /**< IMON Low Limit */                                                                                                                              
#define ADT7490_VTT_LIMIT_HIGH                   0x86 /**< VTT High Limit */                                                                                                                              
#define ADT7490_IMON_LIMIT_HIGH                  0x87 /**< IMON High Limit */                                                                                                                              
#define ADT7490_OPER_POINT_PECI                  0x8A /**< PECI Operating Point register */                                                                                                                             
#define ADT7490_OPER_POINT_REM1                  0x8B /**< Remote 1 Operating Point register */                                                                                                                              
#define ADT7490_OPER_POINT_LOCAL                 0x8C /**< Local Temperature Operating Point 
                                                        *  register 
                                                        */                                                                                                                             
#define ADT7490_OPER_POINT_REM2                  0x8D /**< Remote 2 Operating Point register */                                                                                                                             
#define ADT7490_DYNAMIC_T_MIN_CTRL_1             0x8E /**< Dynamic Tmin Control 1 */                                                                                                                             
#define ADT7490_DYNAMIC_T_MIN_CTRL_2             0x8F /**< Dynamic Tmin Control 2 */                                                                                                                              
#define ADT7490_DYNAMIC_T_MIN_CTRL_3             0x90 /**< Dynamic Tmin Control 3 */                                                                                                                              
#define ADT7490_PECI0_TEMP_OFFSET                0x94 /**< PECI0 Temperature Offset */                                                                                                                              
#define ADT7490_PECI1_TEMP_OFFSET                0x95 /**< PECI1 Temperature Offset */                                                                                                                              
#define ADT7490_PECI2_TEMP_OFFSET                0x96 /**< PECI2 Temperature Offset */                                                                                                                              
#define ADT7490_PECI3_TEMP_OFFSET                0x97 /**< PECI3 Temperature Offset */                                                                                                                              

#define ADT7490_MAX_OFFSET ADT7490_PECI3_TEMP_OFFSET

#define ADT7490_INVALID_RPM(rpm) \
    ((rpm) == 0xffff) || ((rpm) == 0)

/* the adt7490 internal control structure */
typedef struct adt7490_ctx_t {
    void                  *devo;
    fan_brdspec_t         brdspec;
} adt7490_ctx_t;

static ccl_err_t    _ret;

/* adt7490 register read */
static ccl_err_t _adt7490_reg_read(void *p_priv, b_u32 offset, 
                                   b_u32 offset_sz, b_u32 idx, 
                                   b_u8 *p_value, b_u32 size)
{
    adt7490_ctx_t   *p_adt7490;
    i2c_route_t      i2c_route;

    if ( !p_priv || offset > ADT7490_MAX_OFFSET || 
         !p_value || idx ) {
        PDEBUG("Error! Invalid input parameters\n");
        ADT7490_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    PDEBUG("123$=> offset=0x%03x, size=%d\n", offset, size);

    p_adt7490 = (adt7490_ctx_t *)p_priv;     
    memcpy(&i2c_route, &p_adt7490->brdspec.i2c.bus_id, sizeof(i2c_route_t));

    _ret = devi2ccore_read_buff(&i2c_route, 
                                 p_adt7490->brdspec.extra_mux_pin, 
                                 offset, offset_sz, 
                                 p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to write over spi bus\n");
        ADT7490_ERROR_RETURN(_ret);
    }

    return _ret;
}

/* adt7490 register write */
static ccl_err_t _adt7490_reg_write(void *p_priv, b_u32 offset, 
                                    b_u32 offset_sz, b_u32 idx, 
                                    b_u8 *p_value, b_u32 size)
{
    adt7490_ctx_t   *p_adt7490;
    i2c_route_t      i2c_route;

    if ( !p_priv || offset > ADT7490_MAX_OFFSET || 
         !p_value || idx ) {
        PDEBUG("Error! Invalid input parameters\n");
        ADT7490_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    PDEBUG("123$=> offset=0x%03x, value=0x%02x\n", offset, *p_value);

    p_adt7490 = (adt7490_ctx_t *)p_priv;     
    memcpy(&i2c_route, &p_adt7490->brdspec.i2c.bus_id, sizeof(i2c_route_t));

    _ret = devi2ccore_write_buff(&i2c_route, 
                                p_adt7490->brdspec.extra_mux_pin, 
                                offset, offset_sz, 
                                p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to write over spi bus\n");
        ADT7490_ERROR_RETURN(_ret);
    }

    return _ret;
}

static ccl_err_t _adt7490_read_measurement_data(void *priv, b_u8 *buf)
{
    adt7490_ctx_t   *p_adt7490;
    fanctl_stats_t     *fan_stats;

    if ( !priv || !buf ) {
        PDEBUG("Error! Invalid input parameters\n");
        ADT7490_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_adt7490 = (adt7490_ctx_t *)priv;     
    fan_stats = (fanctl_stats_t *)buf;

    return CCL_OK;
}

static ccl_err_t _adt7490_init(void *p_priv)
{
    adt7490_ctx_t   *p_adt7490;
    b_u8            regval;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        ADT7490_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_adt7490 = (adt7490_ctx_t *)p_priv;

    /* Enable monitoring */
    regval = 0x1;
    ADT7490_REG_WRITE(p_adt7490, ADT7490_CONFIG_1, 
                      sizeof(b_u8), 0, 
                      (b_u8 *)&regval, sizeof(b_u8));

//    return CCL_OK;
    /* Set automatic fan control base on remote 1
       temperature sensor */
    regval = 0x02;
    ADT7490_REG_WRITE(p_adt7490, ADT7490_CONFIG_PWM1, 
                      sizeof(b_u8), 0, 
                      (b_u8 *)&regval, sizeof(b_u8));
    ADT7490_REG_WRITE(p_adt7490, ADT7490_CONFIG_PWM2, 
                      sizeof(b_u8), 0, 
                      (b_u8 *)&regval, sizeof(b_u8));
    ADT7490_REG_WRITE(p_adt7490, ADT7490_CONFIG_PWM3, 
                      sizeof(b_u8), 0, 
                      (b_u8 *)&regval, sizeof(b_u8));
    /* Set minimum temperature at which the fans
       start to turn on under automatic fan control */
    regval = 0x20;
    ADT7490_REG_WRITE(p_adt7490, ADT7490_TEMP_MIN_REM_1, 
                      sizeof(b_u8), 0, 
                      (b_u8 *)&regval, sizeof(b_u8));
    ADT7490_REG_WRITE(p_adt7490, ADT7490_TEMP_MIN_REM_2, 
                      sizeof(b_u8), 0, 
                      (b_u8 *)&regval, sizeof(b_u8));
    ADT7490_REG_WRITE(p_adt7490, ADT7490_TEMP_MIN_LOCAL, 
                      sizeof(b_u8), 0, 
                      (b_u8 *)&regval, sizeof(b_u8));
    /* Set Trange to 53 degrees, high frequency PWM mode */
    regval = 0xe8;
    ADT7490_REG_WRITE(p_adt7490, ADT7490_TEMP_REM1_T_RANGE, 
                      sizeof(b_u8), 0, 
                      (b_u8 *)&regval, sizeof(b_u8));
    ADT7490_REG_WRITE(p_adt7490, ADT7490_TEMP_REM2_T_RANGE, 
                      sizeof(b_u8), 0, 
                      (b_u8 *)&regval, sizeof(b_u8));
    ADT7490_REG_WRITE(p_adt7490, ADT7490_TEMP_LOCAL_T_RANGE, 
                      sizeof(b_u8), 0, 
                      (b_u8 *)&regval, sizeof(b_u8));
    /* Set minimum duty cycle to 12.5% */
    regval = 0x20;
    ADT7490_REG_WRITE(p_adt7490, ADT7490_DUTY_CYCLE_MIN_PWM1, 
                      sizeof(b_u8), 0, 
                      (b_u8 *)&regval, sizeof(b_u8));
    ADT7490_REG_WRITE(p_adt7490, ADT7490_DUTY_CYCLE_MIN_PWM2, 
                      sizeof(b_u8), 0, 
                      (b_u8 *)&regval, sizeof(b_u8));
    ADT7490_REG_WRITE(p_adt7490, ADT7490_DUTY_CYCLE_MIN_PWM3, 
                      sizeof(b_u8), 0, 
                      (b_u8 *)&regval, sizeof(b_u8));  
    return CCL_OK;
}

static ccl_err_t _adt7490_configure_test(void *p_priv)
{
    adt7490_ctx_t   *p_adt7490;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        ADT7490_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_adt7490 = (adt7490_ctx_t *)p_priv;

    return CCL_OK;
}

static ccl_err_t _adt7490_run_test(void *p_priv, 
                                   __attribute__((unused)) b_u8 *buf, 
                                   __attribute__((unused)) b_u32 size)
{
    adt7490_ctx_t   *p_adt7490;
    b_u8            regval, regval1, regval2;
    fanctl_stats_t  *stats;
    b_u32           temp1, temp2, temp3;
    b_u32           rpm;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        ADT7490_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_adt7490 = (adt7490_ctx_t *)p_priv;

    if (size == sizeof(fanctl_stats_t)) {
        stats = (fanctl_stats_t *)buf;
        memset(&stats->temp_local, 0, sizeof(*stats)-DEVMAN_ATTRS_NAME_LEN);
    }

    _ret = _adt7490_reg_read(p_priv, ADT7490_TEMP_READ_REM_1, 
                             sizeof(b_u8), 0, 
                             &regval, sizeof(b_u8));
    _ret |= _adt7490_reg_read(p_priv, ADT7490_TEMP_READ_REM_2, 
                             sizeof(b_u8), 0, 
                             &regval1, sizeof(b_u8));
    _ret |= _adt7490_reg_read(p_priv, ADT7490_TEMP_READ_LOCAL, 
                             sizeof(b_u8), 0, 
                             &regval2, sizeof(b_u8));
    if ( _ret == CCL_OK ) { 
        temp1 = regval;
        temp2 = regval1;
        temp3 = regval2;
        if (size == sizeof(fanctl_stats_t)) {
            stats->temp_remote1 = temp1;
            stats->temp_remote2 = temp2;
            stats->temp_local = temp3;
        }
    }
    _ret |= _adt7490_reg_read(p_priv, ADT7490_TACH1_READ_LOW, 
                             sizeof(b_u8), 0, 
                             &regval, sizeof(b_u8));
    _ret |= _adt7490_reg_read(p_priv, ADT7490_TACH1_READ_HIGH, 
                             sizeof(b_u8), 0, 
                             &regval1, sizeof(b_u8));
    if ( _ret == CCL_OK ) { 
        rpm = regval1 << 8 | regval;
        if (size == sizeof(fanctl_stats_t)) 
            stats->speed_rpm1 = ADT7490_INVALID_RPM(rpm) ? 0 : (90000 * 60)/rpm;
    }
    _ret |= _adt7490_reg_read(p_priv, ADT7490_TACH2_READ_LOW, 
                             sizeof(b_u8), 0, 
                             &regval, sizeof(b_u8));
    _ret |= _adt7490_reg_read(p_priv, ADT7490_TACH2_READ_HIGH, 
                             sizeof(b_u8), 0, 
                             &regval1, sizeof(b_u8));
    if ( _ret == CCL_OK ) { 
        rpm = regval1 << 8 | regval;
        if (size == sizeof(fanctl_stats_t)) 
            stats->speed_rpm2 = ADT7490_INVALID_RPM(rpm) ? 0 : (90000 * 60)/rpm;
    }
    _ret |= _adt7490_reg_read(p_priv, ADT7490_TACH3_READ_LOW, 
                             sizeof(b_u8), 0, 
                             &regval, sizeof(b_u8));
    _ret |= _adt7490_reg_read(p_priv, ADT7490_TACH3_READ_HIGH, 
                             sizeof(b_u8), 0, 
                             &regval1, sizeof(b_u8));
    if ( _ret == CCL_OK ) { 
        rpm = regval1 << 8 | regval;
        if (size == sizeof(fanctl_stats_t)) 
            stats->speed_rpm3 = ADT7490_INVALID_RPM(rpm) ? 0 : (90000 * 60)/rpm;
    }

    if ( _ret ) 
        ADT7490_ERROR_RETURN(_ret);

    PDEBUG("temp_remote1=%d, temp_remote2=%d, temp_local=%d\n",
           temp1, temp2, temp3);

    return CCL_OK;
}

ccl_err_t adt7490_attr_read(void *p_priv, b_u32 attrid, 
                            b_u32 offset, b_u32 offset_sz, 
                            b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || offset > ADT7490_MAX_OFFSET || 
         !p_value || idx ) {
        PDEBUG("Error! Invalid input parameters\n"
               "p_priv=%p, offset=%d, p_value=%p, idx=%d\n",
               p_priv, offset, p_value, idx);
        ADT7490_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch (attrid) {
    case eADT7490_CONFIG_1:                    
    case eADT7490_CONFIG_2:                    
    case eADT7490_CONFIG_3:                    
    case eADT7490_CONFIG_4:                    
    case eADT7490_CONFIG_5:                    
    case eADT7490_CONFIG_6:                    
    case eADT7490_CONFIG_7:                    
    case eADT7490_PECI0:                       
    case eADT7490_PECI1:                       
    case eADT7490_PECI2:                       
    case eADT7490_PECI3:                       
    case eADT7490_IMON:                        
    case eADT7490_VTT:                         
    case eADT7490_EXT_RES_3:                   
    case eADT7490_VOLT_READ_PIN22:             
    case eADT7490_VOLT_READ_PIN23:             
    case eADT7490_VOLT_READ_PIN4:              
    case eADT7490_VOLT_READ_PIN20:             
    case eADT7490_VOLT_READ_PIN21:             
    case eADT7490_TEMP_READ_REM_1:             
    case eADT7490_TEMP_READ_LOCAL:             
    case eADT7490_TEMP_READ_REM_2:             
    case eADT7490_TACH1_READ_LOW:              
    case eADT7490_TACH1_READ_HIGH:             
    case eADT7490_TACH2_READ_LOW:              
    case eADT7490_TACH2_READ_HIGH:             
    case eADT7490_TACH3_READ_LOW:              
    case eADT7490_TACH3_READ_HIGH:             
    case eADT7490_TACH4_READ_LOW:              
    case eADT7490_TACH4_READ_HIGH:             
    case eADT7490_DUTY_CYCLE_PWM1:             
    case eADT7490_DUTY_CYCLE_PWM2:             
    case eADT7490_DUTY_CYCLE_PWM3:             
    case eADT7490_PECI_LIMIT_LOW:              
    case eADT7490_PECI_LIMIT_HIGH:             
    case eADT7490_PECI_CONFIG_1:               
    case eADT7490_PECI_CONFIG_2:               
    case eADT7490_DUTY_CYCLE_MAX_PWM1:         
    case eADT7490_DUTY_CYCLE_MAX_PWM2:         
    case eADT7490_DUTY_CYCLE_MAX_PWM3:         
    case eADT7490_PECI_T_MIN:                  
    case eADT7490_PECI_T_RANGE_ENHCD:          
    case eADT7490_PECI_T_CTRL_LIMIT:           
    case eADT7490_VERSION:                     
    case eADT7490_INT_STATUS_1:                
    case eADT7490_INT_STATUS_2:                
    case eADT7490_INT_STATUS_3:                
    case eADT7490_INT_STATUS_4:                
    case eADT7490_VIN_2_5_LIMIT_LOW:           
    case eADT7490_VIN_2_5_LIMIT_HIGH:          
    case eADT7490_VCCP_LIMIT_LOW:              
    case eADT7490_VCCP_LIMIT_HIGH:             
    case eADT7490_VCC_LIMIT_LOW:               
    case eADT7490_VCC_LIMIT_HIGH:              
    case eADT7490_VIN_5_LIMIT_LOW:             
    case eADT7490_VIN_5_LIMIT_HIGH:            
    case eADT7490_VIN_12_LIMIT_LOW:            
    case eADT7490_VIN_12_LIMIT_HIGH:           
    case eADT7490_TEMP_REM_1_LIMIT_LOW:        
    case eADT7490_TEMP_REM_1_LIMIT_HIGH:       
    case eADT7490_TEMP_LOCAL_LIMIT_LOW:        
    case eADT7490_TEMP_LOCAL_LIMIT_HIGH:       
    case eADT7490_TEMP_REM_2_LIMIT_LOW:        
    case eADT7490_TEMP_REM_2_LIMIT_HIGH:       
    case eADT7490_TACH1_LIMIT_LOW_BYTE:        
    case eADT7490_TACH1_LIMIT_HIGH_BYTE:       
    case eADT7490_TACH2_LIMIT_LOW_BYTE:        
    case eADT7490_TACH2_LIMIT_HIGH_BYTE:       
    case eADT7490_TACH3_LIMIT_LOW_BYTE:        
    case eADT7490_TACH3_LIMIT_HIGH_BYTE:       
    case eADT7490_TACH4_LIMIT_LOW_BYTE:        
    case eADT7490_TACH4_LIMIT_HIGH_BYTE:       
    case eADT7490_CONFIG_PWM1:                 
    case eADT7490_CONFIG_PWM2:                 
    case eADT7490_CONFIG_PWM3:                 
    case eADT7490_TEMP_REM1_T_RANGE: 
    case eADT7490_TEMP_LOCAL_T_RANGE:
    case eADT7490_TEMP_REM2_T_RANGE: 
    case eADT7490_ENHANCED_ACOUSTICS1:         
    case eADT7490_ENHANCED_ACOUSTICS2:         
    case eADT7490_DUTY_CYCLE_MIN_PWM1:         
    case eADT7490_DUTY_CYCLE_MIN_PWM2:         
    case eADT7490_DUTY_CYCLE_MIN_PWM3:         
    case eADT7490_TEMP_MIN_REM_1:              
    case eADT7490_TEMP_MIN_LOCAL:              
    case eADT7490_TEMP_MIN_REM_2:              
    case eADT7490_TEMP_REM_1_THERM_LIMIT:      
    case eADT7490_TEMP_LOCAL_THERM_LIMIT:      
    case eADT7490_TEMP_REM_2_THERM_LIMIT:      
    case eADT7490_TEMP_REM_1_LOCAL_HIST:       
    case eADT7490_TEMP_REM_2_PECI_HIST:        
    case eADT7490_XNOR_TREE_TEST_EN:           
    case eADT7490_TEMP_REM_1_OFFSET:           
    case eADT7490_TEMP_LOCAL_OFFSET:           
    case eADT7490_TEMP_REM_2_OFFSET:           
    case eADT7490_INT_MASK_1:                  
    case eADT7490_INT_MASK_2:                  
    case eADT7490_INT_MASK_3:                  
    case eADT7490_INT_MASK_4:                  
    case eADT7490_EXTENDED_RESOLUTION_1:       
    case eADT7490_EXTENDED_RESOLUTION_2:       
    case eADT7490_THERM_TIMER_STATUS:          
    case eADT7490_THERM_TIMER_LIMIT:           
    case eADT7490_TACH_PULSES_PER_REVOL:       
    case eADT7490_MFG_TEST_1:                  
    case eADT7490_MFG_TEST_2:                  
    case eADT7490_GPIO_CONFIG:                 
    case eADT7490_VTT_LIMIT_LOW:               
    case eADT7490_IMON_LIMIT_LOW:              
    case eADT7490_VTT_LIMIT_HIGH:              
    case eADT7490_IMON_LIMIT_HIGH:             
    case eADT7490_OPER_POINT_PECI:             
    case eADT7490_OPER_POINT_REM1:             
    case eADT7490_OPER_POINT_LOCAL:            
    case eADT7490_OPER_POINT_REM2:             
    case eADT7490_DYNAMIC_T_MIN_CTRL_1:        
    case eADT7490_DYNAMIC_T_MIN_CTRL_2:        
    case eADT7490_DYNAMIC_T_MIN_CTRL_3:        
    case eADT7490_PECI0_TEMP_OFFSET:           
    case eADT7490_PECI1_TEMP_OFFSET:           
    case eADT7490_PECI2_TEMP_OFFSET:           
    case eADT7490_PECI3_TEMP_OFFSET:           
        _ret = _adt7490_reg_read(p_priv, offset, offset_sz, idx, p_value, size);
        break;
    case eADT7490_MEASUREMENT_DATA:
        _ret = _adt7490_read_measurement_data(p_priv, p_value);
        break;
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK) {
        PDEBUG("Failure in attribute id: %d\n", attrid);
    }

    return _ret;               
}

ccl_err_t adt7490_attr_write(void *p_priv, b_u32 attrid, 
                             b_u32 offset, b_u32 offset_sz, 
                             b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || offset > ADT7490_MAX_OFFSET || 
         !p_value || idx ) {
        PDEBUG("Error! Invalid input parameters\n"
               "p_priv=%p, offset=%d, p_value=%p, idx=%d\n",
               p_priv, offset, p_value, idx);
        ADT7490_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch (attrid) {
    case eADT7490_CONFIG_1:                    
    case eADT7490_CONFIG_2:                    
    case eADT7490_CONFIG_3:                    
    case eADT7490_CONFIG_4:                    
    case eADT7490_CONFIG_5:                    
    case eADT7490_CONFIG_6:                    
    case eADT7490_CONFIG_7:                    
    case eADT7490_PECI0:                       
    case eADT7490_PECI1:                       
    case eADT7490_PECI2:                       
    case eADT7490_PECI3:                       
    case eADT7490_IMON:                        
    case eADT7490_VTT:                         
    case eADT7490_EXT_RES_3:                   
    case eADT7490_VOLT_READ_PIN22:             
    case eADT7490_VOLT_READ_PIN23:             
    case eADT7490_VOLT_READ_PIN4:              
    case eADT7490_VOLT_READ_PIN20:             
    case eADT7490_VOLT_READ_PIN21:             
    case eADT7490_TEMP_READ_REM_1:             
    case eADT7490_TEMP_READ_LOCAL:             
    case eADT7490_TEMP_READ_REM_2:             
    case eADT7490_TACH1_READ_LOW:              
    case eADT7490_TACH1_READ_HIGH:             
    case eADT7490_TACH2_READ_LOW:              
    case eADT7490_TACH2_READ_HIGH:             
    case eADT7490_TACH3_READ_LOW:              
    case eADT7490_TACH3_READ_HIGH:             
    case eADT7490_TACH4_READ_LOW:              
    case eADT7490_TACH4_READ_HIGH:             
    case eADT7490_DUTY_CYCLE_PWM1:             
    case eADT7490_DUTY_CYCLE_PWM2:             
    case eADT7490_DUTY_CYCLE_PWM3:             
    case eADT7490_PECI_LIMIT_LOW:              
    case eADT7490_PECI_LIMIT_HIGH:             
    case eADT7490_PECI_CONFIG_1:               
    case eADT7490_PECI_CONFIG_2:               
    case eADT7490_DUTY_CYCLE_MAX_PWM1:         
    case eADT7490_DUTY_CYCLE_MAX_PWM2:         
    case eADT7490_DUTY_CYCLE_MAX_PWM3:         
    case eADT7490_PECI_T_MIN:                  
    case eADT7490_PECI_T_RANGE_ENHCD:          
    case eADT7490_PECI_T_CTRL_LIMIT:           
    case eADT7490_VERSION:                     
    case eADT7490_INT_STATUS_1:                
    case eADT7490_INT_STATUS_2:                
    case eADT7490_INT_STATUS_3:                
    case eADT7490_INT_STATUS_4:                
    case eADT7490_VIN_2_5_LIMIT_LOW:           
    case eADT7490_VIN_2_5_LIMIT_HIGH:          
    case eADT7490_VCCP_LIMIT_LOW:              
    case eADT7490_VCCP_LIMIT_HIGH:             
    case eADT7490_VCC_LIMIT_LOW:               
    case eADT7490_VCC_LIMIT_HIGH:              
    case eADT7490_VIN_5_LIMIT_LOW:             
    case eADT7490_VIN_5_LIMIT_HIGH:            
    case eADT7490_VIN_12_LIMIT_LOW:            
    case eADT7490_VIN_12_LIMIT_HIGH:           
    case eADT7490_TEMP_REM_1_LIMIT_LOW:        
    case eADT7490_TEMP_REM_1_LIMIT_HIGH:       
    case eADT7490_TEMP_LOCAL_LIMIT_LOW:        
    case eADT7490_TEMP_LOCAL_LIMIT_HIGH:       
    case eADT7490_TEMP_REM_2_LIMIT_LOW:        
    case eADT7490_TEMP_REM_2_LIMIT_HIGH:       
    case eADT7490_TACH1_LIMIT_LOW_BYTE:        
    case eADT7490_TACH1_LIMIT_HIGH_BYTE:       
    case eADT7490_TACH2_LIMIT_LOW_BYTE:        
    case eADT7490_TACH2_LIMIT_HIGH_BYTE:       
    case eADT7490_TACH3_LIMIT_LOW_BYTE:        
    case eADT7490_TACH3_LIMIT_HIGH_BYTE:       
    case eADT7490_TACH4_LIMIT_LOW_BYTE:        
    case eADT7490_TACH4_LIMIT_HIGH_BYTE:       
    case eADT7490_CONFIG_PWM1:                 
    case eADT7490_CONFIG_PWM2:                 
    case eADT7490_CONFIG_PWM3:                 
    case eADT7490_TEMP_REM1_T_RANGE: 
    case eADT7490_TEMP_LOCAL_T_RANGE:
    case eADT7490_TEMP_REM2_T_RANGE: 
    case eADT7490_ENHANCED_ACOUSTICS1:         
    case eADT7490_ENHANCED_ACOUSTICS2:         
    case eADT7490_DUTY_CYCLE_MIN_PWM1:         
    case eADT7490_DUTY_CYCLE_MIN_PWM2:         
    case eADT7490_DUTY_CYCLE_MIN_PWM3:         
    case eADT7490_TEMP_MIN_REM_1:              
    case eADT7490_TEMP_MIN_LOCAL:              
    case eADT7490_TEMP_MIN_REM_2:              
    case eADT7490_TEMP_REM_1_THERM_LIMIT:      
    case eADT7490_TEMP_LOCAL_THERM_LIMIT:      
    case eADT7490_TEMP_REM_2_THERM_LIMIT:      
    case eADT7490_TEMP_REM_1_LOCAL_HIST:       
    case eADT7490_TEMP_REM_2_PECI_HIST:        
    case eADT7490_XNOR_TREE_TEST_EN:           
    case eADT7490_TEMP_REM_1_OFFSET:           
    case eADT7490_TEMP_LOCAL_OFFSET:           
    case eADT7490_TEMP_REM_2_OFFSET:           
    case eADT7490_INT_MASK_1:                  
    case eADT7490_INT_MASK_2:                  
    case eADT7490_INT_MASK_3:                  
    case eADT7490_INT_MASK_4:                  
    case eADT7490_EXTENDED_RESOLUTION_1:       
    case eADT7490_EXTENDED_RESOLUTION_2:       
    case eADT7490_THERM_TIMER_STATUS:          
    case eADT7490_THERM_TIMER_LIMIT:           
    case eADT7490_TACH_PULSES_PER_REVOL:       
    case eADT7490_MFG_TEST_1:                  
    case eADT7490_MFG_TEST_2:                  
    case eADT7490_GPIO_CONFIG:                 
    case eADT7490_VTT_LIMIT_LOW:               
    case eADT7490_IMON_LIMIT_LOW:              
    case eADT7490_VTT_LIMIT_HIGH:              
    case eADT7490_IMON_LIMIT_HIGH:             
    case eADT7490_OPER_POINT_PECI:             
    case eADT7490_OPER_POINT_REM1:             
    case eADT7490_OPER_POINT_LOCAL:            
    case eADT7490_OPER_POINT_REM2:             
    case eADT7490_DYNAMIC_T_MIN_CTRL_1:        
    case eADT7490_DYNAMIC_T_MIN_CTRL_2:        
    case eADT7490_DYNAMIC_T_MIN_CTRL_3:        
    case eADT7490_PECI0_TEMP_OFFSET:           
    case eADT7490_PECI1_TEMP_OFFSET:           
    case eADT7490_PECI2_TEMP_OFFSET:           
    case eADT7490_PECI3_TEMP_OFFSET:           
        _ret = _adt7490_reg_write(p_priv, offset, offset_sz, idx, p_value, size);
        break;
    case eADT7490_INIT:
        _ret = _adt7490_init(p_priv);
        break;
    case eADT7490_CONFIGURE_TEST:
        _ret = _adt7490_configure_test(p_priv);
        break;
    case eADT7490_RUN_TEST:
        _ret = _adt7490_run_test(p_priv, p_value, size);
        break;
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK) {
        PDEBUG("Failure in attribute id: %d\n", attrid);
    }

    return _ret;               
}

/* adt7490 attributes definition */
static device_attr_t _attrs[] = {
    {
        .name = "i2c_bus", 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset = offsetof(struct adt7490_ctx_t, brdspec.i2c.bus_id),
        .format = "%d"
    },
    {
        .name = "i2c_addr",
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset = offsetof(struct adt7490_ctx_t, brdspec.i2c.dev_i2ca),
        .format = "0x%03x"
    },
    {
        .name = "name", 
        .type = eDEVMAN_ATTR_STRING, .maxnum = 1, 
        .offset = offsetof(struct adt7490_ctx_t, brdspec.name),
        .size = DEVMAN_DEVNAME_LEN,
        .format = "%s"
    },
    {
        .name = "config_1", .id = eADT7490_CONFIG_1, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_CONFIG_1
    },
    {
        .name = "config_2", .id = eADT7490_CONFIG_2, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_CONFIG_2
    },
    {
        .name = "config_3", .id = eADT7490_CONFIG_3, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_CONFIG_3
    },
    {
        .name = "config_4", .id = eADT7490_CONFIG_4, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_CONFIG_4
    },
    {
        .name = "config_5", .id = eADT7490_CONFIG_5, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_CONFIG_5
    },
    {
        .name = "config_6", .id = eADT7490_CONFIG_6, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_CONFIG_6
    },
    {
        .name = "config_7", .id = eADT7490_CONFIG_7, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_CONFIG_7
    },
    {
        .name = "peci_0", .id = eADT7490_PECI0, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_PECI0
    },
    {
        .name = "peci_1", .id = eADT7490_PECI1, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_PECI1
    },
    {
        .name = "peci_2", .id = eADT7490_PECI2, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_PECI2
    },
    {
        .name = "peci_3", .id = eADT7490_PECI3, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_PECI3
    },
    {
        .name = "imon", .id = eADT7490_IMON, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_IMON
    },
    {
        .name = "vtt", .id = eADT7490_VTT, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_VTT
    },
    {
        .name = "ext_res_3", .id = eADT7490_EXT_RES_3, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_EXT_RES_3
    },
    {
        .name = "volt_read_pin22", .id = eADT7490_VOLT_READ_PIN22, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_VOLT_READ_PIN22
    },
    {
        .name = "volt_read_pin23", .id = eADT7490_VOLT_READ_PIN23, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_VOLT_READ_PIN23
    },
    {
        .name = "volt_read_pin4", .id = eADT7490_VOLT_READ_PIN4, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_VOLT_READ_PIN4
    },
    {
        .name = "volt_read_pin20", .id = eADT7490_VOLT_READ_PIN20, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_VOLT_READ_PIN20
    },
    {
        .name = "volt_read_pin21", .id = eADT7490_VOLT_READ_PIN21, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_VOLT_READ_PIN21
    },
    {
        .name = "temp_read_rem1", .id = eADT7490_TEMP_READ_REM_1, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_TEMP_READ_REM_1
    },
    {
        .name = "temp_read_local", .id = eADT7490_TEMP_READ_LOCAL, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_TEMP_READ_LOCAL
    },
    {
        .name = "temp_read_rem2", .id = eADT7490_TEMP_READ_REM_2, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_TEMP_READ_REM_2
    },
    {
        .name = "tach1_read_low", .id = eADT7490_TACH1_READ_LOW, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_TACH1_READ_LOW
    },
    {
        .name = "tach1_read_high", .id = eADT7490_TACH1_READ_HIGH, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_TACH1_READ_HIGH
    },
    {
        .name = "tach2_read_low", .id = eADT7490_TACH2_READ_LOW, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_TACH2_READ_LOW
    },
    {
        .name = "tach2_read_high", .id = eADT7490_TACH2_READ_HIGH, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_TACH2_READ_HIGH
    },
    {
        .name = "tach3_read_low", .id = eADT7490_TACH3_READ_LOW, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_TACH3_READ_LOW
    },
    {
        .name = "tach3_read_high", .id = eADT7490_TACH3_READ_HIGH, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_TACH3_READ_HIGH
    },
    {
        .name = "tach4_read_low", .id = eADT7490_TACH4_READ_LOW, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_TACH4_READ_LOW
    },
    {
        .name = "tach4_read_high", .id = eADT7490_TACH4_READ_HIGH, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_TACH4_READ_HIGH
    },
    {
        .name = "duty_cycle_pwm1", .id = eADT7490_DUTY_CYCLE_PWM1, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_DUTY_CYCLE_PWM1
    },
    {
        .name = "duty_cycle_pwm2", .id = eADT7490_DUTY_CYCLE_PWM2, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_DUTY_CYCLE_PWM2
    },
    {
        .name = "duty_cycle_pwm3", .id = eADT7490_DUTY_CYCLE_PWM3, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_DUTY_CYCLE_PWM3
    },
    {
        .name = "peci_limit_low", .id = eADT7490_PECI_LIMIT_LOW, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_PECI_LIMIT_LOW
    },
    {
        .name = "peci_limit_high", .id = eADT7490_PECI_LIMIT_HIGH, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_PECI_LIMIT_HIGH
    },
    {
        .name = "peci_config_1", .id = eADT7490_PECI_CONFIG_1, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_PECI_CONFIG_1
    },
    {
        .name = "peci_config_2", .id = eADT7490_PECI_CONFIG_2, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_PECI_CONFIG_2
    },
    {
        .name = "duty_cycle_max_pwm1", .id = eADT7490_DUTY_CYCLE_MAX_PWM1, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_DUTY_CYCLE_MAX_PWM1
    },
    {
        .name = "duty_cycle_max_pwm2", .id = eADT7490_DUTY_CYCLE_MAX_PWM2, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_DUTY_CYCLE_MAX_PWM2
    },
    {
        .name = "duty_cycle_max_pwm3", .id = eADT7490_DUTY_CYCLE_MAX_PWM3, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_DUTY_CYCLE_MAX_PWM3
    },
    {
        .name = "peci_t_min", .id = eADT7490_PECI_T_MIN, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_PECI_T_MIN
    },
    {
        .name = "peci_t_range_enhcd", .id = eADT7490_PECI_T_RANGE_ENHCD, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_PECI_T_RANGE_ENHCD
    },
    {
        .name = "peci_t_ctrl_limit", .id = eADT7490_PECI_T_CTRL_LIMIT, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_PECI_T_CTRL_LIMIT
    },
    {
        .name = "version", .id = eADT7490_VERSION, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_VERSION
    },
    {
        .name = "int_status_1", .id = eADT7490_INT_STATUS_1, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_INT_STATUS_1
    },
    {
        .name = "int_status_2", .id = eADT7490_INT_STATUS_2, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_INT_STATUS_2
    },
    {
        .name = "int_status_3", .id = eADT7490_INT_STATUS_3, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_INT_STATUS_3
    },
    {
        .name = "int_status_4", .id = eADT7490_INT_STATUS_4, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_INT_STATUS_4
    },
    {
        .name = "vin_2_5_limit_low", .id = eADT7490_VIN_2_5_LIMIT_LOW, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_VIN_2_5_LIMIT_LOW
    },
    {
        .name = "vin_2_5_limit_high", .id = eADT7490_VIN_2_5_LIMIT_HIGH, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_VIN_2_5_LIMIT_HIGH
    },
    {
        .name = "vccp_limit_low", .id = eADT7490_VCCP_LIMIT_LOW, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_VCCP_LIMIT_LOW
    },
    {
        .name = "vccp_limit_high", .id = eADT7490_VCCP_LIMIT_HIGH, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_VCCP_LIMIT_HIGH
    },
    {
        .name = "vcc_limit_low", .id = eADT7490_VCC_LIMIT_LOW, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_VCC_LIMIT_LOW
    },
    {
        .name = "vcc_limit_high", .id = eADT7490_VCC_LIMIT_HIGH, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_VCC_LIMIT_HIGH
    },
    {
        .name = "vin_5_limit_low", .id = eADT7490_VIN_5_LIMIT_LOW, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_VIN_5_LIMIT_LOW
    },
    {
        .name = "vin_5_limit_high", .id = eADT7490_VIN_5_LIMIT_HIGH, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_VIN_5_LIMIT_HIGH
    },
    {
        .name = "vin_12_limit_low", .id = eADT7490_VIN_12_LIMIT_LOW, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_VIN_12_LIMIT_LOW
    },
    {
        .name = "vin_12_limit_high", .id = eADT7490_VIN_12_LIMIT_HIGH, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_VIN_12_LIMIT_HIGH
    },
    {
        .name = "temp_rem1_limit_low", .id = eADT7490_TEMP_REM_1_LIMIT_LOW, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_TEMP_REM_1_LIMIT_LOW
    },
    {
        .name = "temp_rem1_limit_high", .id = eADT7490_TEMP_REM_1_LIMIT_HIGH, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_TEMP_REM_1_LIMIT_HIGH
    },
    {
        .name = "temp_local_limit_low", .id = eADT7490_TEMP_LOCAL_LIMIT_LOW, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_TEMP_LOCAL_LIMIT_LOW
    },
    {
        .name = "temp_local_limit_high", .id = eADT7490_TEMP_LOCAL_LIMIT_HIGH, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_TEMP_LOCAL_LIMIT_HIGH
    },
    {
        .name = "temp_rem2_limit_low", .id = eADT7490_TEMP_REM_2_LIMIT_LOW, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_TEMP_REM_2_LIMIT_LOW
    },
    {
        .name = "temp_rem2_limit_high", .id = eADT7490_TEMP_REM_2_LIMIT_HIGH, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_TEMP_REM_2_LIMIT_HIGH
    },
    {
        .name = "tach1_limit_low_byte", .id = eADT7490_TACH1_LIMIT_LOW_BYTE, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_TACH1_LIMIT_LOW_BYTE
    },
    {
        .name = "tach1_limit_high_byte", .id = eADT7490_TACH1_LIMIT_HIGH_BYTE, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_TACH1_LIMIT_HIGH_BYTE
    },
    {
        .name = "tach2_limit_low_byte", .id = eADT7490_TACH2_LIMIT_LOW_BYTE, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_TACH2_LIMIT_LOW_BYTE
    },
    {
        .name = "tach2_limit_high_byte", .id = eADT7490_TACH2_LIMIT_HIGH_BYTE, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_TACH2_LIMIT_HIGH_BYTE
    },
    {
        .name = "tach3_limit_low_byte", .id = eADT7490_TACH3_LIMIT_LOW_BYTE, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_TACH3_LIMIT_LOW_BYTE
    },
    {
        .name = "tach3_limit_high_byte", .id = eADT7490_TACH3_LIMIT_HIGH_BYTE, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_TACH3_LIMIT_HIGH_BYTE
    },
    {
        .name = "tach4_limit_low_byte", .id = eADT7490_TACH4_LIMIT_LOW_BYTE, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_TACH4_LIMIT_LOW_BYTE
    },
    {
        .name = "tach4_limit_high_byte", .id = eADT7490_TACH4_LIMIT_HIGH_BYTE, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_TACH4_LIMIT_HIGH_BYTE
    },
    {
        .name = "config_pwm1", .id = eADT7490_CONFIG_PWM1, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_CONFIG_PWM1
    },
    {
        .name = "config_pwm2", .id = eADT7490_CONFIG_PWM2, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_CONFIG_PWM2
    },
    {
        .name = "config_pwm3", .id = eADT7490_CONFIG_PWM3, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_CONFIG_PWM3
    },
    {
        .name = "temp_rem1_t_range", .id = eADT7490_TEMP_REM1_T_RANGE, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_TEMP_REM1_T_RANGE
    },
    {
        .name = "temp_local_t_range", .id = eADT7490_TEMP_LOCAL_T_RANGE, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_TEMP_LOCAL_T_RANGE
    },
    {
        .name = "temp_rem2_t_range", .id = eADT7490_TEMP_REM2_T_RANGE, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_TEMP_REM2_T_RANGE
    },
    {
        .name = "enhanced_acoustics_1", .id = eADT7490_ENHANCED_ACOUSTICS1, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_ENHANCED_ACOUSTICS1
    },
    {
        .name = "enhanced_acoustics_2", .id = eADT7490_ENHANCED_ACOUSTICS2, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_ENHANCED_ACOUSTICS2
    },
    {
        .name = "duty_cycle_min_pwm1", .id = eADT7490_DUTY_CYCLE_MIN_PWM1, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_DUTY_CYCLE_MIN_PWM1
    },
    {
        .name = "duty_cycle_min_pwm2", .id = eADT7490_DUTY_CYCLE_MIN_PWM2, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_DUTY_CYCLE_MIN_PWM2
    },
    {
        .name = "duty_cycle_min_pwm3", .id = eADT7490_DUTY_CYCLE_MIN_PWM3, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_DUTY_CYCLE_MIN_PWM3
    },
    {
        .name = "temp_min_rem_1", .id = eADT7490_TEMP_MIN_REM_1, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_TEMP_MIN_REM_1
    },
    {
        .name = "temp_min_local", .id = eADT7490_TEMP_MIN_LOCAL, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_TEMP_MIN_LOCAL
    },
    {
        .name = "temp_min_rem_2", .id = eADT7490_TEMP_MIN_REM_2, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_TEMP_MIN_REM_2
    },
    {
        .name = "temp_rem_1_therm_limit", .id = eADT7490_TEMP_REM_1_THERM_LIMIT, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_TEMP_REM_1_THERM_LIMIT
    },
    {
        .name = "temp_local_therm_limit", .id = eADT7490_TEMP_LOCAL_THERM_LIMIT, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_TEMP_LOCAL_THERM_LIMIT
    },
    {
        .name = "temp_rem_2_therm_limit", .id = eADT7490_TEMP_REM_2_THERM_LIMIT, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_TEMP_REM_2_THERM_LIMIT
    },
    {
        .name = "temp_rem_1_local_hist", .id = eADT7490_TEMP_REM_1_LOCAL_HIST, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_TEMP_REM_1_LOCAL_HIST
    },
    {
        .name = "temp_rem_2_peci_hist", .id = eADT7490_TEMP_REM_2_PECI_HIST, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_TEMP_REM_2_PECI_HIST
    },
    {
        .name = "xnor_tree_test_en", .id = eADT7490_XNOR_TREE_TEST_EN, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_XNOR_TREE_TEST_EN
    },
    {
        .name = "temp_rem_1_offset", .id = eADT7490_TEMP_REM_1_OFFSET, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_TEMP_REM_1_OFFSET
    },
    {
        .name = "temp_local_offset", .id = eADT7490_TEMP_LOCAL_OFFSET, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_TEMP_LOCAL_OFFSET
    },
    {
        .name = "temp_rem_2_offset", .id = eADT7490_TEMP_REM_2_OFFSET, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_TEMP_REM_2_OFFSET
    },
    {
        .name = "int_mask_1", .id = eADT7490_INT_MASK_1, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_INT_MASK_1
    },
    {
        .name = "int_mask_2", .id = eADT7490_INT_MASK_2, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_INT_MASK_2
    },
    {
        .name = "int_mask_3", .id = eADT7490_INT_MASK_3, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_INT_MASK_3
    },
    {
        .name = "int_mask_4", .id = eADT7490_INT_MASK_4, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_INT_MASK_4
    },
    {
        .name = "ext_resolution_1", .id = eADT7490_EXTENDED_RESOLUTION_1, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_EXTENDED_RESOLUTION_1
    },
    {
        .name = "ext_resolution_2", .id = eADT7490_EXTENDED_RESOLUTION_2, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_EXTENDED_RESOLUTION_2
    },
    {
        .name = "therm_timer_status", .id = eADT7490_THERM_TIMER_STATUS, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_THERM_TIMER_STATUS
    },
    {
        .name = "therm_timer_limit", .id = eADT7490_THERM_TIMER_LIMIT, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_THERM_TIMER_LIMIT
    },
    {
        .name = "tach_pulses_per_revol", .id = eADT7490_TACH_PULSES_PER_REVOL, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_TACH_PULSES_PER_REVOL
    },
    {
        .name = "mfg_test_1", .id = eADT7490_MFG_TEST_1, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_MFG_TEST_1
    },
    {
        .name = "mfg_test_2", .id = eADT7490_MFG_TEST_2, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_MFG_TEST_2
    },
    {
        .name = "gpio_config", .id = eADT7490_GPIO_CONFIG, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_GPIO_CONFIG
    },
    {
        .name = "vtt_limit_low", .id = eADT7490_VTT_LIMIT_LOW, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_VTT_LIMIT_LOW
    },
    {
        .name = "imon_limit_low", .id = eADT7490_IMON_LIMIT_LOW, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_IMON_LIMIT_LOW
    },
    {
        .name = "vtt_limit_high", .id = eADT7490_VTT_LIMIT_HIGH, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_VTT_LIMIT_HIGH
    },
    {
        .name = "imon_limit_high", .id = eADT7490_IMON_LIMIT_HIGH, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_IMON_LIMIT_HIGH
    },
    {
        .name = "oper_point_peci", .id = eADT7490_OPER_POINT_PECI, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_OPER_POINT_PECI
    },
    {
        .name = "oper_point_rem1", .id = eADT7490_OPER_POINT_REM1, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_OPER_POINT_REM1
    },
    {
        .name = "oper_point_local", .id = eADT7490_OPER_POINT_LOCAL, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_OPER_POINT_LOCAL
    },
    {
        .name = "oper_point_rem2", .id = eADT7490_OPER_POINT_REM2, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_OPER_POINT_REM2
    },
    {
        .name = "dynamic_t_min_ctrl_1", .id = eADT7490_DYNAMIC_T_MIN_CTRL_1, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_DYNAMIC_T_MIN_CTRL_1
    },
    {
        .name = "dynamic_t_min_ctrl_2", .id = eADT7490_DYNAMIC_T_MIN_CTRL_2, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_DYNAMIC_T_MIN_CTRL_2
    },
    {
        .name = "dynamic_t_min_ctrl_3", .id = eADT7490_DYNAMIC_T_MIN_CTRL_3, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_DYNAMIC_T_MIN_CTRL_3
    },
    {
        .name = "peci0_temp_offset", .id = eADT7490_PECI0_TEMP_OFFSET, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_PECI0_TEMP_OFFSET
    },
    {
        .name = "peci1_temp_offset", .id = eADT7490_PECI1_TEMP_OFFSET, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_PECI1_TEMP_OFFSET
    },
    {
        .name = "peci2_temp_offset", .id = eADT7490_PECI2_TEMP_OFFSET, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_PECI2_TEMP_OFFSET
    },
    {
        .name = "peci3_temp_offset", .id = eADT7490_PECI3_TEMP_OFFSET, 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset_sz = sizeof(b_u8), .offset = ADT7490_PECI3_TEMP_OFFSET
    },
    {
        .name = "measurement_data", .id = eADT7490_MEASUREMENT_DATA, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = sizeof(fanctl_stats_t), 
        .offset_sz = sizeof(b_u8),
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "init", .id = eADT7490_INIT, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "config_test", .id = eADT7490_CONFIGURE_TEST, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "run_test", .id = eADT7490_RUN_TEST, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, 
        .size = sizeof(board_dev_stats_t), 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
};

/* Device instance hw-specific initializations 
 */
static ccl_err_t _fadt7490_add(void *devo, void *brdspec)
{
    adt7490_ctx_t           *p_adt7490;
    fan_brdspec_t           *p_brdspec;

    if ( !devo || !brdspec ) {
        PDEBUG("Error! Invalid input parameters\n");
        ADT7490_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_adt7490 = devman_device_private(devo);
    if ( !p_adt7490 ) {
        PDEBUG("NULL context area\n");
        ADT7490_ERROR_RETURN(CCL_FAIL);
    }
    p_brdspec = (gen_i2c_dev_brdspec_t *)brdspec;

    PDEBUG("123$=> p_brdspec->bus_id=%d, dev_i2ca=0x%x\n", 
           p_brdspec->i2c.bus_id, p_brdspec->i2c.dev_i2ca);

    p_adt7490->devo = devo;    
    memcpy(&p_adt7490->brdspec, p_brdspec, sizeof(fan_brdspec_t));

    return CCL_OK;
}


/* Device instance hw-specific deinitializations 
 */
static ccl_err_t _fadt7490_cut(void *devo)
{
    adt7490_ctx_t  *p_adt7490;
    if ( !devo ) {
        PDEBUG("Error! Invalid input parameters\n");
        ADT7490_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_adt7490 = devman_device_private(devo);
    if ( !p_adt7490 ) {
        PDEBUG("NULL context area\n");
        ADT7490_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

/* device type description structure */
static device_driver_t _adt7490_driver = {
    .name = "fan",
    .f_add = _fadt7490_add,
    .f_cut = _fadt7490_cut,
    .attrs_num = sizeof(_attrs)/sizeof(_attrs[0]),
                 .attrs = _attrs,
    .priv_size = sizeof(adt7490_ctx_t)
}; 

/* Initialize ADT7490 device type
 */
ccl_err_t devadt7490_init(void)
{
    /* register device type */
    _ret = devman_driver_register(&_adt7490_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to register driver\n");
        ADT7490_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

/* Deinitialize ADT7490 device type
 */
ccl_err_t devadt7490_fini(void)
{
    /* unregister device type */
    _ret = devman_driver_unregister(&_adt7490_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to unregister driver\n");
        ADT7490_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}


