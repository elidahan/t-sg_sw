/********************************************************************************/
/**
 * @file ina220.c
 * @brief Current Monitor INA220 device manager driver 
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <semaphore.h>
#include <linux/limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "i2ccore.h"       
#include "ina220.h"

#define INA220_DEBUG
/* printing/error-returning macros */
#ifdef INA220_DEBUG
    #define PDEBUG(fmt, args...) \
            ccl_syslog_err("INA220:  %s[%d]: " fmt, __FUNCTION__, __LINE__, ## args)
#else
    #define PDEBUG(fmt, args...)
#endif
#define INA220_ERROR_RETURN(err) do { \
   ccl_syslog_err("Error! %s[%d], error=%i\n", __FUNCTION__, __LINE__, (err)); \
   return err; \
} while (0)

/* ina220 registers offsets */
#define	INA220_CONFIG     0x00      /**< Configuration */
#define	INA220_SHUNTV     0x01      /**< Shunt voltage measurement data */
#define	INA220_BUSV       0x02      /**< Bus voltage measurement data */
#define	INA220_POWER      0x03      /**< Power measurement data */
#define	INA220_CURRENT    0x04      /**< Current flowing through the shunt register */
#define	INA220_CALIB      0x05      /**< System calibration */

#define INA220_MAX_OFFSET INA220_CALIB

#define	INA220_CONFIG_MODE_MASK    0x0007
#define	INA220_CONFIG_CADC_MASK    0x0078
#define	INA220_CONFIG_BADC_MASK    0x0780
#define	INA220_CONFIG_PG0_MASK     0x0800
#define	INA220_CONFIG_PG1_MASK     0x1000
#define	INA220_CONFIG_BRNG_MASK    0x2000
#define	INA220_CONFIG_RST_MASK     0x8000


/* the ina220 internal control structure */
typedef struct ina220_ctx_t {
    void                     *devo;
    currmon_brdspec_t        brdspec;
} ina220_ctx_t;

static ccl_err_t _ret;                     

/* ina220 register read */
static ccl_err_t _ina220_reg_read(void *p_priv, b_u32 offset, 
                                   b_u32 offset_sz, b_u32 idx, 
                                   b_u8 *p_value, b_u32 size)
{
    ina220_ctx_t     *p_ina220;
    i2c_route_t      i2c_route;

    if ( !p_priv || !p_value || idx || 
         !size || offset > INA220_MAX_OFFSET) {
        PDEBUG("Error! Invalid input parameters\n");
        INA220_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    PDEBUG("123$=> offset=0x%03x, size=%d\n", offset, size);

    p_ina220 = (ina220_ctx_t *)p_priv; 
          
    memcpy(&i2c_route, &p_ina220->brdspec.i2c.bus_id, sizeof(i2c_route_t));
    _ret = devi2ccore_read_buff(&i2c_route, 
                                p_ina220->brdspec.extra_mux_pin, 
                                offset, offset_sz, 
                                p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to read over i2c bus\n");
        INA220_ERROR_RETURN(_ret);
    }
    PDEBUG("offset=0x%02x, size=%d, value=0x%04x\n", offset, size, *(b_u16 *)p_value);
    /* According to INA220 DS bytes must be reordered */
    _ret = devman_byte_reorder(p_value, size);
    if ( _ret ) {
        PDEBUG("devman_byte_reorder error\n");
        INA220_ERROR_RETURN(_ret);
    }
    PDEBUG("offset=0x%02x, size=%d, value=0x%04x\n", offset, size, *(b_u16 *)p_value);

    return _ret;
}

/* ina220 register write */
static ccl_err_t _ina220_reg_write(void *p_priv, b_u32 offset, 
                                      b_u32 offset_sz, b_u32 idx, 
                                      b_u8 *p_value, b_u32 size)
{
    ina220_ctx_t     *p_ina220;
    i2c_route_t      i2c_route;

    if ( !p_priv || !p_value || idx || 
         !size || offset > INA220_MAX_OFFSET) {
        PDEBUG("Error! Invalid input parameters\n");
        INA220_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    PDEBUG("offset=0x%02x, size=%d, value=0x%04x\n", offset, size, *(b_u16 *)p_value);

    p_ina220 = (ina220_ctx_t *)p_priv;     
    memcpy(&i2c_route, &p_ina220->brdspec.i2c.bus_id, sizeof(i2c_route_t));

    /* According to INA220 DS bytes must be reordered */
    _ret = devman_byte_reorder(p_value, size);
    if ( _ret ) {
        PDEBUG("devman_byte_reorder error\n");
        INA220_ERROR_RETURN(_ret);
    }
    PDEBUG("offset=0x%02x, size=%d, value=0x%04x\n", offset, size, *(b_u16 *)p_value);

    _ret = devi2ccore_write_buff(&i2c_route, 
                                 p_ina220->brdspec.extra_mux_pin, 
                                 offset, offset_sz, 
                                 p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to write over i2c bus\n");
        INA220_ERROR_RETURN(_ret);
    }

    return _ret;
}

static ccl_err_t _ina220_read_measurement_data(void *priv, b_u8 *buf)
{
    ina220_ctx_t     *p_ina220;
    currmon_stats_t  *currmon_stats;

    if ( !priv || !buf ) {
        PDEBUG("Error! Invalid input parameters\n");
        INA220_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_ina220 = (ina220_ctx_t *)priv;
    currmon_stats = (currmon_stats_t *)buf;

    return CCL_OK;
}

static ccl_err_t _ina220_init(void *p_priv)
{
    ina220_ctx_t     *p_ina220;
    b_double64       current_lsb;
    b_u32            cal;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        INA220_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_ina220 = (ina220_ctx_t *)p_priv;

    cal = 0.04096 / (p_ina220->brdspec.current_lsb * p_ina220->brdspec.rshunt);

    PDEBUG("current_lsb: %lf, cal=0x%04x\n",
           current_lsb, cal);

    _ret = _ina220_reg_write(p_priv, INA220_CALIB, sizeof(b_u8), 
                             0, (b_u8 *)&cal, sizeof(b_u16));
    if ( _ret ) { 
        PDEBUG("_ina220_reg_write error\n");
        INA220_ERROR_RETURN(_ret);
    }


    return CCL_OK;
}

static ccl_err_t _ina220_configure_test(void *p_priv)
{
    ina220_ctx_t     *p_ina220;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        INA220_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_ina220 = (ina220_ctx_t *)p_priv;

    return CCL_OK;
}

static ccl_err_t _ina220_run_test(void *p_priv, 
                                  b_u8 *buf, 
                                  __attribute__((unused)) b_u32 size)
{
    ina220_ctx_t     *p_ina220;
    b_double64       current_amps;
    b_double64       busv;
    b_u16            regval;
    currmon_stats_t  *stats;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        INA220_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    if (size == sizeof(currmon_stats_t)) {
        stats = (currmon_stats_t *)buf;
        memset(&stats->bus_voltage, 0, sizeof(*stats)-DEVMAN_ATTRS_NAME_LEN);
    }

    p_ina220 = (ina220_ctx_t *)p_priv;
    _ret = _ina220_reg_read(p_priv, INA220_CURRENT, sizeof(b_u8), 
                             0, (b_u8 *)&regval, sizeof(b_u16));
    if ( _ret == CCL_OK ) { 
        current_amps = regval * p_ina220->brdspec.current_lsb;
        if (size == sizeof(currmon_stats_t))
            if (current_amps > 0 && current_amps < p_ina220->brdspec.current_max) {
                stats->current = current_amps;
            }
    }
    _ret |= _ina220_reg_read(p_priv, INA220_BUSV, sizeof(b_u8), 
                             0, (b_u8 *)&regval, sizeof(b_u16));
    if ( _ret == CCL_OK ) { 
        busv = (regval >> 3) * 0.004;
        if (size == sizeof(currmon_stats_t)) 
            stats->bus_voltage = busv;
    }

    if ( _ret ) 
        INA220_ERROR_RETURN(_ret);

    PDEBUG("addr=0x%02x, current_amps=%lf busv=%lf, current_lsb=%lf\n",
           p_ina220->brdspec.i2c.dev_i2ca, current_amps, busv, p_ina220->brdspec.current_lsb);

    return CCL_OK;
}

ccl_err_t ina220_attr_read(void *p_priv, b_u32 attrid, 
                             b_u32 offset, b_u32 offset_sz, 
                             b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || !p_value || idx || !size ) {
        PDEBUG("Error! Invalid input parameters\n");
        INA220_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch ( attrid ) {
    case eINA220_CONFIG:
    case eINA220_SHUNTV:
    case eINA220_BUSV:  
    case eINA220_POWER: 
    case eINA220_CURRENT:
    case eINA220_CALIB: 
        _ret = _ina220_reg_read(p_priv, offset, offset_sz, idx, p_value, size);
        break;
    case eINA220_MEASUREMENT_DATA:
        _ret = _ina220_read_measurement_data(p_priv, p_value);
        break;
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK) {
        PDEBUG("Failure in attribute id: %d\n", attrid);
    }

    return _ret;
}

ccl_err_t ina220_attr_write(void *p_priv, b_u32 attrid, 
                              b_u32 offset, b_u32 offset_sz, 
                              b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || !p_value || idx || !size ) {
        PDEBUG("Error! Invalid input parameters\n");
        INA220_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch ( attrid ) {
    case eINA220_CONFIG: 
    case eINA220_CALIB:  
        _ret = _ina220_reg_write(p_priv, offset, offset_sz, idx, p_value, size);
        break;
    case eINA220_INIT:
        _ret = _ina220_init(p_priv);
        break;
    case eINA220_CONFIGURE_TEST:
        _ret = _ina220_configure_test(p_priv);
        break;
    case eINA220_RUN_TEST:
        _ret = _ina220_run_test(p_priv, p_value, size);
        break;
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK) {
        PDEBUG("Failure in attribute id: %d\n", attrid);
    }

    return _ret;
}

/* ina220 attributes definition */
static device_attr_t _attrs[] = {
    {
        .name = "i2c_bus", 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset = offsetof(struct ina220_ctx_t, brdspec.i2c.bus_id),
        .format = "%d"
    },
    {
        .name = "i2c_addr", 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset = offsetof(struct ina220_ctx_t, brdspec.i2c.dev_i2ca),
        .format = "0x%03x"
    },
    {
        .name = "name", 
        .type = eDEVMAN_ATTR_STRING, .maxnum = 1, 
        .offset = offsetof(struct ina220_ctx_t, brdspec.name),
        .size = DEVMAN_DEVNAME_LEN,
        .format = "%s"
    },
    {
        .name = "mux_pin", 
        .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset = offsetof(struct ina220_ctx_t, brdspec.extra_mux_pin),
        .flags = eDEVMAN_ATTRFLAG_RO, .format = "%d"
    },  
    {
        .name = "config", .id = eINA220_CONFIG, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset = INA220_CONFIG, .offset_sz = sizeof(b_u8) 
    },
    {
        .name = "shuntv", .id = eINA220_SHUNTV, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset = INA220_SHUNTV, .offset_sz = sizeof(b_u8) 
    },
    {
        .name = "busv", .id = eINA220_BUSV, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset = INA220_BUSV, .offset_sz = sizeof(b_u8) 
    },
    {
        .name = "power", .id = eINA220_POWER, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset = INA220_POWER, .offset_sz = sizeof(b_u8) 
    },
    {
        .name = "curren", .id = eINA220_CURRENT, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset = INA220_CURRENT, .offset_sz = sizeof(b_u8) 
    },
    {
        .name = "calib", .id = eINA220_CALIB, 
        .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset = INA220_CALIB, .offset_sz = sizeof(b_u8) 
    },
    {
        .name = "measurement_data", .id = eINA220_MEASUREMENT_DATA, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = sizeof(currmon_stats_t), 
        .offset_sz = sizeof(b_u8),
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "init", .id = eINA220_INIT, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "config_test", .id = eINA220_CONFIGURE_TEST, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "run_test", .id = eINA220_RUN_TEST, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, 
        .size = sizeof(board_dev_stats_t), 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
};

/* Device instance hw-specific initializations 
 */
static ccl_err_t _fina220_add(void *devo, void *brdspec)
{
    ina220_ctx_t           *p_ina220;
    currmon_brdspec_t      *p_brdspec;

    if ( !devo || !brdspec ) {
        PDEBUG("Error! Invalid input parameters\n");
        INA220_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_ina220 = devman_device_private(devo);
    if ( !p_ina220 ) {
        PDEBUG("NULL context area\n");
        INA220_ERROR_RETURN(CCL_FAIL);
    }
    p_brdspec = (currmon_brdspec_t *)brdspec;

    PDEBUG("123$=> p_brdspec->bus_id=%d, dev_i2ca=0x%x\n", 
           p_brdspec->i2c.bus_id, p_brdspec->i2c.dev_i2ca);

    p_ina220->devo = devo;    
    memcpy(&p_ina220->brdspec, p_brdspec, sizeof(currmon_brdspec_t));

    return CCL_OK;
}


/* Device instance hw-specific deinitializations 
 */
static ccl_err_t _fina220_cut(void *devo)
{
    if ( !devo ) {
        PDEBUG("Error! Invalid input parameters\n");
        INA220_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    return CCL_OK;
}

/* device type description structure */
static device_driver_t _ina220_driver = {
    .name = "currmon",
    .f_add = _fina220_add,
    .f_cut = _fina220_cut,
    .attrs_num = sizeof(_attrs)/sizeof(_attrs[0]),
    .attrs = _attrs,
    .priv_size = sizeof(ina220_ctx_t)
}; 

/* Initialize INA220 device type
 */
ccl_err_t devina220_init(void)
{
    /* register device type */
    _ret = devman_driver_register(&_ina220_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to register driver\n");
        INA220_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

/* Deinitialize INA220 device type
 */
ccl_err_t devina220_fini(void)
{
    /* unregister device type */
    _ret = devman_driver_unregister(&_ina220_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to unregister driver\n");
        INA220_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

