/********************************************************************************/
/**
 * @file lm75a.c
 * @brief LM75 device manager driver 
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>

#include <ccl_types.h>
#include "devman.h"
#include "devboard.h"
#include "i2cbus.h"

//#define LM75A_DEBUG
/* printing/error-returning macros */
#ifdef LM75A_DEBUG
 #define PDEBUG(fmt, args...) bcl_log_err("LM75A: %s(): " fmt, __FUNCTION__, ## args)
#else
 #define PDEBUG(fmt, args...)
#endif
#define LM75A_ERROR_RETURN(err)                               \
{                                                           \
   PDEBUG("Error! line=%i, error=%i\n", __LINE__, (err));   \
   return err;                                              \
}

/* lm75a registers offsets */
#define LM75A_POINTER_REGISTER       0x00
#define LM75A_TEMPERATURE            0x01
#define LM75A_CONFIGURATION          0x02
#define LM75A_THYST                  0x03
#define LM75A_TOS                    0x04
#define LM75A_PRODUCT_ID             0x05

static ccl_err_t _rc;

/* the lm75a internal control structure */
typedef struct lm75a_ctx_t {
    void                    *devo;
    gen_i2c_dev_brdspec_t   brdspec;
    read_f                  f_read;
    write_f                 f_write;
} lm75a_ctx_t;

/* lm75a register write */
static ccl_err_t _flm75a_reg_write(void *p_priv, int offset, int idx, char *p_value, int size)
{
    lm75a_ctx_t   *p_lm75a;
    i2c_route_t      i2c_route;

    if ( !p_priv || !p_value || idx || size != sizeof(b_u8) ) {
        PDEBUG("Error! Invalid input parameters\n");
        LM75A_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    PDEBUG("123$=> offset=0x%03x, value=0x%02x\n", offset, *p_value);

    p_lm75a = (lm75a_ctx_t *)p_priv;     
    memcpy(&i2c_route, &p_lm75a->brdspec.i2c.bus_id, sizeof(i2c_route_t));

    p_lm75a->f_write(&i2c_route, offset, idx, p_value, size);

    return CCL_OK;
}

/* lm75a register read */
static ccl_err_t _flm75a_reg_read(void *p_priv, int offset, int idx, char *p_value, int size)
{
    lm75a_ctx_t   *p_lm75a;
    i2c_route_t      i2c_route;

    if ( !p_priv || !p_value || idx || size != sizeof(b_u8) ) {
        PDEBUG("Error! Invalid input parameters\n");
        LM75A_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    PDEBUGG("123$=> offset=0x%03x, size=%d\n", offset, size);

    p_lm75a = (lm75a_ctx_t *)p_priv;     
    memcpy(&i2c_route, &p_lm75a->brdspec.i2c.bus_id, sizeof(i2c_route_t));

    p_lm75a->f_read(&i2c_route, offset, idx, p_value, size);

    return CCL_OK;
}

/* lm75a attributes definition */
static device_attr_t _attrs[] = {
    {
        .name = "i2c_bus", .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, .offset = offsetof(struct lm75a_ctx_t, brdspec.bus_id),
        .format = "%d"
    },
    {
        .name = "i2c_addr", .type = eDEVMAN_ATTR_HALF, .maxnum = 1, .offset = offsetof(struct lm75a_ctx_t, brdspec.dev_i2ca),
        .format = "0x%03x"
    },
    {
        .name = "pointer_reg", .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, .offset = LM75A_POINTER_REGISTER,
        .f_read = _flm75a_reg_read, .f_write = _flm75a_reg_write
    },
    {
        .name = "temp", .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, .offset = LM75A_TEMPERATURE,
        .f_read = _flm75a_reg_read, .f_write = _flm75a_reg_write
    },
    {
        .name = "config", .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, .offset = LM75A_CONFIGURATION,
        .f_read = _flm75a_reg_read, .f_write = _flm75a_reg_write
    },
    {
        .name = "t_hyst", .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, .offset = LM75A_THYST,
        .f_read = _flm75a_reg_read, .f_write = _flm75a_reg_write
    },
    {
        .name = "t_os", .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, .offset = LM75A_TOS,
        .f_read = _flm75a_reg_read, .f_write = _flm75a_reg_write
    },
    {
        .name = "product_id", .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, .offset = LM75A_PRODUCT_ID,
        .f_read = _flm75a_reg_read, .f_write = _flm75a_reg_write
    },
};

/* Device instance hw-specific initializations 
 */
static ccl_err_t _flm75a_add(void *devo, void *brdspec)
{
    lm75a_ctx_t       *p_lm75a;
    gen_i2c_dev_brdspec_t *p_brdspec;
    b_u32             f_read_val, f_write_val;

    if ( !devo || !brdspec ) {
        PDEBUG("Error! Invalid input parameters\n");
        LM75A_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_lm75a = devman_device_private(devo);
    p_brdspec = (lm75a_brdspec_t *)brdspec;

    PDEBUG("123$=> p_brdspec->bus_id=%d, dev_i2ca=0x%x\n", p_brdspec->bus_id, p_brdspec->dev_i2ca);

    p_lm75a->devo = devo;    
    memcpy(&p_lm75a->brdspec, p_brdspec, sizeof(lm75a_brdspec_t));

    /* get i2ccore device handle */
    _rc = devman_device_object_identify("i2ccore", p_brdspec->bus_id, &i2ccore_devo);
    if ( _rc || !i2ccore_devo ) {
        bcl_log_err("%s(): Error! Can't identify DEVMAN i2ccore/%d device\n", __FUNCTION__, p_brdspec->bus_id);
        return CCL_FAIL;
    }
    /* get i2ccore read handler('f_read' attribute) */
    _rc = devman_attr_get_word(i2ccore_devo, "f_read", &f_read_val);
    if ( _rc ) {
        bcl_log_err("%s(): Error! Can't get i2cbus/%d f_read attr\n", __FUNCTION__, p_brdspec->bus_id);
        return CCL_FAIL;
    }
    /* get i2cbus write handler('f_write' attribute) */
    _rc = devman_attr_get_word(i2ccore_devo, "f_write", &f_write_val);
    if ( _rc ) {
        bcl_log_err("%s(): Error! Can't get i2cbus/%d f_write attr\n", __FUNCTION__, p_brdspec->bus_id);
        return CCL_FAIL;
    }

    /* store the read/write callbacks */
    p_lm75a->f_read = (read_f)f_read_val;
    p_lm75a->f_write = (write_f)f_write_val;

    return CCL_OK;
}


/* Device instance hw-specific deinitializations 
 */
static ccl_err_t _flm75a_cut(void *devo)
{
    lm75a_ctx_t  *p_lm75a;
    if ( !devo ) {
        PDEBUG("Error! Invalid input parameters\n");
        LM75A_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_lm75a = devman_device_private(devo);
    return CCL_OK;
}

/* device type description structure */
static device_driver_t _lm75a_driver = {
    .name = "lm75a",
    .f_add = _flm75a_add,
    .f_cut = _flm75a_cut,
    .attrs_num = sizeof(_attrs)/sizeof(_attrs[0]),
                 .attrs = _attrs,
    .priv_size = sizeof(lm75a_ctx_t)
}; 

/* Initialize LM75A device type
 */
ccl_err_t devlm75a_init(void)
{
    /* register device type */
    _rc = devman_driver_register(&_lm75a_driver);
    if ( _rc ) {
        PDEBUG("Error! Failed to register driver\n");
        LM75A_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

/* Deinitialize LM75A device type
 */
ccl_err_t devlm75a_fini(void)
{
    /* unregister device type */
    _rc = devman_driver_unregister(&_lm75a_driver);
    if ( _rc ) {
        PDEBUG("Error! Failed to unregister driver\n");
        LM75A_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

