/********************************************************************************/
/**
 * @file devmansm.c
 * @brief DEVMAN simple monitor implementation
 * @copyright 2019 CELARE. All rights reserved.
 */
/********************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <semaphore.h>
#include <linux/limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <readline/readline.h>
#include <readline/history.h>


#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"

#define XON  0x11
#define XOFF 0x13

#define DEVMANSM_CMD_CHAR(c) ( ((c)>='a'&&(c)<='z')||((c)>='A'&&(c)<='Z')||((c)>='0'&&(c)<='9')|| \
                               ((c)=='/')||((c)=='_')||((c)=='-')||((c)=='[')||((c)==']')||((c)=='.') )

ccl_logger_t logger;

static char _devmansm_usage[] = 
"\n"
" devmansm -c <command> : launch a single devman command\n"
" devmansm -f <script>  : launch a script file\n"
" devmansm -r <script>  : launch a script file and start dialog\n"
" devmansm -d           : start the dialog\n"
" devmansm -v           : start the dialog in verbose mode, repeat all commans\n"
" devmansm -h           : print this help\n"
;


static char _devman_command_usage[] = 
"\n"
" s|show types                                      - show registered device types\n"
" s|show all                                        - show all devices,instances\n"
" s|show <type>                                     - show all instances of the device type\n"
" s|show <type>/<discrim>                           - show device instance\n"
" s|show <type>/<discrim> <attrname>                - show device instance attribute value\n"
" s|show <type>/<discrim> <attrname><[idx]>         - show attributes array element value\n"
" s|show <type>/<discrim> <attrname><[idx]> <num>   - show attributes array <num> elements from the <idx>\n"
" c|conf <type>/<discrim> <attrname> <value>        - configure device instance attribute value\n"
" c|conf <type>/<discrim> <attrname><[idx]> <value> - configure attributes array element value\n"
" t|test <type>/<discrim> <attrname> <value>        - test device instance attribute value\n"
" t|test <type>/<discrim> <attrname><[idx]> <value> - test attributes array element value\n"
" x|exec <type>/<discrim>                           - show commands defined for the device\n"
" x|exec <type>/<discrim> <command> [parm1 parm2..] - launch device instance command\n"
" h|help <type>                                     - display registered type info\n"
" h|help <type> <attrname>                          - display registered type attribute info\n"
" exit|quit                                         - leave the monitor\n"
;

static int _rc;


static int _fdevmansm_get_token(char *buf, int buf_size, char *token)
{
    int cnt;

    if ( !buf || !buf_size || !token ) {
        return 0;
    }
    /* skip whitespaces */
    for ( cnt = 0; cnt < buf_size; cnt++ ) {
        if ( buf[cnt] != ' ' && buf[cnt]!='\n' && buf[cnt]!='\0' )
            break;
    }
    if ( cnt >= buf_size )
        return 0;
    /* go to buffer end or first whitespace */
    do {
        if ( cnt >= buf_size )
            break;
        *token++ = buf[cnt++];
    } while ( buf[cnt]!=' ' && buf[cnt]!='\n' && buf[cnt]!='\0' );    
    *token = 0;

    return cnt;
}

static int _fdevmansm_like_cmd_token(char *token)
{
    int i, is_cmd; 
    if ( !token )
        return 0;
    is_cmd = (strnlen(token, DEVMAN_CMD_PARM_LEN) != 0);
    for ( i = 0; i < (int)strnlen(token, DEVMAN_CMD_PARM_LEN); i++ ) {
        if ( !DEVMANSM_CMD_CHAR(token[i]) ) {
            is_cmd = 0;
            break;
        }
    }    
    return is_cmd;
}

static int _fdevmansm_command_parse(char *buf, int buf_size, devman_cmd_t *p_devman_cmd)
{
    char    word[DEVMAN_CMD_PARM_LEN];
    int     cut = 0, parm_idx;

    memset(word, 0, sizeof(word));

    cut = _fdevmansm_get_token(buf, buf_size, word);
    buf += cut;
    buf_size -= cut;
    if ( !_fdevmansm_like_cmd_token(word) ) {
        p_devman_cmd->type = eDEVMAN_CMD_NONE;
        return 0;
    }

    if ( !strncmp(word, "show", strnlen("show", DEVMAN_CMD_PARM_LEN)) || 
         !strncmp(word, "s", strnlen("s", DEVMAN_CMD_PARM_LEN)) || 
         !strncmp(word, "--show", strnlen("--show", DEVMAN_CMD_PARM_LEN)) || 
         !strncmp(word, "-s", strnlen("-s", DEVMAN_CMD_PARM_LEN)) ) {
        p_devman_cmd->type = eDEVMAN_CMD_SHOW;
    } else if ( !strncmp(word, "conf", strnlen("conf", DEVMAN_CMD_PARM_LEN)) || 
                !strncmp(word, "c", strnlen("c", DEVMAN_CMD_PARM_LEN)) || 
                !strncmp(word, "--conf", strnlen("--conf", DEVMAN_CMD_PARM_LEN)) || 
                !strncmp(word, "-c", strnlen("-c", DEVMAN_CMD_PARM_LEN)) ) {
        p_devman_cmd->type = eDEVMAN_CMD_CONF;
    } else if ( !strncmp(word, "test", strnlen("test", DEVMAN_CMD_PARM_LEN)) || 
                !strncmp(word, "t", strnlen("t", DEVMAN_CMD_PARM_LEN)) || 
                !strncmp(word, "--test", strnlen("--test", DEVMAN_CMD_PARM_LEN)) || 
                !strncmp(word, "-t", strnlen("-t", DEVMAN_CMD_PARM_LEN)) ) {
        p_devman_cmd->type = eDEVMAN_CMD_TEST;
    } else if ( !strncmp(word, "exec", strnlen("exec", DEVMAN_CMD_PARM_LEN)) || 
                !strncmp(word, "x", strnlen("x", DEVMAN_CMD_PARM_LEN)) || 
                !strncmp(word, "--exec", strnlen("--exec", DEVMAN_CMD_PARM_LEN)) || 
                !strncmp(word, "-x", strnlen("-x", DEVMAN_CMD_PARM_LEN)) ) {
        p_devman_cmd->type = eDEVMAN_CMD_EXEC;
    } else if ( !strncmp(word, "help", strnlen("help", DEVMAN_CMD_PARM_LEN)) || 
                !strncmp(word, "h", strnlen("h", DEVMAN_CMD_PARM_LEN)) || 
                !strncmp(word, "--help", strnlen("--help", DEVMAN_CMD_PARM_LEN)) || 
                !strncmp(word, "-h", strnlen("-h", DEVMAN_CMD_PARM_LEN)) ) {
        p_devman_cmd->type = eDEVMAN_CMD_HELP;
    } else {
        p_devman_cmd->type = eDEVMAN_CMD_NONE;
        printf("Error! Unknown command: %s\n", word);
        return -1;
    }

    for ( parm_idx = 0; parm_idx < DEVMAN_CMD_PARMS_MAXNUM; parm_idx++ ) {
        memset(word, 0, sizeof(word));
        cut = _fdevmansm_get_token(buf, buf_size, word);
        buf += cut;
        buf_size -= cut;
        if ( !cut )
            break;
        if ( !_fdevmansm_like_cmd_token(word) ) {
            p_devman_cmd->type = eDEVMAN_CMD_NONE;
            return 0;
        }

        //printf("word=%s, cut=%d, buf_size%d\n", word, cut, buf_size);

        strncpy(p_devman_cmd->parm[parm_idx], word, DEVMAN_CMD_PARM_LEN);                
    }
    p_devman_cmd->parms_num = parm_idx;

    return 0;
}

int main(int argc, char** argv)
{
    FILE           *fin=stdin;
    FILE           *fout=stdout;
    char            filename[64], buf[500];
    char           *buf1;
    int             verbose=0, resume=0, script=0, line_num=0, i, j;
    devman_cmd_t    devman_cmd;
    unsigned long   brd_flags = 0;   

    memset(buf, 0, sizeof(buf));
    memset(&devman_cmd, 0, sizeof(devman_cmd_t));

    if ( argc < 2 ) {
        printf("%s\n", _devmansm_usage);
        return -1;
    }

    for ( i = 1; i < argc; i++ ) {
        if ( !strncmp(argv[i], "-v", strnlen("-v", DEVMAN_CMD_PARM_LEN)) )
            verbose = 1;
        else if ( !strncmp(argv[i], "-h", strnlen("-h", DEVMAN_CMD_PARM_LEN)) ) {
            printf("%s\n", _devmansm_usage);
            return 0;
        } else if ( !strncmp(argv[i], "-f", strnlen("-f", DEVMAN_CMD_PARM_LEN)) || 
                    (resume=!strncmp(argv[i], "-r", strnlen("-r", DEVMAN_CMD_PARM_LEN))) ) {
            if ( i+1 >= argc ) {
                printf("%s\n", _devmansm_usage);
                return -1;
            }
            strncpy(filename, argv[i+1], sizeof(filename));
            fin = fopen(filename, "r");
            if (!fin) {
                printf("Cant open file <%s> for reading\n", filename);
                return -1;
            }
            verbose = 1;
            script = 1;
            break;
        } else if ( !strncmp(argv[i], "-c", strnlen("-c", DEVMAN_CMD_PARM_LEN)) ) {
            for ( j = i+1; j < argc; j++ ) {
                memcpy((buf+strnlen(buf, DEVMAN_CMD_PARM_LEN)), argv[j], strnlen(argv[j], DEVMAN_CMD_PARM_LEN));
                memcpy((buf+strnlen(buf, DEVMAN_CMD_PARM_LEN)), " ", sizeof(char));
            }
            if ( !buf[0] )
                buf[0] = 0xff;
            break;
        }

        else if ( !strncmp(argv[i], "-d", strnlen("-d", DEVMAN_CMD_PARM_LEN)) || 
                  !strncmp(argv[i], "-v", strnlen("-v", DEVMAN_CMD_PARM_LEN)) ) {
            break;
        }

        else {
            printf("%s\n", _devmansm_usage);
            return -1;
        }
    }


    /* initialize the board devices */   
    _rc = devboard_init(brd_flags);
    if ( _rc ) {
        fprintf(stderr, "Error! Can't register board devices\n");
        return -1;
    }
    /* check if we've got the command from the shell command line */
    if ( buf[0] ) {
        //printf("%s\n", buf);
        _fdevmansm_command_parse(buf, strnlen(buf, DEVMAN_CMD_PARM_LEN), &devman_cmd);
        if ( devman_cmd.type == eDEVMAN_CMD_NONE ) {
            printf("%s\n", _devmansm_usage);
            return -1;
        } else if ( devman_cmd.type == eDEVMAN_CMD_HELP && !devman_cmd.parms_num ) {
            printf("%s\n", _devman_command_usage);
            return -1;
        }
        return devman_command_handle(&devman_cmd);
    }

    printf("DEVMAN Simple Monitor\n");

    _devman_resume_loop:
    while ((buf1 = readline("devman=> ")) != NULL) {
        if (putc(XOFF, fout) == EOF) {
            printf("Error! putc() failed\n");
            return -1;
        }
        if (strlen(buf1) > 0) {
            add_history(buf1);
        }

        if ( script )
            line_num++;
        if ( verbose ) {
            if ( strchr(buf1, '\n') )
                printf("%s", buf1);
            else
                printf("%s\n", buf1);
            fflush(stdout);
        }
        if ( !memcmp(buf1, "exit", 4) || !memcmp(buf1, "quit", 4) ) {
            free(buf1);
            return 0;
        }

        memset(&devman_cmd, 0, sizeof(devman_cmd));
        _rc = _fdevmansm_command_parse(buf1, strnlen(buf1, DEVMAN_CMD_PARM_LEN), &devman_cmd);
        if ( _rc )
            printf("%s\n", _devman_command_usage);

        //devman_command_show(&devman_cmd);

        if ( devman_cmd.type != eDEVMAN_CMD_NONE ) {
            _rc = devman_command_handle(&devman_cmd);
            if ( _rc == CCL_NOT_DEFINED )
                printf("%s\n", _devman_command_usage);
            if ( _rc && script ) {
                free(buf1);
                printf("DEVMANSM: The %s script is stopped at line %d\n", filename, line_num);
                break;
            }
        }
        free(buf1);
        if (putc(XON, fout) == EOF) {
            printf("Error! putc() failed\n");
            return -1;
        }
    }
    if ( resume ) {
        fin=stdin;
        verbose = 0;
        goto _devman_resume_loop;
    }

    devboard_fini();

    printf("\n");

    return _rc;
}

