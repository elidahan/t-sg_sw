/********************************************************************************/
/**
 * @file intc.c
 * @brief Xilinx Interrupt Controller device manager driver
 * @copyright 2019 CELARE. All rights reserved.
 */
/********************************************************************************/
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <semaphore.h>
#include <linux/limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "ccl_intr.h"
#include "devman.h"
#include "devboard.h"
#include "spibus.h"
#include "intc.h"

//#define INTC_DEBUG
/* printing/error-returning macros */
#ifdef INTC_DEBUG
    #define PDEBUG(fmt, args...) \
            ccl_syslog_err("INTC: %s[%d]: " fmt, __FUNCTION__, __LINE__, ## args)
#else
    #define PDEBUG(fmt, args...)
#endif
#define INTC_ERROR_RETURN(err) do { \
   ccl_syslog_err("Error! %s[%d], error=%i\n", __FUNCTION__, __LINE__, (err)); \
   return err; \
} while (0)

#define INTC_REG_READ(context, offset, offset_sz, idx, pvalue, size) do { \
    _ret = _intc_reg_read((context), \
                          (offset), \
                          (offset_sz), (idx), \
                          (pvalue), (size)); \
    if ( _ret ) { \
        PDEBUG("_intc_reg_read error\n"); \
        INTC_ERROR_RETURN(_ret); \
    } \
} while (0)

#define INTC_REG_WRITE(context, offset, offset_sz, idx, pvalue, size) do { \
    _ret = _intc_reg_write((context), \
                           (offset), \
                           (offset_sz), (idx), \
                           (pvalue), (size)); \
    if ( _ret ) { \
        PDEBUG("_intc_reg_write error\n"); \
        INTC_ERROR_RETURN(_ret); \
    } \
} while (0)

/** 
 *  intc registers offsets
 */
#define INTC_ISR_OFFSET           0x00   /**< Interrupt Status Register */              
#define INTC_IPR_OFFSET           0x04   /**< Interrupt Pending Register */             
#define INTC_IER_OFFSET           0x08   /**< Interrupt Enable Register */              
#define INTC_IAR_OFFSET           0x0c   /**< Interrupt Acknowledge Register */         
#define INTC_SIE_OFFSET           0x10   /**< Set Interrupt Enable Register */          
#define INTC_CIE_OFFSET           0x14   /**< Clear Interrupt Enable Register */        
#define INTC_IVR_OFFSET           0x18   /**< Interrupt Vector Register */              
#define INTC_MER_OFFSET           0x1C   /**< Master Enable Register */                 
#define INTC_IMR_OFFSET           0x20   /**< Interrupt mode Register,                  
                                           *  this is present only                      
                                           *  for Fast Interrupt                        
                                           */                                           
#define INTC_ILR_OFFSET           0x24   /**< Interrupt level register */               
#define INTC_IVAR_OFFSET          0x100  /**< Interrupt Vector Address Register         
				                           *  Interrupt 0 Offest, this is present       
				                           *  only for Fast Interrupt                   
                                           */                                           
#define INTC_IVAR_LAST_OFFSET     0x100  /**< Interrupt Vector Address Register
				                           *  Last
                                           */
#define INTC_IVEAR_OFFSET         0x200  /**< Interrupt Vector Extended Address   
					                       *  Register                            
                                           */                                     
#define INTC_IVEAR_LAST_OFFSET    0x2FC  /**< Interrupt Vector Extended Address
					                       *  Register Last
                                           */
#define INTC_MAX_OFFSET INTC_IVEAR_LAST_OFFSET

/** 
 *  Bit definitions for the bits of the MER register
 */
#define INTC_INT_MASTER_ENABLE_MASK      0x1UL
#define INTC_INT_HARDWARE_ENABLE_MASK    0x2UL	/**< once set cannot be cleared */

/**
 * @name mask to specify maximum number of interrupt sources per controller
 * @{
 */
#define INTC_CONTROLLER_MAX_INTRS	        32          /**< Each Controller has 32  
                                                          *  interrupt pins          
                                                          */                         
#define INTC_CONTROLLERS_MAX_NUM            2
#define INTC_CONTROLLERS_MAX_INTRS_NUM      INTC_CONTROLLER_MAX_INTRS * INTC_CONTROLLERS_MAX_NUM
#define INTC_STANDARD_VECTOR_ADDRESS_WIDTH	32U

/** The following data type defines each entry in an interrupt vector table.
 * The callback reference is the base address of the interrupting device
 * for the driver interface given in this file and an instance pointer for the
 * driver interface given in xintc.h file.
 */
typedef struct {
	ccl_interrupt_handler handler;
	void *cb_ref;
} intc_vector_table_entry_t;

/**
 * This typedef contains configuration information for a Axi 
 * intc device. 
 */
typedef struct {
    b_u32 ack_before_service;	/**< Ack location per interrupt */
	b_bool is_fast_intr;		/**< Fast Interrupt enabled */
	b_u32 intr_vector_address;	/**< Interrupt Vector Address */
	b_i32 num_of_intrs;         /**< Number of Interrupt sources */
	b_u8 vector_addr_width;		/**< Width of vector address */
	b_u32 options;		        /**< Device options */
	intc_mode_t mode;		    /**< Intc mode */
	intc_vector_table_entry_t handler_table[INTC_CONTROLLER_MAX_INTRS];
} intc_config_t;

/** @struct intc_ctx_t
 *  intc context structure
 */
typedef struct intc_ctx_t {
    void             *devo;
    intc_brdspec_t   brdspec;
	b_bool           is_ready;		 /**< Device is initialized and ready */
	b_bool           is_started;	 /**< Device has been started */
    b_bool           is_master;      /**< Master/Slave device */
    b_u32            bit_pos_mask[INTC_CONTROLLER_MAX_INTRS];
    intc_config_t    config;
} intc_ctx_t;

static ccl_err_t  _ret;
static int        _rc;
ccl_err_t _intc_init(void *p_priv);

/* interrupt controllers references */
static intc_ctx_t *_p_intc[eDEVBRD_INTC_DEV_NUM];

/**
 *
 * Enable all interrupts in the Master Enable register of the interrupt
 * controller.  The interrupt controller defaults to all interrupts disabled
 * from reset such that this macro must be used to enable interrupts.
 *
 * @param[in] p_intc is a pointer to the Axi INTC instance to be
 *       worked on.
 *
 * @return None
 */
#define INTC_MASTER_ENABLE(p_intc) do { \
    b_u32 regval = (INTC_INT_MASTER_ENABLE_MASK | \
                    INTC_INT_HARDWARE_ENABLE_MASK); \
    INTC_REG_WRITE(p_intc, INTC_MER_OFFSET, \
                   sizeof(b_u32), 0, \
                   (b_u8 *)&regval, sizeof(b_u32)); \
} while (0)

/**
 *
 * Disable all interrupts in the Master Enable register of the interrupt
 * controller.
 *
 * @param[in] p_intc is a pointer to the Axi INTC instance to be
 *       worked on.
 *
 * @return None
 */
#define INTC_MASTER_DISABLE(p_intc) do { \
    b_u32 regval = 0; \
    INTC_REG_WRITE(p_intc, INTC_MER_OFFSET, \
                   sizeof(b_u32), 0, \
                   (b_u8 *)&regval, sizeof(b_u32)); \
} while (0)

/**
 * Enable specific interrupt(s) in the interrupt controller.
 *
 * @param[in] p_intc is a pointer to the Axi INTC instance to be
 *       worked on.
 * @param[in] mask is the 32-bit value to write to the enable 
 *       register. Each bit of the mask corresponds to an
 *       interrupt input signal that is connected to the
 *       interrupt controller (INT0 = LSB). Only the bits which
 *       are set in the mask will enable interrupts.
 *
 * @return None
 */
#define INTC_ENABLE_INTR(p_intc, mask) do { \
    INTC_REG_WRITE(p_intc, INTC_IER_OFFSET, \
                   sizeof(b_u32), 0, \
                   (b_u8 *)&mask, sizeof(b_u32)); \
} while (0)

/**
 *
 * Disable specific interrupt(s) in the interrupt controller.
 *
 * @param[in] p_intc is a pointer to the Axi INTC instance to be
 *       worked on.
 *  @param[in] mask is the 32-bit value to write to the enable
 *       register. Each bit of the mask corresponds to an
 *       interrupt input signal that is connected to the
 *       interrupt controller (INT0 = LSB). Only the bits which
 *       are set in the mask will disable interrupts.
 *
 * @return None
 */
#define INTC_DISABLE_INTR(p_intc, mask) do { \
    b_u32 regval = ~(mask); \
    INTC_REG_WRITE(p_intc, INTC_IER_OFFSET, \
                   sizeof(b_u32), 0, \
                   (b_u8 *)&mask, sizeof(b_u32)); \
} while (0)

/**
 *
 * Acknowledge specific interrupt(s) in the interrupt controller.
 *
 * @param[in] p_intc is a pointer to the Axi INTC instance to be
 *       worked on.
 *  @param[in] mask is the 32-bit value to write to the acknowledge
 *		register. Each bit of the mask corresponds to an interrupt input
 *		signal that is connected to the interrupt controller (INT0 =
 *		LSB).  Only the bits which are set in the mask will acknowledge
 *		interrupts.
 *
 * @return None
 */
#define INTC_ACK_INTR(p_intc, mask) do { \
    INTC_REG_WRITE(p_intc, INTC_IAR_OFFSET, \
                   sizeof(b_u32), 0, \
                   (b_u8 *)&mask, sizeof(b_u32)); \
} while (0)

/**
 * Get the interrupt status from the interrupt controller which indicates
 * which interrupts are active and enabled.
 *
 * @param[in] p_intc is a pointer to the Axi INTC instance to be
 *       worked on.
 * @param[out] regval is the 32-bit contents of the interrupt 
 *      status register. Each bit corresponds to an interrupt
 *      input signal that is connected to the interrupt
 *      controller (INT0 = LSB). Bits which are set indicate an
 *      active interrupt which is also enabled.
 *
 * @return None
 */
#define INTC_GET_INTR_STATUS(p_intc, regval) do { \
    b_u32 temp; \
    INTC_REG_READ(p_intc, INTC_ISR_OFFSET, \
                  sizeof(b_u32), 0, \
                  (b_u8 *)&temp, sizeof(b_u32)); \
    INTC_REG_READ(p_intc, INTC_IER_OFFSET, \
                  sizeof(b_u32), 0, \
                  (b_u8 *)regval, sizeof(b_u32)); \
    *regval &= temp; \
} while (0)

/* intc register read */
static ccl_err_t _intc_reg_read(__attribute__((unused)) void *p_priv, 
                                b_u32 offset, 
                                __attribute__((unused)) b_u32 offset_sz, 
                                b_u32 idx, b_u8 *p_value, b_u32 size)
{
    intc_ctx_t *p_intc; 

    if ( offset > INTC_MAX_OFFSET || 
         !p_value || idx || size != sizeof(b_u32) ) {
        PDEBUG("Error! Invalid input parameters\n");
        INTC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    PDEBUGG("123$=> offset=0x%03x, size=%d\n", offset, size);

    p_intc = (intc_ctx_t *)p_priv;     
    offset += p_intc->brdspec.offset;
    _ret = devspibus_read_buff(p_intc->brdspec.devname, offset, idx, p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to read over spi bus\n");
        INTC_ERROR_RETURN(_ret);
    }
    PDEBUG("p_intc->brdspec.offset=0x%x, offset=0x%03x, value=0x%08x\n", 
           p_intc->brdspec.offset, offset, *(b_u32 *)p_value);

    return _ret;
}

/* intc register write */
static ccl_err_t _intc_reg_write(__attribute__((unused)) void *p_priv, 
                                 b_u32 offset, 
                                 __attribute__((unused)) b_u32 offset_sz, 
                                 b_u32 idx, b_u8 *p_value, b_u32 size)
{
    intc_ctx_t *p_intc; 

    if ( offset > INTC_MAX_OFFSET || 
         !p_value || idx || size != sizeof(b_u32) ) {
        PDEBUG("Error! Invalid input parameters\n");
        INTC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_intc = (intc_ctx_t *)p_priv;     
    offset += p_intc->brdspec.offset;
    PDEBUG("p_intc->brdspec.offset=0x%x, offset=0x%03x, value=0x%08x\n", 
           p_intc->brdspec.offset, offset, *(b_u32 *)p_value);
    _ret = devspibus_write_buff(p_intc->brdspec.devname, offset, idx, p_value, size);
    if ( _ret ) {
        PDEBUG("Error! Failed to write over spi bus\n");
        INTC_ERROR_RETURN(_ret);
    }

    return _ret;
}

static ccl_err_t _intc_configure_test(void *p_priv)
{
    intc_ctx_t *p_intc; 

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        INTC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_intc = (intc_ctx_t *)p_priv;

    return CCL_OK;
}

static ccl_err_t _intc_run_test(void *p_priv, 
                                __attribute__((unused)) b_u8 *buf, 
                                __attribute__((unused)) b_u32 size)
{
    intc_ctx_t *p_intc; 

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        INTC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_intc = (intc_ctx_t *)p_priv;

    return CCL_OK;
}

ccl_err_t intc_attr_read(void *p_priv, b_u32 attrid, 
                          b_u32 offset, b_u32 offset_sz, 
                          b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || offset > INTC_MAX_OFFSET || 
         !p_value || idx || size != sizeof(b_u32) ) {
        PDEBUG("Error! Invalid input parameters\n");
        INTC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch (attrid) {
    case eINTC_ISR:
    case eINTC_IPR:
    case eINTC_IER:
    case eINTC_IAR:
    case eINTC_SIE:
    case eINTC_CIE:
    case eINTC_IVR:
    case eINTC_MER:
    case eINTC_IMR:
    case eINTC_ILR:
    case eINTC_IVAR:
    case eINTC_IVEAR:
        INTC_REG_READ(p_priv, offset, offset_sz, idx, p_value, size);
        break;
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK) {
        PDEBUG("Failure in attribute id: %d\n", attrid);
    }

    return _ret;               

}

ccl_err_t intc_attr_write(void *p_priv, b_u32 attrid, 
                          b_u32 offset, b_u32 offset_sz, 
                          b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || offset > INTC_MAX_OFFSET || 
         !p_value || idx || size != sizeof(b_u32) ) {
        PDEBUG("Error! Invalid input parameters\n");
        INTC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch (attrid) {
    case eINTC_ISR:
    case eINTC_IPR:
    case eINTC_IER:
    case eINTC_IAR:
    case eINTC_SIE:
    case eINTC_CIE:
    case eINTC_IVR:
    case eINTC_MER:
    case eINTC_IMR:
    case eINTC_ILR:
    case eINTC_IVAR:
    case eINTC_IVEAR:
        INTC_REG_WRITE(p_priv, offset, offset_sz, idx, p_value, size);
        break;
    case eINTC_INIT:
        _ret = _intc_init(p_priv);
        break;
    case eINTC_CONFIGURE_TEST:
        _ret = _intc_configure_test(p_priv);
        break;
    case eINTC_RUN_TEST:
        _ret = _intc_run_test(p_priv, p_value, size);
        break;
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK) {
        PDEBUG("Failure in attribute id: %d\n", attrid);
    }

    return _ret;               

}

/* intc device attributes definition */
static device_attr_t _attrs[] = {
    {
        .name = "isr", .id = eINTC_ISR, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = INTC_ISR_OFFSET
    },
    {
        .name = "ipr", .id = eINTC_IPR, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = INTC_IPR_OFFSET
    },
    {
        .name = "ier", .id = eINTC_IER, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = INTC_IER_OFFSET
    },
    {
        .name = "iar", .id = eINTC_IAR, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = INTC_IAR_OFFSET
    },
    {
        .name = "sie", .id = eINTC_SIE, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = INTC_SIE_OFFSET
    },
    {
        .name = "cie", .id = eINTC_CIE, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = INTC_CIE_OFFSET
    },
    {
        .name = "ivr", .id = eINTC_IVR, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = INTC_IVR_OFFSET
    },
    {
        .name = "mer", .id = eINTC_MER, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = INTC_MER_OFFSET
    },
    {
        .name = "imr", .id = eINTC_IMR, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = INTC_IMR_OFFSET
    },
    {
        .name = "ilr", .id = eINTC_ILR, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = INTC_ILR_OFFSET
    },
    {
        .name = "ivar", .id = eINTC_IVAR, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = INTC_IVAR_OFFSET
    },
    {
        .name = "ivear", .id = eINTC_IVEAR, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32), .offset = INTC_IVEAR_OFFSET
    },
    {
        .name = "init", .id = eINTC_INIT, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "config_test", .id = eINTC_CONFIGURE_TEST, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "run_test", .id = eINTC_RUN_TEST, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, 
        .size = sizeof(board_dev_stats_t), 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
};

/**
 * A stub for the asynchronous callback. The stub is here in case the upper
 * layers forget to set the handler.
 *
 * @param[in] cb_ref is a pointer to the upper layer callback
 *            reference
 *
 * @return None.
 */
static void _intc_stub_handler(void *cb_ref)
{
    if ( !cb_ref ) {
        PDEBUG("Error! Invalid input parameters\n");
    }

    return;
}

/**
 * Enables the interrupt source provided as the argument intrid. Any pending
 * interrupt condition for the specified intrid will occur after this function is
 * called. In Cascade mode, enables corresponding interrupt of Slave controllers
 * depending on the intrid.
 *
 * @param[in] intrid contains the ID of the interrupt source 
 *
 * @return	error code
 */
ccl_err_t _intc_enable(b_u8 intrid)
{
	b_u32 regval;
	b_u32 mask;
    intc_ctx_t *p_intc = NULL;

    if ( intrid >= INTC_CONTROLLERS_MAX_INTRS_NUM ) {
        PDEBUG("Error! Invalid input parameters\n");
        INTC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    if (intrid >= INTC_CONTROLLER_MAX_INTRS) {
        /* Enable user required intrid in Slave controller */
        p_intc = _p_intc[1];
        /* Convert from integer id to bit mask */
        mask = p_intc->bit_pos_mask[(intrid % INTC_CONTROLLER_MAX_INTRS)];

	} else {
        p_intc = _p_intc[0];
        /* Convert from integer id to bit mask */
        mask = p_intc->bit_pos_mask[intrid];
	}
    /*
     * Enable the selected interrupt source by reading the
     * interrupt enable register and then modifying only the
     * specified interrupt id enable
     */
    INTC_REG_READ(p_intc, INTC_IER_OFFSET, 
                  sizeof(b_u32), 0, 
                  (b_u8 *)&regval, sizeof(b_u32));

    regval |= mask;
    INTC_REG_WRITE(p_intc, INTC_IER_OFFSET, 
                   sizeof(b_u32), 0, 
                   (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/*****************************************************************************/
/**
*
* Disables the interrupt source provided as the argument intrid such that the
* interrupt controller will not cause interrupts for the specified intrid. The
* interrupt controller will continue to hold an interrupt condition for the
* intrid, but will not cause an interrupt.In Cascade mode, disables corresponding
* interrupt of Slave controllers depending on the intrid.
*
* @param	intrid contains the ID of the interrupt source and should be in the
*		range of 0 to XPAR_INTC_MAX_NUM_INTR_INPUTS - 1 with 0 being the
*		highest priority interrupt.
*
* @return	None.
*
* @note		None.
*
****************************************************************************/
ccl_err_t _intc_disable(b_u8 intrid)
{
	b_u32 regval;
	b_u32 mask;
    intc_ctx_t *p_intc = NULL;

    if ( intrid >= INTC_CONTROLLERS_MAX_INTRS_NUM ) {
        PDEBUG("Error! Invalid input parameters\n");
        INTC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    if (intrid >= INTC_CONTROLLER_MAX_INTRS) {
        /* Enable user required intrid in Slave controller */
        p_intc = _p_intc[1];
        /* Convert from integer id to bit mask */
        mask = p_intc->bit_pos_mask[(intrid % INTC_CONTROLLER_MAX_INTRS)];
	} else {
        p_intc = _p_intc[0];
        /* Convert from integer id to bit mask */
        mask = p_intc->bit_pos_mask[intrid];
	}

    /*
     * Disable the selected interrupt source by reading the
     * interrupt enable register and then modifying only the
     * specified interrupt id enable
     */
    INTC_REG_READ(p_intc, INTC_IER_OFFSET, 
                  sizeof(b_u32), 0, 
                  (b_u8 *)&regval, sizeof(b_u32));

    regval &= ~mask;
    INTC_REG_WRITE(p_intc, INTC_IER_OFFSET, 
                   sizeof(b_u32), 0, 
                   (b_u8 *)&regval, sizeof(b_u32));

    return CCL_OK;
}

/**
 * Acknowledges the interrupt source provided as the argument intrid. When the
 * interrupt is acknowledged, it causes the interrupt controller to clear its
 * interrupt condition.In Cascade mode, acknowledges corresponding interrupt
 * source of Slave controllers depending on the intrid.
 *
 * @param[in]	intrid contains the ID of the interrupt source 
 *
 * @return	error code
 */
ccl_err_t _intc_acknowledge(b_u8 intrid)
{
	b_u32 mask;
    intc_ctx_t *p_intc = NULL;

    if ( intrid >= INTC_CONTROLLERS_MAX_INTRS_NUM ) {
        PDEBUG("Error! Invalid input parameters\n");
        INTC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

	if (intrid >= INTC_CONTROLLER_MAX_INTRS) {
		/* Enable user required intrid in Slave controller */
		p_intc = _p_intc[1];

		/* Convert from integer id to bit mask */
		mask = p_intc->bit_pos_mask[(intrid % INTC_CONTROLLER_MAX_INTRS)];
        INTC_REG_WRITE(p_intc, INTC_IAR_OFFSET, 
                       sizeof(b_u32), 0, 
                       (b_u8 *)&mask, sizeof(b_u32));
	} else {
        p_intc = _p_intc[0];
		/*
		 * The intrid is used to create the appropriate mask for the
		 * desired bit position.
		 */
		mask = p_intc->bit_pos_mask[intrid];

		/*
		 * Acknowledge the selected interrupt source, no read of the
		 * acknowledge register is necessary since only the bits set
		 * in the mask will be affected by the write
		 */
        INTC_REG_WRITE(p_intc, INTC_IAR_OFFSET, 
                       sizeof(b_u32), 0, 
                       (b_u8 *)&mask, sizeof(b_u32));
	}

    return CCL_OK;
}

/**
 * Makes the connection between the intrid of the interrupt source and the
 * associated handler that is to run when the interrupt is recognized. The
 * argument provided in this call as the Callback ref is used as 
 * the argument for the handler when it is called. In Cascade 
 * mode, connects handler to Slave controller handler table 
 * depending on the interrupt intrid. 
 *
 * @param[in] idx contains the ID of the interrupt source 
 * @param[in] handler to the handler for that interrupt.
 * @param[in] cb_ref is the callback reference, usually the 
 *      instance pointer of the connecting driver.
 *
 * @return error code
 */
ccl_err_t _intc_connect(b_u8 idx, ccl_interrupt_handler handler, void *cb_ref)
{
    intc_ctx_t *p_intc = NULL;
    b_u8 intrid, dev_id;

    if ( idx >= INTC_CONTROLLERS_MAX_INTRS_NUM ) {
        PDEBUG("Error! Invalid input parameters\n");
        INTC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    if (idx >= INTC_CONTROLLER_MAX_INTRS) {
        dev_id = 1;
        intrid = idx % INTC_CONTROLLER_MAX_INTRS;
    } else {
        dev_id = 0;
        intrid = idx;
    }

    p_intc = _p_intc[dev_id];
    p_intc->config.handler_table[intrid].handler = handler;
    p_intc->config.handler_table[intrid].cb_ref = cb_ref;

	return CCL_OK;
}

/**
 * Updates the interrupt table with the Null Handler and NULL arguments at the
 * location pointed at by the intrid. This effectively disconnects that interrupt
 * source from any handler. The interrupt is disabled also. In Cascade mode,
 * disconnects handler from Slave controller handler table depending on the
 * interrupt intrid.
 *
 * @param[in] idx contains the ID of the interrupt source
 *
 * @return	None.
 */
ccl_err_t _intc_disconnect(b_u8 idx)
{
	b_u32 regval;
	b_u32 mask;
    intc_ctx_t *p_intc = NULL;
    b_u8 intrid, dev_id;

    if ( idx >= INTC_CONTROLLERS_MAX_INTRS_NUM ) {
        PDEBUG("Error! Invalid input parameters\n");
        INTC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

	/*
	 * Disable the interrupt such that it won't occur while disconnecting
	 * the handler, only disable the specified interrupt id without
	 * modifying the other interrupt ids
	 */

	/* Disconnect Handlers for Slave controllers in Cascade Mode */
    if (idx >= INTC_CONTROLLER_MAX_INTRS) {
        dev_id = 1;
        intrid = idx % INTC_CONTROLLER_MAX_INTRS;
    } else {
        dev_id = 0;
        intrid = idx;
    }
    /* Enable user required intrid in Slave controller */
    p_intc = _p_intc[dev_id];
    /* Convert from integer id to bit mask */
    mask = p_intc->bit_pos_mask[intrid];
    INTC_REG_READ(p_intc, INTC_IER_OFFSET, 
                  sizeof(b_u32), 0, 
                  (b_u8 *)&regval, sizeof(b_u32));

    regval &= ~mask;
    INTC_REG_WRITE(p_intc, INTC_IER_OFFSET, 
                   sizeof(b_u32), 0, 
                   (b_u8 *)&regval, sizeof(b_u32));
    /*
     * Disconnect the handler and connect a stub, the callback
     * reference must be set to this instance to allow unhandled
     * interrupts to be tracked
     */
    p_intc->config.handler_table[intrid].handler = _intc_stub_handler;
    p_intc->config.handler_table[intrid].cb_ref = p_intc;

    return CCL_OK;
}

/**
 * This function is called by primary interrupt handler for the driver to handle
 * all Controllers in Cascade mode.It will resolve which interrupts are active
 * and enabled and call the appropriate interrupt handler. It uses the
 * AckBeforeService flag in the configuration data to determine when to
 * acknowledge the interrupt. Highest priority interrupts are serviced first.
 * This function assumes that an interrupt vector table has been previously
 * initialized.  It does not verify that entries in the table are valid before
 * calling an interrupt handler.This function calls itself recursively to handle
 * all interrupt controllers.
 *
 * @param[in] dev_id is the zero-based device ID
 *
 * @return	error code 
 */ 
ccl_err_t _intc_device_interrupt_handler(b_u32 dev_id)
{
	b_u32 status;
	b_u32 mask = 1;
	int idx;
	b_u32 regval;
    intc_ctx_t *p_intc = NULL;

    if ( dev_id >= INTC_CONTROLLERS_MAX_NUM ) {
        PDEBUG("Error! Invalid input parameters\n");
        INTC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_intc = _p_intc[dev_id];
	/* Get the interrupts that are waiting to be serviced */
    INTC_GET_INTR_STATUS(p_intc, &status);

	/* Mask the Fast Interrupts */
	if (p_intc->config.is_fast_intr == CCL_TRUE) {
        INTC_REG_READ(p_intc, INTC_IMR_OFFSET, 
                      sizeof(b_u32), 0, 
                      (b_u8 *)&regval, sizeof(b_u32));
		status &=  ~regval;
	}

	/* Service each interrupt that is active and enabled by
	 * checking each bit in the register from LSB to MSB which
	 * corresponds to an interrupt input signal
	 */
	for (idx = 0; idx < p_intc->config.num_of_intrs; idx++) {
		if (status & 1) {
			/* In Cascade mode call this function recursively
             * for interrupt id INTC_CONTROLLER_MAX_INTRS - 1 
             * and until interrupts of last instance/controller are handled
			 */
			if ((idx == INTC_CONTROLLER_MAX_INTRS - 1) &&
			  (p_intc->config.mode != eDEVBRD_INTC_MODE_SECONDARY)) {
				_intc_device_interrupt_handler(++dev_id);
			}

			/* If the interrupt has been setup to
			 * acknowledge it before servicing the
			 * interrupt, then ack it */
			if (p_intc->config.ack_before_service & mask) {
				INTC_ACK_INTR(p_intc, mask);
			}

			/* Handler of INTC_CONTROLLER_MAX_INTRS - 1 interrupt Id has to be called only
			 * for Last controller in cascade Mode
			 */
			if (!((idx == INTC_CONTROLLER_MAX_INTRS - 1) &&
			  (p_intc->config.mode != eDEVBRD_INTC_MODE_SECONDARY))) {

				/* The interrupt is active and enabled, call
				 * the interrupt handler that was setup with
				 * the specified parameter
				 */
				p_intc->config.handler_table[idx].handler(p_intc);
			}
			/* If the interrupt has been setup to acknowledge it
			 * after it has been serviced then ack it
			 */
            if (!(p_intc->config.ack_before_service & mask)) {
                INTC_ACK_INTR(p_intc, mask);
            }

			/*
			 * Read the ISR again to handle architectures with
			 * posted write bus access issues.
			 */
             INTC_GET_INTR_STATUS(p_intc, &status);

			/*
			 * If only the highest priority interrupt is to be
			 * serviced, exit loop and return after servicing
			 * the interrupt
			 */
			if (p_intc->config.options == eDEVBRD_INTC_SVC_SGL_ISR_OPTION) {
				return CCL_OK;
			}
		}

		/* Move	 to the next interrupt to check */
		mask <<= 1;
		status >>= 1;

		/* If there are no other bits set indicating that all interrupts
		 * have been serviced, then exit the loop
		 */
		if (status == 0) 
            break;
	}
    return CCL_OK;
}

/**
 *
 * Initialize a specific interrupt controller instance/driver. The
 * initialization entails:
 *
 *	- Initialize fields of the XIntc structure
 *	- Initial vector table with stub function calls
 *	- All interrupt sources are disabled
 *	- Interrupt output is disabled
 *
 * @param[in] p_intc is a pointer to the XIntc instance to be worked on.
 *
 *  @return error code
 */
ccl_err_t _intc_init(void *p_priv)
{
    intc_ctx_t *p_intc;
	b_u8       i;
	b_u32      next_bit_mask = 1;
    b_u32      regval;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        INTC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_intc = (intc_ctx_t *)p_priv;
    /*
     * Initialize all the data needed to perform interrupt processing for
     * each interrupt ID up to the maximum used
     */
    for (i = 0; i < p_intc->config.num_of_intrs; i++) {

        /*
         * Initalize the handler to point to a stub to handle an
         * interrupt which has not been connected to a handler. Only
         * initialize it if the handler is 0 or XNullhandler, which
         * means it was not initialized statically by the tools/user.
         * Set the callback reference to this instance so that
         * unhandled interrupts can be tracked.
         */
        if ( p_intc->config.handler_table[i].handler == 0) 
            p_intc->config.handler_table[i].handler = _intc_stub_handler;
        p_intc->config.handler_table[i].cb_ref = p_intc;

        /*
         * Initialize the bit position mask table such that bit
         * positions are lookups only for each interrupt id, with 0
         * being a special case
         * (bit_pos_mask[] = { 1, 2, 4, 8, ... })
         */
        p_intc->bit_pos_mask[i] = next_bit_mask;
        next_bit_mask *= 2;
    }

    if (p_intc->is_master) {
        /*
         * If the device is started, disallow the initialize and return a status
         * indicating it is started.  This allows the user to stop the device
         * and reinitialize, but prevents a user from inadvertently initializing
         */
        if (p_intc->is_started) {
            PDEBUG("Device already started\n");
            return CCL_OK;
        }

        /*
         * Set some default values
         */
        p_intc->is_ready = CCL_FALSE;
        p_intc->is_started = CCL_FALSE;
        p_intc->config.options = eDEVBRD_INTC_SVC_SGL_ISR_OPTION;
        p_intc->config.mode = eDEVBRD_INTC_MODE_PRIMARY;

        /*
         * Disable IRQ output signal
         * Disable all interrupt sources
         * Acknowledge all sources
         */
        regval = 0;
        INTC_REG_WRITE(p_intc, INTC_MER_OFFSET, 
                       sizeof(b_u32), 0, 
                       (b_u8 *)&regval, sizeof(b_u32));
        INTC_REG_WRITE(p_intc, INTC_IER_OFFSET, 
                       sizeof(b_u32), 0, 
                       (b_u8 *)&regval, sizeof(b_u32));
        regval = 0xffffffff;
        INTC_REG_WRITE(p_intc, INTC_IAR_OFFSET, 
                       sizeof(b_u32), 0, 
                       (b_u8 *)&regval, sizeof(b_u32));
        /* reenable interrupt id 31 for interaction with slave device */
        regval = p_intc->bit_pos_mask[31];
        INTC_REG_WRITE(p_intc, INTC_IER_OFFSET, 
                       sizeof(b_u32), 0, 
                       (b_u8 *)&regval, sizeof(b_u32));
    } else {
        regval = 0xffffffff;
        INTC_REG_WRITE(p_intc, INTC_IAR_OFFSET, 
                       sizeof(b_u32), 0, 
                       (b_u8 *)&regval, sizeof(b_u32));
        regval = 0;
        INTC_REG_WRITE(p_intc, INTC_IER_OFFSET, 
                       sizeof(b_u32), 0, 
                       (b_u8 *)&regval, sizeof(b_u32));
        INTC_REG_WRITE(p_intc, INTC_MER_OFFSET, 
                       sizeof(b_u32), 0, 
                       (b_u8 *)&regval, sizeof(b_u32));
        p_intc->config.mode = eDEVBRD_INTC_MODE_SECONDARY;
    }

	/*
	 * Indicate the instance is now ready to use, successfully initialized
	 */
	p_intc->is_ready = CCL_TRUE;

	return CCL_OK;
}

/**
 * Starts the interrupt controller by enabling the output from the controller
 * to the processor. Interrupts may be generated by the interrupt controller
 * after this function is called.
 *
 * It is necessary for the caller to connect the interrupt handler of this
 * component to the proper interrupt source. This function also starts Slave
 * controllers in Cascade mode.
 *
 * @param[in] p_intc is a pointer to axieth1g_ctx_t instance to 
 *       be worked on
 * @param[in] mode determines if software is allowed to simulate interrupts or
 *		real interrupts are allowed to occur. Note that these modes are
 *		mutually exclusive. The interrupt controller hardware resets in
 *		a mode that allows software to simulate interrupts until this
 *		mode is exited. It cannot be reentered once it has been exited.
 *
 *		One of the following values should be used for the mode.
 *		- INTC_SIMULATION_MODE enables simulation of interrupts only
 *		- INTC_REAL_MODE enables hardware interrupts only
 *
 * @return error code
 *
 * @note 	Must be called after Interrupt Controller initialization is completed.
 */
ccl_err_t _intc_start(intc_ctx_t *p_intc, b_u8 mode)
{
	b_u32 master_en = INTC_INT_MASTER_ENABLE_MASK;

	/*
	 * Assert the arguments
	 */
    if ( !p_intc || 
         ((mode != INTC_SIMULATION_MODE) && 
         (mode != INTC_REAL_MODE)) || 
         !p_intc->is_ready ) {
        PDEBUG("Error! Invalid input parameters\n");
        INTC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

	if (mode == INTC_REAL_MODE) 
        master_en |= INTC_INT_HARDWARE_ENABLE_MASK;	

	/*
	 * Indicate the instance is ready to be used and is started before we
	 * enable the device.
	 */
	p_intc->is_started = CCL_TRUE;

    INTC_REG_WRITE(p_intc, INTC_MER_OFFSET, 
                   sizeof(b_u32), 0, 
                   (b_u8 *)&master_en, sizeof(b_u32));

	return CCL_OK;
}

static ccl_err_t _fintc_cmd_start(devo_t devo, 
                                  device_cmd_parm_t parms[], 
                                  __attribute__((unused)) b_u16 parms_num)
{
    intc_ctx_t *p_intc = devman_device_private(devo);

    /* start the device */
    _ret = _intc_start(p_intc, (b_u8)parms[0].value);
    if ( _ret ) {
        PDEBUG("_intc_start error\n");
        INTC_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

static ccl_err_t _fintc_cmd_init(devo_t devo, 
                                 __attribute__((unused)) device_cmd_parm_t parms[], 
                                 __attribute__((unused)) b_u16 parms_num)
{
    void *p_priv = devman_device_private(devo);

    /* start the device */
    _ret = _intc_init(p_priv);
    if ( _ret ) {
        PDEBUG("_intc_init error\n");
        INTC_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

/* device commands */
static device_cmd_t _cmds[] = 
{
    { 
        .name = "start", .f_cmd = _fintc_cmd_start, .parms_num = 1,
        .parms = (device_cmd_parm_t[])
        { 
            { .name = "operation mode", .type = eDEVMAN_ATTR_BYTE, .size = sizeof(b_u8), .format = "%d", .flags = eDEVMAN_ATTRFLAG_OPTIONAL }
        }, 
        .help = "Start INTC device" 
    },
    { 
        .name = "init", .f_cmd = _fintc_cmd_init, .parms_num = 0,
        .help = "Init INTC device" 
    },
};

/* Device instance hw-specific initializations 
 */
static ccl_err_t _fintc_add(void *devo, void *brdspec) 
{
    intc_ctx_t       *p_intc;
    intc_brdspec_t   *p_brdspec;

    if ( !devo || !brdspec ) {
        PDEBUG("Error! Invalid input parameters\n");
        INTC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_intc = devman_device_private(devo);
    if ( !p_intc ) {
        PDEBUG("NULL context area\n");
        INTC_ERROR_RETURN(CCL_FAIL);
    }
    p_brdspec = (intc_brdspec_t *)brdspec;
    /* back reference the device manager object handle */    
    p_intc->devo = devo;    
    p_intc->is_master = (p_brdspec->dev_id == eDEVBRD_INTC_DEV_ARTIX_0) ?
                        CCL_TRUE : CCL_FALSE;  
    /* save the board specific info */
    memcpy(&p_intc->brdspec, p_brdspec, sizeof(intc_brdspec_t));

#if 0
    /* init the device */
    _ret = _intc_init(p_intc);
    if (_ret) {
        PDEBUG("_intc_reset error\n");
        INTC_ERROR_RETURN(_ret);
    }

    /* start the device */
    _ret = _intc_start(p_intc, p_brdspec->op_mode);
    if (_ret) {
        PDEBUG("intc_start error\n");
        INTC_ERROR_RETURN(_ret);
    }
#endif
    _p_intc[p_brdspec->dev_id] = p_intc;

    return CCL_OK;
}

/* Device instance hw-specific deinitializations 
 */
static ccl_err_t _fintc_cut(void *devo)
{
    intc_ctx_t  *p_intc;
    if ( !devo ) {
        PDEBUG("Error! Invalid input parameters\n");
        INTC_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_intc = devman_device_private(devo);
    if ( !p_intc ) {
        PDEBUG("NULL context area\n");
        INTC_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

/* device type description structure */
static device_driver_t _intc_driver = {
    .name = "intc",
    .f_add = _fintc_add,
    .f_cut = _fintc_cut,
    .attrs = _attrs,
    .attrs_num = sizeof(_attrs)/sizeof(_attrs[0]),
    .cmds = _cmds,
    .cmds_num = sizeof(_cmds)/sizeof(_cmds[0]),
    .priv_size = sizeof(intc_ctx_t)
}; 

/* Initialize INTC device type
 */
ccl_err_t devintc_init(void)
{
    /* register device type */
    _rc = devman_driver_register(&_intc_driver);
    if ( _rc ) {
        PDEBUG("Error! Failed to register driver\n");
        INTC_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}

/* Deinitialize INTC device type
 */
ccl_err_t devintc_fini(void)
{
    /* unregister device type */
    _rc = devman_driver_unregister(&_intc_driver);
    if ( _rc ) {
        PDEBUG("Error! Failed to unregister driver\n");
        INTC_ERROR_RETURN(CCL_FAIL);
    }
    return CCL_OK;
}
