/********************************************************************************/
/**
 * @file m41st87w.c
 * @brief M41ST87W Real Time Clock (RTC) device manager driver 
 * @copyright 2018 CELARE. All rights reserved.
 */
/********************************************************************************/
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <semaphore.h>
#include <linux/limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "ccl_types.h"
#include "ccl_util.h"
#include "ccl_logger.h"
#include "devman.h"
#include "devboard.h"
#include "i2ccore.h"       
#include "m41st87w.h"

//#define M41ST87W_DEBUG
/* printing/error-returning macros */
#ifdef M41ST87W_DEBUG
    #define PDEBUG(fmt, args...) \
            ccl_syslog_err("M41ST87W: %s[%d]: " fmt, __FUNCTION__, __LINE__, ## args)
#else
    #define PDEBUG(fmt, args...)
#endif
#define M41ST87W_ERROR_RETURN(err) do { \
   ccl_syslog_err("Error! %s[%d], error=%i\n", __FUNCTION__, __LINE__, (err)); \
   return err; \
} while (0)

/* m41st87w registers offsets */
#define M41ST87W_10S_100S_SECS            0x00 /**< 10s/100s seconds */
#define M41ST87W_SECS                     0x01 /**< Seconds */
#define M41ST87W_MINUTES                  0x02 /**< Minutes */
#define M41ST87W_CENTURY_HOURS            0x03 /**< Century/Hours */
#define M41ST87W_DAY                      0x04 /**< Day */
#define M41ST87W_DATE                     0x05 /**< Date */
#define M41ST87W_MONTH                    0x06 /**< Month */
#define M41ST87W_YEAR                     0x07 /**< Year */
#define M41ST87W_CONTROL                  0x08 /**< Control */
#define M41ST87W_WATCGDOG                 0x09 /**< Watchdog */
#define M41ST87W_AL_MONTH                 0x0A /**< Alarm month */
#define M41ST87W_AL_DATE                  0x0B /**< Alarm date */
#define M41ST87W_AL_HOUR                  0x0C /**< Alarm hour */
#define M41ST87W_AL_MIN                   0x0D /**< Alarm minutes */
#define M41ST87W_AL_SEC                   0x0E /**< Alarm seconds */
#define M41ST87W_FLAGS                    0x0F /**< Flags */ 
#define M41ST87W_SQW                      0x13 /**< Square wave frequency */
#define M41ST87W_TAMPER_1                 0x14 /**< Tamper configuration and statuses */
#define M41ST87W_TAMPER_2                 0x15 /**< Tamper configuration and statuses */
#define M41ST87W_SERIAL_NUM               0x16 /**< Serial number */

#define M41ST87W_USER_RAM_START_OFFSET    0x20
#define M41ST87W_USER_RAM_END_OFFSET      0x9F
#define M41ST87W_USER_RAM_SIZE            0x80 /**< 128 bytes */

#define M41ST87W_SECS_ST_MASK             0x80 /**< Stop Bit Mask */ 
#define M41ST87W_AL_HOUR_HT_MASK          0x40 /**< Halt Update Mask */

static ccl_err_t _ret;                     

/* the m41st87w internal control structure */
typedef struct m41st87w_ctx_t {
    void                    *devo;
    gen_i2c_dev_brdspec_t   brdspec;
} m41st87w_ctx_t;

/* m41st87w register read */
static ccl_err_t _m41st87w_reg_read(void *p_priv, b_u32 offset, 
                                     b_u32 offset_sz, b_u32 idx, 
                                     b_u8 *p_value, b_u32 length)
{
    m41st87w_ctx_t   *p_m41st87w;
    i2c_route_t      i2c_route;

    if ( !p_priv || offset > M41ST87W_USER_RAM_END_OFFSET || 
         !p_value || idx ) {
        PDEBUG("Error! Invalid input parameters\n");
        M41ST87W_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    PDEBUG("123$=> offset=0x%03x, value=0x%x, length=%d\n", 
           offset, p_value, length);

    p_m41st87w = (m41st87w_ctx_t *)p_priv;     
    memcpy(&i2c_route, &p_m41st87w->brdspec.i2c.bus_id, sizeof(i2c_route_t));

    _ret = devi2ccore_read_buff(&i2c_route, 
                                p_m41st87w->brdspec.extra_mux_pin, 
                                offset, p_m41st87w->brdspec.addr_width, 
                                p_value, length);
    if ( _ret ) {
        PDEBUG("Error! Failed to read over i2c bus\n");
        M41ST87W_ERROR_RETURN(_ret);
    }

    return _ret;
}

/* m41st87w register write */
static ccl_err_t _m41st87w_reg_write(void *p_priv, b_u32 offset, 
                                      b_u32 offset_sz, b_u32 idx, 
                                      b_u8 *p_value, b_u32 length)
{
    m41st87w_ctx_t   *p_m41st87w;
    i2c_route_t      i2c_route;

    if ( !p_priv || offset > M41ST87W_USER_RAM_END_OFFSET || 
         !p_value || idx ) {
        PDEBUG("Error! Invalid input parameters\n");
        M41ST87W_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    PDEBUG("123$=> offset=0x%03x, value=0x%x, length=%d\n", 
           offset, *(b_u32 *)p_value, length);

    p_m41st87w = (m41st87w_ctx_t *)p_priv;     
    memcpy(&i2c_route, &p_m41st87w->brdspec.i2c.bus_id, sizeof(i2c_route_t));

    _ret = devi2ccore_write_buff(&i2c_route, 
                                 p_m41st87w->brdspec.extra_mux_pin, 
                                 offset, p_m41st87w->brdspec.addr_width, 
                                 p_value, length);
    if ( _ret ) {
        PDEBUG("Error! Failed to write over i2c bus\n");
        M41ST87W_ERROR_RETURN(_ret);
    }

    return _ret;
}

static ccl_err_t _m41st87w_read_measurement_data(void *priv, b_u8 *buf)
{
    m41st87w_ctx_t   *p_m41st87w;
    rtc_stats_t      *rtc_stats;

    if ( !priv || !buf ) {
        PDEBUG("Error! Invalid input parameters\n");
        M41ST87W_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_m41st87w = (m41st87w_ctx_t *)priv;     
    rtc_stats = (rtc_stats_t *)buf;

    return CCL_OK;
}

static ccl_err_t _m41st87w_init(void *p_priv)
{
    m41st87w_ctx_t   *p_m41st87w;
    b_u8             regval;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        M41ST87W_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_m41st87w = (m41st87w_ctx_t *)p_priv;

    _ret = _m41st87w_reg_read(p_m41st87w, M41ST87W_SECS, 0, 0, &regval, 1);
    if ( _ret ) {
        PDEBUG("Error! _m41st87w_reg_read failed\n");
        M41ST87W_ERROR_RETURN(_ret);
    }
    regval &= ~M41ST87W_SECS_ST_MASK;
    _ret = _m41st87w_reg_write(p_m41st87w, M41ST87W_SECS, 0, 0, &regval, 1);
    if ( _ret ) {
        PDEBUG("Error! _m41st87w_reg_write failed\n");
        M41ST87W_ERROR_RETURN(_ret);
    }
    _ret = _m41st87w_reg_read(p_m41st87w, M41ST87W_AL_HOUR, 0, 0, &regval, 1);
    if ( _ret ) {
        PDEBUG("Error! _m41st87w_reg_read failed\n");
        M41ST87W_ERROR_RETURN(_ret);
    }
    regval &= ~M41ST87W_AL_HOUR_HT_MASK;
    _ret = _m41st87w_reg_write(p_m41st87w, M41ST87W_AL_HOUR, 0, 0, &regval, 1);
    if ( _ret ) {
        PDEBUG("Error! _m41st87w_reg_write failed\n");
        M41ST87W_ERROR_RETURN(_ret);
    }

    return CCL_OK;
}

static ccl_err_t _m41st87w_configure_test(void *p_priv)
{
    m41st87w_ctx_t   *p_m41st87w;

    if ( !p_priv ) {
        PDEBUG("Error! Invalid input parameters\n");
        M41ST87W_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_m41st87w = (m41st87w_ctx_t *)p_priv;

    return CCL_OK;
}

static ccl_err_t _m41st87w_run_test(void *p_priv, b_u8 *buf, b_u32 size)
{
    m41st87w_ctx_t   *p_m41st87w;
    b_u8     hours;
    b_u8     minutes;
    b_u8     secs;
    b_i32    i;

    if ( !p_priv || !buf ) {
        PDEBUG("Error! Invalid input parameters\n");
        M41ST87W_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    p_m41st87w = (m41st87w_ctx_t *)p_priv;

    _ret = _m41st87w_reg_read(p_m41st87w, M41ST87W_CENTURY_HOURS, 0, 0, &hours, 1);
    _ret |= _m41st87w_reg_read(p_m41st87w, M41ST87W_MINUTES, 0, 0, &minutes, 1);
    _ret |= _m41st87w_reg_read(p_m41st87w, M41ST87W_SECS, 0, 0, &secs, 1);
    if (_ret) 
        M41ST87W_ERROR_RETURN(_ret);

    ccl_syslog_err("hours: 0x%02x, minutes: 0x%02x, seconds: 0x%02x\n", 
           hours, minutes, secs);

    if (size == sizeof(rtc_stats_t)) {
        rtc_stats_t *stats = (rtc_stats_t *)buf;
        stats->hours = hours;
        stats->minutes = minutes;
        stats->seconds = secs;
    }

    return CCL_OK;
}

ccl_err_t rtc_attr_read(void *p_priv, b_u32 attrid, 
                         b_u32 offset, b_u32 offset_sz, 
                         b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || offset > M41ST87W_USER_RAM_END_OFFSET || 
         !p_value || idx ) {
        PDEBUG("Error! Invalid input parameters\n");
        M41ST87W_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch ( attrid ) {
    case eM41ST87W_TENS:
    case eM41ST87W_SECS:
    case eM41ST87W_MINUTES:
    case eM41ST87W_CENT_HOURS:
    case eM41ST87W_DAY:
    case eM41ST87W_DATE:
    case eM41ST87W_MONTH:
    case eM41ST87W_YEAR:
    case eM41ST87W_CTRL:
    case eM41ST87W_WATCHDOG:
    case eM41ST87W_AL_MONTH:
    case eM41ST87W_AL_DATE:
    case eM41ST87W_AL_MIN:
    case eM41ST87W_AL_SEC:
    case eM41ST87W_FLAGS:
    case eM41ST87W_SQW:
    case eM41ST87W_TAMPER_1:
    case eM41ST87W_TAMPER_2:
    case eM41ST87W_TAMPER_3:
    case eM41ST87W_BUFFER:
        _ret = _m41st87w_reg_read(p_priv, offset, offset_sz, idx, p_value, size);
        break;
    case eM41ST87W_MEASUREMENT_DATA:
        _ret = _m41st87w_read_measurement_data(p_priv, p_value);
        break;
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK) {
        PDEBUG("Failure in attribute id: %d\n", attrid);
    }

    return _ret;
}

ccl_err_t rtc_attr_write(void *p_priv, b_u32 attrid, 
                         b_u32 offset, b_u32 offset_sz, 
                         b_u32 idx, b_u8 *p_value, b_u32 size)
{
    if ( !p_priv || offset > M41ST87W_USER_RAM_END_OFFSET || 
         !p_value || idx ) {
        PDEBUG("Error! Invalid input parameters\n");
        M41ST87W_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    switch ( attrid ) {
    case eM41ST87W_TENS:
    case eM41ST87W_SECS:
    case eM41ST87W_MINUTES:
    case eM41ST87W_CENT_HOURS:
    case eM41ST87W_DAY:
    case eM41ST87W_DATE:
    case eM41ST87W_MONTH:
    case eM41ST87W_YEAR:
    case eM41ST87W_CTRL:
    case eM41ST87W_WATCHDOG:
    case eM41ST87W_AL_MONTH:
    case eM41ST87W_AL_DATE:
    case eM41ST87W_AL_MIN:
    case eM41ST87W_AL_SEC:
    case eM41ST87W_FLAGS:
    case eM41ST87W_SQW:
    case eM41ST87W_TAMPER_1:
    case eM41ST87W_TAMPER_2:
    case eM41ST87W_TAMPER_3:
    case eM41ST87W_BUFFER:
        _ret = _m41st87w_reg_write(p_priv, offset, offset_sz, idx, p_value, size);
        break;
    case eM41ST87W_INIT:
        _ret = _m41st87w_init(p_priv);
        break;
    case eM41ST87W_CONFIGURE_TEST:
        _ret = _m41st87w_configure_test(p_priv);
        break;
    case eM41ST87W_RUN_TEST:
        _ret = _m41st87w_run_test(p_priv, p_value, size);
        break;
    default:
        PDEBUG("invalid attribute id: %d\n", attrid);
        _ret = CCL_BAD_PARAM_ERR;
    }
    if (_ret != CCL_OK) {
        PDEBUG("Failure in attribute id: %d\n", attrid);
    }

    return _ret;
}

/* m41st87w attributes definition */
static device_attr_t _attrs[] = {
    {
        .name = "i2c_bus", .id = eM41ST87W_I2C_BUS, .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset = offsetof(struct m41st87w_ctx_t, brdspec.i2c.bus_id),
        .format = "%d"
    },
    {
        .name = "i2c_addr", .id = eM41ST87W_I2C_ADDR, .type = eDEVMAN_ATTR_HALF, .maxnum = 1, 
        .offset = offsetof(struct m41st87w_ctx_t, brdspec.i2c.dev_i2ca),
        .format = "0x%03x"
    },
    {
        .name = "name", 
        .type = eDEVMAN_ATTR_STRING, .maxnum = 1, 
        .offset = offsetof(struct m41st87w_ctx_t, brdspec.name),
        .size = DEVMAN_DEVNAME_LEN,
        .format = "%s"
    },
    {
        .name = "tens", .id = eM41ST87W_TENS, .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset = M41ST87W_10S_100S_SECS, .offset_sz = sizeof(b_u8)
    },
    {
        .name = "secs", .id = eM41ST87W_SECS, .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset = M41ST87W_SECS, .offset_sz = sizeof(b_u8)
    },
    {
        .name = "minutes", .id = eM41ST87W_MINUTES, .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset = M41ST87W_MINUTES, .offset_sz = sizeof(b_u8)
    },
    {
        .name = "cent_hours", .id = eM41ST87W_CENT_HOURS, .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset = M41ST87W_CENTURY_HOURS, .offset_sz = sizeof(b_u8)        
    },
    {
        .name = "day", .id = eM41ST87W_DAY, .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset = M41ST87W_DAY, .offset_sz = sizeof(b_u8)
    },
    {
        .name = "date", .id = eM41ST87W_DATE, .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset = M41ST87W_DATE, .offset_sz = sizeof(b_u8)
    },
    {
        .name = "month", .id = eM41ST87W_MONTH, .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset = M41ST87W_MONTH, .offset_sz = sizeof(b_u8)
    },
    {
        .name = "year", .id = eM41ST87W_YEAR, .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset = M41ST87W_YEAR, .offset_sz = sizeof(b_u8)
    },
    {
        .name = "ctrl", .id = eM41ST87W_CTRL, .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset = M41ST87W_CONTROL, .offset_sz = sizeof(b_u8)
    },
    {
        .name = "watchdog", .id = eM41ST87W_WATCHDOG, .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset = M41ST87W_WATCGDOG, .offset_sz = sizeof(b_u8)
    },
    {
        .name = "al_month", .id = eM41ST87W_AL_MONTH, .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset = M41ST87W_AL_MONTH, .offset_sz = sizeof(b_u8)
    },
    {
        .name = "al_date", .id = eM41ST87W_AL_DATE, .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset = M41ST87W_AL_DATE, .offset_sz = sizeof(b_u8)
    },
    {
        .name = "al_hour", .id = eM41ST87W_AL_MIN, .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset = M41ST87W_AL_HOUR, .offset_sz = sizeof(b_u8)
    },
    {
        .name = "al_min", .id = eM41ST87W_AL_SEC, .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset = M41ST87W_AL_MIN, .offset_sz = sizeof(b_u8)
    },
    {
        .name = "al_sec", .id = eM41ST87W_FLAGS, .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset = M41ST87W_AL_SEC, .offset_sz = sizeof(b_u8)
    },
    {
        .name = "flags", .id = eM41ST87W_SQW, .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset = M41ST87W_FLAGS, .offset_sz = sizeof(b_u8)
    },
    {
        .name = "sqw", .id = eM41ST87W_TAMPER_1, .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset = M41ST87W_SQW, .offset_sz = sizeof(b_u8)
    },
    {
        .name = "tamper_1", .id = eM41ST87W_TAMPER_2, .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset = M41ST87W_TAMPER_1, .offset_sz = sizeof(b_u8)
    },
    {
        .name = "tamper_2", .id = eM41ST87W_TAMPER_3, .type = eDEVMAN_ATTR_BYTE, .maxnum = 1, 
        .offset = M41ST87W_TAMPER_2, .offset_sz = sizeof(b_u8)
    },
    {
        .name = "serial_num", .id = eM41ST87W_SERIAL_NUM, .type = eDEVMAN_ATTR_DOUBLE, .maxnum = 1, 
        .offset = M41ST87W_SERIAL_NUM, .offset_sz = sizeof(b_u8)
    },
    {
        .name = "buffer", .id = eM41ST87W_BUFFER, .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, 
        .offset = 0, .offset_sz = sizeof(b_u8)
    },
    {
        .name = "measurement_data", .id = eM41ST87W_MEASUREMENT_DATA, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, .size = sizeof(rtc_stats_t), 
        .offset_sz = sizeof(b_u8),
        .flags = eDEVMAN_ATTRFLAG_RO
    },
    {
        .name = "init", .id = eM41ST87W_INIT, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "config_test", .id = eM41ST87W_CONFIGURE_TEST, 
        .type = eDEVMAN_ATTR_WORD, .maxnum = 1, 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
    {
        .name = "run_test", .id = eM41ST87W_RUN_TEST, 
        .type = eDEVMAN_ATTR_BUFF, .maxnum = 1, 
        .size = sizeof(rtc_stats_t), 
        .offset_sz = sizeof(b_u32),
        .flags = eDEVMAN_ATTRFLAG_WO
    },
};

/* Device instance hw-specific initializations 
 */
static ccl_err_t _fm41st87w_add(void *devo, void *brdspec)
{
    m41st87w_ctx_t           *p_m41st87w;
    gen_i2c_dev_brdspec_t    *p_brdspec;

    if ( !devo || !brdspec ) {
        PDEBUG("Error! Invalid input parameters\n");
        M41ST87W_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }
    /* get context area/board specific info*/
    p_m41st87w = devman_device_private(devo);
    if ( !p_m41st87w ) {
        PDEBUG("NULL context area\n");
        M41ST87W_ERROR_RETURN(CCL_FAIL);
    }
    p_brdspec = (gen_i2c_dev_brdspec_t *)brdspec;

    PDEBUG("123$=> p_brdspec->i2c.bus_id=%d, dev_i2ca=0x%x\n", 
           p_brdspec->i2c.bus_id, p_brdspec->i2c.dev_i2ca);

    p_m41st87w->devo = devo;    
    memcpy(&p_m41st87w->brdspec, p_brdspec, sizeof(gen_i2c_dev_brdspec_t));

    return CCL_OK;
}


/* Device instance hw-specific deinitializations 
 */
static ccl_err_t _fm41st87w_cut(void *devo)
{
    if ( !devo ) {
        PDEBUG("Error! Invalid input parameters\n");
        M41ST87W_ERROR_RETURN(CCL_BAD_PARAM_ERR);
    }

    return CCL_OK;
}

/* device type description structure */
static device_driver_t _m41st87w_driver = {
    .name = "rtc",
    .f_add = _fm41st87w_add,
    .f_cut = _fm41st87w_cut,
    .attrs_num = sizeof(_attrs)/sizeof(_attrs[0]),
                 .attrs = _attrs,
    .priv_size = sizeof(m41st87w_ctx_t)
}; 

/* Initialize M41ST87W device type
 */
ccl_err_t devm41st87w_init(void)
{
    /* register device type */
    _ret = devman_driver_register(&_m41st87w_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to register driver\n");
        M41ST87W_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

/* Deinitialize M41ST87W device type
 */
ccl_err_t devm41st87w_fini(void)
{
    /* unregister device type */
    _ret = devman_driver_unregister(&_m41st87w_driver);
    if ( _ret ) {
        PDEBUG("Error! Failed to unregister driver\n");
        M41ST87W_ERROR_RETURN(CCL_FAIL);
    }

    return CCL_OK;
}

