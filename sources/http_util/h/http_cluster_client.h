#ifndef HTTP_CLUSTER_CLIENT_H
#define HTTP_CLUSTER_CLIENT_H

#include "http_cluster_node.h"
#include "http_cluster_sched_strat.h"

#include <pthread.h>

#include "ccl_types.h"

typedef struct http_cluster_client_s http_cluster_client_t;

typedef ccl_err_t (*http_cluster_reconnect_fn_t)(http_cluster_client_t *cluster);
typedef void (*destroy_http_cluster_fn_t)  (http_cluster_client_t *cluster);

struct http_cluster_client_s {
    http_cluster_node_conn_t *connections;
    pthread_t *reconnectors;
    size_t nodes_num;
    http_cluster_sched_t *sched_strat;
    int _failed;
    time_t conn_timeout;

    pthread_mutex_t failed_flag_lock;
    pthread_cond_t failed_flag_cond;
};

ccl_err_t init_http_cluster(http_cluster_client_t *cluster,
                           http_cluster_node_t *nodes,
                           size_t nodes_num,
                           http_cluster_sched_t *sched_strat,
                           time_t conn_timeout);

ccl_err_t http_cluster_send_post_req(http_cluster_client_t *cluster,
                                    const char *path, const char *body,
                                    size_t length, const char *encoding_type,
                                    const char *content_type);

ccl_err_t http_cluster_send_multipart_post_req(http_cluster_client_t *cluster,
                                              const char *path,
                                              const char **form_names,
                                              const char **buffer_names,
                                              const char **bodies,
                                              const char **content_types,
                                              const size_t *bodies_sizes,
                                              size_t const bodies_number);

ccl_err_t http_cluster_reconnect(http_cluster_client_t *cluster);

void deinit_http_cluster(http_cluster_client_t *cluster);

int is_http_cluster_failed(const http_cluster_client_t *cluster);

#endif //HTTP_CLUSTER_CLIENT_H
