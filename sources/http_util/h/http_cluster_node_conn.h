#ifndef HTTP_CLUSTER_NODE_CONN_H
#define HTTP_CLUSTER_NODE_CONN_H

#include <curl/curl.h>
#include <stdint.h>
#include "ccl_types.h"


#define HTTP_CLUSTER_NODE_SSL_EN      ( 1 << 0 )

typedef struct http_cluster_node_conn_s http_cluster_node_conn_t;

struct http_cluster_node_conn_s {

    CURL *_curl;

    const char *_host_name;
    uint16_t _port;
    int _ssl_enabled;
    time_t _conn_timeout;
    const char *_conn_info;

    http_cluster_node_conn_t *next;

};

ccl_err_t create_http_cluster_node_conn(http_cluster_node_conn_t *conn,
                                       const char *_host_name,
                                       const uint16_t port,
                                       const int ssl_enabled,
                                       const time_t conn_timeout);

/**
 * @brief Sending POST request to HTTP cluster server using libcurl.
 *
 * @param[in] node_conn
 * @param[in] path
 * @param[in] body
 * @param[in] length - if length == 0, strlen(body) will be used as length
 * @param[in] content_encoding
 * @param[in] content_type
 * @return CCL_OK on success
 */
ccl_err_t http_cluster_node_send_post(http_cluster_node_conn_t *node_conn,
                                     const char *path, const char *body,
                                     size_t length, const char *content_encoding,
                                     const char *content_type);

/**
 * @brief Sending multipart/formdata POST request to HTTP cluster server using libcurl.
 *
 * @param[in] node_conn
 * @param[in] path
 * @param[in] names
 * @param[in] bodies
 * @param[in] content_types
 * @param[in] bodies_sizes
 * @param[in] bodies_number
 * @return CCL_OK on success
 */
ccl_err_t http_cluster_node_send_multipart_post(http_cluster_node_conn_t *node_conn,
                                               const char *path,
                                               const char **form_names, const char **buffer_names,
                                               const char **bodies,
                                               const char **content_types,
                                               const size_t *bodies_sizes,
                                               size_t const bodies_number);

ccl_err_t http_cluster_node_reconnect(http_cluster_node_conn_t *node_conn);

void destroy_http_cluster_node_conn(http_cluster_node_conn_t *node_conn);

const char *http_cluster_conn_get_info(http_cluster_node_conn_t *node_conn);

#endif //HTTP_CLUSTER_NODE_CONN_H
