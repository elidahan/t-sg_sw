#ifndef HTTP_CLUSTER_NODE_H
#define HTTP_CLUSTER_NODE_H

#include <stdint.h>
#include <time.h>

#include "cJSON.h"

#include "http_cluster_node_conn.h"

typedef struct {
    const char *host_name;
    uint16_t port;
    int ssl_enabled;
} http_cluster_node_t;

void init_http_cluster_node(http_cluster_node_t *node, const char *host_name, uint16_t port, int ssl_enabled);

ccl_err_t connect_to_http_cluster_node(http_cluster_node_t *node, http_cluster_node_conn_t *node_conn, time_t conn_timeout);

void clone_http_cluster_node(http_cluster_node_t *dst, const http_cluster_node_t *orig);

void destroy_http_cluster_node(http_cluster_node_t *node);

int init_http_cluster_node_from_json(http_cluster_node_t *node, cJSON *node_json, int ssl_enabled);

#endif //HTTP_CLUSTER_NODE_H
