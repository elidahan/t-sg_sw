#ifndef HTTP_CLUSTER_ACTIVE_NODE_SCHED_H
#define HTTP_CLUSTER_ACTIVE_NODE_SCHED_H

#include "http_cluster_sched_strat.h"

http_cluster_sched_t *create_active_node_http_cluster_sched();

#endif //HTTP_CLUSTER_ACTIVE_NODE_SCHED_H
