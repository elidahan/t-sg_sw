#ifndef HTTP_CLUSTER_SCHED_STRAT_H
#define HTTP_CLUSTER_SCHED_STRAT_H

#include "http_cluster_node.h"

#include "ccl_types.h"

#include <pthread.h>

typedef struct http_cluster_sched_s http_cluster_sched_t;

typedef http_cluster_node_conn_t *(*get_next_node_conn_fn_t)(http_cluster_sched_t *sched);
typedef void (*cluster_sched_add_alive_conn_fn_t)(http_cluster_sched_t *sched,
                                                  http_cluster_node_conn_t *conn);
typedef void (*cluster_sched_steal_conn_fn_t)(http_cluster_sched_t *sched,
                                              http_cluster_node_conn_t *conn);
typedef int (*cluster_sched_has_alive_conn_fn_t) (http_cluster_sched_t *sched);
typedef void (*destroy_http_cluster_sched_fn_t)  (http_cluster_sched_t *sched);


struct http_cluster_sched_s {
    get_next_node_conn_fn_t           _get_next_node;
    cluster_sched_has_alive_conn_fn_t _has_alive_conn;
    cluster_sched_add_alive_conn_fn_t _add_alive_conn;
    cluster_sched_steal_conn_fn_t     _steal_conn;
    destroy_http_cluster_sched_fn_t   _destructor;
    pthread_mutex_t sched_lock;
};

void init_http_cluster_sched(http_cluster_sched_t *sched,
                             get_next_node_conn_fn_t select_node_fn_impl,
                             cluster_sched_add_alive_conn_fn_t add_alive_conn_impl,
                             cluster_sched_has_alive_conn_fn_t has_alive_conn_impl,
                             cluster_sched_steal_conn_fn_t steal_conn_impl,
                             destroy_http_cluster_sched_fn_t destructor_fn_impl);

http_cluster_node_conn_t *http_cluster_sched_get_next_node_conn(http_cluster_sched_t *sched);

void http_cluster_sched_add_alive_conn(http_cluster_sched_t *sched,
                                       http_cluster_node_conn_t *conn);

void http_cluster_sched_steal_conn(http_cluster_sched_t *sched,
                                   http_cluster_node_conn_t *conn);

int http_cluster_sched_has_alive_conn(http_cluster_sched_t *sched);

void destroy_http_cluster_sched(http_cluster_sched_t *sched);

#endif //HTTP_CLUSTER_SCHED_STRAT_H
