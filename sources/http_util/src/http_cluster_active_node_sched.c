#include "http_cluster_active_node_sched.h"

#include "ccl_util.h"
#include "ccl_logger.h"

#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <assert.h>

extern ccl_logger_t logger;

typedef struct {
    http_cluster_sched_t parent;

    http_cluster_node_conn_t *active_node_conn;
    http_cluster_node_conn_t *backup_nodes;
} http_cluster_active_node_sched_t;

#define LIST_PREPEND_ITEM(head, item) \
    item->next = head; \
    head = item; \

#define LIST_APPEND_ITEM(head, item, type) \
    type node = head; \
    type prev = NULL; \
    while (node) { \
        prev = node; \
        node = node->next; \
    } \
    prev->next = item;

#define LIST_CLEANUP(head, cleanup_fn, type) \
    while (head) { \
        type tmp = head->next; \
        cleanup_fn(head); \
        head = tmp; \
    } \

#define LIST_POP_ITEM(head, item) \
    if (!head) { \
        item = NULL; \
    } else { \
        item = head; \
        head = head->next; \
    }\

#define LIST_REMOVE_ITEM(head, item, type) \
    assert(item); \
    type node = head; \
    if (head == item) { \
        head = head->next; \
    } \
    while (node) { \
        if (node->next == item) { \
            node->next = item->next; \
        } \
        node = node->next; \
    } \

static void _active_node_sched_add_node(http_cluster_sched_t *sched, http_cluster_node_conn_t *node) {
    http_cluster_active_node_sched_t *active_sched = (http_cluster_active_node_sched_t *)sched;
    if (!active_sched->active_node_conn) {
        active_sched->active_node_conn = node;
    } else {
        LIST_PREPEND_ITEM(active_sched->backup_nodes, node);
    }
}

static http_cluster_node_conn_t *_active_node_sched_get_next(http_cluster_sched_t *sched) {
    http_cluster_active_node_sched_t *active_sched = (http_cluster_active_node_sched_t *)sched;
    if (!active_sched->active_node_conn) {
        LIST_POP_ITEM(active_sched->backup_nodes, active_sched->active_node_conn);
    }
    return active_sched->active_node_conn;
}

static int _active_node_sched_has_alive_nodes(http_cluster_sched_t *sched) {
    http_cluster_active_node_sched_t *active_sched = (http_cluster_active_node_sched_t *)sched;
    return active_sched->active_node_conn != NULL;
}

static void _active_node_sched_steal_node(http_cluster_sched_t *sched, http_cluster_node_conn_t *node) {
    http_cluster_active_node_sched_t *active_sched = (http_cluster_active_node_sched_t *)sched;
    if (node == active_sched->active_node_conn) {
        LIST_POP_ITEM(active_sched->backup_nodes, active_sched->active_node_conn);
    } else {
        LIST_REMOVE_ITEM(active_sched->backup_nodes, active_sched->active_node_conn, http_cluster_node_conn_t *);
    }
}

static void _active_node_sched_destructor(http_cluster_sched_t *sched) {}

http_cluster_sched_t *create_active_node_http_cluster_sched() {
    http_cluster_active_node_sched_t *sched = calloc(1, sizeof(*sched));
    MEMORY_ALLOCATION_FAILURE_CHECK(sched, &logger, "calloc");

    init_http_cluster_sched(&sched->parent, _active_node_sched_get_next,
                            _active_node_sched_add_node,
                            _active_node_sched_has_alive_nodes,
                            _active_node_sched_steal_node,
                            _active_node_sched_destructor);
    return &sched->parent;
}
