#include "http_cluster_node.h"

#include "ccl_util.h"
#include "ccl_logger.h"

#include <stdlib.h>

extern ccl_logger_t logger;

void init_http_cluster_node(http_cluster_node_t *node, const char *host_name, uint16_t port, int ssl_enabled)
{
    memset(node, 0, sizeof(*node));
    node->port = port;
    node->host_name = strdup(host_name);
    node->ssl_enabled = ssl_enabled;
}

ccl_err_t connect_to_http_cluster_node(http_cluster_node_t *node, http_cluster_node_conn_t * node_conn, time_t conn_timeout)
{
    return create_http_cluster_node_conn(node_conn,
                                         node->host_name,
                                         node->port,
                                         node->ssl_enabled,
                                         conn_timeout);
}

void destroy_http_cluster_node(http_cluster_node_t *node)
{
    free((char*)node->host_name);
}


void clone_http_cluster_node(http_cluster_node_t *dst, const http_cluster_node_t *orig) {
    init_http_cluster_node(dst, orig->host_name, orig->port, orig->ssl_enabled);
}


int init_http_cluster_node_from_json(http_cluster_node_t *node, cJSON *node_json, int ssl_enabled) {
    cJSON *hostname = cJSON_GetObjectItem(node_json, "hostname");
    if (!hostname || hostname->type != cJSON_String) {
        ccl_syslog_err("Panic, failed to find \"hostname\" field in es cluster node config\n");
        return -1;
    }

    cJSON *port = cJSON_GetObjectItem(node_json, "port");
    if (!port || port->type != cJSON_Number) {
        ccl_syslog_err("Panic, failed to find \"port\" field in es cluster node config\n");
        return -1;
    }

    init_http_cluster_node(node, hostname->valuestring, port->valueint, ssl_enabled);

    return 0;
}
