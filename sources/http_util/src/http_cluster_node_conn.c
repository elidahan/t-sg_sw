#include "http_cluster_node_conn.h"

#include "ccl_util.h"
#include "ccl_logger.h"

#include <signal.h>
#include <stdlib.h>

#include <curl/curl.h>

extern ccl_logger_t logger;

#define CONTENT_ENCODING_BUFF_SIZE 50
#define CONTENT_TYPE_BUFF_SIZE 50

#define PRINTL(fmt, args...) fprintf(stderr, fmt, ## args) 

/**
 * @brief Curl callback to silently drop HTTP response
 *
 * @param[in]   ptr
 * @param[in]  	size
 * @param[in]  	nmemb
 * @param[in]   userdata
 * @return size * nmemb
 */
static size_t _curl_write_callback_stub(char *ptr, size_t size, size_t nmemb, void *userdata) {
    return size * nmemb;
}

#define _http_cluster_node_send_post_basic(node_conn, path, ret, prepare_headers_fn, args...) \
    sigset_t set; \
    sigemptyset(&set); \
    sigaddset(&set, SIGALRM); \
    pthread_sigmask(SIG_BLOCK, &set, NULL); \
\
    char *request_url = NULL; \
\
    CURLcode rcode; \
    struct curl_slist *headers = prepare_headers_fn(node_conn, args); \
\
    curl_easy_setopt(node_conn->_curl, CURLOPT_WRITEFUNCTION, _curl_write_callback_stub); \
    if (node_conn->_ssl_enabled & HTTP_CLUSTER_NODE_SSL_EN){ \
        asprintf(&request_url, "https://%s:%hu/%s", node_conn->_host_name, node_conn->_port, path); \
        curl_easy_setopt(node_conn->_curl, CURLOPT_SSL_VERIFYPEER, 0L); \
        curl_easy_setopt(node_conn->_curl, CURLOPT_SSL_VERIFYHOST, 0L); \
    } else { \
        asprintf(&request_url, "%s:%hu/%s", node_conn->_host_name, node_conn->_port, path); \
    } \
    MEMORY_ALLOCATION_FAILURE_CHECK(request_url, &logger, "asprintf"); \
    curl_easy_setopt(node_conn->_curl, CURLOPT_URL, request_url); \
\
    rcode = curl_easy_perform(node_conn->_curl); \
\
    long http_code = 0; \
    curl_easy_getinfo (node_conn->_curl, CURLINFO_RESPONSE_CODE, &http_code); \
    if (http_code / 100 != 2) \
    { \
        ccl_log_error(&logger, "%s: HTTP request returned non 200 status code %d, URL - %s", \
                  __func__, http_code, request_url); \
    } \
\
    free(request_url); \
    curl_slist_free_all(headers);\
\
    pthread_sigmask(SIG_UNBLOCK, &set, NULL); \
\
    if (rcode != CURLE_OK) { \
        ccl_log_crit_ratelimited(&logger, "%s: Got CURL error %s at path %s", \
                                 __func__, curl_easy_strerror(rcode), path); \
        if (rcode == CURLE_COULDNT_CONNECT) { \
            ret = CCL_CONNECT_FAILED; \
        } else { \
            ret = CCL_FAIL; \
        } \
    } else { \
        ret = CCL_OK; \
    } \

/**
 * @brief Prepare multipart/form-data headers
 *
 * @param[in]   node_conn
 * @param[in]  	names
 * @param[in]  	bodies
 * @param[in]   content_types
 * @param[in]   bodies_sizes
 * @param[in]   bodies_number
 * @return NULL
 * @note CURLOPT_HTTPPOST API is deprecated, but MIME API is not available in centos7
 */
static struct curl_slist *_prepare_curl_multipart_headers(http_cluster_node_conn_t *node_conn,
                                                          const char **form_names,
                                                          const char **buffer_names,
                                                          const char **bodies,
                                                          const char **content_types,
                                                          const size_t *bodies_sizes,
                                                          size_t const bodies_number)
{
    struct curl_httppost* post = NULL;
    struct curl_httppost* last = NULL;

    for (size_t i = 0; i < bodies_number; ++i) {
        curl_formadd(&post, &last,
                     CURLFORM_PTRNAME, form_names[i],
                     CURLFORM_BUFFER, buffer_names[i],
                     CURLFORM_BUFFERPTR, bodies[i],
                     CURLFORM_CONTENTTYPE, content_types[i],
                     CURLFORM_BUFFERLENGTH, bodies_sizes[i],
                     CURLFORM_END);
    }
    curl_easy_setopt(node_conn->_curl, CURLOPT_HTTPPOST, post);
    return NULL;
}

ccl_err_t http_cluster_node_send_multipart_post(http_cluster_node_conn_t *node_conn,
                                               const char *path,
                                               const char **form_names,
                                               const char **buffer_names,
                                               const char **bodies,
                                               const char **content_types,
                                               const size_t *bodies_sizes,
                                               size_t const bodies_number)
{
#ifdef CURL_EASY_PERFORM_PROFILE_ENABLED
    PROFILE_START
#endif

    ccl_err_t ret;
    _http_cluster_node_send_post_basic(node_conn, path, ret,
                                       _prepare_curl_multipart_headers,
                                       form_names, buffer_names, bodies, content_types,
                                       bodies_sizes, bodies_number);

#ifdef CURL_EASY_PERFORM_PROFILE_ENABLED
    char *profile_info_str = NULL;
    asprintf(&profile_info_str, "perform sending %zu bytes in POST body", length);
    PROFILE_END(&logger, profile_info_str);
    free(profile_info_str);
#endif

    return ret;
}

static struct curl_slist *_prepare_curl_post_headers(http_cluster_node_conn_t *node_conn,
                                                     const char *body,
                                                     size_t length,
                                                     const char *encoding_type,
                                                     const char *content_type)
{
    char content_type_buff[CONTENT_TYPE_BUFF_SIZE];
    snprintf(content_type_buff, sizeof(content_type_buff) - 1, "Content-Type: %s", content_type);

    struct curl_slist *headers = NULL;
    headers = curl_slist_append(headers, content_type_buff);

    if (encoding_type) {
        char content_encoding_buff[CONTENT_ENCODING_BUFF_SIZE];
        snprintf(content_encoding_buff, sizeof(content_encoding_buff) - 1, "Content-Encoding: %s", encoding_type);
        curl_slist_append(headers, content_encoding_buff);
    }
//    curl_easy_setopt(node_conn->_curl, CURLOPT_VERBOSE, 1);
    curl_easy_setopt(node_conn->_curl, CURLOPT_HTTPHEADER, headers);

    curl_easy_setopt(node_conn->_curl, CURLOPT_CUSTOMREQUEST, "POST");

    curl_easy_setopt(node_conn->_curl, CURLOPT_POSTFIELDSIZE, length);
    curl_easy_setopt(node_conn->_curl, CURLOPT_POSTFIELDS, body); // data goes here

    return headers;
}

ccl_err_t http_cluster_node_send_post(http_cluster_node_conn_t *node_conn,
                                     const char *path,
                                     const char *body,
                                     size_t length,
                                     const char *encoding_type,
                                     const char *content_type)
{
#ifdef CURL_EASY_PERFORM_PROFILE_ENABLED
    PROFILE_START
#endif

    ccl_err_t ret;
    _http_cluster_node_send_post_basic(node_conn, path, ret, _prepare_curl_post_headers,
                                       body, length, encoding_type, content_type);

#ifdef CURL_EASY_PERFORM_PROFILE_ENABLED
    char *profile_info_str = NULL;
    asprintf(&profile_info_str, "perform sending %zu bytes in POST body", length);
    PROFILE_END(&logger, profile_info_str);
    free(profile_info_str);
#endif

    return ret;
}

static ccl_err_t _connect_http_srv(http_cluster_node_conn_t *node_conn) {

    char *http_srv_url = NULL;


    curl_easy_cleanup(node_conn->_curl);

    node_conn->_curl = curl_easy_init();
    if (!node_conn->_curl) {
        ccl_log_panic(&logger, "%s: curl_easy_init failed, error: %s",
                  __func__, curl_easy_strerror(CURLE_FAILED_INIT));
    }
    curl_easy_setopt(node_conn->_curl, CURLOPT_TCP_KEEPALIVE, 1L);
    /* keep-alive idle time to 120 seconds */
    curl_easy_setopt(node_conn->_curl, CURLOPT_TCP_KEEPIDLE, 120L);
    /* interval time between keep-alive probes: 60 seconds */
    curl_easy_setopt(node_conn->_curl, CURLOPT_TCP_KEEPINTVL, 60L);
    /* complete connection within 10 seconds */
    curl_easy_setopt(node_conn->_curl, CURLOPT_CONNECTTIMEOUT,
                     node_conn->_conn_timeout);
    if (node_conn->_ssl_enabled & HTTP_CLUSTER_NODE_SSL_EN){
        asprintf(&http_srv_url, "https://%s:%u", node_conn->_host_name, node_conn->_port);
        curl_easy_setopt(node_conn->_curl, CURLOPT_SSL_VERIFYPEER, 0L);
        curl_easy_setopt(node_conn->_curl, CURLOPT_SSL_VERIFYHOST, 0L);
    } else {
        asprintf(&http_srv_url, "%s:%u", node_conn->_host_name, node_conn->_port);
    }
    curl_easy_setopt(node_conn->_curl, CURLOPT_URL, http_srv_url);

    ccl_err_t res = CCL_OK;
    CURLcode rcode = curl_easy_perform(node_conn->_curl);
    if ( rcode != CURLE_OK ) {
        ccl_log_error(&logger, "%s: Failed to connect to HTTP server %s, curl return code: %d, error: %s",
                  __func__, http_srv_url, rcode, curl_easy_strerror(rcode));
        res = (rcode == CURLE_COULDNT_CONNECT) ? CCL_CONNECT_FAILED : CCL_FAIL;

    } else {
        ccl_log_info(&logger, "%s: Successfully connected to HTTP server %s", __func__, http_srv_url);
    }

    free(http_srv_url);
    return res;
}

ccl_err_t create_http_cluster_node_conn(http_cluster_node_conn_t *node_conn,
                                       const char *_host_name,
                                       const uint16_t port,
                                       const int ssl_enabled,
                                       const time_t conn_timeout)
{
    memset(node_conn, 0, sizeof(*node_conn));
    node_conn->_host_name = strdup(_host_name);
    node_conn->_port = port;
    node_conn->_ssl_enabled = ssl_enabled;
    node_conn->_conn_timeout = conn_timeout;

    asprintf((char**)&node_conn->_conn_info, "%s:%hu",
             node_conn->_host_name, node_conn->_port);

    return _connect_http_srv(node_conn);
}


void destroy_http_cluster_node_conn(http_cluster_node_conn_t *node_conn)
{
    curl_easy_cleanup(node_conn->_curl);
    free((char*)node_conn->_host_name);
    free((char*)node_conn->_conn_info);
}


ccl_err_t http_cluster_node_reconnect(http_cluster_node_conn_t *node_conn)
{
    return _connect_http_srv(node_conn);
}

const char *http_cluster_conn_get_info(http_cluster_node_conn_t *node_conn) {
    return node_conn->_conn_info;
}
