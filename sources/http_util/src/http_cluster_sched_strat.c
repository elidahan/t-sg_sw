#include "http_cluster_sched_strat.h"
#include "ccl_util.h"
#include "ccl_logger.h"

#include <stdlib.h>
#include <string.h>

extern ccl_logger_t logger;

void init_http_cluster_sched(http_cluster_sched_t *sched,
                             get_next_node_conn_fn_t get_node_fn_impl,
                             cluster_sched_add_alive_conn_fn_t add_alive_conn_impl,
                             cluster_sched_has_alive_conn_fn_t has_alive_conn_impl,
                             cluster_sched_steal_conn_fn_t steal_conn_impl,
                             destroy_http_cluster_sched_fn_t destructor_fn_impl)
{
    memset(sched, 0, sizeof(*sched));
    sched->_destructor = destructor_fn_impl;
    sched->_get_next_node = get_node_fn_impl;
    sched->_add_alive_conn = add_alive_conn_impl;
    sched->_has_alive_conn = has_alive_conn_impl;
    sched->_steal_conn = steal_conn_impl;
    SYSCALL_PANIC_FAILURE_CHECK(pthread_mutex_init(&sched->sched_lock, NULL), 0, &logger, "pthread_mutex_init");
}


void destroy_http_cluster_sched(http_cluster_sched_t *sched)
{
    sched->_destructor(sched);
    SYSCALL_PANIC_FAILURE_CHECK(pthread_mutex_destroy(&sched->sched_lock), 0, &logger, "pthread_mutex_destroy");
    free(sched);
}


int http_cluster_sched_has_alive_conn(http_cluster_sched_t *sched) {
    SYSCALL_PANIC_FAILURE_CHECK(pthread_mutex_lock(&sched->sched_lock), 0, &logger, "pthread_mutex_lock");
    int res = sched->_has_alive_conn(sched);
    SYSCALL_PANIC_FAILURE_CHECK(pthread_mutex_unlock(&sched->sched_lock), 0, &logger, "pthread_mutex_unlock");
    return res;
}


void http_cluster_sched_steal_conn(http_cluster_sched_t *sched, http_cluster_node_conn_t *conn) {
    SYSCALL_PANIC_FAILURE_CHECK(pthread_mutex_lock(&sched->sched_lock), 0, &logger, "pthread_mutex_lock");
    sched->_steal_conn(sched, conn);
    SYSCALL_PANIC_FAILURE_CHECK(pthread_mutex_unlock(&sched->sched_lock), 0, &logger, "pthread_mutex_unlock");
}


void http_cluster_sched_add_alive_conn(http_cluster_sched_t *sched, http_cluster_node_conn_t *conn) {
    SYSCALL_PANIC_FAILURE_CHECK(pthread_mutex_lock(&sched->sched_lock), 0, &logger, "pthread_mutex_lock");
    sched->_add_alive_conn(sched, conn);
    SYSCALL_PANIC_FAILURE_CHECK(pthread_mutex_unlock(&sched->sched_lock), 0, &logger, "pthread_mutex_unlock");
}


http_cluster_node_conn_t *http_cluster_sched_get_next_node_conn(http_cluster_sched_t *sched) {
    SYSCALL_PANIC_FAILURE_CHECK(pthread_mutex_lock(&sched->sched_lock), 0, &logger, "pthread_mutex_lock");
    http_cluster_node_conn_t *node_conn = sched->_get_next_node(sched);
    SYSCALL_PANIC_FAILURE_CHECK(pthread_mutex_unlock(&sched->sched_lock), 0, &logger, "pthread_mutex_unlock");
    return node_conn;
}
