#include "http_cluster_client.h"

#include "ccl_util.h"
#include "ccl_logger.h"

#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <assert.h>
#include <unistd.h>

extern ccl_logger_t logger;

void deinit_http_cluster(http_cluster_client_t *cluster)
{
    destroy_http_cluster_sched(cluster->sched_strat);
    for (size_t i = 0; i < cluster->nodes_num; ++i) {
        if (cluster->reconnectors[i]) {
            pthread_cancel(cluster->reconnectors[i]);
            pthread_join(cluster->reconnectors[i], NULL);
        }
        destroy_http_cluster_node_conn(cluster->connections + i);
    }

    SYSCALL_PANIC_FAILURE_CHECK(pthread_cond_destroy(&cluster->failed_flag_cond), 0, &logger, "pthread_cond_destroy");
    SYSCALL_PANIC_FAILURE_CHECK(pthread_mutex_destroy(&cluster->failed_flag_lock), 0, &logger, "pthread_mutex_destroy");

    free(cluster->reconnectors);
    free(cluster->connections);
}

struct node_reconn_thread_arg {
    http_cluster_client_t *cluster;
    http_cluster_node_conn_t *conn;
};

#define DEFAULT_CONN_FAILURE_SLEEP_TIMEOUT 5

static void *_http_cluster_node_bg_reconnector(void *arg) {

    struct node_reconn_thread_arg *args = arg;

    http_cluster_client_t *cluster = args->cluster;
    http_cluster_node_conn_t *node_conn = args->conn;

    ccl_log_debug(&logger, "%s: Thread to reconnect to HTTP cluster node %s started",
              __func__, http_cluster_conn_get_info(node_conn));

    free(args);

    while (1) {
        pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);

        ccl_err_t res = http_cluster_node_reconnect(node_conn);

        pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
        pthread_testcancel();

        if (res != CCL_OK) {
            ccl_log_warn(&logger, "%s: Failed to reconnect to HTTP cluster node %s, going to sleep for %lu seconds",
                     __func__, http_cluster_conn_get_info(node_conn), DEFAULT_CONN_FAILURE_SLEEP_TIMEOUT);
            sleep(DEFAULT_CONN_FAILURE_SLEEP_TIMEOUT);
        } else {
            break;
        }
    }

    /**
     * Here we assume that user of this object is single threaded, so
     * node connection can't go down after checking for _alive_conn and locking
     * failed_flag_lock mutex.
     */
    http_cluster_sched_add_alive_conn(cluster->sched_strat, node_conn);

    if (http_cluster_sched_has_alive_conn(cluster->sched_strat)) {

        SYSCALL_PANIC_FAILURE_CHECK(pthread_mutex_lock(&cluster->failed_flag_lock), 0,
                                    &logger, "pthread_mutex_lock");

        cluster->_failed = 0;

        SYSCALL_PANIC_FAILURE_CHECK(pthread_cond_signal(&cluster->failed_flag_cond), 0,
                                    &logger, "pthread_cond_signal");

        SYSCALL_PANIC_FAILURE_CHECK(pthread_mutex_unlock(&cluster->failed_flag_lock), 0,
                                    &logger, "pthread_mutex_unlock");

    }

    return NULL;
}



static void _http_cluster_start_bg_node_reconnection(http_cluster_client_t *cluster,
                                                     http_cluster_node_conn_t *conn)
{
    size_t reconn_index = conn - cluster->connections;

    struct node_reconn_thread_arg *arg = malloc(sizeof(*arg));
    MEMORY_ALLOCATION_FAILURE_CHECK(arg, &logger, "malloc");
    arg->cluster = cluster;
    arg->conn = conn;

    SYSCALL_PANIC_FAILURE_CHECK(pthread_create(cluster->reconnectors + reconn_index,
                                               NULL,
                                               _http_cluster_node_bg_reconnector, arg),
                                0, &logger, "pthread_create");

    if (cluster->reconnectors[reconn_index]) {
        int retval = pthread_join(cluster->reconnectors[reconn_index], NULL); //can fail end return ESRCH if this is a first run
        ccl_log_debug(&logger, "%s: Reconnection thread #%d joined: %d", __func__, 
                  *(cluster->reconnectors + reconn_index), retval);
    }
}

ccl_err_t init_http_cluster(http_cluster_client_t *cluster,
                           http_cluster_node_t *nodes,
                           size_t nodes_num,
                           http_cluster_sched_t *sched_strat,
                           time_t conn_timeout)
{
    memset(cluster, 0, sizeof(*cluster));

    SYSCALL_PANIC_FAILURE_CHECK(pthread_mutex_init(&cluster->failed_flag_lock, NULL), 0, &logger, "pthread_mutex_init");
    SYSCALL_PANIC_FAILURE_CHECK(pthread_cond_init(&cluster->failed_flag_cond, NULL), 0, &logger, "pthread_cond_init");

    cluster->conn_timeout = conn_timeout;
    cluster->sched_strat = sched_strat;
    cluster->nodes_num = nodes_num;
    cluster->reconnectors = calloc(cluster->nodes_num, sizeof(*cluster->reconnectors));
    MEMORY_ALLOCATION_FAILURE_CHECK(cluster->reconnectors, &logger, "calloc");
    cluster->connections = calloc(cluster->nodes_num, sizeof(*cluster->connections));
    MEMORY_ALLOCATION_FAILURE_CHECK(cluster->connections, &logger, "calloc");
    for (size_t i = 0; i < cluster->nodes_num; ++i) {
        ccl_err_t res = connect_to_http_cluster_node(nodes + i, cluster->connections + i, conn_timeout);
        if (res != CCL_OK) {

            if (res == CCL_CONNECT_FAILED) {
                _http_cluster_start_bg_node_reconnection(cluster, cluster->connections + i);
            } else {
                deinit_http_cluster(cluster);
                return res;
            }

        } else {

            http_cluster_sched_add_alive_conn(cluster->sched_strat, cluster->connections + i);
        }
    }

    if (!http_cluster_sched_has_alive_conn(cluster->sched_strat)) {
        cluster->_failed = 1;
        return CCL_CONNECT_FAILED;
    }
    return CCL_OK;
}

#define _http_cluster_send_post_basic(cluster, post_fn, args...) \
    if (is_http_cluster_failed(cluster)) { \
        return CCL_CONNECT_FAILED; \
    } \
\
    http_cluster_node_conn_t *node_conn = http_cluster_sched_get_next_node_conn(cluster->sched_strat); \
    assert(node_conn); \
    ccl_err_t res = post_fn(node_conn, args); \
\
    if (res == CCL_CONNECT_FAILED) { \
\
        http_cluster_sched_steal_conn(cluster->sched_strat, node_conn); \
        _http_cluster_start_bg_node_reconnection(cluster, node_conn); \
\
        if (!http_cluster_sched_has_alive_conn(cluster->sched_strat)) { \
            cluster->_failed = 1; \
        } else { \
            res = CCL_OK; \
        } \
    } \
    return res;\

ccl_err_t http_cluster_send_post_req(http_cluster_client_t *cluster, const char *path,
                                    const char *body, size_t length, const char *encoding_type,
                                    const char *content_type)
{
    _http_cluster_send_post_basic(cluster, http_cluster_node_send_post,
                                  path, body, length,
                                  encoding_type, content_type);
}

ccl_err_t http_cluster_send_multipart_post_req(http_cluster_client_t *cluster,
                                              const char *path,
                                              const char **form_names,
                                              const char **buffer_names,
                                              const char **bodies,
                                              const char **content_types,
                                              const size_t *bodies_sizes,
                                              size_t const bodies_number)
{
    _http_cluster_send_post_basic(cluster, http_cluster_node_send_multipart_post,
                                  path, form_names, buffer_names, bodies, content_types,
                                  bodies_sizes, bodies_number);
}


ccl_err_t http_cluster_reconnect(http_cluster_client_t *cluster)
{
    ccl_err_t res = CCL_OK;

    struct timespec timeout;

    SYSCALL_PANIC_FAILURE_CHECK(pthread_mutex_lock(&cluster->failed_flag_lock), 0, &logger, "pthread_mutex_lock");

    while (cluster->_failed) {

        clock_gettime(CLOCK_REALTIME, &timeout);

        timeout.tv_sec += cluster->conn_timeout;

        int cond_res = pthread_cond_timedwait(&cluster->failed_flag_cond, &cluster->failed_flag_lock, &timeout);

        if (cond_res == ETIMEDOUT) {
            ccl_log_warn(&logger, "%s: Connection with HTTP cluster was not restored after timeout %lu",
                     __func__, cluster->conn_timeout);
            res = CCL_CONNECT_FAILED;
            break;
        }
    }

    SYSCALL_PANIC_FAILURE_CHECK(pthread_mutex_unlock(&cluster->failed_flag_lock), 0, &logger, "pthread_mutex_unlock");

    return res;
}

int is_http_cluster_failed(const http_cluster_client_t *cluster) {
    return cluster->_failed;
}
