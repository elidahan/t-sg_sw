#!/bin/sh

repo init -u https://source.codeaurora.org/external/imx/imx-manifest -b imx-linux-sumo -m imx-4.14.98-2.0.0_ga.xml
repo sync

cd sources
git clone git@git.embedian.com:developer/meta-smarcfimx7-sumo.git
cd ..
DISTRO=fsl-imx-fb MACHINE=imx7dsmarc source ./t-sg_sw/tsg_setup.sh -b tsg_build

MACHINE=imx7dsmarc bitbake -k fsl-image-validation-imx
